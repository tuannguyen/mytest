package com.bp.pensionline.consumer;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.FormSQLHandler;

/** 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/15
 * Consumer for ChangebankDetails Servlet - used to call SQL handler and pass values
 *
 */

public class ChangeBankConsumer {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
	private HashMap requestMap = null; 
	
	/**
	 * Constructor
	 * @param map
	 */
	public ChangeBankConsumer(HashMap map){
		requestMap = map;	
		
	}
	
	/**
	 * update data
	 * @return
	 */
	public boolean processData(){		
		
		boolean isUpdated = false;		

		/*
		 * Get parameters from map
		 */
		String userName = (String)requestMap.get("userName");
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		String data = (String)requestMap.get("Form");
		
		String name = (String)requestMap.get("AccountName");
		String accNumber = (String)requestMap.get("AccountNumber");
		String bank = (String)requestMap.get("Bank");
		String sortCode = (String)requestMap.get("SortCode");
		String rollNumber = (String)requestMap.get("RollNumber");		
		String isBank = (String)requestMap.get("IsBank");
		
		if(isBank.compareTo("checked") == 0){//check if the pay method is bank or building society
			//if bank then roll number must be blank
			rollNumber = "";
		}
		else{
			//if building society then sortcode and accNo must be blank
			sortCode = "";
			accNumber = "";
		}
		
		//insert into casework		
		int id = 0;
		try{			
			CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
			id = caseWorkSQL.insertOracleBankDetails(bGroup, refNo, name, bank, accNumber, sortCode, rollNumber);
			
		}
		catch (Exception e) {			
			LOG.info(e.getMessage() +e.getCause());
		}		
		String casework = String.valueOf(id);//Get caseworkId return
		
		//Update into BP_FORMS table in mySQL Db
		FormSQLHandler formHandler = new FormSQLHandler();
		isUpdated = formHandler.insertSqlDb(userName, bGroup, refNo, casework, Environment.CHANGE_BANK, data);
		formHandler.closeConnection();							
		
		return isUpdated;
	}
}