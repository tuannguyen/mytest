package com.bp.pensionline.consumer;

import java.util.HashMap;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.EOWSQLHandler;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * Consumer for EOW Servlet - used to call SQL handler and pass values
 */
public class EOWConsumer {
	
	private HashMap requestMap = null; 
	
	public EOWConsumer(HashMap map){
		requestMap = map;				
	}
	
	/**
	 * This function is called only to load data from database
	 * @return
	 */
	public HashMap selectData(){
		
		/*		
		 * Get parameters from map
		 */
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		
		HashMap resultMap = new HashMap();
		
		//EOWSQLHandler sqlHandler = new EOWSQLHandler(Environment.SQL);		
		EOWSQLHandler sqlHandler = new EOWSQLHandler(Environment.PENSIONLINE);
		resultMap.put("AdditionalInformation", sqlHandler.selectSqlDb(bGroup, refNo));
		
		EOWSQLHandler oracleHandler = new EOWSQLHandler(Environment.AQUILA);
		resultMap.put("OracleData", oracleHandler.selectOracleDb(bGroup, refNo));		

		//Closing connection to database
		sqlHandler.closeConnection();
		oracleHandler.closeConnection();
		return resultMap;
	}	
	
	/**
	 * This function is called only to update data from database
	 * @return
	 */
	public boolean processData(){		
		boolean isUpdated = false;
		
		EOWSQLHandler oracleHandler = new EOWSQLHandler(Environment.AQUILA);
		//EOWSQLHandler sqlHandler = new EOWSQLHandler(Environment.SQL);
		EOWSQLHandler sqlHandler = new EOWSQLHandler(Environment.PENSIONLINE);
		
		
		
		/*
		 * Get parameters from map
		 */
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		String form = (String)requestMap.get("Form");
		
		HashMap updateMap = (HashMap)requestMap.get("updateMap");
		
		//Get SurvivorPercentage
		int SurvivorPercentage = 100;
		SurvivorPercentage = (Integer)requestMap.get("SurvivorPercentage");
		
		/*
		 * Update old information in DB
		 */
		oracleHandler.updateOracleDb(bGroup, refNo);
		sqlHandler.updateSqlDb(bGroup, refNo);
		
		//Read objects im request map
		for(int i = 1; i <= updateMap.size(); i++){			

			HashMap map = (HashMap)updateMap.get(i);
			String objName = (String)map.get("objName");
			
			if(objName.equals("LumpSum")){
				
				String title = (String)map.get("Title");
				String forenames = (String)map.get("Forenames");
				String initials = (String)map.get("Initials");
				String surname = (String)map.get("Surname");
				String street = (String)map.get("Street");
				String postalTown = (String)map.get("PostalTown");
				String district = (String)map.get("District");
				String county = (String)map.get("County");
				String postcode = (String)map.get("Postcode");
				String country = (String)map.get("Country");
				String relationship = (String)map.get("Relationship");
				String percentage = (String)map.get("Percentage");
				
				isUpdated = oracleHandler.insertOracleDb("LumpSum",bGroup, refNo, title, forenames, street, postalTown, initials, surname, district, county, postcode, country, relationship, percentage);
				
			}
			
			if(objName.equals("SurvivorsPension")){
				
				String title = (String)map.get("Title");
				String forenames = (String)map.get("Forenames");
				String initials = (String)map.get("Initials");
				String surname = (String)map.get("Surname");
				String street = (String)map.get("Street");
				String postalTown = (String)map.get("PostalTown");
				String district = (String)map.get("District");
				String county = (String)map.get("County");
				String postcode = (String)map.get("Postcode");
				String country = (String)map.get("Country");
				String relationship = (String)map.get("Relationship");
				String percentage = String.valueOf(SurvivorPercentage);				
				
				isUpdated = oracleHandler.insertOracleDb("SurvivorsPension",bGroup, refNo, title, forenames, street, postalTown, initials, surname, district, county, postcode, country, relationship, percentage);
				
			}
			
			if(objName.equals("AdditionalInformation")){
								
				String mess = (String)map.get("Message");
				
				isUpdated = sqlHandler.insertSqlDb(bGroup, refNo, mess, form);
			}
		}
		
		//Close Connection to database
		sqlHandler.closeConnection();
		oracleHandler.closeConnection();
		return isUpdated;
	}
}