package com.bp.pensionline.consumer;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.ChangeEmailSQLHandler;
import com.bp.pensionline.database.FormSQLHandler;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * Consumer for ChangeEmail Servlet - used to call SQL handler and pass values
 */
public class ChangeEmailConsumer {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap requestMap = null; 
	
	public ChangeEmailConsumer(HashMap map){
		requestMap = map;				
	}
	
	public boolean processData(){
		/*
		 *  This function is called only to update data from database
		 */
		boolean isUpdated = false;
		
		ChangeEmailSQLHandler oracleHandler = new ChangeEmailSQLHandler(Environment.AQUILA);
		
		/*
		 * Get parameters from map
		 */
		String userName = (String)requestMap.get("userName");
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		
		String emailaddress = (String)requestMap.get("emailaddress");
		
		
		String data = (String)requestMap.get("Form");
	
		/*
		 * Update DB
		 */
		isUpdated = oracleHandler.updateOracleDb(bGroup, refNo, emailaddress);
		oracleHandler.closeConnection();
		
		if(isUpdated){
			int cwNo = -1;
			
			try{
				CaseWorkSQLHandler cwSQLHandler = new CaseWorkSQLHandler();
				cwNo = cwSQLHandler.insertOracleEmail(bGroup, refNo, emailaddress);
			}catch(Exception e){
				LOG.error("could not get casework id in email consumer", e);
			}
			//Update into BP_FORMS table in mySQL Db			
			FormSQLHandler formHandler = new FormSQLHandler();
			formHandler.insertSqlDb(userName, bGroup, refNo, ""+cwNo, Environment.CHANGE_EMAIL, data);
			formHandler.closeConnection();		
		}
			
		return isUpdated;
	}
}