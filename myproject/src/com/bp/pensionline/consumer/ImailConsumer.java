package com.bp.pensionline.consumer;

import java.util.HashMap;

import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.ImailSQLHandler;

/**
 * @author Duy Thao Nguyen
 * @version 1.1
 * @since 04/05/2007
 *
 * <blockquote>
 * This class provide to ImailHandler interact with com.bp.pensionline.database.ImailSQLHandler and com.bp.pensionline.database.CaseWrorkSQLHandler 
 * </blockquote>
 */
public class ImailConsumer {
	private HashMap valueMap = null;

	/**
	 * @param map
	 * Contructor with map param
	 */
	public ImailConsumer(HashMap map) {
		valueMap = map;
	}

	/**
	 * @throws Exception
	 * this method does 2 job : insert into oracle databae and insert into mysql database 
	 */
	public void processData() throws Exception {
		CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
		// Business process: Remove casework, replaced by using IMail
		//caseWorkSQL.insertOracleImail(valueMap);
		
		
		ImailSQLHandler.insertMySqlImail(valueMap);
		

	}

}
