package com.bp.pensionline.consumer;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.opencms.file.CmsObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.ChangeAddressSQLHandler;
import com.bp.pensionline.database.FormSQLHandler;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/8
 * Consumer for ChangeAddress Servlet - used to call SQL handler and pass values
 *
 */
public class ChangeAddressConsumer {
	
	private HashMap requestMap = null; 
	
	public ChangeAddressConsumer(HashMap map){
		requestMap = map;				
	}
	
	public boolean processData(){
		/*
		 *  This function is called only to update data from database
		 */
		
		boolean isUpdated = false;
		boolean isTestHarness = false;
		isTestHarness = CheckConfigurationKey.getBooleanValue("testHarness.enable");
		/*
		 * Get parameters from map
		 */
		String userName = (String)requestMap.get("userName");
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		String data = (String)requestMap.get("Form");
		
		String home = (String)requestMap.get("Home");
		String street = (String)requestMap.get("Street");
		String district = (String)requestMap.get("District");
		String town = (String)requestMap.get("Town");
		String county = (String)requestMap.get("County");
		String postcode = (String)requestMap.get("Postcode");
		String country = (String)requestMap.get("Country");
		
		//Check tesherness is enabled or not
		
		if(isTestHarness){
			//testHarness.enable = TRUE
			try{
				String fileName = Environment.XMLFILE_DIR + "Member_" + bGroup + "_" + refNo + ".xml";
				CmsObject obj = SystemAccount.getAdminCmsObject();
				boolean boo = obj.existsResource(fileName);
				if (boo) {

					XmlReader reader = new XmlReader();
					ByteArrayInputStream bIn = new ByteArrayInputStream(reader.readFile(fileName));
					DocumentBuilderFactory Domfactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = Domfactory.newDocumentBuilder();
					Document document = null;
					document = builder.parse(bIn);						
					
					NodeList rootList = document.getElementsByTagName("Address");
					
					if (rootList != null) {
						
						Node root = rootList.item(0);
						if (root.getNodeType() == Node.ELEMENT_NODE) {

							StringBuffer buff = new StringBuffer();
							//Append all values into one to update XML file  
							buff.append(home).append(" - ");
							buff.append(street).append(" - ");
							buff.append(district).append(" - ");
							buff.append(town).append(" - ");
							buff.append(county).append(" - ");
							buff.append(postcode).append(" - ");
							buff.append(country);

							NodeList memList = root.getChildNodes();
							Node node = memList.item(0);
							node.setTextContent(buff.toString());
							
							com.bp.pensionline.test.BookmarkMember.delXML(fileName);
							com.bp.pensionline.test.BookmarkMember.toXML(document, fileName);								
							isUpdated = true;						
						} 
						else {
						}
					} 
					else {			
					}
				}	
			}
			catch (Exception e) {
				// TODO: handle exception
			}		
		}
		else{			
			ChangeAddressSQLHandler oracleHandler = new ChangeAddressSQLHandler(Environment.AQUILA);
			/*
			 * Update Oracle DB
			 */
			isUpdated = oracleHandler.updateOracleDb(bGroup, refNo, home, street, district, town, county, postcode, country);
			oracleHandler.closeConnection();
			
		}
		
		if(isUpdated){			
			//Update into BP_FORMS table in mySQL Db
			FormSQLHandler formHandler = new FormSQLHandler();
			formHandler.insertSqlDb(userName, bGroup, refNo, null, Environment.CHANGE_ADDRESS, data);
			formHandler.closeConnection();
		}
		return isUpdated;
	}
}