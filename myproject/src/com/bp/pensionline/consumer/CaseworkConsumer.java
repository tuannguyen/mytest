package com.bp.pensionline.consumer;

import java.util.HashMap;

import com.bp.pensionline.database.FormSQLHandler;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/17
 * Consumer for CaseworkHandler Servlet - used to call SQL handler and pass values
 */
public class CaseworkConsumer {
	
	private HashMap requestMap = null; 
	
	/**
	 * Constructor
	 * @param map
	 */
	public CaseworkConsumer(HashMap map){
		requestMap = map;				
	}
	
	/**
	 * select data from database using SQLHandler
	 * @return
	 */
	public HashMap selectData(){
		
		//Get parameters
		String casework = (String)requestMap.get("CaseWorkID");
		String bGroup = (String)requestMap.get("Bgroup");
		
		//Update into BP_FORMS table in mySQL Db
		FormSQLHandler handler = new FormSQLHandler();
		HashMap map = handler.selectSqlDb(casework, bGroup);
		handler.closeConnection();
				
		return map;
	}
}