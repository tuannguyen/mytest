package com.bp.pensionline.consumer;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.ChangePhoneSQLHandler;
import com.bp.pensionline.database.FormSQLHandler;

/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * Consumer for ChangePhone Servlet - used to call SQL handler and pass values
 */

public class ChangePhoneConsumer {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private HashMap requestMap = null; 
	
	public ChangePhoneConsumer(HashMap map){
		requestMap = map;				
	}
	
	public boolean processData(){
		/*
		 *  This function is called only to update data from database
		 */
		boolean isUpdated = false;
		
		ChangePhoneSQLHandler oracleHandler = new ChangePhoneSQLHandler(Environment.AQUILA);
		
		/*
		 * Get parameters from map
		 */
		String userName = (String)requestMap.get("userName");
		String bGroup = (String)requestMap.get("bGroup");
		String refNo = (String)requestMap.get("refNo");
		
		String phone = (String)requestMap.get("phone");
		String mobile = (String)requestMap.get("mobile");
		String fax = (String)requestMap.get("fax");
		
		String data = (String)requestMap.get("Form");
		
		//handle empty 
		
		/*
		 * Update DB
		 */
		isUpdated = oracleHandler.updateOracleDb(bGroup, refNo, phone, fax, mobile);
		oracleHandler.closeConnection();
		
		if(isUpdated){
			//raise casework
			int cwNo = -1;
			
			try{
				CaseWorkSQLHandler cwSQLHandler = new CaseWorkSQLHandler();
				cwNo = cwSQLHandler.insertOraclePhone( bGroup,  refNo, phone,  fax,  mobile);
			}catch(Exception e){
				LOG.error("could not get casework id in phone consumer", e);
			}
			
			//Update into BP_FORMS table in mySQL Db
			FormSQLHandler formHandler = new FormSQLHandler();
			formHandler.insertSqlDb(userName, bGroup, refNo, ""+cwNo, Environment.CHANGE_PHONE, data);
			formHandler.closeConnection();
		}
		return isUpdated;
	}
}