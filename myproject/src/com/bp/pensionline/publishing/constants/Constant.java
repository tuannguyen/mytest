/*
 * Constant.java
 *
 * Created on March 15, 2007, 2:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.publishing.constants;

/**
 *
 * @author mall
 */
public class Constant {
    public static final String PARAM = "xml";
    public static final String CONTENT_TYPE = "text/xml";
    
    /** Creates a new instance of Constant */
    public static final String AJAX_PUBLISHING_REQUESTED = "AjaxPublishingParameterRequest";
    public static final String AJAX_PUBLISHING_RESPONSE = "AjaxPublishingParameterResponse";    
    
    /* Password letter */
    public static final String PUBLISHING_PAGE_ID = "ptb_pageId";
    public static final String PUBLISHING_PAGE_URI = "ptb_pageURI";
    public static final String PUBLISHING_ACTION = "ptb_action";
    public static final String PUBLISHING_REASON = "ptb_reason";   
    public static final String PUBLISHING_BUG_ID = "ptb_bugId";      
    
    public static final String PUBLISHING_ACTION_REVOKECHANGE = "RevokeChange";
    public static final String PUBLISHING_ACTION_UNDOCHANGE = "UndoChange";
    public static final String PUBLISHING_ACTION_SEND4REVIEW = "Send4Review";
    public static final String PUBLISHING_ACTION_PASSCHECKING = "PassChecking";
    public static final String PUBLISHING_ACTION_APPROVECHANGE = "ApproveChange";   
    public static final String PUBLISHING_ACTION_REJECTCHANGE = "RejectChange"; 
    public static final String PUBLISHING_ACTION_UNAPPROVECHANGE = "UnapproveChange";
    public static final String PUBLISHING_ACTION_DEPLOYALLCHANGES = "DeployAlChanges";
    public static final String PUBLISHING_ACTION_PAUSEDEPLOY = "PauseDeploy"; 
    public static final String PUBLISHING_ACTION_UNPAUSEDEPLOY = "UnpauseDeploy";

}
