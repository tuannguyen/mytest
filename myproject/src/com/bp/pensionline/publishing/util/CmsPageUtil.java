package com.bp.pensionline.publishing.util;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsFolder;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.CmsUser;
import org.opencms.importexport.CmsVfsImportExportHandler;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.report.CmsShellReport;
import org.opencms.util.CmsUUID;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.publishing.dto.ReleasePageDTO;

public class CmsPageUtil
{
	public static final Log LOG = CmsLog.getLog(CmsPageUtil.class);
	
	public static Vector<ReleasePageDTO> listAllChangedFiles(CmsObject cms, String path)
			throws CmsException
	{
		Vector<ReleasePageDTO> updatePageDTOs = new Vector<ReleasePageDTO>();	
		
		CmsFolder cmsfolder = null;
		CmsFile cmsFile = null;

		String pathDir = "";
		String pathFile = "";
		
		if (cms.existsResource(path))
		{
			List listFolders = cms.getSubFolders(path);		
			List listFilesNoDir = cms.getFilesInFolder(path, CmsResourceFilter.ALL);			
			
			for (int m = 0; m < listFilesNoDir.size(); m++)
			{
				cmsFile = (CmsFile) listFilesNoDir.get(m);
				String fileName = cmsFile.getName();
				// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
				if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
						&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
				{
//					LOG.info("file name: " + (path + fileName));
//					LOG.info("file type: " + cmsFile.getState());
					if (cmsFile.getState() == CmsResource.STATE_CHANGED)
					{
						ReleasePageDTO pageDTO = new ReleasePageDTO();
						pathFile = path + fileName;
						pageDTO.setPageURI(pathFile);
						pageDTO.setMode("Edited");
						long lastUpdate = cmsFile.getDateLastModified();
						pageDTO.setLastUpdate(DateTimeUtil.formatToOrderingTime(lastUpdate));
						pageDTO.setLastModifiedBy(cmsFile.getUserLastModified().toString());
						updatePageDTOs.addElement(pageDTO);
					}
					else if (cmsFile.getState() == CmsResource.STATE_NEW)
					{
						ReleasePageDTO pageDTO = new ReleasePageDTO();
						pathFile = path + fileName;
						pageDTO.setPageURI(pathFile);
						pageDTO.setMode("New");
						long lastUpdate = cmsFile.getDateLastModified();
						pageDTO.setLastUpdate(DateTimeUtil.formatToOrderingTime(lastUpdate));
						pageDTO.setLastModifiedBy(cmsFile.getUserLastModified().toString());
						updatePageDTOs.addElement(pageDTO);
					}
					/*
					 * Ignore deleting in galleries in this implementation
					 */
					else if (cmsFile.getState() == CmsResource.STATE_DELETED)
					{
						LOG.info("Deleted file name: " + (path + fileName));
						ReleasePageDTO pageDTO = new ReleasePageDTO();
						pathFile = path + fileName;
						pageDTO.setPageURI(pathFile);
						pageDTO.setMode("Deleted");
						long lastUpdate = cmsFile.getDateLastModified();
						pageDTO.setLastUpdate(DateTimeUtil.formatToOrderingTime(lastUpdate));
						 
						updatePageDTOs.addElement(pageDTO);
					}
				}			
			}
	
			for (int i = 0; i < listFolders.size(); i++)
			{
				cmsfolder = (CmsFolder) listFolders.get(i);
				pathDir = path + cmsfolder.getName() + "/";
				updatePageDTOs.addAll(listAllChangedFiles(cms, pathDir));			
			}
		}
		return updatePageDTOs;
	}

	/**
	 * Get all template HTML files potential for publishing to initialize the publishing page system
	 * exclude the files that matches one of parterns in excludeFiles
	 * @param cms
	 * @param path
	 * @return
	 * @throws CmsException
	 */
	public static Vector<String> listAllTemplateHtmlFiles(CmsObject cms,
			String path, String[] excludeFiles) throws CmsException
	{
		Vector<String> filesfolders = new Vector<String>();

		CmsFolder cmsfolder = null;
		CmsFile cmsFile = null;

		String pathDir = "";
		String pathFile = "";

		if (cms.existsResource(path))
		{

			List listFolders = cms.getSubFolders(path);
			List listFilesNoDir = cms.getFilesInFolder(path, CmsResourceFilter.ALL);

			for (int m = 0; m < listFilesNoDir.size(); m++)
			{
				cmsFile = (CmsFile) listFilesNoDir.get(m);
				String fileName = cmsFile.getName();
				// only get publishing files that are the not JSP, CSS, CLASS, JS and XML and has type = 6 (not struture content)
				if (fileName != null && fileName.endsWith(".html") && fileName.charAt(0) != '~'
					&& cmsFile.getTypeId() == 6)
				{
					pathFile = path + fileName;
					
					boolean isExcluded = false;
					
					if (excludeFiles != null && excludeFiles.length > 0)
					{
						for (int i = 0; i < excludeFiles.length; i++)
						{
							String excludeFilePartern = excludeFiles[i];
							if (pathFile.matches(excludeFilePartern))
							{
								isExcluded = true;
								break;
							}
						}
					}	
					
					if (!isExcluded)
					{
						filesfolders.addElement(pathFile);
					}
					
					//LOG.info(pathFile + " has type: " + cmsFile.getTypeId());
				}
			}

			for (int i = 0; i < listFolders.size(); i++)
			{
				cmsfolder = (CmsFolder) listFolders.get(i);
				
				// exclude folders								
				pathDir = path + cmsfolder.getName() + "/";
				filesfolders.addAll(listAllTemplateHtmlFiles(cms, pathDir, excludeFiles));
							
			}
		}
		return filesfolders;
	}

	/**
	 * Get all struture HTML files potential for publishing to initialize the publishing page system
	 * @param cms
	 * @param path
	 * @return
	 * @throws CmsException
	 */
	public static Vector<String> listAllStrutureHtmlFiles(CmsObject cms,
			String path) throws CmsException {
		LOG.info("listAllStrutureHtmlFiles():BEGIN");
		Vector<String> filesfolders = new Vector<String>();

		CmsFile cmsFile = null;
		String pathFile = "";

		//cms.getRequestContext().setCurrentProject(cms.readProject("Offline"));
		String root = cms.getRequestContext().getSiteRoot();
		LOG.info("Root: "+root);
		if (root.toLowerCase().indexOf("/sites/")!=-1) {
			cms.getRequestContext().setSiteRoot("/");
		}
		
		if (cms.existsResource(path)) { 
		//try {
			List listFilesNoDir = cms.getFilesInFolder(path, CmsResourceFilter.ALL);
			LOG.info("All files in "+path+": "+listFilesNoDir.size());
			for (int m = 0; m < listFilesNoDir.size(); m++)
			{
				cmsFile = (CmsFile) listFilesNoDir.get(m);
				String fileName = cmsFile.getName();
				// only get publishing files that are the not JSP, CSS, CLASS, JS and XML and has type = 6 (not struture content)
				if (fileName != null && fileName.endsWith(".html") && fileName.charAt(0) != '~'
					&& cmsFile.getTypeId() != 6)
				{
					pathFile = path + fileName;
					filesfolders.addElement(pathFile);	
					//LOG.info(pathFile + " has type: " + cmsFile.getTypeId());
				}
			}
		} /*catch (CmsException cme) {
			LOG.error("EXCEPTION: "+cme.toString());
			cme.printStackTrace();
		} */
		LOG.info("HTML files in "+path+": "+filesfolders.size());
		LOG.info("listAllStrutureHtmlFiles():END");
		return filesfolders;
	}	
	
	/**
	 * Get all the JSP files potential for publishing to initialize the publishing page system for struture content
	 * , excludes the files that matches one of parterns in excludeFiles 
	 * @param cms
	 * @param path
	 * @return
	 * @throws CmsException
	 */
	public static Vector<String> listAllJSPFiles(CmsObject cms,
			String path, String[] excludeFiles) throws CmsException
	{
		Vector<String> filesfolders = new Vector<String>();

		CmsFolder cmsfolder = null;
		CmsFile cmsFile = null;

		String pathDir = "";
		String pathFile = "";

		if (cms.existsResource(path))
		{

			List listFolders = cms.getSubFolders(path);
			List listFilesNoDir = cms.getFilesInFolder(path, CmsResourceFilter.ALL);

			for (int m = 0; m < listFilesNoDir.size(); m++)
			{
				cmsFile = (CmsFile) listFilesNoDir.get(m);
				String fileName = cmsFile.getName();
				// only get publishing files that are the not JSP, CSS, CLASS, JS and XML and has type = 6 (not struture content)
				if (fileName != null && fileName.endsWith(".jsp") && fileName.charAt(0) != '~')
				{
					pathFile = path + fileName;
					
					boolean isExcluded = false;
					
					if (excludeFiles != null && excludeFiles.length > 0)
					{
						for (int i = 0; i < excludeFiles.length; i++)
						{
							String excludeFilePartern = excludeFiles[i];
							if (pathFile.matches(excludeFilePartern))
							{
								isExcluded = true;
								break;
							}
						}
					}
					
					if (!isExcluded)
					{
						filesfolders.addElement(pathFile);
					}
						
					//LOG.info(pathFile + " has type: " + cmsFile.getTypeId());
				}
			}

			for (int i = 0; i < listFolders.size(); i++)
			{
				cmsfolder = (CmsFolder) listFolders.get(i);
				pathDir = path + cmsfolder.getName() + "/";
				
				filesfolders.addAll(listAllJSPFiles(cms, pathDir, excludeFiles));
			}
		}
		return filesfolders;
	}	
	
	public static synchronized int getPageState (CmsObject cms, String resourceURI) {
		if (resourceURI != null)
		{
			try
			{				
				if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
				{
					CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
					if (cmsResource.isFile() )
					{
						String fileName = cmsResource.getName();
						// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
						if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
								&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
						{
							return cmsResource.getState().getState();
						}												
					}
				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error in getting resouce status: " + cmse.toString());
			}

		}
		
		return -1;
	}
	
	public static boolean isPageExisted (CmsObject cms, String resourceURI)
	{
		if (resourceURI != null)
		{
			if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Get last modified time of the resouce
	 * @param cms
	 * @param resourceURI
	 * @return
	 */
	public static long getPageLastModifyTime (CmsObject cms, String resourceURI)
	{
		if (resourceURI != null)
		{
			try
			{				
				if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
				{
					CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
					if (cmsResource.isFile() )
					{
						String fileName = cmsResource.getName();
						// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
						if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
								&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
						{
							return cmsResource.getDateLastModified();
						}												
					}
				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error in getting resouce last modified: " + cmse.toString());
			}

		}
		
		return -1;
	}
	/**
	 * Get last modified user id of the resouce
	 * @param cms
	 * @param resourceURI
	 * @return
	 */
	public static String getPageLastModifyUserId (CmsObject cms, String resourceURI)
	{
		if (resourceURI != null)
		{
			try
			{				
				if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
				{
					CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
					if (cmsResource.isFile() )
					{
						String fileName = cmsResource.getName();
						// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
						if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
								&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
						{
							return cmsResource.getUserLastModified().toString();
						}												
					}
				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error in getting resouce last modified: " + cmse.toString());
			}

		}
		
		return null;
	}	
	
	/**
	 * Get last modified user name of the resouce
	 * @param cms
	 * @param resourceURI
	 * @return
	 */
	public static String getPageLastModifyUserName (CmsObject cms, String resourceURI)
	{
		if (resourceURI != null)
		{
			try
			{				
				if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
				{
					CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
					if (cmsResource.isFile() )
					{
						String fileName = cmsResource.getName();
						// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
						if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
								&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
						{
							CmsUUID userId = cmsResource.getUserLastModified();
							if (userId != null)
							{
								CmsUser lastModifyUser = cms.readUser(userId);
								if (lastModifyUser != null)
								{
									return lastModifyUser.getName();
								}
							}
						}												
					}
				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error in getting resouce last modified: " + cmse.toString());
			}

		}
		
		return null;
	}		
	
	/**
	 * Undo all changes made to this resouce from lastest publishing
	 * @param cms
	 * @param resourceURI: fullpath
	 * @return
	 */
	public static boolean undoAllChanges (CmsObject cms, String resourceURI)
	{
		if (resourceURI != null)
		{
			try
			{				
				if (cms.existsResource(resourceURI, CmsResourceFilter.ALL))
				{
					CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
					if (cmsResource != null && cmsResource.isFile() )
					{
						String fileName = cmsResource.getName();
						// only get publishing files that are the not JSP, CSS, CLASS, JS and XML
						if (!fileName.endsWith(".jsp") && !fileName.endsWith(".css") 
								&& !fileName.endsWith(".class") && !fileName.endsWith(".js") && !fileName.endsWith(".xml"))
						{
							// If page is new created, delete it
							if (cmsResource.getState() == CmsResource.STATE_NEW)
							{
								LOG.info("Undoing changes (delete new created file) made to:  " + fileName);
								checkLock(cms, resourceURI);
								cms.deleteResource(resourceURI, CmsResource.DELETE_PRESERVE_SIBLINGS);
							}
							else
							{
								LOG.info("Undoing changes (undo all changes) made to:  " + fileName);
								// lock the resource
								checkLock(cms, resourceURI);
								// undo all chages
								cms.undoChanges(resourceURI, false);
								// release the lock
								cms.unlockResource(resourceURI);
							}
							
							return true;
						}												
					}
				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error in undoAllChanges: " + cmse.toString());
			}

		}
		
		return false;
	}	
	
    /**
     * Checks the lock state of the resource and locks it if the autolock feature is enabled.<p>
     * 
     * @param resource the resource name which is checked
     * @throws CmsException if reading or locking the resource fails
     */
    public static void checkLock(CmsObject cms, String resource) throws CmsException {
        //checkLock(cms, resource, org.opencms.lock.CmsLock.COMMON);
    	//pension line based 7.0.5
    	CmsResource res = cms.readResource(resource, CmsResourceFilter.ALL);
        CmsLock lock = cms.getLock(res);
        if (lock.isNullLock()) {
            // resource is not locked, lock it automatically
            cms.lockResource(resource);
        } else if (!lock.getUserId().equals(cms.getRequestContext().currentUser().getId())) {
        	cms.changeLock(resource);
        } 
    }

    /**
     * Checks the lock state of the resource and locks it if the autolock feature is enabled.<p>
     * 
     * @param resource the resource name which is checked
     * @param mode flag indicating the mode (temporary or common) of a lock
     * @throws CmsException if reading or locking the resource fails
     */
    public static void checkLock(CmsObject cms, String resource, int mode) throws CmsException {

        CmsResource res = cms.readResource(resource, CmsResourceFilter.ALL);
        CmsLock lock = cms.getLock(res);
//        if (OpenCms.getWorkplaceManager().autoLockResources()) {
//            // autolock is enabled, check the lock state of the resource
//            if (lock.isNullLock()) {
//                // resource is not locked, lock it automatically
//                cms.lockResource(resource, mode);
//            } else if (!lock.getUserId().equals(cms.getRequestContext().currentUser().getId())) 
//            {
//                throw new CmsException(Messages.get().container(Messages.ERR_WORKPLACE_LOCK_RESOURCE_1, resource));
//            }
//        } else {
//            if (lock.isNullLock()
//                || (!lock.isNullLock() && !lock.getUserId().equals(cms.getRequestContext().currentUser().getId()))) {
//                throw new CmsException(Messages.get().container(Messages.ERR_WORKPLACE_LOCK_RESOURCE_1, resource));
//        	}
//    	}
        
        // autolock is enabled, check the lock state of the resource
        if (lock.isNullLock()) {
            // resource is not locked, lock it automatically
            cms.lockResource(resource, mode);
        } else if (!lock.getUserId().equals(cms.getRequestContext().currentUser().getId())) 
        {
        	cms.changeLock(resource);
        }        
    }
    
    /**
     * Extrac all id of embeded changed child pages that are contained in the paged 
     * @param cms
     * @param pageURI
     * @param allChildPages
     * @return 
     */
    public static Vector<ReleasePageDTO> getEmbededChildPagesChanged (CmsObject cms, String pageURI, Vector allChildPages)
    {
    	Vector<ReleasePageDTO> embededPages = new Vector<ReleasePageDTO>();
    	
    	if (pageURI != null && allChildPages != null && allChildPages.size() > 0)
		{
        	try
    		{
        		String linkContent = getLinksInContent(cms, pageURI);
        		//LOG.info("linkContent: " + linkContent);
        		for (int i = 0; i < allChildPages.size(); i++)
    			{
    				ReleasePageDTO childPageDTO = (ReleasePageDTO)allChildPages.elementAt(i);
    				String childPageURI = childPageDTO.getPageURI();
    				if (linkContent != null && linkContent.indexOf(childPageURI) >= 0)
					{
    					LOG.info("pageURI: " + pageURI + " has " + childPageURI);
    					embededPages.add(childPageDTO);
					}				
    			}
    		}
    		catch (CmsException cmse)
    		{
    			LOG.error("Error in getBodyContent: " + cmse.toString());
    		}
		}
    	
    	return embededPages;
    }
    
    /**
     * Get body content of HTML page
     * @param cms
     * @param pageURI
     * @return
     * @throws CmsException
     */
	public static String getLinksInContent (CmsObject cms, String pageURI) throws CmsException
	{		
		if (pageURI.endsWith(".html") || pageURI.endsWith(".htm"))
		{
			StringBuffer resultBuf = new StringBuffer();
			
			boolean exist = cms.existsResource(pageURI);
			byte[] contentBytes = null;
			if(exist){//if true
				
				//read file and return a byte array
				CmsFile xmlFile = (CmsFile)cms.readFile(pageURI);		
				contentBytes = xmlFile.getContents();			
			}
			
			if (contentBytes == null)
			{
				return null;
			}
			
			// Get the template file names from letter_mapping.xml
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			try
			{
				builder = factory.newDocumentBuilder();

				ByteArrayInputStream is = new ByteArrayInputStream(contentBytes);

				document = builder.parse(is);				
				
				NodeList linkNodes = document.getElementsByTagName("link");
				
				// finding the letter node for reset password letter
				for (int i = 0; i < linkNodes.getLength(); i++)
				{
					Node linkNode = linkNodes.item(i);
					// finding CDATA section node
					if (linkNode.getNodeType() == Node.ELEMENT_NODE)
					{
						NodeList targetNodes = linkNode.getChildNodes();
						for (int j = 0; j < targetNodes.getLength(); j++)
						{
							Node targetNode = targetNodes.item(j);
							if (targetNode != null && targetNode.getNodeType() == Node.ELEMENT_NODE &&
									targetNode.getNodeName() != null && targetNode.getNodeName().equals("target"))
							{
								NodeList textNodes = targetNode.getChildNodes();
								for (int k = 0; k < textNodes.getLength(); k++)
								{
									Node textNode = textNodes.item(k);
									if (textNode.getNodeType() == Node.CDATA_SECTION_NODE)
									{
										resultBuf.append(((CDATASection)textNode).getData()).append("\n");
										break;
									}						
								}
								break;
							}						
						}						

					}
					
				}	
			}
			catch (Exception ex)
			{
				LOG.error("Error in parsing XML getBodyContent of publishing: " + ex.toString());
				return null;
			}		

			// replace any special squence of charaters that make the transformation failed.
//			String result = resultBuf.toString().replaceAll("&nbsp;", " ");
//			result = result.replaceAll("[\n]", " ");
//			result = result.replaceAll("[\t]", "");
			
			return resultBuf.toString();
		}
		
		return null;
	}
	
	/**
	 * Publish pages in the current system
	 * @param pageURIs
	 * @return
	 */
	public static boolean publishPage (CmsObject cms, String pageURI)
	{		
		if (pageURI != null)
		{
			try
			{			
				LOG.info("Publishing page: " + pageURI);
				cms.publishResource(pageURI);
				LOG.info("Page pulished: " + pageURI);
				return true;			
			}
			catch (Exception e)
			{
				LOG.error("Error while publishing resource: " + pageURI + " : " + e);
			}
			
		}
		
		return false;
	}
	
	/**
	 * Publish pages in the current system
	 * @param pageURIs
	 * @return
	 */
	public static void publishPage (CmsObject cms, ArrayList<String> pageURIs)
	{		
		if (cms != null && pageURIs != null)
		{
			try
			{
				
                List publishResources = new ArrayList(pageURIs.size());

				// get the offline resource(s) in direct publish mode
				Iterator i = pageURIs.iterator();
				while (i.hasNext())
				{
					String resName = (String) i.next();
					LOG.info("Publishing file: " + resName);
					try
					{						
						if (cms.existsResource(resName, CmsResourceFilter.ALL))
						{
							CmsResource res = cms.readResource(resName, CmsResourceFilter.ALL);
							publishResources.add(res);
							
							// check if the resource is locked
							CmsLock lock = cms.getLock(resName);
							if (!lock.isNullLock())
							{
								// resource is locked, so unlock it
								cms.changeLock(resName);
								cms.unlockResource(resName);
							}							
						}
					}
					catch (CmsException cmse)
					{
						LOG.error("Error in initilize publish resources: " + cmse);
					}
				}
				
				// create publish list for direct publish
                CmsPublishList publishList = cms.getPublishList(
                    publishResources,
                    false,
                    false);
                //cms.checkPublishPermissions(publishList);
                
                //LOG.info("Project type: " + cms.getRequestContext().currentProject().getType());
                PLPublishThread thread = new PLPublishThread(cms, publishList);
                LOG.info("Start publishing thread");
                // start the publish thread
                thread.start();
                // join() with current thread to avoid reading unpulishing pages from recent_changes.jsp
                thread.join();
                			
			}
			catch (Exception e)
			{
				LOG.error("Error in publishing files: " + e.toString());
			}

			
		}
		
	}	
	
	
	
    /**
     * Exports a list of resources from the current site root to a ZIP file.<p>
     * 
     * The resource names in the list must be separated with a ";".<p>
     *
     * @param exportFile the name (absolute path) of the ZIP file to export to 
     * @param pathList the list of resource to export, separated with a ";"
     * @throws Exception if something goes wrong
     */
    public static boolean exportResources(CmsObject cms, String exportFile, ArrayList exportPaths, boolean includeSystem)
    {
    	if (exportFile == null || exportPaths == null || exportPaths.size() == 0)
		{
			return false;
		}
    	
    	// final exportPaths variable
    	ArrayList validExportPaths = new ArrayList();    	
    	try
		{
        	for (int i = 0; i < exportPaths.size(); i++)
    		{
    			String resourceURI = (String)exportPaths.get(i);
    			if (cms.existsResource(resourceURI))
    			{
    				CmsResource cmsResource = cms.readResource(resourceURI, CmsResourceFilter.ALL);
    				if (cmsResource.isFile() && cmsResource.getState() != CmsResource.STATE_DELETED)
    				{
    					validExportPaths.add(resourceURI);
    				}    				
    			}
    		}
        	
		}
		catch (CmsException e)
		{
			LOG.error("Error in checking resources: " + e);
			return false;
		}

    	try
		{
            CmsVfsImportExportHandler vfsExportHandler = new CmsVfsImportExportHandler();
            vfsExportHandler.setFileName(exportFile);
            vfsExportHandler.setExportPaths(validExportPaths);
            vfsExportHandler.setIncludeSystem(includeSystem);
            vfsExportHandler.setIncludeUnchanged(true);
            vfsExportHandler.setExportUserdata(false);

            OpenCms.getImportExportManager().exportData(
            	cms,
                vfsExportHandler,
                new CmsShellReport(cms.getRequestContext().getLocale()));
		}
		catch (Exception e)
		{
			LOG.error("Error while exporting VFS resources: " + e.toString());
		}

        // Always return true so ignore any exporting issue
        return true;
    }	
}
