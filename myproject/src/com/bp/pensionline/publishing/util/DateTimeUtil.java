package com.bp.pensionline.publishing.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

public class DateTimeUtil
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static long formatToOrderingTime (long timeInMillis)
	{
		long orderingTime = -1;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderingTimeString = dateFormat.format(new Date(timeInMillis));
		
		orderingTime = new Long(orderingTimeString).longValue();
		
		return orderingTime;
	}
	
	public static Date getDateFromOrderingTime (long timeInOrderingFormat)
	{
		try
		{
			String timeOrderingStr = new Long(timeInOrderingFormat).toString();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = dateFormat.parse(timeOrderingStr);
			
			return date;
		}
		catch (Exception e)
		{
			LOG.error("Error in getDateFromOrderingTime: " + e.toString());			
		}

		return new Date();
	}	
}
