package com.bp.pensionline.publishing.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.workplace.CmsWorkplaceManager;
import org.opencms.workplace.CmsWorkplaceSettings;

import com.bp.pensionline.publishing.constants.Constant;
import com.bp.pensionline.util.SystemAccount;

public class ProjectSwitchingHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String ptbRole = request.getParameter("ptb_role");
		String ptbAction = request.getParameter("ptb_action");
		LOG.info("action = " + ptbAction);
		LOG.info("ptbRole = " + ptbRole);
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		CmsObject cmsObject = SystemAccount.getDefaultObject();

		String xmlResponse = null;

		if (currentUser != null)
		{
			if (ptbRole != null && ptbRole.equals("Authors"))
			{
				if (ptbAction!= null && ptbAction.equals("Check_in"))
				{
					// Tell the landing page manager I'm editing page in offline mode. Don't worry for me!
					currentUser.setAdditionalInfo("PtbEditingFlag", "1");
				}
				else if (ptbAction!= null && ptbAction.equals("Check_out"))
				{
					String editingFlag = (String)currentUser.getAdditionalInfo("PtbEditingFlag");
					if (editingFlag != null && editingFlag.equals("1"))
					{
						// Good, you are working. Remember to flag up your existence.
						currentUser.setAdditionalInfo("PtbEditingFlag", "0");
					}
					else 
					{
						// Where are you! You're off.
						currentUser.setAdditionalInfo("PtbEditingFlag", "0");
						
						// Bye! I'm going to the Online mode
						if (cmsObject != null)
						{
							try
							{
								CmsWorkplaceSettings wp_settings = (CmsWorkplaceSettings)request.getSession().getAttribute(CmsWorkplaceManager.SESSION_WORKPLACE_SETTINGS);
								wp_settings.setProject(cmsObject.readProject("Online").getUuid());
								//wp_settings.setProject(cmsObject.readProject("Online").getId());
								request.getSession().setAttribute(CmsWorkplaceManager.SESSION_WORKPLACE_SETTINGS, wp_settings);								
							}
							catch (CmsException cmse)
							{
								LOG.error("Error in switching project: " + cmse.toString());
								xmlResponse = buildXmlResponseError("Switching project failed.");
							}

						}
						
						// Hey, did you complete the tasks?
						String ptbEditingComplete = (String)currentUser.getAdditionalInfo("PtbEditingComplete");
						if (ptbEditingComplete != null && ptbEditingComplete.equals("1"))
						{
							// Great! You complete! I will update the list for you.
							currentUser.setAdditionalInfo("PtbEditingComplete", "0");
							buildXmlResponse(true);
						}
						else
						{
							// You not! Ok, you can do it later.
							buildXmlResponse(false);
						}
					}
				}
			}
		}
		else
		{
			xmlResponse = buildXmlResponseError("Current user not specified!");
		}

		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isEditComplete) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <PtbEditingComplete>").append(isEditComplete).append("</PtbEditingComplete>\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}	

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}