package com.bp.pensionline.publishing.handler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.publishing.constants.Constant;
import com.bp.pensionline.publishing.database.PublishingToolbarSQLHandler;
import com.bp.pensionline.publishing.dto.ReleasePageDTO;
import com.bp.pensionline.publishing.util.CmsPageUtil;
import com.bp.pensionline.publishing.util.DateTimeUtil;
import com.bp.pensionline.util.SystemAccount;

public class EditorLandingHandler extends HttpServlet {

	public static final Log LOG = CmsLog.getLog(EditorLandingHandler.class);
	private static final long serialVersionUID = 1L;	
	public static boolean isSynchronized = false;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

	}
	
	/**
	 * Return the current state in the publishing workflow of the resouce
	 * @param request
	 * @return
	 */
	public static ReleasePageDTO processCurrentPublishingPage(HttpServletRequest request, String pageURI)
	{
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		ReleasePageDTO releasePageDTO =  null;

		if (pageURI != null && currentUser != null && (pageURI.endsWith(".html") || pageURI.endsWith(".jsp")))	// will add more access control here
		{
			try
			{
				String userName = (String)currentUser.getAdditionalInfo(Environment.MEMBER_USERNAME);
				
				CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
				//cmsAdminObj.getRequestContext().setSiteRoot("/");				
				//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
				
				if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP)
						|| cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP)
						|| cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
						|| cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP))
				{
					String fullPathPageURI = "/sites/default" + pageURI;
					LOG.info("Processing page: " + fullPathPageURI);													
					
					// Get the release page if existing in the publshing workflow, null if not
					releasePageDTO = PublishingToolbarSQLHandler.getReleasePageByURI(fullPathPageURI);
					
					if (pageURI.indexOf("/faq/") > -1 && pageURI.endsWith("_faq.jsp"))
					{
						String folderName = pageURI.substring("/faq/".length(), pageURI.indexOf("_faq.jsp"));
						
						// List all struture html in this folder and check if there is any changes
						Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, "/sites/default/faq/" + folderName + "/");
						
						// add the header of the FAQ, consider it as a child
						if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/faq/" + folderName + "_header.html"))
						{
							structureHTMLs.add("/sites/default/faq/" + folderName + "_header.html");
						}
						
						// synchronize the child pages
						Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
						//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());

						if (newUpdatedChilds.size() > 0)
						{												    
							// check if the JSP has been in publishing workflow						
							if (releasePageDTO == null)
							{
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
								
								// insert a new parent page for holder
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(fullPathPageURI);
								releasePageDTO.setMode("Edited");
								releasePageDTO.setStatus("Editing");
								releasePageDTO.setLastUpdate(comparingPageLastModified);	// not necessary
								releasePageDTO.setSelfEdited("N");	
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
								
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
									LOG.info("New updated child URI: " + childPage.getPageURI());
									// set parentId for the new updated child pages and update parent to Editing status
									PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											releasePageDTO.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());								
								}								
							}
							else
							{								
								Vector<String> validPageIds = new Vector<String>();
								boolean updateStatus = false;
								
								LOG.info("Structure Parent PageId: " + releasePageDTO.getPageId());
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
									
									// update parent page to editing if there is one child page "Editing"
									// Use self_edited as flag indicating that child page has been updated more
									if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
									{										
										updateStatus = true;
										PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
										
				    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
				    					
										String childPageURI = childPage.getPageURI();
										String fileName = null;
										if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
										{
											fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
										}
										
										// record actions for this edit, change the display from "Edited" to "Updated"
										String childPageModeDes = childPage.getMode();
										if (childPageModeDes != null && childPageModeDes.equals("Edited"))
										{
											childPageModeDes = "Updated";
										}										
										String actionNotes = buildActionNotes("Elements in page updated!", 
												null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
										PublishingToolbarSQLHandler.insertReleaseAction(
												releasePageDTO.getPageId(), 
												lastChildModifyUserId, 
												"Editing", actionNotes, childPage.getLastUpdate());										
									}
									validPageIds.add(Integer.valueOf(childPage.getPageId()).toString());
								}	
								
								if (updateStatus)
								{
									PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), "Editing");
									releasePageDTO.setStatus("Editing");
								}
								
								// remove all orphan pages
								PublishingToolbarSQLHandler.removeOphanPage(releasePageDTO.getPageId(), validPageIds);								
							}
											
						}
						else
						{
							// delete if JSP page existed in the workflow
							if (releasePageDTO != null)
							{
								PublishingToolbarSQLHandler.removeReleasePage(releasePageDTO.getPageId());
								PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(releasePageDTO.getPageId());
							}												
						}
					}
					else if (pageURI.indexOf("/dictionary/") > -1 && pageURI.endsWith("_dictionary.jsp"))
					{
						String folderName = pageURI.substring("/dictionary/".length(), pageURI.indexOf("_dictionary.jsp"));
						
						// List all struture html in this folder and check if there is any changes
						Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, 
								"/sites/default/dictionary/" + folderName.toUpperCase() + "/");
						
						// add the header of the FAQ, consider it as a child
						if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/dictionary/" + folderName + "_header.html"))
						{
							structureHTMLs.add("/sites/default/dictionary/" + folderName + "_header.html");
						}						
						
						// synchronize the child pages
						Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
						//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());

						if (newUpdatedChilds.size() > 0)
						{												    
							// check if the JSP has been in publishing workflow						
							if (releasePageDTO == null)
							{
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
								
								// insert a new parent page for holder
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(fullPathPageURI);
								releasePageDTO.setMode("Edited");
								releasePageDTO.setStatus("Editing");
								releasePageDTO.setLastUpdate(comparingPageLastModified);	// not necessary
								releasePageDTO.setSelfEdited("N");	
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
								
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
									LOG.info("New updated child URI: " + childPage.getPageURI());
									// set parentId for the new updated child pages and update parent to Editing status
									PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}	
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											releasePageDTO.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());								
								}								
							}
							else
							{								
								Vector<String> validPageIds = new Vector<String>();
								boolean updateStatus = false;
								
								LOG.info("Structure Parent PageId: " + releasePageDTO.getPageId());
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
									
									// update parent page to editing if there is one child page "Editing"
									// Use self_edited as flag indicating that child page has been updated more
									if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
									{										
										updateStatus = true;
										PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
										
				    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
				    					
										String childPageURI = childPage.getPageURI();
										String fileName = null;
										if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
										{
											fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
										}
										
										// record actions for this edit, change the display from "Edited" to "Updated"
										String childPageModeDes = childPage.getMode();
										if (childPageModeDes != null && childPageModeDes.equals("Edited"))
										{
											childPageModeDes = "Updated";
										}
										String actionNotes = buildActionNotes("Elements in page updated!", 
												null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
										PublishingToolbarSQLHandler.insertReleaseAction(
												releasePageDTO.getPageId(), 
												lastChildModifyUserId, 
												"Editing", actionNotes, childPage.getLastUpdate());										
									}
									validPageIds.add(new Integer(childPage.getPageId()).toString());
								}	
								
								if (updateStatus)
								{
									PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), "Editing");
									releasePageDTO.setStatus("Editing");
								}
								
								// remove all orphan pages
								PublishingToolbarSQLHandler.removeOphanPage(releasePageDTO.getPageId(), validPageIds);								
							}
											
						}
						else
						{
							// delete if JSP page existed in the workflow
							if (releasePageDTO != null)
							{
								PublishingToolbarSQLHandler.removeReleasePage(releasePageDTO.getPageId());
								PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(releasePageDTO.getPageId());
							}												
						}
					}	
					else if (pageURI.indexOf("/RSS/") > -1 && pageURI.endsWith("newsChannel.jsp"))
					{
						String folderName = "newsRss";
						
						// List all struture html in this folder and check if there is any changes
						Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, 
								"/sites/default/RSS/" + folderName + "/");
						
						// add the header of the RSS, consider it as a child
						if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/RSS/news_header.html"))
						{
							structureHTMLs.add("/sites/default/RSS/news_header.html");
						}						
						
						// synchronize the child pages
						Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
						//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());

						if (newUpdatedChilds.size() > 0)
						{												    
							// check if the JSP has been in publishing workflow						
							if (releasePageDTO == null)
							{
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
								
								// insert a new parent page for holder
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(fullPathPageURI);
								releasePageDTO.setMode("Edited");
								releasePageDTO.setStatus("Editing");
								releasePageDTO.setLastUpdate(comparingPageLastModified);	// not necessary
								releasePageDTO.setSelfEdited("N");	
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
								
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
									LOG.info("New updated child URI: " + childPage.getPageURI());
									// set parentId for the new updated child pages and update parent to Editing status
									PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}	
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											releasePageDTO.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());								
								}								
							}
							else
							{								
								Vector<String> validPageIds = new Vector<String>();
								boolean updateStatus = false;
								
								LOG.info("Structure Parent PageId: " + releasePageDTO.getPageId());
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
									
									// update parent page to editing if there is one child page "Editing"
									// Use self_edited as flag indicating that child page has been updated more
									if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
									{										
										updateStatus = true;
										PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
										
				    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
				    					
										String childPageURI = childPage.getPageURI();
										String fileName = null;
										if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
										{
											fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
										}
										
										// record actions for this edit, change the display from "Edited" to "Updated"
										String childPageModeDes = childPage.getMode();
										if (childPageModeDes != null && childPageModeDes.equals("Edited"))
										{
											childPageModeDes = "Updated";
										}
										String actionNotes = buildActionNotes("Elements in page updated!", 
												null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
										PublishingToolbarSQLHandler.insertReleaseAction(
												releasePageDTO.getPageId(), 
												lastChildModifyUserId, 
												"Editing", actionNotes, childPage.getLastUpdate());										
									}
									validPageIds.add(new Integer(childPage.getPageId()).toString());
								}	
								
								if (updateStatus)
								{
									PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), "Editing");
									releasePageDTO.setStatus("Editing");
								}
								
								// remove all orphan pages
								PublishingToolbarSQLHandler.removeOphanPage(releasePageDTO.getPageId(), validPageIds);								
							}
											
						}
						else
						{
							// delete if JSP page existed in the workflow
							if (releasePageDTO != null)
							{
								PublishingToolbarSQLHandler.removeReleasePage(releasePageDTO.getPageId());
								PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(releasePageDTO.getPageId());
							}												
						}
					}	
					else if (pageURI.indexOf("/RSS/") > -1 && pageURI.endsWith("eventsChannel.jsp"))
					{
						String folderName = "eventsRss";
						
						// List all struture html in this folder and check if there is any changes
						Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, 
								"/sites/default/RSS/" + folderName + "/");
						
						// add the header of the RSS, consider it as a child
						if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/RSS/events_header.html"))
						{
							structureHTMLs.add("/sites/default/RSS/events_header.html");
						}						
						
						// synchronize the child pages
						Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
						//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());

						if (newUpdatedChilds.size() > 0)
						{												    
							// check if the JSP has been in publishing workflow						
							if (releasePageDTO == null)
							{
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
								
								// insert a new parent page for holder
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(fullPathPageURI);
								releasePageDTO.setMode("Edited");
								releasePageDTO.setStatus("Editing");
								releasePageDTO.setLastUpdate(comparingPageLastModified);	// not necessary
								releasePageDTO.setSelfEdited("N");	
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
								
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
									LOG.info("New updated child URI: " + childPage.getPageURI());
									// set parentId for the new updated child pages and update parent to Editing status
									PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}	
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											releasePageDTO.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());								
								}								
							}
							else
							{								
								Vector<String> validPageIds = new Vector<String>();
								boolean updateStatus = false;
								
								LOG.info("Structure Parent PageId: " + releasePageDTO.getPageId());
								for (int j = 0; j < newUpdatedChilds.size(); j++)
								{
									ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
									
									// update parent page to editing if there is one child page "Editing"
									// Use self_edited as flag indicating that child page has been updated more
									if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
									{										
										updateStatus = true;
										PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPage.getPageId());
										
				    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
				    					
										String childPageURI = childPage.getPageURI();
										String fileName = null;
										if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
										{
											fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
										}
										
										// record actions for this edit, change the display from "Edited" to "Updated"
										String childPageModeDes = childPage.getMode();
										if (childPageModeDes != null && childPageModeDes.equals("Edited"))
										{
											childPageModeDes = "Updated";
										}
										String actionNotes = buildActionNotes("Elements in page updated!", 
												null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
										PublishingToolbarSQLHandler.insertReleaseAction(
												releasePageDTO.getPageId(), 
												lastChildModifyUserId, 
												"Editing", actionNotes, childPage.getLastUpdate());										
									}
									validPageIds.add(new Integer(childPage.getPageId()).toString());
								}	
								
								if (updateStatus)
								{
									PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), "Editing");
									releasePageDTO.setStatus("Editing");
								}
								
								// remove all orphan pages
								PublishingToolbarSQLHandler.removeOphanPage(releasePageDTO.getPageId(), validPageIds);								
							}
											
						}
						else
						{
							// delete if JSP page existed in the workflow
							if (releasePageDTO != null)
							{
								PublishingToolbarSQLHandler.removeReleasePage(releasePageDTO.getPageId());
								PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(releasePageDTO.getPageId());
							}												
						}
					}		
					// end of XML structured contents
					else if (pageURI.endsWith(".html"))
					{
						String auditStatus = "Unknown";
						String auditNotes = "";
						boolean resouceUpdated = false;
						
						int resourceState = CmsPageUtil.getPageState(cmsAdminObj, fullPathPageURI);
						if (resourceState == CmsResource.STATE_CHANGED.getState() || resourceState == CmsResource.STATE_NEW.getState())
						{
							resouceUpdated = true;
						}					
						
						if (resouceUpdated)	// If resource is updated, get information of page
						{
							boolean pageEdited = false;												

							long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, fullPathPageURI);
							String lastModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, fullPathPageURI);
							String lastModifyUserName = SystemAccount.getUserNameFromId(cmsAdminObj, lastModifyUserId);
							if (lastModifyUserName == null)
							{
								lastModifyUserName = "Unknown";
							}							
							
							if (releasePageDTO != null)
							{							
								// Check if page is modified by compare last update stored in bp_release_pages with 
								// pageLastModified
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
								long lastPublishingUpdate = PublishingToolbarSQLHandler.getReleasePageLastUpdate(
										releasePageDTO.getPageId());
								
								releasePageDTO.setMode("Edited");
															
								LOG.info("Page exists in publishing workflow: " + fullPathPageURI);
								if (comparingPageLastModified != lastPublishingUpdate)
								{
									// Page is re-edited. Reverse back to editing regardless of what current state
									// of page is and record action for the change made.
									pageEdited = true;
									
									// update last_update in bp_release_pages
									PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), 
											"Editing", comparingPageLastModified);
									
									releasePageDTO.setStatus("Editing");
									releasePageDTO.setLastUpdate(comparingPageLastModified);
									releasePageDTO.setSelfEdited("Y");
							
									// Create a defaut notes for this Edit action
									SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
									String atTimeStr = dateFormat.format(new Date(pageLastModified));
									String actionNotes = buildActionNotes("Page edited", lastModifyUserName, atTimeStr, "");
									PublishingToolbarSQLHandler.insertReleaseAction(
											releasePageDTO.getPageId(), 
											lastModifyUserId, 
											"Editing", actionNotes, comparingPageLastModified);
									
									auditStatus = "Editing";
									auditNotes = "Page modified";
									// Audit the page edited action
									PublishingToolbarSQLHandler.doAudit(
											comparingPageLastModified,
											fullPathPageURI,
											lastModifyUserId, 
											auditStatus,
											auditNotes);	
								}							
								
							}
							else  // this page is new edited or created. Start the workflow for it.
							{
								pageEdited = true;
								
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
								
								LOG.info("Page not exists in publishing workflow: " + fullPathPageURI);
								String newPageStatus = "Editing"; // start the publishing workflow with a new page
								
								// Create a defaut notes for this Edit action
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
								String atTimeStr = dateFormat.format(new Date(pageLastModified));							
								
								// insert a publishing flow record
								String changeMode = "Edited";
								String actionPhrase = "Page edited";
								auditStatus = "Editing";
								auditNotes = "Page modified";
								
								if (resourceState ==  CmsResource.STATE_NEW.getState())
								{
									changeMode = "New";
									auditStatus = "Created";
									auditNotes = "New page created";
									actionPhrase = "Page created";
								}
//								else if (resourceState ==  CmsResource.STATE_DELETED)	// will be handle seperately
//								{
//									changeMode = "Deleted";
//									auditStatus = "Deleted";
//									auditNotes = "Page deleted";
//								}
								
								String actionNotes = buildActionNotes(actionPhrase, lastModifyUserName, atTimeStr, "");
								
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(fullPathPageURI);
								releasePageDTO.setMode(changeMode);
								releasePageDTO.setStatus(newPageStatus);
								releasePageDTO.setLastUpdate(comparingPageLastModified);
								releasePageDTO.setSelfEdited("Y");
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
								
								// insert a very first action record
								PublishingToolbarSQLHandler.insertReleaseAction(
										releasePageDTO.getPageId(), 
										lastModifyUserId, 
										newPageStatus, actionNotes, comparingPageLastModified);	
								
								// Audit the page edited action
								PublishingToolbarSQLHandler.doAudit(
										comparingPageLastModified,
										fullPathPageURI,
										lastModifyUserId, 
										auditStatus,
										auditNotes);							
							}
							
							// Process page status if page is edited
							if (pageEdited)
							{
								// Check if the editing is an adding a link to a new child page
								Vector<ReleasePageDTO> allChildPages = PublishingToolbarSQLHandler.getAllChildPages();
								
								if (allChildPages != null && allChildPages.size() > 0)
								{
									// Get all embeded modified files in this page
									Vector<ReleasePageDTO> newEmbededChangedPages = CmsPageUtil.getEmbededChildPagesChanged(cmsAdminObj, 
											fullPathPageURI, allChildPages);	
									
									// Update parent for these child pages
									for (int i = 0; i < newEmbededChangedPages.size(); i++)
									{									
										ReleasePageDTO newEmbededChangedPage = (ReleasePageDTO)newEmbededChangedPages.get(i);
										LOG.info("Added new link to updated attachment: " + newEmbededChangedPage.getPageURI());
										PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), newEmbededChangedPage.getPageId());
									}
								}																
							}
						}
					}

				}
			}
			catch (CmsException cmse)
			{
				LOG.error("Error while listing all editing files: " + cmse.toString());
			}
		}
		
		return releasePageDTO;
	}
	
	/**
	 * Get the action histories maded to a page
	 * @param pageId
	 * @return
	 */
	public static String getReleasePageActionHistories (int pageId)
	{
		return PublishingToolbarSQLHandler.buildActionHistories(pageId);
	}
	
	/**
	 * Get the action histories maded to a page
	 * @param pageId
	 * @return
	 */
	public static String getOrphanPageActionHistories (int pageId)
	{
		return PublishingToolbarSQLHandler.buildOrphanActionHistories(pageId);
	}

	/**
	 * Get the userId of last publishing action: Send4Review, Check, Approve
	 * @param pageId
	 * @return
	 */	
	public static boolean checkPublishingActionValid (int pageId, String action, String currentUserId)
	{
		if (currentUserId != null && action != null)
		{
			String lastUserId = null;
			if (action.equals(Constant.PUBLISHING_ACTION_SEND4REVIEW))
			{
				lastUserId = PublishingToolbarSQLHandler.getLastPublishingActionUserId(pageId, "Edited");
			}
			else if (action.equals(Constant.PUBLISHING_ACTION_PASSCHECKING))
			{
				lastUserId = PublishingToolbarSQLHandler.getLastPublishingActionUserId(pageId, "Checked");
			}
			
			// Dont care about other action. Approve action is the last action so it is not necessary to check
			
			if (currentUserId.equals(lastUserId))
			{
				return false;
			}
		}
		
		return true;
	}	
	
	public static void synchronizeResources (HttpServletRequest request) {
		LOG.info("synchronizeResources():BEGIN");
		try {
			//isSynchronized = true;
			
			CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
			//cmsAdminObj.getRequestContext().setSiteRoot("/");				
			//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			//Delete redundancy first
			cleanUnpublishingPages(cmsAdminObj);
			
			Vector<ReleasePageDTO> galEditingPages = CmsPageUtil.listAllChangedFiles(cmsAdminObj, "/system/galleries/");
			Vector<ReleasePageDTO> childChangedPages = synchronizeGalleriesFolder(galEditingPages);
			
			synchronizeEditingParentPages(childChangedPages);
			// these pages are not likely to be changed in a short time. Save it to the session for later used
			//request.getSession().setAttribute("GalleryPagesChanged", childChangedPages);
			
			// synchronize strutured content such as FAQ and Dictionary
			synchronizeFAQFolder();
			synchronizeDicFolder();
			synchronizeRSSFolder();
			
//			SynchronizedUtilThread sync = new SynchronizedUtilThread();
//			sync.start();
		} catch (CmsException cmse) {
			LOG.error("Error in synchronizeGalleryResources: " + cmse.toString());
		}
		LOG.info("synchronizeResources():END");
	}	
	
	/**
	 * Synchronize editing child pages between CMS tables and publishing tables. 
	 * Used for images and PDF, DOC in galleries
	 */
	public static Vector<ReleasePageDTO> synchronizeGalleriesFolder (Vector cmsEditingPageDTOs)
	{				
		LOG.info("synchronizeGalleriesFolder():BEGIN");
		System.out.println("synchronizeGalleriesFolder():BEGIN");
		Vector<ReleasePageDTO> changedPages = new Vector<ReleasePageDTO>();
		
		if (cmsEditingPageDTOs != null)	// will add more access control here
		{
			for (int i = 0; i < cmsEditingPageDTOs.size(); i++)
			{
				// check if there is a file in the current release existed in bp_release_pages
				ReleasePageDTO cmsPageDTO = (ReleasePageDTO)cmsEditingPageDTOs.elementAt(i);
				ReleasePageDTO childPageDTO = PublishingToolbarSQLHandler.getReleaseChildPageByURI(cmsPageDTO.getPageURI());

				if (childPageDTO == null)	// new content change, add it to the publishing workflow
				{
					// Check if change is Edited or New mode
					String mode = cmsPageDTO.getMode();
					String pageURI = cmsPageDTO.getPageURI();
					
					childPageDTO = new ReleasePageDTO();
					childPageDTO.setPageURI(pageURI);
					childPageDTO.setMode(mode);
					childPageDTO.setLastUpdate(cmsPageDTO.getLastUpdate());
					
					int pageId = PublishingToolbarSQLHandler.insertReleaseChildPage(childPageDTO);
					childPageDTO.setPageId(pageId);
					
					String auditStatus = "Unknown";
					String auditNotes = "";
					if (mode != null && mode.equals("Edited"))
					{
						auditStatus = "Editing";
						auditNotes = "Resource modified";
					}
					else if (mode != null && mode.equals("New"))
					{
						auditStatus = "Created";
						auditNotes = "New resource created";
					}
					/*
					 * Ignore deleting in galleries in this implementation
					 */					
					else if (mode != null && mode.equals("Deleted"))
					{
						auditStatus = "Deleted";
						auditNotes = "Resource deleted";
					}					
					// Audit the page modified action based on mode
					PublishingToolbarSQLHandler.doAudit(
							cmsPageDTO.getLastUpdate(),
							pageURI,
							cmsPageDTO.getLastModifiedBy(), 
							auditStatus,
							auditNotes);					
				}
				else
				{
					// update page's parent pages status and last modify in if page is edited from last record
					System.out.println("Last updates: "+childPageDTO.getLastUpdate()+"(DB)-"+cmsPageDTO.getLastUpdate()+"(CMS)");
					System.out.println("Modes: "+childPageDTO.getMode()+"(DB)-"+cmsPageDTO.getMode()+"(CMS)");
					if (cmsPageDTO.getLastUpdate() != childPageDTO.getLastUpdate()||
						!cmsPageDTO.getMode().trim().equalsIgnoreCase(childPageDTO.getMode().trim()))
					{
						System.out.println("File "+cmsPageDTO.getPageURI()+" has been changed");
						// Get all parent pages of this child pages
						Vector<Integer> parentPageIds = PublishingToolbarSQLHandler.getParentPagesOfChild(childPageDTO.getPageId());
						
						String childPageURI = childPageDTO.getPageURI();
						
						String fileName = null;
						if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
						{
							fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
						}
						
						// Update parent pages' status to Edited
						for (int j = 0; j < parentPageIds.size(); j++)
						{
							int parentId = parentPageIds.get(j).intValue();
							
							if (parentId > -1)
							{
								PublishingToolbarSQLHandler.updateReleasePageStatus(parentId, "Edited");
																
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPageDTO.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Attachment in page updated.", 
										null, null, fileName + "(" + childPageModeDes + ")");
								
								PublishingToolbarSQLHandler.insertReleaseAction(
										parentId, 
										cmsPageDTO.getLastModifiedBy(), 
										"Editing", actionNotes, DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()));														
							}														
						}
						
						// update lastUpdate field of child page to the new value
						PublishingToolbarSQLHandler.updateChildPageLastUpdate (childPageDTO.getPageId(), cmsPageDTO.getLastUpdate(), cmsPageDTO.getMode(), "Editing");
					}					
				}
				
				changedPages.add(childPageDTO);
			}
			
			// clean up unused pages
			PublishingToolbarSQLHandler.cleanReleaseChildPages(changedPages, "/system/galleries/");
		}
		LOG.info("Changed size in Galleries: "+changedPages.size());
		System.out.println("synchronizeGalleriesFolder():END");
		LOG.info("synchronizeGalleriesFolder():END");
		return changedPages;
	}
	
	/**
	 * Pass through all HTML file in sites/default and update DB if it contains child changed pages
	 * @param childChangedPages
	 */
	public static void synchronizeEditingParentPages (Vector<ReleasePageDTO> childChangedPages)
	{
		LOG.info("synchronizeEditingParentPages():BEGIN");
		try
		{
			Vector copyChildChangedPages = null;
			if (childChangedPages != null)
			{
				copyChildChangedPages = (Vector)childChangedPages.clone();
			}
			
			CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
			//cmsAdminObj.getRequestContext().setSiteRoot("/");				
			//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			// exclude FAQ, RSS,  
			String[] excludeFiles = new String [] {
					"(/sites/default/faq/)([\\w]*)(_header.html)",
					"(/sites/default/dictionary/)([\\w]*)(_header.html)",
					"(/sites/default/RSS/)([\\w]*)(_header.html)"};
			Vector<String> htmlFiles = CmsPageUtil.listAllTemplateHtmlFiles(cmsAdminObj, "/sites/default/", excludeFiles);
			
			// For each html file, check if file is changed. If Yes, insert or update to bp_release_pages
			for (int i = 0; i < htmlFiles.size(); i++)
			{
				boolean resouceUpdated = false;
				boolean resourceDeleted = false;
				String htmlURI = htmlFiles.elementAt(i);
				String auditStatus = "Unknown";
				String auditNotes = "";
				
				ReleasePageDTO releasePageDTO =  null;				
				
//				if (embededChangedPages != null)
//				{
//					LOG.info("" + htmlURI + " all changes left: " + copyChildChangedPages.size());
//					LOG.info("" + htmlURI + " embeded changes: " + embededChangedPages.size());
//				}				
				int resourceState = CmsPageUtil.getPageState(cmsAdminObj, htmlURI);
				if (resourceState == CmsResource.STATE_CHANGED.getState() || resourceState == CmsResource.STATE_NEW.getState())
				{
					resouceUpdated = true;
				}
				else if (resourceState == CmsResource.STATE_DELETED.getState())
				{
					resourceDeleted = true;
				}
				
				//LOG.info("" + htmlURI + " has tyep: " + resourceState);
				
				if (!resourceDeleted)
				{
					// Get all embeded modified files in this page
					Vector<ReleasePageDTO> embededChangedPages = CmsPageUtil.getEmbededChildPagesChanged(cmsAdminObj, 
							htmlURI, copyChildChangedPages);
					
					// Remove the embeded changes from the whole list
					//copyChildChangedPages.removeAll(embededChangedPages);
					
					if (resouceUpdated)	// If resource is updated, get information of page
					{
						releasePageDTO = PublishingToolbarSQLHandler.getReleasePageByURI(htmlURI);

						long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, htmlURI);
						String lastModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, htmlURI);
						String lastModifyUserName = SystemAccount.getUserNameFromId(cmsAdminObj, lastModifyUserId);
						if (lastModifyUserName == null)
						{
							lastModifyUserName = "Unknown";
						}
						
						if (releasePageDTO != null)
						{
							
							// Check if page is modified by compare last update stored in bp_release_pages with 
							// pageLastModified
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
							long lastPublishingUpdate = PublishingToolbarSQLHandler.getReleasePageLastUpdate(
									releasePageDTO.getPageId());
							releasePageDTO.setMode("Edited");
														
							LOG.info("Page exists in publishing workflow: " + htmlURI);
							if (comparingPageLastModified != lastPublishingUpdate)
							{
								// Page is re-edited. Reverse back to editing regardless of what current state
								// of page is and record action for the change made.
								
								// update last_update in bp_release_pages
								PublishingToolbarSQLHandler.updateReleasePageStatus(releasePageDTO.getPageId(), 
										"Editing", comparingPageLastModified);
								
								releasePageDTO.setStatus("Editing");
								releasePageDTO.setLastUpdate(comparingPageLastModified);
						
								// Create a defaut notes for this Edit action
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
								String atTimeStr = dateFormat.format(new Date(pageLastModified));
								String actionNotes = buildActionNotes("Page edited", lastModifyUserName, atTimeStr, "");
								PublishingToolbarSQLHandler.insertReleaseAction(
										releasePageDTO.getPageId(), 
										lastModifyUserId, 
										"Editing", actionNotes, comparingPageLastModified);
								
								auditStatus = "Editing";
								auditNotes = "Page modified";
								// Audit the page edited action
								PublishingToolbarSQLHandler.doAudit(
										comparingPageLastModified,
										htmlURI,
										lastModifyUserId, 
										auditStatus,
										auditNotes);	
							}							
							
						}
						else  // this page is new edited or created. Start the workflow for it.
						{
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
							
							LOG.info("Page not exists in publishing workflow: " + htmlURI);
							String newPageStatus = "Editing"; // start the publishing workflow with a new page
							
							// Create a defaut notes for this Edit action
							SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
							String atTimeStr = dateFormat.format(new Date(pageLastModified));							
							
							// insert a publishing flow record
							String changeMode = "Edited";
							String actionPhrase = "Page edited";
							auditStatus = "Editing";
							auditNotes = "Page modified";
							
							if (resourceState ==  CmsResource.STATE_NEW.getState())
							{
								changeMode = "New";
								auditStatus = "Created";
								auditNotes = "New page created";
								actionPhrase = "Page created";
							}
//							else if (resourceState ==  CmsResource.STATE_DELETED)	// will be handle seperately
//							{
//								changeMode = "Deleted";
//								auditStatus = "Deleted";
//								auditNotes = "Page deleted";
//							}
							
							String actionNotes = buildActionNotes(actionPhrase, lastModifyUserName, atTimeStr, "");
							
							releasePageDTO = new ReleasePageDTO();
							releasePageDTO.setPageURI(htmlURI);
							releasePageDTO.setMode(changeMode);
							releasePageDTO.setStatus(newPageStatus);
							releasePageDTO.setLastUpdate(comparingPageLastModified);
							releasePageDTO.setSelfEdited("Y");
							
							releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));
							
							// insert a very first action record
							PublishingToolbarSQLHandler.insertReleaseAction(
									releasePageDTO.getPageId(), 
									lastModifyUserId, 
									newPageStatus, actionNotes, comparingPageLastModified);	
							
							// Audit the page edited action
							PublishingToolbarSQLHandler.doAudit(
									comparingPageLastModified,
									htmlURI,
									lastModifyUserId, 
									auditStatus,
									auditNotes);							
						}		
					}
					else
					{
						// Page is not updated, but check if it embeded files in this page was changed.
						// If Yes, consisder this page as attachment holder for the publishing							
						if (embededChangedPages != null && embededChangedPages.size() > 0)
						{					
							releasePageDTO = PublishingToolbarSQLHandler.getReleasePageByURI(htmlURI);
								
							if (releasePageDTO == null)	// page does not exist in the workflow
							{											
								LOG.info("Create new release page for this impacted page: ");
//								 Insert a new release page with self_edit = false then set the parentId of child pages
								// with new page's id. Record edit action as well.
								long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, htmlURI);
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
								
								releasePageDTO = new ReleasePageDTO();
								releasePageDTO.setPageURI(htmlURI);
								releasePageDTO.setMode("Impacted");
								releasePageDTO.setStatus("Edited");	// available for Reviewer
								releasePageDTO.setLastUpdate(comparingPageLastModified);
								releasePageDTO.setSelfEdited("N");
								
								releasePageDTO.setPageId(PublishingToolbarSQLHandler.insertReleasePage(releasePageDTO));								
								
							}							
						}
					}
					
					// synchronize parent-children relationships for this page and it's attachments
					if (releasePageDTO != null && releasePageDTO.getPageId() != -1 && 						
							embededChangedPages != null && embededChangedPages.size() > 0)
					{						
						// update parentId and get the embeded file names to build the action notes
						for (int j = 0; j < embededChangedPages.size(); j++)
						{
		    				ReleasePageDTO childPageDTO = (ReleasePageDTO)embededChangedPages.elementAt(j);
		    				int childPageId = childPageDTO.getPageId();
		    				
		    				// add new child-parent relationship
		    				boolean updateOk = PublishingToolbarSQLHandler.addChildToPage(releasePageDTO.getPageId(), childPageId);
		    				
							// record an action for an edit attachment
		    				if (updateOk)
							{
		    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPageDTO.getPageURI());
		    					
								String childPageURI = childPageDTO.getPageURI();
								String fileName = null;
								if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
								{
									fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
								}	
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPageDTO.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Attachment in page updated.", 
										null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
								PublishingToolbarSQLHandler.insertReleaseAction(
										releasePageDTO.getPageId(), 
										lastChildModifyUserId, 
										releasePageDTO.getStatus(), actionNotes, 
										DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()));
							}		    				
						}
					}					
				}	// end of if (!resourceDeleted)
				else
				{
					// Handle deleting request in a different manor
					ReleasePageDTO deletedPage = PublishingToolbarSQLHandler.getReleasePageByURI(htmlURI);
					LOG.info("DELETE: " + deletedPage);
					boolean insertNew = true;
					if (deletedPage != null)	// page existed in the workflow
					{
						LOG.info("DELETE Mode: " + deletedPage.getMode());
						if (deletedPage.getMode() != null && deletedPage.getMode().equals("Deleted")) // Ignore
						{
							LOG.info("DELETE: Page already deleted and in the publishing workflow --> Ignore");
							insertNew = false;
						}
						else
						{
							// update mode to Deleted and status to Editing (start a new request)
							// Delete the old one including parent and children
							PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(deletedPage.getPageId());
							// Delete actions related to this page
							PublishingToolbarSQLHandler.removeReleaseActions(deletedPage.getPageId());
							PublishingToolbarSQLHandler.removeReleasePage(deletedPage.getPageId());
							LOG.info("DELETE: Clean information related to the old page");
						}
					}
					
					if (insertNew)
					{
						// Insert a new one										
						long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, htmlURI);
						String lastModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, htmlURI);
						String lastModifyUserName = SystemAccount.getUserNameFromId(cmsAdminObj, lastModifyUserId);
						LOG.info("Insert new delete page");
						if (lastModifyUserName == null)
						{
							lastModifyUserName = "Unknown";
						}						
						
						deletedPage = new ReleasePageDTO();
						deletedPage.setPageURI(htmlURI);
						deletedPage.setMode("Deleted");
						deletedPage.setStatus("Editing");
						deletedPage.setSelfEdited("Y");
						deletedPage.setLastUpdate(DateTimeUtil.formatToOrderingTime(pageLastModified));
						
						deletedPage.setPageId(PublishingToolbarSQLHandler.insertReleasePage(deletedPage));
						
						// Create a defaut notes for this Edit action
						LOG.info("Create action notes and audit the action");
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
						String atTimeStr = dateFormat.format(new Date(pageLastModified));						
						String actionNotes = buildActionNotes("Page deleted", lastModifyUserName, atTimeStr, "");
						// insert a very first action record
						PublishingToolbarSQLHandler.insertReleaseAction(
								deletedPage.getPageId(), 
								lastModifyUserId, 
								"Editing", actionNotes, DateTimeUtil.formatToOrderingTime(pageLastModified));	
						
						// Audit the page edited action
						PublishingToolbarSQLHandler.doAudit(
								DateTimeUtil.formatToOrderingTime(pageLastModified),
								htmlURI,
								lastModifyUserId, 
								"Deleted",
								"Page deleted");
					}
				}
				
			}
			
			//cleanUnpublishingPages(cmsAdminObj);
			LOG.info("synchronizeEditingParentPages():END");
		}
		catch (Exception cmse)
		{
			LOG.error("Error in synchronizeEditingParentPages: " + cmse.toString());
		}
	}
	
	/**
	 * FAQ folder contain struture content and need to handle in a different way
	 *
	 */
	public static void synchronizeFAQFolder ()
	{
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
			//cmsAdminObj.getRequestContext().setSiteRoot("/");				
			//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			Vector<String> jspFiles = CmsPageUtil.listAllJSPFiles(cmsAdminObj, "/sites/default/faq/", null);
			
			// For each jsp file, check if the struture contents it includes has been changed
			for (int i = 0; i < jspFiles.size(); i++)
			{
				String jspFullPath = jspFiles.elementAt(i);
				String jspFilename = jspFullPath.substring("/sites/default/faq/".length());
				//LOG.info("FAQ JSP file: " + jspFullPath);
				// Get the correspond folder name
				if (jspFilename.endsWith("_faq.jsp"))
				{
					String folderName = jspFilename.substring(0, jspFilename.indexOf("_faq.jsp"));
					
					// List all struture html in this folder and check if there is any changes
					Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, "/sites/default/faq/" + folderName + "/");
					
					// add the header of the FAQ, consider it as a child
					if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/faq/" + folderName + "_header.html"))
					{
						structureHTMLs.add("/sites/default/faq/" + folderName + "_header.html");
					}
					
					// synchronize the child pages
					Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
					//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());
					
					ReleasePageDTO jspPage = PublishingToolbarSQLHandler.getReleasePageByURI(jspFullPath);
					
					if (newUpdatedChilds.size() > 0)
					{												    
						// check if the JSP has been in publishing workflow						
						if (jspPage == null)
						{
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
							
							// insert a new parent page for holder
							jspPage = new ReleasePageDTO();
							jspPage.setPageURI(jspFullPath);
							jspPage.setMode("Edited");
							jspPage.setStatus("Editing");
							jspPage.setLastUpdate(comparingPageLastModified);	// not necessary
							jspPage.setSelfEdited("N");	
							
							jspPage.setPageId(PublishingToolbarSQLHandler.insertReleasePage(jspPage));
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
								LOG.info("FAQ: New updated child URI: " + childPage.getPageURI());
								// set parentId for the new updated child pages and update parent to Editing status
								PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
								
		    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
		    					
								String childPageURI = childPage.getPageURI();
								String fileName = null;
								if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
								{
									fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
								}
								
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPage.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Elements in page updated!", 
										null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
								PublishingToolbarSQLHandler.insertReleaseAction(
										jspPage.getPageId(), 
										lastChildModifyUserId, 
										"Editing", actionNotes, childPage.getLastUpdate());								
							}								
						}
						else
						{
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							Vector<String> validPageIds = new Vector<String>();
							boolean updateStatus = false;
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
								
								// update parent page to editing if there is one child page "Editing"
								// Use self_edited as flag indicating that child page has been updated more
								if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
								{
									updateStatus = true;
									PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null,  fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											jspPage.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());										
								}
								validPageIds.add(Integer.valueOf(childPage.getPageId()).toString());
							}	
							
							if (updateStatus)
							{
								PublishingToolbarSQLHandler.updateReleasePageStatus(jspPage.getPageId(), "Editing");
							}
							
							// remove all orphan pages
							PublishingToolbarSQLHandler.removeOphanPage(jspPage.getPageId(), validPageIds);								
						}
						
			
					}
					else
					{
						// delete if JSP page existed in the workflow
						if (jspPage != null)
						{
							PublishingToolbarSQLHandler.removeReleasePage(jspPage.getPageId());
							PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(jspPage.getPageId());
						}												
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.info("General error while synchronizeFAQFolder: " + e.toString());
		}		
	}
	
	/**
	 * dictionary folder contain struture content and need to handle in a different way
	 *
	 */
	public static void synchronizeDicFolder ()
	{
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
			//cmsAdminObj.getRequestContext().setSiteRoot("/");				
			//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			Vector<String> jspFiles = CmsPageUtil.listAllJSPFiles(cmsAdminObj, "/sites/default/dictionary/", null);
			
			// For each jsp file, check if the struture contents it includes has been changed
			for (int i = 0; i < jspFiles.size(); i++)
			{
				String jspFullPath = jspFiles.elementAt(i);
				String jspFilename = jspFullPath.substring("/sites/default/dictionary/".length());
				//LOG.info("Dictionary JSP file: " + jspFullPath);
				// Get the correspond folder name
				if (jspFilename.endsWith("_dictionary.jsp"))
				{
					String folderName = jspFilename.substring(0, jspFilename.indexOf("_dictionary.jsp"));
					
					// List all struture html in this folder and check if there is any changes
					Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, "/sites/default/dictionary/" + folderName.toUpperCase() + "/");
					
					// add the header of the dictionary, consider it as a child
					if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/dictionary/" + folderName + "_header.html"))
					{
						structureHTMLs.add("/sites/default/dictionary/" + folderName + "_header.html");
					}					
					
					// synchronize the child pages
					Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
					//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());
					
					ReleasePageDTO jspPage = PublishingToolbarSQLHandler.getReleasePageByURI(jspFullPath);
					
					if (newUpdatedChilds.size() > 0)
					{												    
						// check if the JSP has been in publishing workflow						
						if (jspPage == null)
						{
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
							
							// insert a new parent page for holder
							jspPage = new ReleasePageDTO();
							jspPage.setPageURI(jspFullPath);
							jspPage.setMode("Edited");
							jspPage.setStatus("Editing");
							jspPage.setLastUpdate(comparingPageLastModified);	// not necessary
							jspPage.setSelfEdited("N");	
							
							jspPage.setPageId(PublishingToolbarSQLHandler.insertReleasePage(jspPage));
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
								LOG.info("Dic: New updated child URI: " + childPage.getPageURI());
								// set parentId for the new updated child pages and update parent to Editing status
								PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
								
		    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
		    					
								String childPageURI = childPage.getPageURI();
								String fileName = null;
								if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
								{
									fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
								}
								
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPage.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Elements in page updated!", 
										null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
								PublishingToolbarSQLHandler.insertReleaseAction(
										jspPage.getPageId(), 
										lastChildModifyUserId, 
										"Editing", actionNotes, childPage.getLastUpdate());								
							}								
						}
						else
						{
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							Vector<String> validPageIds = new Vector<String>();
							boolean updateStatus = false;
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
								
								// update parent page to editing if there is one child page "Editing"
								// Use self_edited as flag indicating that child page has been updated more
								if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
								{
									updateStatus = true;
									PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											jspPage.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());										
								}
								validPageIds.add(Integer.valueOf(childPage.getPageId()).toString());
							}	
							
							if (updateStatus)
							{
								PublishingToolbarSQLHandler.updateReleasePageStatus(jspPage.getPageId(), "Editing");
							}
							
							// remove all orphan pages
							PublishingToolbarSQLHandler.removeOphanPage(jspPage.getPageId(), validPageIds);								
						}
						
			
					}
					else
					{
						// delete if JSP page existed in the workflow
						if (jspPage != null)
						{
							PublishingToolbarSQLHandler.removeReleasePage(jspPage.getPageId());
							PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(jspPage.getPageId());
						}												
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.info("General error while synchronizeDicFolder: " + e.toString());
		}		
	}	
	
	/**
	 * dictionary folder contain struture content and need to handle in a different way
	 *
	 */
	public static void synchronizeRSSFolder ()
	{
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
			//cmsAdminObj.getRequestContext().setSiteRoot("/");				
			//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			Vector<String> jspFiles = CmsPageUtil.listAllJSPFiles(cmsAdminObj, "/sites/default/RSS/", null);
			
			// For each jsp file, check if the struture contents it includes has been changed
			for (int i = 0; i < jspFiles.size(); i++)
			{
				String jspFullPath = jspFiles.elementAt(i);
				String jspFilename = jspFullPath.substring("/sites/default/RSS/".length());
				//LOG.info("RSS JSP file: " + jspFullPath);
				// Get the correspond folder name
				if (jspFilename.equals("newsChannel.jsp"))
				{
					// List all struture html in this folder and check if there is any changes
					Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, "/sites/default/RSS/newsRss/");
					
					// add the header of the RSS, consider it as a child
					if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/RSS/news_header.html"))
					{
						structureHTMLs.add("/sites/default/RSS/news_header.html");
					}
					
					// synchronize the child pages
					Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
					LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());
					
					ReleasePageDTO jspPage = PublishingToolbarSQLHandler.getReleasePageByURI(jspFullPath);
					
					if (newUpdatedChilds.size() > 0)
					{												    
						// check if the JSP has been in publishing workflow						
						if (jspPage == null)
						{
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
							
							// insert a new parent page for holder
							jspPage = new ReleasePageDTO();
							jspPage.setPageURI(jspFullPath);
							jspPage.setMode("Edited");
							jspPage.setStatus("Editing");
							jspPage.setLastUpdate(comparingPageLastModified);	// not necessary
							jspPage.setSelfEdited("N");	
							
							jspPage.setPageId(PublishingToolbarSQLHandler.insertReleasePage(jspPage));
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
								LOG.info("RSS: New updated child URI: " + childPage.getPageURI());
								// set parentId for the new updated child pages and update parent to Editing status
								PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
								
		    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
		    					
								String childPageURI = childPage.getPageURI();
								String fileName = null;
								if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
								{
									fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
								}
								
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPage.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Elements in page updated!", 
										null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
								PublishingToolbarSQLHandler.insertReleaseAction(
										jspPage.getPageId(), 
										lastChildModifyUserId, 
										"Editing", actionNotes, childPage.getLastUpdate());								
							}								
						}
						else
						{
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							Vector<String> validPageIds = new Vector<String>();
							boolean updateStatus = false;
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
								
								// update parent page to editing if there is one child page "Editing"
								// Use self_edited as flag indicating that child page has been updated more
								if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
								{
									updateStatus = true;
									PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											jspPage.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());										
								}
								validPageIds.add(Integer.valueOf(childPage.getPageId()).toString());
							}	
							
							if (updateStatus)
							{
								PublishingToolbarSQLHandler.updateReleasePageStatus(jspPage.getPageId(), "Editing");
							}
							
							// remove all orphan pages
							PublishingToolbarSQLHandler.removeOphanPage(jspPage.getPageId(), validPageIds);								
						}
						
			
					}
					else
					{
						// delete if JSP page existed in the workflow
						if (jspPage != null)
						{
							PublishingToolbarSQLHandler.removeReleasePage(jspPage.getPageId());
							PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(jspPage.getPageId());
						}												
					}
				}
				else if (jspFilename.equals("eventsChannel.jsp"))
				{
					// List all struture html in this folder and check if there is any changes
					Vector<String> structureHTMLs = CmsPageUtil.listAllStrutureHtmlFiles(cmsAdminObj, "/sites/default/RSS/eventsRss/");
					
					// add the header of the RSS, consider it as a child
					if (CmsPageUtil.isPageExisted(cmsAdminObj, "/sites/default/RSS/events_header.html"))
					{
						structureHTMLs.add("/sites/default/RSS/events_header.html");
					}
					
					// synchronize the child pages
					Vector<ReleasePageDTO> newUpdatedChilds = synchronizeStrutureContent(cmsAdminObj, -1, structureHTMLs);
					//LOG.info("newUpdatedChilds: " + newUpdatedChilds.size());
					
					ReleasePageDTO jspPage = PublishingToolbarSQLHandler.getReleasePageByURI(jspFullPath);
					
					if (newUpdatedChilds.size() > 0)
					{												    
						// check if the JSP has been in publishing workflow						
						if (jspPage == null)
						{
							long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
							
							// insert a new parent page for holder
							jspPage = new ReleasePageDTO();
							jspPage.setPageURI(jspFullPath);
							jspPage.setMode("Edited");
							jspPage.setStatus("Editing");
							jspPage.setLastUpdate(comparingPageLastModified);	// not necessary
							jspPage.setSelfEdited("N");	
							
							jspPage.setPageId(PublishingToolbarSQLHandler.insertReleasePage(jspPage));
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);
								LOG.info("RSS: New updated child URI: " + childPage.getPageURI());
								// set parentId for the new updated child pages and update parent to Editing status
								PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
								
		    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
		    					
								String childPageURI = childPage.getPageURI();
								String fileName = null;
								if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
								{
									fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
								}
								
								// record actions for this edit, change the display from "Edited" to "Updated"
								String childPageModeDes = childPage.getMode();
								if (childPageModeDes != null && childPageModeDes.equals("Edited"))
								{
									childPageModeDes = "Updated";
								}
								String actionNotes = buildActionNotes("Elements in page updated!", 
										null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
								PublishingToolbarSQLHandler.insertReleaseAction(
										jspPage.getPageId(), 
										lastChildModifyUserId, 
										"Editing", actionNotes, childPage.getLastUpdate());								
							}								
						}
						else
						{
							//LOG.info("Structure Parent PageId: " + jspPage.getPageId());
							Vector<String> validPageIds = new Vector<String>();
							boolean updateStatus = false;
							
							for (int j = 0; j < newUpdatedChilds.size(); j++)
							{
								ReleasePageDTO childPage = newUpdatedChilds.elementAt(j);								
								
								// update parent page to editing if there is one child page "Editing"
								// Use self_edited as flag indicating that child page has been updated more
								if (childPage.getSelfEdited() != null && childPage.getSelfEdited().equals("Y"))
								{
									updateStatus = true;
									PublishingToolbarSQLHandler.addChildToPage(jspPage.getPageId(), childPage.getPageId());
									
			    					String lastChildModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, childPage.getPageURI());
			    					
									String childPageURI = childPage.getPageURI();
									String fileName = null;
									if (childPageURI != null && childPageURI.lastIndexOf('/') >= 0)
									{
										fileName = childPageURI.substring(childPageURI.lastIndexOf('/') + 1);
									}
									
									// record actions for this edit, change the display from "Edited" to "Updated"
									String childPageModeDes = childPage.getMode();
									if (childPageModeDes != null && childPageModeDes.equals("Edited"))
									{
										childPageModeDes = "Updated";
									}
									String actionNotes = buildActionNotes("Elements in page updated!", 
											null, null, fileName + "(" + childPageModeDes + ")");			// Ignore last user name modify the child page						
									PublishingToolbarSQLHandler.insertReleaseAction(
											jspPage.getPageId(), 
											lastChildModifyUserId, 
											"Editing", actionNotes, childPage.getLastUpdate());										
								}
								validPageIds.add(Integer.valueOf(childPage.getPageId()).toString());
							}	
							
							if (updateStatus)
							{
								PublishingToolbarSQLHandler.updateReleasePageStatus(jspPage.getPageId(), "Editing");
							}
							
							// remove all orphan pages
							PublishingToolbarSQLHandler.removeOphanPage(jspPage.getPageId(), validPageIds);								
						}
						
			
					}
					else
					{
						// delete if JSP page existed in the workflow
						if (jspPage != null)
						{
							PublishingToolbarSQLHandler.removeReleasePage(jspPage.getPageId());
							PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(jspPage.getPageId());
						}												
					}
				}				
			}
		}
		catch (Exception e)
		{
			LOG.info("General error while synchronizeRSSFolder: " + e.toString());
		}		
	}		
	
	/**
	 * Synchronize strutre in VFS with Publishing workflow
	 * @param strutureHTMLs
	 */
	public static Vector<ReleasePageDTO> synchronizeStrutureContent (CmsObject cmsAdminObj, 
			int parentId, Vector<String> structureHTMLs) {
		LOG.info("synchronizeStrutureContent():BEGIN");
		Vector<ReleasePageDTO> childPages = new Vector<ReleasePageDTO>();
		
		try
		{
			if (structureHTMLs != null && cmsAdminObj != null)
			{
				for (int i = 0; i < structureHTMLs.size(); i++)
				{
					String htmlURI = structureHTMLs.elementAt(i);					
					int resourceState = CmsPageUtil.getPageState(cmsAdminObj, htmlURI);
					
					boolean resouceUpdated = false;
					boolean resourceDeleted = false;					
					String auditStatus = "Unknown";
					String auditNotes = ""; 
					
					//LOG.info("Strutured HTML: " + htmlURI + " has state: " + resourceState);
					if (resourceState == CmsResource.STATE_CHANGED.getState() || resourceState == CmsResource.STATE_NEW.getState())
					{
						resouceUpdated = true;
					}
					else if (resourceState == CmsResource.STATE_DELETED.getState())
					{
						resourceDeleted = true;
					}
					
					if (!resourceDeleted)
					{
						if (resouceUpdated)	// If resource is updated, get information of page
						{							
							// Consisder this structure html as child page
							ReleasePageDTO updatePage = PublishingToolbarSQLHandler.getReleaseChildPageByURI(htmlURI);

							long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, htmlURI);
							String lastModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, htmlURI);
							String lastModifyUserName = SystemAccount.getUserNameFromId(cmsAdminObj, lastModifyUserId);
							if (lastModifyUserName == null)
							{
								lastModifyUserName = "Unknown";
							}
							
							if (updatePage != null)
							{		
								updatePage.setSelfEdited("N");
								// Check if page is modified by compare last update stored in bp_release_pages with 
								// pageLastModified
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
								long lastPublishingUpdate = updatePage.getLastUpdate();
															
								LOG.info("Child page exists in publishing workflow: " + htmlURI);
								if (comparingPageLastModified != lastPublishingUpdate)
								{
									LOG.info("Child page modified again: " + comparingPageLastModified);
									
									updatePage.setStatus("Editing");
									updatePage.setLastUpdate(comparingPageLastModified);
									updatePage.setSelfEdited("Y");
									
									auditStatus = "Editing";
									auditNotes = "Resource modified";
									// Audit the page edited action
									PublishingToolbarSQLHandler.doAudit(
											comparingPageLastModified,
											htmlURI,
											lastModifyUserId, 
											auditStatus,
											auditNotes);	
									PublishingToolbarSQLHandler.updateChildPageLastUpdate (updatePage.getPageId(), updatePage.getLastUpdate(), updatePage.getMode(), "Editing");
								}		
								
							}
							else  // this page is new edited or created. Start the workflow for it.
							{
								long comparingPageLastModified = DateTimeUtil.formatToOrderingTime(pageLastModified);
								
								LOG.info("Child page not exists in publishing workflow: " + htmlURI);
								
								// insert a publishing flow record
								String changeMode = "Edited";
								auditStatus = "Editing";
								auditNotes = "Resource modified";
								
								if (resourceState ==  CmsResource.STATE_NEW.getState())
								{
									changeMode = "New";
									auditStatus = "Created";
									auditNotes = "New resource created";
								}
								
								updatePage = new ReleasePageDTO();
								updatePage.setPageURI(htmlURI);
								updatePage.setMode(changeMode);
								updatePage.setLastUpdate(comparingPageLastModified);
								updatePage.setSelfEdited("Y");
								
								updatePage.setPageId(PublishingToolbarSQLHandler.insertReleaseChildPage(updatePage));
								
								// Audit the page edited action
								PublishingToolbarSQLHandler.doAudit(
										comparingPageLastModified,
										htmlURI,
										lastModifyUserId, 
										auditStatus,
										auditNotes);																	
							}	
							
							childPages.add(updatePage);
						}											
					}	// end of if (!resourceDeleted)
					else
					{
						// Handle deleting request in a different manor
						ReleasePageDTO deletedPage = PublishingToolbarSQLHandler.getReleaseChildPageByURI(htmlURI);
						//LOG.info("STRUCTURE DELETE : " + deletedPage);
						// Insert a new one										
						long pageLastModified = CmsPageUtil.getPageLastModifyTime(cmsAdminObj, htmlURI);
						String lastModifyUserId = CmsPageUtil.getPageLastModifyUserId(cmsAdminObj, htmlURI);
						String lastModifyUserName = SystemAccount.getUserNameFromId(cmsAdminObj, lastModifyUserId);
						//LOG.info("Insert new delete page");
						if (lastModifyUserName == null)
						{
							lastModifyUserName = "Unknown";
						}
						
						if (deletedPage != null)	// page existed in the workflow
						{
							deletedPage.setSelfEdited("N");
							
							if (deletedPage.getMode() != null && deletedPage.getMode().equals("Deleted")) // Ignore
							{
								LOG.info("DELETE: Page already deleted and in the publishing workflow --> Ignore");								
							}
							else
							{
								// update mode to Deleted and status to Editing (start a new request)
								// Delete the old one including parent and children
								PublishingToolbarSQLHandler.updateReleaseChildPageMode (deletedPage.getPageId(), 
										"Deleted", deletedPage.getLastUpdate());
								LOG.info("DELETE: Change structure pulishing mode to Deleted");
								deletedPage.setSelfEdited("Y");
							}
						}
						else	// insert a new record to child page table
						{												
							deletedPage = new ReleasePageDTO();
							deletedPage.setPageURI(htmlURI);
							deletedPage.setMode("Deleted");
							deletedPage.setSelfEdited("Y");
							deletedPage.setLastUpdate(DateTimeUtil.formatToOrderingTime(pageLastModified));
							
							deletedPage.setPageId(PublishingToolbarSQLHandler.insertReleaseChildPage(deletedPage));
																					
						}
												
						// Audit the page edited action
						PublishingToolbarSQLHandler.doAudit(
								DateTimeUtil.formatToOrderingTime(pageLastModified),
								htmlURI,
								lastModifyUserId, 
								"Deleted",
								"Resource deleted");						
						
						childPages.add(deletedPage);
					}
					
				}
			}
		}
		catch (Exception e)
		{
			LOG.info("General error while synchronizeStrutureContent: " + e.toString());
		}
		LOG.info("HTML size: "+structureHTMLs.size()+". Changed size: "+childPages.size());
		LOG.info("synchronizeStrutureContent():END");
		return childPages;
	}
	
	/**
	 * Clean the publishing pages in DB that become UNCHANGED in OpenCMS for synchronization
	 *
	 */
	public static void cleanUnpublishingPages(CmsObject cms)
	{
		if (cms == null)
		{
			return;
		}
		
		Vector allPublishingPages = PublishingToolbarSQLHandler.getAllPublishingPages();
		for (int i = 0; i < allPublishingPages.size(); i++)
		{
			ReleasePageDTO publishingPage = (ReleasePageDTO)allPublishingPages.elementAt(i);
			if (publishingPage != null && publishingPage.getSelfEdited() != null && publishingPage.getSelfEdited().equals("Y"))
			{
				String pageURI = publishingPage.getPageURI();
				int pageId = publishingPage.getPageId();
				int pageState = CmsPageUtil.getPageState(cms, pageURI);
				String lastModifiedUserId = CmsPageUtil.getPageLastModifyUserId(cms, pageURI);
				
				// remove the page from publishing flow if page is unchanged or page is no longer exists
				if (pageState == CmsResource.STATE_UNCHANGED.getState() || pageState == -1) {
					LOG.info("Page status="+pageState+ ", then delete");
					// Delete all record of this page in the publishing workflow
					PublishingToolbarSQLHandler.removeReleaseActions(pageId);
					PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(pageId);
					PublishingToolbarSQLHandler.removeReleasePage(pageId);
					
					// audit this action
					PublishingToolbarSQLHandler.doAudit(
							DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
							pageURI, lastModifiedUserId, "Undone", "Page undone in OpenCms");
				}
			}			
		}
		
		PublishingToolbarSQLHandler.removeAllInvalidParentPages();
	}
	
	/**
	 * Check if there is any pages in publishing workflow available to deploy
	 * @return
	 */
	public static boolean hasDeployablePages ()
	{
		return PublishingToolbarSQLHandler.hasDeployablePages();
	}
	
	public static String buildActionNotes (String actionPhrase, String userName, String time, String reason)
	{
		StringBuffer actionNotes = new StringBuffer();
		if (actionPhrase != null)
		{
			actionNotes.append(actionPhrase);
			
			if (userName != null)
			{
				actionNotes.append(" by ").append(userName);
			}
			if (time != null)
			{
				actionNotes.append(" at ").append(time);
			}
			if (reason != null)
			{
				actionNotes.append("\n").append("Reason:\n").append(reason);
			}			
		}
		
		return actionNotes.toString();
	}

	public String buildXmlResponse(boolean isResouceUpdated) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <PtbResourceUpdated>").append(isResouceUpdated).append("</PtbResourceUpdated>\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}