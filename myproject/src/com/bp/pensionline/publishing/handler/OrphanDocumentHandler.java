package com.bp.pensionline.publishing.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.publishing.constants.Constant;
import com.bp.pensionline.publishing.database.PublishingToolbarSQLHandler;
import com.bp.pensionline.publishing.dto.ReleasePageDTO;
import com.bp.pensionline.publishing.util.CmsPageUtil;
import com.bp.pensionline.publishing.util.DateTimeUtil;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

public class OrphanDocumentHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(OrphanDocumentHandler.class);
	private static final long serialVersionUID = 1L;
	
	private int ptbPageId = -1;
	private String ptbPageURI;
	private String ptbAction;
	private String ptbReason;
	private String ptbBugId;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean publishResult = false;
		boolean isReloadPage = true;

		if (parseRequestXml(xml))
		{
			if (currentUser != null)
			{
				try
				{
					String userName = (String)currentUser.getAdditionalInfo(Environment.MEMBER_USERNAME);
					if (userName == null) userName = currentUser.getName();
					System.out.println("Try to process publishing by current user: "+userName+"\n"+currentUser);
					
					CmsObject cmsAdminObj = SystemAccount.getPublishingAdminCmsObject();				
					//cmsAdminObj.getRequestContext().setSiteRoot("/");				
					//cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
					
					// User in Deployers group, deploy all changes
					if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP))
					{
						if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_DEPLOYALLCHANGES) &&
								ptbBugId != null && !ptbBugId.trim().equals(""))
						{
							// ptbBugId contains package name							
							// get HTML approved parent pages
							Vector<ReleasePageDTO> approvedParentPages =  PublishingToolbarSQLHandler.getAllApprovedParentPages();
							Vector<ReleasePageDTO> allChildPages =  PublishingToolbarSQLHandler.getAllChildPages();
							
							if (approvedParentPages != null && approvedParentPages.size() > 0)
							{
								int releaseId = -1;
								
								LOG.info("Number of approved pages: " + approvedParentPages.size());
								for (int i = 0; i < approvedParentPages.size(); i++)
								{
									//LOG.info("Getting approved page at: " + i);
									ReleasePageDTO approvedPage = approvedParentPages.elementAt(i);
									if (approvedPage != null && 
											PublishingToolbarSQLHandler.isParentPageReadyToDeploy(approvedPage.getPageId()))
									{
										if (releaseId == -1)
										{											
											releaseId = PublishingToolbarSQLHandler.createReleasePackage(ptbBugId, ptbReason);
											LOG.info("Release package created with Id: " + releaseId);
										}
										if (releaseId > -1)
										{
											Vector<ReleasePageDTO> approvedChildPages = 
												PublishingToolbarSQLHandler.getChildPagesByParent(approvedPage.getPageId());
											PublishingToolbarSQLHandler.createReleaseNote(releaseId, approvedPage, approvedChildPages);
										}
									}							
								}
								
								LOG.info("Create a release notes done. All children size: " + allChildPages.size());
								
								// create release package zip file BEGIN
								// get a list of pages that have status 'Deployed' but not in 'Impacted' mode
								ArrayList<String> deployedPages = PublishingToolbarSQLHandler.getDeployedParentURIs();
								LOG.info("After creating release notes: Number of deployed pages: " + deployedPages.size());
								for (int i = 0; i < allChildPages.size(); i++)
								{
									ReleasePageDTO childPage = allChildPages.elementAt(i);
									if (childPage != null && PublishingToolbarSQLHandler.isChildPageReadyToDeploy(childPage.getPageId()))
									{
										deployedPages.add(childPage.getPageURI());
									}
								}
								
								LOG.info("getUndeletedDeployedParentURIs including children: " + deployedPages.size());
								
								if (deployedPages != null && deployedPages.size() > 0)
								{
									// get deploy folder config path
									String deployFolder = CheckConfigurationKey.getStringValue("publishing.deployfolder");
									LOG.info("Exporting deployed pages to " + ptbBugId);
									// export files to the gif file
									publishResult = CmsPageUtil.exportResources(cmsAdminObj, deployFolder + ptbBugId,
											deployedPages, true);
									
									// Publish all the page deployed
									CmsPageUtil.publishPage(cmsAdminObj, deployedPages);
									
									// audit deploy action
									for (int i = 0; i < deployedPages.size(); i++)
									{
										PublishingToolbarSQLHandler.doAudit(
												DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
												deployedPages.get(i),
												currentUser.getId().toString(), 
												"Deployed",
												ptbReason);										
									}	
									if (publishResult)
									{
										// Clean up published pages
										cleanUpDeployedPages();
									}
//																											
								}
							}							

						}								
					}		
					else {
						if (ptbPageId != -1 && ptbPageURI != null && !ptbPageURI.trim().equals("")) {
							//String fullPathPageURI = "/sites/default" + ptbPageURI;
							String fullPathPageURI = ptbPageURI;
							SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy h:mm a");
							String atTimeStr = dateFormat.format(new Date(System.currentTimeMillis()));
							long updateTime = DateTimeUtil.formatToOrderingTime(System.currentTimeMillis());
							String mode = PublishingToolbarSQLHandler.getOrphanDocumentMode(ptbPageId);
							if (mode.indexOf('_')!=-1) {
								mode = mode.substring(0, mode.indexOf('_'));
							}
							LOG.info("PageURI = " + fullPathPageURI);
							
							// If authors submit a edited page for review
							if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP)) {
								publishResult = doEditorTask(currentUser, cmsAdminObj, atTimeStr, 
											 				 fullPathPageURI, mode, updateTime, 
											 				 publishResult, isReloadPage);
							}
							// User in both Editor and reviewer group (PUBLISHING_EDITOR_REVIEWER_GROUP)
							else if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)) {
								publishResult = doEditorReviewerTask(currentUser, cmsAdminObj, atTimeStr, 
													 				 fullPathPageURI, mode, updateTime, 
													 				 publishResult, isReloadPage);
							}							
							// User in Reviewers group 
							else if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP)) {
								publishResult = doReviewerTask(currentUser, cmsAdminObj, atTimeStr, 
															   fullPathPageURI, mode, updateTime, 
															   publishResult, isReloadPage);
							}
							// User in Authorisers group 
							if (cmsAdminObj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP)) {
								publishResult = doAuthoriserTask(currentUser, cmsAdminObj, atTimeStr, 
																 fullPathPageURI, mode, updateTime, 
																 publishResult, isReloadPage);
							}
						}
					}
				}
				catch (Exception e)
				{
					LOG.error("Error while updating page info: " + e.toString());
					buildXmlResponseError("Updating page info failed: " + e.toString());
				}
			}
			else
			{
				LOG.error("Publishing error: Current user is null");
			}
		}
		
		xmlResponse = buildXmlResponse(publishResult, isReloadPage);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <PtbActionResult>").append(isActionOk).append("</PtbActionResult>\n");
		buffer.append("     <PtbPageReload>").append(willReloadPage).append("</PtbPageReload>\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_PUBLISHING_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{
		
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList paramNodes = root.getChildNodes();
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.PUBLISHING_PAGE_ID))
					{
						//LOG.info(" getting ptbPageURI: " + paramNode.getTextContent());
						try
						{
							ptbPageId = Integer.parseInt(paramNode.getTextContent());
						}
						catch (Exception e)	// ParseException
						{
							LOG.error("Error while getting pageId from request param XML: " + e.toString());
							ptbPageId = -1;
						}
						
					}
					if (paramNode.getNodeName().equals(Constant.PUBLISHING_PAGE_URI))
					{
						//LOG.info(" getting ptbPageURI: " + paramNode.getTextContent());
						ptbPageURI = paramNode.getTextContent();
					}					
					if (paramNode.getNodeName().equals(Constant.PUBLISHING_ACTION))
					{
						//LOG.info(" getting ptbAction: " + paramNode.getTextContent());
						ptbAction = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.PUBLISHING_REASON))
					{
						//LOG.info(" getting ptbReason: " + paramNode.getTextContent());
						ptbReason = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.PUBLISHING_BUG_ID))
					{
						//LOG.info(" getting ptbBugId: " + paramNode.getTextContent());
						ptbBugId = paramNode.getTextContent();
					}						
				}
			}

			bais.close();

			LOG.info("ptbPageURI: " + ptbPageURI);
			LOG.info("ptbAction: " + ptbAction);
			LOG.info("ptbReason: " + ptbReason);
			LOG.info("ptbBugId: " + ptbBugId);
			
			return (ptbPageURI != null && !ptbPageURI.trim().equals("") && ptbAction != null && !ptbAction.trim().equals(""));

		}
		catch (Exception ex)
		{
			LOG.error("XML string: " + xml);
			LOG.error("Error in parsing publishing request XML: " + ex.toString());
		}
		

		return false;

	}	
	
	public static void cleanUpDeployedPages ()
	{
		Vector<Integer> deployedPageIds = PublishingToolbarSQLHandler.getAllDeployedParentPageIds();
		for (int i = 0; i < deployedPageIds.size(); i++)
		{
			int parentPageId = deployedPageIds.get(i).intValue(); 
			
			PublishingToolbarSQLHandler.removeReleasePage(parentPageId);
			PublishingToolbarSQLHandler.removeReleaseActions(parentPageId);
			PublishingToolbarSQLHandler.removeReleaseChildPagesByParent(parentPageId);					
		}
		
		PublishingToolbarSQLHandler.removeAllInvalidParentPages();
	}		


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	private boolean doEditorTask(CmsUser currentUser, CmsObject cmsAdminObj,
								 String atTimeStr, String fullPathPageURI, 
								 String mode, long updateTime,
								 boolean publishResult, boolean isReloadPage) {
		if (Constant.PUBLISHING_ACTION_SEND4REVIEW.equals(ptbAction)) {
			// update the publishing page in bp_release_pages
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Edited", ptbBugId);
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Submit page for review",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId,  
					currentUser.getId().toString(),
					"Edited", actionNotes, updateTime);
			
			// Audit the action. This is special case, we do not need to check if page is self edited
			if (publishResult) {
				PublishingToolbarSQLHandler.doAudit(
						DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
						ptbPageURI,
						currentUser.getId().toString(), 
						"Edited",
						ptbReason);											
			}
		} else if (Constant.PUBLISHING_ACTION_REVOKECHANGE.equals(ptbAction)) {
			//publishResult = PublishingToolbarSQLHandler.updatePageInfo(ptbPageId, "Editing", ptbReason, ptbBugId);
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Editing", ptbBugId);									
			
			// Create a notes for this Revoke action
			String actionNotes = EditorLandingHandler.buildActionNotes("Revoke page",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Editing", actionNotes, updateTime);	
			
			// Audit the action
			if (publishResult) {
				PublishingToolbarSQLHandler.doAudit(
						DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
						ptbPageURI,
						currentUser.getId().toString(), 
						"Revoked",
						ptbReason);										
			}									
		} else if (Constant.PUBLISHING_ACTION_UNDOCHANGE.equals(ptbAction)) {															
			// Undo all changes of this resouce		
			boolean undoResult = true;
			undoResult = CmsPageUtil.undoAllChanges(cmsAdminObj, fullPathPageURI);
			
			if ("New".equals(mode)) {
				LOG.info("Page is new created. Undo changes will redirect to recent_changes.jsp");
				isReloadPage = false;
			}
			
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Undone",
					ptbReason);
			
			// If page has attachment, update the self_edited to "N"										
			// remove all publishing data related to this page: 
			// release, action. Audit the undo action
			boolean removePageResult = PublishingToolbarSQLHandler.removeOrphanReleasePage(ptbPageId);
			boolean removeActionResult = PublishingToolbarSQLHandler.removeReleaseActions(ptbPageId);
			publishResult = (removePageResult && removeActionResult);																	
		}
		return publishResult;
	}
	
	private boolean doEditorReviewerTask(CmsUser currentUser, CmsObject cmsAdminObj,
			 							 String atTimeStr, String fullPathPageURI, 
			 							 String mode, long updateTime,
			 							 boolean publishResult, boolean isReloadPage) {						
		if (Constant.PUBLISHING_ACTION_SEND4REVIEW.equals(ptbAction)) {
			// update the publishing page in bp_release_pages
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Edited", ptbBugId);
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Submit page for review",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId,  
					currentUser.getId().toString(),
					"Edited", actionNotes, updateTime);
			
			// Audit the action. This is special case, we do not need to check if page is self edited
			if (publishResult) {
				PublishingToolbarSQLHandler.doAudit(
						DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
						ptbPageURI,
						currentUser.getId().toString(), 
						"Edited",
						ptbReason);											
			}
			
		} else if (Constant.PUBLISHING_ACTION_REVOKECHANGE.equals(ptbAction)) {
			//publishResult = PublishingToolbarSQLHandler.updatePageInfo(ptbPageId, "Editing", ptbReason, ptbBugId);
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Editing", ptbBugId);									
			
			// Create a notes for this Revoke action
			String actionNotes = EditorLandingHandler.buildActionNotes("Revoke page",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Editing", actionNotes, updateTime);	
			
			// Audit the action
			if (publishResult) {
				PublishingToolbarSQLHandler.doAudit(
						DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
						ptbPageURI,
						currentUser.getId().toString(), 
						"Revoked",
						ptbReason);										
			}	
			
		} else if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_UNDOCHANGE)) {															
			// Undo all changes of this resouce		
			boolean undoResult = true;
			undoResult = CmsPageUtil.undoAllChanges(cmsAdminObj, fullPathPageURI);
			
			if ("New".equals(mode)) {
				LOG.info("Page is new created. Undo changes will redirect to recent_changes.jsp");
				isReloadPage = false;
			}
			
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Undone",
					ptbReason);
			
			// If page has attachment, update the self_edited to "N"										
			// remove all publishing data related to this page: 
			// release, action. Audit the undo action
			boolean removePageResult = PublishingToolbarSQLHandler.removeOrphanReleasePage(ptbPageId);
			boolean removeActionResult = PublishingToolbarSQLHandler.removeReleaseActions(ptbPageId);
			publishResult = (removePageResult && removeActionResult);
			
		} else if (Constant.PUBLISHING_ACTION_PASSCHECKING.equals(ptbAction)) {
			// update the publishing page in bp_release_pages to Checked
			LOG.info("updateReleasePageStatus to 'Checked'");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Checked", ptbBugId);//updateReleasePageStatus(ptbPageId, "Checked");
			LOG.info("publishResult = " + publishResult);
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page passed checking",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Checked", actionNotes, updateTime);
			
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Checked",
					ptbReason);
			
		} else if (Constant.PUBLISHING_ACTION_REJECTCHANGE.equals(ptbAction)) {
			// update the publishing page in bp_release_pages to Checked
			//publishResult = PublishingToolbarSQLHandler.updateReleasePageStatus(ptbPageId, "Editing");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Editing", ptbBugId);
		
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page rejected",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Editing", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')											
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Rejected",
					ptbReason);			
		}
		return publishResult;
	}
	
	private boolean doReviewerTask(CmsUser currentUser, CmsObject cmsAdminObj,
			 					   String atTimeStr, String fullPathPageURI, 
			 					   String mode, long updateTime,
			 					   boolean publishResult, boolean isReloadPage) {
		if (Constant.PUBLISHING_ACTION_PASSCHECKING.equals(ptbAction)) {
			// update the publishing page in bp_release_pages to Checked
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Checked", ptbBugId);//updateReleasePageStatus(ptbPageId, "Checked");
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page passed checking",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Checked", actionNotes, updateTime);
			
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Checked",
					ptbReason);			
		} else if (Constant.PUBLISHING_ACTION_REJECTCHANGE.equals(ptbAction)) {
			// update the publishing page in bp_release_pages to Checked
			//publishResult = PublishingToolbarSQLHandler.updateReleasePageStatus(ptbPageId, "Editing");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Editing", ptbBugId);
		
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page rejected",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Editing", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')											
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Rejected",
					ptbReason);		
		}
		return publishResult;
	}
	
	private boolean doAuthoriserTask(CmsUser currentUser, CmsObject cmsAdminObj,
			   						 String atTimeStr, String fullPathPageURI, 
			   						 String mode, long updateTime,
			   						 boolean publishResult, boolean isReloadPage) {							
		if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_APPROVECHANGE)) {									
			// update the publishing page in bp_release_pages to Approved
			//publishResult = PublishingToolbarSQLHandler.updateReleasePageStatus(ptbPageId, "Approved");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Approved", ptbBugId);
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page approved",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Approved", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Approved",
					ptbReason);														
		} else if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_UNAPPROVECHANGE)) {
			// update the publishing page in bp_release_pages to Checked
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Editing", ptbBugId);
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Page unapproved",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Editing", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Unapproved",
					ptbReason);
			
		} else if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_PAUSEDEPLOY)) {
			// update the publishing page in bp_release_pages to Paused
			//publishResult = PublishingToolbarSQLHandler.updateReleasePageStatus(ptbPageId, "Paused");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Paused", ptbBugId);
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Deployment page paused",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Paused", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Paused",
					ptbReason);
			
		} else if (ptbAction != null && ptbAction.equals(Constant.PUBLISHING_ACTION_UNPAUSEDEPLOY)) {
			// update the publishing page in bp_release_pages to Paused
			//publishResult = PublishingToolbarSQLHandler.updateReleasePageStatus(ptbPageId, "Approved");
			publishResult = PublishingToolbarSQLHandler.updateOrphanPageInfo(ptbPageId, "Approved", ptbBugId);
			
			// Create a notes for this Submit action
			String actionNotes = EditorLandingHandler.buildActionNotes("Deployment page unpaused",
					currentUser.getName(), atTimeStr, ptbReason);
			
			// insert new record into the publishing action in bp_release_actions								
			PublishingToolbarSQLHandler.insertOrphanReleaseAction(
					ptbPageId, 
					currentUser.getId().toString(),
					"Unpaused", actionNotes, updateTime);
			
			// Audit the action
			// audit for current page: Ignore page that is not modified (self_edited = 'N')
			PublishingToolbarSQLHandler.doAudit(
					DateTimeUtil.formatToOrderingTime(System.currentTimeMillis()),
					ptbPageURI,
					currentUser.getId().toString(), 
					"Unpaused",
					ptbReason);	
		}
		return publishResult;
	}
}