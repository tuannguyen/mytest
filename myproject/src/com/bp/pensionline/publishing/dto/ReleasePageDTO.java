package com.bp.pensionline.publishing.dto;

public class ReleasePageDTO
{
	private int pageId = -1;
	private String pageURI = null;
	private String mode = null;
	private String status = null;
	private String bugId = null;
	private String notes = null;
	private long lastUpdate = -1;
	private String selfEdited = "N";
	private String lastModifiedBy = null;	// CMSUUID
	
	// additional field for orphan
	private boolean isOrphan = false;
	private boolean isAbleSend4Review = false;
	private boolean isAblePassChecking = false;
	// added for child page
	private int parentId = -1;
	
	// Aditional fields for Undo Changes action and Permission checking
	private int restoreVersion = -1;
	private boolean isPermissionUpdated = false;
	private String permissionUpdateDes = null;

	/**
	 * @return the bugId
	 */
	public String getBugId()
	{
		return bugId;
	}

	/**
	 * @param bugId the bugId to set
	 */
	public void setBugId(String bugId)
	{
		this.bugId = bugId;
	}

	/**
	 * @return the lastUpdate
	 */
	public long getLastUpdate()
	{
		return lastUpdate;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(long lastUpdate)
	{
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the mode
	 */
	public String getMode()
	{
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode)
	{
		this.mode = mode;
	}

	/**
	 * @return the notes
	 */
	public String getNotes()
	{
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	/**
	 * @return the pageId
	 */
	public int getPageId()
	{
		return pageId;
	}

	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(int pageId)
	{
		this.pageId = pageId;
	}

	/**
	 * @return the pageURI
	 */
	public String getPageURI()
	{
		return pageURI;
	}

	/**
	 * @param pageURI the pageURI to set
	 */
	public void setPageURI(String pageURI)
	{
		this.pageURI = pageURI;
	}

	/**
	 * @return the parentId
	 */
	public int getParentId()
	{
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(int parentId)
	{
		this.parentId = parentId;
	}

	/**
	 * @return the selfEdited
	 */
	public String getSelfEdited()
	{
		return selfEdited;
	}

	/**
	 * @param selfEdited the selfEdited to set
	 */
	public void setSelfEdited(String selfEdited)
	{
		this.selfEdited = selfEdited;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy()
	{
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy)
	{
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * @param isOrphan the isOrphan to set
	 */
	public void setOrphan(boolean isOrphan) {
		this.isOrphan = isOrphan;
	}

	/**
	 * @return the isOrphan
	 */
	public boolean isOrphan() {
		return isOrphan;
	}

	/**
	 * @param isAbleSend4Review the isAbleSend4Review to set
	 */
	public void setAbleSend4Review(boolean isAbleSend4Review) {
		this.isAbleSend4Review = isAbleSend4Review;
	}

	/**
	 * @return the isAbleSend4Review
	 */
	public boolean isAbleSend4Review() {
		return isAbleSend4Review;
	}

	/**
	 * @param isAblePassChecking the isAblePassChecking to set
	 */
	public void setAblePassChecking(boolean isAblePassChecking) {
		this.isAblePassChecking = isAblePassChecking;
	}

	/**
	 * @return the isAblePassChecking
	 */
	public boolean isAblePassChecking() {
		return isAblePassChecking;
	}

	/**
	 * @return the restoreVersion
	 */
	public int getRestoreVersion()
	{
		return restoreVersion;
	}

	/**
	 * @param restoreVersion the restoreVersion to set
	 */
	public void setRestoreVersion(int restoreVersion)
	{
		this.restoreVersion = restoreVersion;
	}

	/**
	 * @return the isPermissionUpdated
	 */
	public boolean isPermissionUpdated()
	{
		return isPermissionUpdated;
	}

	/**
	 * @param isPermissionUpdated the isPermissionUpdated to set
	 */
	public void setPermissionUpdated(boolean isPermissionUpdated)
	{
		this.isPermissionUpdated = isPermissionUpdated;
	}

	/**
	 * @return the permissionUpdateDes
	 */
	public String getPermissionUpdateDes()
	{
		return permissionUpdateDes;
	}

	/**
	 * @param permissionUpdateDes the permissionUpdateDes to set
	 */
	public void setPermissionUpdateDes(String permissionUpdateDes)
	{
		this.permissionUpdateDes = permissionUpdateDes;
	}
	
	
}
