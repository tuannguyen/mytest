package com.bp.pensionline.publishing.dto;

public class ReleasePackageDTO
{
	private int releaseId = -1;
	private String packageName = null;
	private long releasedAt = -1;
	private String notes = null;
	
	/**
	 * @return the notes
	 */
	public String getNotes()
	{
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	/**
	 * @return the packageName
	 */
	public String getPackageName()
	{
		return packageName;
	}
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}
	/**
	 * @return the releasedAt
	 */
	public long getReleasedAt()
	{
		return releasedAt;
	}
	/**
	 * @param releasedAt the releasedAt to set
	 */
	public void setReleasedAt(long releasedAt)
	{
		this.releasedAt = releasedAt;
	}
	/**
	 * @return the releaseId
	 */
	public int getReleaseId()
	{
		return releaseId;
	}
	/**
	 * @param releaseId the releaseId to set
	 */
	public void setReleaseId(int releaseId)
	{
		this.releaseId = releaseId;
	}
	
	
}
