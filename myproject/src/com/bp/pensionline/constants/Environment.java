package com.bp.pensionline.constants;

import com.bp.pensionline.util.CheckConfigurationKey;


/*
 * (DataDicConfigurationXML) copyright BP Pensions 2003
 */



// JDK Imports

// J2EE Imports

/**
 * Gives access to various environmental config information
 *
 *  @author dcarlyle
 *  @author $Author: vinh.dao $
 *  @version $Revision: 1.4 $
 * 
 * 
 *  $Id: Environment.java,v 1.4 2007/04/03 09:29:37 vinh.dao Exp $
 */
public class Environment {
	
   /** Hanoi team references - note for REFNO and BGROUP they should match the mapper tags.*/
   
   /*Match the XML tags, @see the wiki to help http://aquilaforum.com/lighthouse/index.php?title=New_design_including_Benefit_statement_data */
	public static final String MEMBER_BGROUP="Bgroup";
	public static final String MEMBER_CREFNO="MEMBER_CREFNO";
	public static final String MEMBER_REFNO="Refno";
	public static final String MEMBER_GROUP="MEMBER_GROUP";
	public static final String MEMBER_USERNAME="MEMBER_USERNAME";
	public static final String MEMBER_USERPASSWORD="MEMBER_USERPASSWORD";
	public static final String MEMBER_CURRENT_GROUP="MEMBER_CURRENT_GROUP";
	public static final String MEMBER_DESCRIPTION="MEMBER_DESCRIPTION";
	public static final String SELECT_MEMBER="SELECT_MEMBER";
	public static final String MEMBER_FILENAME="MEMBER_FILENAME"; 
	public static final String MEMBER_KEY="MEMBER_KEY";
	public static final String MEMBER_DETAIL_KEY="MEMBER_DETAIL_KEY";
	public static final String MEMBER_EOW="EOW";
	public static final String MEMBER_AGCODE="Agcode";
	public static final String MEMBER_CALCTYPE="CalcType";
	
	// Added by Binh Nguyen: November 13, 2009
	public static final String GROUP_BP1_USER = "BP1_User_group";
	public static final String GROUP_FORCE_SELF_SERVICE_USER = 
								"Force_self_service_group";
	
	// Added by Huy
	public static final String MEMBER_SESSION_ID = "MEMBER_SESSION_ID";
	
	//HUY
	public static final String MEMBER_CALCTOTAL="total_calcs";
	public static final String MEMBER_CALCOLD="calc_old";
	public static final String MEMBER_CALCTIMEOUT="calc_timeout";
	public static final String MEMBER_CALCOUTPUTWAIT="calc_output_wait";
	
	public static final String CR_CALCTYPE="99";
	public static final String RR_CALCTYPE="97";
	public static final String RD_CALCTYPE="96";
	
	public static final String AGCODE="BP01";
	
	public static final String MEMBER_STATUS="MembershipStatus";
	public static final String MEMBER_CONTRIBUTION_HISTORY="ContributionHistory";
	public static final String MEMBER_CURRENTLY_CONTRIBUTING="CurrentlyContributing";
	public static final String MEMBER_CONTRIBUTION_COUNT = "ContributionHistoryCount";
	public static final String MEMBER_POSSTART="Start";
	public static final String MEMBER_POSEND="End";
	public static final String MEMBER_STATUSRAW="MembershipStatusRaw";
	public static final String MEMBER_SI="SecurityIndicator";
	public static final String MEMBER_SCHEME="Scheme";
	public static final String MEMBER_ACCESS_LEVEL="MEMBER_ACCESS_LEVEL";
	public static final String MEMBER_NUMBER_OF_RECORDS="MEMBER_NUMBER_OF_RECORDS";	
	public static final String MEMBER_SCHEME_RAW="SchemeRaw";
	public static final String AVC_NO_FUND_NAME="no fund name found";
	
	/** Additional fields for web stats */
	
	public static final String MEMBER_GENDER = "Gender.Text";
	public static final String MARITAL_STATUS = "MaritalStatus.Text";
	public static final String WSPOSTCODE = "WsPostcode";
	public static final String OVERSEASINDICATOR = "OverseasIndicator.Text";
	public static final String PENSIONABLESALARY = "PensionableSalary";
	public static final String DOB = "Dob";
	public static final String PENSION_START_DATE = "PensionStartDate";	// HUY: Added for Payslip
	
	/** Force_password_change group */
	public static final String FORCE_PASSWORD_CHANGE = "FORCE_CHANGE_PASSWORD";
   /** YEARS_OF_SERVICE to calculate the LTA - 20 by default */
   public static int YEARS_OF_SERVICE = 20;
	 
	 
   /** mapping for pensionline database access to aquila*/
   public static int AQUILA = 1;
   /** mapping for pensionline letters */
   public static int LETTERS = 2;
   /** mapping for pensionline CMS system*/
   public static int CMS = 3;
   /** mapping for pensionline CMS system*/
   public static int SQL = 4;
   
   /** mapping for pensionline CMS system*/
   public static int WEBSTATS = 5;
   
   /** mapping for pensionline additional components */
   public static int PENSIONLINE = 6;
   
   /** mapping for pensionline database access to Aviary*/
   public static int AVIARY = 7;
   
   /** mapping for pensionline database access to ExtraView*/
   public static int EXTRAVIEW = 8;
   
   /** mapping for pensionline database access to Aviary*/
   public static int RELEASE = 9;
   
   
   /** mapping for pensionline database access to Aviary*/
   public static int REPORT = 10;
   
   public static final String RECORD_LIST="RECORD_LIST";
   
   
   /** default oracle jndi */
   public static String ORACLE_DB = "java:DefaultDS";
   
   
   /**XML file Name*/
   public static String XMLFILE_DIR="/system/modules/com.bp.pensionline.test_members/";
   public static String PAYSLIPMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/PaySlipMapper.xml";
   public static String RECORDMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/RecordMapper.xml";
   public static String MEMBERMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/MemberMapper.xml";
   public static String SECURITYMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/SecurityMapper.xml";
   public static String TESTMEMBERMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/TestMemberMapper.xml";
   public static String CALCMAPPER_FILE="/system/modules/com.bp.pensionline.test_members/CalculationMapping.xml";
   public static String COMPANY_SCHEME_FILE="/system/modules/com.bp.pensionline.test_members/CompanySchemeMap.xml";
   public static String CALC_ADJUST_FLAG_FILE="/system/modules/com.bp.pensionline.test_members/CalcAdjustFlag.xml";
   public static String CALC_REDUNDANCY_DATE_CHANGE_EFFECTIVE_FILE="/system/modules/com.bp.pensionline.test_members/RedundancyDateChangeEffective.xml";
   
   public static String MEMBERMAPPERFILE_DIR="/system/modules/com.bp.pensionline.test_members/MemberMapper/";

   public static final String DICTIONARY_DIR = "/sites/default/dictionary/";
   public static final String CAVEAT_DIR = "/sites/default/caveat/";
   public static final String FAQ_DIR = "/sites/default/faq/";
   public static final String SITE_DEFAULT = "/sites/default";
   
   // Letter mapping file and Letter constants
   public static String LETTER_MAPPING_FILE="/system/modules/com.bp.pensionline.letter/letter_mapping.xml";
   public static String LETTER_MEMBER_QUERY_FILE="/system/modules/com.bp.pensionline.letter/letter_member.sql";
   public static String LETTER_SIGNATURE_SETTING_FILE="/system/modules/com.bp.pensionline.letter/signature.xml";
   public static String LETTER_TAG_NAME					= "LETTER";
   public static String LETTER_BEFOREHEADER_TAG_NAME	= "BEFORE_HEADER";
   public static String LETTER_HEADER_TAG_NAME			= "HEADER";
   public static String LETTER_BODY_TAG_NAME			= "BODY";
   public static String LETTER_FOOTER_TAG_NAME			= "FOOTER";
   public static String LETTER_AFTERFOOTER_TAG_NAME		= "AFTER_FOOTER";
   public static String LETTER_SIGNATURE_LOCAL_TAG_NAME		= "SIGNATURE_LOCAL";
   public static String LETTER_SIGNATURE_CMS_TAG_NAME		= "SIGNATURE_CMS";
   public static String LETTER_TEMPLATE_TAG_NAME		= "LETTER_TEMPLATE";
   public static String LETTER_XSLT_TAG_NAME			= "XSLT";
   public static String LETTER_XSL_HTML2FO_TAG_NAME		= "HTML2FO_XSL";
   public static String LETTER_XSL_FO2WORD_TAG_NAME		= "FO2WORD_XSL";   
   public static String LETTER_BODY_CONTENT_TAG_NAME	= "content";

   public static String[] TAGNAME={"PeriodOfService", "PeriodOfServiceData", "Contribution"};
  // public static String[] TAGNAME={"PeriodOfService", "PeriodOfServiceData"};   
   public static String POSTAG="PeriodOfService";
   public static String POSDTAG="PeriodOfServiceData";
   public static String TWOTAGS=POSTAG+POSDTAG;
// Begin IMail define
  	public static final String IMAIL_BGROUP="Bgroup";
  	public static final String IMAIL_REFNO="Refno";
  	public static final String IMAIL_NI="NI";
  	public static final String IMAIL_CREATIONDATE="CreationDate";
  	public static final String IMAIL_CASEWORKID="IMAIL_CASEWORKID";
  	public static final String IMAIL_SUBJECT="Subject";
  	public static final String IMAIL_MESSAGE="Message";
  	public static final String IMAIL_FORMDATA="IMAIL_FORMDATA";
  	public static final String IMAIL_QUERY="Query";
  	public static final String IMAIL_EMAILADRESS="emailaddress";
  	public static final String IMAIL_TAG="Imail";
  	public static final String IMAIL_NAME="Name";
  	public static final String IMAIL_ADDRESS="Address";
  	public static final String IMAIL_TEMP="IMAIL_TEMP";
  	
  	
  //End IMail define
//  begin define CompanyScheme
	public static final String COMPANY_SCHEME="CompanyScheme";
	public static final String COMPANY_SCHEME_TAG="CompanySchemeMap";
	public static final String COMPANY_SCHEME_BP="BP";
	public static final String COMPANY_SCHEME_BC="BC";
	public static final String COMPANY_SCHEME_SUB="SUBS";
	// end define CompanyScheme
   
// Begin: define BP_Wall_groups Table field name
	public static final String WALL_DATA="Data";
	public static final String WALL_GNAME="GName";
	public static final String WALL_SUPERUSER="SUPERUSER";
	public static final String WALL_SYSTEM="SYSTEM";
	
	
  //End: define BP_Wall_groups Table field name
	
//	 Begin: define BP_FORMS Table field name
	public static final String CHANGE_EMAIL="ChangeOfEmail";
	public static final String CHANGE_PHONE="ChangeOfPhone";
	public static final String CHANGE_ADDRESS="ChangeOfAddress";	
	public static final String CHANGE_BANK="ChangeOfBank";
	
	// Added by Huy for publishing toolbar
	public static final String PUBLISHING_EDITOR_GROUP = CheckConfigurationKey.getStringValue("publishing.group.editor");
	public static final String PUBLISHING_EDITOR_REVIEWER_GROUP = CheckConfigurationKey.getStringValue("publishing.group.editor_reviewer");
	public static final String PUBLISHING_REVIEWER_GROUP = CheckConfigurationKey.getStringValue("publishing.group.reviewer");
	public static final String PUBLISHING_AUTHORISER_GROUP = CheckConfigurationKey.getStringValue("publishing.group.authoriser");
	public static final String PUBLISHING_DEPLOYER_GROUP = CheckConfigurationKey.getStringValue("publishing.group.deployer");
	
	public static final String PL_REPORT_RUNNER = "PL_REPORT_RUNNER";
	public static final String PL_REPORT_EDITOR = "PL_REPORT_EDITOR";	
	
	// Added by Huy for webstats viewers
	public static final String WEBSTATS_VIEWER_GROUP = "WebStatsViewers";
	
  //End
	//BEGIN: Define calculation member map
	
//	END: Define calculation member map
   /**
    * 
    */
   public Environment() {
      super();
   }
  
   
 
}//:~ End of Environment
