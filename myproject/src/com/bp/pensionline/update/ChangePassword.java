package com.bp.pensionline.update;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.MigrationDataHandler;
import com.bp.pensionline.handler.SwapUserHandler;
import com.bp.pensionline.handler.UserExpriedHandler;
import com.bp.pensionline.handler.PensionLinePasswordHandler;

public class ChangePassword extends HttpServlet {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private static final long serialVersionUID = 1L;

	private String requestUserName = null;

	private String requestOldPassword = null;

	private String requestNewPassword = null;

	private String requestConfirmPassword = null;

	private String responseMessage = "";
	
	private String ninoName = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// get request Parameter
		String requestXml = request.getParameter(Constant.PARAM);
		//LOG.info("requestXml: " + requestXml);
		
		// get UserName from session
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null)	requestUserName = currentUser.getName();
		
		LOG.info("Change password for user: " + requestUserName);
		
		if (requestUserName == null) {
			responseMessage = "You must login before change your password.";
		} else if (requestXml == null) {
			responseMessage = "Fail to change password.";
		} else {
			try {
				if (parseRequestXml(requestXml)) {
					if (checkPasswordValidation(request)) {
						CmsObject obj = SystemAccount.getAdminCmsObject();
						
						obj.setPassword(requestUserName, requestOldPassword,
								requestNewPassword);
						
						UserExpriedHandler.delete(requestUserName, Environment.FORCE_PASSWORD_CHANGE);
						if(obj.userInGroup(requestUserName, Environment.FORCE_PASSWORD_CHANGE)){
							SwapUserHandler.getInstance().unSwapUser(requestUserName, Environment.FORCE_PASSWORD_CHANGE, obj);
							
						}
						responseMessage = "Password has been changed.";
					}
				}
			} catch (Exception ex) {
				responseMessage = "Fail to change password.";
				LOG.error("responseMessage: " + ex.getMessage());
			}
		}

		response.getWriter().print(responseMessage);

	}

	// get parameter from RequestXML
	private Boolean parseRequestXml(String xml)
	{

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{
					if (node.getNodeName().equalsIgnoreCase(Constant.OLDPASSWORD_NODE))
					{						
						//requestOldPassword = node.getTextContent();
						requestOldPassword = getCDataText(node);

					}
					else if (node.getNodeName().equalsIgnoreCase(
							Constant.NEWPASSWORD_NODE))
					{
						requestNewPassword = getCDataText(node);
					}
					else if (node.getNodeName().equalsIgnoreCase(
							Constant.CONFIRMPASSWORD_NODE))
					{
						//requestConfirmPassword = node.getTextContent();
						requestConfirmPassword = getCDataText(node);						
					}
				}
			}
			bais.close();

		}
		catch (Exception ex)
		{
			LOG.error("Exception: " + ex.getMessage());
			requestOldPassword = "";
			requestNewPassword = "";
			requestConfirmPassword = "";
			responseMessage = "Invalid data.";
			return false;
		}
		 requestOldPassword = ToUniCode(requestOldPassword);
		 requestNewPassword = ToUniCode(requestNewPassword);
		 requestConfirmPassword = ToUniCode(requestConfirmPassword);

		return true;
	}
	
	public String getCDataText(Node aNode)
	{
		if (aNode != null)
		{
			NodeList requestTagNodes = aNode.getChildNodes();
			for (int j = 0; j < requestTagNodes.getLength(); j++)
			{
				Node current = requestTagNodes.item(j);
				if(current.getNodeType() == Node.CDATA_SECTION_NODE)
				{
					return ((CDATASection)current).getData();
				}
			}			
		}
		
		return "";
	}
	
	private String ToUniCode(String str){
		
		StringBuffer buffer = new StringBuffer(str);
		
		String[] arrUni = new String[]{"�","+"};
		String[] arrAscii = new String[]{"&pound;","&plusH;"};
		
		try{
		
		for(int i=0; i< arrAscii.length; i++){
			
			int idx = -1;
			int length = arrAscii[i].length();
			
			idx = str.indexOf(arrAscii[i]);
			
			while (idx >= 0){
				
				
				buffer = buffer.replace(idx, idx + length, arrUni[i]);
				
		
				idx = buffer.indexOf(arrAscii[i]); 
				
			}
			
		}		
		
		}catch(Exception ex){
			
			
		}
		
		return buffer.toString();
		
	}
	
	private String ToUniChar( String ch){
		

		String[] arrUni = new String[]{"�"};
		String[] arrAscii = new String[]{"&pound;"};
		
		for(int i= 0; i<arrUni.length; i++){
			
			if(ch.equals(arrAscii[i])){
				
				return arrUni[i];
				
			}
			
		}
		
		return ch;	
		
	}
	

	// check Password validations
	private boolean checkPasswordValidation(HttpServletRequest request) {
		if (!requestNewPassword.equalsIgnoreCase(requestConfirmPassword)) {
			responseMessage = "The new passwords do not match,"
				+ " text entered in CONFIRM NEW PASSWORD does not"
				+ " match text entered in NEW PASSWORD.";
			return false;
		}

		String temp = requestUserName;
		if (requestUserName.contains(SystemAccount.webUnitName)) {
			temp = requestUserName.replace(SystemAccount.webUnitName, "");
			ninoName = (String) request.getSession().getAttribute("Nino");
		}
		if (requestNewPassword.indexOf(temp) >= 0) {
			responseMessage = "Your password must not contain your username.";
			return false;
		}
		
		// Checks if the entered password is matched with the NINO
		
		if ((ninoName != null) && (requestNewPassword.indexOf(ninoName) >= 0)) {
			responseMessage = "Your password must not contain your"
				+ " National Insurance Number.";
			return false;
		}
		
		PensionLinePasswordHandler passwordHandler = new PensionLinePasswordHandler();
		try {
			passwordHandler.validatePassword(requestNewPassword);
		}catch(Exception ex){
			responseMessage=ex.getMessage();
			return false;			
		}
		
		return true;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
