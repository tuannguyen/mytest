package com.bp.pensionline.update;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.opencms.file.CmsUser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.ClearMemberCache;
import com.bp.pensionline.util.SystemAccount;


public class NewsLetterHandler extends HttpServlet {

	private String requestDeliveryType;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String responseMessage = "FALSE";
		
		String requestXml = request.getParameter(Constant.PARAM);

		String bGroup = null;
		String refNo = null;
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
		}
		
		if (requestXml == null || bGroup == null || refNo == null) {
			responseMessage = "FALSE";
		} else
		{
			if (parseRequestXml(requestXml)) {
				try {
					if (UpdateSubscribeOption(bGroup, refNo,
							requestDeliveryType)) {					
						responseMessage = "TRUE";
						ClearMemberCache.clearCache(currentUser);
					}
				} catch (Exception ex) {
					responseMessage = "FALSE";
				}
			}
		}
				
		response.getWriter().print(responseMessage);

	}

	// get parameter from RequestXML
	private Boolean parseRequestXml(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equalsIgnoreCase(
							Constant.SUBCRIBEOPTION_NOTE)) {
						requestDeliveryType = node.getTextContent();
					}
				}
			}
			bais.close();

		} catch (Exception ex) {
			requestDeliveryType = "";
			return false;
		}
		
		return true;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

	private Boolean UpdateSubscribeOption(String bGroup, String refNo,
			String new_delivery_type) {

//		String updateQuery = "UPDATE BASIC ba SET BD48X='" + new_delivery_type
//				+ "' WHERE ba.bgroup='" + bGroup + "' and ba.refno='" + refNo
//				+ "'";
		String updateQuery = "UPDATE BASIC ba SET BD57X='" + new_delivery_type
		+ "' WHERE ba.bgroup='" + bGroup + "' and ba.refno='" + refNo
		+ "'";		
		
		Connection conn = getConnection();

		try {
			if (conn != null) {
				Statement stmt = conn.createStatement();
				stmt.execute(updateQuery);
				DBConnector connector = DBConnector.getInstance();
				connector.close(conn);//conn.close();
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();		
			return false;
		}		
		return false;
	}

	private Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {
			DBConnector dbConn = DBConnector.getInstance();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}
}
