package com.bp.pensionline.update;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;

import java.sql.PreparedStatement;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;

import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 *
 */
public class WantTo extends HttpServlet {

	private String requestUpdateName;
	private String requestUpdateValue;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(Constant.CONTENT_TYPE);

		String requestXml = request.getParameter(Constant.PARAM);


		String bGroup = null;
		String refNo = null;
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
		}	

		if (requestXml != null && bGroup !=null && refNo != null) {
			try {
				parseRequestXml(requestXml);//parse XML Request	
				System.out.println(requestXml);
				boolean isUpdated = false;
				String state = "false";
				try {
					CmsObject cms = SystemAccount.getAdminCmsObject();
					/*
					Map map = cms.getConfigurations();
					String key = "testHarness.enable";
					state = map.get(key).toString();
					*/
					state = CheckConfigurationKey.getStringValue("testHarness.enable");
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				if (state.equals("true")) {
					// testHarness.enable = TRUE
					String fileName = Environment.XMLFILE_DIR + "Member_" + bGroup + "_" + refNo + ".xml";

					CmsObject obj = SystemAccount.getAdminCmsObject();
					boolean boo = obj.existsResource(fileName);
					if (boo) {

						XmlReader reader = new XmlReader();
						ByteArrayInputStream bIn = new ByteArrayInputStream(reader.readFile(fileName));
						DocumentBuilderFactory Domfactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder builder = Domfactory.newDocumentBuilder();
						Document document = null;
						document = builder.parse(bIn);						
						
						NodeList rootList = document.getElementsByTagName(requestUpdateName);
						
						if (rootList != null) {
							
							Node root = rootList.item(0);
							if (root.getNodeType() == Node.ELEMENT_NODE) {


								NodeList parList = document.getElementsByTagName("ContributoryOptionChangePending");
								Node par = parList.item(0);
								if (root.getNodeType() == Node.ELEMENT_NODE) {
									NodeList cList = par.getChildNodes();
									Node cNode = cList.item(0);
									cNode.setTextContent("true");
								}
								else{										
								}
								

								NodeList memList = root.getChildNodes();
								Node node = memList.item(0);
								node.setTextContent(requestUpdateValue);

								com.bp.pensionline.test.BookmarkMember.delXML(fileName);
								com.bp.pensionline.test.BookmarkMember.toXML(document, fileName);								
								isUpdated = true;
								
							} else {

							}
						} else {

						}
					} else {

					}
				} else {
					// testHarness.enable = FALSE
					Connection conn = getConnection();// get Connection

					try {
						if (conn != null) {
							int i = 0;
							/*
							 * Still waiting for database table's information - require ContributoryOptionChangePending
							 */
							PreparedStatement stmt = conn.prepareStatement("UPDATE BASIC SET CA26X = ?" +
									", ContributoryOptionChangePending = true where bgroup = ? and refno = ?");								
							i = stmt.executeUpdate();
							
							if (i > 0) {
								isUpdated = true;
								System.out.println("Number of Row(s) Effected:" + i);
							} else {
								System.out.println("------- NO row Effected-------");
							}
							
							stmt.close();
							DBConnector connector = DBConnector.getInstance();
							connector.close(conn);//conn.close();
						}
					} 
					catch (Exception ex) {
						ex.printStackTrace();
					}
				}

				if (isUpdated == true) {					
					response.getWriter().print("true");
				} else {					
					response.getWriter().print("false");
				}
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}	
	
	private void parseRequestXml(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					
					if (node.getNodeName().equals(Constant.REQUESTED_TAG)) {
						requestUpdateName = node.getTextContent();
						System.out.println(requestUpdateName);
					}					
					if (node.getNodeName().equals(Constant.UPDATE_FIELD)) {
						requestUpdateValue = node.getTextContent();
						System.out.println(requestUpdateValue);
					}
				}
			}
			bais.close();

		} catch (Exception ex) {
			requestUpdateName = "";
			requestUpdateValue = "";

		}
	}

	/**
	 * get Connection
	 * 
	 * @return conn
	 * 
	 */
	private Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {

			DBConnector dbConn = DBConnector.getInstance();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
