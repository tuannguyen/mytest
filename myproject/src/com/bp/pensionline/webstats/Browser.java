package com.bp.pensionline.webstats;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


/**
 * @author Huy Tran
 * 
 * This class provide easy to use methods to get information of browser that users use to access PensionLine.
 */
public class Browser
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	protected HttpServletRequest request;

	protected HttpSession session;
	
	protected String sessionId;

	protected String userAgent = "";

	protected String company = "";

	protected String name = "";

	protected String version = "";

	protected String os = "";
	
	protected String osName = "";
	
	protected String osVersion = "";	
	
	protected String referrer = "";
	
	protected boolean isJavascriptOn = true;
	
	

	public Browser(HttpServletRequest request)
	{
		super();
		this.request = request;
		if (request != null)
		{
			session = request.getSession();
			sessionId = session.getId();
			userAgent = request.getHeader("User-Agent");
			//LOG.info("Browser User-Agent: " + userAgent);
			// processOS before process Browser
			processOS(userAgent);
			
			processBrowser(userAgent);
			
			setReferrer();
			
			if (osName.equals("Windows")) {
				if (osVersion.equals("NT 5.0")) {
					osVersion = "2000";
				}
				if (osVersion.equals("NT 5.1")) {
					osVersion = "XP";
				}
				if (osVersion.equals("NT 5.2")) {
					osVersion = "2003";
				}
				if (osVersion.equals("NT 6.0")) {
					osVersion = "Vista";
				}
			}				
		}
		
	}
	
	public Browser(HttpServletRequest request, String sessionID) {

		this.request = request;	
		if (request != null)
		{
			session = request.getSession();
			userAgent = request.getHeader("User-Agent");
			
			//LOG.info("Browser User-Agent: " + userAgent);
			
			// processOS before process Browser
			processOS(userAgent);
			
			processBrowser(userAgent);
			
			setReferrer();
			
			if (osName.equals("Windows")) {
				if (osVersion.equals("NT 5.0")) {
					osVersion = "2000";
				}
				if (osVersion.equals("NT 5.1")) {
					osVersion = "XP";
				}
				if (osVersion.equals("NT 5.2")) {
					osVersion = "2003";
				}
				if (osVersion.equals("NT 6.0")) {
					osVersion = "Vista";
				}
			}		
		}
		
		this.sessionId = sessionID;
	}	
	
	/**
	 * Process browser data from browser User-Agent string
	 * @param browserStr
	 */
	public void processOS (String browserStr)
	{
		if (browserStr == null)
		{
			return;
		}
		browserStr = browserStr.toLowerCase();
		// Smartphone OS
		// Blackberry		
		if (contains(browserStr, "blackberry"))
		{
			osVersion = "Blackberry";
			osName = "Smartphone";
		}	
		// Sony Ericsson		
		if (contains(browserStr, "ericsson"))
		{
			osVersion = "Sony Ericsson";
			osName = "Smartphone";
		}	
		// Nokia
		if (contains(browserStr, "nokia"))
		{
			osVersion = "Nokia";
			osName = "Smartphone";
		}
		// Symbian
		if (contains(browserStr, "symbianos/([\\d|.]*)"))
		{
			osVersion = versionMatch(browserStr, "symbianos/([\\d|.]*)");
			osName = "Symbian";
		}
		// SunOS
		if (contains(browserStr, "sunos[ |/]([\\d|.]*)"))
		{
			osVersion = versionMatch(browserStr, "sunos[ |/]([\\d|.]*))");
			osName = "SunOS";
		}
		// AmigaOS
		if (contains(browserStr, "amiga"))
		{
			osName = "AmigaOS";
		}
		// AmigaOS
		if (contains(browserStr, "amigaos([\\d|.]*)"))
		{
			osVersion = versionMatch(browserStr, "amigaos([\\d|.]*)");
			osName = "AmigaOS";
		}	
		// Linux
		if (contains(browserStr, "linux"))
		{
			osName = "Linux";
		}
		// Linux
		if (contains(browserStr, "linux[ |/]([\\d|.]*)"))
		{
			osVersion = versionMatch(browserStr, "linux[ |/]([\\d|.]*)");
			osName = "Linux";
		}	
		// Linux
		if (contains(browserStr, "(free|open|net)bsd"))
		{
			osVersion = versionMatch(browserStr, "(free|open|net)bsd");
			osName = "BSD";
		}
		// Debian first as ubunto is a clone
		if (contains(browserStr, "debian"))
		{
			osVersion = "Debian";
			osName = "Linux";
		}
		if (contains(browserStr, "ubuntu"))
		{
			osVersion = "Ubuntu";
			osName = "Linux";
		}
		if (contains(browserStr, "suse"))
		{
			osVersion = "Suse";
			osName = "Linux";
		}
		if (contains(browserStr, "fedora"))
		{
			osVersion = "Fedora";
			osName = "Linux";
		}
		if (contains(browserStr, "gentoo"))
		{
			osVersion = "Gentoo";
			osName = "Linux";
		}
		// OS/2
		if (contains(browserStr, "os/2"))
		{
			osVersion = "";
			osName = "OS/2";
			if (contains(browserStr, "(warp [\\d|.]*)"))
			{
				osVersion = versionMatch(browserStr, "(warp [\\d|.]*)");
			}			
		}
		// MacOS
		if (contains(browserStr, "macintosh"))
		{
			osName = "MacOS";
		}
		if (contains(browserStr, "mac_powerpc"))
		{
			osName = "MacOS";
		}
		if (contains(browserStr, "mac os ([\\d|.|\\w]*)"))
		{
			osVersion = versionMatch(browserStr, "mac os ([\\d|.|\\w]*)").toUpperCase();
			osName = "MacOS";
		}
		if (contains(browserStr, "windows"))
		{
			osName = "Windows";
		}
		if (contains(browserStr, "win ?(9[5|8|x]|nt3\\.51|nt4\\.0)"))
		{
			osVersion = versionMatch(browserStr, "win ?(9[5|8|x]|nt3\\.51|nt4\\.0)");
			osName = "Windows";
		}
		if (contains(browserStr, "win ?9x 4\\.9"))
		{
			osVersion = "ME";
			osName = "Windows";
		}	
		if (contains(browserStr, "windows (3\\.1[0|1]?|ce|95|98|me|xp|2000|2003)"))
		{
			osVersion = versionMatch(browserStr, "windows (3\\.1[0|1]?|ce|95|98|me|xp|2000|2003)").toUpperCase();
			osName = "Windows";
		}	
		if (contains(browserStr, "windows (nt[ ]?[\\d|.|\\w]*)"))
		{
			osVersion = versionMatch(browserStr, "windows (nt[ ]?[\\d|.|\\w]*)").toUpperCase();
			osName = "Windows";
		}		
	}
	
	/**
	 * Process browser data from browser User-Agent string
	 * @param browserStr
	 */
	public void processBrowser (String browserStr)
	{
		if (browserStr == null)
		{
			return;
		}
		browserStr = browserStr.toLowerCase();
		// Nearly everything starts here, but if there is nothing else, then its
		// representing netscape of some sort.		
		if (contains(browserStr, "mozilla/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "mozilla/([\\d|.]*)");
			name = "Mozilla";
		}
		// a lot of things fake being MSIE, so start here and specialise later.
		if (contains(browserStr, "msie[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "msie[ |/]([\\d|.]*)");
			name = "MSIE";
		}
		// Pocket IE
		if (contains(browserStr, "mspie[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "mspie[ |/]([\\d|.]*)");
			name = "MSPIE";
		}
		// MSIE...Win CE
		if (contains(browserStr, "msie[ |/]([\\d|.]*).*WIN.[^;]*CE"))
		{
			version = versionMatch(browserStr, "msie[ |/]([\\d|.]*).*WIN.[^;]*CE");
			name = "MSPIE";
		}	
		// Opera
		if (contains(browserStr, "opera[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "opera[ |/]([\\d|.]*)");
			name = "Opera";
		}	
		// Netscape	
		if (contains(browserStr, "netscape[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "netscape[ |/]([\\d|.]*)");
			name = "Netscape";
		}		
		// Safari
		if (contains(browserStr, "safari/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "safari/([\\d|.]*)");
			name = "Safari";
		}
		// firefox
		if (contains(browserStr, "firefox[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "firefox[ |/]([\\d|.]*)");
			name = "Firefox";
		}	
		// Firebird
		if (contains(browserStr, "firebird[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "firebird[ |/]([\\d|.]*)");
			name = "Firebird";
		}	
		// Galeon
		if (contains(browserStr, "galeon"))
		{
			name = "Galeon";
		}	
		// Galeon with version
		if (contains(browserStr, "galeon[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "galeon[ |/]([\\d|.]*)");
			name = "Galeon";
		}
		// Camino
		if (contains(browserStr, "camino/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "camino/([\\d|.]*)");
			name = "Camino";
		}
		// Konqueror
		if (contains(browserStr, "konqueror[ |/]([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "konqueror[ |/]([\\d|.]*)");
			name = "Konqueror";
		}
		// put in some hand held stuff		
		// Blackberry
		if (contains(browserStr, "blackberry[\\d|.]*/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "blackberry[\\d|.]*/([\\d|.]*)");
			name = "Blackberry";
		}	
		// Nokia series 60
		if (contains(browserStr, "nokia[\\d|.]*/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "nokia[\\d|.]*/([\\d|.]*)");
			name = "Nokia Series 60";
		}	
		
		// Put in a few search bots
		// Wget
		if (contains(browserStr, "wget/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "wget/([\\d|.]*)");
			name = "wget";
			osName = "";
			osVersion = "";
		}
		// HTTrack
		if (contains(browserStr, "httrack ([\\d|.|x]*)"))
		{
			version = versionMatch(browserStr, "httrack ([\\d|.|x]*)");
			name = "HTTrack";
			osName = "";
			osVersion = "";			
		}
		// Google bot
		if (contains(browserStr, "googlebot"))
		{
			version = "";
			name = "Google bot";
			osName = "";
			osVersion = "";		
		}
		if (contains(browserStr, "googlebot/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "googlebot/([\\d|.]*)");
			name = "Google bot";
			osName = "";
			osVersion = "";		
		}	
		// Yahoo MMCrawler bot
		if (contains(browserStr, "yahoo-mmcrawler/([\\d|.|x]*)"))
		{
			version = versionMatch(browserStr, "yahoo-mmcrawler/([\\d|.|x]*)");
			name = "Yahoo MMCrawler bot";
			osName = "";
			osVersion = "";		
		}
		// Seek bot
		if (contains(browserStr, "seekbot/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "seekbot/([\\d|.]*)");
			name = "Seek bot";
			osName = "";
			osVersion = "";		
		}
		// Open bot
		if (contains(browserStr, "openbot/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "openbot/([\\d|.]*)");
			name = "Open bot";
			osName = "";
			osVersion = "";		
		}		
		// Yahoo Slurp bot
		if (contains(browserStr, "yahoo! slurp"))
		{
			version = "";
			name = "Yahoo Slurp bot";
			osName = "";
			osVersion = "";		
		}
		// MSN bot
		if (contains(browserStr, "msnbot/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "msnbot/([\\d|.]*)");
			name = "MSN bot";
			osName = "";
			osVersion = "";			
		}
		// Kompass bot
		if (contains(browserStr, "kompassbot/([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "kompassbot/([\\d|.]*)");
			name = "Kompass bot";
			osName = "";
			osVersion = "";		
		}
		// Ask Jeeves bot
		if (contains(browserStr, "ask jeeves/teoma"))
		{
			version = "";
			name = "Ask Jeeves bot";			
		}
		// Snap preview bot
		if (contains(browserStr, "snappreviewbot"))
		{
			version = "";
			name = "Snap preview bot";			
		}		
		// MS Frontpage
		if (contains(browserStr, "ms frontpage ([\\d|.]*)"))
		{
			version = versionMatch(browserStr, "ms frontpage ([\\d|.]*))");
			name = "MS Frontpage";		
		}
		
		// Tidy up a bit.
		if (osName != null && osName.equals("Windows") && 
				osVersion != null && osVersion.equals("CE") && 
				name != null && name.equals("MSIE")) 
		{
			name = "MSPIE";
		}		
		
	}
	
	/**
	 * Set referer page that the request is triggered
	 *
	 */
	private void setReferrer()
	{
		if (request != null && request.getHeader("Referer") != null)
		{
			referrer = request.getHeader("Referer");
		}
		else
		{
			referrer = "Unknown";
		}
	}	
	
	/**
	 * Return a browser match the regex input
	 * @return
	 */
	private String versionMatch (String browserStr, String regex)
	{
		if (browserStr != null && regex != null)
		{
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(browserStr);
			if (matcher.find())
			{
				String versionStr =  matcher.group();
				if (versionStr.indexOf("/") != -1)
				{
					return versionStr.substring(versionStr.indexOf("/") + 1);
				}
				else if (versionStr.indexOf(" ") != -1)
				{
					return versionStr.substring(versionStr.indexOf(" ") + 1);
				}
				else
				{
					return versionStr;
				}
			}				
		}
		
		return "";
	}
	
	private boolean contains (String str, String regex)
	{
		if (str != null && regex != null)
		{
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(str);
			if (matcher.find())
			{
				return true;
			}				
		}
		return false;
		
	}
	
	public String getSessionId()
	{
		if (sessionId == null)
		{
			return "";
		}
		return sessionId;
	}	

	/*
	 * Simply returns the name of the browser
	 */
	public String getCompany()
	{
		return name;
	}

	public boolean isJavascriptOn()
	{
		return isJavascriptOn;
	}

	public String getName()
	{
		return name;
	}

	public String getOs()
	{
		os = osName + " " + osVersion;
		return os;
	}

	public String getReferrer()
	{
		return referrer;
	}

	public String getUserAgent()
	{
		if (userAgent == null)
		{
			return "";
		}
		return userAgent;
	}

	public String getVersion()
	{
		return version;
	}
}
