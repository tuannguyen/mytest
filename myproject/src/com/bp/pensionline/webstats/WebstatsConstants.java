package com.bp.pensionline.webstats;

/**
 * 
 * @author Tu Nguyen
 * @version 1.0
 * @since 25/06/2007
 * 
 */

/**
 * @author SonNT
 * @version 2.0
 * @since 27/06/2007 
 * 
 * 	@author Tu Nguyen
 * 	@version 3.0
 */

public class WebstatsConstants {
	
	// define all the static references for webstats package
	/** TODO complete the rest of all references */
	
	public static final String BP_STATS_AUTH = "BP_STATS_AUTH";
	public static final String BP_STATS_UNAUTH = "BP_STATS_UNAUTH";
	public static final String BP_STATS_POS = "BP_STATS_POS";
	public static final String BP_STATS_LOGOUT = "BP_STATS_LOGOUT";
	public static final String BP_STATS_SEARCH = "BP_STATS_SEARCH";
	public static final String BP_STATS_ACCESS = "BP_STATS_ACCESS";
	
	public static final String SEQNO = "SEQNO";	
	public static final String SESSIONID="SESSIONID";	
	
	public static final String EVENT_TIME = "EVENT_TIME";
	public static final String EVENT_TIME_MS = "EVENT_TIME_MS";
	
	public static final String EVENT_YEAR = "EVENT_YEAR";
	public static final String EVENT_MONTH = "EVENT_MONTH";
	public static final String EVENT_DAY = "EVENT_DAY";
	public static final String EVENT_DATE = "EVENT_DATE";
	
	public static final String EVENT_SEC = "EVENT_SEC";
	public static final String EVENT_MIN = "EVENT_MIN";	
	public static final String EVENT_HOUR = "EVENT_HOUR";
	
	public static final String BROWSER_STRING = "BROWSER_STRING";
	public static final String JAVASCRIPT_ON = "JAVASCRIPT_ON";
	public static final String BROWSER_TYPE = "BROWSER_TYPE";
	public static final String BROWSER_NAME = "BROWSER_NAME";
	public static final String BROWSER_VERSION= "BROWSER_VERSION";
	public static final String BROWSER_OS = "BROWSER_OS";
	
	public static final String OUT_COME = "OUT_COME";
	public static final String SUPER_USER = "SUPER_USER";
	public static final String USER_TYPE = "USER_TYPE";
	
	
	public static final String PAGE_SEQNO = "PAGE_SEQNO";
	public static final String SEARCH_SEQNO = "SEARCH_SEQNO";
	public static final String UNAUTH_SEQNO = "UNAUTH_SEQNO";
	public static final String AUTH_SEQNO = "AUTH_SEQNO";
	public static final String POS_SEQNO = "POS_SEQNO";
	
	public static final String SEARCH_STRING = "SEARCH_STRING";
	public static final String RESULT_COUNT  = "RESULT_COUNT";
	public static final String CONTENT_GROUP  = "CONTENT_GROUP";
	public static final String URI  = "URI";
	public static final String REFERRER  = "REFERRER";
	public static final String PAGE_SIZE  = "PAGE_SIZE";
	
	
	public static final String DURATION = "DURATION";
	public static final String AGENT = "AGENT";
	
	public static final String COMPANY = "COMPANY";
	public static final String MEMBER_SCHEME = "MEMBER_SCHEME";
	public static final String MEMBER_STATUS = "MEMBER_STATUS";
	public static final String GENDER = "GENDER";
	public static final String MARITAL_STATUS = "MARITAL_STATUS";
	public static final String AGE_RANGE = "AGE_RANGE";
	public static final String SALARY = "SALARY";
	public static final String LOCATION = "LOCATION";
	public static final String OVERSEAS = "OVERSEAS";
	
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";
	
	public static long UNAUTHNO = 0;
  
}
