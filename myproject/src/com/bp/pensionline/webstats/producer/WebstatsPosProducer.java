package com.bp.pensionline.webstats.producer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.webstats.consumer.WebstatsPosConsumer;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class WebstatsPosProducer extends WebstatsProducer{
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	/* 
	 * Start the statistics by thread
	 */
		
	/**
	 * @param paramRequest
	 */
	public void runStats(HttpServletRequest paramRequest) {

		try {
			
			if (paramRequest != null) {
				WebstatsPosConsumer thread = new WebstatsPosConsumer();
				thread.setParam(paramRequest);
				thread.start();
			}			

		} catch (Exception ex) {
			
			ex.printStackTrace();
			LOG.error("com.bp.pensionline.webstats.producer.WebstatsPosProducer.runStats error: ",ex);
			
		}

	}
	
	/*
	 * Default Java Bean Constructor
	 */
	public WebstatsPosProducer() {
		super();

	}
}
