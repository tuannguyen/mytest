package com.bp.pensionline.webstats.consumer;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tu Nguyen
 *
 */
public abstract class WebstatsConsumer extends Thread{
	
	  public abstract void setParam(HttpServletRequest requestCopy);
	  public abstract void run();

	  //public abstract void storeInfoOnSession();

	  //public abstract MemberDao calculate(Date DoR, int accrual_rate, double cash);
	  //public abstract MemberDao runCalculate(Date DoR, int accrual_rate, double cash);
	  //public abstract MemberDao runCalculate(Date DoR);

	  //DC calculation
	  //public abstract MemberDao calculateDC();
	  //WC calculation
	  //public abstract MemberDao calculateWC(); 
	  //public abstract void deleteAdministratorLocks(String refNo, String bGroup);

}
