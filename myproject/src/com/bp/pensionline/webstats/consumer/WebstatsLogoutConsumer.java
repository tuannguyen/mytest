package com.bp.pensionline.webstats.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.util.Mapper;
import com.bp.pensionline.webstats.WebstatsConstants;
import com.bp.pensionline.webstats.WebstatsSQLHandler;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class WebstatsLogoutConsumer extends WebstatsConsumer {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	HttpServletRequest request = null;
	HttpSession session = null;
	Mapper map = null;

	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#setParam(com.bp.pensionline.dao.MemberDao, javax.servlet.http.HttpServletRequest)
	 */
	public void setParam(HttpServletRequest paramRequest) {

		this.request = paramRequest;
		this.session = paramRequest.getSession();
		this.map = new Mapper();
	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#run()
	 */
	public void run(){
		
		try{			
			//Get params from servlet
			String sessionID = session.getId();
			long authNo = 0;
			if(session.getAttribute(WebstatsConstants.AUTH_SEQNO) != null){
				authNo = (Long)session.getAttribute(WebstatsConstants.AUTH_SEQNO);	
			}
			
			long posNo = 0;			
			if(session.getAttribute(WebstatsConstants.POS_SEQNO) != null){
				posNo = (Long)session.getAttribute(WebstatsConstants.POS_SEQNO);
			}
			
			long pageNo = (Long)session.getAttribute(WebstatsConstants.PAGE_SEQNO);
			
			//Set into map
			Mapper map = new Mapper();
			
			map.set(WebstatsConstants.SESSIONID, sessionID);			
			map.set(WebstatsConstants.AUTH_SEQNO, authNo);					
			map.set(WebstatsConstants.POS_SEQNO, posNo);
			
			//update duration if posNo present in session
			if(posNo > 0){
				
				WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_POS, posNo);
				
			}
			
			//update duration if authNo present in session
			if(authNo > 0){
				
				WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_AUTH, authNo);
				
			}
			
			//update duration if pageNo present in session
			if(pageNo > 0){
				
				WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_ACCESS, pageNo);
				
			}
			
			WebstatsSQLHandler.insertStatsLogout(map);
			
			
		} 
		catch(Exception e) 
		{
			//e.printStackTrace();
			LOG.error("com.bp.pensionline.webstats.consumer.WebstatsLogoutConsumer.run error: ", e);

		}
		
	}
}
