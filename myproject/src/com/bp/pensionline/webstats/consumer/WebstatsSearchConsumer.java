package com.bp.pensionline.webstats.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.util.Mapper;
import com.bp.pensionline.webstats.WebstatsConstants;
import com.bp.pensionline.webstats.WebstatsSQLHandler;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class WebstatsSearchConsumer extends WebstatsConsumer {

public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	HttpServletRequest request = null;
	HttpSession session = null;
	Mapper map = null;

	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#setParam(com.bp.pensionline.dao.MemberDao, javax.servlet.http.HttpServletRequest)
	 */
	public void setParam(HttpServletRequest paramRequest) {

		this.request = paramRequest;
		this.session = paramRequest.getSession();
		this.map = new Mapper();
	}
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#run()
	 */
	public void run(){
		try{
			
			//Get params from servlet
			String sessionID = session.getId();
			
			long authNo = 0;
			if(session.getAttribute(WebstatsConstants.AUTH_SEQNO) != null){
				authNo = (Long)session.getAttribute(WebstatsConstants.AUTH_SEQNO);	
			}
			
			long posNo = 0; 
			if(session.getAttribute(WebstatsConstants.POS_SEQNO) != null){
				posNo = (Long)session.getAttribute(WebstatsConstants.POS_SEQNO);
			}	
			
			String searchString = (String)session.getAttribute(WebstatsConstants.SEARCH_STRING);
			int resultCount = Integer.parseInt((String)session.getAttribute(WebstatsConstants.RESULT_COUNT));

			//Set into map
			Mapper map = new Mapper();
			
			map.set(WebstatsConstants.SESSIONID, sessionID);			
			map.set(WebstatsConstants.AUTH_SEQNO, authNo);					
			map.set(WebstatsConstants.POS_SEQNO, posNo);
			map.set(WebstatsConstants.SEARCH_STRING, searchString);
			map.set(WebstatsConstants.RESULT_COUNT, resultCount);
			
			//reset searchNo
			long searchNo = WebstatsSQLHandler.insertStatsSearch(map);
			session.removeAttribute(WebstatsConstants.SEARCH_SEQNO);
			session.setAttribute(WebstatsConstants.SEARCH_SEQNO, searchNo);
			
			
		} catch(Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.webstats.consumer.WebstatsSearchConsumer.run error: ",e);

		}
		
	}
}
