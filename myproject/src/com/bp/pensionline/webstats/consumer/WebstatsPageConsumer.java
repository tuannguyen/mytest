package com.bp.pensionline.webstats.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import com.bp.pensionline.webstats.Browser;
import com.bp.pensionline.util.GetContentGroup;
import com.bp.pensionline.util.Mapper;
import com.bp.pensionline.util.TextUtil;

import com.bp.pensionline.webstats.WebstatsConstants;
import com.bp.pensionline.webstats.WebstatsSQLHandler;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 *	@author Tu Nguyen
 *	@version 2.0
 */
public class WebstatsPageConsumer extends WebstatsConsumer {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
		
	HttpServletRequest request = null;
	HttpSession session = null;
	Mapper map = null;

	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#setParam(com.bp.pensionline.dao.MemberDao, javax.servlet.http.HttpServletRequest)
	 */
	public void setParam(HttpServletRequest paramRequest) {

		this.request = paramRequest;
		this.session = paramRequest.getSession();
		this.map = new Mapper();

	}
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#run()
	 */
	public void run(){
		try {
			
			//Get params from servlet
			Browser browser = new Browser(request);
			
			String sessionID = session.getId();
			
			
			
			String path = request.getPathInfo();
			if(path == null || path.compareTo("/") == 0){
				path = "/index.html";
			}

			long authNo = 0;
			if(session.getAttribute(WebstatsConstants.AUTH_SEQNO) != null){
				authNo = (Long)session.getAttribute(WebstatsConstants.AUTH_SEQNO);
			}
			
			long unAuthNo = 0;
			if(session.getAttribute(WebstatsConstants.UNAUTH_SEQNO) != null){
				unAuthNo = (Long)session.getAttribute(WebstatsConstants.UNAUTH_SEQNO);
			}
			
			long posNo = 0;
			if(session.getAttribute(WebstatsConstants.POS_SEQNO) != null){
				posNo = (Long)session.getAttribute(WebstatsConstants.POS_SEQNO);
			}
			
			long searchNo = 0;
			if(session.getAttribute(WebstatsConstants.SEARCH_SEQNO) != null){
				searchNo = (Long)session.getAttribute(WebstatsConstants.SEARCH_SEQNO);
				session.removeAttribute(WebstatsConstants.SEARCH_SEQNO);
				//System.out.println("SEARCH SEQNO *************"+ searchNo);
			}
			
			String contGroup = GetContentGroup.getContentGroup(path);
			String uri = path;
			String referrer = browser.getReferrer();
			long pageSize = TextUtil.getPagesize(path);
			
			
			
						
			
			
			//Set into map
			Mapper map = new Mapper();
			
			map.set(WebstatsConstants.SESSIONID, sessionID);
			
			map.set(WebstatsConstants.AUTH_SEQNO, authNo);
			map.set(WebstatsConstants.UNAUTH_SEQNO, unAuthNo);					
			map.set(WebstatsConstants.POS_SEQNO, posNo);			
			map.set(WebstatsConstants.SEARCH_SEQNO, searchNo);			
			
			map.set(WebstatsConstants.CONTENT_GROUP, contGroup);			
			map.set(WebstatsConstants.URI, uri);
			map.set(WebstatsConstants.REFERRER, referrer);
			map.set(WebstatsConstants.PAGE_SIZE, pageSize);			
			
			String searchStr = (String)session.getAttribute(WebstatsConstants.SEARCH_STRING);
			
			long pageNo = 0;
			if(session.getAttribute(WebstatsConstants.PAGE_SEQNO) != null){
				pageNo = (Long)session.getAttribute(WebstatsConstants.PAGE_SEQNO);
			}
			
			
			//set unAuthNo
			if(authNo == 0 && unAuthNo == 0){
				
				map.set(WebstatsConstants.BROWSER_STRING, browser.getUserAgent());
				map.set(WebstatsConstants.JAVASCRIPT_ON, browser.isJavascriptOn());
				map.set(WebstatsConstants.BROWSER_TYPE, browser.getCompany());
				map.set(WebstatsConstants.BROWSER_NAME, browser.getName());
				map.set(WebstatsConstants.BROWSER_VERSION, browser.getVersion());
				map.set(WebstatsConstants.BROWSER_OS, browser.getOs());
				
				unAuthNo = WebstatsSQLHandler.insertStatsUnauth(map);
				session.setAttribute(WebstatsConstants.UNAUTH_SEQNO, unAuthNo);
				WebstatsConstants.UNAUTHNO = unAuthNo;
				//LOG.info("Insert new unauthenticate statistic record!");				
			}
			
			
			
			//remove search string
			if(searchStr != null){
				request.getSession().removeAttribute(WebstatsConstants.SEARCH_STRING);
			}
			
			//if pageNo present in session, update duration and remove previous pageNo
			if(pageNo > 0){
				
				WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_ACCESS, pageNo);
				session.removeAttribute(WebstatsConstants.PAGE_SEQNO);
				//LOG.info("Update duration statistic record!");
			}			
			
			//update new pageNo
			long newPageNo = WebstatsSQLHandler.insertStatsAccess(map);
			session.setAttribute(WebstatsConstants.PAGE_SEQNO, newPageNo);
						

		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.webstats.producer.WebstatsPageProducer.runStats error: ",ex);
			ex.printStackTrace();
		}
	}
	
}
