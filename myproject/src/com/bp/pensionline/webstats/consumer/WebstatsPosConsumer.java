package com.bp.pensionline.webstats.consumer;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.handler.CompanySchemeRawHandler;
import com.bp.pensionline.util.Mapper;
import com.bp.pensionline.util.ObjectCloner;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.util.TextUtil;
import com.bp.pensionline.webstats.WebstatsConstants;
import com.bp.pensionline.webstats.WebstatsSQLHandler;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 *	@author Tu Nguyen
 *	@version 2.0
 */
public class WebstatsPosConsumer extends WebstatsConsumer {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	

	HttpServletRequest request = null;
	HttpSession session = null;
	Mapper map = null;
	
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#setParam(javax.servlet.http.HttpServletRequest)
	 */
	public void setParam(HttpServletRequest paramRequest) {
		try {
			this.request = paramRequest;
			this.session = paramRequest.getSession();
			map = new Mapper();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	public void run(){
		try{
			//Get params from servlet
			String sessionID = session.getId();
			
			long authNo = 0;
			
			if(session.getAttribute(WebstatsConstants.AUTH_SEQNO) != null){
				authNo = (Long)session.getAttribute(WebstatsConstants.AUTH_SEQNO);
			}
			
			//CmsUser cmsUser=SystemAccount.getCurrentUser(sessionID);
			CmsUser cmsUser=SystemAccount.getCurrentUser(request);
			if(cmsUser.getAdditionalInfo(Environment.MEMBER_KEY) != null){
				MemberDao memberCopy=(MemberDao)cmsUser.getAdditionalInfo(Environment.MEMBER_KEY);			
				MemberDao memberDao = (MemberDao)ObjectCloner.deepCopy(memberCopy);
				
				String bGroup = memberDao.get(Environment.MEMBER_BGROUP);
				String refno = memberDao.get(Environment.MEMBER_REFNO);
				String schemeRaw = memberDao.get(Environment.MEMBER_SCHEME_RAW);
				CompanySchemeRawHandler companyHandler = null;
				try{
					companyHandler = CompanySchemeRawHandler.getInstance();
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("***************************  CompanySchemeRawHandler.getInstance() ERROR");
				}
				
				String company = TextUtil.getCompany(companyHandler.getCompanyGroup(bGroup, schemeRaw));			
				
				String memScheme = memberDao.get(Environment.MEMBER_SCHEME);
				String memStatus = memberDao.get(Environment.MEMBER_STATUS);
				
				
				String gender = memberDao.get(Environment.MEMBER_GENDER);	
				if (gender == null){
					gender = "Unknown";//(Male / Female)
				}
				
				String maritalStatus = memberDao.get(Environment.MARITAL_STATUS);	
				if (maritalStatus == null){
					maritalStatus = "Other";
					//Married','Divorced','Widow(er)','Benficiary','Single','Cohabitee','Other
				}
								
				String age = TextUtil.getAge(memberDao.get(Environment.DOB));
				
								
				String salary = TextUtil.getSalary(memberDao.get(Environment.PENSIONABLESALARY));
				
				// Update by HuyTran to fix the WSPOSTCODE 
				Map<String, String> addressMap = WebstatsSQLHandler.getMemberAddress(bGroup, refno);
				
				String location = addressMap.get(Environment.WSPOSTCODE);			
				if(location == null){
					location = "";
				}
				// get the first 4 chars of postcode
				if(location.length() > 4){
					location = location.substring(0, 4);
				}				
				// We will need to define the Postcode in the sql
//				String location = memberDao.get(Environment.WSPOSTCODE);			
//				if(location == null){
//					location = "";
//				}
//				// get the first 4 chars of postcode
//				if(location.length() > 4){
//					location = location.substring(0, 4);
//				}
				
//				String overseasString = memberDao.get(Environment.OVERSEASINDICATOR);	
				String overseasString = addressMap.get(Environment.OVERSEASINDICATOR);
				boolean overseas;
				if (overseasString == null){
					overseas = false;//true/false
				}
				overseas = Boolean.parseBoolean(overseasString);
				
				//Set into map
				Mapper map = new Mapper();
				
				map.set(WebstatsConstants.SESSIONID, sessionID);		
				
				map.set(WebstatsConstants.AUTH_SEQNO, authNo);
				map.set(WebstatsConstants.COMPANY, company);
				map.set(WebstatsConstants.MEMBER_SCHEME, memScheme);
				map.set(WebstatsConstants.MEMBER_STATUS, memStatus);
				map.set(WebstatsConstants.GENDER, gender);
				map.set(WebstatsConstants.MARITAL_STATUS, maritalStatus);			
				map.set(WebstatsConstants.AGE_RANGE, age);
				map.set(WebstatsConstants.SALARY, salary);			
				map.set(WebstatsConstants.LOCATION, location);
				map.set(WebstatsConstants.OVERSEAS, overseas);
				
				long newPosNo = WebstatsSQLHandler.insertStatsPos(map);
				long posNo = 0;
				if(session.getAttribute(WebstatsConstants.POS_SEQNO) != null){
					posNo = (Long)session.getAttribute(WebstatsConstants.POS_SEQNO);
					
				}
				
				
				//remove previous posNo
				if(posNo > 0){
					
					WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_POS, posNo);
					session.removeAttribute(WebstatsConstants.POS_SEQNO);
					
				}
				
				//set new posNp
				session.setAttribute(WebstatsConstants.POS_SEQNO, newPosNo);
			}
			else{
				System.out.println("MemberDao dose not exist");
			}
			
			
			
		} catch(Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOG.error("com.bp.pensionline.webstats.consumer.WebstatsPosConsumer.run error: ",e);

		}
		
	}
}
