package com.bp.pensionline.webstats.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;

import org.opencms.main.CmsLog;

import com.bp.pensionline.webstats.Browser;
import com.bp.pensionline.util.Mapper;

import com.bp.pensionline.webstats.WebstatsConstants;
import com.bp.pensionline.webstats.WebstatsSQLHandler;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 *	@author Tu Nguyen
 *	@version 2.0
 */
public class WebstatsLoginConsumer extends WebstatsConsumer {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	

	HttpServletRequest request = null;
	HttpSession session = null;
	Mapper map = null;
	
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#setParam(javax.servlet.http.HttpServletRequest)
	 */
	public void setParam(HttpServletRequest paramRequest) {

		this.request = paramRequest;
		this.session = paramRequest.getSession();
		map = new Mapper();
	}
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.webstats.consumer.WebstatsConsumer#run()
	 */
	public void run(){
		try {
			
			//Get params from servlet			
			//Browser browser = new Browser(request);
			
			String sessionID = session.getId();

			Browser browser = new Browser(request, sessionID);

			String outcome = (String)session.getAttribute(WebstatsConstants.OUT_COME);

			
			
			String userType = (String)session.getAttribute(WebstatsConstants.USER_TYPE);
			
			
			long unauthNo = 0;
			try{
				unauthNo = (Long)session.getAttribute(WebstatsConstants.UNAUTH_SEQNO);
			}
			catch(Exception e){
				unauthNo = WebstatsConstants.UNAUTHNO;
			}
						
			
			//Set into map
			Mapper map = new Mapper();
			
			map.set(WebstatsConstants.SESSIONID, sessionID);		
			
			map.set(WebstatsConstants.BROWSER_STRING, browser.getUserAgent());
			
			map.set(WebstatsConstants.JAVASCRIPT_ON, browser.isJavascriptOn());
			map.set(WebstatsConstants.BROWSER_TYPE, browser.getCompany());
			map.set(WebstatsConstants.BROWSER_NAME, browser.getName());
			map.set(WebstatsConstants.BROWSER_VERSION, browser.getVersion());
			map.set(WebstatsConstants.BROWSER_OS, browser.getOs());
			
			map.set(WebstatsConstants.OUT_COME, outcome);
			map.set(WebstatsConstants.USER_TYPE, userType);
			
			map.set(WebstatsConstants.UNAUTH_SEQNO, unauthNo);
			
			long authNo = WebstatsSQLHandler.insertStatsAuth(map);
			
			//check if login success or faile
			if(outcome.compareTo(WebstatsConstants.SUCCESS) == 0)
			{
				LOG.info("--------------WebstatsLoginConsumer AUTH_SEQNO: " + authNo);
				WebstatsSQLHandler.updateDuration(WebstatsConstants.BP_STATS_UNAUTH, unauthNo);
				session.setAttribute(WebstatsConstants.AUTH_SEQNO, authNo);				
				session.removeAttribute(WebstatsConstants.UNAUTH_SEQNO);
			}		

		} 
		catch (Exception ex) 
		{
			LOG.error("com.bp.pensionline.webstats.producer.WebstatsLoginConsumer.run error: ", ex);			
		}
	}
}