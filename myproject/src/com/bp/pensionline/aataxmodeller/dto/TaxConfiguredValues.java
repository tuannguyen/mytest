package com.bp.pensionline.aataxmodeller.dto;

public class TaxConfiguredValues
{
    private String averageSalaryIncrease;
    private String annualAllowanceTaxRate;
    private String annualInflation;
    private String capitalisation;
    private String annualAllowance;
    private String lta;
    
    private boolean isConfigurationUsed = false;

    /**
     * Default constructor.
     */
    public TaxConfiguredValues() {
    }

    /**
     * Get the average salary.
     *
     * @return the averageSalaryIncrease
     */
    public String getAverageSalaryIncrease() {
        return averageSalaryIncrease;
    }

    /**
     * Set the average salary.
     *
     * @param averageSalaryIncrease the averageSalaryIncrease to set
     */
    public void setAverageSalaryIncrease(String averageSalaryIncrease) {
        this.averageSalaryIncrease = averageSalaryIncrease;
    }

    /**
     * Get the annual allowance tax rate.
     *
     * @return the annualAllowanceTaxRate
     */
    public String getAnnualAllowanceTaxRate() {
        return annualAllowanceTaxRate;
    }

    /**
     * Set the annual allowance tax rate.
     *
     * @param annualAllowanceTaxRate the annualAllowanceTaxRate to set
     */
    public void setAnnualAllowanceTaxRate(String annualAllowanceTaxRate) {
        this.annualAllowanceTaxRate = annualAllowanceTaxRate;
    }

    /**
     * Get the annual inflation.
     *
     * @return the annualInflation
     */
    public String getAnnualInflation() {
        return annualInflation;
    }

    /**
     * Set the annual inflation.
     *
     * @param annualInflation the annualInflation to set
     */
    public void setAnnualInflation(String annualInflation) {
        this.annualInflation = annualInflation;
    }

    /**
     * Get the capitalisation.
     *
     * @return the capitalisation
     */
    public String getCapitalisation() {
        return capitalisation;
    }

    /**
     * Set the capitalisation.
     *
     * @param capitalisation the capitalisation to set
     */
    public void setCapitalisation(String capitalisation) {
        this.capitalisation = capitalisation;
    }

    /**
     * Get the annual allowance.
     *
     * @return the annualAllowance
     */
    public String getAnnualAllowance() {
        return annualAllowance;
    }

    /**
     * Set the annual allowance.
     *
     * @param annualAllowance the annualAllowance to set
     */
    public void setAnnualAllowance(String annualAllowance) {
        this.annualAllowance = annualAllowance;
    }

	/**
	 * @return the lta
	 */
	public String getLta()
	{
		return lta;
	}

	/**
	 * @param lta the lta to set
	 */
	public void setLta(String lta)
	{
		this.lta = lta;
	}

	/**
	 * @return the isConfigurationUsed
	 */
	public boolean isConfigurationUsed()
	{
		return isConfigurationUsed;
	}

	/**
	 * @param isConfigurationUsed the isConfigurationUsed to set
	 */
	public void setConfigurationUsed(boolean isConfigurationUsed)
	{
		this.isConfigurationUsed = isConfigurationUsed;
	}
	
	
}
