package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MemberDetail implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	private String bGroup;
	private String refno;
	private String nino;
	private String name;
	private String gender;
	private Date dateOfBirth;
	private Date joinedCompany;
	private Date joinedScheme;
	private Date serviceQualified;
	private double pensionableSalary;
	private double aVCFund;
	private double aVERFund;
	private int tVINADays;
	private int tVINBDays;
	private int tVINCDays;
	private int augmentationDays;
	private double lTA;
	private double bankedEGP;
	
	private double cpi;// added cpi from aquila
	
	private Date dateAt55th;
	private Date dateAt60th;
	private Date dateAt65th;
	
	private ArrayList<AbsenceHistory> absenceHistory = new ArrayList<AbsenceHistory>();
	private ArrayList<SchemeHistory> schemeHistory = new ArrayList<SchemeHistory>();
	private ArrayList<SalaryHistory> salaryHistory = new ArrayList<SalaryHistory>();
	private ArrayList<AugmentationHistory> augmentationHistory = new ArrayList<AugmentationHistory>();
	
	private ArrayList<ComFactor> comFactors = new ArrayList<ComFactor>();
	private ArrayList<ERFFactor> erfFactors = new ArrayList<ERFFactor>();
	private ArrayList<ERFFactor> tvinErfFactors = new ArrayList<ERFFactor>();
	private ArrayList<ERFFactor> veraFactors = new ArrayList<ERFFactor>();
	
	
	private boolean veraIndicator = false;
	
	/**
	 * Constructor
	 */
	public MemberDetail(){
		
	}
	
	/**
	 * @return the bGroup
	 */
	public String getBGroup()
	{
		return bGroup;
	}
	/**
	 * @param bGroup the bGroup to set
	 */
	public void setBGroup(String bGroup)
	{
		this.bGroup = bGroup;
	}
	/**
	 * @return the refno
	 */
	public String getRefno()
	{
		return refno;
	}
	/**
	 * @param refno the refno to set
	 */
	public void setRefno(String refno)
	{
		this.refno = refno;
	}
	/**
	 * @return the nino
	 */
	public String getNino()
	{
		return nino;
	}
	/**
	 * @param nino the nino to set
	 */
	public void setNino(String nino)
	{
		this.nino = nino;
	}
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	/**
	 * @return the gender
	 */
	public String getGender()
	{
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender)
	{
		this.gender = gender;
	}
	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth()
	{
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
		if (dateOfBirth != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateOfBirth);
			
			// 50th birthday
			calendar.add(Calendar.YEAR, 55);
			this.dateAt55th = calendar.getTime();
			// 60th birthday
			calendar.add(Calendar.YEAR, 5);
			this.dateAt60th = calendar.getTime();
			// 65th birthday
			calendar.add(Calendar.YEAR, 5);
			this.dateAt65th = calendar.getTime();
		}		
	}
	/**
	 * @return the joinedCompany
	 */
	public Date getJoinedCompany()
	{
		return joinedCompany;
	}
	/**
	 * @param joinedCompany the joinedCompany to set
	 */
	public void setJoinedCompany(Date joinedCompany)
	{
		this.joinedCompany = joinedCompany;
	}
	/**
	 * @return the joinedScheme
	 */
	public Date getJoinedScheme()
	{
		return joinedScheme;
	}
	/**
	 * @param joinedScheme the joinedScheme to set
	 */
	public void setJoinedScheme(Date joinedScheme)
	{
		this.joinedScheme = joinedScheme;
	}
		
	/**
	 * @return the serviceQualified
	 */
	public Date getServiceQualified()
	{
		return serviceQualified;
	}

	/**
	 * @param serviceQualified the serviceQualified to set
	 */
	public void setServiceQualified(Date serviceQualified)
	{
		this.serviceQualified = serviceQualified;
	}

	/**
	 * @return the pensionableSalary
	 */
	public double getPensionableSalary()
	{
		return pensionableSalary;
	}
	/**
	 * @param pensionableSalary the pensionableSalary to set
	 */
	public void setPensionableSalary(double pensionableSalary)
	{
		this.pensionableSalary = pensionableSalary;
	}
	/**
	 * @return the aVCFund
	 */
	public double getAVCFund()
	{
		return aVCFund;
	}
	/**
	 * @param aVCFund the aVCFund to set
	 */
	public void setAVCFund(double aVCFund)
	{
		this.aVCFund = aVCFund;
	}
	/**
	 * @return the aVERFund
	 */
	public double getAVERFund()
	{
		return aVERFund;
	}
	/**
	 * @param aVERFund the aVERFund to set
	 */
	public void setAVERFund(double aVERFund)
	{
		this.aVERFund = aVERFund;
	}
	/**
	 * @return the tVINADays
	 */
	public int getTVINADays()
	{
		return tVINADays;
	}
	/**
	 * @param tVINADays the tVINADays to set
	 */
	public void setTVINADays(int tVINADays)
	{
		this.tVINADays = tVINADays;
	}
	/**
	 * @return the tVINBDays
	 */
	public int getTVINBDays()
	{
		return tVINBDays;
	}
	/**
	 * @param tVINBDays the tVINBDays to set
	 */
	public void setTVINBDays(int tVINBDays)
	{
		this.tVINBDays = tVINBDays;
	}
	/**
	 * @return the tVINCDays
	 */
	public int getTVINCDays()
	{
		return tVINCDays;
	}
	/**
	 * @param tVINCDays the tVINCDays to set
	 */
	public void setTVINCDays(int tVINCDays)
	{
		this.tVINCDays = tVINCDays;
	}
	/**
	 * @return the augmentationDays
	 */
	public int getAugmentationDays()
	{
		return augmentationDays;
	}
	/**
	 * @param augmentationDays the augmentationDays to set
	 */
	public void setAugmentationDays(int augmentationDays)
	{
		this.augmentationDays = augmentationDays;
	}
	/**
	 * @return the lTA
	 */
	public double getLTA()
	{
		return lTA;
	}
	/**
	 * @param lTA the lTA to set
	 */
	public void setLTA(double lTA)
	{
		this.lTA = lTA;
	}
	/**
	 * @return the bankedEGP
	 */
	public double getBankedEGP()
	{
		return bankedEGP;
	}
	/**
	 * @param bankedEGP the bankedEGP to set
	 */
	public void setBankedEGP(double bankedEGP)
	{
		this.bankedEGP = bankedEGP;
	}
	/**
	 * @return the dateAt55th
	 */
	public Date getDateAt55th()
	{
		return dateAt55th;
	}
	
	/**
	 * @return the dateAt60th
	 */
	public Date getDateAt60th()
	{
		return dateAt60th;
	}
	
	/**
	 * @return the dateAt65th
	 */
	public Date getDateAt65th()
	{
		return dateAt65th;
	}
	
	/**
	 * @return the absenceHistory
	 */
	public ArrayList<AbsenceHistory> getAbsenceHistory()
	{
		return absenceHistory;
	}
	/**
	 * @param absenceHistory the absenceHistory to set
	 */
	public void setAbsenceHistory(ArrayList<AbsenceHistory> absenceHistory)
	{
		this.absenceHistory = absenceHistory;
	}
	/**
	 * @return the schemeHistory
	 */
	public ArrayList<SchemeHistory> getSchemeHistory()
	{
		return schemeHistory;
	}
	/**
	 * @param schemeHistory the schemeHistory to set
	 */
	public void setSchemeHistory(ArrayList<SchemeHistory> schemeHistory)
	{
		this.schemeHistory = schemeHistory;
	}
		
	
	
	/**
	 * @return the augmentationHistory
	 */
	public ArrayList<AugmentationHistory> getAugmentationHistory()
	{
		return augmentationHistory;
	}

	/**
	 * @param augmentationHistory the augmentationHistory to set
	 */
	public void setAugmentationHistory(ArrayList<AugmentationHistory> augmentationHistory)
	{
		this.augmentationHistory = augmentationHistory;
	}

	/**
	 * @return the salaryHistory
	 */
	public ArrayList<SalaryHistory> getSalaryHistory()
	{
		return salaryHistory;
	}
	/**
	 * @param salaryHistory the salaryHistory to set
	 */
	public void setSalaryHistory(ArrayList<SalaryHistory> salaryHistory)
	{
		this.salaryHistory = salaryHistory;
	}
	public ArrayList<ComFactor> getComFactors()
	{
		return comFactors;
	}
	
	public ArrayList<ERFFactor> getERFFactors()
	{
		return erfFactors;
	}


	/**
	 * @return the tvinErfFactors
	 */
	public ArrayList<ERFFactor> getTvinErfFactors()
	{
		return tvinErfFactors;
	}

	/**
	 * @param tvinErfFactors the tvinErfFactors to set
	 */
	public void setTvinErfFactors(ArrayList<ERFFactor> tvinErfFactors)
	{
		this.tvinErfFactors = tvinErfFactors;
	}

	public ArrayList<ERFFactor> getVeraFactors()
	{
		return veraFactors;
	}
	public void setVeraFactors(ArrayList<ERFFactor> veraFactors)
	{
		this.veraFactors = veraFactors;
	}
	public boolean isVeraIndicator()
	{
		return veraIndicator;
	}
	public void setVeraIndicator(boolean veraIndicator)
	{
		this.veraIndicator = veraIndicator;
	}
	
	public double getCpi()
	{
		return cpi;
	}
	public void setCpi(double cpi)
	{
		this.cpi = cpi;
	}
	
	public double getSalaryBefore (Date when)
	{
		for (int i = 0; i < salaryHistory.size(); i++)
		{
			SalaryHistory salaryDetail = salaryHistory.get(i);
			if (salaryDetail.getEffective() != null && salaryDetail.getEffective().compareTo(when) <= 0)
			{
				return salaryDetail.getSalary();
			}
		}
		
		return 0.0;
	}	
}
