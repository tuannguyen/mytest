package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;

/**
 * 
 * @author Huy Tran
 * 
 * Store all augmentation transfer-in histories of member
 *
 */
public class AugmentationHistory implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	public static final String AUGMENTATION_TYPE_WEST = "WEST";
	public static final String AUGMENTATION_TYPE_OBPC = "OBPC";
	public static final String AUGMENTATION_TYPE_OEST = "OEST";
	public static final String AUGMENTATION_TYPE_EAST = "EAST";
	
	private String sub;
	private String type;
	private int days;
	
	private int accrual;
	/**
	 * @return the sub
	 */
	public String getSub()
	{
		return sub;
	}
	/**
	 * @param sub the sub to set
	 */
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}
	/**
	 * @return the days
	 */
	public int getDays()
	{
		return days;
	}
	/**
	 * @param days the days to set
	 */
	public void setDays(int days)
	{
		this.days = days;
	}
	/**
	 * @return the accrual
	 */
	public int getAccrual()
	{
		return accrual;
	}
	/**
	 * @param accrual the accrual to set
	 */
	public void setAccrual(int accrual)
	{
		this.accrual = accrual;
	}
	
	
}
