package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.Date;

public class PensionDetails implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	private Date DoR;
	private double unreducedPension;
	private double reducedPension;
	private double maxSchemeCash;
	private double pensionWithChosenCash;	
	private double pensionWithMaxCash;
	private double pensionWithHaftCash;
	private double pensionPot;
	private double cashLumpSum;
	
	/**
	 * COnstructor
	 */
	public PensionDetails(){
		
	}
	public Date getDoR()
	{
		return DoR;
	}
	public void setDoR(Date doR)
	{
		DoR = doR;
	}
	
	/**
	 * @return the unreducedPension
	 */
	public double getUnreducedPension()
	{
		return unreducedPension;
	}
	/**
	 * @param unreducedPension the unreducedPension to set
	 */
	public void setUnreducedPension(double unreducedPension)
	{
		this.unreducedPension = unreducedPension;
	}
	public double getReducedPension()
	{
		return reducedPension;
	}
	public void setReducedPension(double reducedPension)
	{
		this.reducedPension = reducedPension;
	}
	
	
	/**
	 * @return the pensionWithChosenCash
	 */
	public double getPensionWithChosenCash()
	{
		return pensionWithChosenCash;
	}
	/**
	 * @param pensionWithChosenCash the pensionWithChosenCash to set
	 */
	public void setPensionWithChosenCash(double pensionWithChosenCash)
	{
		this.pensionWithChosenCash = pensionWithChosenCash;
	}
	public double getMaxSchemeCash()
	{
		return maxSchemeCash;
	}
	public void setMaxSchemeCash(double maxSchemeCash)
	{
		this.maxSchemeCash = maxSchemeCash;
	}
	public double getPensionPot()
	{
		return pensionPot;
	}
	public void setPensionPot(double pensionPot)
	{
		this.pensionPot = pensionPot;
	}
	/**
	 * @return the cashLumpSum
	 */
	public double getCashLumpSum()
	{
		return cashLumpSum;
	}
	/**
	 * @param cashLumpSum the cashLumpSum to set
	 */
	public void setCashLumpSum(double cashLumpSum)
	{
		this.cashLumpSum = cashLumpSum;
	}
	/**
	 * @return the pensionWithMaxCash
	 */
	public double getPensionWithMaxCash()
	{
		return pensionWithMaxCash;
	}
	/**
	 * @param pensionWithMaxCash the pensionWithMaxCash to set
	 */
	public void setPensionWithMaxCash(double pensionWithMaxCash)
	{
		this.pensionWithMaxCash = pensionWithMaxCash;
	}
	/**
	 * @return the pensionWithHaftCash
	 */
	public double getPensionWithHaftCash()
	{
		return pensionWithHaftCash;
	}
	/**
	 * @param pensionWithHaftCash the pensionWithHaftCash to set
	 */
	public void setPensionWithHaftCash(double pensionWithHaftCash)
	{
		this.pensionWithHaftCash = pensionWithHaftCash;
	}		
	
	
	
}
