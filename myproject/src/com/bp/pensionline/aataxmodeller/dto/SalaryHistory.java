package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.Date;

public class SalaryHistory implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	private Date effective;
	private double salary;
	/**
	 * @return the effective
	 */
	public Date getEffective()
	{
		return effective;
	}
	/**
	 * @param effective the effective to set
	 */
	public void setEffective(Date effective)
	{
		this.effective = effective;
	}
	/**
	 * @return the salary
	 */
	public double getSalary()
	{
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary)
	{
		this.salary = salary;
	}
	
}
