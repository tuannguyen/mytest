/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.dto;

import com.bp.pensionline.util.StringUtil;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author   Admin
 * @version  1.0
 */
public class DefaultInfo implements Cloneable {

    //~ Instance fields --------------------------

    private String label;
    private String value;

    //~ Constructors -----------------------------

    /**
     * Default constructor.<br>
     * instantiate empty value for all variables
     */
    public DefaultInfo() {
        label = StringUtil.EMPTY_STRING;
        value = StringUtil.EMPTY_STRING;
    }

    //~ Methods ----------------------------------

    /**
     * DOCUMENT ME!
     *
     * @return  the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  label  the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the value
     */
    public String getValue() {
        return value;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  value  the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  CloneNotSupportedException  DOCUMENT ME!
     */
    @Override public Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return super.clone();
    }
}
