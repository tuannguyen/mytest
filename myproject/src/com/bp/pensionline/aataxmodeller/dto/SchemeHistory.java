package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.Date;

public class SchemeHistory implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	private Date from;
	private Date to;
	private String category;
	private int accrual;
	/**
	 * @return the from
	 */
	public Date getFrom()
	{
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from)
	{
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo()
	{
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to)
	{
		this.to = to;
	}
	/**
	 * @return the category
	 */
	public String getCategory()
	{
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category)
	{
		this.category = category;
	}
	/**
	 * @return the accrual
	 */
	public int getAccrual()
	{
		return accrual;
	}
	/**
	 * @param accrual the accrual to set
	 */
	public void setAccrual(int accrual)
	{
//		if (accrual == 0)
//		{
//			this.accrual = 100;
//		}
//		else if (accrual == 20)
//		{
//			this.accrual = 120;
//		}
//		else if (accrual == 80)
//		{
//			this.accrual = 80;
//		}
//		else
//		{
//			
//		}
		this.accrual = accrual;
	}
	
	
}
