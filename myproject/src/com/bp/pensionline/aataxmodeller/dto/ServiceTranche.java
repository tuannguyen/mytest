package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.Date;

public class ServiceTranche implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	private String trancheType;
	private Date from;
	private Date to;
	private String category;
	private int years;
	private int days;	
	private double totalYears;
	private int totalDays;
	private double FTE = 1.0;
	private double serviceYears;
	private int accrual;
	private double accrued;
	private double serviceYearsAt60th;
	private double ERF = 1.0;
	
	private boolean serviceIndicator = true;
	
	/**
	 * @return the trancheType
	 */
	public String getTrancheType()
	{
		return trancheType;
	}
	/**
	 * @param trancheType the trancheType to set
	 */
	public void setTrancheType(String trancheType)
	{
		this.trancheType = trancheType;
	}
	/**
	 * @return the from
	 */
	public Date getFrom()
	{
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from)
	{
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo()
	{
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to)
	{
		this.to = to;
	}
	/**
	 * @return the category
	 */
	public String getCategory()
	{
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category)
	{
		this.category = category;
	}
	/**
	 * @return the years
	 */
	public int getYears()
	{
		return years;
	}
	/**
	 * @param years the years to set
	 */
	public void setYears(int years)
	{
		this.years = years;
	}
	/**
	 * @return the days
	 */
	public int getDays()
	{
		return days;
	}
	/**
	 * @param days the days to set
	 */
	public void setDays(int days)
	{
		this.days = days;
	}
	/**
	 * @return the totalYears
	 */
	public double getTotalYears()
	{
		return totalYears;
	}
	/**
	 * @param totalYears the totalYears to set
	 */
	public void setTotalYears(double totalYears)
	{
		this.totalYears = totalYears;
	}
	
	
	/**
	 * @return the totalDays
	 */
	public int getTotalDays()
	{
		return totalDays;
	}
	/**
	 * @param totalDays the totalDays to set
	 */
	public void setTotalDays(int totalDays)
	{
		this.totalDays = totalDays;
	}
	/**
	 * @return the fTE
	 */
	public double getFTE()
	{
		return FTE;
	}
	/**
	 * @param fTE the fTE to set
	 */
	public void setFTE(double fTE)
	{
		FTE = fTE;
	}
	/**
	 * @return the serviceYears
	 */
	public double getServiceYears()
	{
		return serviceYears;
	}
	/**
	 * @param serviceYears the serviceYears to set
	 */
	public void setServiceYears(double serviceYears)
	{
		this.serviceYears = serviceYears;
	}
	/**
	 * @return the accrual
	 */
	public int getAccrual()
	{
		return accrual;
	}
	/**
	 * @param accrual the accrual to set
	 */
	public void setAccrual(int accrual)
	{
		this.accrual = accrual;
	}
	/**
	 * @return the accrued
	 */
	public double getAccrued()
	{
		return accrued;
	}
	/**
	 * @param accrued the accrued to set
	 */
	public void setAccrued(double accrued)
	{
		this.accrued = accrued;
	}
	/**
	 * @return the serviceYearsAt60th
	 */
	public double getServiceYearsAt60th()
	{
		return serviceYearsAt60th;
	}
	/**
	 * @param serviceYearsAt60th the serviceYearsAt60th to set
	 */
	public void setServiceYearsAt60th(double serviceYearsAt60th)
	{
		this.serviceYearsAt60th = serviceYearsAt60th;
	}
	public double getERF()
	{
		return ERF;
	}
	public void setERF(double eRF)
	{
		ERF = eRF;
	}
	/**
	 * @return the serviceIndicator
	 */
	public boolean isServiceIndicator()
	{
		return serviceIndicator;
	}
	/**
	 * @param serviceIndicator the serviceIndicator to set
	 */
	public void setServiceIndicator(boolean serviceIndicator)
	{
		this.serviceIndicator = serviceIndicator;
	}
	
	
}
