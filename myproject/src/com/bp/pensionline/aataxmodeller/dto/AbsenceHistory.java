package com.bp.pensionline.aataxmodeller.dto;

import java.io.Serializable;
import java.util.Date;

public class AbsenceHistory implements Serializable
{
	public static final long serialVersionUID = 0L;
	
	public static final String ABSENCE_TYPE_PT = "PT";
	public static final String ABSENCE_TYPE_TA = "TA";
	
	private String type;
	private Date from;
	private Date to;
	private double worked;
	private double employed;
	private boolean serviceIndicator = true;
	
	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}
	/**
	 * @return the from
	 */
	public Date getFrom()
	{
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from)
	{
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo()
	{
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to)
	{
		this.to = to;
	}
	/**
	 * @return the worked
	 */
	public double getWorked()
	{
		return worked;
	}
	/**
	 * @param worked the worked to set
	 */
	public void setWorked(double worked)
	{
		this.worked = worked;
	}
	/**
	 * @return the employed
	 */
	public double getEmployed()
	{
		return employed;
	}
	/**
	 * @param employed the employed to set
	 */
	public void setEmployed(double employed)
	{
		this.employed = employed;
	}
	/**
	 * @return the serviceIndicator
	 */
	public boolean isServiceIndicator()
	{
		return serviceIndicator;
	}
	/**
	 * @param serviceIndicator the serviceIndicator to set
	 */
	public void setServiceIndicator(boolean serviceIndicator)
	{
		this.serviceIndicator = serviceIndicator;
	}

	
}
