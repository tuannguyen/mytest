/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.dto;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public class TaxYear {

    //~ Instance fields ----------------------------------------------------------------------------

    private String year;
    private String taxYear;
    private double soySalary;
    private double soyServiceYears;
    private double soyAccrued;
    private double soyBenefit;
    private double cpi;
    private double cpiReval;
    private double salaryIncrease;
    private double eoySalary;
    private double eoyServiceYears;
    private double eoYAccrued;
    private double eoyBenefit;
    private double accrualRate;
    private double increase;
    private double aAFactor;
    private double aACheck;
    private double annualAllowance;
    private double aAExcess;
    private double taxRate;
    private double taxAmount;
	/**
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(String year)
	{
		this.year = year;
	}
	/**
	 * @return the taxYear
	 */
	public String getTaxYear()
	{
		return taxYear;
	}
	/**
	 * @param taxYear the taxYear to set
	 */
	public void setTaxYear(String taxYear)
	{
		this.taxYear = taxYear;
	}
	/**
	 * @return the soySalary
	 */
	public double getSoySalary()
	{
		return soySalary;
	}
	/**
	 * @param soySalary the soySalary to set
	 */
	public void setSoySalary(double soySalary)
	{
		this.soySalary = soySalary;
	}
	/**
	 * @return the soyServiceYears
	 */
	public double getSoyServiceYears()
	{
		return soyServiceYears;
	}
	/**
	 * @param soyServiceYears the soyServiceYears to set
	 */
	public void setSoyServiceYears(double soyServiceYears)
	{
		this.soyServiceYears = soyServiceYears;
	}
	/**
	 * @return the soyAccrued
	 */
	public double getSoyAccrued()
	{
		return soyAccrued;
	}
	/**
	 * @param soyAccrued the soyAccrued to set
	 */
	public void setSoyAccrued(double soyAccrued)
	{
		this.soyAccrued = soyAccrued;
	}
	/**
	 * @return the soyBenefit
	 */
	public double getSoyBenefit()
	{
		return soyBenefit;
	}
	/**
	 * @param soyBenefit the soyBenefit to set
	 */
	public void setSoyBenefit(double soyBenefit)
	{
		this.soyBenefit = soyBenefit;
	}
	/**
	 * @return the cpi
	 */
	public double getCpi()
	{
		return cpi;
	}
	/**
	 * @param cpi the cpi to set
	 */
	public void setCpi(double cpi)
	{
		this.cpi = cpi;
	}
	/**
	 * @return the cpiReval
	 */
	public double getCpiReval()
	{
		return cpiReval;
	}
	/**
	 * @param cpiReval the cpiReval to set
	 */
	public void setCpiReval(double cpiReval)
	{
		this.cpiReval = cpiReval;
	}
	/**
	 * @return the salaryIncrease
	 */
	public double getSalaryIncrease()
	{
		return salaryIncrease;
	}
	/**
	 * @param salaryIncrease the salaryIncrease to set
	 */
	public void setSalaryIncrease(double salaryIncrease)
	{
		this.salaryIncrease = salaryIncrease;
	}
	/**
	 * @return the eoySalary
	 */
	public double getEoySalary()
	{
		return eoySalary;
	}
	/**
	 * @param eoySalary the eoySalary to set
	 */
	public void setEoySalary(double eoySalary)
	{
		this.eoySalary = eoySalary;
	}
	/**
	 * @return the eoyServiceYears
	 */
	public double getEoyServiceYears()
	{
		return eoyServiceYears;
	}
	/**
	 * @param eoyServiceYears the eoyServiceYears to set
	 */
	public void setEoyServiceYears(double eoyServiceYears)
	{
		this.eoyServiceYears = eoyServiceYears;
	}
	/**
	 * @return the eoYAccrued
	 */
	public double getEoyAccrued()
	{
		return eoYAccrued;
	}
	/**
	 * @param eoYAccrued the eoYAccrued to set
	 */
	public void setEoYAccrued(double eoYAccrued)
	{
		this.eoYAccrued = eoYAccrued;
	}
	/**
	 * @return the eoyBenefit
	 */
	public double getEoyBenefit()
	{
		return eoyBenefit;
	}
	/**
	 * @param eoyBenefit the eoyBenefit to set
	 */
	public void setEoyBenefit(double eoyBenefit)
	{
		this.eoyBenefit = eoyBenefit;
	}
	/**
	 * @return the accrualRate
	 */
	public double getAccrualRate()
	{
		return accrualRate;
	}
	/**
	 * @param accrualRate the accrualRate to set
	 */
	public void setAccrualRate(double accrualRate)
	{
		this.accrualRate = accrualRate;
	}
	/**
	 * @return the increase
	 */
	public double getIncrease()
	{
		return increase;
	}
	/**
	 * @param increase the increase to set
	 */
	public void setIncrease(double increase)
	{
		this.increase = increase;
	}
	/**
	 * @return the aAFactor
	 */
	public double getaAFactor()
	{
		return aAFactor;
	}
	/**
	 * @param aAFactor the aAFactor to set
	 */
	public void setaAFactor(double aAFactor)
	{
		this.aAFactor = aAFactor;
	}
	/**
	 * @return the aACheck
	 */
	public double getAACheck()
	{
		return aACheck;
	}
	/**
	 * @param aACheck the aACheck to set
	 */
	public void setAACheck(double aACheck)
	{
		this.aACheck = aACheck;
	}
	/**
	 * @return the annualAllowance
	 */
	public double getAnnualAllowance()
	{
		return annualAllowance;
	}
	/**
	 * @param annualAllowance the annualAllowance to set
	 */
	public void setAnnualAllowance(double annualAllowance)
	{
		this.annualAllowance = annualAllowance;
	}
	/**
	 * @return the aAExcess
	 */
	public double getaAExcess()
	{
		return aAExcess;
	}
	/**
	 * @param aAExcess the aAExcess to set
	 */
	public void setaAExcess(double aAExcess)
	{
		this.aAExcess = aAExcess;
	}
	/**
	 * @return the taxRate
	 */
	public double getTaxRate()
	{
		return taxRate;
	}
	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(double taxRate)
	{
		this.taxRate = taxRate;
	}
	/**
	 * @return the taxAmount
	 */
	public double getTaxAmount()
	{
		return taxAmount;
	}
	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(double taxAmount)
	{
		this.taxAmount = taxAmount;
	}

    //~ Methods ------------------------------------------------------------------------------------


}
