/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.modeller;

import com.bp.pensionline.aataxmodeller.dto.DefaultInfo;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.aataxmodeller.util.DefaultConfiguration;
import com.bp.pensionline.aataxmodeller.util.NumberUtil;
import com.bp.pensionline.aataxmodeller.util.TaxUtil;

import org.apache.commons.logging.Log;

import org.opencms.main.CmsLog;

import java.util.Date;
import java.util.Hashtable;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public class TaxModeller {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Log for the class. */
    private static final Log LOG = CmsLog.getLog(TaxModeller.class);
    
    private static final int DAYS_IN_60_YEARS = 21900; // 60x365

    //~ Instance fields ----------------------------------------------------------------------------

    /** Represent configuration file name . */
    private String currentTaxYear;

    /** Represent configuration file name . */
    private double serviceYears;

    /** Represent configuration file name . */
    private double modelledSalary;

    /** Represent configuration file name . */
    private double modelledAccrualRate;

    /** Represent configuration file name . */
    private double modelledSalaryIncrease;

    /** Represent configuration file name . */
    private double modelledCPI;

    /** Represent configuration file name . */
    private double modelledAATaxRate;
    
    private Date systemDate = new Date();
    
    

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * The method get current tax.
     *
     * @return currentTaxYear current tax value in String
     */
    public String getCurrentTaxYear() {
        return currentTaxYear;
    }

    /**
     * The method set current tax year.
     *
     * @param currentTaxYear the String value
     */
    public void setCurrentTaxYear(String currentTaxYear) {
        this.currentTaxYear = currentTaxYear;
    }

    /**
     * The method get working years.
     *
     * @return number of working years
     */
    public double getServiceYears() {
        return serviceYears;
    }

    /**
     * The method store working years.
     *
     * @param serviceYears number of working years
     */
    public void setServiceYears(double serviceYears) {
        this.serviceYears = serviceYears;
    }

    /**
     * Get modelled salary.
     *
     * @return modelledSalary double value
     */
    public double getModelledSalary() {
        return modelledSalary;
    }

    /**
     * The method store modelled salary value.
     *
     * @param modelledSalary double value
     */
    public void setModelledSalary(double modelledSalary) {
        this.modelledSalary = modelledSalary;
    }



    /**
	 * @return the modelledAccrualRate
	 */
	public double getModelledAccrualRate()
	{
		return modelledAccrualRate;
	}

	/**
	 * @param modelledAccrualRate the modelledAccrualRate to set
	 */
	public void setModelledAccrualRate(double modelledAccrualRate)
	{
		this.modelledAccrualRate = modelledAccrualRate;
	}

	/**
     * Get modelled salary value.
     *
     * @return modelledSalaryIncrease the double value
     */
    public double getModelledSalaryIncrease() {
        return modelledSalaryIncrease;
    }

    /**
     * The method store index of increase salary.
     *
     * @param modelledSalaryIncrease the double value
     */
    public void setModelledSalaryIncrease(double modelledSalaryIncrease) {
        this.modelledSalaryIncrease = modelledSalaryIncrease;
    }

    /**
     * The method get cpi value.
     *
     * @return modelledCPI the double value
     */
    public double getModelledCPI() {
        return modelledCPI;
    }

    /**
     * The method store cpi index.
     *
     * @param modelledCPI the double value
     */
    public void setModelledCPI(double modelledCPI) {
        this.modelledCPI = modelledCPI;
    }

    /**
     * Set annual allowance tax rate.
     *
     * @return modelledAATaxRate the double value
     */
    public double getModelledAATaxRate() {
        return modelledAATaxRate;
    }

    /**
     * Get annual allowance tax rate.
     *
     * @param modelledAATaxRate double value
     */
    public void setModelledAATaxRate(double modelledAATaxRate) {
        this.modelledAATaxRate = modelledAATaxRate;
    }
    
    

    /**
	 * @return the systemDate
	 */
	public Date getSystemDate()
	{
		return systemDate;
	}

	/**
	 * @param systemDate the systemDate to set
	 */
	public void setSystemDate(Date systemDate)
	{
		this.systemDate = systemDate;
	}

    /**
     * Calculate the next tax year based on 60th equivalent.
     *
     * @param  soyServiceYears         number of years in working
     * @param  soySalary               currency salary
     * @param  salaryIncrease       index of annual increasing salary
     * @param  taxRate              annual allowance tax rate
     * @param  cpi                  consumer price index measure by percentage
     * @param  capitalisationFactor index of capitalisation
     * @param  annualAllowance      number of annual allowance
     *
     * @return A TaxYear object contains all tax year data which show the step
     *         of calculation
     *
     * @throws NumberFormatException error exception in calculation.
     */
    public TaxYear estimateTaxYear(
    		double soyServiceYears,
    		double soySalary, 
    		int accrual,
    		double salaryIncrease, 
    		double taxRate, 
    		double cpi, 
    		double capitalisationFactor,
    		double annualAllowance) 
    {
    	LOG.info("TaxModeller: serviceYears: " + soyServiceYears);
    	LOG.info("TaxModeller: salary: " + soySalary);
    	LOG.info("TaxModeller: accrual: " + accrual);
    	LOG.info("TaxModeller: salaryIncrease: " + salaryIncrease);
    	LOG.info("TaxModeller: taxRate: " + taxRate);
    	LOG.info("TaxModeller: cpi: " + cpi);
    	LOG.info("TaxModeller: capitalisationFactor: " + capitalisationFactor);
    	LOG.info("TaxModeller: annualAllowance: " + annualAllowance);
    	
        TaxYear nextTaxYear = new TaxYear();
        try 
        {
            // year and tax year   
            String year = DateUtil.getModelledYear(getSystemDate());
            nextTaxYear.setYear(year);
            
            String taxYear = DateUtil.getModelledTaxYear(getSystemDate());
            nextTaxYear.setTaxYear(taxYear);
            
        	nextTaxYear.setAccrualRate(accrual);
        	nextTaxYear.setCpi(cpi);
            nextTaxYear.setSalaryIncrease(salaryIncrease);        	
        	nextTaxYear.setaAFactor(capitalisationFactor);
        	
            // Calculate tax for only next year.
        	double soyAccrued = 0.00;
        	soyAccrued = soyServiceYears / 60; 
        	
        	nextTaxYear.setSoySalary(soySalary);
        	nextTaxYear.setSoyServiceYears(soyServiceYears);
        	nextTaxYear.setSoyAccrued(soyAccrued);
            
            double soyBenefit = soyAccrued * soySalary;
            nextTaxYear.setSoyBenefit(soyBenefit);                       
            
            double eoySalary = soySalary * (1 + salaryIncrease);
            nextTaxYear.setEoySalary(eoySalary);
            double eoyServiceYears = soyServiceYears;
            double eoyAccrued = soyAccrued;
            double eoyBenefit = soyBenefit;
            if (accrual > 0)
            {
            	eoyServiceYears = soyServiceYears + 60/(double)accrual;
            	eoyAccrued = soyAccrued + (1 / (double)accrual);
            	eoyBenefit = eoyAccrued * eoySalary;
            }
            nextTaxYear.setEoyServiceYears(eoyServiceYears);
            nextTaxYear.setEoYAccrued(eoyAccrued);
            nextTaxYear.setEoyBenefit(eoyBenefit);
            
            double cpiRevalue = soyBenefit * (1 + cpi);
            nextTaxYear.setCpiReval(cpiRevalue);
            
            double increase = eoyBenefit - cpiRevalue;
            nextTaxYear.setIncrease(increase);
            
            double aaCheck = increase * capitalisationFactor;
            nextTaxYear.setAACheck(aaCheck);
            
            nextTaxYear.setAnnualAllowance(annualAllowance);
            
            double aaExcess = aaCheck - annualAllowance;
            nextTaxYear.setaAExcess(aaExcess);
            
            nextTaxYear.setTaxRate(taxRate);
            
            double taxAmount = aaExcess * taxRate;
            

            // If aaCheck is not greater annual allowance.
            if (aaCheck <= annualAllowance) {
                taxAmount = 0;
            }
            nextTaxYear.setTaxAmount(taxAmount);
            
            
        } 
        catch (Exception e) 
        {
            LOG.error("Error while calculating next tax year: " + e.toString());
        }

        return nextTaxYear;
    }
    
    /**
     * Calculate the next tax year based on 60th equivalent.
     *
     * @param  serviceYears         number of years in working
     * @param  salary               currency salary
     * @param  salaryIncrease       index of annual increasing salary
     * @param  taxRate              annual allowance tax rate
     * @param  cpi                  consumer price index measure by percentage
     * @param  capitalisationFactor index of capitalisation
     * @param  annualAllowance      number of annual allowance
     *
     * @return A TaxYear object contains all tax year data which show the step
     *         of calculation
     *
     * @throws NumberFormatException error exception in calculation.
     */
    public TaxYear calculateTaxYearByServiceYears(
    		double soyServiceYears,
    		double soySalary, 
    		double eoyServiceYears,
    		double eoySalary,
    		int accrual,
    		double taxRate, 
    		double cpi, 
    		double capitalisationFactor,
    		double annualAllowance) 
    {
//    	LOG.info("TaxModeller: soy serviceYears: " + soyServiceYears);
//    	LOG.info("TaxModeller: soy salary: " + soySalary);
//    	LOG.info("TaxModeller: eoy serviceYears: " + eoyServiceYears);
//    	LOG.info("TaxModeller: eoy salary: " + eoySalary);
//    	LOG.info("TaxModeller: taxRate: " + taxRate);
//    	LOG.info("TaxModeller: cpi: " + cpi);
//    	LOG.info("TaxModeller: capitalisationFactor: " + capitalisationFactor);
//    	LOG.info("TaxModeller: annualAllowance: " + annualAllowance);
//    	
        TaxYear currentTaxYear = new TaxYear();
        try 
        {
            // year and tax year   
            String year = DateUtil.getModelledYear(getSystemDate());
            currentTaxYear.setYear(year);
            
            String taxYear = DateUtil.getModelledTaxYear(getSystemDate());
            currentTaxYear.setTaxYear(taxYear);
            
        	currentTaxYear.setAccrualRate(accrual);
        	currentTaxYear.setCpi(cpi);
        	if (soySalary > 0)
        	{
        		currentTaxYear.setSalaryIncrease((eoySalary - soySalary) / soySalary);  
        	}
        	currentTaxYear.setaAFactor(capitalisationFactor);
        	
            // Start of year
        	double soyAccrued =  soyServiceYears / 60; 
        	
        	currentTaxYear.setSoySalary(soySalary);
        	currentTaxYear.setSoyServiceYears(soyServiceYears);
        	currentTaxYear.setSoyAccrued(soyAccrued);
            double soyBenefit = soyAccrued * soySalary;
            currentTaxYear.setSoyBenefit(soyBenefit);                       
            
            // End of year
            currentTaxYear.setEoySalary(eoySalary);
            currentTaxYear.setEoyServiceYears(eoyServiceYears);
            
            double eoyAccrued = eoyServiceYears / 60;
            double eoyBenefit = soyBenefit;
            if (accrual > 0)
            {            	
            	eoyBenefit = eoyAccrued * eoySalary;
            }
           
            currentTaxYear.setEoYAccrued(eoyAccrued);
            currentTaxYear.setEoyBenefit(eoyBenefit);
            
            
            // Taxable amount
            double cpiRevalue = soyBenefit * (1 + cpi);
            currentTaxYear.setCpiReval(cpiRevalue);
            
            double increase = eoyBenefit - cpiRevalue;
            currentTaxYear.setIncrease(increase);
            
            double aaCheck = increase * capitalisationFactor;
            currentTaxYear.setAACheck(aaCheck);
            
            currentTaxYear.setAnnualAllowance(annualAllowance);
            
            double aaExcess = aaCheck - annualAllowance;
            currentTaxYear.setaAExcess(aaExcess);
            
            currentTaxYear.setTaxRate(taxRate);
            
            double taxAmount = aaExcess * taxRate;
            

            // If aaCheck is not greater annual allowance.
            if (aaCheck <= annualAllowance) {
                taxAmount = 0;
            }
            currentTaxYear.setTaxAmount(taxAmount);
            
            
        } 
        catch (Exception e) 
        {
        	LOG.error("Error while calculating current tax year: " + e.toString());
        }
        
        return currentTaxYear;
    }    
    
    /**
     * Calculate the next tax year based on 60th equivalent.
     *
     * @param  serviceYears         number of years in working
     * @param  salary               currency salary
     * @param  salaryIncrease       index of annual increasing salary
     * @param  taxRate              annual allowance tax rate
     * @param  cpi                  consumer price index measure by percentage
     * @param  capitalisationFactor index of capitalisation
     * @param  annualAllowance      number of annual allowance
     *
     * @return A TaxYear object contains all tax year data which show the step
     *         of calculation
     *
     * @throws NumberFormatException error exception in calculation.
     */
    public TaxYear calculateTaxYearByServiceDays(
    		double soyServiceDays,
    		double soySalary, 
    		double eoyServiceDays,
    		double eoySalary,
    		int accrual,
    		double taxRate, 
    		double cpi, 
    		double capitalisationFactor,
    		double annualAllowance) 
    {
//    	LOG.info("TaxModeller: soy serviceYears: " + soyServiceYears);
//    	LOG.info("TaxModeller: soy salary: " + soySalary);
//    	LOG.info("TaxModeller: eoy serviceYears: " + eoyServiceYears);
//    	LOG.info("TaxModeller: eoy salary: " + eoySalary);
//    	LOG.info("TaxModeller: taxRate: " + taxRate);
//    	LOG.info("TaxModeller: cpi: " + cpi);
//    	LOG.info("TaxModeller: capitalisationFactor: " + capitalisationFactor);
//    	LOG.info("TaxModeller: annualAllowance: " + annualAllowance);
//    	
        TaxYear currentTaxYear = new TaxYear();
        try 
        {
            // year and tax year   
            String year = DateUtil.getModelledYear(getSystemDate());
            currentTaxYear.setYear(year);
            
            String taxYear = DateUtil.getModelledTaxYear(getSystemDate());
            currentTaxYear.setTaxYear(taxYear);
            
        	currentTaxYear.setAccrualRate(accrual);
        	currentTaxYear.setCpi(cpi);
        	if (soySalary > 0)
        	{
        		currentTaxYear.setSalaryIncrease((eoySalary - soySalary) / soySalary);  
        	}
        	currentTaxYear.setaAFactor(capitalisationFactor);
        	
            // Start of year
        	double soyAccrued =  soyServiceDays / DAYS_IN_60_YEARS; 
        	
        	currentTaxYear.setSoySalary(soySalary);
        	currentTaxYear.setSoyServiceYears(soyServiceDays / 365);
        	currentTaxYear.setSoyAccrued(soyAccrued);
            double soyBenefit = soyAccrued * soySalary;
            currentTaxYear.setSoyBenefit(soyBenefit);                       
            
            // End of year
            currentTaxYear.setEoySalary(eoySalary);
            currentTaxYear.setEoyServiceYears(eoyServiceDays / 365);
            
            double eoyAccrued = eoyServiceDays / DAYS_IN_60_YEARS;
            double eoyBenefit = soyBenefit;
            if (accrual > 0)
            {            	
            	eoyBenefit = eoyAccrued * eoySalary;
            }
           
            currentTaxYear.setEoYAccrued(eoyAccrued);
            currentTaxYear.setEoyBenefit(eoyBenefit);
            
            
            // Taxable amount
            double cpiRevalue = soyBenefit * (1 + cpi);
            currentTaxYear.setCpiReval(cpiRevalue);
            
            double increase = eoyBenefit - cpiRevalue;
            currentTaxYear.setIncrease(increase);
            
            double aaCheck = increase * capitalisationFactor;
            currentTaxYear.setAACheck(aaCheck);
            
            currentTaxYear.setAnnualAllowance(annualAllowance);
            
            double aaExcess = aaCheck - annualAllowance;
            currentTaxYear.setaAExcess(aaExcess);
            
            currentTaxYear.setTaxRate(taxRate);
            
            double taxAmount = aaExcess * taxRate;
            

            // If aaCheck is not greater annual allowance.
            if (aaCheck <= annualAllowance) {
                taxAmount = 0;
            }
            currentTaxYear.setTaxAmount(taxAmount);
            
            
        } 
        catch (Exception e) 
        {
        	LOG.error("Error while calculating current tax year: " + e.toString());
        }
        
        return currentTaxYear;
    }     

    /**
     * Main functions for testing.
     *
     * @param args no use
     */
    public static void main(String[] args) {
        TaxModeller model = new TaxModeller();

        // TaxYear year = model.calculateNextTaxYear(25, 120000, 60, 0.05,
        // 0.02, 16);
        //DefaultConfigValues values = model.loadConfigToObjectValues();
        TaxYear year1 = model.estimateTaxYear(25.35, 326000, 60,
                0.05,
                0.5,
                0.02,
                16,
                50000);

        // TaxYear year1 = model.calculateNextTaxYearDefault(25, 120000, 60,
        // 0.05, 0.5, 0.02, 16, 50000);
        System.out.println(year1.getEoySalary());
        System.out.println(NumberUtil.formatToDecimal(year1.getTaxAmount()));
    }

    /**
     * Load key to properties.
     *
     * @return DefaultConfigValues object value
     */
    public TaxConfiguredValues getTaxConfiguredValues(String taxYear) 
    {
    	TaxConfiguredValues values = new TaxConfiguredValues();
    	
    	DefaultConfiguration config = new DefaultConfiguration();

        // Load property from configuration file.
        Hashtable<String, DefaultInfo> defaultConfigs = config.getDefaultValues();

        // Set elements into properties.
        DefaultInfo info = new DefaultInfo();
        if (defaultConfigs.containsKey(TaxUtil.AVERAGE_SALARY_INCREASE_TAG)) {
            info = defaultConfigs.get(TaxUtil.AVERAGE_SALARY_INCREASE_TAG);
            values.setAverageSalaryIncrease(info.getValue());
        }
        if (defaultConfigs.containsKey(TaxUtil.ANNUAL_TAX_RATE_TAG)) {
            info = defaultConfigs.get(TaxUtil.ANNUAL_TAX_RATE_TAG);
            values.setAnnualAllowanceTaxRate(info.getValue());
        }
        if (defaultConfigs.containsKey(TaxUtil.ANNUAL_INFLATION_TAG)) {
            info = defaultConfigs.get(TaxUtil.ANNUAL_INFLATION_TAG);
            values.setAnnualInflation(info.getValue());
        }
        if (defaultConfigs.containsKey(TaxUtil.ANNUAL_ALLOWANCE_TAG)) {
            info = defaultConfigs.get(TaxUtil.ANNUAL_ALLOWANCE_TAG);
            values.setAnnualAllowance(info.getValue());
        }
        values.setCapitalisation(config.getCapValueWithYear(taxYear));
        values.setLta(config.getLTAValueWithYear(taxYear));
        
        values.setConfigurationUsed(config.isDefaultConfigUsed());

        return values;
    }
}
