package com.bp.pensionline.aataxmodeller.modeller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.util.Timer;

public class TaxModellerAuditor
{
	public static final Log LOG = CmsLog.getLog(TaxModellerAuditor.class);

	private static final String ACCESS_AUDIT_QUERY = "Insert into BP_STATS_POS "
			+ "(session_id, event_time, event_time_ms, event_year, event_month, event_day, "
			+ "event_hour, event_minute, event_second, auth_seqno, company, member_scheme, member_status, "
			+ "gender, marital_status, age_range, salary_range, location, overseas, duration) values( "
			+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String GET_MAX_SEQNO_QUERY = "select max(seqno) AS CASENO FROM BP_STATS_POS";

	public void doAccessAudit(HttpServletRequest request)
	{
		
		Connection con = getWebstatsConnection();;
				
		if (con != null && request != null)
		{
			CmsUser currentUser = SystemAccount.getCurrentUser(request);
			String bgroup = null;
			String refno = null;
			if (currentUser != null)
			{
				bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
				refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			}
			
			long seqno = getMaxSeqno(con);
			if(seqno <= 0 || request == null || bgroup == null || refno == null) return;
				
			/**
			 * get the String from the HashMap to insert into the database TODO
			 * complete pulling out information from HashMap
			 */
			String sessionId = request.getSession().getId();
			long authNo = -1;
			
			String company = "BP";
			String memScheme = bgroup + "-" + refno; 
			String memStatus = "AC";
			String gender = "N/A";		
			String marital = "Other";
			String age = "-1";
			String salary = "-1";
			String location = "N/A";
			boolean overseas = false;
			
			try
			{
				// set up the connection to Mysql database
	
				// con = connector.getDBConnFactory(Environment.WEBSTATS);
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(ACCESS_AUDIT_QUERY);
	
				// set all the parameters into the insert query
				/**
				 * TODO complete the rest of setting parameters. Be aware of the
				 * type
				 */
				pstm.setString(1, sessionId);
				pstm.setBigDecimal(2, Timer.getTime());
				pstm.setBigDecimal(3, Timer.getTimeMs());
				pstm.setInt(4, Timer.getYear());
				pstm.setInt(5, Timer.getMonth());
				pstm.setInt(6, Timer.getDay());
				pstm.setInt(7, Timer.getHour());
				pstm.setInt(8, Timer.getMin());
				pstm.setInt(9, Timer.getSec());
				pstm.setLong(10, authNo);
				pstm.setString(11, company);
				pstm.setString(12, memScheme);
				pstm.setString(13, memStatus);
				pstm.setString(14, gender);
				pstm.setString(15, marital);
				pstm.setString(16, age);
				pstm.setString(17, salary);
				pstm.setString(18, location);
				pstm.setBoolean(19, overseas);
				pstm.setLong(20, 0);	
				
				pstm.execute();
				con.commit();
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				LOG.error("Error while auditing Tax Modeller access. " + e.toString());
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{				
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						con.close();
					}
					catch (Exception e)
					{
					}
				}
			}
		}

	}

	public void doModelAudit(HttpServletRequest request, String modeledSalary, String modeledTaxRate)
	{
		Connection con = getWebstatsConnection();;
		
		if (con != null && request != null)
		{
			CmsUser currentUser = SystemAccount.getCurrentUser(request);
			String bgroup = null;
			String refno = null;
			if (currentUser != null)
			{
				bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
				refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			}
			
			
			long seqno = getMaxSeqno(con);
			if (seqno <= 0 || request == null || bgroup == null || refno == null || 
					modeledSalary == null || modeledTaxRate == null)
			{
				return;
			}
		
		
			/**
			 * get the String from the HashMap to insert into the database TODO
			 * complete pulling out information from HashMap
			 */
			String sessionId = request.getSession().getId();
			long authNo = -1;
			
			String company = "BP";
			String memScheme = bgroup + "-" + refno; 
			String memStatus = "AC";
			String gender = "N/A";		
			String marital = "Other";
			
			String salary = modeledSalary;
			String age = modeledTaxRate;
			String location = "N/A";
			boolean overseas = false;
			
			try
			{
				// set up the connection to Mysql database
	
				// con = connector.getDBConnFactory(Environment.WEBSTATS);
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(ACCESS_AUDIT_QUERY);
	
				// set all the parameters into the insert query
				/**
				 * TODO complete the rest of setting parameters. Be aware of the
				 * type
				 */
				pstm.setString(1, sessionId);
				pstm.setBigDecimal(2, Timer.getTime());
				pstm.setBigDecimal(3, Timer.getTimeMs());
				pstm.setInt(4, Timer.getYear());
				pstm.setInt(5, Timer.getMonth());
				pstm.setInt(6, Timer.getDay());
				pstm.setInt(7, Timer.getHour());
				pstm.setInt(8, Timer.getMin());
				pstm.setInt(9, Timer.getSec());
				pstm.setLong(10, authNo);
				pstm.setString(11, company);
				pstm.setString(12, memScheme);
				pstm.setString(13, memStatus);
				pstm.setString(14, gender);
				pstm.setString(15, marital);
				pstm.setString(16, age);
				pstm.setString(17, salary);
				pstm.setString(18, location);
				pstm.setBoolean(19, overseas);
				pstm.setLong(20, 0);	
				
				pstm.execute();
				con.commit();
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				LOG.error("Error while auditing Tax Modeller model. " + e.toString());
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{				
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						con.close();
					}
					catch (Exception e)
					{
					}
				}
			}
		}
	}
	
	public void doTaxChoiceAudit(HttpServletRequest request, String choice)
	{
		Connection con = getWebstatsConnection();;
		
		if (con != null && request != null)
		{
			CmsUser currentUser = SystemAccount.getCurrentUser(request);
			String bgroup = null;
			String refno = null;
			if (currentUser != null)
			{
				bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
				refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			}
			
			
			long seqno = getMaxSeqno(con);
			if (seqno <= 0 || request == null || bgroup == null || refno == null || choice == null)
			{
				return;
			}
		
		
			/**
			 * get the String from the HashMap to insert into the database TODO
			 * complete pulling out information from HashMap
			 */
			String sessionId = request.getSession().getId();
			long authNo = -1;
			
			String company = "BP";
			String memScheme = bgroup + "-" + refno; 
			String memStatus = "AC";
			String gender = "N/A";		
			String marital = "Other";
			
			String salary = "N/A";
			String age = choice;
			String location = "N/A";
			boolean overseas = false;
			
			try
			{
				// set up the connection to Mysql database
	
				// con = connector.getDBConnFactory(Environment.WEBSTATS);
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(ACCESS_AUDIT_QUERY);
	
				// set all the parameters into the insert query
				/**
				 * TODO complete the rest of setting parameters. Be aware of the
				 * type
				 */
				pstm.setString(1, sessionId);
				pstm.setBigDecimal(2, Timer.getTime());
				pstm.setBigDecimal(3, Timer.getTimeMs());
				pstm.setInt(4, Timer.getYear());
				pstm.setInt(5, Timer.getMonth());
				pstm.setInt(6, Timer.getDay());
				pstm.setInt(7, Timer.getHour());
				pstm.setInt(8, Timer.getMin());
				pstm.setInt(9, Timer.getSec());
				pstm.setLong(10, authNo);
				pstm.setString(11, company);
				pstm.setString(12, memScheme);
				pstm.setString(13, memStatus);
				pstm.setString(14, gender);
				pstm.setString(15, marital);
				pstm.setString(16, age);
				pstm.setString(17, salary);
				pstm.setString(18, location);
				pstm.setBoolean(19, overseas);
				pstm.setLong(20, 0);	
				
				pstm.execute();
				con.commit();
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				LOG.error("Error while auditing Tax Modeller select choice. " + e.toString());
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{				
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						con.close();
					}
					catch (Exception e)
					{
					}
				}
			}
		}
	}	

	private Connection getWebstatsConnection()
	{
		try
		{
			DBConnector connector = DBConnector.getInstance();
			return connector.getDBConnFactory(Environment.WEBSTATS);
		}
		catch (SQLException sqle)
		{
			LOG.error("Error while getting Webstats connection. " + sqle.toString());
		}
		
		return null;
	}

	/**
	 * @param tableName
	 * 
	 * @return
	 */
	private static long getMaxSeqno(Connection con) 
	{
        if (con != null)
        {
	        //String query = "select max(SEQNO) AS CASENO FROM " + tableName
	        try 
	        {
	            long caseCode = 0;
	            DBConnector connector = DBConnector.getInstance();
	            con = connector.getDBConnFactory(Environment.WEBSTATS);
	
	            // con = connector.getDBConnFactory(Environment.WEBSTATS);
	            Statement stm = con.createStatement();
	            ResultSet rs = stm.executeQuery(GET_MAX_SEQNO_QUERY);
	            if (rs.next()) 
	            {
	                caseCode = rs.getLong("CASENO");
	            }
	            return caseCode;
	            
	        } 
	        catch (Exception e) 
	        {
	            // TODO: handle exception
	            LOG.error("Error while getting max seqno from BP_STATS_POS table: " + e.toString());
	        } 
	        finally 
	        {
	            if (con != null) 
	            {
	                try {
	                    if (con != null)
	                    {
	                    	con.close();
	                    }
	                } catch (Exception ex) {
	                    LOG.info("Error while closing Webstats connection");
	                }
	            }
	        }
        }
        
        return 0;
    }
	
	public void doAETestModelAudit(HttpServletRequest request, Date systemDate, double modelledSalary, int modelledAccrual, int retireAge, 
			PensionDetails pensionDetails, TaxYear currentTaxYear, TaxYear nextTaxYear)
	{
		Connection con = getWebstatsConnection();;
		
		if (con != null && request != null)
		{
			CmsUser currentUser = SystemAccount.getCurrentUser(request);
			String bgroup = null;
			String refno = null;
			if (currentUser != null)
			{
				bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
				refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			}
			
			
			long seqno = getMaxSeqno(con);
			if (seqno <= 0 || request == null || bgroup == null || refno == null || 
					systemDate == null || pensionDetails == null || currentTaxYear == null || nextTaxYear == null)
			{
				return;
			}
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(systemDate);
		
			/**
			 * get the String from the HashMap to insert into the database TODO
			 * complete pulling out information from HashMap
			 */
			String sessionId = request.getSession().getId();
			long authNo = -5;
						
			String memScheme = bgroup + "-" + refno; 
			String location = "" + modelledAccrual;									
			String salary = "" + modelledSalary;
			String age = "" + retireAge;
			String marital = "" + Math.round(pensionDetails.getReducedPension());
			String gender = "" + Math.round(pensionDetails.getMaxSchemeCash());
			String company = "" + (currentTaxYear.getaAExcess() > 0 ?  Math.round(currentTaxYear.getaAExcess()) : 0);
			String memStatus = "" + (nextTaxYear.getaAExcess() > 0 ?  Math.round(nextTaxYear.getaAExcess()) : 0);
			boolean overseas = false;
			
			try
			{
				// set up the connection to Mysql database
	
				// con = connector.getDBConnFactory(Environment.WEBSTATS);
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(ACCESS_AUDIT_QUERY);
	
				// set all the parameters into the insert query
				/**
				 * TODO complete the rest of setting parameters. Be aware of the
				 * type
				 */
				pstm.setString(1, sessionId);
				pstm.setLong(2, 0);
				pstm.setLong(3, 0);
				pstm.setInt(4, calendar.get(Calendar.YEAR));
				pstm.setInt(5, calendar.get(Calendar.MONTH) + 1);
				pstm.setInt(6, calendar.get(Calendar.DAY_OF_MONTH));
				pstm.setInt(7, 0);
				pstm.setInt(8, 0);
				pstm.setInt(9, 0);
				pstm.setLong(10, authNo);
				pstm.setString(11, company);
				pstm.setString(12, memScheme);
				pstm.setString(13, memStatus);
				pstm.setString(14, gender);
				pstm.setString(15, marital);
				pstm.setString(16, age);
				pstm.setString(17, salary);
				pstm.setString(18, location);
				pstm.setBoolean(19, overseas);
				pstm.setLong(20, 0);	
				
				pstm.execute();
				con.commit();
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				LOG.error("Error while auditing AE Modeller. " + e.toString());
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{				
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						con.close();
					}
					catch (Exception e)
					{
					}
				}
			}
		}
	}	
}
