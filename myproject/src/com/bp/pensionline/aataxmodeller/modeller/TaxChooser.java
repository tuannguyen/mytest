package com.bp.pensionline.aataxmodeller.modeller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

public class TaxChooser
{
	public static final Log LOG = CmsLog.getLog(MemberLoader.class);
	
	private static String INSERT_TAX_CHOICE_QUERY = "Insert into add_hist_data (BGROUP, REFNO, DATA_TYPE, AHD01D, AHD01X, SEQNO) values (?, ?, 'AACHOICE', sysdate, ?, ?)";
	
	private static String SELECT_TAX_CHOICE_NEXT_SEQNO_QUERY = "Select nvl(max(SEQNO), 0) from add_hist_data where BGROUP = ? and REFNO = ? and DATA_TYPE = 'AACHOICE'";
	
	/**
	 * Get next seqno for tax choice
	 * @return
	 */
	private int getTaxChoiceNextSeqno (String bGroup, String refno)
	{
		int seqno = 0;
		
		Connection aquilaDBConn = null;
		
		try
		{						
			//aquilaDBConn = connector.getDBConnFactory(Environment.AQUILA);
			aquilaDBConn = getAquilaConnection();
						
			if (aquilaDBConn != null)
			{
				// load member detail info				
				PreparedStatement nextSeqnoPS = aquilaDBConn.prepareStatement(SELECT_TAX_CHOICE_NEXT_SEQNO_QUERY);
				nextSeqnoPS.setString(1, bGroup);
				nextSeqnoPS.setString(2, refno);
				ResultSet memberSIRs = nextSeqnoPS.executeQuery();								
				
				if (memberSIRs.next())
				{
					seqno = memberSIRs.getInt(1);
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("AATAX - Error getting next seqno for tax choice: " + e.toString());			
		}
		finally
		{
			try 
			{
				if (aquilaDBConn != null) aquilaDBConn.close();//con.close();
			} 
			catch (Exception ce) 
			{
				LOG.error("AATAX - Error getting next seqno for tax choice: " + ce.toString());
			}
		}
		
		return (seqno + 1);		
	}
	
	/**
	 * Update tax choice for member
	 * @param taxChoice
	 * @return
	 */
	public boolean updateTaxChoice (String bGroup, String refno, String taxChoice)
	{
		Connection aquilaDBConn = null;
		boolean result = false;
		int seqno = getTaxChoiceNextSeqno(bGroup, refno); 
		try
		{						
			//aquilaDBConn = connector.getDBConnFactory(Environment.AQUILA);
			aquilaDBConn = getAquilaConnection();
			
			if (aquilaDBConn != null)
			{
				LOG.info("Next seqno: " + seqno);
				
				if (seqno > 0)
				{
					aquilaDBConn.setAutoCommit(false);
					// load member detail info				
					PreparedStatement updateTaxChoicePS = aquilaDBConn.prepareStatement(INSERT_TAX_CHOICE_QUERY);
					updateTaxChoicePS.setString(1, bGroup);
					updateTaxChoicePS.setString(2, refno);
					updateTaxChoicePS.setString(3, taxChoice);
					updateTaxChoicePS.setInt(4, seqno);
					LOG.info("Next seqno 1 : " + seqno);
					updateTaxChoicePS.executeUpdate();
					LOG.info("Next seqno 2: " + seqno);
					aquilaDBConn.commit();
					aquilaDBConn.setAutoCommit(true);
					result = true;
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("AATAX - Error while updating tax choice for member " + bGroup +"-" + refno + ": " + e.toString());
			
			try
			{
				aquilaDBConn.rollback();
			}
			catch (Exception ex)
			{
			}			
		}
		finally
		{
			try 
			{
				if (aquilaDBConn != null) aquilaDBConn.close();//con.close();
			} 
			catch (Exception ce) 
			{
				LOG.error("AATAX - Error while closing aquila connection in updating tax choice: " + ce.toString());
			}
		}
		
		return result;
	}
	
	private Connection getAquilaConnection() throws SQLException
	{
		//com.bp.pensionline.aataxmodeller.db.DBConnector testDbConnector = com.bp.pensionline.aataxmodeller.db.DBConnector.getInstance();
		DBConnector connector = DBConnector.getInstance();
		return connector.getDBConnFactory(Environment.AQUILA);
	}	
}
