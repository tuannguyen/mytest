package com.bp.pensionline.aataxmodeller.modeller;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.aataxmodeller.util.OpenCmsFileUtil;
import com.bp.pensionline.aataxmodeller.dto.AbsenceHistory;
import com.bp.pensionline.aataxmodeller.dto.AugmentationHistory;
import com.bp.pensionline.aataxmodeller.dto.ComFactor;
import com.bp.pensionline.aataxmodeller.dto.ERFFactor;
import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.SalaryHistory;
import com.bp.pensionline.aataxmodeller.dto.SchemeHistory;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

public class MemberLoader
{
	public static final Log LOG = CmsLog.getLog(MemberLoader.class);
	
	private static final String MEMBER_DETAIL_QUERY_FILE = "/system/modules/com.bp.pensionline.aataxmodeller/config/member/member_detail.sql";
	private static final String ABSENCE_HISTORY_QUERY_FILE = "/system/modules/com.bp.pensionline.aataxmodeller/config/member/absence_history.sql";
	private static final String SCHEME_HISTORY_QUERY_FILE = "/system/modules/com.bp.pensionline.aataxmodeller/config/member/scheme_history.sql";
	private static final String AUGMENTATION_HISTORY_QUERY_FILE = "/system/modules/com.bp.pensionline.aataxmodeller/config/member/augmentation_history.sql";
	
	public static final String BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR = "([:]{1}[Bb]{1}[Gg]{1}[rR]{1}[Oo]{1}[Uu]{1}[Pp]{1})";
	public static final String REFNO_PARAMETER_PATTERN_WITH_COMPARATOR = "([:]{1}[Rr]{1}[Ee]{1}[Ff]{1}[Nn]{1}[Oo]{1})";

	private static final String MEMBER_SI_QUERY = "select BD29X from BASIC where bgroup = :bgroup and refno = :refno";
	
	// load queries from sql files
	private static String memberDetailQuery = null;//OpenCmsFileUtil.readTextFile(MEMBER_DETAIL_QUERY_FILE); //readQueryFromFile("F:/member_detail.sql");//
	private static String absenceHistoryQuery = null;//OpenCmsFileUtil.readTextFile(ABSENCE_HISTORY_QUERY_FILE); //readQueryFromFile("F:/absence_history.sql"); //
	private static String schemeHistoryQuery = null; //OpenCmsFileUtil.readTextFile(SCHEME_HISTORY_QUERY_FILE); //readQueryFromFile("F:/scheme_history.sql"); //
	private static String augmentationHistoryQuery = null; //OpenCmsFileUtil.readTextFile(SCHEME_HISTORY_QUERY_FILE); //readQueryFromFile("F:/scheme_history.sql"); //
	
	public static final String comFactorQuery = "select " +
			"case when FI01X = '01JW' then 'M' when FI01X = '01JX' then 'F' end as \"Gender\", " +
			"FI02I as \"NRA\", " +
			"FI03E as \"Value\" " +
			"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01JX','01JW')";
	
	// Male and Female use the same ERFs
	public static final String erfFactorQueryTable1 = "select " +
			//"case when FI01X = '01G6' then 'M' when FI01X = '01G7' then 'F' end as \"Gender\", " +
			"FI02I as \"NRA\", " +
			"FI03E as \"Value\" " +
			"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01G6')";
	
	public static final String erfFactorQueryTable2 = "select " +
		//"case when FI01X = '01GB' then 'M' when FI01X = '01GC' then 'F' end as \"Gender\", " +
		"FI02I as \"NRA\", " +
		"FI03E as \"Value\" " +
		"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01GB') and FI02I < 660";
	
	public static final String erfFactorQueryTable3 = "select " +
			//"case when FI01X = '01H0' then 'M' when FI01X = '01H1' then 'F' end as \"Gender\", " +
			"FI02I as \"NRA\", " +
			"FI03E as \"Value\" " +
			"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01H0')";
	
	public static final String erfFactorQueryTable5 = "select " +
		//"case when FI01X = '01JF' then 'M' when FI01X = '01JG' then 'F' end as \"Gender\", " +
		"FI02I as \"NRA\", " +
		"FI03E as \"Value\" " +
		"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01JF')";
	
	public static final String erfFactorQueryTable6 = "select " +
		//"case when FI01X = '009A' then 'M' when FI01X = '009F' then 'F' end as \"Gender\", " +
		"FI02I as \"NRA\", " +
		"FI03E as \"Value\" " +
		"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('009A')";
	
	public static final String veraFactorQueryTable = "select " +
		"FI01X as \"Gender\", " +
		"FI02I as \"NRA\", " +
		"FI03E as \"Value\" " +
		"from  FACTOR_VALUES_INTEGER where  bgroup = 'BPF' and FI01X in ('01HJ', '01HG', '01HD', '01HA', '01H7', '01H4')";	
	
    public static final String salaryHistoryQuery = "select mh02d as \"Effective\", mh03c as \"Salary\" " +
		"from MISCELLANEOUS_HISTORY " +
		"where bgroup =:bgroup and refno = :refno and mh03c is not null order by mh02d desc";
	
	private String bGroup;
	private String refno;
	
	public MemberLoader(String bGroup, String refno)
	{
		this.bGroup = bGroup;
		this.refno = refno;
	}
	
	/**
	 * Get member's SI indicator number
	 * @return
	 */
	public int getMemberSecurityLevel ()
	{
		int si = 0;
		String memberSIQuery = MEMBER_SI_QUERY;
		memberSIQuery = memberSIQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
		memberSIQuery = memberSIQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");
		
		Connection aquilaDBConn = null;
		
		try
		{						
			//aquilaDBConn = connector.getDBConnFactory(Environment.AQUILA);
			aquilaDBConn = getAquilaConnection();
						
			if (aquilaDBConn != null)
			{
				// load member detail info				
				PreparedStatement memberSIPs = aquilaDBConn.prepareStatement(memberSIQuery);
				ResultSet memberSIRs = memberSIPs.executeQuery();								
				
				if (memberSIRs.next())
				{
					si = memberSIRs.getInt(1);
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("AATAX - Error getting member SI: " + e.toString());			
		}
		finally
		{
			try 
			{
				if (aquilaDBConn != null) aquilaDBConn.close();//con.close();
			} 
			catch (Exception ce) 
			{
				LOG.error("AATAX - Error while closing aquila connection in loading member: " + ce.toString());
			}
		}
		
		return si;
	}
	
	/**
	 * Load a member which has reference of bGroup and refno
	 * @return
	 */
	public MemberDetail loadMember()
	{
		MemberDetail member = null;
				
		Connection aquilaDBConn = null;
		
		try
		{						
			if (Headroom.isDebug)
			{
				aquilaDBConn = getDirectConnection();
				memberDetailQuery = readQueryFromFile("F:/member_detail.sql");//
				absenceHistoryQuery = readQueryFromFile("F:/absence_history.sql"); //
				schemeHistoryQuery = readQueryFromFile("F:/scheme_history.sql"); //
				augmentationHistoryQuery = readQueryFromFile("F:/augmentation_history.sql");
			}
			else
			{
				aquilaDBConn = getAquilaConnection();
				memberDetailQuery = OpenCmsFileUtil.readTextFile(MEMBER_DETAIL_QUERY_FILE);
				absenceHistoryQuery = OpenCmsFileUtil.readTextFile(ABSENCE_HISTORY_QUERY_FILE);
				schemeHistoryQuery = OpenCmsFileUtil.readTextFile(SCHEME_HISTORY_QUERY_FILE);
				augmentationHistoryQuery = OpenCmsFileUtil.readTextFile(AUGMENTATION_HISTORY_QUERY_FILE);
			}
			
			//LOG.info("aquilaDBConn: " + aquilaDBConn);

			//Log.info("memberDetailQuery: " + memberDetailQuery);
			//Log.info("absenceHistoryQuery: " + absenceHistoryQuery);
			//Log.info("schemeHistoryQuery: " + schemeHistoryQuery);			
			if (aquilaDBConn != null && bGroup != null && refno != null &&
					memberDetailQuery != null && !memberDetailQuery.trim().equals("") &&
					absenceHistoryQuery != null && !absenceHistoryQuery.trim().equals("") && 
					schemeHistoryQuery != null && !schemeHistoryQuery.trim().equals("") && 
					augmentationHistoryQuery != null && !augmentationHistoryQuery.equals(""))
			{
				// update query with member's reference
				String updatedMemberDetailQuery = memberDetailQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
				updatedMemberDetailQuery = updatedMemberDetailQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");
				
				//LOG.info("updatedMemberDetailQuery: " + updatedMemberDetailQuery);
				
				String updateAbsenceHistoryQuery = absenceHistoryQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
				updateAbsenceHistoryQuery = updateAbsenceHistoryQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");
				
				String updateSchemeHistoryQuery = schemeHistoryQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
				updateSchemeHistoryQuery = updateSchemeHistoryQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");
				
				String updateAugmentationHistoryQuery = augmentationHistoryQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
				updateAugmentationHistoryQuery = updateAugmentationHistoryQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");				
				
				// load member detail info				
				PreparedStatement memberDetailPs = aquilaDBConn.prepareStatement(updatedMemberDetailQuery);
				ResultSet memberDetailRs = memberDetailPs.executeQuery();							
				
				if (memberDetailRs.next())
				{
					member = new MemberDetail();
					member.setBGroup(bGroup);
					member.setRefno(refno);	
					
					member.setNino(memberDetailRs.getString("NINO"));
					member.setName(memberDetailRs.getString("name"));
					member.setGender(memberDetailRs.getString("Gender"));
					member.setDateOfBirth(memberDetailRs.getDate("Date of Birth"));
					member.setJoinedCompany(memberDetailRs.getDate("Joined Company"));
					member.setJoinedScheme(memberDetailRs.getDate("Joined Scheme"));
					member.setServiceQualified(memberDetailRs.getDate("Service Qualified"));
					member.setPensionableSalary(memberDetailRs.getDouble("Pensionable Salary"));
					member.setAVCFund(memberDetailRs.getDouble("AVC fund"));
					member.setAVERFund(memberDetailRs.getDouble("AVER fund"));
					member.setTVINADays(memberDetailRs.getInt("TVIN A days"));
					member.setTVINBDays(memberDetailRs.getInt("TVIN B days"));
					member.setTVINCDays(memberDetailRs.getInt("TVIN C days"));
					member.setAugmentationDays(memberDetailRs.getInt("Augmentation days"));
					member.setLTA(memberDetailRs.getDouble("Lta"));
					member.setCpi(memberDetailRs.getDouble("Cpi")/10000);	// divide to 10 thousand because data stored as 3 digit number
					member.setBankedEGP(memberDetailRs.getDouble("Banked EGP"));
				}
				
				// get member's absence history
				PreparedStatement absenceHistoryPs = aquilaDBConn.prepareStatement(updateAbsenceHistoryQuery);
				ResultSet absenceHistoryRs = absenceHistoryPs.executeQuery();
				 
				ArrayList<AbsenceHistory> absenceHistories = new ArrayList<AbsenceHistory>();
				while (absenceHistoryRs.next())
				{
					AbsenceHistory absenceHistory = new AbsenceHistory();
					absenceHistory.setType(absenceHistoryRs.getString("Absence Type"));
					absenceHistory.setFrom(absenceHistoryRs.getDate("From"));
					absenceHistory.setTo(absenceHistoryRs.getDate("To"));
					absenceHistory.setWorked(absenceHistoryRs.getDouble("Worked"));
					absenceHistory.setEmployed(absenceHistoryRs.getDouble("Employed"));
					
					// only apply for temporarily absence
					if (absenceHistory.getType().equals(AbsenceHistory.ABSENCE_TYPE_TA))							
					{
						if (absenceHistoryRs.getString("Service Indicator") != null && absenceHistoryRs.getString("Service Indicator").equalsIgnoreCase("Y"))
						{
							absenceHistory.setServiceIndicator(false);
						}
						else if (absenceHistoryRs.getString("Service Indicator") != null && absenceHistoryRs.getString("Service Indicator").equalsIgnoreCase("N"))
						{
							// take full service
							absenceHistory.setServiceIndicator(true);
							absenceHistory.setWorked(1.0);
							absenceHistory.setEmployed(1.0);
						}
					}
					else if (absenceHistory.getType().equals(AbsenceHistory.ABSENCE_TYPE_PT))
					{
						absenceHistory.setServiceIndicator(true);
						if (absenceHistoryRs.getString("Service Indicator") != null && absenceHistoryRs.getString("Service Indicator").equalsIgnoreCase("N"))
						{
							// treated as full time							
							absenceHistory.setWorked(1.0);
							absenceHistory.setEmployed(1.0);
						}
					}
					
					
					absenceHistories.add(absenceHistory);
				}

				// clean absence history due to bad SAP data
				ArrayList<AbsenceHistory> cleanedAbsenceHistories = cleanupAbsenceHistory(absenceHistories);
				member.getAbsenceHistory().addAll(cleanedAbsenceHistories);
				
				// get member's scheme history
				PreparedStatement schemeHistoryPs = aquilaDBConn.prepareStatement(updateSchemeHistoryQuery);
				ResultSet schemeHistoryRs = schemeHistoryPs.executeQuery();
				
				while (schemeHistoryRs.next())
				{
					SchemeHistory schemeHistory = new SchemeHistory();
					schemeHistory.setFrom(schemeHistoryRs.getDate("From"));
					schemeHistory.setTo(schemeHistoryRs.getDate("To"));
					schemeHistory.setCategory(schemeHistoryRs.getString("Category"));
					schemeHistory.setAccrual(schemeHistoryRs.getInt("Accrual"));
					
					member.getSchemeHistory().add(schemeHistory);
				}
				
				// get member's augmentation history
				PreparedStatement augmentationHistoryPs = aquilaDBConn.prepareStatement(updateAugmentationHistoryQuery);
				ResultSet augmentationHistoryRs = augmentationHistoryPs.executeQuery();
				
				while (augmentationHistoryRs.next())
				{
					AugmentationHistory augmentationHistory = new AugmentationHistory();
					augmentationHistory.setSub(augmentationHistoryRs.getString("Sub"));
					augmentationHistory.setType(augmentationHistoryRs.getString("Type"));
					augmentationHistory.setDays(augmentationHistoryRs.getInt("Days"));
					augmentationHistory.setAccrual(augmentationHistoryRs.getInt("Accrual"));
					
					member.getAugmentationHistory().add(augmentationHistory);
				}				
				
				// get member's factors
				PreparedStatement comFactorPs = aquilaDBConn.prepareStatement(comFactorQuery);
				ResultSet comFactorRs = comFactorPs.executeQuery();
				
				while (comFactorRs.next())
				{
					ComFactor comFactor = new ComFactor();
					comFactor.setGender(comFactorRs.getString("Gender"));
					comFactor.setNra(comFactorRs.getInt("NRA"));
					comFactor.setValue(comFactorRs.getDouble("Value"));
					
					member.getComFactors().add(comFactor);
				}
				
				Calendar calendarAt1Dec2006 = Calendar.getInstance();
				calendarAt1Dec2006.set(2006, Calendar.DECEMBER, 1, 0, 0, 0);
				calendarAt1Dec2006.set(Calendar.MILLISECOND, 0);

				// Joiners before 1-Dec-2006. Use table 1, 2 and 5
				if (member.getJoinedScheme().before(calendarAt1Dec2006.getTime()))
				{
					// get value from erfFactorTable1 & 2 query
					PreparedStatement erfFactorPs = aquilaDBConn.prepareStatement(erfFactorQueryTable1);
					ResultSet erfFactorRs = erfFactorPs.executeQuery();
					
					while (erfFactorRs.next())
					{
						ERFFactor erfFactor = new ERFFactor();
						//erfFactor.setGender(erfFactorRs.getString("Gender"));
						erfFactor.setNra(erfFactorRs.getInt("NRA"));
						erfFactor.setValue(erfFactorRs.getDouble("Value"));
						
						member.getERFFactors().add(erfFactor);
												
					}
					
					// get more ERFs factor from Without Company Consent table, in case active member retire before 55
					PreparedStatement erfFactorMorePs = aquilaDBConn.prepareStatement(erfFactorQueryTable2);
					ResultSet erfFactorMoreRs = erfFactorMorePs.executeQuery();
					
					while (erfFactorMoreRs.next())
					{
						ERFFactor erfFactor = new ERFFactor();
						//erfFactor.setGender(erfFactorMoreRs.getString("Gender"));
						erfFactor.setNra(erfFactorMoreRs.getInt("NRA"));
						erfFactor.setValue(erfFactorMoreRs.getDouble("Value"));
						
						member.getERFFactors().add(erfFactor);
												
					}
					
					
					//TVIN ERFs
					PreparedStatement tvinERFPre06FactorMorePs = aquilaDBConn.prepareStatement(erfFactorQueryTable5);
					ResultSet tvinERFPre06FactorRs = tvinERFPre06FactorMorePs.executeQuery();
					while (tvinERFPre06FactorRs.next())
					{
						ERFFactor erfFactor = new ERFFactor();
						//erfFactor.setGender(tvinERFFactorRs.getString("Gender"));
						erfFactor.setNra(tvinERFPre06FactorRs.getInt("NRA"));
						erfFactor.setValue(tvinERFPre06FactorRs.getDouble("Value"));
						
						member.getTvinErfFactors().add(erfFactor);
												
					}					
				}
				else // Joiners after 1-Dec-2006 not allowed to retire before 55. Get table 3 and 6
				{
					// get value from erfFactorTable3 & 6 query
					PreparedStatement erfFactorPs = aquilaDBConn.prepareStatement(erfFactorQueryTable3);
					ResultSet erfFactorRs = erfFactorPs.executeQuery();
					
					while (erfFactorRs.next())
					{
						ERFFactor erfFactor = new ERFFactor();
						//erfFactor.setGender(erfFactorRs.getString("Gender"));
						erfFactor.setNra(erfFactorRs.getInt("NRA"));
						erfFactor.setValue(erfFactorRs.getDouble("Value"));
						
						member.getERFFactors().add(erfFactor);
					}
					
					
					
					PreparedStatement tvinERFPos06FactorMorePs = aquilaDBConn.prepareStatement(erfFactorQueryTable6);
					ResultSet tvinERFPos06FactorRs = tvinERFPos06FactorMorePs.executeQuery();
					
					while (tvinERFPos06FactorRs.next())
					{
						ERFFactor erfFactor = new ERFFactor();
						//erfFactor.setGender(tvinERFFactorRs.getString("Gender"));
						erfFactor.setNra(tvinERFPos06FactorRs.getInt("NRA"));
						erfFactor.setValue(tvinERFPos06FactorRs.getDouble("Value"));
						
						member.getTvinErfFactors().add(erfFactor);
												
					}					
										
				}
				
				
				// load VERA factors
				PreparedStatement veraFactorPs = aquilaDBConn.prepareStatement(MemberLoader.veraFactorQueryTable);				
				ResultSet veraFactorRs = veraFactorPs.executeQuery();
				
				while (veraFactorRs.next())
				{
					ERFFactor veraFactor = new ERFFactor();
					veraFactor.setGender(veraFactorRs.getString("Gender"));
					veraFactor.setNra(veraFactorRs.getInt("NRA"));
					veraFactor.setValue(veraFactorRs.getDouble("Value"));
					
					member.getVeraFactors().add(veraFactor);
				}
				
				// load salary history
				// update query with member's reference
				String updatedSalaryHistoryQuery = salaryHistoryQuery.replaceAll(BGROUP_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + bGroup + "'");
				updatedSalaryHistoryQuery = updatedSalaryHistoryQuery.replaceAll(REFNO_PARAMETER_PATTERN_WITH_COMPARATOR, "'" + refno+ "'");
				PreparedStatement salaryHistoryPs = aquilaDBConn.prepareStatement(updatedSalaryHistoryQuery);				
				ResultSet salaryHistoryRs = salaryHistoryPs.executeQuery();
				
				while (salaryHistoryRs.next())
				{
					SalaryHistory salaryHistory = new SalaryHistory();
					salaryHistory.setEffective(salaryHistoryRs.getDate("Effective"));
					salaryHistory.setSalary(salaryHistoryRs.getDouble("Salary"));
					
					member.getSalaryHistory().add(salaryHistory);
				}				
			}			
		}
		catch (Exception e) 
		{
			LOG.error("AATAX - Error loading member: " + e.toString());			
		}
		finally
		{
			try 
			{
				if (aquilaDBConn != null) aquilaDBConn.close();//con.close();
			} 
			catch (Exception ce) 
			{
				LOG.error("AATAX - Error while closing aquila connection in loading member: " + ce.toString());
			}
		}
		
		return member;
	}
	
	/**
	 * Clean up absence history
	 */
	private ArrayList<AbsenceHistory> cleanupAbsenceHistory (ArrayList<AbsenceHistory> absenceHistories)
	{
		ArrayList<AbsenceHistory> cleanupAbsences = new ArrayList<AbsenceHistory>();
		
		if (absenceHistories.size() > 1)
		{
			AbsenceHistory currentAbsence = null;
			for (int i = 0; i < absenceHistories.size(); i++)
			{
				if (currentAbsence == null)
				{
					currentAbsence = absenceHistories.get(i);
				}
				
				if (i < absenceHistories.size() - 1)
				{
					AbsenceHistory nextAbsence = absenceHistories.get(i + 1);
				
					boolean fteEqual = (currentAbsence.getWorked() == nextAbsence.getWorked()) 
						&& (currentAbsence.getEmployed() == nextAbsence.getEmployed());
						
					
					Date absence1From = currentAbsence.getFrom();
					Date absence2To = nextAbsence.getTo();
					
					if (fteEqual && absence1From != null && absence2To != null && DateUtil.getPreviousDay(absence1From).compareTo(absence2To) <= 0)
					{
						// merge 2 absence
						currentAbsence.setFrom(nextAbsence.getFrom());
					}
					else
					{
						// add absence
						cleanupAbsences.add(currentAbsence);
						currentAbsence = null;
					}
				}
				else
				{
					cleanupAbsences.add(currentAbsence);
				}
			}
		}
		else
		{
			cleanupAbsences.addAll(absenceHistories);
		}		
		
		return cleanupAbsences;
	}	
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static String readQueryFromFile (String fileName)
	{
		String query = null;
		StringBuffer queryBuffer = null;
		
		try
		{
			File queryFile = new File(fileName);
			if (queryFile.exists())
			{	
				queryBuffer = new StringBuffer();
				FileInputStream queryFileInputStream = new FileInputStream(queryFile);
				String NL = System.getProperty("line.separator");
			    Scanner scanner = new Scanner(queryFileInputStream, "UTF-8");
			    try 
			    {
			    	while (scanner.hasNextLine())
			    	{
			    		queryBuffer.append(scanner.nextLine() + NL);
			    	}
			    }
			    finally
			    {
			    	scanner.close();
			    }
				
				query = queryBuffer.toString();
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while reading query from " + fileName +  ": " + e.toString());
		}
		
		return query;
	}	
	
	public boolean checkAquilaConnection()
	{
		boolean connectionOk = false;
		//com.bp.pensionline.aataxmodeller.db.DBConnector testDbConnector = com.bp.pensionline.aataxmodeller.db.DBConnector.getInstance();

		DBConnector connector = DBConnector.getInstance();
		Connection conn = null;
		try
		{
			conn = connector.getDBConnFactory(Environment.AQUILA);
			if (conn != null)
			{
				connectionOk = true;
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while getting Aquila connection for tax modeller: " + e.toString());
		}
		finally
		{
			try
			{
				if (conn != null) conn.close();
			}
			catch (Exception e2)
			{
			}			
		}

		return connectionOk;
	}	
	
	private Connection getAquilaConnection() throws SQLException
	{
		//com.bp.pensionline.aataxmodeller.db.DBConnector testDbConnector = com.bp.pensionline.aataxmodeller.db.DBConnector.getInstance();
		DBConnector connector = DBConnector.getInstance();
		return connector.getDBConnFactory(Environment.AQUILA);
		
//		return getDirectConnection();
	}
	
//	public static void main(String[] args) throws ParseException
//	{
//		//MemberLoader memberLoader = new MemberLoader("BPF", "0007244");
//		//MemberDetail member = memberLoader.loadMember();
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
//		
//		MemberDetail member = new MemberDetail();
//		member.setBGroup("BPF");
//		member.setBGroup("0007244");
//		member.setDateOfBirth(dateFormat.parse("28-Jun-1962"));
//		member.setJoinedCompany(dateFormat.parse("26-Aug-2003"));
//		member.setJoinedScheme(dateFormat.parse("26-Aug-2003"));
//		
//		SchemeHistory schemeHistory = new SchemeHistory();
//		schemeHistory.setFrom(dateFormat.parse("1-Jan-2009"));
//		schemeHistory.setTo(null);
//		schemeHistory.setCategory("6060");
//		schemeHistory.setAccrual(60);
//		member.getSchemeHistory().add(schemeHistory);
//		
//		schemeHistory = new SchemeHistory();
//		schemeHistory.setFrom(dateFormat.parse("1-Jun-2007"));
//		schemeHistory.setTo(dateFormat.parse("31-Dec-2008"));
//		schemeHistory.setCategory("6045");
//		schemeHistory.setAccrual(45);
//		member.getSchemeHistory().add(schemeHistory);
//		
//		schemeHistory = new SchemeHistory();
//		schemeHistory.setFrom(dateFormat.parse("1-Dec-2006"));
//		schemeHistory.setTo(dateFormat.parse("31-May-2007"));
//		schemeHistory.setCategory("6060");
//		schemeHistory.setAccrual(60);
//		member.getSchemeHistory().add(schemeHistory);
//		
//		schemeHistory = new SchemeHistory();
//		schemeHistory.setFrom(dateFormat.parse("26-Aug-2003"));
//		schemeHistory.setTo(dateFormat.parse("30-Nov-2006"));
//		schemeHistory.setCategory("BP60");
//		schemeHistory.setAccrual(60);
//		member.getSchemeHistory().add(schemeHistory);
//		
//		LOG.info("Member loaded: " + member.getPensionableSalary());
//		
//		Headroom headroom = new Headroom(member);
//		LOG.info("Headroom done: " + headroom);
//	}
//	
	   public static Connection getDirectConnection() {
		   Connection conn = null;
		   try {
			   Class.forName("oracle.jdbc.driver.OracleDriver");    
		       String url = "jdbc:oracle:thin:@127.1.1.204:4522:BP101S";  
			   //String url = "calcURL=jdbc:oracle:thin:@127.1.1.203:4524:BP101P";
		       conn = DriverManager.getConnection(url,"BPPL4CMS	", "BPPL4CMS");
		   } catch (Exception e) {
			   e.printStackTrace();
		   }	
		  return conn; 
	   }
}
