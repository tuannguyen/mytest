package com.bp.pensionline.aataxmodeller.test;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;

public class TestMemberDetail {
	public static void main(String args[]){
		Headroom.isDebug = true;
		MockMember mockMem = new MockMember("BPF", "0000021");
		
		MemberLoader memberLoader = new MemberLoader(mockMem.getMemberID(),mockMem.getRefNo());

		MemberDetail memberDetail = memberLoader.loadMember();
		System.out.println(memberDetail.getBGroup());
		System.out.println(memberDetail.getRefno());
		System.out.println(memberDetail.getNino());
		System.out.println(memberDetail.getName());
		System.out.println(memberDetail.getGender());
		System.out.println(memberDetail.getDateOfBirth().toString());
		System.out.println(memberDetail.getJoinedCompany().toString());
		System.out.println(memberDetail.getJoinedScheme().toString());
		System.out.println(memberDetail.getPensionableSalary());
		System.out.println(memberDetail.getAVCFund());
		System.out.println(memberDetail.getAVERFund());
		System.out.println(memberDetail.getTVINADays());
		System.out.println(memberDetail.getTVINBDays());
		System.out.println(memberDetail.getTVINCDays());
		System.out.println(memberDetail.getAugmentationDays());
		System.out.println(memberDetail.getLTA());
		System.out.println(memberDetail.getBankedEGP());
		System.out.println(memberDetail.getCpi());
		System.out.println(memberDetail.getAbsenceHistory().toString());
		System.out.println(memberDetail.getSchemeHistory().toString());
		System.out.println(memberDetail.getSalaryHistory().toString());
		System.out.println(memberDetail.getComFactors().toString());
		System.out.println(memberDetail.getERFFactors().toString());
		System.out.println(memberDetail.getTvinErfFactors().toString());
		System.out.println(memberDetail.getVeraFactors().toString());
	}
}
