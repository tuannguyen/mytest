package com.bp.pensionline.aataxmodeller.test;

import java.io.Serializable;
import java.util.Date;

public class MockAbsenceHistory implements Serializable{
	public static final long serialVersionUID = 0L;
	
	public static final String ABSENCE_TYPE_PT = "PT";
	public static final String ABSENCE_TYPE_TA = "TA";
	
	private String type;
	private Date from;
	private Date to;
	private double worked;
	private double employed;
	private boolean serviceIndicator = true;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = from;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
	public double getWorked() {
		return worked;
	}
	public void setWorked(double worked) {
		this.worked = worked;
	}
	public double getEmployed() {
		return employed;
	}
	public void setEmployed(double employed) {
		this.employed = employed;
	}
	public boolean isServiceIndicator() {
		return serviceIndicator;
	}
	public void setServiceIndicator(boolean serviceIndicator) {
		this.serviceIndicator = serviceIndicator;
	}
}
