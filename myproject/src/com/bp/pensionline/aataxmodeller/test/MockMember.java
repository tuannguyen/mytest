package com.bp.pensionline.aataxmodeller.test;

public class MockMember {
private String refNo;
	
	private String memberID;

	/**
	 * @param refNo the refNo to set
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	/**
	 * @return the refNo
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	/**
	 * @return the memberID
	 */
	public String getMemberID() {
		return memberID;
	}
	
	/**
	 * 
	 * @param refNo
	 * @param memID
	 */
	public MockMember(String memID, String refNo){
		this.refNo = refNo;
		this.memberID = memID;
	}
}
