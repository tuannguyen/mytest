package com.bp.pensionline.aataxmodeller.test;

import java.util.Calendar;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.util.DateUtil;

public class TestHeadroomWithouMockObject {
	public static void main(String args[]){
		Headroom.isDebug = true;
		MemberLoader memberLoader = new MemberLoader("BPF","0000021");
		MemberDetail memberDetail = memberLoader.loadMember();
	
		Calendar calendar = Calendar.getInstance();
		Headroom headroom = new Headroom(memberDetail);
		
		headroom.setCashPercent(100);
		headroom.calculate();
		headroom.debugTranches(headroom.getServiceTranches());
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		double soyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(calendar.getTime()));
		double eoyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
	
		System.out.println(soyServiceYears);
		System.out.println(eoyServiceYears);
		System.out.println(pensionDetails.getCashLumpSum());
		
		TaxModeller taxModeller = new TaxModeller();
		taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		
		TaxYear taxYear = taxModeller.calculateTaxYearByServiceYears( 
				soyServiceYears, memberDetail.getSalaryBefore(DateUtil.getFirstDayOfThisYear(calendar.getTime())),
				eoyServiceYears, headroom.getFps(), 0, 0.50, 0.02, 16, 50000);
		System.out.println(taxYear.getTaxAmount());
		
		System.out.println(taxYear.getAACheck());
		System.out.println(taxYear.getaAExcess());
		System.out.println(taxYear.getaAFactor());
		System.out.println(taxYear.getAccrualRate());
		System.out.println(taxYear.getAccrualRate());
		System.out.println(taxYear.getAnnualAllowance());
		System.out.println(taxYear.getCpi());
		System.out.println(taxYear.getCpiReval());
		System.out.println(taxYear.getEoyBenefit());
		System.out.println(taxYear.getEoySalary());
		System.out.println(taxYear.getIncrease());
		System.out.println(taxYear.getSalaryIncrease());
		System.out.println(taxYear.getSoyBenefit());
		System.out.println(taxYear.getSoySalary());
		System.out.println(taxYear.getTaxRate());
		System.out.println(taxYear.getTaxYear().toString());
		
		TaxYear taxNextYear = taxModeller.calculateTaxYearByServiceDays(25.35, 326000, 60, 0.05, 0, 0.5, 0.02, 16, 50000);
		System.out.println(taxNextYear.getAACheck());
		System.out.println(taxNextYear.getaAExcess());
		System.out.println(taxNextYear.getaAFactor());
		System.out.println(taxNextYear.getAccrualRate());
		System.out.println(taxNextYear.getAccrualRate());
		System.out.println(taxNextYear.getAnnualAllowance());
		System.out.println(taxNextYear.getCpi());
		System.out.println(taxNextYear.getCpiReval());
		System.out.println(taxNextYear.getEoyBenefit());
		System.out.println(taxNextYear.getEoySalary());
		System.out.println(taxNextYear.getIncrease());
		System.out.println(taxNextYear.getSalaryIncrease());
		System.out.println(taxNextYear.getSoyBenefit());
		System.out.println(taxNextYear.getSoySalary());
		System.out.println(taxNextYear.getTaxRate());
		System.out.println(taxNextYear.getTaxYear().toString());
	}
}
