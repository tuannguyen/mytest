package com.bp.pensionline.aataxmodeller.test;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;

public class MockObjectFile {
public MockObjectFile(){
		
	}
	public static void main(String[] args){
		MockObjectFile file = new MockObjectFile();
		file.saveObjectToFile();
	}
	public void saveObjectToFile(){
		Headroom.isDebug = true;
		MockObject mockObject = new MockObject(60, 60, 114400);
		MockMember mockMem = new MockMember("BPF", "0000021");
		
		MemberLoader memberLoader = new MemberLoader(mockMem.getMemberID(),mockMem.getRefNo());
		
		MemberDetail memberDetail = memberLoader.loadMember();
		try {
			FileOutputStream fout = new FileOutputStream("D:/memberDetail.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(memberDetail);
			oos.close();
			System.out.println("Save Object Successfull!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mockObject.getDoR(memberDetail));
		
		Headroom headroom = new Headroom(memberDetail);
		
		headroom.setAccrual(mockObject.getAccurualRate());
		headroom.setDoR(mockObject.getDoR(memberDetail));
		headroom.setCashPercent(50);
		headroom.setFps(mockObject.getCurrentSalary());
		headroom.calculate();
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		try {
			FileOutputStream fout = new FileOutputStream("D:/pensionDetail.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(pensionDetails);
			oos.close();
			System.out.println("Save Object(PensionDetails) Successfull!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
