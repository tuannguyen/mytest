package com.bp.pensionline.aataxmodeller.test;

import java.io.Serializable;

public class MockComFactor implements Serializable{
	public static final long serialVersionUID = 0L;
	
	private String gender;
	private int nra;
	private double value;
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getNra() {
		return nra;
	}
	public void setNra(int nra) {
		this.nra = nra;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
