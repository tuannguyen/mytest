package com.bp.pensionline.aataxmodeller.test;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;

public class TestPensionDetail {
	public static void main(String args[]){
		Headroom.isDebug = true;
		MockMember mockMem = new MockMember("BPF", "0000021");
		
		MemberLoader memberLoader = new MemberLoader(mockMem.getMemberID(),mockMem.getRefNo());

		MemberDetail memberDetail = memberLoader.loadMember();
	
		Headroom headroom = new Headroom(memberDetail);
		headroom.setCashPercent(100);
		headroom.calculate();
		headroom.debugTranches(headroom.getServiceTranches());
		PensionDetails pensionDetails = headroom.getPensionDetails();
		
		System.out.println(pensionDetails.getDoR().toString());
		System.out.println(pensionDetails.getCashLumpSum());
		System.out.println(pensionDetails.getMaxSchemeCash());
		System.out.println(pensionDetails.getPensionPot());
		System.out.println(pensionDetails.getPensionWithChosenCash());
		System.out.println(pensionDetails.getReducedPension());
	}
}
