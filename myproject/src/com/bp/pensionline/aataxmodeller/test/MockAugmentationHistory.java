package com.bp.pensionline.aataxmodeller.test;

import java.io.Serializable;

public class MockAugmentationHistory implements Serializable{
	public static final long serialVersionUID = 0L;
	
	public static final String AUGMENTATION_TYPE_WEST = "WEST";
	public static final String AUGMENTATION_TYPE_OBPC = "OBPC";
	public static final String AUGMENTATION_TYPE_OEST = "OEST";
	public static final String AUGMENTATION_TYPE_EAST = "EAST";
	
	private String sub;
	private String type;
	private int days;
	private int accrual;
	
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public int getAccrual() {
		return accrual;
	}
	public void setAccrual(int accrual) {
		this.accrual = accrual;
	}
}
