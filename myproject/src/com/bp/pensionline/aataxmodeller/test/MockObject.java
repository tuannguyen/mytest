package com.bp.pensionline.aataxmodeller.test;

import java.util.Calendar;
import java.util.Date;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;

public class MockObject {
private int retirementAge;
	
	private int accrualRate;
	
	private double currentSalary;

	/**
	 * @param retirementAge the retirementAge to set
	 */
	public void setRetirementAge(int retirementAge) {
		this.retirementAge = retirementAge;
	}

	/**
	 * @return the retirementAge
	 */
	public int getRetirementAge() {
		return retirementAge;
	}

	/**
	 * @param accurualRate the accurualRate to set
	 */
	public void setAccurualRate(int accrualRate) {
		this.accrualRate = accrualRate;
	}

	/**
	 * @return the accurualRate
	 */
	public int getAccurualRate() {
		return accrualRate;
	}

	/**
	 * @param currentSalary the currentSalary to set
	 */
	public void setCurrentSalary(double currentSalary) {
		this.currentSalary = currentSalary;
	}

	/**
	 * @return the currentSalary
	 */
	public double getCurrentSalary() {
		return currentSalary;
	}
	
	/**
	 * 
	 * @param retireAge
	 * @param accrual
	 * @param salary
	 */
	public MockObject(int retireAge, int accrual, double salary){
		this.retirementAge = retireAge;
		this.accrualRate = accrual;
		this.currentSalary = salary;
	}
	
	/**
	 * 
	 * @param memberDetail
	 * @return date of retirement
	 */
	public Date getDoR(MemberDetail memberDetail){
		Calendar cal = Calendar.getInstance();
		cal.setTime(memberDetail.getDateOfBirth());
		int year = cal.get(Calendar.YEAR);
		int yor = year + this.retirementAge;
		cal.set(yor, cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
		return cal.getTime();
	}
}
