package com.bp.pensionline.aataxmodeller.test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Calendar;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.util.DateUtil;

public class TestHeadRoom {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//System.out.println("Hello");
		testCashLumpSum100Age60();
		//testCashLumpSum100Age65();
		//testCashLumpSum50Age65();
		//testCashLumpSum50Age60();
	}
	
	/**
	 * Get Data of MemberDetail from file
	 * @return
	 */
	public static MemberDetail getObjectData(){
		   MemberDetail mem;
		   try{
			   FileInputStream fin = new FileInputStream("D:/memberDetail.ser");
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   mem = (MemberDetail) ois.readObject();
			   ois.close();
			   return mem;
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	   }
	
	/**
	 * Get Data of PensionDetails from file
	 * @return
	 */
	/*public PensionDetails getPensionData(){
			PensionDetails pension;
			try{
				 
				   FileInputStream fin = new FileInputStream("D:/pensionDetail.ser");
				   ObjectInputStream ois = new ObjectInputStream(fin);
				   pension = (PensionDetails) ois.readObject();
				   ois.close();
		 
				   return pension;
		 
			   }catch(Exception ex){
				   ex.printStackTrace();
				   return null;
			   }
	}*/
	
	/**
	 * Test One
	 * Cash Lump Sum 50% - RetirementAge - 65
	 * Created on 12/08/2011
	 * @throws Exception 
	 */
	public static void testCashLumpSum50Age65() throws Exception{
		Headroom.isDebug = true;
		MockObject mockObject = new MockObject(65,45,101040);
		MemberDetail mem = getObjectData();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mockObject.getDoR(mem));
		
		Headroom headroom = new Headroom(mem);
		
		headroom.setAccrual(mockObject.getAccurualRate());
		headroom.setDoR(mockObject.getDoR(mem));
		headroom.setCashPercent(50);
		headroom.setFps(mockObject.getCurrentSalary());
		headroom.calculate();
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		double soyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(calendar.getTime()));
		double eoyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		
		//Cash Lump Sum
		double cashLumpSum;
		cashLumpSum = pensionDetails.getCashLumpSum();
		System.out.println(cashLumpSum);		
		
		//Annual Pension = Reduced Pension
		double pension;
		pension = pensionDetails.getReducedPension();
		System.out.println(pension);
		//Pension With Max Cash = Max Scheme Cash
		double maxCash;
		maxCash = pensionDetails.getMaxSchemeCash();
		System.out.println(maxCash);
		
		//Tax
		TaxModeller taxModeller = new TaxModeller();
		taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		TaxYear taxYear = taxModeller.calculateTaxYearByServiceYears( 
				soyServiceYears, mem.getSalaryBefore(DateUtil.getFirstDayOfThisYear(calendar.getTime())),
				eoyServiceYears, headroom.getFps(), 0, 0.50, 0.02, 16, 50000);
		System.out.println("Tax ammount for this year: " + taxYear.getTaxAmount());
		double tax;
		tax = taxYear.getTaxAmount();
		System.out.println(tax);
		
		//writer.writeExcelFile("D:\\Result_CashLumpSum50Age65.xls", "CashLumpSum50Age65",cashLumpSum, pension, maxCash, tax, 
								//mockExcel.getCashLumpSum50Expected(), mockExcel.getAnnualPensionExpected(), 
								//mockExcel.getPensionWithMaxCashExpected(), mockExcel.getTaxExpected());
	}
	
	/**
	 * Test Two
	 * Cash Lump Sum 100% - RetirementAge - 65
	 * Created on 12/08/2011
	 * @throws Exception 
	 */
	public static void testCashLumpSum100Age65() throws Exception{
		Headroom.isDebug = true;
		MockObject mockObject = new MockObject(65,45,101040);
		
		MemberDetail mem = getObjectData();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mem.getDateAt65th());
		Headroom headroom = new Headroom(mem);
		
		headroom.setAccrual(mockObject.getAccurualRate());
		headroom.setDoR(mockObject.getDoR(mem));
		headroom.setCashPercent(100);
		headroom.setFps(mockObject.getCurrentSalary());
		headroom.calculate();
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		double soyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(calendar.getTime()));
		double eoyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		
		//Cash Lump Sum
		double cashLumpSum;
		cashLumpSum = pensionDetails.getCashLumpSum();
		System.out.println(cashLumpSum);		
		
		//Annual Pension = Reduced Pension
		double pension;
		pension = pensionDetails.getReducedPension();
		System.out.println(pension);
		//Pension With Max Cash = Max Scheme Cash
		double maxCash;
		maxCash = pensionDetails.getMaxSchemeCash();
		System.out.println(maxCash);
		
		//Tax
		TaxModeller taxModeller = new TaxModeller();
		taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		TaxYear taxYear = taxModeller.calculateTaxYearByServiceYears( 
				soyServiceYears, mem.getSalaryBefore(DateUtil.getFirstDayOfThisYear(calendar.getTime())),
				eoyServiceYears, headroom.getFps(), 0, 0.50, 0.02, 16, 50000);
		System.out.println("Tax ammount for this year: " + taxYear.getTaxAmount());
		double tax;
		tax = taxYear.getTaxAmount();
		System.out.println(tax);
		//writer.writeExcelFile("D:\\Result_CashLumpSum100Age65.xls", "CashLumpSum100Age65", cashLumpSum, pension, maxCash, tax, 
				//mockExcel.getCashLumpSum100Expected(), mockExcel.getAnnualPensionExpected(), 
				//mockExcel.getPensionWithMaxCashExpected(), mockExcel.getTaxExpected());
	}
	
	/**
	 * Test Three
	 * Cash Lump Sum 50% - RetirementAge - 60
	 * Created on 12/08/2011
	 * @throws Exception 
	 */ 
	public static void testCashLumpSum50Age60() throws Exception{
		Headroom.isDebug = true;
		MockObject mockObject = new MockObject(60,45,101040);
		
		MemberDetail mem = getObjectData();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mem.getDateAt60th());
		Headroom headroom = new Headroom(mem);
		headroom.setAccrual(mockObject.getAccurualRate());
		headroom.setDoR(mockObject.getDoR(mem));
		headroom.setCashPercent(50);
		headroom.setFps(mockObject.getCurrentSalary());
		headroom.calculate();
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		double soyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(calendar.getTime()));
		double eoyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		
		//Cash Lump Sum
		double cashLumpSum;
		cashLumpSum= pensionDetails.getCashLumpSum();
		System.out.println(cashLumpSum );		
		
		//Annual Pension = Reduced Pension
		double pension;
		pension = pensionDetails.getReducedPension();
		System.out.println(pension);
		//Pension With Max Cash = Max Scheme Cash
		double maxCash;
		maxCash = pensionDetails.getMaxSchemeCash();
		System.out.println(maxCash);
		
		//Tax
		TaxModeller taxModeller = new TaxModeller();
		taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		TaxYear taxYear = taxModeller.calculateTaxYearByServiceYears( 
				soyServiceYears, mem.getSalaryBefore(DateUtil.getFirstDayOfThisYear(calendar.getTime())),
				eoyServiceYears, headroom.getFps(), 0, 0.50, 0.02, 16, 50000);
		System.out.println("Tax ammount for this year: " + taxYear.getTaxAmount());
		double tax;
		tax = taxYear.getTaxAmount();
		System.out.println(tax);
		
		//writer.writeExcelFile("D:\\Result_CashLumpSum50Age60.xls", "CashLumpSum50Age60", cashLumpSum, pension, maxCash, tax, 
				//mockExcel.getCashLumpSum50Expected(), mockExcel.getAnnualPensionExpected(), 
				//mockExcel.getPensionWithMaxCashExpected(), mockExcel.getTaxExpected());
	}
	
	/**
	 * Test Four
	 * Cash Lump Sum 100% - RetirementAge - 60
	 * Created on 12/08/2011
	 * @throws Exception 
	 */
	public static void testCashLumpSum100Age60() throws Exception{
		Headroom.isDebug = true;
		MockObject mockObject = new MockObject(60,45,101040);
		
		MemberDetail mem = getObjectData();

		System.out.println("Name :"+mem.getName());
		System.out.println("DoB :"+ mem.getDateOfBirth());
		System.out.println("55 age :"+ mem.getDateAt55th());
		System.out.println("60 age :"+ mem.getDateAt60th());
		System.out.println("65 age :"+ mem.getDateAt65th());
		System.out.println("AugmentationDays :"+mem.getAugmentationDays());
		System.out.println("CPI :"+mem.getCpi());
		System.out.println("PensionableSalary :"+mem.getPensionableSalary());
		System.out.println("A Days :"+mem.getTVINADays());
		System.out.println("B Days :"+mem.getTVINBDays());
		System.out.println("C Days :"+mem.getTVINCDays());
		System.out.println("LTA :"+mem.getLTA());
		
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mem.getDateAt60th());
		Headroom headroom = new Headroom(mem);
		headroom.setAccrual(mockObject.getAccurualRate());
		headroom.setDoR(mockObject.getDoR(mem));
		headroom.setCashPercent(100);
		headroom.setFps(mockObject.getCurrentSalary());
		headroom.calculate();
		
		PensionDetails pensionDetails = headroom.getPensionDetails();
		double soyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(calendar.getTime()));
		System.out.println("soyServiceYears: " + soyServiceYears);
		double eoyServiceYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		System.out.println("eoyServiceYears: " + eoyServiceYears);
		//Cash Lump Sum
		double cashLumpSum;
		cashLumpSum= pensionDetails.getCashLumpSum();
		System.out.println("CashLumpSum: " + cashLumpSum);		
		
		//Annual Pension = Reduced Pension
		double pension;
		pension = pensionDetails.getReducedPension();
		System.out.println("Pension: " + pension);
		//Pension With Max Cash = Max Scheme Cash
		double maxCash;
		maxCash = pensionDetails.getMaxSchemeCash();
		System.out.println("MaxCash :" + maxCash);
		
		//Tax
		TaxModeller taxModeller = new TaxModeller();
		taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(calendar.getTime()));
		TaxYear taxYear = taxModeller.calculateTaxYearByServiceYears( 
				soyServiceYears, mem.getSalaryBefore(DateUtil.getFirstDayOfThisYear(calendar.getTime())),
				eoyServiceYears, headroom.getFps(), 0, 0.50, 0.02, 16, 50000);
		System.out.println("Tax ammount for this year: " + taxYear.getTaxAmount());
		double tax;
		tax = taxYear.getTaxAmount();
		System.out.println(tax);
		
		//writer.writeExcelFile("D:\\Result_CashLumpSum100Age60.xls", "CashLumpSum100Age60", cashLumpSum, pension, maxCash, tax, 
				//mockExcel.getCashLumpSum100Expected(), mockExcel.getAnnualPensionExpected(), 
				//mockExcel.getPensionWithMaxCashExpected(), mockExcel.getTaxExpected());
	}
}
