/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.modeller.TaxChooser;
import com.bp.pensionline.aataxmodeller.modeller.TaxModellerAuditor;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public class SelectTaxChoice extends HttpServlet {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Storing log for this class. */
    public static final Log LOG = CmsLog.getLog(
            org.opencms.jsp.CmsJspLoginBean.class);

    /** Represent a tag name. */
    public static final String AJAX_TAX_MODELLER_RESPONSE_TAG = "TaxModeller";
    
	public static final String AJAX_CURRENT_CHOICE_TAG = "CurrentChoice";
	public static final String AJAX_CURRENT_CHOICE_DATE_TAG = "CurrentChoiceDate";
	
    private static final long serialVersionUID = 1L;

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * The method that handle with requests.
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception
     * @throws IOException      exception
     */
    protected void processRequest(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constant.CONTENT_TYPE);
        
        PrintWriter out = response.getWriter();

        String xmlResponse = null;
        
        // Get request from client
        String taxChoice = request.getParameter("choice");
        CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String bgroup = null;
		String refno = null;
		MemberDao memberDao = null;
		if (currentUser != null)
		{
			bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
		}
		
        LOG.info("Tax choice for " + bgroup + "-" + refno + ": " + taxChoice);
        try
		{
        	if (memberDao != null && bgroup != null && refno != null && taxChoice != null 
        			&& !bgroup.trim().equals("") && !refno.trim().equals("") && !taxChoice.trim().equals(""))
        	{
        		TaxChooser taxChooser = new TaxChooser();
        		
        		boolean updateResult = taxChooser.updateTaxChoice(bgroup, refno, taxChoice.toLowerCase());
        		if (updateResult)
        		{
        			String currentChoice = taxChoice;
        			String currentChoiceTimestamp = new SimpleDateFormat("dd MMMM yyyy HH:MM").format(new Date());
        			String currentChoiceTime = new SimpleDateFormat("HH:MM").format(new Date());
        			String currentChoiceDate = new SimpleDateFormat("dd MMMM yyyy").format(new Date());
        			
        			memberDao.set("AaCurrentChoice", currentChoice);
        			memberDao.set("AaCurrentChoiceTimestamp", currentChoiceTimestamp);
        			memberDao.set("AaCurrentChoiceTime", currentChoiceTime);
    				memberDao.set("AaCurrentChoiceDate", currentChoiceDate);
    				
    				// Audit the member select tax choice to BP_STATS_POS
    				TaxModellerAuditor auditor = new TaxModellerAuditor();
    				auditor.doTaxChoiceAudit(request, taxChoice.toLowerCase());
    				
    				
    				currentUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
    				
        			xmlResponse = buildXmlReponse(currentChoice, currentChoiceDate);
        		}
        		else
        		{
        			xmlResponse = buildXmlResponseError("Error while updating tax choice for member " + bgroup + "-" + refno);
        		}  
        		
        		LOG.info("Response tax choice for " + bgroup + "-" + refno + ": " + xmlResponse);
        	}

            // Return an TaxYear object in document format.           

		}
		catch (Exception e)
		{
			LOG.error("Error while updating tax choice for membeer " + bgroup + "-" + refno + ": " + e.toString());
			xmlResponse = buildXmlResponseError("Error while updating tax choice for membeer " + bgroup + "-" + refno + ": " + e.toString());
		}

		out.print(xmlResponse);
		out.close();
    }

    /**
     * The method that builds key/value to structured xml document.<br>
     *
     * @param  taxYear newConfigs Hashtable store pair of value
     *
     * @return a structured document.
     */
    public String buildXmlReponse(String currentChoice, String currentChoiceDate) {
    	
        StringBuffer buffer = new StringBuffer();
     // Add open tag modeller.
        buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        buffer.append("<").append(AJAX_CURRENT_CHOICE_TAG).append(">").append(currentChoice).append("</").append(AJAX_CURRENT_CHOICE_TAG).append(">");  
        buffer.append("<").append(AJAX_CURRENT_CHOICE_DATE_TAG).append(">").append(currentChoiceDate).append("</").append(AJAX_CURRENT_CHOICE_DATE_TAG).append(">");  
        // Add close tag for modeller
        buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        
        // Return structured document xml type.
        return buffer.toString();
    }

    /**
     * Make a xml structured content.
     *
     * @param  message error description
     *
     * @return error message
     */
    public String buildXmlResponseError(String message) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        buffer.append("<Error>").append(message).append("</Error>").append("");
        buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");

        return buffer.toString();
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Print servlet information!
     *
     * @return String information!
     */
    public String getServletInfo() {
        return "Given short description";
    }
}
