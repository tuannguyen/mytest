/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.ServiceTranche;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.modeller.TaxModellerAuditor;
import com.bp.pensionline.aataxmodeller.util.NumberUtil;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 * 
 * <p>
 * Optionally, enter a longer description.
 * </p>
 * 
 * @author Admin
 * @version 1.0
 */
public class HeadroomCheckingService extends HttpServlet
{
	
	// ~ Static fields/initializers
	// -----------------------------------------------------------------

	/** Storing log for this class. */
	public static final Log LOG = CmsLog.getLog(HeadroomCheckingService.class);

	/** Represent a tag name. */
	public static final String AJAX_TAX_MODELLER_RESPONSE_TAG = "TaxModeller";
	public static final String AJAX_MEMBER_DETAIL_RESPONSE_TAG = "MemberDetail";
	
	public static final String AJAX_SERVICE_HISTORIES_RESPONSE_TAG = "ServiceHistories";
	public static final String AJAX_SERVICE_HISTORY_RESPONSE_TAG = "ServiceHistory";
	public static final String AJAX_TOTAL_YEARS_RESPONSE_TAG = "TotalYears";
	public static final String AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG = "TotalServiceYears";
	public static final String AJAX_TOTAL_ACCRUED_RESPONSE_TAG = "TotalAccrued";
	public static final String AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG = "TotalServiceYearsAt60th";
	
	public static final String AJAX_TAX_YEAR_RESPONSE_TAG = "TaxYearInfo";
	
	
	
	private static final long serialVersionUID = 1L;

	// ~ Methods
	// ------------------------------------------------------------------------------------

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		
		String xmlResponse = null;

		// Get request from client
//		String bgroup = request.getParameter("bgroup");
//		String refno = request.getParameter("refno");
//		String sdate = request.getParameter("sdate");
		
		Date headroomDate = new Date();
		
//		if (headroomDate == null || DateUtil.isDateBeforeToday(headroomDate) )
//		{
//			// check if sdate is a past date
//			headroomDate = new Date();
//		}
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String bgroup = null;
		String refno = null;
		if (currentUser != null)
		{
			bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
		}
		
		try
		{
			MemberLoader memberLoader = new MemberLoader(bgroup, refno);

			MemberDetail memberDetail = memberLoader.loadMember();
			
			if (memberDetail != null)
			{
				// Audit the member access to BP_STATS_POS
				TaxModellerAuditor auditor = new TaxModellerAuditor();
				auditor.doAccessAudit(request);
				
				Headroom headroom = new Headroom(memberDetail, headroomDate);
				headroom.calculate();
				
				// calculate tax
				TaxModeller taxModeller = new TaxModeller();
				taxModeller.setSystemDate(headroomDate);
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(headroomDate);
				TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(headroomDate));
				
				double soYSalary = memberDetail.getPensionableSalary();
								
				double taxrate = Double.parseDouble(values.getAnnualAllowanceTaxRate()) / 100;
				
				double servieYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(headroomDate));
				servieYears = NumberUtil.parseDouble(NumberUtil.formatToDecimal(servieYears));
				
	            double salaryIncrease = Double.parseDouble(values.getAverageSalaryIncrease()) / 100;
	            double inflation = Double.parseDouble(values.getAnnualInflation()) / 100;
	            
	            // if configurations is false then use member's detail data
	            if (!values.isConfigurationUsed())
	            {
	            	inflation= memberDetail.getCpi();
	            }
	            // System.out.println("date---------->" + calYear);
	            
	            TaxYear nextTaxYear = taxModeller.estimateTaxYear(
	            		servieYears,
            			soYSalary,
            			headroom.getCurrentAccrual(),
            			salaryIncrease, taxrate, inflation,
                        Double.parseDouble(values.getCapitalisation()),
                        Double.parseDouble(values.getAnnualAllowance()));				
				
				xmlResponse = buildXmlResponse(headroom, nextTaxYear);
			}
			else
			{
				LOG.error("Error while loading member for headroom check: " + bgroup + "-" + refno);
				xmlResponse = buildXmlResponseError("Error while checking member: " + bgroup + "-" + refno);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while checking headroom for member: " + e.toString());
			xmlResponse = buildXmlResponseError("Error while checking member");
		}
		
		out.print(xmlResponse);
		out.close();
	}

	/**
	 * Build xml response but not use this method. Use redirect for not AJAX processing
	 * 
	 * @param currentSalary xml String value.
	 * @param taxRateOnExcess
	 * 
	 * @return response of xml content
	 */
	public String buildXmlResponse(Headroom headroom, TaxYear taxYear)
	{
		String xmlResponse = null;		
		
		if (headroom != null)
		{
			Date _1stJanThisYear = DateUtil.getFirstDayOfThisYear(headroom.getHeadroomDate());
			
			StringBuffer xmlResponseBuffer = new StringBuffer();
			xmlResponseBuffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
			
			// service histories
			ArrayList<ServiceTranche> serviceHistories = headroom.getServiceTranchesToDate(_1stJanThisYear);
			xmlResponseBuffer.append("<").append(AJAX_SERVICE_HISTORIES_RESPONSE_TAG).append(">");
			
			// hide future tranche
			if (serviceHistories.size() > 1)
			{
				for (int i = 0; i < serviceHistories.size(); i++)
				{
					ServiceTranche serviceHistory = serviceHistories.get(i);
					xmlResponseBuffer.append("<").append(AJAX_SERVICE_HISTORY_RESPONSE_TAG).append(">");
					
					xmlResponseBuffer.append("<").append("from").append(">");
					xmlResponseBuffer.append(DateUtil.formatDate(serviceHistory.getFrom()));
					xmlResponseBuffer.append("</").append("from").append(">");
					xmlResponseBuffer.append("<").append("to").append(">");
					xmlResponseBuffer.append(DateUtil.formatDate(serviceHistory.getTo()));
					xmlResponseBuffer.append("</").append("to").append(">");
					xmlResponseBuffer.append("<").append("category").append(">");
					xmlResponseBuffer.append(serviceHistory.getCategory());
					xmlResponseBuffer.append("</").append("category").append(">");
					xmlResponseBuffer.append("<").append("years").append(">");
					xmlResponseBuffer.append(serviceHistory.getYears());
					xmlResponseBuffer.append("</").append("years").append(">");
					xmlResponseBuffer.append("<").append("days").append(">");
					xmlResponseBuffer.append(serviceHistory.getDays());
					xmlResponseBuffer.append("</").append("days").append(">");
					xmlResponseBuffer.append("<").append("tyears").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getTotalYears()));
					xmlResponseBuffer.append("</").append("tyears").append(">");
					xmlResponseBuffer.append("<").append("fte").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getFTE()));
					xmlResponseBuffer.append("</").append("fte").append(">");
					xmlResponseBuffer.append("<").append("service").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getServiceYears()));
					xmlResponseBuffer.append("</").append("service").append(">");
					xmlResponseBuffer.append("<").append("accrual").append(">");
					xmlResponseBuffer.append(serviceHistory.getAccrual());
					xmlResponseBuffer.append("</").append("accrual").append(">");
					xmlResponseBuffer.append("<").append("accrued").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(
							serviceHistory.getAccrued() * headroom.getMemberDetail().getPensionableSalary()));
					xmlResponseBuffer.append("</").append("accrued").append(">");
					xmlResponseBuffer.append("<").append("service60th").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getServiceYearsAt60th()));
					xmlResponseBuffer.append("</").append("service60th").append(">");
					
					xmlResponseBuffer.append("</").append(AJAX_SERVICE_HISTORY_RESPONSE_TAG).append(">");
				}
			}
			xmlResponseBuffer.append("</").append(AJAX_SERVICE_HISTORIES_RESPONSE_TAG).append(">");		
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_YEARS_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalYearsToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_YEARS_RESPONSE_TAG).append(">");	
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalServiceYearsToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG).append(">");	
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_ACCRUED_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalAccruedMoneyToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_ACCRUED_RESPONSE_TAG).append(">");
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalServiceYearsAt60thToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG).append(">");
			
			// append tax year data
			xmlResponseBuffer.append(buildTaxYearXmlReponse(taxYear));
			
			xmlResponseBuffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
			
			xmlResponse = xmlResponseBuffer.toString();
		}
		
		LOG.info("Response XML: " + xmlResponse);
		return xmlResponse;
	}
	
    /* The method that builds key/value to structured xml document.<br>
    *
    * @param  taxYear newConfigs Hashtable store pair of value
    *
    * @return a structured document.
    */
   private String buildTaxYearXmlReponse(TaxYear taxYear) {
   	
       StringBuffer buffer = new StringBuffer();
       if (taxYear != null)
       {
	        // Add open tag modeller.
	        buffer.append("<").append(AJAX_TAX_YEAR_RESPONSE_TAG).append(">");
	
	        // Add pair of value inside
	        buffer.append("<Year>").append(taxYear.getYear()).append("</Year>")
	        	.append("<TaxYear>").append(taxYear.getTaxYear()).append("</TaxYear>")
	        	.append("<SOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getSoySalary())).append("</SOYSalary>")
	        	.append("<Accrued>").append(NumberUtil.formatToPercent(taxYear.getSoyAccrued())).append("</Accrued>")
	        	.append("<SOYBenefit>").append(NumberUtil.formatToDecimal(taxYear.getSoyBenefit())).append("</SOYBenefit>")
	        	.append("<CPI>").append(NumberUtil.formatToPercent(taxYear.getCpi())).append("</CPI>")
	        	.append("<CpiReval>").append(NumberUtil.formatToDecimal(taxYear.getCpiReval())).append("</CpiReval>")
	            .append("<SalaryIncrease>").append(NumberUtil.formatToPercent(taxYear.getSalaryIncrease())).append("</SalaryIncrease>")
	            .append("<EOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getEoySalary())).append("</EOYSalary>")
	            .append("<AccruedRate>").append(taxYear.getAccrualRate()).append("</AccruedRate>")
	            .append("<EOYBenefit>").append(taxYear.getEoyBenefit()).append("</EOYBenefit>")
	            .append("<Increase>").append(NumberUtil.formatToDecimal(taxYear.getIncrease())).append("</Increase>")
	            .append("<AAFac>").append(taxYear.getaAFactor()).append("</AAFac>")
	            .append("<AACheck>").append(NumberUtil.formatToDecimal(taxYear.getAACheck())).append("</AACheck>")
	            .append("<AnnualAllowance>").append(taxYear.getAnnualAllowance()).append("</AnnualAllowance>")
	            .append("<AAExcess>").append(NumberUtil.formatToDecimal(taxYear.getaAExcess())).append("</AAExcess>")
	            .append("<TaxRate>").append(NumberUtil.formatToPercent(taxYear.getTaxRate())).append("</TaxRate>")
	            .append("<Tax>").append(NumberUtil.formatToNearestPound(taxYear.getTaxAmount())).append("</Tax>");
	
	        // Add close tag for modeller
	        buffer.append("</").append(AJAX_TAX_YEAR_RESPONSE_TAG).append(">");
       }
       
       // Return structured document xml type.
       return buffer.toString();
   }		

	/**
	 * Make a xml structured content.
	 * 
	 * @param message
	 *            error description
	 * 
	 * @return error message
	 */
	public String buildXmlResponseError(String message)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>")
				.append("\n");
		buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");

		return buffer.toString();
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Print servlet information!
	 * 
	 * @return String information!
	 */
	public String getServletInfo()
	{
		return "Given short description";
	}
}
