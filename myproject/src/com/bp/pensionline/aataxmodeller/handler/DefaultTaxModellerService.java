/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.aataxmodeller.dto.DefaultInfo;
import com.bp.pensionline.aataxmodeller.util.ConfigurationUtil;
import com.bp.pensionline.aataxmodeller.util.DefaultConfiguration;
import com.bp.pensionline.aataxmodeller.util.OpenCmsFileUtil;
import com.bp.pensionline.aataxmodeller.util.TaxUtil;
import com.bp.pensionline.test.Constant;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public class DefaultTaxModellerService extends HttpServlet {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Storing log for this class. */
    public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

    /** Represent a tag name. */
    public static final String AJAX_TAX_MODELLER_RESPONSE = "TaxModeller";

    /** Represent node name. */
    public static final String DEFAULT_RESPONSE = "DefaultValues";
    private static final long serialVersionUID = 1L;

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * The method that handle with requests.
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception
     * @throws IOException      exception
     */
    protected void processRequest(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constant.CONTENT_TYPE);
        ConfigurationUtil util = new ConfigurationUtil();
        
        String AverageSalaryIncrease = (String)util.isNull(request.getParameter("AverageSalaryIncrease"), "");
        String AnnualAllowanceTaxRate = (String)util.isNull(request.getParameter("AnnualAllowanceTaxRate"), "");
        String AnnualInflation = (String)util.isNull(request.getParameter("AnnualInflation"), "");
        String AnnualAllowance = (String)util.isNull(request.getParameter("AnnualAllowance"), "");
        
        byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + TaxUtil.CONFIG_FILE_NAME);

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
		updateXMLFile(defaultValueContentBytes, AnnualInflation, AnnualAllowanceTaxRate, AnnualAllowance, AverageSalaryIncrease, response);
    }
    /**
     * The method update default.xml except LTA and Capitalasition<br>
     *
     * @param  doc array of bytes.
     * @param  AnnualInflation: update for AnnualInflation.
     * @param  AnnualAllowanceTaxRate: update for AnnualAllowanceTaxRate.
     * @param  AnnualAllowance: update for AnnualAllowance.
     * @return AverageSalaryIncrease: update for AverageSalaryIncrease.
     * @return response: this will write the result of update xml
     */
    public void updateXMLFile(byte[] doc, String AnnualInflation, String AnnualAllowanceTaxRate, String AnnualAllowance, String AverageSalaryIncrease, HttpServletResponse response) {
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTags = root.getChildNodes();

            // NodeList nodeList = root.getElementsByTagName(tag);
            Node subNode = null;
            NodeList child = null;
            String tagObjectName = null;
            NodeList nodeList = null;

            // Loop over sub tags inside defaultvalue tag
            for (int tagIndex = 0; tagIndex < listObjectTags.getLength(); tagIndex++) {
                if (listObjectTags.item(tagIndex).getNodeType() == Node.ELEMENT_NODE) {
                    tagObjectName = listObjectTags.item(tagIndex).getNodeName();
                }
                if (tagObjectName != null) {
                    // Get tag by name
                    nodeList = root.getElementsByTagName(tagObjectName);
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        // Get item at current element
                        subNode = nodeList.item(i);

                        // Check update for all subNode because some value of LTA and Capitalisation already existed so we do not create new xml file here
                       if(subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE) 
                                && subNode.getNodeName().equalsIgnoreCase(DefaultConfiguration.ANNUAL_INFLATION)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                    
                                	if (child.item(j).getNodeName().equals(DefaultConfiguration.VALUE) && child.item(j).hasChildNodes()) {
                                        // Set value
                                		child.item(j).getFirstChild().setNodeValue(AnnualInflation);
                                    }
                                }
                            }
                        } else if(subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE) 
                                && subNode.getNodeName().equalsIgnoreCase(DefaultConfiguration.ANNUAL_ALLOWANCE_TAX_RATE)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                    
                                	if (child.item(j).getNodeName().equals(DefaultConfiguration.VALUE) && child.item(j).hasChildNodes()) {
                                        // Set value
                                		child.item(j).getFirstChild().setNodeValue(AnnualAllowanceTaxRate);
                                    }
                                }
                            }
                        } else if(subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE) 
                                && subNode.getNodeName().equalsIgnoreCase(DefaultConfiguration.ANNUAL_ALLOWANCE)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                	if (child.item(j).getNodeName().equals(DefaultConfiguration.VALUE) && child.item(j).hasChildNodes()) {
                                        // Set value
                                		child.item(j).getFirstChild().setNodeValue(AnnualAllowance);
                                    }
                                }
                            }
                        } else if(subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE) 
                                && subNode.getNodeName().equalsIgnoreCase(DefaultConfiguration.AVERAGE_SALARY_INCRESE)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                	if (child.item(j).getNodeName().equals(DefaultConfiguration.VALUE) && child.item(j).hasChildNodes()) {
                                        // Set value
                                		child.item(j).getFirstChild().setNodeValue(AverageSalaryIncrease);
                                    }
                                }
                            }
                        }
                    }
                }
            }
          //Output the XML

            //set up a transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(document);
            trans.transform(source, result);
            String xmlString = sw.toString();
            
            String xmlResponse;
    		boolean isConfigured = buildXmlResponse(xmlString, TaxUtil.CONFIG_FILE_NAME);
    		if (isConfigured) {
    			xmlResponse = TaxUtil.buildSuccessXmlMessage();
    		} else {
    			xmlResponse = TaxUtil
    					.buildXmlResponseError("Unknown error while tax modeller process!");
    		}
    		PrintWriter out = response.getWriter();
    		out.print(xmlResponse);
    		out.close();
    		LOG.info("Response XML: " + xmlResponse);
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }

    }
    /**
     * Build xml response.
     *
     * @param  xml String value.
     * @return fileName: name of the file
     * 
     * @return response of xml content
     */
    public boolean buildXmlResponse(String xml, String fileName) {
        boolean isConfigured = false;
        DefaultConfiguration defaultTax = new DefaultConfiguration();
        try {
            isConfigured = defaultTax.writeToConfiguredXmlFile(fileName, xml);
            return isConfigured;
        } catch (Exception e) {
            // Store message error log.
            LOG.info(e.getMessage());
        }

        return isConfigured;
    }
    /**
     * Build xml response.
     *
     * @param  xml String value.
     *
     * @return response of xml content
     */
    public boolean buildXmlResponse(String xml) {
        boolean isConfigured = false;
        String filteredJSXml = TaxUtil.filter(xml);
        DefaultConfiguration defaultTax = new DefaultConfiguration();
        String structuredDocument;
        try {
            Hashtable<String, String> jsContent = defaultTax.convertJSXMLContent(filteredJSXml.getBytes());

            // Load configuration content to collection of objects
            Hashtable<String, DefaultInfo> configuredContent = defaultTax.getDefaultValues();

            // Get a collection of replaced objects
            Hashtable<String, DefaultInfo> updatedContent = defaultTax.replaceFromRequest(jsContent, configuredContent);
            structuredDocument = defaultTax.formatToXmlContent(updatedContent);

            // Update content to configuration file.
            isConfigured = defaultTax.writeToConfiguredXmlFile(TaxUtil.CONFIG_FILE_NAME, structuredDocument);

            return isConfigured;
        } catch (Exception e) {
            // Store message error log.
            LOG.info(e.getMessage());
        }

        return isConfigured;
    }

    /**
     * Make a xml structured content.
     *
     * @param  message error description
     *
     * @return error message
     */
    public String buildXmlResponseError(String message) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<").append(DEFAULT_RESPONSE).append(">\n");
        buffer.append("     <Error>").append(message).append("</Error>").append("\n");
        buffer.append("</").append(DEFAULT_RESPONSE).append(">\n");

        return buffer.toString();
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Print servlet information!
     *
     * @return String information!
     */
    public String getServletInfo() {
        return "Given short description";
    }
}
