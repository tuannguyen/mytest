/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.modeller.TaxModellerAuditor;
import com.bp.pensionline.aataxmodeller.util.NumberUtil;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public class TaxModellerService extends HttpServlet {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Storing log for this class. */
    public static final Log LOG = CmsLog.getLog(
            org.opencms.jsp.CmsJspLoginBean.class);

    /** Represent a tag name. */
    public static final String AJAX_TAX_MODELLER_RESPONSE_TAG = "TaxModeller";
    private static final long serialVersionUID = 1L;

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * The method that handle with requests.
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception
     * @throws IOException      exception
     */
    protected void processRequest(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constant.CONTENT_TYPE);
        
        PrintWriter out = response.getWriter();

        String xmlResponse = null;
        
        // Get request from client
        String modelledSalary = request.getParameter("salary");
        String modelledTaxrate = request.getParameter("taxrate");
        String inputServiceYears = request.getParameter("services");
        String inputAccrual = request.getParameter("accrual");
        String sdate = request.getParameter("sdate");
        String referer = request.getParameter("referer");
		
		Date calculationDate = DateUtil.parseDate(sdate);
		
		if (calculationDate == null)
		{
			calculationDate = new Date();
		}
		
        LOG.info("Modelled parameters: " + modelledSalary + ", " + modelledTaxrate + ", " + inputServiceYears + ", " + inputAccrual + ", " + sdate);
        try
		{
        	
    		CmsUser currentUser = SystemAccount.getCurrentUser(request);
//    		String bgroup = null;
//    		String refno = null;
    		MemberDetail memberDetail = null;
    		if (currentUser != null)
    		{
    			memberDetail = (MemberDetail)currentUser.getAdditionalInfo().get(Environment.MEMBER_DETAIL_KEY);
    		}
    		
        	// referer != null means modeling from test page 
        	if (referer == null)
        	{
	        	TaxModellerAuditor auditor = new TaxModellerAuditor();
				auditor.doModelAudit(request, modelledSalary, modelledTaxrate);
        	}
        	
        	TaxModeller taxModeller = new TaxModeller();
			taxModeller.setSystemDate(calculationDate);
			
			Calendar calendar = Calendar.getInstance();
			TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(calendar.getTime()));
			
			double soYSalary = NumberUtil.parseDouble(modelledSalary);							
			double taxrate = 0.00;
			if (modelledTaxrate != null)
			{
				taxrate = NumberUtil.parseDouble(modelledTaxrate) / 100;
			}
			else
			{
				taxrate = Double.parseDouble(values.getAnnualAllowanceTaxRate()) / 100;
			}
			
			double serviceYears = NumberUtil.parseDouble(inputServiceYears);
			int accrual = NumberUtil.parseInt(inputAccrual);
			
            double salaryIncrease = NumberUtil.parseDouble(values.getAverageSalaryIncrease()) / 100;
            double inflation = NumberUtil.parseDouble(values.getAnnualInflation()) / 100;
            
            // if configurations is false then use member's detail data
            if (!values.isConfigurationUsed() && memberDetail != null)
            {
            	inflation= memberDetail.getCpi();
            }
            
            TaxYear nextTaxYear = taxModeller.estimateTaxYear(serviceYears, soYSalary, accrual, salaryIncrease,
            		taxrate, inflation, NumberUtil.parseDouble(values.getCapitalisation()), NumberUtil.parseDouble(values.getAnnualAllowance()));

            // Return an TaxYear object in document format.
            xmlResponse = buildXmlReponse(nextTaxYear);

		}
		catch (Exception e)
		{
			LOG.error("Error while calculating tax for membeer: " + e.toString());
			xmlResponse = buildXmlResponseError("Error while calculating tax for membeer:  " + e.toString());
		}

		out.print(xmlResponse);
		out.close();
    }

    /**
     * The method that builds key/value to structured xml document.<br>
     *
     * @param  taxYear newConfigs Hashtable store pair of value
     *
     * @return a structured document.
     */
    public String buildXmlReponse(TaxYear taxYear) {
    	
        StringBuffer buffer = new StringBuffer();
        if (taxYear != null)
        {
	        // Add open tag modeller.
	        buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
	
	        // Add pair of value inside
	        buffer.append("<Year>").append(taxYear.getYear()).append("</Year>")
	        	.append("<TaxYear>").append(taxYear.getTaxYear()).append("</TaxYear>")
	        	.append("<SOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getSoySalary())).append("</SOYSalary>")
	        	.append("<Accrued>").append(NumberUtil.formatToPercent(taxYear.getSoyAccrued())).append("</Accrued>")
	        	.append("<SOYBenefit>").append(NumberUtil.formatToDecimal(taxYear.getSoyBenefit())).append("</SOYBenefit>")
	        	.append("<CPI>").append(NumberUtil.formatToPercent(taxYear.getCpi())).append("</CPI>")
	        	.append("<CpiReval>").append(NumberUtil.formatToDecimal(taxYear.getCpiReval())).append("</CpiReval>")
	            .append("<SalaryIncrease>").append(NumberUtil.formatToPercent(taxYear.getSalaryIncrease())).append("</SalaryIncrease>")
	            .append("<EOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getEoySalary())).append("</EOYSalary>")
	            .append("<AccruedRate>").append(taxYear.getAccrualRate()).append("</AccruedRate>")
	            .append("<EOYBenefit>").append(taxYear.getEoyBenefit()).append("</EOYBenefit>")
	            .append("<Increase>").append(NumberUtil.formatToDecimal(taxYear.getIncrease())).append("</Increase>")
	            .append("<AAFac>").append(taxYear.getaAFactor()).append("</AAFac>")
	            .append("<AACheck>").append(NumberUtil.formatToDecimal(taxYear.getAACheck())).append("</AACheck>")
	            .append("<AnnualAllowance>").append(taxYear.getAnnualAllowance()).append("</AnnualAllowance>")
	            .append("<AAExcess>").append(NumberUtil.formatToDecimal(taxYear.getaAExcess())).append("</AAExcess>")
	            .append("<TaxRate>").append(NumberUtil.formatToPercent(taxYear.getTaxRate())).append("</TaxRate>")
	            .append("<Tax>").append(NumberUtil.formatToNearestPound(taxYear.getTaxAmount())).append("</Tax>");
	
	        // Add close tag for modeller
	        buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        }
        
        LOG.info("XML response: " + buffer.toString());	
        // Return structured document xml type.
        return buffer.toString();
    }

    /**
     * Make a xml structured content.
     *
     * @param  message error description
     *
     * @return error message
     */
    public String buildXmlResponseError(String message) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        buffer.append("<Error>").append(message).append("</Error>").append("");
        buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");

        return buffer.toString();
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * The method which response to request from client side.<br>
     *
     * @param  request  HttpServletRequest
     * @param  response HttpServletResponse
     *
     * @throws ServletException exception occur
     * @throws IOException      exception.
     */
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Print servlet information!
     *
     * @return String information!
     */
    public String getServletInfo() {
        return "Given short description";
    }
}
