/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.dto.AbsenceHistory;
import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.SalaryHistory;
import com.bp.pensionline.aataxmodeller.dto.SchemeHistory;
import com.bp.pensionline.aataxmodeller.dto.ServiceTranche;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.util.NumberUtil;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Please enter a short description for this class.
 * 
 * <p>
 * Optionally, enter a longer description.
 * </p>
 * 
 * @author Admin
 * @version 1.0
 */
public class MemberLoadingService extends HttpServlet
{
	
	// ~ Static fields/initializers
	// -----------------------------------------------------------------

	/** Storing log for this class. */
	public static final Log LOG = CmsLog.getLog(MemberLoadingService.class);

	/** Represent a tag name. */
	public static final String AJAX_TAX_MODELLER_RESPONSE_TAG = "TaxModeller";
	public static final String AJAX_MEMBER_DETAIL_RESPONSE_TAG = "MemberDetail";
	
	public static final String AJAX_SCHEME_HISTORIES_RESPONSE_TAG = "SchemeHistories";
	public static final String AJAX_SCHEME_HISTORY_RESPONSE_TAG = "SchemeHistory";
		
	public static final String AJAX_ABSENCE_HISTORIES_RESPONSE_TAG = "AbsenceHistories";
	public static final String AJAX_ABSENCE_HISTORY_RESPONSE_TAG = "AbsenceHistory";
	
	// added by HUY 08-nov-2011 to include salary histories
	public static final String AJAX_SALARY_HISTORIES_RESPONSE_TAG = "SalaryHistories";
	public static final String AJAX_SALARY_HISTORY_RESPONSE_TAG = "SalaryHistory";	
	
	public static final String AJAX_HEADROOM_RESPONSE_TAG = "Headroom";
	public static final String AJAX_SERVICE_HISTORIES_RESPONSE_TAG = "ServiceHistories";
	public static final String AJAX_SERVICE_HISTORY_RESPONSE_TAG = "ServiceHistory";
	public static final String AJAX_TOTAL_YEARS_RESPONSE_TAG = "TotalYears";
	public static final String AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG = "TotalServiceYears";
	public static final String AJAX_TOTAL_ACCRUED_RESPONSE_TAG = "TotalAccrued";
	public static final String AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG = "TotalServiceYearsAt60th";
	public static final String AJAX__TO_1ST_SERVICE_YEARS_60TH_RESPONSE_TAG = "To1stServiceYearsAt60th";	
	
	public static final String AJAX_TAX_YEAR_RESPONSE_TAG = "TaxYearInfo";
	
	public static final String SI_LEVEL_ERROR_MSG = "The sensitivity indicator for this member is higher than the level you have been granted.\n" +
			"Please see a member of the PensionLine support team or a team leader for more details";

	/** DOCUMENT ME! */
	public static final String[] AJAX_MEMBER_DETAIL_RESPONSE_TAGS = new String[]
	{ "reference", "nino", "name", "gender", "date_of_birth", "joined_company",
			"joined_scheme", "birthday_55th", "birthday_60th", "birthday_65th",
			"salary", "avc_fund", "aver_fund", "tvin_a", "tvin_b", "tvin_c",
			"augmentation", "lta", "banked_egp" };
	
	
	
	private static final long serialVersionUID = 1L;

	// ~ Methods
	// ------------------------------------------------------------------------------------

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		
		String xmlResponse = null;

		// Get request from client
		String bgroup = request.getParameter("bgroup");
		String refno = request.getParameter("refno");	
		String sdate = request.getParameter("sdate");
		
		Date headroomDate = DateUtil.parseDate(sdate);
		
		if (headroomDate == null)
		{
			// check if sdate is a past date
			headroomDate = new Date();
		}
		
		try
		{
			MemberLoader memberLoader = new MemberLoader(bgroup, refno);
			
			if (!memberLoader.checkAquilaConnection())
			{
				xmlResponse = buildXmlResponseError("Error while connecting to database server");
			}
			else
			{				
				// check current user SI and member's SI
				int currentUserSI = getCurrentUserSecurityIndicator(request);
				int memberSI = memberLoader.getMemberSecurityLevel();
				
				if (memberSI > currentUserSI)
				{
					xmlResponse = buildXmlResponseError(SI_LEVEL_ERROR_MSG);
				}
				else
				{
					
					MemberDetail memberDetail = memberLoader.loadMember();
					CmsUser currentUser = SystemAccount.getCurrentUser(request);

					
					if (memberDetail != null)
					{
						// store member details to use thoughout user's session
						Headroom headroom = new Headroom(memberDetail, headroomDate);
						headroom.calculate();
						if (currentUser != null)
						{
							currentUser.setAdditionalInfo(Environment.MEMBER_DETAIL_KEY, memberDetail);
							currentUser.setAdditionalInfo("HEADROOM_DATE", headroomDate);
						}						
						
						TaxModeller taxModeller = new TaxModeller();
						taxModeller.setSystemDate(headroomDate);
						
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(headroomDate);
						TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(headroomDate));			            						
						
						double soYSalary = memberDetail.getPensionableSalary();
										
						double taxrate = Double.parseDouble(values.getAnnualAllowanceTaxRate()) / 100;
						
						double servieYears = headroom.getTotalServiceYearsAt60thToDate(DateUtil.getFirstDayOfThisYear(headroomDate));
						
						servieYears = NumberUtil.parseDouble(NumberUtil.formatToDecimal(servieYears));
						
			            double salaryIncrease = Double.parseDouble(values.getAverageSalaryIncrease()) / 100;
			            double inflation = Double.parseDouble(values.getAnnualInflation()) / 100;
			            double lta = Double.parseDouble(values.getLta());
			            
			            // if configurations is false then use member's detail data
			            if (!values.isConfigurationUsed())
			            {
			            	inflation= memberDetail.getCpi();
			            	lta = memberDetail.getLTA();
			            }
			            headroom.setLta(lta);
			            
			            // System.out.println("date---------->" + calYear);
			            
			            TaxYear nextTaxYear = taxModeller.estimateTaxYear(
			            		servieYears,
		            			soYSalary,
		            			headroom.getCurrentAccrual(),
		            			salaryIncrease, taxrate, inflation,
		                        Double.parseDouble(values.getCapitalisation()),
		                        Double.parseDouble(values.getAnnualAllowance()));
			            
						xmlResponse = buildXmlResponse(memberDetail, headroom, nextTaxYear);
					}
					else
					{
						LOG.error("Error while loading member: " + bgroup + "-" + refno);
						xmlResponse = buildXmlResponseError("Member " + bgroup + "-" + refno + " does not exist");
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while loading member: " + e.toString());
			xmlResponse = buildXmlResponseError("Error while loading member: " + e.toString());
		}
		
		out.print(xmlResponse);
		out.close();
	}
	

	/**
	 * Build xml response but not use this method. Use redirect for not AJAX processing
	 * 
	 * @param currentSalary xml String value.
	 * @param taxRateOnExcess
	 * 
	 * @return response of xml content
	 */
	public String buildXmlResponse(MemberDetail memberDetail, Headroom headroom, TaxYear taxYear)
	{
		String xmlResponse = null;		
		
		if (memberDetail != null)
		{
			StringBuffer xmlResponseBuffer = new StringBuffer();
			xmlResponseBuffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
			//member details
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAG).append(">");
						
			//reference
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[0]).append(">");
			xmlResponseBuffer.append(memberDetail.getBGroup() + "-" + memberDetail.getRefno());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[0]).append(">");
			// nino
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[1]).append(">");
			xmlResponseBuffer.append(memberDetail.getNino());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[1]).append(">");
			// Name
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[2]).append(">");
			xmlResponseBuffer.append(memberDetail.getName());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[2]).append(">");
			// gender
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[3]).append(">");
			xmlResponseBuffer.append(memberDetail.getGender());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[3]).append(">");	
			// date of birth
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[4]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getDateOfBirth()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[4]).append(">");
			// joined company
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[5]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getJoinedCompany()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[5]).append(">");	
			// joined scheme
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[6]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getJoinedScheme()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[6]).append(">");
			// 55th birthday
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[7]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getDateAt55th()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[7]).append(">");		
			// 60th birthday
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[8]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getDateAt60th()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[8]).append(">");	
			// 65th birthday
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[9]).append(">");
			xmlResponseBuffer.append(DateUtil.formatDate(memberDetail.getDateAt65th()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[9]).append(">");
			// salary
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[10]).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(memberDetail.getPensionableSalary()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[10]).append(">");
			// avc fund
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[11]).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(memberDetail.getAVCFund()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[11]).append(">");			
			// aver fund
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[12]).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(memberDetail.getAVERFund()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[12]).append(">");
			// tvin a
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[13]).append(">");
			xmlResponseBuffer.append(memberDetail.getTVINADays());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[13]).append(">");
			// tvin b
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[14]).append(">");
			xmlResponseBuffer.append(memberDetail.getTVINBDays());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[14]).append(">");
			// tvin c
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[15]).append(">");
			xmlResponseBuffer.append(memberDetail.getTVINCDays());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[15]).append(">");
			// augmentation
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[16]).append(">");
			xmlResponseBuffer.append(memberDetail.getAugmentationDays());
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[16]).append(">");
			// lta
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[17]).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(memberDetail.getLTA()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[17]).append(">");		
			// banked
			xmlResponseBuffer.append("<").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[18]).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(memberDetail.getBankedEGP()));
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAGS[18]).append(">");			
			
			xmlResponseBuffer.append("</").append(AJAX_MEMBER_DETAIL_RESPONSE_TAG).append(">");
			
			// scheme histories
			ArrayList<SchemeHistory> schemeHistories = memberDetail.getSchemeHistory();
			xmlResponseBuffer.append("<").append(AJAX_SCHEME_HISTORIES_RESPONSE_TAG).append(">");
			for (int i = 0; i < schemeHistories.size(); i++)
			{
				SchemeHistory schemeHistory = schemeHistories.get(i);
				xmlResponseBuffer.append("<").append(AJAX_SCHEME_HISTORY_RESPONSE_TAG).append(">");
				
				xmlResponseBuffer.append("<").append("from").append(">");
				xmlResponseBuffer.append(DateUtil.formatDate(schemeHistory.getFrom()));
				xmlResponseBuffer.append("</").append("from").append(">");
				xmlResponseBuffer.append("<").append("to").append(">");
				xmlResponseBuffer.append(DateUtil.formatDate(schemeHistory.getTo()));
				xmlResponseBuffer.append("</").append("to").append(">");
				xmlResponseBuffer.append("<").append("category").append(">");
				xmlResponseBuffer.append(schemeHistory.getCategory());
				xmlResponseBuffer.append("</").append("category").append(">");
				xmlResponseBuffer.append("<").append("accrual").append(">");
				xmlResponseBuffer.append(schemeHistory.getAccrual());
				xmlResponseBuffer.append("</").append("accrual").append(">");
				
				xmlResponseBuffer.append("</").append(AJAX_SCHEME_HISTORY_RESPONSE_TAG).append(">");
			}
			xmlResponseBuffer.append("</").append(AJAX_SCHEME_HISTORIES_RESPONSE_TAG).append(">");
			
			// absence histories
			ArrayList<AbsenceHistory> absenceHistories = memberDetail.getAbsenceHistory();
			xmlResponseBuffer.append("<").append(AJAX_ABSENCE_HISTORIES_RESPONSE_TAG).append(">");
			for (int i = 0; i < absenceHistories.size(); i++)
			{
				AbsenceHistory absenceHistory = absenceHistories.get(i);
				xmlResponseBuffer.append("<").append(AJAX_ABSENCE_HISTORY_RESPONSE_TAG).append(">");
				
				xmlResponseBuffer.append("<").append("from").append(">");
				xmlResponseBuffer.append(DateUtil.formatDate(absenceHistory.getFrom()));
				xmlResponseBuffer.append("</").append("from").append(">");
				xmlResponseBuffer.append("<").append("to").append(">");
				xmlResponseBuffer.append(DateUtil.formatDate(absenceHistory.getTo()));
				xmlResponseBuffer.append("</").append("to").append(">");
				xmlResponseBuffer.append("<").append("type").append(">");
				xmlResponseBuffer.append(absenceHistory.getType());
				xmlResponseBuffer.append("</").append("type").append(">");
				xmlResponseBuffer.append("<").append("worked").append(">");
				xmlResponseBuffer.append(absenceHistory.getWorked());
				xmlResponseBuffer.append("</").append("worked").append(">");
				xmlResponseBuffer.append("<").append("employed").append(">");
				xmlResponseBuffer.append(absenceHistory.getEmployed());
				xmlResponseBuffer.append("</").append("employed").append(">");
				
				xmlResponseBuffer.append("</").append(AJAX_ABSENCE_HISTORY_RESPONSE_TAG).append(">");
			}
			xmlResponseBuffer.append("</").append(AJAX_ABSENCE_HISTORIES_RESPONSE_TAG).append(">");
			
			// salary histories
			ArrayList<SalaryHistory> salaryHistories = memberDetail.getSalaryHistory();
			xmlResponseBuffer.append("<").append(AJAX_SALARY_HISTORIES_RESPONSE_TAG).append(">");
			
			for (int i = 0; i < (salaryHistories.size() > 3 ? 3 : salaryHistories.size()); i++)
			{
				SalaryHistory salaryHistory = salaryHistories.get(i);
				xmlResponseBuffer.append("<").append(AJAX_SALARY_HISTORY_RESPONSE_TAG).append(">");
				
				xmlResponseBuffer.append("<").append("Effective").append(">");
				xmlResponseBuffer.append(DateUtil.formatDate(salaryHistory.getEffective()));
				xmlResponseBuffer.append("</").append("Effective").append(">");
				xmlResponseBuffer.append("<").append("Salary").append(">");
				xmlResponseBuffer.append(NumberUtil.formatToNearestPound(salaryHistory.getSalary()));
				xmlResponseBuffer.append("</").append("Salary").append(">");				
				
				xmlResponseBuffer.append("</").append(AJAX_SALARY_HISTORY_RESPONSE_TAG).append(">");
			}
			xmlResponseBuffer.append("</").append(AJAX_SALARY_HISTORIES_RESPONSE_TAG).append(">");			
			
			
			// append headroom data
			xmlResponseBuffer.append(buildHeadroomXmlResponse(headroom));
			
			// append tax year data
			xmlResponseBuffer.append(buildTaxYearXmlReponse(taxYear));
			
			xmlResponseBuffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
			
			xmlResponse = xmlResponseBuffer.toString();
		}
		
		LOG.info("Response XML: " + xmlResponse);
		return xmlResponse;
	}
	
	private String buildHeadroomXmlResponse(Headroom headroom)
	{
		String xmlResponse = "";		
		
		if (headroom != null)
		{			
			StringBuffer xmlResponseBuffer = new StringBuffer();
			xmlResponseBuffer.append("<").append(AJAX_HEADROOM_RESPONSE_TAG).append(">");
			
			Date _1stJanThisYear = DateUtil.getFirstDayOfThisYear(headroom.getHeadroomDate());
			
			// service histories
			ArrayList<ServiceTranche> serviceHistories = headroom.getServiceTranchesToDate(_1stJanThisYear);
			headroom.debugTranches(serviceHistories);
			
			xmlResponseBuffer.append("<").append(AJAX_SERVICE_HISTORIES_RESPONSE_TAG).append(">");
			
			// hide future tranche
			if (serviceHistories.size() > 1)
			{
				for (int i = 0; i < serviceHistories.size(); i++)
				{
					ServiceTranche serviceHistory = serviceHistories.get(i);
					xmlResponseBuffer.append("<").append(AJAX_SERVICE_HISTORY_RESPONSE_TAG).append(">");
					
					xmlResponseBuffer.append("<").append("from").append(">");
					xmlResponseBuffer.append(DateUtil.formatDate(serviceHistory.getFrom()));
					xmlResponseBuffer.append("</").append("from").append(">");
					xmlResponseBuffer.append("<").append("to").append(">");
					xmlResponseBuffer.append(DateUtil.formatDate(serviceHistory.getTo()));
					xmlResponseBuffer.append("</").append("to").append(">");
					xmlResponseBuffer.append("<").append("category").append(">");
					xmlResponseBuffer.append(serviceHistory.getCategory());
					xmlResponseBuffer.append("</").append("category").append(">");
					xmlResponseBuffer.append("<").append("years").append(">");
					xmlResponseBuffer.append(serviceHistory.getYears());
					xmlResponseBuffer.append("</").append("years").append(">");
					xmlResponseBuffer.append("<").append("days").append(">");
					xmlResponseBuffer.append(serviceHistory.getDays());
					xmlResponseBuffer.append("</").append("days").append(">");
					xmlResponseBuffer.append("<").append("tyears").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getTotalYears()));
					xmlResponseBuffer.append("</").append("tyears").append(">");
					xmlResponseBuffer.append("<").append("fte").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getFTE()));
					xmlResponseBuffer.append("</").append("fte").append(">");
					xmlResponseBuffer.append("<").append("service").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getServiceYears()));
					xmlResponseBuffer.append("</").append("service").append(">");
					xmlResponseBuffer.append("<").append("accrual").append(">");
					xmlResponseBuffer.append(serviceHistory.getAccrual());
					xmlResponseBuffer.append("</").append("accrual").append(">");
					xmlResponseBuffer.append("<").append("accrued").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(
							serviceHistory.getAccrued() * headroom.getMemberDetail().getPensionableSalary()));
					xmlResponseBuffer.append("</").append("accrued").append(">");
					xmlResponseBuffer.append("<").append("service60th").append(">");
					xmlResponseBuffer.append(NumberUtil.formatToDecimal(serviceHistory.getServiceYearsAt60th()));
					xmlResponseBuffer.append("</").append("service60th").append(">");
					
					xmlResponseBuffer.append("</").append(AJAX_SERVICE_HISTORY_RESPONSE_TAG).append(">");
				}
			}
			xmlResponseBuffer.append("</").append(AJAX_SERVICE_HISTORIES_RESPONSE_TAG).append(">");		
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_YEARS_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalYearsToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_YEARS_RESPONSE_TAG).append(">");	
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalServiceYearsToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_SERVICE_YEARS_RESPONSE_TAG).append(">");	
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_ACCRUED_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalAccruedMoneyToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_ACCRUED_RESPONSE_TAG).append(">");
			
			xmlResponseBuffer.append("<").append(AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG).append(">");
			xmlResponseBuffer.append(NumberUtil.formatToDecimal(headroom.getTotalServiceYearsAt60thToDate(_1stJanThisYear)));
			xmlResponseBuffer.append("</").append(AJAX_TOTAL_SERVICE_YEARS_60TH_RESPONSE_TAG).append(">");			
			
			xmlResponseBuffer.append("</").append(AJAX_HEADROOM_RESPONSE_TAG).append(">");
			
			xmlResponse = xmlResponseBuffer.toString();
		}
		
		LOG.info("Response headroom XML: " + xmlResponse);
		return xmlResponse;
	}		
	
	   /**
     * The method that builds key/value to structured xml document.<br>
     *
     * @param  taxYear newConfigs Hashtable store pair of value
     *
     * @return a structured document.
     */
    private String buildTaxYearXmlReponse(TaxYear taxYear) {
    	
        StringBuffer buffer = new StringBuffer();
        if (taxYear != null)
        {
	        // Add open tag modeller.
	        buffer.append("<").append(AJAX_TAX_YEAR_RESPONSE_TAG).append(">");
	
	        // Add pair of value inside
	        buffer.append("<Year>").append(taxYear.getYear()).append("</Year>")
	        	.append("<TaxYear>").append(taxYear.getTaxYear()).append("</TaxYear>")
	        	.append("<SOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getSoySalary())).append("</SOYSalary>")
	        	.append("<Accrued>").append(NumberUtil.formatToPercent(taxYear.getSoyAccrued())).append("</Accrued>")
	        	.append("<SOYBenefit>").append(NumberUtil.formatToDecimal(taxYear.getSoyBenefit())).append("</SOYBenefit>")
	        	.append("<CPI>").append(NumberUtil.formatToPercent(taxYear.getCpi())).append("</CPI>")
	        	.append("<CpiReval>").append(NumberUtil.formatToDecimal(taxYear.getCpiReval())).append("</CpiReval>")
	            .append("<SalaryIncrease>").append(NumberUtil.formatToPercent(taxYear.getSalaryIncrease())).append("</SalaryIncrease>")
	            .append("<EOYSalary>").append(NumberUtil.formatToDecimal(taxYear.getEoySalary())).append("</EOYSalary>")
	            .append("<AccruedRate>").append(taxYear.getAccrualRate()).append("</AccruedRate>")
	            .append("<EOYBenefit>").append(taxYear.getEoyBenefit()).append("</EOYBenefit>")
	            .append("<Increase>").append(NumberUtil.formatToDecimal(taxYear.getIncrease())).append("</Increase>")
	            .append("<AAFac>").append(taxYear.getaAFactor()).append("</AAFac>")
	            .append("<AACheck>").append(NumberUtil.formatToDecimal(taxYear.getAACheck())).append("</AACheck>")
	            .append("<AnnualAllowance>").append(taxYear.getAnnualAllowance()).append("</AnnualAllowance>")
	            .append("<AAExcess>").append(NumberUtil.formatToDecimal(taxYear.getaAExcess())).append("</AAExcess>")
	            .append("<TaxRate>").append(NumberUtil.formatToPercent(taxYear.getTaxRate())).append("</TaxRate>")
	            .append("<Tax>").append(NumberUtil.formatToNearestPound(taxYear.getTaxAmount())).append("</Tax>");
	
	        // Add close tag for modeller
	        buffer.append("</").append(AJAX_TAX_YEAR_RESPONSE_TAG).append(">");
        }
        
        LOG.info("XML response: " + buffer.toString());	
        // Return structured document xml type.
        return buffer.toString();
    }	

	/**
	 * Make a xml structured content.
	 * 
	 * @param message
	 *            error description
	 * 
	 * @return error message
	 */
	public String buildXmlResponseError(String message)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>")
				.append("\n");
		buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");

		return buffer.toString();
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Print servlet information!
	 * 
	 * @return String information!
	 */
	public String getServletInfo()
	{
		return "Given short description";
	}
	
	public static int getCurrentUserSecurityIndicator (HttpServletRequest request)
	{
		int securityIndicator = -1;	// Default to all report runner
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		if(currentUser != null)
		{			
			String accDescription = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL)).trim(); 
			LOG.info("accDescription: " + accDescription);
			
			// if description is blank then return 0
			if (accDescription == null || accDescription.trim().equals(""))
			{
				return -1;
			}
			
			try {
				// Modified by Huy due to Jira: http://www.c-mg.info/jira/browse/REPORTING-96
				
				/*
				 * 	When setting up a user for superuser and reporting duties, the description field is used to determine the access level to member records - Default (standard records), 
				 * 	Execs, Senior Execs and Team. In the current design a blank description means the user has no level at all. Also, any whitespace means the user has no access level 
				 * 	either. 
					It was the original intention, that the decription would be trimmed of all white space and then compared with the level mapper. It was also the intention that after 
					trimming, if the description was an empty string "Default" would be assumed. 
					While in here, can we please add a new feature. As well as the above tokens, can we add in a user alignment token. for example "Aquila:BPJOHNSN" (without the quotes) 
					will take the DATASEC_HIGH column assigned to the BPJOHNSN user in the administrator database (USER_INFO table). A comma separated list should be supported and the 
					highest level from each element will be the users actual le	vel. For example, the following are all valid strings based on all of the above.
					 
					"" = default level 
					" D efa ult " = default level 
					"Default,Team" = Team level is effective 
					"Team, Aquila:BPTMPUSR" = probably team levels will be effective. 
					The concern in the business is that people's levels are not consistent across administrator and PensionLine because team leaders do not correctly request the accounts. 
				 */
				if (accDescription != null)
				{
					
					Connection aquilaConn = null;
					
					String selectSIQuery = "Select DATASEC_HIGH from USER_INFO where USER_NAME = ?";
					
					// split the security indicator
					String[] securityDescriptions = accDescription.split(",");
					
					for (int i = 0; i < securityDescriptions.length; i++)
					{
						String securityDescription = securityDescriptions[i].trim();
						LOG.info("Security description: " + securityDescription);
						int securityIndicatorTmp = -1;
						
						if (securityDescription.trim().toLowerCase().indexOf("aquila:") == 0)
						{
							String aquilaUserID = securityDescription.substring("aquila:".length());
																	
							try
							{
								// get security indicator from Aquila database
								if (aquilaConn == null)
								{
									aquilaConn = DBConnector.getInstance().getDBConnFactory(Environment.AQUILA);
								}										
								
								if (aquilaConn != null)
								{
									PreparedStatement pstm = aquilaConn.prepareStatement(selectSIQuery);
									
									pstm.setString(1, aquilaUserID);
									
									ResultSet rs = pstm.executeQuery();
									if (rs.next())
									{
										securityIndicatorTmp = Integer.parseInt(rs.getString(1));
									}
									
									// if DATASEC_HIGH return 0, then the highest value
									if (securityIndicatorTmp == 0)
									{
										securityIndicatorTmp = 101;
									}
								}
							}
							catch (Exception e) 
							{
								LOG.error("Error while getting security indicator from Aquila: " + e.toString());
							}
						}
						else
						{
							try
							{
								// Read security indicator from a mapper file
								XmlReader reader = new XmlReader();
								
								byte[] arr = reader.readFile(Environment.SECURITYMAPPER_FILE);
					
								DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
								
								DocumentBuilder builder = factory.newDocumentBuilder();
								
								Document document = null;
					
								ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
								
								document = builder.parse(bIn);
					
								NodeList accessLevelNodeList = document.getElementsByTagName(securityDescription);
					
								if (accessLevelNodeList == null || securityDescription == null || securityDescription.equals(""))
								{
									// if the description of the superUser acc dose not exist in
									// the file SecurityMapper
									LOG.error("From mapper file: SecurityIndicator not set for this member: " + securityDescription);
								} 
								else 
								{
									// if exist
									Node accessLevelNode = accessLevelNodeList.item(0);
									if (accessLevelNode.getNodeType() == Node.ELEMENT_NODE) 
									{
										securityIndicatorTmp = Integer.parseInt(accessLevelNode.getTextContent());
										LOG.info("From mapper file: superuser member " + currentUser.getName() + " has security indicator = " + securityIndicatorTmp + " with " + securityDescription);
									}
								}							
							}
							catch (Exception e) 
							{
								LOG.error("Error while getting security indicator from mapper file: " + e.toString());
							}
						}
						
						if (securityIndicator < securityIndicatorTmp)
						{
							securityIndicator = securityIndicatorTmp;
						}
					}
					
					if (aquilaConn != null)
					{
						DBConnector.getInstance().close(aquilaConn);
					}
				}				
			}
			catch (Exception e) 
			{
				LOG.error("Error in getting report runner SI number: " + e.toString());
			}
		}
		
		LOG.info("Member SI: " + securityIndicator);
		
		return securityIndicator;
	}		
}
