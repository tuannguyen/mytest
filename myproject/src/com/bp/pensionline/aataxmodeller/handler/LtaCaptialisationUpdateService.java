package com.bp.pensionline.aataxmodeller.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.aataxmodeller.util.ConfigurationUtil;
import com.bp.pensionline.aataxmodeller.util.DefaultConfiguration;
import com.bp.pensionline.aataxmodeller.util.OpenCmsFileUtil;
import com.bp.pensionline.aataxmodeller.util.TaxUtil;

@SuppressWarnings("serial")
public class LtaCaptialisationUpdateService extends HttpServlet{
	public static final Log LOG = CmsLog.getLog(LtaCaptialisationUpdateService.class);

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ConfigurationUtil util = new ConfigurationUtil();
		String editedElement = (String)util.isNull(request.getParameter("editedElement"),"");
		String editedElementValue = (String)util.isNull(request.getParameter("editedElementValue"),"");
		String type = (String)util.isNull(request.getParameter("type"),"");
		 
		byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + TaxUtil.CONFIG_FILE_NAME);

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
		System.out.println("AE Admin type: " + type);
		System.out.println("AE Admin editedElement: " + editedElement);
		System.out.println("AE Admin editedElementValue: " + editedElementValue);
		updateXMLFile(defaultValueContentBytes, editedElement, editedElementValue, response, type);
		
	}
	
	/**
     * The method update default.xml.<br>
     *
     * @param  doc array of bytes.
     * @param  type: type of sub note (LTA or Capitalisation).
     * @param  editedElement: element of sub note.
     * @param  editedElementValue: element value.
     * @return HashMap objects.
     */
    public void updateXMLFile(byte[] doc, String editedElement, String editedElementValue, HttpServletResponse response, String type) {
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTags = root.getChildNodes();

            // NodeList nodeList = root.getElementsByTagName(tag);
            Node subNode = null;
            NodeList child = null;
            String tagObjectName = null;
            NodeList nodeList = null;
            boolean updateNextYear = false;
            String currentYearStr = "";
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            String currentYStr = String.valueOf(currentYear);
            // Loop over sub tags inside defaultvalue tag
            for (int tagIndex = 0; tagIndex < listObjectTags.getLength(); tagIndex++) {
                if (listObjectTags.item(tagIndex).getNodeType() == Node.ELEMENT_NODE) {
                    tagObjectName = listObjectTags.item(tagIndex).getNodeName();
                }
                if (tagObjectName != null) {
                    // Get tag by name
                    nodeList = root.getElementsByTagName(tagObjectName);
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        // Get item at current element
                        subNode = nodeList.item(i);

                        // Check update for subNode via type (LTA or Capitalisation)
                        if (subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE) 
                                && subNode.getNodeName().equalsIgnoreCase(type)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            System.out.println("child length: " + child.getLength());
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                    
                                    //get all current LTA value from default.xml
                                    if((child.item(j).getNodeName().trim().equalsIgnoreCase(editedElement))&& child.item(j).hasChildNodes()){
                                    	child.item(j).getFirstChild().setNodeValue(editedElementValue);
                                    	updateNextYear = true;
                                    	currentYearStr = editedElement;
                                    	//defaultLTAValues.put(child.item(j).getNodeName().trim(), child.item(j).getFirstChild().getNodeValue().trim());
                                    }
                                    //Update the LTA value from the year after
                                    if((currentYearStr.contains(currentYStr)) && (updateNextYear) && (child.item(j).getNodeName().trim().compareTo(currentYearStr))>0 && child.item(j).hasChildNodes()){
                                    	child.item(j).getFirstChild().setNodeValue(editedElementValue);
                                    	//defaultLTAValues.put(child.item(j).getNodeName().trim(), child.item(j).getFirstChild().getNodeValue().trim());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Output the XML

            //set up a transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(document);
            trans.transform(source, result);
            String xmlString = sw.toString();
            
            String xmlResponse;
    		boolean isConfigured = buildXmlResponse(xmlString, TaxUtil.CONFIG_FILE_NAME);
    		if (isConfigured) {
    			xmlResponse = TaxUtil.buildSuccessXmlMessage();
    		} else {
    			xmlResponse = TaxUtil
    					.buildXmlResponseError("Unknown error while tax modeller process!");
    		}
    		PrintWriter out = response.getWriter();
    		out.print(xmlResponse);
    		out.close();
    		LOG.info("Response XML: " + xmlResponse);
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }

    }
    /**
     * Build xml response.
     *
     * @param  xml String value.
     *
     * @return response of xml content
     */
    public boolean buildXmlResponse(String xml, String fileName) {
        boolean isConfigured = false;
        DefaultConfiguration defaultTax = new DefaultConfiguration();
        try {
            isConfigured = defaultTax.writeToConfiguredXmlFile(fileName, xml);
            return isConfigured;
        } catch (Exception e) {
            // Store message error log.
            LOG.info(e.getMessage());
        }

        return isConfigured;
    }
}
