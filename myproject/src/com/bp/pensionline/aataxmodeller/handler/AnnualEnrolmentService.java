/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.PensionDetails;
import com.bp.pensionline.aataxmodeller.dto.SchemeHistory;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.dto.TaxYear;
import com.bp.pensionline.aataxmodeller.modeller.Headroom;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.modeller.TaxModellerAuditor;
import com.bp.pensionline.aataxmodeller.util.ConfigurationUtil;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.aataxmodeller.util.DefaultConfiguration;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 * 
 * <p>
 * Optionally, enter a longer description.
 * </p>
 * 
 * @author Admin
 * @version 1.0
 */
public class AnnualEnrolmentService extends HttpServlet
{
	
	// ~ Static fields/initializers
	// -----------------------------------------------------------------

	/** Storing log for this class. */
	public static final Log LOG = CmsLog.getLog(AnnualEnrolmentService.class);

	/** Represent a tag name. */
	public static final String AJAX_ANNUAL_ENROLMENT_RESPONSE_TAG = "AnualEnrolment";
	
	public static final int MAX_RETIRE_AGE = 65;
	
	private static final long serialVersionUID = 1L;

	// ~ Methods
	// ------------------------------------------------------------------------------------

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		
		String xmlResponse = null;

		// Get request from client
		String overideAccrualParam = request.getParameter("accrual");
		String overideSalaryParam = request.getParameter("salary");
		//String overideCashPercentParam = request.getParameter("cash");
		
		Date headroomDate = new Date();

		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
//		String bgroup = null;
//		String refno = null;
		MemberDetail memberDetail = null;
		//MemberDao memberDao = null;
		if (currentUser != null)
		{
			memberDetail = (MemberDetail)currentUser.getAdditionalInfo().get(Environment.MEMBER_DETAIL_KEY);
			//memberDao = (MemberDao)currentUser.getAdditionalInfo().get(Environment.MEMBER_KEY);
		}
		
		try
		{
			
			if (memberDetail != null)
			{
				// check member is over 65;
				int memberAge = DateUtil.getYearsAndDaysBetween(memberDetail.getDateOfBirth(), headroomDate)[0];
				
				if (memberAge >= MAX_RETIRE_AGE)
				{
					xmlResponse = buildXmlResponseError("Based on your data, you will not be able to use this tool.");
					out.print(xmlResponse);
					out.close();
					return;
				}
				
				LOG.info("overideAccrualParam: " + overideAccrualParam);
				LOG.info("overideSalaryParam: " + overideSalaryParam);
				//LOG.info("overideCashPercentParam: " + overideCashPercentParam);
				int overideAccrual = -1;
				double overideSalary = -1;		
				//int overideCashPercent = 0;
							
				if (overideAccrualParam != null)
				{
					try
					{
						overideAccrual = Integer.parseInt(overideAccrualParam);						
					}
					catch (NumberFormatException nfe)
					{
						LOG.error("Overide accrual parameter is not a number: " + overideAccrualParam);
					}					
				}
				
				if (overideSalaryParam != null)
				{
					try
					{
						overideSalary = Double.parseDouble(overideSalaryParam);
					}
					catch (NumberFormatException nfe)
					{
						LOG.error("Overide salary parameter is not a number: " + overideSalaryParam);
					}	
				}				
//				if (overideCashPercentParam != null)
//				{
//					try
//					{
//						overideCashPercent = Integer.parseInt(overideCashPercentParam);
//					}
//					catch (NumberFormatException nfe)
//					{
//						LOG.error("Overide cash percent parameter is not a number: " + overideCashPercentParam);
//					}					
//				}
				
				

				Headroom headroom = new Headroom(memberDetail, headroomDate);
				headroom.setEffectiveDate(DateUtil.getFirstDayOfNextYear(headroomDate));								
				
				
				// Audit the member access to BP_STATS_POS
//				TaxModellerAuditor auditor = new TaxModellerAuditor();
//				auditor.doAccessAudit(request);

									
				int currentAccrual = headroom.getAccrual();
				double currentSalary = headroom.getFps();
	            		            
	            // Tax modeller configuration
				TaxModeller taxModeller = new TaxModeller();				
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(headroomDate);
				TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(headroomDate));						
				double taxrate = Double.parseDouble(values.getAnnualAllowanceTaxRate()) / 100;
	            double inflation = Double.parseDouble(values.getAnnualInflation()) / 100;
	            double lta = Double.parseDouble(values.getLta());
	            			            		            
	            // if configurations is false then use member's detail data
	            if (!values.isConfigurationUsed())
	            {
	            	inflation= memberDetail.getCpi();
	            	lta = memberDetail.getLTA();
	            }		
	            
	            headroom.setLta(lta);
	           // headroom.setCashPercent(overideCashPercent);	            
	            
	            Calendar calendarAt1stOct = Calendar.getInstance();
	            calendarAt1stOct.setTime(headroomDate);
	            calendarAt1stOct.set(Calendar.DAY_OF_MONTH, 1);
	            calendarAt1stOct.set(Calendar.MONTH, Calendar.OCTOBER);
	            calendarAt1stOct.set(Calendar.HOUR_OF_DAY, 0);
	            calendarAt1stOct.set(Calendar.MINUTE, 0);
	            calendarAt1stOct.set(Calendar.SECOND, 0);
	            
	            TaxYear taxYear = null;
	            // if headroom date < 1st October, calculate for current PIP year
	            if (headroomDate.before(calendarAt1stOct.getTime()))
	            {
	            	Date dateAt31DecLastYear = DateUtil.getPreviousDay(DateUtil.getFirstDayOfThisYear(headroomDate));
	            	Date dateAt31DecThisYear = DateUtil.getEndDayOfThisYear(headroomDate);
	            	
	            	// Calculate current tax year
		            double soySalary = memberDetail.getSalaryBefore(dateAt31DecLastYear);
					double eoySalary = memberDetail.getSalaryBefore(dateAt31DecThisYear);
	            	
	            	// if modelling
					if (overideAccrual > -1)
					{										
						headroom.setAccrual(overideAccrual);
					}					
					if (overideSalary > -1)
					{
						headroom.setFps(overideSalary);
						eoySalary = overideSalary;
					}
					
					headroom.calculate();		
		            
		            // debug
		            headroom.debugTranches(headroom.getServiceTranches());

					double soyServiceDays = headroom.getTotalServiceDaysAt60thToDate(dateAt31DecLastYear);	// 31-Dec-last year
					double eoyServiceDays = headroom.getTotalServiceDaysAt60thToDate(dateAt31DecThisYear); // 31-Dec-this year
					
					// use current accrual to calculate tax
					taxModeller.setSystemDate(DateUtil.getFirstDayOfThisYear(headroomDate));
					taxYear = taxModeller.calculateTaxYearByServiceDays(
							soyServiceDays, soySalary, eoyServiceDays, eoySalary, 
			        		headroom.getCurrentAccrual(), taxrate, inflation,
			                Double.parseDouble(values.getCapitalisation()),
			                Double.parseDouble(values.getAnnualAllowance()));
	            }
	            else
	            {
	            	// calculate for next PIP year
	            	Date dateAt31DecThisYear = DateUtil.getEndDayOfThisYear(headroomDate);
	            	Date dateAt31DecNextYear = DateUtil.getEndDayOfNextYear(headroomDate);
	            	
	            	// Calculate current tax year
		            double soySalary = memberDetail.getSalaryBefore(dateAt31DecThisYear);
					double eoySalary = memberDetail.getSalaryBefore(dateAt31DecNextYear);	
					
	            	// if modelling
					if (overideAccrual > -1)
					{										
						headroom.setAccrual(overideAccrual);
					}					
					if (overideSalary > -1)
					{
						headroom.setFps(overideSalary);
						eoySalary = overideSalary;
					}	
					headroom.calculate();
					// debug
		            headroom.debugTranches(headroom.getServiceTranches());
				
		            double soyServiceDays = headroom.getTotalServiceDaysAt60thToDate(dateAt31DecThisYear);	// 31-Dec-this year
					double eoyServiceDays = headroom.getTotalServiceDaysAt60thToDate(dateAt31DecNextYear); // 31-Dec-next year
					
					// use overide accrual to calcualte tax
					taxModeller.setSystemDate(DateUtil.getFirstDayOfNextYear(headroomDate));
					taxYear = taxModeller.calculateTaxYearByServiceDays(
							soyServiceDays, soySalary, eoyServiceDays, eoySalary, 
			        		headroom.getAccrual(), taxrate, inflation,
			                Double.parseDouble(values.getCapitalisation()),
			                Double.parseDouble(values.getAnnualAllowance()));	            	
	            }	            	            
				
				
				// audit input and output
				TaxModellerAuditor auditor = new TaxModellerAuditor();
				auditor.doAETestModelAudit(request, headroomDate, 
						headroom.getFps(), headroom.getAccrual(), 65, headroom.getPensionDetails(), 
						taxYear, taxYear);
				
				xmlResponse = buildXmlResponse(headroom, taxYear, calendarAt1stOct.get(Calendar.YEAR), currentAccrual, currentSalary, overideAccrual, overideSalary);				
			}
			else
			{
				LOG.error("Error while getting member from user session for annual enrolment!");
				xmlResponse = buildXmlResponseError("Error while checking member: " + memberDetail);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while getting annual enrolment for member: " + e.toString());
			xmlResponse = buildXmlResponseError("Error while getting annual enrolment for member. " + e.toString());
		}
		
		out.print(xmlResponse);
		out.close();
	}

	/**
	 * Build xml response but not use this method. Use redirect for not AJAX processing
	 * 
	 * @param currentSalary xml String value.
	 * @param taxRateOnExcess
	 * 
	 * @return response of xml content
	 */
	public String buildXmlResponse(Headroom headroom, TaxYear taxYear, int currentYear, 
			int currentAccrual, double currentSalary, int modelAccrual, double modelSalary)
	{
		String xmlResponse = null;		
		
		if (headroom != null && taxYear != null)
		{
			Date today = headroom.getHeadroomDate();
			
			Calendar calendar = Calendar.getInstance();					
			
			StringBuffer xmlResponseBuffer = new StringBuffer();
			xmlResponseBuffer.append("<").append(AJAX_ANNUAL_ENROLMENT_RESPONSE_TAG).append(">");
			
			
			// Member information
			calendar.setTime(headroom.getMemberDetail().getDateOfBirth());
			int birthYear = calendar.get(Calendar.YEAR);
			xmlResponseBuffer.append("<BirthYear>").append(birthYear).append("</BirthYear>");
			
			calendar.setTime(headroom.getMemberDetail().getJoinedScheme());
			int schemeYear = calendar.get(Calendar.YEAR);			
			xmlResponseBuffer.append("<SchemeYear>").append(schemeYear).append("</SchemeYear>");
			xmlResponseBuffer.append("<CurrentYear>").append(currentYear).append("</CurrentYear>");
			xmlResponseBuffer.append("<PipYear>").append(taxYear.getYear()).append("</PipYear>");
			xmlResponseBuffer.append("<LTA>").append((int)headroom.getLta()).append("</LTA>");
			// scheme histories
			xmlResponseBuffer.append("<SchemeHistories>");
			ArrayList<SchemeHistory> schemeHistories = headroom.getMemberDetail().getSchemeHistory();
			for (int i = 0; i < schemeHistories.size(); i++)
			{
				xmlResponseBuffer.append("<SchemeHistory>");
				SchemeHistory schemeHistory = schemeHistories.get(i);
				Date schemeFrom = schemeHistory.getFrom();
				calendar.setTime(schemeFrom);
				int year = calendar.get(Calendar.YEAR);
				xmlResponseBuffer.append("<Year>").append(year).append("</Year>");
				
				int accrual = schemeHistory.getAccrual();
				xmlResponseBuffer.append("<Accrual>").append(accrual).append("</Accrual>");
				xmlResponseBuffer.append("</SchemeHistory>");
			}
			xmlResponseBuffer.append("</SchemeHistories>");	
			
			// current option
			DefaultConfiguration configuration = new DefaultConfiguration();
			HashMap<String, String> salaryConfig = configuration.loadConfigurationsForServiceSalary();
			String lowerBoundStr = salaryConfig.get(ConfigurationUtil.LOWER_BOUND);
			String upperBoundStr = salaryConfig.get(ConfigurationUtil.UPER_BOUND);
			
			double minSalary = 0.0;
			if (lowerBoundStr != null)
			{
				minSalary = currentSalary - (currentSalary * Integer.parseInt(lowerBoundStr) / 100);
			}
			double maxSalary = headroom.getLta();
			if (lowerBoundStr != null)
			{
				maxSalary = currentSalary + (currentSalary * Integer.parseInt(upperBoundStr) / 100);
			}
			
			String carChoice = configuration.loadConfigurationsForCarConfig();
			
			xmlResponseBuffer.append("<CurrentOptions>");
			xmlResponseBuffer.append("<Salary>").append((int)currentSalary).append("</Salary>");
			xmlResponseBuffer.append("<MinSalary>").append((int)minSalary).append("</MinSalary>");
			xmlResponseBuffer.append("<MaxSalary>").append((int)maxSalary).append("</MaxSalary>");
			xmlResponseBuffer.append("<Accrual>").append(currentAccrual).append("</Accrual>");
			xmlResponseBuffer.append("<CarChoice>").append(carChoice).append("</CarChoice>");
			xmlResponseBuffer.append("</CurrentOptions>");
			
			// modelling option
			xmlResponseBuffer.append("<ModelOptions>");
			xmlResponseBuffer.append("<Salary>").append(modelSalary).append("</Salary>");
			xmlResponseBuffer.append("<Accrual>").append(modelAccrual).append("</Accrual>");													
			xmlResponseBuffer.append("</ModelOptions>");				
			
			
			// Current tax year
			xmlResponseBuffer.append("<TaxYear>");
			xmlResponseBuffer.append("<Year>").append(taxYear.getYear()).append("</Year>");
			xmlResponseBuffer.append("<FiscalYear>").append(taxYear.getTaxYear()).append("</FiscalYear>");
			xmlResponseBuffer.append("<Amount>").append((int)(taxYear.getaAExcess() > 0 ? taxYear.getaAExcess(): 0)).append("</Amount>");
			xmlResponseBuffer.append("<Charge>").append((int)taxYear.getTaxAmount()).append("</Charge>");			
			xmlResponseBuffer.append("</TaxYear>");						
			
			// Retirement details
			xmlResponseBuffer.append("<Retirements>");
			calendar.setTime(headroom.getMemberDetail().getDateAt65th());
			int yearAt65 = calendar.get(Calendar.YEAR);
			calendar.setTime(headroom.getMemberDetail().getDateAt55th());
			int yearAt55 = calendar.get(Calendar.YEAR);
			calendar.setTime(headroom.getMemberDetail().getDateOfBirth());
			int yearAtBirth = calendar.get(Calendar.YEAR);
			
			int memberCurrentAge = DateUtil.getYearsAndDaysBetween(headroom.getMemberDetail().getDateOfBirth(), today)[0] + 1;
			int retireStartYear = (yearAt55 > (yearAtBirth + memberCurrentAge)) ? yearAt55 : (yearAtBirth + memberCurrentAge);
			
			for (int i = retireStartYear; i <= yearAt65; i++)
			{				
				calendar.setTime(headroom.getMemberDetail().getDateOfBirth());
				calendar.set(Calendar.YEAR, i);
				headroom.setDoR(calendar.getTime());
				headroom.calculate();	
				
				PensionDetails pensionDetails = headroom.getPensionDetails();
				if (pensionDetails != null)
				{
					xmlResponseBuffer.append("<Retirement>");
					xmlResponseBuffer.append("<Year>").append(i).append("</Year>");
					xmlResponseBuffer.append("<Pension>").append((int)pensionDetails.getReducedPension()).append("</Pension>");
					xmlResponseBuffer.append("<PensionWithMaxCash>").append((int)pensionDetails.getPensionWithMaxCash()).append("</PensionWithMaxCash>");
					xmlResponseBuffer.append("<PensionWithHaftCash>").append((int)pensionDetails.getPensionWithHaftCash()).append("</PensionWithHaftCash>");	
					xmlResponseBuffer.append("<CashLumpSum>").append((int)pensionDetails.getCashLumpSum()).append("</CashLumpSum>");
					xmlResponseBuffer.append("<SchemeCash>").append((int)pensionDetails.getMaxSchemeCash()).append("</SchemeCash>");
					xmlResponseBuffer.append("<PensionPot>").append((int)pensionDetails.getPensionPot()).append("</PensionPot>");
					xmlResponseBuffer.append("</Retirement>");
				}
			}
			xmlResponseBuffer.append("</Retirements>");
			
			xmlResponseBuffer.append("</").append(AJAX_ANNUAL_ENROLMENT_RESPONSE_TAG).append(">");
			
			xmlResponse = xmlResponseBuffer.toString();
		}
		
		LOG.info("Response XML: " + xmlResponse);
		return xmlResponse;
	}	

	/**
	 * Make a xml structured content.
	 * 
	 * @param message
	 *            error description
	 * 
	 * @return error message
	 */
	public String buildXmlResponseError(String message)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("<").append(AJAX_ANNUAL_ENROLMENT_RESPONSE_TAG).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>")
				.append("\n");
		buffer.append("</").append(AJAX_ANNUAL_ENROLMENT_RESPONSE_TAG).append(">\n");

		return buffer.toString();
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Print servlet information!
	 * 
	 * @return String information!
	 */
	public String getServletInfo()
	{
		return "Given short description";
	}
}
