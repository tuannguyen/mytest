/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.handler;

import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.aataxmodeller.util.NumberUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 * 
 * <p>
 * Optionally, enter a longer description.
 * </p>
 * 
 * @author Admin
 * @version 1.0
 */
public class UpdateMarginalTaxRate extends HttpServlet
{
	
	// ~ Static fields/initializers
	// -----------------------------------------------------------------

	/** Storing log for this class. */
	public static final Log LOG = CmsLog.getLog(UpdateMarginalTaxRate.class);

	/** Represent a tag name. */
	public static final String AJAX_TAX_MODELLER_RESPONSE_TAG = "TaxModeller";
	
	public static final String AJAX_MARGINAL_TAX_RATE_TAG = "AaMarginalTaxRate";
	public static final String AJAX_TAX_DUE_TAG = "AaTaxDue";
	public static final String AJAX_ALT_TAX_DUE_TAG = "AltAaTaxDue";
		
	
	
	private static final long serialVersionUID = 1L;

	// ~ Methods
	// ------------------------------------------------------------------------------------

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		
		String xmlResponse = null;

		// Get request from client
//		String bgroup = request.getParameter("bgroup");
//		String refno = request.getParameter("refno");
//		String sdate = request.getParameter("sdate");
		
		
//		if (headroomDate == null || DateUtil.isDateBeforeToday(headroomDate) )
//		{
//			// check if sdate is a past date
//			headroomDate = new Date();
//		}
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String bgroup = null;
		String refno = null;
		MemberDao memberDao = null;
		if (currentUser != null)
		{
			bgroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refno = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
		}
		
		try
		{
			if (memberDao != null)
			{
				TaxModeller taxModeller = new TaxModeller();
				Calendar calendar = Calendar.getInstance();
				TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(calendar.getTime()));
				
				//debugMemberDao(memberDao);
				
				double taxAmtDouble = 0.00;
				double altTaxAmtDouble = 0.00;
				double marginalTaxRateDouble =  0.00;
				
				String taxableAmt = memberDao.get("AaTaxableAmt");
				String altTaxableAmt = memberDao.get("AltAaTaxableAmt");
				
				String marginalTaxRate = request.getParameter("rate");
				
				LOG.info("Modelled tax rate: " + marginalTaxRate);
				
				// get default aa tax rate if parameter not found
				if (marginalTaxRate == null)
				{
					marginalTaxRate = values.getAnnualAllowanceTaxRate();
				}
				marginalTaxRateDouble = NumberUtil.parseDouble(marginalTaxRate);
				
				
				taxAmtDouble = NumberUtil.currencyToDecimal(taxableAmt);
				altTaxAmtDouble = NumberUtil.currencyToDecimal(altTaxableAmt);
			
				
				
				String taxDue = NumberUtil.formatToNearestPound((taxAmtDouble * marginalTaxRateDouble)/ 100);
				String altTaxDue = NumberUtil.formatToNearestPound((altTaxAmtDouble * marginalTaxRateDouble)/ 100);
				
				xmlResponse = buildXmlResponse(NumberUtil.formatToNearestPound(marginalTaxRateDouble), taxDue, altTaxDue);
				
			}
			else
			{
				xmlResponse = buildXmlResponseError("Error while updating marginal tax rate. Member data not available!");
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while updating marginal tax rate " + bgroup + "-" + refno + ": " + e.toString());
			xmlResponse = buildXmlResponseError("Error while updating marginal tax rate");
		}
		
		LOG.info("xmlResponse: " + xmlResponse);
		out.print(xmlResponse);
		out.close();
	}

	/**
	 * Build xml response but not use this method. Use redirect for not AJAX processing
	 * 
	 * @param currentSalary xml String value.
	 * @param taxRateOnExcess
	 * 
	 * @return response of xml content
	 */
	public String buildXmlResponse(String marginalTaxRate, String taxDue, String altTaxDue)
	{
        StringBuffer buffer = new StringBuffer();
        
        buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
        buffer.append("<").append(AJAX_MARGINAL_TAX_RATE_TAG).append(">").append(marginalTaxRate).append("</").append(AJAX_MARGINAL_TAX_RATE_TAG).append(">");  
        buffer.append("<").append(AJAX_TAX_DUE_TAG).append(">").append(taxDue).append("</").append(AJAX_TAX_DUE_TAG).append(">");
        buffer.append("<").append(AJAX_ALT_TAX_DUE_TAG).append(">").append(altTaxDue).append("</").append(AJAX_ALT_TAX_DUE_TAG).append(">");
        buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">");
   
   // Return structured document xml type.
   return buffer.toString();
	}
		

	/**
	 * Make a xml structured content.
	 * 
	 * @param message
	 *            error description
	 * 
	 * @return error message
	 */
	public String buildXmlResponseError(String message)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("<").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>")
				.append("\n");
		buffer.append("</").append(AJAX_TAX_MODELLER_RESPONSE_TAG).append(">\n");

		return buffer.toString();
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	/**
	 * Print servlet information!
	 * 
	 * @return String information!
	 */
	public String getServletInfo()
	{
		return "Given short description";
	}
	
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
			xmlString.append("</MemberData>\n");
//			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
//					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}	
}
