package com.bp.pensionline.aataxmodeller.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.aataxmodeller.util.ConfigurationUtil;
import com.bp.pensionline.aataxmodeller.util.DefaultConfiguration;
import com.bp.pensionline.aataxmodeller.util.TaxUtil;

@SuppressWarnings("serial")
public class AACarryCarSalaryConfigService extends HttpServlet {

	/** Storing log for this class. */
	public static final Log LOG = CmsLog
			.getLog(AACarryCarSalaryConfigService.class);

	
	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The method which response to request from client side.<br>
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception occur
	 * @throws IOException
	 *             exception.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * The method that handle with requests.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * 
	 * @throws ServletException
	 *             exception
	 * @throws IOException
	 *             exception
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ConfigurationUtil util = new ConfigurationUtil();
		HashMap<String, String> map = new HashMap<String, String>();
		String type = (String)util.isNull(request.getParameter("type"), "");
		String xml="";
		String fileName="";
		if(type.equalsIgnoreCase("AaCarryConfig")){
			String initTag = (String) util.isNull(request.getParameter("initTag"),"");
			String operationTag = (String) util.isNull(request.getParameter("operationTag"), "");
			String endTag = (String) util.isNull(request.getParameter("endTag"), "");
			map.put(ConfigurationUtil.INIT_TAG, initTag);
			map.put(ConfigurationUtil.OPERATION_TAG, operationTag);
			map.put(ConfigurationUtil.END_TAG, endTag);
			xml = util.buildXML("AaCarryConfig", map);
			fileName =ConfigurationUtil.AA_CARRY_CONFIG;
		} else if(type.equalsIgnoreCase("carConfig")){
			String optionValue = (String)util.isNull(request.getParameter("serviceCar"), "");
			xml = util.buildXML(ConfigurationUtil.CAR_CONFIG_ELEMENT, optionValue);
			fileName=ConfigurationUtil.CAR_CONFIG;
		}else if(type.equalsIgnoreCase("serviceSalary")){
			String lowerBound = (String) util.isNull(request.getParameter("lowerBound"),"");
			String uperBound = (String) util.isNull(request.getParameter("uperBound"), "");
			map.put(ConfigurationUtil.LOWER_BOUND, lowerBound);
			map.put(ConfigurationUtil.UPER_BOUND, uperBound);
			xml = util.buildXML("serviceSalary", map);
			fileName=ConfigurationUtil.SALARY_SERVICE;
		}
		
		String xmlResponse;
		boolean isConfigured = buildXmlResponse(xml, fileName);
		if (isConfigured) {
			xmlResponse = TaxUtil.buildSuccessXmlMessage();
		} else {
			xmlResponse = TaxUtil
					.buildXmlResponseError("Unknown error while tax modeller process!");
		}
		PrintWriter out = response.getWriter();
		out.print(xmlResponse);
		out.close();
		LOG.info("Response XML: " + xml);

	}
	 /**
     * Build xml response.
     *
     * @param  xml String value.
     *
     * @return response of xml content
     */
    public boolean buildXmlResponse(String xml, String fileName) {
        boolean isConfigured = false;
        DefaultConfiguration defaultTax = new DefaultConfiguration();
        try {
            isConfigured = defaultTax.writeToConfiguredXmlFile(fileName, xml);
            return isConfigured;
        } catch (Exception e) {
            // Store message error log.
            LOG.info(e.getMessage());
        }

        return isConfigured;
    }
}
