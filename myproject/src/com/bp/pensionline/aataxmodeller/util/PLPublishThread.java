package com.bp.pensionline.aataxmodeller.util;


import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.publish.CmsPublishManager;
import org.opencms.publish.Messages;
import org.opencms.report.A_CmsReportThread;
import org.opencms.report.CmsShellReport;
import org.opencms.report.I_CmsReport;

public class PLPublishThread extends A_CmsReportThread
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
    /** The CmsObject used to start this thread. */
    private CmsObject m_cms;

    /** The list of resources to publish. */
    private CmsPublishList m_publishList;
    
    I_CmsReport m_report = null;



    /**
     * Creates a Thread that publishes the Cms resources contained in the specified Cms publish 
     * list.<p>
     * 
     * @param cms the current OpenCms context object
     * @param publishList a Cms publish list
     * @param settings the workplace settings of the current user
     * @see org.opencms.file.CmsObject#getPublishList(org.opencms.file.CmsResource, boolean)
     * @see org.opencms.file.CmsObject#getPublishList()
     */
    public PLPublishThread(CmsObject cms, CmsPublishList publishList) {

        //super(cms, Messages.get().getBundle().key(Messages.GUI_PUBLISH_TRHEAD_NAME_0));
    	//super(cms, Messages.get().getBundle().key(Messages.GUI_PUBLISH_TRHEAD_NAME_0));
    	super(cms, "Publishing Thread");
        m_cms = cms;
        m_publishList = publishList;

        m_report = new CmsShellReport(cms.getRequestContext().getLocale());
    }

    /**
     * @see org.opencms.report.A_CmsReportThread#getReportUpdate()
     */
    public String getReportUpdate() {

        return getReport().getReportUpdate();
    }
    
    

    @Override
	protected I_CmsReport getReport()
	{
		// TODO Auto-generated method stub
    	if (m_report == null)
		{
    		m_report = new CmsShellReport(m_cms.getRequestContext().getLocale());
		}
		return m_report;
	}

	/**
     * @see java.lang.Runnable#run()
     */
    public void run() {

        try {
        	m_report.println(Messages.get().container(Messages.RPT_PUBLISH_RESOURCE_BEGIN_0), I_CmsReport.FORMAT_HEADLINE);
        	CmsPublishManager publishManager = OpenCms.getPublishManager();
        	LOG.info("Star publishing time: "+System.currentTimeMillis());
        	publishManager.publishProject(m_cms, m_report, m_publishList);
        	while (publishManager.isRunning()) {
        		this.sleep(500);
        		if (!publishManager.isRunning()) {
        			break;
        		}
        	}
        	LOG.info("End publishing time: "+System.currentTimeMillis());
        	m_report.println(Messages.get().container(Messages.RPT_PUBLISH_RESOURCE_END_0), I_CmsReport.FORMAT_HEADLINE);
        } catch (Exception e) {
        	m_report.println(e);
            //LOG.error(Messages.get().getBundle().key(Messages.LOG_PUBLISH_PROJECT_FAILED_0), e);
            LOG.error(Messages.get().getBundle().key(Messages.ERR_PUBLISH_ENGINE_ERROR_0), e);
        }
    }
}
