/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.aataxmodeller.util;

import org.apache.commons.logging.Log;

import org.opencms.main.CmsLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author admin
 */
public final class TaxUtil {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Log for the class. */
    private static final Log LOG = CmsLog.getLog(TaxUtil.class);

    /** Represent configuration file name . */
    public static String XMLFILE_DIR = "/system/modules/com.bp.pensionline.aataxmodeller/config/";

    /** Represent configuration file name . */
    public static final String CONFIG_FILE_NAME = "default.xml";

    /** Represent configuration file name . */
    public static final String CONFIG_FLAG_FILE_NAME = "default_flag.xml";

    /** Represent node name. */
    public static final String AVERAGE_SALARY_INCREASE_TAG = "AverageSalaryIncrease";

    /** Represent node name. */
    public static final String ANNUAL_TAX_RATE_TAG = "AnnualAllowanceTaxRate";

    /** Represent node name. */
    public static final String CAPITALISATION_TAG = "Capitalisation";

    /** Represent node name. */
    public static final String ANNUAL_ALLOWANCE_TAG = "AnnualAllowance";

    /** Represent node name. */
    public static final String ANNUAL_INFLATION_TAG = "AnnualInflation";
    
    /** Represent node name. */
    public static final String LTA_TAG = "LTA";

    /** Represent a tag name. */
    public static final String AJAX_TAX_MODELLER_RESPONSE = "TaxModeller";

    /** Represent status message in structured document. */
    public static final String UPDATED_SUCCESS = "<status>update successful</status>";

    /** Represent a tag name. */
    public static final String AJAX_DEFAULT_RESPONSE = "DefaultValues";

    /** Represent a tag name. */
    public static final String FREE_TAX_TYPE = "-1";

    /** Represent a tag name. */
    public static final String PERCENTAGE_FORMAT = "#%";

    /** Represent a tag name. */
    public static final String TAX_DECIMAL_FORMAT = "#,##0.00";

    //~ Constructors -------------------------------------------------------------------------------

    /**
     * Default Constructor.<br>
     */
    private TaxUtil() {
    }

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * Returns the contents of the file in a byte array.
     *
     * @param  file File this method should read
     *
     * @return byte[] Returns a byte[] array of the contents of the file
     *
     * @throws IOException exception occurs on read file
     */
    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            LOG.info("File is too large to process");

            return null;
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while ((offset < bytes.length) && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException(ModellerMessage.INCOMPLETE_LOG + file.getName());
        }
        is.close();

        // Return array of bytes
        return bytes;
    }

    /**
     * Make a xml structured content.
     *
     * @param  message error description
     *
     * @return error message
     */
    public static String buildXmlResponseError(String message) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<").append(AJAX_DEFAULT_RESPONSE).append(">\n");
        buffer.append("     <Error>").append(message).append("</Error>").append("\n");
        buffer.append("</").append(AJAX_DEFAULT_RESPONSE).append(">\n");

        // Return response error description.
        return buffer.toString();
    }

    /**
     * The method allow replace all special characters.
     *
     * @param  xml String value
     *
     * @return String value.
     */
    public static String filter(String xml) {
        xml = xml.replaceAll("\\(amp\\)", "&");
        xml = xml.replaceAll("\\(lt\\)", "<");
        xml = xml.replaceAll("\\(gt\\)", ">");
        xml = xml.replaceAll("\\(plus\\)", "+");
        xml = xml.replaceAll("\\(percent\\)", "%");

        // After relace all
        return xml;
    }

    /**
     * The method get status document.
     *
     * @return a structured document.
     */
    public static String buildSuccessXmlMessage() {
        return UPDATED_SUCCESS;
    }

    /**
     * Format number.
     *
     * @param  num double value
     *
     * @return String value with formated number.
     *
     * @throws NumberFormatException any error occurs
     */
    public static String getFormatPercentage(double num) throws NumberFormatException {
        // Get percentage instance
        NumberFormat percentFormat = NumberFormat.getPercentInstance();
        percentFormat = new DecimalFormat(PERCENTAGE_FORMAT);

        return percentFormat.format(num);
    }

    /**
     * Format tax number.
     *
     * @param  num double value
     *
     * @return String value with formated number.
     *
     * @throws NumberFormatException any error occurs
     */
    public static String getTaxDisplayFormat(double num) throws NumberFormatException {
        // Get percentage instance
        NumberFormat taxFormat = NumberFormat.getPercentInstance();
        taxFormat = new DecimalFormat(TAX_DECIMAL_FORMAT);

        return taxFormat.format(num);
    }
}
