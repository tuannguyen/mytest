/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.aataxmodeller.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.aataxmodeller.dto.DefaultInfo;

/**
 * Class FactorConfiguration.
 *
 * <p>The class process factor table and parse xml.</p>
 *
 * @author  admin
 * @version 1.0
 */
public class DefaultConfiguration {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Log for the class. */
    private static final Log LOG = CmsLog.getLog(DefaultConfiguration.class);

    /** Represent node name. */
    public static final String DEFAULT_TAG = "DefaultValues";

    /** Represent open tag. */
    public static final String OPEN_TAG = "<";

    /** Represent close tag. */
    public static final String CLOSE_TAG = ">";

    /** Represent new line. */
    public static final String NEW_LINE = "\n";

    /** Represent slash symbol. */
    public static final String SLASH = "/";

    /** Represent blank symbol. */
    public static final String INDENTATION_LEVEL_1 = "   ";

    /** Represent blank symbol. */
    public static final String INDENTATION_LEVEL_2 = "      ";

    /** Represent name. */
    public static final String LABEL = "Label";

    /** Represent name. */
    public static final String VALUE = "Value";  
    
    /** Represent Capitalisation  */
    public static final String CAPITALISATION = "Capitalisation";

    /** Represent LTA  */
    public static final String LTA = "LTA"; 
    
    /** Represent AnnualInflation  */
    public static final String ANNUAL_INFLATION = "AnnualInflation";

    /** Represent AnnualAllowanceTaxRate  */
    public static final String ANNUAL_ALLOWANCE_TAX_RATE = "AnnualAllowanceTaxRate"; 
    
    /** Represent AnnualAllowance  */
    public static final String ANNUAL_ALLOWANCE = "AnnualAllowance";

    /** Represent AverageSalaryIncrease  */
    public static final String AVERAGE_SALARY_INCRESE = "AverageSalaryIncrease"; 
    
    Hashtable<String, DefaultInfo> defaultValues = new Hashtable<String, DefaultInfo>();
    HashMap<String, String> defaultLTAValues = new HashMap<String, String>();
    HashMap<String, String> defaultCapValues = new HashMap<String, String>();
    private boolean isDefaultConfigUsed = false;

    //~ Constructors -------------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    public DefaultConfiguration() 
    {
    	loadConfigurations();
    }

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * The method for configuration loading .<br>
     *
     * @return Hashtable objects.
     */
    public Hashtable<String, DefaultInfo> getDefaultValues() {
    	return this.defaultValues;
    }
    
    public void loadConfigurations() {
        // Create object read xml file
        // XmlReader reader = new XmlReader();
        byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + TaxUtil.CONFIG_FILE_NAME);

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
        defaultValues = getDefaultValuesFromXML(defaultValueContentBytes);
        
        // Huy added on phase 2. Get the configration flag
        byte[] configFlagContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + TaxUtil.CONFIG_FLAG_FILE_NAME);
        isDefaultConfigUsed  = getConfigFlagFromXML(configFlagContentBytes);
    }  
    public HashMap<String, String> loadConfigurationsForAaCarryConfig() {
        // Create object read xml file
        // XmlReader reader = new XmlReader();
        byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + ConfigurationUtil.AA_CARRY_CONFIG); //

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
        HashMap<String, String> resultForAaCarryConfig = getDefaultValuesFromXMLForAaCarryConfig(defaultValueContentBytes);
        return resultForAaCarryConfig;
    }    
    public String loadConfigurationsForCarConfig() {
        // Create object read xml file
        // XmlReader reader = new XmlReader();
        byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + ConfigurationUtil.CAR_CONFIG); //

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
        String resultForCarConfig = getDefaultValuesFromXMLForCarConfig(defaultValueContentBytes);
        return resultForCarConfig;
    }  
    public HashMap<String, String> loadConfigurationsForServiceSalary() {
        // Create object read xml file
        // XmlReader reader = new XmlReader();
        byte[] defaultValueContentBytes = OpenCmsFileUtil.getFileData(TaxUtil.XMLFILE_DIR + ConfigurationUtil.SALARY_SERVICE); //

        // content = convertToHastable(reader.readFile(TaxUtil.XMLFILE_DIR
        // + TaxUtil.CONFIG_FILE_NAME));
        HashMap<String, String> resultForServiceSlary = getDefaultValuesFromXMLForServiceSalary(defaultValueContentBytes);
        return resultForServiceSlary;
    }    
    /**
     * The method store value to HashMap.<br>
     *
     * @param  doc array of bytes.
     *
     */
    public HashMap<String, String> getDefaultValuesFromXMLForAaCarryConfig(byte[] doc) {
        HashMap<String, String> content = new HashMap<String, String>();
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            // Following code serve testing from file File file = new
            // File("d:\\default.xml"); ByteArrayInputStream byteStream = new
            // ByteArrayInputStream(TaxUtil.getBytesFromFile(file));
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTagsAA = root.getChildNodes();
            // NodeList nodeList = root.getElementsByTagName(tag);
            String tagObjectName;
            Node subNode = null;
            NodeList child = null;
            NodeList nodeList = null;

            // Loop over sub tags inside defaultvalue tag
            for (int tagIndex = 0; tagIndex < listObjectTagsAA.getLength(); tagIndex++) {
                if (listObjectTagsAA.item(tagIndex).getNodeType() == Node.ELEMENT_NODE) {
                    tagObjectName = listObjectTagsAA.item(tagIndex).getNodeName();
                    if (tagObjectName != null) {
                        nodeList = root.getElementsByTagName(tagObjectName);
                        for (int i = 0; i < nodeList.getLength(); i++) {
                            // Get item at current element
                            subNode = nodeList.item(i);
                            // Check child list of current node
                            if (subNode.hasChildNodes()
                                    && (subNode.getNodeType() == Node.ELEMENT_NODE)) {
                                child = subNode.getChildNodes();
                                content.put(subNode.getNodeName().trim(), child.item(0).getNodeValue().trim());
                            }
                        }
                    }
                    
                }                
            }
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }
        // Return objects
        return content;
    }
    /**
     * The method store value to String.<br>
     *
     * @param  doc array of bytes.
     *
     * @return String objects.
     */
    public String getDefaultValuesFromXMLForCarConfig(byte[] doc) {
        String contentCarConfig="";
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            // Following code serve testing from file File file = new
            // File("d:\\default.xml"); ByteArrayInputStream byteStream = new
            // ByteArrayInputStream(TaxUtil.getBytesFromFile(file));
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTagsAA = root.getChildNodes();
            // NodeList nodeList = root.getElementsByTagName(tag);
            contentCarConfig = listObjectTagsAA.item(0).getNodeValue().trim();
           
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }
        // Return objects
        return contentCarConfig;
    }
    /**
     * The method store value to HashMap.<br>
     *
     * @param  doc array of bytes.
     *
     * @return HashMap objects.
     */
    public HashMap<String, String> getDefaultValuesFromXMLForServiceSalary(byte[] doc) {
        HashMap<String, String> contentServiceSalary = new HashMap<String, String>();
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            // Following code serve testing from file File file = new
            // File("d:\\default.xml"); ByteArrayInputStream byteStream = new
            // ByteArrayInputStream(TaxUtil.getBytesFromFile(file));
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTagsService = root.getChildNodes();
            // NodeList nodeList = root.getElementsByTagName(tag);
            String tagObjectName;
            Node subNode = null;
            NodeList child = null;
            NodeList nodeList = null;

            // Loop over sub tags inside defaultvalue tag
            for (int tagIndex = 0; tagIndex <listObjectTagsService.getLength(); tagIndex++) {
                if (listObjectTagsService.item(tagIndex).getNodeType() == Node.ELEMENT_NODE) {
                    tagObjectName = listObjectTagsService.item(tagIndex).getNodeName();
                    if (tagObjectName != null) {
                        // Get tag by name
                        nodeList = root.getElementsByTagName(tagObjectName);
                        for (int i = 0; i < nodeList.getLength(); i++) {
                            // Get item at current element
                            subNode = nodeList.item(i);
                            
                            // Check child list of current node
                            if (subNode.hasChildNodes()
                                    && (subNode.getNodeType() == Node.ELEMENT_NODE)) {
                                child = subNode.getChildNodes();
                                contentServiceSalary.put(subNode.getNodeName().trim(), child.item(0).getNodeValue().trim());
                            }
                        }
                    }
                    
                }                
            }
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }
        // Return objects
        return contentServiceSalary;
    }
    /**
     * The method store value to HashMap.<br>
     *
     * @param  doc array of bytes.
     *
     * @return HashMap objects.
     */
    public Hashtable<String, DefaultInfo> getDefaultValuesFromXML(byte[] doc) {
        Hashtable<String, DefaultInfo> content = null;
        ByteArrayInputStream byteStream = null;
        try {
        	
        	Calendar cal = Calendar.getInstance();
	    	String currentTaxYear = DateUtil.getTaxYearAsString(cal.getTime());
	    	//this will be used to clarify the current year so LTA and Capitalisation will take the correct value
	    	String currentYearNodeName = "Year_" + currentTaxYear; 
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            // Following code serve testing from file File file = new
            // File("d:\\default.xml"); ByteArrayInputStream byteStream = new
            // ByteArrayInputStream(TaxUtil.getBytesFromFile(file));
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            NodeList listObjectTags = root.getChildNodes();

            // NodeList nodeList = root.getElementsByTagName(tag);
            Node subNode = null;
            NodeList child = null;
            String tagObjectName = null;
            NodeList nodeList = null;
            content = new Hashtable<String, DefaultInfo>();
            DefaultInfo defaultInfo = null;

            // Loop over sub tags inside defaultvalue tag
            for (int tagIndex = 0; tagIndex < listObjectTags.getLength(); tagIndex++) {
                if (listObjectTags.item(tagIndex).getNodeType() == Node.ELEMENT_NODE) {
                    tagObjectName = listObjectTags.item(tagIndex).getNodeName();
                }
                if (tagObjectName != null) {
                    // Get tag by name
                    nodeList = root.getElementsByTagName(tagObjectName);
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        // Get item at current element
                        subNode = nodeList.item(i);

                        // Check child list of current node
                        if (subNode.hasChildNodes()
                                && (subNode.getNodeType() == Node.ELEMENT_NODE)) {
                            // Get list of children
                            child = subNode.getChildNodes();
                            defaultInfo = new DefaultInfo();
                            for (int j = 0; j < child.getLength(); j++) {
                                // Check at current item of sub child list
                                if (child.item(j).hasChildNodes() && (child.item(j).getNodeType() == Node.ELEMENT_NODE)) {
                                	if(child.item(j).getNodeName().contains(currentYearNodeName)&& child.item(j).hasChildNodes()){
                                		defaultInfo.setLabel(subNode.getNodeName());
                                		defaultInfo.setValue(child.item(j).getFirstChild().getNodeValue().trim());
                                	}
                                    if (child.item(j).getNodeName().equals(LABEL) && child.item(j).hasChildNodes()) {
                                        // Set label value
                                        defaultInfo.setLabel(child.item(j).getFirstChild().getNodeValue().trim());
                                    } else if (child.item(j).getNodeName().equals(VALUE) && child.item(j).hasChildNodes()) {
                                        // Set value
                                        defaultInfo.setValue(child.item(j).getFirstChild().getNodeValue().trim());
                                    }
                                    //get all current LTA value from default.xml
                                    if((child.item(j).getNodeName().trim().contains("Year"))
                                    		&& subNode.getNodeName().equalsIgnoreCase(LTA)
                                    		&&child.item(j).hasChildNodes()){
                                    	
                                    	defaultLTAValues.put(child.item(j).getNodeName().trim(), child.item(j).getFirstChild().getNodeValue().trim());
                                    }
                                    //get all current LTA value from default.xml
                                    if((child.item(j).getNodeName().trim().contains("Year"))
                                    		&& subNode.getNodeName().equalsIgnoreCase(CAPITALISATION)
                                    		&&child.item(j).hasChildNodes()){
                                    	defaultCapValues.put(child.item(j).getNodeName().trim(), child.item(j).getFirstChild().getNodeValue().trim());
                                    }
                                }
                            }
                            // Add object type to Hash map
                            content.put(subNode.getNodeName(),
                                defaultInfo);
                        }
                    }
                }
            }
        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting default values from XML:" + e.getMessage());
        }

        // Return objects
        return content;
    }
    /**
     * 
     * @return all child node of the LTA node with year order
     */
    public TreeMap<String, String> getLTAValues(){
    	TreeMap<String, String> sortedMap = new TreeMap<String, String>(defaultLTAValues); 
    	return sortedMap;
    }
    
    /**
     * 
     * @return all child node of the Capitalisation node with year order
     */
    public TreeMap<String, String> getCapValues(){
    	TreeMap<String, String> sortedMap = new TreeMap<String, String>(defaultCapValues); 
    	return sortedMap;
    }
    /**
     * 
     * @param year this must be formated YYYY (ex: 2011) 
     * @return: value of LTA value with this year or null if the year does not exist
     */
    public String getLTAValueWithYear(String pipYear){
    	ConfigurationUtil  util = new ConfigurationUtil();
    	String yearRequired = "Year_"+ pipYear;
    	Iterator it = defaultLTAValues.keySet().iterator();
    	while(it.hasNext()){
    		String key = (String)it.next();
    		if(key.contains(yearRequired)){
    			return defaultLTAValues.get(key);
    		}
    	}
    	return null;
    }
    
    /**
     * 
     * @param year this must be formated YYYY (ex: 2011) 
     * @return: value of LTA value with this year or null if the year does not exist
     */
    public String getCapValueWithYear(String taxYear){
    	ConfigurationUtil  util = new ConfigurationUtil();
    	String yearRequired = "Year_"+ taxYear;
    	Iterator it = defaultCapValues.keySet().iterator();
    	while(it.hasNext()){
    		String key = (String)it.next();
    		if(key.contains(yearRequired)){
    			return defaultCapValues.get(key);
    		}
    	}
    	return null;
    }
    
    /**
     * The method store value to hashtable.<br>
     *
     * @param  doc array of bytes.
     *
     * @return Hashtable objects.
     */
    public boolean getConfigFlagFromXML(byte[] doc) 
    {
        ByteArrayInputStream byteStream = null;
        try {
            // Create object read xml file
            byteStream = new ByteArrayInputStream(doc);

            // Following code serve testing from file File file = new
            // File("d:\\default.xml"); ByteArrayInputStream byteStream = new
            // ByteArrayInputStream(TaxUtil.getBytesFromFile(file));
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parse array of bytes
            Document document = builder.parse(byteStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            Element root = document.getDocumentElement();
            
            String configValue = root.getTextContent();
            if (configValue != null && configValue.equalsIgnoreCase("true"))
            {
            	return true;
            }

        } 
        catch (Exception e) 
        {
            LOG.error("AA TaxModeller - Error while getting config flag from XML:" + e.getMessage());
        }

        // Return objects
        return false;
    }    

    /**
     * The method that builds key/Object to structured xml document.<br>
     *
     * @param  newConfigs Hashtable store pair of value
     *
     * @return a structured document.
     */
    public String formatToXmlContent(Hashtable<String, DefaultInfo> newConfigs) {
        Iterator<Entry<String, DefaultInfo>> iterator = newConfigs.entrySet().iterator();
        StringBuffer buffer = new StringBuffer();
        Entry<String, DefaultInfo> entry = null;

        // Add open tag modeller.
        buffer.append(OPEN_TAG).append(DEFAULT_TAG).append(CLOSE_TAG).append(NEW_LINE);
        String label = null;
        String value = null;
        String key = null;
        while (iterator.hasNext()) {
            entry = iterator.next();
            key = entry.getKey();
            label = entry.getValue().getLabel();
            value = entry.getValue().getValue();

            // Add a begin key name tag
            buffer.append(INDENTATION_LEVEL_1).append(OPEN_TAG).append(key).append(CLOSE_TAG).append(NEW_LINE);

            // Add pair of value inside
            buffer.append(INDENTATION_LEVEL_2).append(OPEN_TAG).append(LABEL).append(CLOSE_TAG).append(label).append(OPEN_TAG)
                  .append(SLASH).append(LABEL).append(CLOSE_TAG).append(NEW_LINE);
            buffer.append(INDENTATION_LEVEL_2).append(OPEN_TAG).append(VALUE).append(CLOSE_TAG).append(value).append(OPEN_TAG)
                  .append(SLASH).append(VALUE).append(CLOSE_TAG).append(NEW_LINE);

            // Add a close key name tag
            buffer.append(INDENTATION_LEVEL_1).append(OPEN_TAG).append(SLASH).append(key).append(CLOSE_TAG).append(NEW_LINE);
        }

        // Add close tag for modeller
        buffer.append(OPEN_TAG).append(SLASH).append(DEFAULT_TAG).append(CLOSE_TAG);

        // Return structured document xml type.
        return buffer.toString();
    }

    /**
     * The method store value to hashtable.<br>
     *
     * @param  xmlBytes array of bytes
     *
     * @return Hashtable an object data type.
     */
    public Hashtable<String, String> convertJSXMLContent(byte[] xmlBytes) {
        Hashtable<String, String> jsContent = new Hashtable<String, String>();
        ByteArrayInputStream arrayStream = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            arrayStream = new ByteArrayInputStream(xmlBytes);

            // Parse array of bytes
            Document document = builder.parse(arrayStream);
            document.getDocumentElement().normalize();

            // From root get specified node
            NodeList nodeList = document.getElementsByTagName(DEFAULT_TAG);
            Node subNode = null;
            NodeList child = null;
            for (int i = 0; i < nodeList.getLength(); i++) {
                subNode = nodeList.item(i);

                // Check child list of current node
                if (subNode.hasChildNodes() && (subNode.getNodeType() == Node.ELEMENT_NODE)) {
                    child = subNode.getChildNodes();
                    for (int j = 0; j < child.getLength(); j++) {
                        if (child.item(j).getNodeType() == Node.ELEMENT_NODE) {
                            // Put node name and value to Hashtable
                            jsContent.put(child.item(j).getNodeName(), child.item(j).getTextContent());
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.info("Error in store hashtable:" + e.getMessage());
        } finally {
            try {
                arrayStream.close();
            } catch (IOException ioe) {
                LOG.info("Error IO Exception:" + ioe.getCause());
            }
        }

        return jsContent;
    }

    /**
     * The method that update new value for collection of objects.<br>
     *
     * @param  jsContent         collection of objects from js request
     * @param  configuredContent collection of objects load from configuration
     *                           file
     *
     * @return list of replace object
     *
     * @throws Exception
     */
    public Hashtable<String, DefaultInfo> replaceFromRequest(Hashtable<String, String> jsContent,
        Hashtable<String, DefaultInfo> configuredContent) throws Exception {
        DefaultInfo newInfo = null;
        try {
            // Get list of object from configuration file
            Iterator<Entry<String, DefaultInfo>> iteratorConfigured = configuredContent.entrySet().iterator();
            Entry<String, DefaultInfo> entry;
            DefaultInfo currentInfo = null;
            String key = null;
            String newValue = null;
            while (iteratorConfigured.hasNext()) {
                // Get current entry
                entry = iteratorConfigured.next();

                // Get key at current entry
                key = entry.getKey();
                currentInfo = entry.getValue();

                // Check keys need to be updated by client request
                if (jsContent.containsKey(key)) {
                    newValue = jsContent.get(key);
                    newInfo = new DefaultInfo();

                    // Set new value by client request
                    newInfo.setValue(newValue);

                    // Set intact value
                    newInfo.setLabel(currentInfo.getLabel());

                    // Do update object DefaultInfo
                    configuredContent.put(key, newInfo);
                }
            }
        } catch (Exception e) {
            LOG.info(ModellerMessage.LOG_MESSAGE + e.getMessage());
        }

        return configuredContent;
    }

    /**
     * The method write string to file.<br>
     *
     * @param  name a file name.
     * @param  xml  structured document.
     *
     * @return isCompleted boolean status value.
     *
     * @throws IOException any exception.
     */
    public boolean writeToConfiguredXmlFile(String name, String xml) {
        boolean isCompleted = false;
        try {
            // Read content from file which is stored to a workspace
            // configured file.
            OpenCmsFileUtil.createTextFile(TaxUtil.XMLFILE_DIR
                + name, xml.getBytes());
            List<String> resources = new ArrayList<String>();
            resources.add(TaxUtil.XMLFILE_DIR + name);

            // Do published a node
            // Leave the file unpublished because publishing damage the current user session that makes
            // security checking failed.
            //OpenCmsFileUtil.publishResources(resources);
            LOG.info("Create text file successfully in " + TaxUtil.XMLFILE_DIR + name);
            isCompleted = true;

            return isCompleted;
        } catch (Exception e) {
            LOG.info("An exception error at " + TaxUtil.XMLFILE_DIR + name);
        }

        return isCompleted;
    }

	public boolean isDefaultConfigUsed()
	{
		return isDefaultConfigUsed;
	}

	public void setDefaultConfigUsed(boolean isDefaultConfigUsed)
	{
		this.isDefaultConfigUsed = isDefaultConfigUsed;
	}

    
}
