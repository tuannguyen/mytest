/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.aataxmodeller.util;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  Admin
 * @version 1.0
 */
public final class ModellerMessage {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** Represent a tag name. */
    public static final String SERVLET_ERROR_DESCRIPTION = "Unknown error while servlet process at ";

    /** Represent a tag name. */
    public static final String INCOMPLETE_LOG = "Could not completely read file ";

    /** Represent a tag name. */
    public static final String FORMAT_EXCEPTION = "format exception occurrence at";

    /** Represent name. */
    static final String LOG_MESSAGE = "An exception error during the process";
}
