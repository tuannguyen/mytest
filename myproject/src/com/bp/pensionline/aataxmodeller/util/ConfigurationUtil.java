/*
 /* Tuan added
 */
		
package com.bp.pensionline.aataxmodeller.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

public class ConfigurationUtil {

	/** Xml file */
	public static final String AA_CARRY_CONFIG = "AaCarryConfig.xml";
	public static final String INIT_TAG = "initTag";
	public static final String OPERATION_TAG = "operationTag";
	public static final String END_TAG = "endTag";
	
	public static final String CAR_CONFIG = "CarConfig.xml";
	public static final String CAR_CONFIG_ELEMENT = "CarConfig";
	
	public static final String SALARY_SERVICE = "SalaryService.xml";
	public static final String LOWER_BOUND = "lowerBound";
	public static final String UPER_BOUND = "uperBound";

	/**
	 * Constructor
	 */
	public ConfigurationUtil() {

	}
	/**
	 * Returns a default value if the object passed is null.
	 * 
	 * @param o
	 *            object
	 * @param dflt
	 *            default object
	 * @return object
	 */
	public Object isNull(final Object o, final Object dflt) {
		if (o == null) {
			return dflt;
		} else {
			return o;
		}
	}
	/**
	 * 
	 * @param root
	 *            this is the root element
	 * @param map
	 *            this contents child element of root
	 * @return String of xml
	 */
	public String buildXML(String root, HashMap<String, String> map) {

		StringBuffer xmlResponseBuffer = new StringBuffer();
		xmlResponseBuffer.append("<").append(root).append(">").append('\n');
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String element = (String) it.next();
			xmlResponseBuffer.append("<").append(element).append(">");
			xmlResponseBuffer.append(map.get(element));
			xmlResponseBuffer.append("</").append(element).append(">").append(
					'\n');
		}
		xmlResponseBuffer.append("</").append(root).append(">");
		return xmlResponseBuffer.toString();
	}

	/**
	 * 
	 * @param root
	 *            this is the root element
	 * @param map
	 *            this contents child element of root
	 * @return String of xml
	 */
	public String buildXML(String root, String value) {
		StringBuffer xmlResponseBuffer = new StringBuffer();
		xmlResponseBuffer.append("<").append(root).append(">").append(value)
				.append("</").append(root).append(">");
		return xmlResponseBuffer.toString();
	}

	public void writeFile(String fileName, String data) {
		try {
			File carConfigXML = new File(fileName);
			FileWriter fstream = new FileWriter(carConfigXML);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(data.toString());
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String readFile(String fileName) {
		StringBuffer xmlResponseBuffer = new StringBuffer();
		try {
			FileInputStream fstream = new FileInputStream(fileName);

			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// append content to string buffer
				xmlResponseBuffer.append(strLine);
			}

			// Close the input stream
			in.close();
		} catch (Exception e) {// Catch exception if any
			e.printStackTrace();
		}
		return xmlResponseBuffer.toString();
	}
}
