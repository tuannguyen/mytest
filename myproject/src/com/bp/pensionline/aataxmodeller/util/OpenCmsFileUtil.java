/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.aataxmodeller.util;

import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.db.CmsPublishList;

import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.types.CmsResourceTypePlain;

import org.opencms.lock.CmsLock;

import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import java.io.ByteArrayInputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  admin
 * @version 1.0
 */
public class OpenCmsFileUtil {

    //~ Static fields/initializers -----------------------------------------------------------------

    /** DOCUMENT ME! */
    public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

    //~ Methods ------------------------------------------------------------------------------------

    /**
     * Use this method with restriction.
     *
     * @param  filename     DOCUMENT ME!
     * @param  contentBytes DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static boolean createTextFile(String filename, byte[] contentBytes) {
        boolean reportCreated = false;
        try {
            CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
            cmsAdminObj.getRequestContext().setSiteRoot("/");
            cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
            if (!cmsAdminObj.existsResource(filename)) {
                cmsAdminObj.createResource(filename, CmsResourceTypePlain.getStaticTypeId(), contentBytes, new ArrayList());
                CmsLock lock = cmsAdminObj.getLock(filename);
                if (!lock.isNullLock()) {
                    // resource is unlocked, so lock it
                    // cmsAdminObj.lockResource(filename);
                    cmsAdminObj.changeLock(filename);
                    cmsAdminObj.unlockResource(filename);
                }

                // cmsAdminObj.writeFile((CmsFile)file);
                // cmsAdminObj.changeLock(filename);
                // cmsAdminObj.unlockResource(filename);
                reportCreated = true;
            } else {
                CmsFile cmsFile = cmsAdminObj.readFile(filename, CmsResourceFilter.ALL);
                if (cmsFile != null) {
                    cmsFile.setContents(contentBytes);
                    CmsLock lock = cmsAdminObj.getLock(filename);
                    if (lock.isNullLock()) {
                        // resource is unlocked, so lock it
                        cmsAdminObj.lockResource(filename);
                    } else {
                        cmsAdminObj.changeLock(filename);
                    }
                    cmsAdminObj.writeFile(cmsFile);
                    cmsAdminObj.unlockResource(filename);
                }
            }
        } catch (CmsException cmse) {
            LOG.error("Error while creating resouces in CMS: " + filename + " : " + cmse.toString());
        }

        return reportCreated;
    }

    /**
     * Publish all resource in a List.
     *
     * @param resources
     */
    public static void publishResources(List<String> resources) {
        try {
            CmsObject cms = SystemAccount.getAdminCmsObject();
            cms.getRequestContext().setSiteRoot("/");
            cms.getRequestContext().setCurrentProject(cms.readProject("Offline"));
            List publishResources = new ArrayList(resources.size());

            // get the offline resource(s) in direct publish mode
            Iterator i = resources.iterator();
            while (i.hasNext()) {
                String resName = (String) i.next();
                LOG.info("Get resources that need to be published: " + resName);
                try {
                    if (cms.existsResource(resName, CmsResourceFilter.ALL)) {
                        CmsResource res = cms.readResource(resName, CmsResourceFilter.ALL);
                        publishResources.add(res);

                        // check if the resource is locked
                        CmsLock lock = cms.getLock(resName);
                        if (!lock.isNullLock()) {
                            // resource is locked, so unlock it
                            cms.changeLock(resName);
                            cms.unlockResource(resName);
                        }
                    }
                } catch (CmsException cmse) {
                    LOG.error("Error in initilize publish resources: " + cmse);
                }
            }

            // create publish list for direct publish
            CmsPublishList publishList = OpenCms.getPublishManager().getPublishList(cms,
                    publishResources,
                    false,
                    true);

            // cms.checkPublishPermissions(publishList); LOG.info("Project type:
            // " + cms.getRequestContext().currentProject().getType());
            PLPublishThread thread = new PLPublishThread(cms, publishList);
            LOG.info("Start publishing thread");

            // start the publish thread
            thread.start();

            // join() with current thread to avoid reading unpulishing pages
            // from recent_changes.jsp
            thread.join();
        } catch (Exception e) {
            LOG.error("Error in publishing files: " + e.toString());
        }
    }

    /**
     * Read a text file from OpenCms.
     *
     * @param  filename
     *
     * @return
     */
    public static String readTextFile(String filename) {
        String text = null;
        if (filename != null) {
            byte[] fileContentBytes = getFileData(filename);
            if (fileContentBytes != null) {
                StringBuffer textBuffer = new StringBuffer();
                ByteArrayInputStream textInputStream = new ByteArrayInputStream(fileContentBytes);
                String NL = System.getProperty("line.separator");
                Scanner scanner = new Scanner(textInputStream, "UTF-8");
                try {
                    while (scanner.hasNextLine()) {
                        textBuffer.append(scanner.nextLine() + NL);
                    }
                } finally {
                    scanner.close();
                }
                text = textBuffer.toString();
            }
        }

        return text;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  filename DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static byte[] getFileData(String filename) {
        try {
            CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
            cmsAdminObj.getRequestContext().setSiteRoot("/");
            cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
            boolean exist = cmsAdminObj.existsResource(filename);

            // LOG.info("File " + filename + " existed: " + exist);
            // check if the file is exist or not
            if (exist) { // if true

                // read file and return a byte array
                CmsFile file = (CmsFile) cmsAdminObj.readFile(filename, CmsResourceFilter.ALL);
                byte[] arr = file.getContents();

                // LOG.info("File size" + arr.length);
                return arr;
            }
        } catch (Exception ex) {
            LOG.error("Error in reading report file content: " + filename + ". Exception is: " + ex);
        }

        return null;
    }
}
