package com.bp.pensionline.aataxmodeller.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class NumberUtil
{
	public static final String DEFAULT_CURRENCY_NUMBER_FORMAT = "#,##0.00";
	public static final String NEAREAST_POUND_NUMBER_FORMAT = "#,##0";
	
	private static final DecimalFormat numberFormat = new DecimalFormat(DEFAULT_CURRENCY_NUMBER_FORMAT);
	private static final DecimalFormat nearestPoundFormat = new DecimalFormat(NEAREAST_POUND_NUMBER_FORMAT);
	
	public static String formatToUkPound (double money)
	{
		return ("�" + numberFormat.format(money));
	}
	
	public static String formatToDecimal (double number)
	{
		return numberFormat.format(number);
	}
	
	public static String formatToNearestPound (double number)
	{
		return nearestPoundFormat.format(number);
	}	
	
	public static String formatToPercent (double number)
	{
		return numberFormat.format(number * 100) + "%";
	}	
	
	/**
	 * Parse a string to get the double number
	 * @param source
	 * @return
	 */
	public static double parseDouble (String source)
	{
		if (source != null)
		{
			try
			{
				return numberFormat.parse(source).doubleValue();
			}
			catch (ParseException pe)
			{
			}
		}
		
		return 0.00;
	}
	
	/**
	 * Parse a string to get the double number
	 * @param source
	 * @return
	 */
	public static int parseInt (String source)
	{
		if (source != null)
		{
			try
			{
				return Integer.parseInt(source);
			}
			catch (NumberFormatException nfe)
			{				
			}
		}
		
		return 0;
	}	
	
	public static void main(String[] args)
	{
		System.out.println("double value: " + new SimpleDateFormat("dd MMMM yyyy").format(new Date()));
		
	}
	
	   /**
	    * Strip all characters from input string which are not numbers or decimal points
	    * e.g. �45,954.45 becomes 45954.45. The result is then converted to a double.
	    * 
	     * @param data
	     * @return
	     */
	    public static double currencyToDecimal(String data)  {
	      if (data == null) {
	         return 0.0;
	      }
	      try{	        
		      int length = data.length();
		      StringBuffer output = new StringBuffer (length);
		
		      char currChar;
		      for (int i=0; i < length; i++) {
		         currChar = data.charAt(i);
		         if ( ((currChar>='0') && (currChar<='9'))) {
		            output.append(currChar);
		         }else if (currChar == '.')  {
		             //the decimal point, keep this
		             output.append(currChar);
		         }else{
		            // we only want a-z, 0-9 so miss it out
		         }
		      }
		      return Double.parseDouble((output.toString()));
		
		  }catch(NumberFormatException nfex){
		      //bad format do nothing
		      return 0.0;
		  }
	   }  	
}
