package com.bp.pensionline.aataxmodeller.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;
import org.opencms.main.CmsLog;

public class DateUtil
{
	public static final Log LOG = CmsLog.getLog(DateUtil.class);
	
	public static final String AA_TAX_MODELLER_DATE_FORMAT = "dd-MMM-yyyy";
	
	/**
	 * Returns the Julian day number that begins at noon of this day, Positive
	 * year signifies A.D., negative year B.C. Remember that the year after 1
	 * B.C. was 1 A.D.
	 * 
	 * ref : Numerical Recipes in C, 2nd ed., Cambridge University Press 1992
	 */
	// Gregorian Calendar adopted Oct. 15, 1582 (2299161)
	public static int JGREG = 15 + 31 * (10 + 12 * 1582);
	public static double HALFSECOND = 0.5;

	public static double toJulian(int[] ymd)
	{
		int year = ymd[0];
		int month = ymd[1]; // jan=1, feb=2,...
		int day = ymd[2];
		int julianYear = year;
		if (year < 0)
			julianYear++;
		int julianMonth = month;
		if (month > 2)
		{
			julianMonth++;
		}
		else
		{
			julianYear--;
			julianMonth += 13;
		}

		double julian = (java.lang.Math.floor(365.25 * julianYear)
				+ java.lang.Math.floor(30.6001 * julianMonth) + day + 1720995.0);
		if (day + 31 * (month + 12 * year) >= JGREG)
		{
			// change over to Gregorian calendar
			int ja = (int) (0.01 * julianYear);
			julian += 2 - ja + (0.25 * ja);
		}
		return java.lang.Math.floor(julian);
	}

	/**
	 * Converts a Julian day to a calendar date ref : Numerical Recipes in C,
	 * 2nd ed., Cambridge University Press 1992
	 */
	public static int[] fromJulian(double injulian)
	{
		int jalpha, ja, jb, jc, jd, je, year, month, day;
		double julian = injulian + HALFSECOND / 86400.0;
		ja = (int) julian;
		if (ja >= JGREG)
		{
			jalpha = (int) (((ja - 1867216) - 0.25) / 36524.25);
			ja = ja + 1 + jalpha - jalpha / 4;
		}

		jb = ja + 1524;
		jc = (int) (6680.0 + ((jb - 2439870) - 122.1) / 365.25);
		jd = 365 * jc + jc / 4;
		je = (int) ((jb - jd) / 30.6001);
		day = jb - jd - (int) (30.6001 * je);
		month = je - 1;
		if (month > 12)
			month = month - 12;
		year = jc - 4715;
		if (month > 2)
			year--;
		if (year <= 0)
			year--;

		return new int[]
		{ year, month, day };
	}
	
	/**
	 * Get the days between 2 dates
	 */
	public static int getDaysBetween (Date from, Date to)
	{
		int daysBetween = 0;
		if (from != null && to != null && to.compareTo(from) >= 0)
		{
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(from);
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(to);	
			
			DateTime start = new DateTime (calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH) + 1, calendar1.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
			DateTime end = new DateTime (calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH) + 1, calendar2.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
			
			Days days = Days.daysBetween(start, end);
			daysBetween = days.getDays() + 1;
		}
		
		return daysBetween;
	}
	
	/**
	 * Get the years between 2 dates and the remain days
	 * @param from
	 * @param to
	 * @return
	 */
	public static int[] getYearsAndDaysBetween(Date from, Date to)
	{
		int[] yearsAndDays = new int[] {-1, -1};
		if (from != null && to != null)
		{	
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(from);
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(to);	
			
			DateTime start = new DateTime (calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH) + 1, calendar1.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
			DateTime end = new DateTime (calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH) + 1, calendar2.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
			
			Years years = Years.yearsBetween(start, end);
			int yearsBetween = years.getYears();

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(from);
			calendar.add(Calendar.YEAR, yearsBetween);
			
			int remainDays = getDaysBetween(calendar.getTime(), to);
			
			yearsAndDays[0] = yearsBetween;
			yearsAndDays[1] = remainDays;
		}
		
		return yearsAndDays;
	}
	
	/**
	 * return the date before or after the milestone date.
	 * @param days
	 * @return
	 */
	public static Date getOffsetDate (Date milestone, int offsetDays)
	{
		Date offsetDate = new Date();
		if (milestone != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(milestone);
			calendar.add(Calendar.DATE, offsetDays > 0 ? (offsetDays - 1) : (offsetDays + 1));
			
			offsetDate = calendar.getTime();
		}
		
		return offsetDate;
	}
	
	/**
	 * Get the date at 31-Dec compared to the input date
	 * Eg, if input date is 25-11-2006, result will be 31-12-2006
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfNextMonth (Date date)
	{
		Date firstDayOfNextMonth = null;
		
		// TODO: Using Calendar class to get the 31-Dec compared to the input date. Remember to check input date != null first
		if(date != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);			
			calendar.add(Calendar.MONTH, 1);
			firstDayOfNextMonth = calendar.getTime();
        }
		
		return firstDayOfNextMonth;
	}
	
	/**
	 * Get the first day of next year
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfNextYear (Date date)
	{
		Date firstDayOfNextYear = null;
		
		// TODO: Using Calendar class to get the 31-Dec compared to the input date. Remember to check input date != null first
		if(date != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);			
			calendar.set(Calendar.MONTH, Calendar.JANUARY);
			calendar.add(Calendar.YEAR, 1);
			firstDayOfNextYear = calendar.getTime();
        }
		
		return firstDayOfNextYear;
	}
	
	/**
	 * Get the end day of next year
	 * @param date
	 * @return
	 */
	public static Date getEndDayOfNextYear (Date date)
	{
		Date firstDayOfNextYear = null;
		
		// TODO: Using Calendar class to get the 31-Dec compared to the input date. Remember to check input date != null first
		if(date != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.DAY_OF_MONTH, 31);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);			
			calendar.set(Calendar.MONTH, Calendar.DECEMBER);
			calendar.add(Calendar.YEAR, 1);
			firstDayOfNextYear = calendar.getTime();
        }
		
		return firstDayOfNextYear;
	}	
	
	
	/**
	 * Get the next day of input date
	 * Eg, if input date is 25-11-2006, result will be 26-11-2007
	 * @param date
	 * @return
	 */
	public static Date getNextDay (Date date)
	{
		Date nextDay = null;
		//TODO:  Using Calendar class to get the next day of the input date. Remember to check input date != null first
		if(date != null)
		{
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);                    
                    calendar.add(Calendar.DATE, 1);
                    nextDay = calendar.getTime();
        }
		return nextDay;
	}
	
	
	/**
    * Get the previous day of input date
    * Eg: if input date is 28-2-2007, result will be 27-2-2007
    * @param date
    * @return
    */
    public static Date getPreviousDay(Date date)
	{
        Date previousDay = null;
        if(date != null)
		{
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.add(Calendar.DATE, -1);
                previousDay = calendar.getTime();
        }
        return previousDay;
    }
    
	/**
     * Get the new year day of current year
     * Eg: if input date is 28-2-2007, result will be 27-2-2007
     * @param date
     * @return
     */
     public static Date getFirstDayOfThisYear(Date date)
 	{
         Date newYearDay = null;
         if(date != null)
 		{
                 Calendar calendar = Calendar.getInstance();
                 calendar.setTime(date);
                 calendar.set(Calendar.MONTH, Calendar.JANUARY);
                 calendar.set(Calendar.DAY_OF_MONTH, 1);
                 calendar.set(Calendar.HOUR, 0);
                 calendar.set(Calendar.MINUTE, 0);
                 calendar.set(Calendar.SECOND, 0);
                 calendar.set(Calendar.MILLISECOND, 0);
                 newYearDay = calendar.getTime();
         }
         return newYearDay;
     } 
     
 	/**
      * Get the new year day of current year
      * Eg: if input date is 28-2-2007, result will be 27-2-2007
      * @param date
      * @return
      */
     public static Date getEndDayOfThisYear(Date date)
     {
          Date newYearDay = null;
          if(date != null)
          {
                  Calendar calendar = Calendar.getInstance();
                  calendar.setTime(date);
                  calendar.set(Calendar.MONTH, Calendar.DECEMBER);
                  calendar.set(Calendar.DAY_OF_MONTH, 31);
                  calendar.set(Calendar.HOUR, 0);
                  calendar.set(Calendar.MINUTE, 0);
                  calendar.set(Calendar.SECOND, 0);
                  calendar.set(Calendar.MILLISECOND, 0);
                  newYearDay = calendar.getTime();
          }
          return newYearDay;
     }      
    
    /**
     * Check if date is before today. True if input date is null
     * @param date
     * @return
     */
    public static boolean isDateBeforeToday (Date date)
    {
    	if (date != null)
    	{
    		Date today = new Date();
    		Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            today = calendar.getTime();
            
            if (date.before(today))
            {
            	return true;
            }
            else
            {
            	return false;
            }
    	}
    	
    	return true;
    }
    
    public static int getMonthsBetween2Date(Date from, Date to)
    {
    	int month = 0;
    	
    	if (from != null && to != null)
		{	
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(from);
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(to);	
			
			DateTime start = new DateTime (calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH) + 1, calendar1.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
			DateTime end = new DateTime (calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH) + 1, calendar2.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);

			Months months = Months.monthsBetween(start, end);
			month = months.getMonths();
		}
    	
    	return month;
    }
    
    /**
     * Format a date with default template
     * @param date
     * @return
     */
    public static String formatDate (Date date)
    {
    	if (date != null)
    	{
    		return new SimpleDateFormat(AA_TAX_MODELLER_DATE_FORMAT).format(date);
    	}
    	
    	return "";
    }
    
    /**
     * parse a date from string source with default template
     * @param date
     * @return
     */
    public static Date parseDate (String dateStr)
    {
    	if (dateStr != null)
    	{
    		try
			{
				return new SimpleDateFormat(AA_TAX_MODELLER_DATE_FORMAT).parse(dateStr);
			}
			catch (ParseException pe)
			{
				LOG.error("Error in parsing date: " + pe.toString());
			}
    	}
    	
    	return null;
    }   
    
	/**
	 * @param date
	 * @return
	 */
	public static String getModelledYear(Date date)
	{
		String taxYear = null;
		if (date != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			
			return "" + year;
		}
		return taxYear;
	}    
    
	/**
	 * @param date
	 * @return
	 */
	public static String getModelledTaxYear(Date date)
	{
		String taxYear = null;
		if (date != null)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			
			return "" + year +"/" + (year+1);
		}
		return taxYear;
	}
	
	public static String getTaxYearAsString(Date date)
	{
		if (date != null)
		{
			Calendar calendarAt1April = Calendar.getInstance();
			calendarAt1April.set(Calendar.MONTH, Calendar.APRIL);
			calendarAt1April.set(Calendar.DAY_OF_MONTH, 6);
			calendarAt1April.set(Calendar.HOUR, 0);
			calendarAt1April.set(Calendar.MINUTE, 0);
			calendarAt1April.set(Calendar.SECOND, 0);
			calendarAt1April.set(Calendar.MILLISECOND, 0);
			
			int year = calendarAt1April.get(Calendar.YEAR);
			if (date.before(calendarAt1April.getTime()))
			{
				return (year - 1) + "_" + (year);
			}
			else
			{
				return (year) + "_" + (year + 1);
			}
		}
		
		return "";
			
	}
	
	public static void main(String args[]) throws Exception
	{
		
		Calendar calendarAt6thApr2012 = Calendar.getInstance();
		calendarAt6thApr2012.set(Calendar.YEAR, 2012);
		calendarAt6thApr2012.set(Calendar.DAY_OF_MONTH, 6);
		calendarAt6thApr2012.set(Calendar.MONTH, Calendar.APRIL);
		calendarAt6thApr2012.set(Calendar.HOUR_OF_DAY, 0);
		calendarAt6thApr2012.set(Calendar.MINUTE, 0);
		calendarAt6thApr2012.set(Calendar.SECOND, 0);
		
		System.out.println(getTaxYearAsString(calendarAt6thApr2012.getTime()));
		// FIRST TEST reference point
//		LOG.info("Julian date for May 23, 1968 : "
//				+ toJulian(new int[]
//				{ 1968, 5, 23 }));
//		// output : 2440000
//		int results[] = fromJulian(toJulian(new int[]
//		{ 1968, 5, 23 }));
//		LOG.info("... back to calendar : " + results[0] + " "
//				+ results[1] + " " + results[2]);
//
//		// SECOND TEST today
//		Calendar today = Calendar.getInstance();
//		double todayJulian = toJulian(new int[]
//		{ today.get(Calendar.YEAR), today.get(Calendar.MONTH) + 1,
//				today.get(Calendar.DATE) });
//		LOG.info("Julian date for today : " + todayJulian);
//		results = fromJulian(todayJulian);
//		LOG.info("... back to calendar : " + results[0] + " "
//				+ results[1] + " " + results[2]);
//
//		// THIRD TEST
//		double from = toJulian(new int[] { 2008, 1, 1 });
//		double to = toJulian(new int[] { 2008, 3, 1 });
//		LOG.info("Between 2005-01-01 and 2005-01-31 : "
//				+ (to - from) + " days");

		/*
		 * expected output : Julian date for May 23, 1968 : 2440000.0 ... back
		 * to calendar 1968 5 23 Julian date for today : 2453487.0 ... back to
		 * calendar 2005 4 26 Between 2005-01-01 and 2005-01-31 : 30.0 days
		 */
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d-MMM-yyyy");
        Date from = simpleDateFormat.parse("29-Feb-1956");
        System.out.println("From: " + from.toString());
        Date to = simpleDateFormat.parse("28-Feb-2011");
        
        System.out.println("to: " + to.toString());
////        LOG.info("Days between: " + getDaysBetween(from, to));
////        LOG.info("Years between: " + getYearsAndDaysBetween(from, to)[0] + " years and " + getYearsAndDaysBetween(from, to)[1] + " days");
////        
//        System.out.println("Offset date: " + getDaysBetween(from, to));
//        
//        System.out.println("from: " + getOffsetDate(from, 1991));
        
       //String test = simpleDateFormat.format(new Date());
       System.out.println("test: " + getMonthsBetween2Date(from, to));
	}	
}
