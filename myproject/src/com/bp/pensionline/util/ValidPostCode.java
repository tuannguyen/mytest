package com.bp.pensionline.util;

import java.util.regex.*;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
/**
 * @author SonNT
 * @version 1.0
 * @since 2007/5/10
 */
public class ValidPostCode
{   
	/**
	 * Validate postcode
	 * @param postcode
	 * @return
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	public static boolean validPostcode(String postcode) 
	{	 
		boolean matchFound = false;
		 
		try{
			/*
			 * Compile regular expression
			 * pattern string was taken from - http://www.govtalk.gov.uk/gdsc/schemaHtml/bs7666-v2-0-xsd-PostCodeType.htm
			 * followed this link - http://regexlib.com/REDetails.aspx?regexp_id=695
			 */	
			
			Pattern pattern = Pattern.compile("(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})");

			//Determine if pattern exists in input		
			Matcher matcher = pattern.matcher(postcode);
			matchFound = matcher.matches();    // true			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			
			LOG.error(e.getMessage() + e.getCause());
		}		
		return matchFound;
	}	
}