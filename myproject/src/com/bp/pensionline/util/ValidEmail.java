package com.bp.pensionline.util;

import java.util.regex.*;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

/**
 * @author SonNT
 * @version 1.0
 * @since 2007/5/10
 */
public class ValidEmail 
{   
	/**
	 * Validate email address
	 * @param emailaddress
	 * @return
	 */
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	public static boolean validEmail(String emailaddress) 
	{
		boolean matchFound = false;
			 
		try{
			/*
			 * Compile regular expression
			 * pattern string was taken from - http://www.dreamincode.net/code/snippet1013.htm
			 */	

			Pattern pattern = Pattern.compile("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,3})$");

			//Determine if pattern exists in input		
			Matcher matcher = pattern.matcher(emailaddress);
			matchFound = matcher.matches();    // true			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error(e.getMessage() + e.getCause());
		}
		
		return matchFound;
	}
	
}