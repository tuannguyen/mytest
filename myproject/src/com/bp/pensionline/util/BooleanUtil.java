/*
 * (ConfigurationXML) copyright BP Pensions 2003
 */
package com.bp.pensionline.util;

// JDK Imports

// J2EE Imports

// BP Imports

/**
 *  A general utility class for converting values to true & false.  Because the world is
 * full of many truths & falsehoods (as well as falsehoods masquirading as truths), we provide
 * a number of standard conversion (0==false, 1==true or even 0==true, 1==false).
 *  Created by: mcobby, last updated by $Author: dom_the_bom $
 *
 *  @author dom the bomb 
 *  @author $Author: dom_the_bom $
 *  @version $Revision: 1.1 $
 * 
 *  $Id: BooleanUtil.java,v 1.1 2007/02/11 00:38:06 dom_the_bom Exp $
 */
public class BooleanUtil {

	/**
	 * 
	 */
	public BooleanUtil() {
		super();
	}

  public static boolean toBoolean(String str) {
	      // sanity check
	      if (str == null) {
	         return false;
	      }
	
	      //  get rid of fluff
	      str = str.trim();
	      if (str.equalsIgnoreCase("y") || 
	          str.equalsIgnoreCase("yes") || 
	          str.equalsIgnoreCase("true") || 
	          str.equalsIgnoreCase("1")) {
	             // any of the above are recognised as true
	             return true;
	      }
	      
	      return false;  // default to false
	   }

	   public static boolean toBoolean(String str, String myTrue, String myFalse) {
	      // sanity check
	      if ((str == null) || (myTrue == null) || (myFalse == null)){
	         return false;
	      }
	
	      //  get rid of fluff
	      str = str.trim();
	      if (str.equalsIgnoreCase(myTrue)) {
	         // are you the truth?
	         return true;
	      }
	            
	      return false;  // default to false
	   }
       
       
      public static boolean toBoolean(int val, int trueVal, int falseVal) {
          if (val == trueVal) {
              return true;
          } else if (val == falseVal) {
              return false;
          }
                              
          return false; // failsafe
      }
      
      
      /** Converts an int to a boolean - one is considered true; everything else is 
       * considered false;
       * @param value
       * @return boolean representation of the value
       */
      public static boolean intToBoolean(int value) {
          // one is true
          // everything else is false
          if (value == 1) {
              return true; 
          } else {
              return false;
          }
      }

}
