package com.bp.pensionline.util;
import org.opencms.file.CmsUser;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.factory.DataAccess;

/**
 * @author SonNT
 * @version 1.0
 * @since 2007/05/25
 * This class is used to update MemberDao
 */
public class ClearMemberCache{
	
	public static void clearCache(CmsUser currentUser){
		
		//Get current Member Dao
		MemberDao memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
		
		//Get bGroup and refNo
		String bGroup = memberDao.get(Environment.MEMBER_BGROUP);
		String refNo = memberDao.get(Environment.MEMBER_REFNO);
		
		//Call factory to get new instance of MemberDao
		memberDao = DataAccess.getDaoFactory().getMemberDao(bGroup, refNo, null);
		
		//Delete current MemberDao in user's info
		currentUser.deleteAdditionalInfo(Environment.MEMBER_KEY);
		//Add new one
		currentUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
	}
	
}


