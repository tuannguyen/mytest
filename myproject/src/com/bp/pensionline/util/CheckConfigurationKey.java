package com.bp.pensionline.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.opencms.main.CmsSystemInfo;
import org.opencms.main.OpenCms;
import org.opencms.util.CmsPropertyUtils;

public class CheckConfigurationKey {
	private static String configPath = OpenCms.getSystemInfo().getAbsoluteRfsPathRelativeToWebInf(CmsSystemInfo.FOLDER_CONFIG + CmsSystemInfo.FILE_PROPERTIES);
	
	public static final Log LOG = CmsLog.getLog(CheckConfigurationKey.class);
	
	public static ExtendedProperties getConfigurations () throws IOException
	{
		//LOG.debug("configPath: " + configPath);
		return CmsPropertyUtils.loadProperties(configPath);
	}
	
	public static boolean checkKeyExit(String key)
	{
		try
		{
			return getConfigurations().containsKey(key);
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in checkKeyExit from configuration: " + key);
			return false;
		}

	}

	public static boolean getBooleanValue(String key)
	{
		try
		{
			if (checkKeyExit(key))
			{				
				return getConfigurations().getBoolean(key);
			}
			else
			{
				return false;
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in getBooleanValue from configuration: " + key);
			return false;
		}

	}

	public static String getStringValue(String key)
	{
		try 
		{
			//LOG.info("Value for "+key+": "+getConfigurations().getString(key));
			return getConfigurations().getString(key);
		} 
		catch (Exception e) 
		{
			LOG.error("Error in getStringValue from configuration: " + key + " : " + e);
			return null;
		}
		
	}
	
	public static String getPublishProperty(String key) {
		try {
			FileInputStream file = new FileInputStream(configPath);
			Properties pros = new Properties();
			pros.load(file);
			LOG.info("Value for "+key+": "+pros.getProperty(key));
			return pros.getProperty(key);
		} catch (Exception e) {
			LOG.error("Exception occurred: "+e.toString());
			return null;
		}
	}
}
