/*
 * (ConfigurationXML) copyright BP Pensions 2003
 */
package com.bp.pensionline.util;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.XmlReader;


// JDK Imports

// J2EE Imports

// BP Imports

/**
 * Provides some commone (and some not so common) text utility methods.
 *  Created by: mcobby, last updated by $Author: cvsuser $
 *
 *  @author dom the bomb....!
 *  @author $Author: cvsuser $
 *  @version $Revision: 1.1 $
 * 
 *  $Id: TextUtil.java,v 1.1 2007/06/25 08:57:52 cvsuser Exp $
 */
public class TextUtil {
   static Logger log = Logger.getLogger(TextUtil.class.getName());

   /** An empty string accessible by any class, saves on redundant object creation */
   public static final String NULLSTR = ""; 
   /** Similar to NULLSTR except this string is one char (space) long */
   public static final String SPACESTR = " ";

   /**
    * 
    */
   public TextUtil() {
      super();
   }
    
    /**
     * curtail a string over a given length and adds "..." on the end. for instance, a string "ABCDEFGHIJ"
     * which is being curtailed to a length of 8 would be ABCDE... (len includes the ...) while the 
     * string "ABC" being curtailed to a length of 10 would just return "ABC". If string is of length <= 3
     * then it just returns the original string as it doesnt make sense to curtail a string of less than 3 chars
     * given that "..." is itself 3 chars
     * 
     * @param txt
     * @param len
     * @return
     */
    public static String curtailString(String txt, int len) {
        String result = null;
        
        // check data hgas actually been passed in
        if (txt==null) {
            return "";
        }
        
        // check for short restrictions, also stops negative values
        if (txt.length() <= 3) {
            return txt;
        }
        
        // now chop the string        
        if (txt.length() > len) {
           // -4 because "..." is 3 chars long and another one because string is indexed 0..n-1
           StringBuffer sb = new StringBuffer(txt.substring(0, len-4));          
           sb.append("...");
           result = sb.toString();
        } else {
           // txt is shorter than curtail length, return original string
           result = txt; 
        }
        
        return result;        
    }
  
    /**
     * Chops a string down to it's max length
    * @param txt
    * @param len
    * @return
    */
   public static String chopString(String txt, int len) {
       String result = null;
       
       // check data hgas actually been passed in
       if (txt==null) {
           return "";
       }
             
       // now chop the string        
       if (txt.length() > len) {
          // -4 because "..." is 3 chars long and another one because string is indexed 0..n-1
          StringBuffer sb = new StringBuffer(txt.substring(0, len));                    
          result = sb.toString();
       } else {
          // txt is shorter than curtail length, return original string
          result = txt; 
       }
       
       return result;        
   }    
    


   /**
    *  Given a string, this method replaces all occurrences of
    *  '<' with '&lt;', all occurrences of '>' with
    *  '&gt;', and (to handle cases that occur inside attribute
    *  values), all occurrences of double quotes with
    *  '&quot;' and all occurrences of '&' with '&amp;'.
    *  Without such filtering, an arbitrary string
    *  could not safely be inserted in a Web page.
    */
   public static String htmlTagEscape(String data)
   {
       // stop any NPE
       if (data == null) {
           return "";
       }
       
       char c;
       StringBuffer result = new StringBuffer(data.length());

       for(int i=0; i < data.length(); i++)
       {
          c = data.charAt(i);
          if (c == '<') {
             result.append("&lt;");
          } else if (c == '>') {
             result.append("&gt;");
          } else if (c == '"') {
             result.append("&quot;");
          } else if (c == '&') {
             result.append("&amp;");
          } else {
             result.append(c);
          }
       }

       return(result.toString());
   }


   // Allow tags??  <B> <I> <P> <A> <LI> <OL> <UL> <EM> <BR> <TT> <STRONG> <BLOCKQUOTE> <DIV>



   public static StringBuffer stripTags(StringBuffer data)
   {
      return stripTags (data.toString());
   }


   /**
    * Strip all characters from input string which are not in the range A-Z (case insensitive)
    * and 0-9.  The result is then dropped to all lower case.
    * 
     * @param data
     * @return
     */
    public static StringBuffer stripNonAZ09toLower(String data)  {
      if (data == null) {
         return new StringBuffer(1);
      }

      int length = data.length();
      StringBuffer output = new StringBuffer (length);

      char currChar;
      for (int i=0; i < length; i++) {
         currChar = data.charAt(i);
         if (((currChar>='a') && (currChar<='z')) || ((currChar>='0') && (currChar<='9'))) {
            output.append(currChar);
         } else if((currChar>='A') && (currChar<='Z')) {
            output.append(Character.toLowerCase(currChar));
         }else{
            // we only want a-z, 0-9 so miss it out
         }
      }

      return output;
   }
    
    /**
     * Strip all characters from input string which are not in the range A-Z (case insensitive)
     * and 0-9.  The result is then dreturned in the same case it was presented in.
     * 
      * @param data
      * @return
      */
     public static StringBuffer stripNonAZ09(String data)  {
       if (data == null) {
          return new StringBuffer(1);
       }

       int length = data.length();
       StringBuffer output = new StringBuffer (length);

       char currChar;
       for (int i=0; i < length; i++) {
          currChar = data.charAt(i);
          if (((currChar>='a') && (currChar<='z')) || ((currChar>='0') && (currChar<='9'))) {
             output.append(currChar);
          } else if((currChar>='A') && (currChar<='Z')) {
             output.append(currChar);
          }else{
             // we only want a-z, 0-9 so miss it out
          }
       }

       return output;
    }

    
    /**
     * strips all non-numeric characters from the string
     * @param data
     * @return
     */
    public static StringBuffer stripNonNumericChars(String data)  {
        if (data == null) {
            return new StringBuffer(1);
        }

        int length = data.length();
        StringBuffer output = new StringBuffer (length);

        char currChar;
        for (int i=0; i < length; i++) {
            currChar = data.charAt(i);
            if ((currChar>='0') && (currChar<='9')) {
                output.append(currChar);          
            }else{
                // we only want 0-9 so miss it out
            }
        }
        return output;
    }
    
    
    
   /**
    * Take all HTML tags out of a string
     * @param data
     * @return
     */
    public static StringBuffer stripTags(String data) {
      if (data == null) {
         return null;
      }

      // Let's remove the HTML tags
      boolean tagFound=false;
      StringBuffer output= new StringBuffer (data.length());
      for (int i=0; i < data.length(); i++){
         if (tagFound) {
            if (data.charAt(i)=='>') {
               tagFound = false;
            }
         } else {
            if (data.charAt(i)=='<') {
               tagFound=true;
               output.append(' ');
            } else {
               output.append(data.charAt(i));
            }
        }

      } // end for

      return output;

   } // end stripTags



   /**
    * Strip all white space characters out of the string. originally written for the 
    * HTTP unit tests to compare crumbtrails. 
    * @param data
    * @return
    */
   public static StringBuffer stripWhitespace(String myString) {
       StringBuffer buf = new StringBuffer(myString.length());
       for (int i = 0; i < myString.length(); i++)
           if (myString.charAt(i) > ' ') // Greater than Space
               buf.append(myString.charAt(i));

       return buf;
   }



    /**
     * Make sure a string is not null.
     *
     * @param   theString Any string, possibly null
     * @param   theMessage
     */
    public static void assertNotBlank(String theString, String theMessage) {
        if (theString == null) {
            throw new IllegalArgumentException("Null argument not allowed: "
                + theMessage);
        }
        if (theString.trim().equals("")) {
            throw new IllegalArgumentException("Blank argument not allowed: "
                + theMessage);
        }
    } /* assertNotBlank(String, String) */


    /**
     * Make sure a string is not null.
     *
     * @param   theString Any string, possibly null
     * @return  An empty string if the original was null, else the original
     */
    public static String notNull(String theString) {
        if (theString == null) {
                return new String("");
        }
        return theString;
    } /* notNull(String) */


    public static void assertBoolean(String theString, String theMessage) {
        assertNotBlank(theString, theMessage);
        if (!(theString.equalsIgnoreCase("yes")
            || theString.equalsIgnoreCase("true")
            || theString.equalsIgnoreCase("no")
            || theString.equalsIgnoreCase("false")
            || theString.equalsIgnoreCase("y")
            || theString.equalsIgnoreCase("n"))) {
                throw new IllegalArgumentException(theMessage);
        }
    }

    /**
     * Replaces occurances of a string in a string with a string.
     *
     * @param tag The searched for string.
     * @param replacement The string to replace each occurance with.
     * @param in_str The text to be scanned.
     * @param all Whether to replace all occurances or, if false stop after the first replcaement.
     *
     * @return The number of instances replaced.
     */
    public static String substitute(String tag, String replacement, String in_str, boolean all) {
      // Fixed pk/mc 
      if (in_str == null)
         return "";
      
      int num_replaced = 0;
      int tagIndex = in_str.indexOf(tag);
      while (tagIndex != -1) {
            num_replaced++;
       in_str = in_str.substring(0, tagIndex) + replacement + in_str.substring(tagIndex + tag.length());
       if (!all) break;
       tagIndex = in_str.indexOf(tag);
      }
        return in_str;
    }
    /**
     * Replaces occurances of a string in a string with a string.
     *
     * @param currency The parameter contains  sign
     * We will convert it into &#163; to handle XML parser
     */
    public static String substituteCurrency(String currency){
    	return substitute("�",  "&#163;", currency, true);
      	
    }
    
    /**
     * Upperises the first letter of each word in a string. WARNING - NOT IMPLEMENTED
     *
     * @param instr The string to be transmogrified.
     * @return the new string.
     */
    public static String upperFirstLetterOfEachWord(String instr)
    {
        //TODO: implement this.
        return instr;
    }

    /**
     * Upperises the first letter and lower cases the rest of the word. WARNING - ONLY WORKS FOR ONE WORD
     *
     * @param instr The string to be transmogrified.
     * @return the new string.
     */
    public static String upperFirstLetter(String instr)   {
        return instr.substring(0,1).toUpperCase() + instr.substring(1).toLowerCase();
    }
    
    /**
     * @param rawSalary
     * @return
     */
    public static String getSalary(String rawSalary){
    	

    	
    	if(rawSalary == null){
    		rawSalary = "0";
    	}
    	
    	Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
    	String salRange = "1-10k";
    	rawSalary = rawSalary.replaceAll("&#163;", "");
    	rawSalary = rawSalary.replaceAll(",", "");
    	float salary = 0;
    	// catch exception of parsing: Huy modified
    	try
    	{
    		salary = Float.parseFloat(rawSalary);
    	}
    	catch (NumberFormatException nfe)
    	{
    		LOG.error("Error in parsing rawSalary: " + rawSalary);
    	}
    	
		
		XmlReader reader = new XmlReader();
		
		try{
			
			byte[] b = reader.readFile(Environment.XMLFILE_DIR + "SalaryMapper.xml");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(b);
			document = builder.parse(is);

			NodeList salaryList = document.getElementsByTagName("Salary").item(0).getChildNodes();
			
			for(int i = 0; i < salaryList.getLength(); i++){
				
				Node salNode = salaryList.item(i);
				if(salNode.getNodeType() == Node.ELEMENT_NODE){
					
					//System.out.println(i + "<" + salNode.getNodeName() + ">" + salNode.getTextContent());
					if(salNode.getNodeName().compareTo("other") == 0){
						salRange =  salNode.getTextContent();
					}else{
						long sal = Long.parseLong(salNode.getNodeName().replaceAll("u", ""));
						if(salary <= sal){

							salRange =  salNode.getTextContent();
							i = salaryList.getLength();
						}
					}
				}
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.TextUtil.getSalary() error: ",e);
		}	
		
		
		//System.out.println("Salary Range return:" + salRange);
		
    	return salRange;
    }

    
    /**
     * @param dob
     * @return
     */
    public static String getAge(String dob){
    	    	    	
    	Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
    	
    	String ageRange = "1-20";    	
    	java.util.Date DoB = null;
    	
    	if(dob != null){
    		DoB=DateUtil.getNormalDate(dob);
    	}    	
		
		int age = 0;
		try {
			// return the corrent age - 17 years 10 days will be 18 years of age
			if(DoB != null){
				age = DateUtil.getAge(DoB) + 1;
			}
			else{
				age = 0;
			}
				
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			LOG.error("com.bp.pensionline.util.TextUtil.getAge() error: "+e1);
		}

		
		XmlReader reader = new XmlReader();
		
		try{
			
			byte[] b = reader.readFile(Environment.XMLFILE_DIR + "AgeMapper.xml");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(b);
			document = builder.parse(is);

			NodeList salaryList = document.getElementsByTagName("Age").item(0).getChildNodes();
			
			for(int i = 0; i < salaryList.getLength(); i++){
				
				Node salNode = salaryList.item(i);
				if(salNode.getNodeType() == Node.ELEMENT_NODE){
					
					//System.out.println(i + "<" + salNode.getNodeName() + ">" + salNode.getTextContent());
					
					if(salNode.getNodeName().compareTo("other") == 0){
						ageRange =  salNode.getTextContent();
					}
					else{
						int sal = Integer.parseInt(salNode.getNodeName().replaceAll("u", ""));
						if(age <= sal){

							ageRange =  salNode.getTextContent();
							i = salaryList.getLength();
						}
					}
				}
				
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.TextUtil.getAge() error: ",e);
		}				
		
    	return ageRange;
    }

    
    /**
     * @param company
     * @return
     */
    public static String getCompany(String company){
    	

    	if(company == null){
    		company = "BP";
    	}
    	
    	Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
		
		XmlReader reader = new XmlReader();
		
		try{
			
			byte[] b = reader.readFile(Environment.XMLFILE_DIR + "CompanyMapper.xml");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(b);
			document = builder.parse(is);

			NodeList salaryList = document.getElementsByTagName("Company").item(0).getChildNodes();
			
			for(int i = 0; i < salaryList.getLength(); i++){
				
				Node salNode = salaryList.item(i);

				if(salNode.getNodeType() == Node.ELEMENT_NODE){					
					if(salNode.getTextContent().indexOf(company) > -1){
						
						company =  salNode.getTextContent();
						i = salaryList.getLength();
					}
				}
				
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.TextUtil.getCompany() error: ",e);
		}				

    	return company;
    }
 
 	/**
 	 * @param path
 	 * @return
 	 */
 	public static long getPagesize(String path){
 		
	 	if(path == null){
	 		return 0;
	 	}
	 	
	 	long pagesize = 0;
	 	Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	 	
			try{
				
				if(path.endsWith("/")){
					path += "index.html";
				}				

				//get Admin permission to read file from VFS folder
				CmsObject cmsObj = SystemAccount.getAdminCmsObject();				
				cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());				
				cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));				
				boolean exist = cmsObj.existsResource(path);
				
				//check if the file is exist or not
				if(exist){//if true
					//read file and return a byte array
					CmsFile file = (CmsFile)cmsObj.readFile(path);		
					pagesize = file.getLength();
				}				
				
			}
			catch (Exception e) {
				// TODO: handle exception
				LOG.error("com.bp.pensionline.util.TextUtil.getPagesize() error: ",e);
			}				

	 	return pagesize;
 	}
    
 	public static Mapper sessionParameters = new Mapper();

}
