package com.bp.pensionline.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsContextInfo;
import org.opencms.main.CmsLog;
import org.opencms.main.CmsSessionInfo;
import org.opencms.main.CmsSessionManager;
import org.opencms.main.OpenCms;
import org.opencms.security.CmsOrgUnitManager;
import org.opencms.util.CmsUUID;


/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 08-05-2007
 * 
 */

public class SystemAccount
{
	public static final Log LOG = CmsLog.getLog(SystemAccount.class);
	
	private static CmsObject adminObj = null;
	private static CmsObject publishingAdminObj = null;
	public static final String webUnitName = CheckConfigurationKey.getStringValue("webuser.unitname") + "/";
	public SystemAccount() {
		
	}
	
	/**
	 * @return Admin Object used in Publishing Tool bar
	 */
	public static synchronized CmsObject getPublishingAdminCmsObject() {
		try {		
			if(publishingAdminObj == null) {
				String adminUsername = CheckConfigurationKey.getStringValue("publihsingAdminName");
				String adminPassword = CheckConfigurationKey.getStringValue("publihsingAdminPassword");				
				
				CmsObject newAdminObj = OpenCms.initCmsObject(OpenCms.getDefaultUsers().getUserGuest());	
				String adminLoggedInName = newAdminObj.loginUser(adminUsername, adminPassword, CmsContextInfo.LOCALHOST);
				//LOG.info("adminLoggedInName: " + adminLoggedInName);
				if (adminLoggedInName != null && adminLoggedInName.equals(adminUsername)) {
					publishingAdminObj = newAdminObj;					
				}
			}
			publishingAdminObj.getRequestContext().setCurrentProject(publishingAdminObj.readProject("Offline"));
			publishingAdminObj.getRequestContext().setSiteRoot("/");
			LOG.info("Site root: "+publishingAdminObj.getRequestContext().getSiteRoot());
			return publishingAdminObj;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Error in getPublishingAdminCmsObject: " + e);
			e.printStackTrace();
			return null;
		}
	}
	
	public static synchronized CmsObject getAdmCmsObject() {
		try	{
			if(adminObj == null) {
				String adminUsername = 
					CheckConfigurationKey.getStringValue("adminName");
				String adminPassword = 
					CheckConfigurationKey.getStringValue("adminPassword");				
				
				CmsObject newAdminObj = 
					OpenCms.initCmsObject(OpenCms.getDefaultUsers()
							.getUserGuest());	
				String adminLoggedInName = 
					newAdminObj.loginUser(adminUsername, 
							adminPassword, CmsContextInfo.LOCALHOST);
				
				if (adminLoggedInName != null 
						&& adminLoggedInName.equals(adminUsername))	{
					adminObj = newAdminObj;					
				}
			}
			
			adminObj.getRequestContext().setCurrentProject(adminObj.readProject("Offline"));
			adminObj.getRequestContext().setSiteRoot("/");
		}
		catch (Exception e)	{
			LOG.error("System cannot initialize CmsObject, error detail: " + e.getMessage());
		}
		return adminObj;
	}
	
	/*
	 * @author return an new CmsObject has role "Administrator"
	 */
	public static synchronized CmsObject getAdminCmsObject() {
		try
		{		
			if(adminObj == null)
			{
				String adminUsername = CheckConfigurationKey.getStringValue("adminName");
				String adminPassword = CheckConfigurationKey.getStringValue("adminPassword");				
				
				CmsObject newAdminObj = OpenCms.initCmsObject(OpenCms.getDefaultUsers().getUserGuest());	
				String adminLoggedInName = newAdminObj.loginUser(adminUsername, adminPassword, CmsContextInfo.LOCALHOST);
				//LOG.info("adminLoggedInName: " + adminLoggedInName);
				if (adminLoggedInName != null && adminLoggedInName.equals(adminUsername))
				{
					adminObj = newAdminObj;					
				}
			}
			
			return adminObj;
		}
		catch (Exception e)
		{

			// TODO: handle exception
			LOG.error("Error in getAdminCmsObject: " + e);
			e.printStackTrace();
			return null;
		}

	}

	public static CmsObject getDefaultObject()
	{
		try
		{
			CmsObject obj = OpenCms.initCmsObject(OpenCms.getDefaultUsers().getUserGuest());
			return obj;

		}
		catch (Exception e)
		{

			// TODO: handle exception
			LOG.error("Error in getDefaultObject: " + e);
			return null;
		}
	}

	public static CmsUser getCurrentUser(HttpServletRequest request) {
		CmsSessionManager man = OpenCms.getSessionManager();
		CmsUser currentUser = null;
		try {
			CmsSessionInfo info = man.getSessionInfo(request);
			CmsUUID currentUserId = info.getUserId();
			//LOG.info("Try to get user: "+currentUserId==null?"NULL":currentUserId.toString());
			currentUser = getAdminCmsObject().readUser(currentUserId);
		} catch (Exception ex) {
			LOG.error("Error in getCurrentUser: " + ex);
			ex.printStackTrace();
		}
		return currentUser;
	}

	public static CmsUser getCurrentUser(CmsUUID cmsUUID) {
		CmsSessionManager man = OpenCms.getSessionManager();
		CmsUser currentUser = null;
		try {
			/*
			CmsSessionInfo info = man.getSessionInfo(cmsUUID);
			LOG.info("Sesinfo: "+info);
			System.out.println("Sesinfo: "+info);
			CmsUUID currentUserId = info.getUserId();
			*/
			LOG.info("Try to get user: "+cmsUUID==null?"NULL":cmsUUID.toString());
			currentUser = getAdminCmsObject().readUser(cmsUUID); 		
			
		} catch (Exception ex) {
			LOG.error("Error in getCurrentUser: " + ex);
			ex.printStackTrace();
		}
		return currentUser;
	}
	
	public static CmsUser getCurrentUser(String sessionId) {
		CmsSessionManager man = OpenCms.getSessionManager();
		CmsUser currentUser = null;
		try {
			LOG.info("sessionId user info: " + sessionId);
			CmsSessionInfo info = man.getSessionInfo(sessionId);
			LOG.info("Current user info: " + info);
			CmsUUID currentUserId = info.getUserId();
			LOG.info("Try to get user: "+currentUserId==null?"NULL":currentUserId.toString());
			currentUser = getAdminCmsObject().readUser(currentUserId); 
			
		} catch (Exception ex) {
			LOG.error("Error in getCurrentUser: " + ex);
			ex.printStackTrace();
		}
		return currentUser;
	}

	/**
	 * Log out current user
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void logout(HttpServletRequest request) throws Exception
	{
		HttpSession session = request.getSession(false);
		if (session != null)
		{
			session.invalidate();
		}
	}

	/**
	 * @return List groups
	 * @throws Exception
	 */
	public static List getGroups() throws Exception
	{
		CmsObject obj = getAdminCmsObject();
		List group = obj.getGroups();
		
		if (group != null && group.size() > 0)
		{
			return group;
		}
		else
		{
			return null;
		}

	}

	/**
	 * @param username
	 * @return
	 */
	public static CmsUser getUserByName(String username)
	{
		CmsUser cmuser = null;
		try
		{
			CmsObject obj = getAdminCmsObject();
			cmuser = obj.readUser(username);
			
			return cmuser;
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in getUserByName: " + e);

			return null;
		}
	}
	
	public static CmsUser getWebuserByName(String username) {
		CmsUser cmuser = null;
		try {
			CmsObject obj = getAdminCmsObject();
			cmuser = obj.readUser(webUnitName + username);
			return cmuser;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Error in getWebuserByName: " + e);
			return null;
		}
	}
	
	/**
	 * @param username
	 * @return
	 */
	public static CmsUser getUserById(String userId)
	{
		CmsUser cmuser = null;
		try
		{
			CmsObject obj = getAdminCmsObject();
			cmuser = obj.readUser(new CmsUUID(userId));
			
			return cmuser;
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in getUserByName: " + e);

			return null;
		}
	}	

	public static boolean checkUserExists(String username)
	{
		CmsUser cmsuser = getUserByName(username);
		if (cmsuser != null)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	
	public static boolean checkWebuserExist(String username) {
		username = CheckConfigurationKey.getStringValue("webuser.unitname")+"/"+username;
		CmsUser cmsuser = getUserByName(username);
		if (cmsuser != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param name
	 * @param password
	 * @param group
	 * @param description
	 * @param additionalInfos
	 *            Create new WebUsers into OpenCms system
	 */
	public static void createWebUser(String name, String password,
			String group, String description, Map additionalInfos)
	{
		CmsObject cmo = getAdminCmsObject();
		try
		{
			/*
			CmsUser cmu = cmo.addWebUser(name, password, group, description, additionalInfos);
			cmu.setFirstname("FirstName");
			cmu.setLastname("LastName");
			cmu.setEmail("test@mail.com");
			cmo.writeWebUser(cmu);
			*/
			CmsUser cmu = cmo.createUser(name, password, description, additionalInfos);
			cmu.setFirstname("FirstName");
			cmu.setLastname("LastName");
			cmu.setEmail("test@mail.com");
			cmu.setFlags(32768);
			cmo.writeUser(cmu);
			CmsOrgUnitManager unitManager = OpenCms.getOrgUnitManager();
			unitManager.setUsersOrganizationalUnit(cmo, webUnitName, cmu.getName());
			cmo.addUserToGroup(webUnitName+cmu.getName(), "Members");
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in createWebUser: " + e);
			//e.printStackTrace();
		}

	}

	public static void updateUserDescription(String username, String description)
	{
		try
		{
			CmsObject cmo = getAdminCmsObject();
			CmsUser cmu = getUserByName(username);
			cmu.setDescription(description);
			cmo.writeWebUser(cmu);
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in updateUserDescription: " + e);
			//e.printStackTrace();
			//return;
		}

	}

	public static void updateWebuserDescription(String username, String description) {
		try {
			LOG.info("updateWebuserDescription():BEGIN");
			
			CmsObject cmo = getAdmCmsObject();
			CmsUser cmu = getUserByName(webUnitName + username);
			cmu.setDescription(description);
			cmo.writeUser(cmu);
			
			LOG.info("updateWebuserDescription():END");
		} catch (Exception e) {
			LOG.error("Error in updateUserDescription: " + e);
		}

	}
	
	public static void updatePassword(String username, String password)
	{
		try
		{
			CmsObject cmo = getAdminCmsObject();
			/* CmsUser cmu=getUserByName(username); */
			cmo.setPassword(username, password);
			// cmo.writeWebUser(cmu);
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in updatePassword: " + e);
//			e.printStackTrace();
//			return;
		}

	}
	
	public static void updateWebuserPassword(String username, String password) {
		try {
			CmsObject cmo = getAdminCmsObject();
			cmo.setPassword(webUnitName + username, password);
		} catch (Exception e) {
			LOG.error("Error in updatePassword: " + e);
		}
	}
	
	public static String getUserNameFromId (CmsObject cms, String userId)
	{
		if (cms != null && userId != null)
		{
			try
			{
				CmsUUID cmsUserId = new CmsUUID(userId);
				CmsUser user = cms.readUser(cmsUserId);
				if (user != null)
				{
					return user.getName();
				}
			}
			catch (Exception e)
			{
				// TODO: handle exception
				LOG.error("Error in getUserNameFromId: " + e);
			}
		}
		
		return null;

	}
	
	public static String getUserNameFromId (String userId)
	{

		try
		{
			CmsObject cms = getAdminCmsObject();
			if (cms != null && userId != null)
			{				
				CmsUUID cmsUserId = new CmsUUID(userId);
				CmsUser user = cms.readUser(cmsUserId);
				if (user != null)
				{
					return user.getName();
				}
			}
		}
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.error("Error in getUserNameFromId: " + e);
		}
		return null;

	}	

}
