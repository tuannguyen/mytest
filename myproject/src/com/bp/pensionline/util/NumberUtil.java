/*
 * (ConfigurationXML) copyright BP Pensions 2003
 */
package com.bp.pensionline.util;

// JDK Imports

// J2EE Imports

// BP Imports

/**
 *  TA general utility class for numbers.  
 * 
 *  Created by: mcobby, last updated by $Author: dom_the_bom $
 *
 *  @author mcobby
 *  @author $Author: dom_the_bom $
 *  @version $Revision: 1.1 $
 * 
 *  $Id: NumberUtil.java,v 1.1 2007/02/11 00:38:07 dom_the_bom Exp $
 */

import java.text.DecimalFormat;
import java.text.FieldPosition;


public class NumberUtil {
	public static final int DEFAULT_INTVALUE=0;
	public static final double DEFAULT_DOUBLEVALUE=-1.0;
	public static final double DEFAULT_DOUBLEZEROVALUE=0.0;
	/**
	 * Default Constructor 
	 */
	public NumberUtil() {
		super();		
	}
    
   /**
    * Returns the nearest value of ten to the number
    * @param value
    * @return
    */
   public static double toNearestTen(double value) {
       return Math.round(value/10.) * 10.;
   }
   
   /**
    * To no decimal places
    * @param num
    * @return
    */
   public static String toNearestOne(double num)  {
      double rounded = Math.round(num);
      return ""+rounded;
   }
   
   /**
    * 
    * @param currency
    * @param cash
    * @return
    */
   public static String toCurrency(String currency, double cash) {
      DecimalFormat format = new DecimalFormat("#,##0.00");
      FieldPosition f = new FieldPosition(0);
      StringBuffer s = new StringBuffer();
      format.format(cash, s, f);
      return currency + s.toString();
   }
   
   /**
    * 
    * @param currency
    * @return
    */
   public static String to2Dp(double dbl) {
      DecimalFormat format = new DecimalFormat("0.00");
      FieldPosition f = new FieldPosition(0);
      StringBuffer s = new StringBuffer();
      format.format(dbl, s, f);
      return s.toString();
   }
   
   public static String to3Dp(double dbl) {
	      DecimalFormat format = new DecimalFormat("0.000");
	      FieldPosition f = new FieldPosition(0);
	      StringBuffer s = new StringBuffer();
	      format.format(dbl, s, f);
	      return s.toString();
	   }   
   
   public static String to2DpYearSalary(double dbl) 
   {
	   int dblInt12 = (int)(dbl * 100) / 12;
	   double dbl12 = ((double)dblInt12 / 100) * 12;
	   
	   DecimalFormat format = new DecimalFormat("#,##0.00");
	   FieldPosition f = new FieldPosition(0);
	   StringBuffer s = new StringBuffer();
	   format.format(dbl12, s, f);
	   
	   return "�" + s.toString();
   } 
   
   public static String to2DpPercentage(double dbl) {
	      DecimalFormat format = new DecimalFormat("0.00");
	      FieldPosition f = new FieldPosition(0);
	      StringBuffer s = new StringBuffer();
	      format.format(dbl, s, f);
	      return s.toString()+"%";
	   }
   
   
   /**
    * @param currency
    * @param cash
    * @return
    */
   public static String toCurrencyNoDP(String currency, double cash) {
      DecimalFormat format = new DecimalFormat("#,##0");
      FieldPosition f = new FieldPosition(0);
      StringBuffer s = new StringBuffer();
      format.format(cash, s, f);
      return currency + s.toString();
    }   
   
   /**
    * No currency shown in front
    * @param cash
    * @return
    */
   public static String toCurrencyNoDP(double cash) {
      DecimalFormat format = new DecimalFormat("#,##0");
      FieldPosition f = new FieldPosition(0);
      StringBuffer s = new StringBuffer();
      format.format(cash, s, f);
      return s.toString();
    }  
   
   
   /**
    * Returns the number dropped to the nearest ten
    * @param value
    * @return
    */
   public static double toLowestTen(double value) {
      int result = (int)(value/10.0);
       return result * 10;
   }
   
   /**
    * Returns the number dropped to the nearest ten
    * @param value
    * @return
    */
   public static double toLowestThousand(double value) {
      int result = (int)(value/1000.0);
      return result * 1000;
   }
   
   /**
    * @param cash
    * @return
    */
   public static String toNearestPound(double cash)  {
      double ncash = Math.round(cash);
      return toCurrencyNoDP("�", ncash);
   }
   
         
   /**
    * @param cash
    * @return
    */
   public static String toLowestPound(double cash)  {
   	int	 ncash = (int)cash;
   	return toCurrencyNoDP("�", ncash);
   }

   /**
    * To no decimal places
    * @param num
    * @return
    */
   public static String toNoDecPl(double num)  {
      int rounded = (int)(num);
      return ""+rounded;
   }
   
   
   
   /**
    * @param cash
    * @return
    */
   public static String toNearestTenPound(double cash) {
      double ncash = Math.round(cash/10.) * 10.;
      return toCurrencyNoDP("�", ncash);
   }
   
   
   
   /**
    * Returns the number dropped to the nearest ten
    * 
    * @param value
    * @return
    */
   public static String toLowestTenPound(double value) {
      int rounded = (int)(value/10.0);
      return toCurrencyNoDP("�", rounded * 10.);     
   }
      
   /**
    * Returns the appropriate suffix for a number in english. e.g. 1=>st, 2=>nd, 3=>rd, 
    * 4th=>th, 5=>th ... 11th, 12th, 13th, 14th, ..... 20th, 21st, 22nd, .... 
    * and so on and on and on and on and on and on.
    * @param num
    * @return
    */
   public static String getNumSuffix(int num) {
       String result = null;
       
       // ok got to worth out what ages are Xst,Xnd,Xrd,Xth......
       // if 11, 12 or 13 then th  - the expections to the rule
       if ((num==11) || (num==12) || (num==13)) {
          result = "th";             
       } else {
           // calculate remainder of / 10
           int remainder = num % 10;                     
           switch (remainder) {
               
              case 1:    // ends in 1 == st
                         result = "st";
                         break;
                         
              case 2:    // ends in 2 == nd
                         result = "nd";
                         break;
                         
              case 3:    // ends in 3 == rd
                         result = "rd";
                         break;
                         
              default:   // all others == th
                         result = "th";
                         break;               
           }                      
       } // end else
       
       return result;        
   }
   
   
   /**
    * Strip all characters from input string which are not numbers or decimal points
    * e.g. �45,954.45 becomes 45954.45. The result is then converted to a double.
    * 
     * @param data
     * @return
     */
    public static double CurrencyToDecimal(String data)  {
      if (data == null) {
         return 0.0;
      }
      try{	        
	      int length = data.length();
	      StringBuffer output = new StringBuffer (length);
	
	      char currChar;
	      for (int i=0; i < length; i++) {
	         currChar = data.charAt(i);
	         if ( ((currChar>='0') && (currChar<='9')) || currChar == '-') {
	            output.append(currChar);
	         }else if (currChar == '.')  {
	             //the decimal point, keep this
	             output.append(currChar);
	         }else{
	            // we only want a-z, 0-9 so miss it out
	         }
	      }
	      return Double.parseDouble((output.toString()));
	
	  }catch(NumberFormatException nfex){
	      //bad format do nothing
	      return 0.0;
	  }
   }   
    /**
     * @param value
     * @return Int value of value
     */
    public static  int getInt(String value){
    	try {
			int result=Integer.parseInt(value);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			
			return DEFAULT_INTVALUE;
			
		}
    	
    }
    public static double getDouble(String value) {
    	try{
    		double result=Double.parseDouble(value);
    		return result;
    	}catch (Exception e) {
			// TODO: handle exception
    		System.out.println("=================NumberUtil getDouble error: ");
    		System.out.println("////////////// value to convert is : " +value);
    		//e.printStackTrace();
    		return DEFAULT_DOUBLEVALUE;
    		
		}
    	
    }
   
   
}
