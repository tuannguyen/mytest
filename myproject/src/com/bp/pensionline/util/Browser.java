package com.bp.pensionline.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class Browser extends HttpServlet {

	protected HttpServletRequest request;

	protected HttpSession session;

	protected String userAgent = "";

	protected String company = "";

	protected String name = "";

	protected String version;

	protected String os = "";
	
	protected String referrer = "";
	
	protected boolean javascript = true;

	/**
	 * @param request
	 */
	public Browser(HttpServletRequest paramRequest) {

		this.request = paramRequest;
		this.session = this.request.getSession();

		this.setUserAgent(this.request.getHeader("User-Agent"));
		
		this.setCompany();
		this.setName();
		this.setVersion();
		this.setOs();
		this.setReferrer();
		this.setJavascript();

	}


	public Browser(HttpServletRequest paramRequest, String sessionID) {

		this.request = paramRequest;		
		this.session = this.request.getSession();
		

		this.setUserAgent((String)TextUtil.sessionParameters.get(sessionID));
		

		
		this.setCompany();
		this.setName();
		this.setVersion();
		this.setOs();
		this.setReferrer();
		this.setJavascript();

	}

	/**
	 * @param httpUserAgent
	 */
	public void setUserAgent(String httpUserAgent) {
		if(httpUserAgent != null)
			this.userAgent = httpUserAgent.toLowerCase();
		else
			this.userAgent = "";
	}
	
	
	public String getUserAgent(){
		return this.userAgent;
	}
	
	/**
	 * 
	 */
	private void setReferrer(){
		if(this.request.getHeader("Referer") != null)
			this.referrer = this.request.getHeader("Referer");
		else
			this.referrer = "unknown";
	}
	
	/**
	 * @return
	 */
	public String getReferrer(){
		return this.referrer;
	}
	
	/**
	 * 
	 */
	private void setJavascript(){
		/*
		Cookie[] cArray = request.getCookies();
		if(cArray != null && cArray.length > 0){
			for(int i = 0; i < cArray.length; i++){
				Cookie cookie = cArray[i];		
				if(cookie.getName().compareTo("javascript") == 0){
					this.javascript = true;
				}
			}
		}
		*/		
	}
	
	/**
	 * @return
	 */
	public boolean getJavscript(){
		return this.javascript;
	}

	/**
	 * 
	 */
	private void setCompany() {
		if (this.userAgent.indexOf("msie") > -1) {
			this.company = "MSIE";
		} else if (this.userAgent.indexOf("opera") > -1) {
			this.company = "Opera Software";
		} else if (this.userAgent.indexOf("firefox") > -1) {
			this.company = "Firefox";
		} else if (this.userAgent.indexOf("safari") > -1) {
			this.company = "Safari";
		} else {
			this.company = "Other";
		}
	}

	
	/**
	 * @return
	 */
	public String getCompany() {
		return this.company;
	}

	/**
	 * 
	 */
	private void setName() {

		if (this.userAgent.indexOf("msie") > -1) {
			this.name = "Microsoft Internet Explorer";
		} else if (this.userAgent.indexOf("mozilla") > -1) {
			this.name = "Netscape Navigator";
		} else if (this.userAgent.indexOf("opera") > -1) {
			this.name = "Operasoftware Opera";
		} else if (this.userAgent.indexOf("safari") > -1) {
			this.company = "Apple";
		} else {
			this.name = "Other";
		}
		
	}

	
	/**
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 */
	private void setVersion()
	{
		int tmpPos;
		String tmpString;

		if (this.company == "MSIE")
		{
			if (this.userAgent.indexOf("msie") != -1)
			{
				String str = this.userAgent.substring(this.userAgent.indexOf("msie") + 5);
				if (str.indexOf(";") != -1)
				{
					this.version = str.substring(0, str.indexOf(";"));
				}				
			}
		}
		else
		{
			if (this.userAgent != null && this.userAgent.length() > 0)
			{
				tmpPos = this.userAgent.indexOf("/");
				int spacePos = this.userAgent.indexOf(" ", tmpPos);
				if (this.userAgent.indexOf("/") != -1 &&  spacePos != -1)
				{
					this.version = (this.userAgent.substring(tmpPos + 1, spacePos)).trim();
				}
			}
			else
			{
				this.version = "";
			}
		}
	}

	
	/**
	 * @return
	 */
	public String getVersion() {
		return this.version;
	}

	/**
	 * 
	 */
	private void setOs() {
		if (this.userAgent.indexOf("win") > -1) {
			if (this.userAgent.indexOf("Windows 95") > -1
					|| this.userAgent.indexOf("win95") > -1) {
				this.os = "Windows 95";
			}
			if (this.userAgent.indexOf("windows 98") > -1
					|| this.userAgent.indexOf("win98") > -1) {
				this.os = "Windows 98";
			}
			if (this.userAgent.indexOf("windows nt") > -1
					|| this.userAgent.indexOf("winnt") > -1) {
				this.os = "Windows NT";
			}
			if (this.userAgent.indexOf("win16") > -1
					|| this.userAgent.indexOf("windows 3.") > -1) {
				this.os = "Windows 3.x";
			}
			if (this.userAgent.indexOf("mac") > -1
					|| this.userAgent.indexOf("Mac") > -1) {
				this.os = "Macintosh";
			}
			if (this.userAgent.indexOf("linux") > -1
					|| this.userAgent.indexOf("Linux") > -1) {
				this.os = "Linux";
			}
		}else{
			this.os = "Other";
		}
	}
	
	/**
	 * @return
	 */
	public String getOs() {
		return this.os;
	}
}
