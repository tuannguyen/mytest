package com.bp.pensionline.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 16-06-2007
 * This class use to deep copy object
 * 
 *
 */
public class ObjectCloner {
	   // so that nobody can accidentally create an ObjectCloner object
	   private ObjectCloner(){}
	   // returns a deep copy of an object
	   static public Object deepCopy(Object oldObj) throws Exception
	   {
	      ObjectOutputStream oos = null;
	      ObjectInputStream ois = null;
	      try
	      {
	         ByteArrayOutputStream bos = 
	               new ByteArrayOutputStream(); 
	         oos = new ObjectOutputStream(bos); 
	         // serialize and pass the object
	         oos.writeObject(oldObj);  
	         oos.flush();              
	         ByteArrayInputStream bin =new ByteArrayInputStream(bos.toByteArray()); 
	         ois = new ObjectInputStream(bin);                  
	         // return the new object
	         return ois.readObject(); 
	      }
	      catch(Exception e)
	      {
	         
	         throw(e);
	      }
	      finally
	      {
	         oos.close();
	         ois.close();
	      }
	   }
	   
	}
