package com.bp.pensionline.util;



import java.io.Serializable;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;


//Referenced classes of package uk.co.aquilauk.wef.util:
//         Debug

public class JNDIUtil
 	implements Serializable {

	static Logger log = Logger.getLogger(JNDIUtil.class.getName());
	
	private static final long serialVersionUID = 1L;
	private Hashtable<String, String> env;
	private String prefix;
	private String ejbPrefix;
	private String jdbcPrefix;
	private String envPrefix;

 public JNDIUtil() {
     env = new Hashtable<String, String>();
     prefix = "java:comp/env";
     ejbPrefix = "/ejb/";
     jdbcPrefix = "/jdbc/";
     envPrefix = "/";
 }

 public JNDIUtil(String s) {
     env = new Hashtable<String, String>();
     prefix = "java:comp/env";
     ejbPrefix = "/ejb/";
     jdbcPrefix = "/jdbc/";
     envPrefix = "/";
     prefix = s;
 }

 public String getPrefix() {
     return prefix;
 }

 public void setPrefix(String s) {
     prefix = s;
 }

 public String getEjbPrefix() {
     return ejbPrefix;
 }

 public void setEjbPrefix(String s) {
     ejbPrefix = s;
 }

 public String getJdbcPrefix() {
     return jdbcPrefix;
 }

 public void setJdbcPrefix(String s) {
     jdbcPrefix = s;
 }

 public String getEnvPrefix() {
     return envPrefix;
 }

 public void setEnvPrefix(String s) {
     ejbPrefix = s;
 }

 public String getEjbUrl() {
     return prefix + ejbPrefix;
 }

 public String getEjbUrl(String s) {
     return getEjbUrl() + s;
 }

 public String getJdbcUrl() {
     return prefix + jdbcPrefix;
 }

 public String getJdbcUrl(String s) {
     return getJdbcUrl() + s;
 }

 public String getEnvUrl() {
     return prefix + envPrefix;
 }

 public Hashtable getEnvironment() {
     return env;
 }

 public void setEnvironment(Hashtable<String, String> hashtable) {
     env = hashtable;
 }

 public void addEnvironmentEntry(Object obj, Object obj1) {
     if(env == null)
         env = new Hashtable<String, String>();
     env.put(obj.toString(), obj1.toString());
 }

 public void removeEnvironmentEntry(Object obj) {
     if(env != null)
         env.remove(obj);
 }

 private Object lookup(String s) {
     try {
         InitialContext initialcontext = new InitialContext(env);
         return ((Context) (initialcontext)).lookup(s);
     }
     catch(NamingException namingexception) {
    	 log.debug("JNDIUtil.lookup: Exception thrown ", namingexception);
     }
     return ((Object) (null));
 }

 public String stringLookup(String s) {
     return stringLookup(s, ((String) (null)));
 }

 public String stringLookup(String s, String s1) {
     try {
         String s2 = (String)lookup(getEnvUrl() + s);
         log.debug("JNDIUtil.stringLookup: Looking for " + getEnvUrl() + s + ", Found " + (s2 != null ? s2 : "null"));
         if(s2 != null)
             return s2;
         else
             return s1;
     }
     catch(Exception exception) {
    	 log.debug("JNDIUtil.stringLookup: Exception getting environment entry " + s, exception);
     }
     return s1;
 }

 public Boolean booleanLookup(String s) {
     return booleanLookup(s, ((Boolean) (null)));
 }

 public Boolean booleanLookup(String s, Boolean boolean1) {
     try {
    	 log.debug("JNDIUtil.booleanLookup: Looking for " + getEnvUrl() + s);
         Boolean boolean2 = (Boolean)lookup(getEnvUrl() + s);
         log.debug("JNDIUtil.stringLookup: Found " + (boolean2 != null ? !boolean2.booleanValue() ? "false" : "true" : "null"));
         if(boolean2 != null)
             return boolean2;
         else
             return boolean1;
     }
     catch(Exception exception) {
    	 log.debug("JNDIUtil.booleanLookup: Exception getting environment entry " + s, exception);
     }
     return boolean1;
 }

 public Integer integerLookup(String s) {
     return integerLookup(s, ((Integer) (null)));
 }

 public Integer integerLookup(String s, Integer integer) {
     try {
         Integer integer1 = (Integer)lookup(getEnvUrl() + s);
         if(integer1 != null)
             return integer1;
         else
             return integer;
     }
     catch(Exception exception) {
    	 log.debug("JNDIUtil.integerLookup: Exception getting environment entry " + s, exception);
     }
     return integer;
 }

 public DataSource dataSourceLookup(String s) {
     return dataSourceLookup(s, ((DataSource) (null)));
 }

 public DataSource dataSourceLookup(String s, DataSource datasource) {
     try {
         DataSource datasource1 = (DataSource)lookup(getJdbcUrl() + s);
         log.debug("JNDIUtil.dataSourceLookup: Looking for " + getJdbcUrl() + s + ", Found " + (datasource1 != null ? ((Object) (datasource1)).getClass().getName() : "null"));
         if(datasource1 != null)
             return datasource1;
         else
             return datasource;
     }
     catch(Exception exception) {
    	 log.debug("JNDIUtil.dataSourceLookup: Exception getting DataSource " + getJdbcUrl() + s, exception);
     }
     return datasource;
 }
}
