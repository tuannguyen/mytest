package com.bp.pensionline.util;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.XmlReader;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class GetContentGroup{
	
	/**
	 * @param uri
	 * @return
	 */
	public static String getContentGroup(String uri){
		Log LOG = CmsLog.getLog(GetContentGroup.class);
//		//LOG.info("getContentGroup():BEGIN");
		String groupName = "Other";
		XmlReader reader = new XmlReader();
		
		try{
			
			byte[] b = reader.readFile(Environment.XMLFILE_DIR + "ContentGroupMapper.xml");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(b);
			document = builder.parse(is);

			NodeList groupList = document.getElementsByTagName("Group");
			
			for(int i = 0; i < groupList.getLength(); i++){
				
				NodeList pageList = groupList.item(i).getChildNodes();
				
				for(int j = 2; j < pageList.getLength(); j++){
					
					Node name = pageList.item(1);
					
					
					Node pageNode = pageList.item(j);
					
					if(pageNode.getNodeType() == Node.ELEMENT_NODE){
						
						String page = pageNode.getTextContent();						
						
						if(uri.indexOf(page) > -1 || page.indexOf(uri) > -1){						

							groupName = name.getTextContent();
							break;
						}
						
					}
				}
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.GetContentGroup.getContentGroup() error: ",e);
		}		
		//LOG.info("getContentGroup():END");
		return groupName;
	}
}