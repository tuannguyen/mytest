package com.bp.pensionline.util;

import java.util.regex.*;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

/**
 * @author SonNT
 * @version 1.0
 * @since 2007/5/10
 */
public class ValidChangeBank
{   
	/**
	 * Validate change bank details
	 * @param emailaddress
	 * @return
	 */
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	public static int validChangeBank(String accNumber, String sortCode, String bank) 
	{
		int matchFound = 4;
			 
		try{			
			
			Pattern pattern = Pattern.compile("\\d{8}");

			//Determine if pattern exists in input		
			Matcher matcher = pattern.matcher(accNumber);
			if(!matcher.matches()){
				
				return 1;
				
			}			
			
			pattern = Pattern.compile("[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{2}");

			//Determine if pattern exists in input		
			matcher = pattern.matcher(sortCode);
			if(!matcher.matches()){
				
				return 2;
				
			}
			//System.out.println(bank.length());
			pattern = Pattern.compile("[-a-z A-Z0-9_]{0,30}");

			//Determine if pattern exists in input		
			matcher = pattern.matcher(bank);
			if(!matcher.matches()){
				
				return 3;
				
			}			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error(e.getMessage() + e.getCause());
		}
		
		return matchFound;
	}	
	
}