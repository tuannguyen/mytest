package com.bp.pensionline.util;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

public class Mapper extends HashMap{
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private HashMap map = null;
	
	public Mapper(){
		map = new HashMap();
	}
	
	public void set(Object key, Object value){
		try{
			if(map.containsKey(key)){
				map.remove(key);
			}
			map.put(key, value);
			//System.out.println("Mapper SET:'" + key + "' and '" + value + "'");
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.Mapper.set() error: ",e);
		}
	}
	
	public Object get(Object key){
		Object value = null;
		try{
			value = map.get(key);
			//System.out.println("Mapper GET:'" + key + "'-->'" + String.valueOf(value) + "'");
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.util.Mapper.get() error: ",e);
		}
		return value;
	}
}