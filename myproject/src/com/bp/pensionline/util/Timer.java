package com.bp.pensionline.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author SonNT
 * @version 1.0
 * @since 27/06/2007
 *
 */
public class Timer{
	
	
	/**
	 * @return
	 */
	public static int getYear(){
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.YEAR);
		return year;
	}
	/**
	 * @return
	 */
	public static int getMonth(){	
		int year = Calendar.getInstance().get(Calendar.MONTH);		
		return year + 1;
	}
	/**
	 * @return
	 */
	public static int getDay(){
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.DAY_OF_MONTH);
		return year;
	}
	/**
	 * @return
	 */
	public static int getHour(){
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.HOUR_OF_DAY);
		return year;
	}
	/**
	 * @return
	 */
	public static int getMin(){
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.MINUTE);
		return year;
	}
	/**
	 * @return
	 */
	public static int getSec(){
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.SECOND);
		return year;
	}
	/**
	 * @return
	 */
	public static BigDecimal getTime(){
		Calendar cal = new GregorianCalendar();
		BigDecimal time = BigDecimal.valueOf(cal.getTimeInMillis()/1000);		
		return time;
	}
	/**
	 * @return
	 */
	public static BigDecimal getTimeMs(){
		Calendar cal = new GregorianCalendar();
		BigDecimal time = BigDecimal.valueOf(cal.getTimeInMillis());
		return time;
	}
}
 