/*
 * (ConfigurationXML) copyright BP Pensions 2003
 */
package com.bp.pensionline.util;

// JDK Imports
import java.lang.Math;
import java.util.*;
import java.text.*;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


/**
 *  A general utility class for converting Raw data formats to those required.
 *  Backend processing for raw data into business data for addition to XML files for 
 *  Presentations manipulation
 *  
 *  Created by: dcarlyle, last updated by $Author: vinh.dao $
 *
 *  @author dom the bomb 
 *  @author $Author: vinh.dao $
 *  @version $Revision: 1.2 $
 * 
 *  $Id: Variants.java,v 1.2 2007/04/10 15:06:52 vinh.dao Exp $
 */


public class Variants {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * 
	 * @param attribute
	 * @param value
	 * @return
	 */
	
		public Variants(){}
		
		public String convert(String attribute, String value){
		
			String result=value;
			
		if (value!=null && value.trim().length() > 0)
		{
			/** Currency with no specified format */
			if (attribute.endsWith("Salary")||
					attribute.endsWith("SalaryRaw")||
					attribute.endsWith("Pension")||
					(attribute.endsWith("Benefit")&& attribute.startsWith("Eastern"))||
					attribute.endsWith("BenefitRaw")||
					attribute.endsWith("Pound")||
					attribute.endsWith("Money")){
				result=currency("&#163;", result);
				//result=TextUtil.substituteCurrency(result);
			}
			
		
			/** Attribute12 */
			if (attribute.endsWith("12")){
				result= twelve(value);
				result=currency("&#163;", result);
			}
			
			/** AttributeRounded */
			else if(attribute.endsWith("Rounded")){
				result= rounded(value);
				result=currency("&#163;", result);
			}
			
			/** DateNumeric DD/MM/YYYY*/
			else if(attribute.endsWith("Numeric")){
						result= convertDate("DateNumeric", value);
			}
			
			/** DateLong DD MONTH YYYY*/
			else if(attribute.endsWith("Long")){
						result= convertDate("DateLong", value);
			}

			/** DateRaw YYYY-MM-DD*/
			else if(attribute.endsWith("DateRaw") || 
					(attribute.endsWith("Raw") && 
							(attribute.startsWith("Date")||
									attribute.startsWith("Dob")
									&& attribute.startsWith("Nrd")))
								){
						result= convertDate("DateRaw", value);
			}
			
			/** Date DD MON YYYY*/
			else if(attribute.endsWith("Date") || 
					attribute.startsWith("Date")||
					attribute.equalsIgnoreCase("Dob")|| 
					attribute.equalsIgnoreCase("Nrd")||
					attribute.equalsIgnoreCase("Npd")||
					attribute.equalsIgnoreCase("Start")){
				
				result= convertDate("DateOnly", value);
			}
			else if (attribute.equalsIgnoreCase("EmailAddress")){
				if (value.trim().length()==1)
					result="";
			}
				
				
		}
		else
			value="temp";
			
		/**
		 *  Newsletter tag
		 * */
		
		if(attribute.equalsIgnoreCase("Newsletter")){
			result=getNewsletterType(value);
		}	
		
		/**
		 * CPFFlag 
		 * */
		if(attribute.equalsIgnoreCase("StateBenefitsAvailable")){
			result=getStateBenefitsAvailable(value);
		}
	
	/**
		 * MembershipStatus List
		 *AC	Active
		 *LM	Life Member
		 *OO	Opted-Out
		 *PN	Pensioner
		 *PP	Preserved Pensioner
		 *PR	Pensioner with Protected Rights
		 *WD	Dead with Widow/Dependant Benefits
		 *DR	Deferred Retirement
		 *NL	No Liability
		 *PD	Pending
		 *PC	PHI Claimant
		 *NB	New Business
		 *WB	Widow(er) Beneficiary with Pension
		 *DB	Dependant Beneficiary with Pension
		 *CB	Child Beneficiary with Pension
		 *DE	Earmarked benefits from a divorce settlement
		 *DS	Benefits from a Pension Sharing Order
		 *XX	BP Society Members
		 *AN	TNK and GPTP Members	
		 *
		 * */
	
		else if (attribute.equalsIgnoreCase("MembershipStatus")){	
			result=getMembershipDetail(value);	
		}
	
	
		else if(attribute.equalsIgnoreCase("PloDetailsAvailable")){
			result=getPloDetailsAvailable(value);
		
		}
	
		else if(attribute.equalsIgnoreCase("Conditional")){
			result=getConditional(value);
			}
		/**
		 * 0001: BP Pension Scheme
		 * */
		else if (attribute.equalsIgnoreCase("Scheme")){
			result=getSchemeDetail(value);
		}
		
		
		if (result==null||(result.trim().length()==0 && result.length()>0))
			result="";
		return result;
		
	}
	

	/**
	 * 12 - post fix on an attribute, e.g. Attribute12, this means 
	 * what is the value over 12 months.
	 * 
	 * Look for the end of the String and check for 12 - 
	 * 
	 *
	 */
	String twelve(String rawVal ){
		
		double raw = Double.parseDouble(rawVal);
		
		raw /= 12.0;
		
		raw = Math.ceil(raw);
		
		raw *= 12.0;
		
		//now format
		// 2 decima
		raw = twoDecimalPlaces(raw);

		return ""+raw;
	}
	
	/**
	 * Rounded - post fix on an attribute, e.g. AttributeRounded,
	 * 
	 * Look for the end of the String and check for Rounded - 
	 * Multiple by 10, use Math.round, divide it by 10
	 *
	 */
	public String rounded(String rawVal ){
		
		double raw = Double.parseDouble(rawVal);
		raw*=10;
		
		raw=Math.round(raw);
		
		raw/=10;

		return ""+raw;
	}
	
	/**
	 * Multiply the number by 100, use Math.rint, and divide it by 100.00 
	 */
	double twoDecimalPlaces(Double num){
		
		num *= 100.0;
		
		num= Math.rint(num);
		
		num /= 100.0;
		
		return num;
	}
	
	
    /**
    * format a value with currency indicator, in this project, 
    * currency is Sterling Pound (�)
    * 
    * @param currency
    * @param cash
    * @return
    */
	  String currency(String currency, String rawVal) {	
		  double cash = Double.parseDouble(rawVal);
		  DecimalFormat format = new DecimalFormat("#,##0.00");
		  FieldPosition f = new FieldPosition(0);
		  StringBuffer s = new StringBuffer();
		  format.format(cash, s, f);
		  return currency + s.toString();
   } 
   
	  
	  /**
	   * This method is to convert date into different formats
	   * date input in a raw format as yyyy-MM-dd e.g. 2006-06-30
	   * 
	   * @param indicator
	   * @param date
	   * @return
	   * */
	String convertDate(String indicator, String inputDate){
		String outputDate=inputDate;
		
		
	
		try{
			Date dtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(inputDate);
	
			/**
			 * DateOnly format: dd-MMM-yyyy e.g. 30-JUN-2006
			 * */
		      if (indicator.equalsIgnoreCase("DateOnly")){
		    	  outputDate = new SimpleDateFormat("d MMM yyyy").format(dtTmp);	
		      }
		    
		      /**
		       * DateRaw format: yyyy-MM-dd e.g. 2006-06-30
		       * 
		       * */
		      if (indicator.equalsIgnoreCase("DateRaw")){
		    	 
		    	  outputDate = new SimpleDateFormat("yyyy-MM-dd").format(dtTmp);
		      }
		      
		      
		      /**
		       * DateLong format: dd MMMMM yyyy e.g 30th June 2006
		       * 
		       * */
		      if (indicator.equalsIgnoreCase("DateLong")){
		    	  	  outputDate = new SimpleDateFormat("dd MMMMM yyyy").format(dtTmp);
		    	  	  int i=Integer.parseInt(outputDate.substring(0, 2));
		    	  	  String longDateTmp=getLongDate(i);
		    	  	  outputDate=longDateTmp+" " + outputDate.substring(2,outputDate.length());
		      }
			
		      /**
		       * DateNumeric format: dd/MM/yyyy e.g 30/06/2006
		       * 
		       * */
		      if (indicator.equalsIgnoreCase("DateNumeric")){
		    	  outputDate = new SimpleDateFormat("dd/MM/yyyy").format(dtTmp);		
		      }
		      
		      
		      }catch(ParseException parseException){
		    	  parseException.getMessage();
		      }
		    
		return outputDate;
		
	}
	/**
	 * get long date form i.e. 1st, 2nd, 3th, etc.
	 * 
	 * @param i
	 * @return String
	 * 
	 * */
	String getLongDate(int i){
		String temp=String.valueOf(i);
		switch(i){
		case 1:
		case 21:
		case 31:
			temp= i+ "st";
		break;
		
		case 2:
		case 22:
			temp=i+"nd";
		break;
		
		case 3:
		case 23:
			temp=i+"rd";
		break;
		default:
			temp=i+"th";
		}
		return temp;
	}
		
	/** it is only interested in if Scheme is 
	 * - BP Pension Scheme (Scheme value is 0001) or
	 * - BC Scheme: 
	 * 
	 * 0001	BP Pension Scheme
	 * 0050	BP Oil Contributory Pension Fund 1976
	 * 0051	BP Dorset Pension Scheme
	 * 0052	Duckhams & Co. Ltd. Pension Schemes 1972
	 * 0053	Hythe Chemicals Ltd Staff Pension Scheme
	 * 0054	BP Pension Scheme for Industrial Staff
	 * 0055	Mebon Ltd (Staff) Pension
	 * 0056	Mebon Ltd (Works) Pension
	 * 0057	BP Nutrition (UK) Ltd Contributory Sch
	 * 0058	BP Nutrition (UK) Ltd Pension Scheme
	 * 0059	Robert McBride Grp Ltd Contributory Sch
	 * 0060	RMG Pension Scheme
	 * 0061	Robert McBride Grp Ltd Pension & LA Sch
	 * 0062	BP Chemicals Pension Scheme
	 * 0063	BP Pension Scheme for Indian Staff
	 * 0064	BP Pension Scheme for Iraqi Staff
	 * 0065	BP Pension Scheme for Pakistani Staff
	 * 0071	BP Minerals (1981) Pension
	 * 0072	ER Holloway Ltd Pension & Life Ass Sch
	 * 0073	Erinoid Pension Scheme
	 * 0074	Finsbury Pavement Pension
	 * 0075	Hitco Aerospace Pension Scheme
	 * 0076	Kuwait Oil
	 * 0078	Powles Hunt & Sons Ltd Staff
	 * 0080	1950 Provident Fund
	 * 0081	Scicon Staff Pension Scheme
	 * 0082	Talisman Schemes
	 * 0083	BP Chemicals (UK) Ltd & Associated Cos.
	 * 0088	Turkey Pension Scheme
	 * 0099	Ex gratia Allowances
	 * 0100	Britoil Retirement Benefits Plan
	 * 0120	ARCO British Pension Scheme
	 * 0200	BXL Pension Scheme
	 * 0201	BXL Works Pension Scheme
	 * 0275	Amoco (UK) Pension Plan
	 * 0280	Charringtons Pension Plan	
	 * 0300	Scottish Oils Pension Scheme
	 * 0301	Dominion Oils
	 * 0302	BP Advanced Materials Pension Plan
	 * 0303	Ellis & McHardy (4)
	 * 0304	Ellis & McHardy (5)
	 * 0305	BP Gas
	 * 0320	Polygon Retailing Ltd 1988 Ret Fund
	 * 0325	BP Retailing Pension Scheme
	 * 0330	BPFM Pension Scheme
	 * 0459	Amoco Fabrics Pension Plan
	 * 0500	BP Society
	 * 
	 * */
	String getSchemeDetail(String value){
		if (value.equalsIgnoreCase("0001")){
			value="BP Pension Scheme";
		}
	/**check the value of scheme correspond to BC scheme n replace it later
	 * 		else if(value.equalsIgnoreCase("0001")){
	 * 			value="BP Pension Scheme";
	 * 		}
	 * */
		else 
			value="other schemes";
			
		return value;
	}
	 /** <p>Represents the status of the member record. This is vital fo telling us if the
	 * member if:
	 * <li>Active></li>
	 * <li>Deferred</li>
	 * <li>Pensioner</li>
	
	 * */
	
	String getMembershipDetail(String value){
		if (value.equalsIgnoreCase("AC")){
			value="Active";
		}
		else if (value.equalsIgnoreCase("PP")){
			value="Deferred";
		}
		else 
			value="Pensioner";
		
		return value;
	}
	
	String getPloDetailsAvailable(String value){
		
		if (value.equalsIgnoreCase("Y"))
			return "true";
		
			return "false";
	}
	
	String getConditional(String value){
		if (value.equalsIgnoreCase("AC"))
			return "true";
		
			return "false";
	}
	
	
	String getStateBenefitsAvailable(String value){
		if (value.equalsIgnoreCase("Y"))
			return "false";
		
			return "true";
	}
	/**
	 * get detail type of newsletter to be delivered
	 * T : Taped
	 * E : Email
	 * P : Paper
	 * NULL this is the default and it will be assumed the same as if it
      were P status -> Paper.

	 * */
	String getNewsletterType(String value){
		LOG.info("newsletter value: " + value);
		if (value.equalsIgnoreCase("T"))
			return "Taped";
		
		else if (value.equalsIgnoreCase("E"))
			return "Email";
		
		else
			return "Paper";
	}  
}
