package com.bp.pensionline.util;

import java.util.StringTokenizer;

import com.bp.pensionline.test.Constant;

public final class StringUtil {
	
	
	public static String getString(Object val){
			
		if(val == null){
			
			val = new String("");
			
		}
		return val.toString();
		
	}
	
	public static String Escape(String val){
		
		StringTokenizer tokens = new StringTokenizer(val, Constant.DELIMITER);
		
		StringBuffer buffer = new StringBuffer();
		
		
		while(tokens.hasMoreTokens()){
			
			buffer.append(tokens.nextToken()).append("_");
			
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		
		return buffer.toString();
		
	}
	
		
	public static String EMPTY_STRING = "";
	
}
