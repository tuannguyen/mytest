/**
 * 
 */
package com.bp.pensionline.sqlreport.dao;

/**
 * @author anhtuan
 *
 */
public enum DbError {
	OK(0), NO_CONNECTION(1), SQL_SYNTAX(2);
	
	private int errCode;
	DbError(int value){
		errCode = value;
	}
	public int getValue(){
		return errCode;
	}
	public String getDescription(){
		switch(this){
		case OK: return "Successful! ";
		case NO_CONNECTION: return "Connection to DB is interupted";
		case SQL_SYNTAX: return "Error in SQL query string";
		default: return "Unidentifiable error!! ";
		}
	}
}
