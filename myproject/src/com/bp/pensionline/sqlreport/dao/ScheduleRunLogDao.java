/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.sqlreport.dao;

import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.util.LogUtil;

import org.apache.commons.logging.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  anhtuan
 */
public class ScheduleRunLogDao extends GenericDao {

    //~ Static fields/initializers ---------------------------------------------

    /** DOCUMENT ME! */
    public static final Log log = LogUtil.getLog(ScheduleRunLogDao.class);

    /** DOCUMENT ME! */
    public static final String DATE_FORMAT = "dd-MMM-yyyy";

    /** DOCUMENT ME! */
    public static final String TIME_FORMAT = "HH:mm:ss";

    /** DOCUMENT ME! */
    public static final String COL_ID = "id";

    /** DOCUMENT ME! */
    public static final String COL_SCHEDULEID = "schedule_id";

    /** DOCUMENT ME! */
    public static final String COL_RUNTIME = "run_time";

    /** DOCUMENT ME! */
    public static final String COL_RUNBY = "run_by";

    /** DOCUMENT ME! */
    public static final String INSERT;

    /** DOCUMENT ME! */
    public static final String SELECT_LAST_RUNS_BY_SCHEDULE;

    static {
        StringBuilder insert = new StringBuilder();
        insert.append("INSERT INTO sqlreport_schedule_runlog (");
        insert.append(COL_SCHEDULEID);
        insert.append(",").append(COL_RUNBY);
        insert.append(",").append(COL_RUNTIME);
        insert.append(") VALUES(?,?,?)");
        INSERT = insert.toString();
        StringBuilder selLastRunsBySchedule = new StringBuilder();
        selLastRunsBySchedule.append(
            "SELECT * FROM sqlreport_schedule_runlog WHERE ");
        selLastRunsBySchedule.append(COL_ID).append("=(").append("SELECT MAX(")
                             .append(COL_ID).append(")")
                             .append(" FROM sqlreport_schedule_runlog")
                             .append(" WHERE ").append(COL_SCHEDULEID).append(
                                 "=?");
        selLastRunsBySchedule.append(")");
        SELECT_LAST_RUNS_BY_SCHEDULE = selLastRunsBySchedule.toString();
    }

    //~ Constructors -----------------------------------------------------------

    /**
     * Creates a new $class.name$ object.
     *
     * @param   connection  DOCUMENT ME!
     *
     * @throws  SQLException  DOCUMENT ME!
     */
    public ScheduleRunLogDao(Connection connection) throws SQLException {
        super(connection);
    }

    /**
     * Creates a new $class.name$ object.
     *
     * @throws  SQLException  DOCUMENT ME!
     */
    public ScheduleRunLogDao() throws SQLException {
        super();
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @param   scheduleId  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  SQLException  DOCUMENT ME!
     */
    public ScheduleRunLog getLastRunOfSchedule(long scheduleId)
        throws SQLException {
        Schedule schedule = new Schedule();

        return getLastRunOfSchedule(schedule);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   schedule  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  SQLException  DOCUMENT ME!
     */
    public ScheduleRunLog getLastRunOfSchedule(Schedule schedule)
        throws SQLException {
        ScheduleRunLog runlog = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(SELECT_LAST_RUNS_BY_SCHEDULE);
            ps.setLong(1, schedule.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
                runlog = new ScheduleRunLog();
                runlog.setSchedule(schedule);
                runlog.setId(rs.getLong(COL_ID));
                String runBy = rs.getString(COL_RUNBY);
                if (runBy != null) {
                    User user = new User();
                    user.setUserName(runBy);
                    runlog.setManualRunner(user);
                }
                try {
                    runlog.setRunningTime(parseDate(rs.getString(COL_RUNTIME)));
                } catch (ParseException e) {
                    log.error("Invalid Date value of Schedule RunLog record "
                        + runlog.getId());
                    e.printStackTrace();
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }

        return runlog;
    }

    /**
     * DOCUMENT ME!
     *
     * @param   runlog  DOCUMENT ME!
     *
     * @throws  SQLException  DOCUMENT ME!
     */
    public void insert(ScheduleRunLog runlog) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(INSERT);
            ps.setLong(1, runlog.getSchedule().getId());
            if (runlog.getManualRunner() == null) {
                ps.setNull(2, Types.VARCHAR);
            } else {
                ps.setString(2, runlog.getManualRunner().getUserName());
            }
            ps.setString(3, formatDate(runlog.getRunningTime()));
            ps.executeUpdate();
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param   date  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public static String formatDate(Date date) {
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " "
                + TIME_FORMAT);

        return formater.format(date);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   sDate  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  ParseException  DOCUMENT ME!
     */
    public static Date parseDate(String sDate) throws ParseException {
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " "
                + TIME_FORMAT);

        return formater.parse(sDate);
    }
}
