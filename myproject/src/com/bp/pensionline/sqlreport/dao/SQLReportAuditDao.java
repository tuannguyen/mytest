/**
 * 
 */
package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.dto.db.ReportAudit;

/**
 * @author AS5920G
 *
 */
public class SQLReportAuditDao {
	public static final Log LOG = CmsLog.getLog(SQLReportAuditDao.class);
	
	public static boolean auditReportGroup(ReportAudit audit) {
		LOG.info("auditReportGroup():BEGIN");
		boolean isOK = true;
		String sql = "INSERT INTO SQLREPORT_AUDIT (event_date, event_hour, event_minute, event_second, " +
												 " username, firstname, lastname, description, fullname, " +
												 " usergroup, report_id, report_name, report_group_id, " +
												 " report_group_name, report_section, action) " +
					 "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try {
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(sql);
			pstm.setDate(1, audit.getEventDate());
			pstm.setInt(2, audit.getEventHour());
			pstm.setInt(3, audit.getEventMinute());
			pstm.setInt(4, audit.getEventSecond());
			pstm.setString(5, audit.getUserName());
			pstm.setString(6, audit.getFirstName());
			pstm.setString(7, audit.getLastName());
			pstm.setString(8, audit.getDescription());
			pstm.setString(9, audit.getFullName());
			pstm.setString(10, audit.getUserGroup());
			pstm.setString(11, audit.getReportId());
			pstm.setString(12, audit.getReportName());
			pstm.setString(13, audit.getReportGroupId());
			pstm.setString(14, audit.getReportGroupName());
			pstm.setString(15, audit.getReportSection());
			pstm.setString(16, audit.getAction());
			pstm.executeUpdate();
			con.commit();
			con.setAutoCommit(true);
			
		} catch (SQLException sle) {
			isOK = false;
			sle.printStackTrace();
			LOG.error("EXCEPTION while auditing report group "+audit.getReportGroupName());
		} finally {
			if (con != null) {
				try {
					connector.close(con);
				} catch (Exception e) {
					LOG.error("Error in closing MS SQL connection: " + e.toString());
					isOK = false;
				}
			}
		}
		LOG.info("auditReportGroup():END");
		return isOK;
	}
	
	public static boolean auditReport(List<ReportAudit> audits) {
		LOG.info("auditReportGroup():BEGIN");
		boolean isOK = true;
		String sql = "INSERT INTO SQLREPORT_AUDIT (event_date, event_hour, event_minute, event_second, " +
												 " username, firstname, lastname, description, fullname, " +
												 " usergroup, report_id, report_name, report_group_id, " +
												 " report_group_name, report_section, action) " +
					 "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try {
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(sql);
			for (int i=0; i<audits.size(); i++) {
				ReportAudit audit = audits.get(i);
				pstm.setDate(1, audit.getEventDate());
				pstm.setInt(2, audit.getEventHour());
				pstm.setInt(3, audit.getEventMinute());
				pstm.setInt(4, audit.getEventSecond());
				pstm.setString(5, audit.getUserName());
				pstm.setString(6, audit.getFirstName());
				pstm.setString(7, audit.getLastName());
				pstm.setString(8, audit.getDescription());
				pstm.setString(9, audit.getFullName());
				pstm.setString(10, audit.getUserGroup());
				pstm.setString(11, audit.getReportId());
				pstm.setString(12, audit.getReportName());
				if (audit.getReportGroupId()!=null) {
					pstm.setString(13, audit.getReportGroupId());
				} else {
					pstm.setNull(13, java.sql.Types.NVARCHAR);
				}
				if (audit.getReportGroupName() != null) {
					pstm.setString(14, audit.getReportGroupName());
				} else {
					pstm.setNull(14, java.sql.Types.NVARCHAR);
				}
				pstm.setString(15, audit.getReportSection());
				pstm.setString(16, audit.getAction());
				pstm.addBatch();
			}
			pstm.executeBatch();
			con.commit();
			con.setAutoCommit(true);
			
		} catch (SQLException sle) {
			isOK = false;
			sle.printStackTrace();
			LOG.error("EXCEPTION while auditing report "+sle.toString());
		} finally {
			if (con != null) {
				try {
					connector.close(con);
				} catch (Exception e) {
					LOG.error("Error in closing MS SQL connection: " + e.toString());
					isOK = false;
				}
			}
		}
		LOG.info("auditReportGroup():END");
		return isOK;
	}
}
