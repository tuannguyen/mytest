package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.dto.db.User;

public class UserDao extends GenericDao{

	public UserDao(Connection connection) throws SQLException {
		super(connection);
	}
	
	public UserDao() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<User> getReportRunners(int minRunLevel){
		//TODO: Query all report-runners who has RunLevel > minRunLevel
		return null;
	}
}
