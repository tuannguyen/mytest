package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;

import com.bp.pensionline.sqlreport.dto.db.DownloadLog;
import com.bp.pensionline.sqlreport.util.LogUtil;

public class DownloadLogDao extends GenericDao {
	public static final Log log = LogUtil.getLog(ScheduleDao.class);
	
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "HH:mm:ss";
	
	public static final String COL_ID = "id";
	public static final String COL_RUNLOGID = "runlog_id";
	public static final String COL_DOWNLOADTIME = "download_time";
	public static final String COL_DOWNLOADBY = "download_by";

	public static final String INSERT;
	static{
		StringBuilder insert = new StringBuilder();
		insert.append("INSERT INTO sqlreport_downloadlog ");
		insert.append("(");
		insert.append(COL_RUNLOGID);
		insert.append(",").append(COL_DOWNLOADBY);
		insert.append(",").append(COL_DOWNLOADTIME);
		insert.append(") VALUES(?,?,?)");
		INSERT = insert.toString();
	}
	public static String formatDate(Date date){
		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
		return formater.format(date);
	}
	
	public DownloadLogDao(Connection connection) throws SQLException {
		super(connection);
	}

	public DownloadLogDao() throws SQLException {
		super();
	}

	public void insert(DownloadLog downloadlog) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(INSERT);
			ps.setLong(1, downloadlog.getRunLog().getId());
			ps.setString(2, downloadlog.getDownloadBy().getUserName());
			ps.setString(3, formatDate(downloadlog.getDownloadTime()));
			
			ps.executeUpdate();
		} finally{
			if(ps != null){
				ps.close();
			}
		}
	}	
}
