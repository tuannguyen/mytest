package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsUUID;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.util.SystemAccount;

public class ReportGroupDao
{
	public static final String COL_ID = "id";
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	public static boolean insertReportGroup(String groupName)
	{
		String groupId = new CmsUUID().toString();
		String insertReportGroupSQL = "insert into sqlreport_group " +
				"(id, name) " +
				"values (?, ?)";

		// validation
		if (groupName == null || groupName.trim().equals(""))
		{
			return false;
		}
		
		boolean result = true;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(insertReportGroupSQL);
			pstm.setString(1, groupId);
			pstm.setString(2, groupName);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in insertReportGroup: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in insertReportGroup rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return result;			
	}
	
	/**
	 * Get all report group
	 * @return
	 */
	public static Vector<ReportGroup> getAllReportGroups ()
	{
		String getAllReportgroupSQL = "select id, name from sqlreport_group";
		Vector<ReportGroup> results = new Vector<ReportGroup>();
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllReportgroupSQL);
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String id = rs.getString("id");
				String name = rs.getString("name");
				ReportGroup reportGroupDTO = new ReportGroup();
				reportGroupDTO.setId(id);
				reportGroupDTO.setName(name);
				
				results.add(reportGroupDTO);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getAllReportGroups: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return results;		
	}
	
	/**
	 * Get all report group
	 * @return
	 */
	public static Vector<ReportGroup> getReportGroupsOfUser (String userId)
	{
		String getAllReportgroupSQL = "select id, name from sqlreport_group g, sqlreport_usergroup ug " +
				"where g.id = ug.group_id and ug.user_id = ?";
		Vector<ReportGroup> results = new Vector<ReportGroup>();
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllReportgroupSQL);
			pstm.setString(1, userId);
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String id = rs.getString("id");
				String name = rs.getString("name");
				ReportGroup reportGroupDTO = new ReportGroup();
				reportGroupDTO.setId(id);
				reportGroupDTO.setName(name);
				
				results.add(reportGroupDTO);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getReportGroupsOfUser: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return results;		
	}	
	
	public static ReportGroup getReportGroupInfo (String reportGroupId)
	{
		String getAllReportgroupSQL = "select id, name from sqlreport_group where id = ?";
		ReportGroup reportGroupDTO = null;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllReportgroupSQL);
			pstm.setString(1, reportGroupId);
			ResultSet rs = pstm.executeQuery();				
			if (rs.next())
			{
				String id = rs.getString("id");
				String name = rs.getString("name");
				reportGroupDTO = new ReportGroup();
				reportGroupDTO.setId(id);
				reportGroupDTO.setName(name);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getReportGroupInfo: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return reportGroupDTO;		
	}	
	
	public static boolean updateReportGroup (String id, String name)
	{
		String updateReportGroupSQL = "Update sqlreport_group set name = ? " +
				"where id = '" + id + "'";

		// validation
		if (	id == null || id.trim().equals("") ||
				name == null || name.trim().equals(""))
		{
			return false;
		}
		
		boolean result = true;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(updateReportGroupSQL);
			pstm.setString(1, name);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in updateReportGroup: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in updateReportGroup rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return result;		
	}	
	
	/**
	 * Delete a report group
	 * @param groupId
	 */
	public static void removeReportGroup (String groupId)
	{
		String deleteMasterJRXMLSQL = "delete from sqlreport_group where id = ?";
		String deleteReportGroupLinks = "delete from sqlreport_reportgroup where report_group_id = ?";
		String deleteUserGroupLinks = "delete from sqlreport_usergroup where group_id = ?";

		// validation
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(deleteMasterJRXMLSQL);
			pstm.setString(1, groupId);
			pstm.executeUpdate();
			
			pstm = con.prepareStatement(deleteReportGroupLinks);
			pstm.setString(1, groupId);
			pstm.executeUpdate();
			
			pstm = con.prepareStatement(deleteUserGroupLinks);
			pstm.setString(1, groupId);			
			pstm.executeUpdate();	
			
			con.commit();			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in removeReportGroup: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in removeReportGroup rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}		
	}	
	
	/**
	 * Get all group id of a report
	 * @param reportId
	 * @return
	 */
	public static Vector<String> getGroupIdsOfReport (String reportId)
	{
		Vector<String> groupIds = new Vector<String>();
		String getGroupIdsOfReportSQL = "select report_group_id from sqlreport_reportgroup where report_id = ?";
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getGroupIdsOfReportSQL);
			pstm.setString(1, reportId);
			
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String groupId = rs.getString("report_group_id");				
				groupIds.add(groupId);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getGroupIdsOfReport: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
				
		return groupIds;
	}
	
	/**
	 * Add a this report to a group
	 * @param reportId
	 * @param groupId
	 * @return
	 */
	public static void addGroupToReport (String reportId, String groupId)
	{
		if (reportId != null && groupId != null)
		{
			String insertReportReportGroupSQL = "insert into sqlreport_reportgroup " +
					"(report_id, report_group_id) " +
					"values (?, ?)";
			Connection con = null;
			DBConnector connector = DBConnector.getInstance();
			try
			{
				con = connector.getDBConnFactory(Environment.PENSIONLINE);
				
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(insertReportReportGroupSQL);
				pstm.setString(1, reportId);
				pstm.setString(2, groupId);
				pstm.executeUpdate();			
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (SQLException sqle)
			{
				LOG.error("Error in addGroupToReport: " + sqle.toString());
				try
				{
					con.rollback();
				}
				catch (Exception e)
				{
					LOG.error("Error in insertReportGroup rollback: " + e.toString());
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);
					}
					catch (Exception e)
					{
						LOG.error("Error in closing MySQL connection: " + e.toString());
					}
				}
			}		
		}		
	}
	
	/**
	 * Remove all groups of a report
	 * @param reportId
	 */
	public static void removeAllGroupsOfReport (String reportId)
	{
		if (reportId != null)
		{
			String removeAllGroupsOfReportSQL = "delete from sqlreport_reportgroup " +
			"where report_id = ?";
			Connection con = null;
			DBConnector connector = DBConnector.getInstance();
			try
			{
				con = connector.getDBConnFactory(Environment.PENSIONLINE);
				
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(removeAllGroupsOfReportSQL);
				pstm.setString(1, reportId);
				pstm.executeUpdate();			
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (SQLException sqle)
			{
				LOG.error("Error in removeAllGroupsOfReport: " + sqle.toString());
				try
				{
					con.rollback();
				}
				catch (Exception e)
				{
					LOG.error("Error in removeAllGroupsOfReport rollback: " + e.toString());
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);
					}
					catch (Exception e)
					{
						LOG.error("Error in closing MySQL connection: " + e.toString());
					}
				}
			}				
		}
	}
	
	/**
	 * Remove all groups of a report
	 * @param reportId
	 */
	public static void removeAllReportsOfGroup (String groupId)
	{
		if (groupId != null)
		{
			String removeAllGroupsOfReportSQL = "delete from sqlreport_reportgroup " +
			"where report_group_id = ?";
			Connection con = null;
			DBConnector connector = DBConnector.getInstance();
			try
			{
				con = connector.getDBConnFactory(Environment.PENSIONLINE);
				
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(removeAllGroupsOfReportSQL);
				pstm.setString(1, groupId);
				pstm.executeUpdate();			
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (SQLException sqle)
			{
				LOG.error("Error in removeAllReportsOfGroup: " + sqle.toString());
				try
				{
					con.rollback();
				}
				catch (Exception e)
				{
					LOG.error("Error in removeAllReportsOfGroup rollback: " + e.toString());
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);
					}
					catch (Exception e)
					{
						LOG.error("Error in closing MySQL connection: " + e.toString());
					}
				}
			}				
		}
	}	
	
	/**
	 * Check if a report has been added to a group
	 * @param reportId
	 * @param groupId
	 * @return
	 */
	public static boolean isReportAddedToGroup (String reportId, String groupId)
	{
		boolean isAdded = false;
		
		String getReportReportGroupSQL = "select * from sqlreport_reportgroup where report_id = ? and report_group_id = ?";
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getReportReportGroupSQL);
			pstm.setString(1, reportId);
			pstm.setString(2, groupId);
			
			ResultSet rs = pstm.executeQuery();			
			if (rs.next())
			{
				isAdded = true;
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in isReportAddedToGroup: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}		
		return isAdded;
	}
	
	/**
	 * Return a list of CmsUser objects in the group
	 * @param cmsGroupName
	 * @return
	 */
	public static List<CmsUser> listAllUserInCmsGroup (String cmsGroupName)
	{
		List<CmsUser> usersOfGroup = new ArrayList<CmsUser>();
		CmsObject cmsObject = SystemAccount.getAdminCmsObject();
		if (cmsObject != null)
		{
			try
			{
				List usersOfGroupOrg = cmsObject.getUsersOfGroup(cmsGroupName);
				if (usersOfGroup != null)
				{
					for (int i = 0; i < usersOfGroupOrg.size(); i++)
					{
						CmsUser cmsUser = (CmsUser)usersOfGroupOrg.get(i);
						if (cmsUser.isEnabled())
						{
							usersOfGroup.add(cmsUser);
						}
					}
				}
			}
			catch (CmsException e)
			{
				LOG.error("Error in getting users of CMS group: " + e);
			}			
		}
		return usersOfGroup;
	}
	
	/**
	 * Check if userName is in the group with cmsGroupName
	 * @param cmsGroupName
	 * @return
	 */
	public static boolean isUserInCmsGroup (String userName, String cmsGroupName)
	{
		boolean isInGroup = false;
		CmsObject cmsObject = SystemAccount.getAdminCmsObject();
		if (cmsObject != null)
		{
			try
			{
				isInGroup = cmsObject.userInGroup(userName, cmsGroupName);
			}
			catch (CmsException e)
			{
				LOG.error("Error in getting users of CMS group: " + e);
			}			
		}
		
		return isInGroup;
	}	
	
	/**
	 * Add a this report to a group
	 * @param reportId
	 * @param groupId
	 * @return
	 */
	public static void addUserToReportGroup (String groupId, String userId)
	{
		if (groupId != null && userId != null)
		{
			String insertReportReportGroupSQL = "insert into sqlreport_usergroup " +
					"(user_id, group_id) " +
					"values (?, ?)";
			Connection con = null;
			DBConnector connector = DBConnector.getInstance();
			try
			{
				con = connector.getDBConnFactory(Environment.PENSIONLINE);
				
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(insertReportReportGroupSQL);
				pstm.setString(1, userId);
				pstm.setString(2, groupId);
				pstm.executeUpdate();			
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (SQLException sqle)
			{
				LOG.error("Error in addUserToReportGroup: " + sqle.toString());
				try
				{
					con.rollback();
				}
				catch (Exception e)
				{
					LOG.error("Error in addUserToReportGroup rollback: " + e.toString());
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);
					}
					catch (Exception e)
					{
						LOG.error("Error in closing MySQL connection: " + e.toString());
					}
				}
			}		
		}		
	}	
	
	/**
	 * Check if a report has been added to a group
	 * @param reportId
	 * @param groupId
	 * @return
	 */
	public static boolean isUserAddedToGroup (String userId, String groupId)
	{
		boolean isAdded = false;
		
		String getReportReportGroupSQL = "select * from sqlreport_usergroup where user_id = ? and group_id = ?";
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getReportReportGroupSQL);
			pstm.setString(1, userId);
			pstm.setString(2, groupId);
			
			ResultSet rs = pstm.executeQuery();			
			if (rs.next())
			{
				isAdded = true;
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in isUserAddedToGroup: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}		
		return isAdded;
	}	
	
	/**
	 * Remove all groups of a report
	 * @param groupId
	 */
	public static void removeAllUsersOfGroup (String groupId)
	{
		if (groupId != null)
		{
			String removeAllGroupsOfReportSQL = "delete from sqlreport_usergroup " +
			"where group_id = ?";
			Connection con = null;
			DBConnector connector = DBConnector.getInstance();
			try
			{
				con = connector.getDBConnFactory(Environment.PENSIONLINE);
				
				con.setAutoCommit(false);
				PreparedStatement pstm = con.prepareStatement(removeAllGroupsOfReportSQL);
				pstm.setString(1, groupId);
				pstm.executeUpdate();			
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (SQLException sqle)
			{
				LOG.error("Error in removeAllUsersOfGroup: " + sqle.toString());
				try
				{
					con.rollback();
				}
				catch (Exception e)
				{
					LOG.error("Error in removeAllUsersOfGroup rollback: " + e.toString());
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);
					}
					catch (Exception e)
					{
						LOG.error("Error in closing MySQL connection: " + e.toString());
					}
				}
			}				
		}
	}	

	/**
	 * Remove all groups of a report
	 * @param groupId
	 */
	public static void removeAllUsersOfGroups ()
	{
		String removeAllUserOfGroupsSQL = "delete from sqlreport_usergroup";
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(removeAllUserOfGroupsSQL);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in removeAllUsersOfGroups: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in removeAllUsersOfGroups rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}				
	}	
}
