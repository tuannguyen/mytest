package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.Schedule.Status;
import com.bp.pensionline.sqlreport.util.LogUtil;

public class ScheduleDao extends GenericDao
{
	public static final Log log = LogUtil.getLog(ScheduleDao.class);
	
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "HH:mm:ss";
	
	public static final String COL_ID = "id";
	public static final String COL_REPORTGROUPID = "reportgroup_id";
	public static final String COL_TITLE = "title";
	public static final String COL_NOTES = "notes";
	public static final String COL_CRONEXPR = "cron_expr";
	public static final String COL_OUTPUTFORMAT = "output_format";
	public static final String COL_STARTDATE = "start_date";
	public static final String COL_ENDDATE = "end_date";
	public static final String COL_RUNLEVEL = "run_level";
//	public static final String COL_CREATEDTIME = "created_time";
//	public static final String COL_CREATEDBY = "created_by";
	public static final String COL_STATUS = "status";
	public static final String COL_EMAILFROM = "email_from";

	public static final String SELECT_SCHEDULES_ACTIVE = "SELECT * FROM sqlreport_schedule WHERE status < 8";
	public static final String INSERT_SCHEDULE;
	public static final String UPDATE_SCHEDULE;
	
	static{
		StringBuilder insert = new StringBuilder();
		insert.append("INSERT INTO sqlreport_schedule ");
		insert.append("(");
		insert.append(COL_REPORTGROUPID);
		insert.append(",").append(COL_TITLE);
		insert.append(",").append(COL_CRONEXPR);
		insert.append(",").append(COL_OUTPUTFORMAT);
		insert.append(",").append(COL_STATUS);
		insert.append(",").append(COL_RUNLEVEL);
		insert.append(",").append(COL_NOTES);
		insert.append(",").append(COL_EMAILFROM);
		insert.append(",").append(COL_STARTDATE);
		insert.append(",").append(COL_ENDDATE);
//		insert.append(",").append(COL_CREATEDBY);
//		insert.append(",").append(COL_CREATEDTIME);
		insert.append(") VALUES(?,?,?,?,?,?,?,?,?,?)");
		INSERT_SCHEDULE = insert.toString();

		StringBuilder update = new StringBuilder();
		update.append("UPDATE sqlreport_schedule SET ");
		update.append(COL_REPORTGROUPID).append("=?");
		update.append(",").append(COL_TITLE).append("=?");
		update.append(",").append(COL_CRONEXPR).append("=?");
		update.append(",").append(COL_OUTPUTFORMAT).append("=?");
		update.append(",").append(COL_STATUS).append("=?");
		update.append(",").append(COL_RUNLEVEL).append("=?");
		update.append(",").append(COL_NOTES).append("=?");
		update.append(",").append(COL_EMAILFROM).append("=?");
		update.append(",").append(COL_STARTDATE).append("=?");
		update.append(",").append(COL_ENDDATE).append("=?");
//		update.append(",").append(COL_CREATEDBY).append("=?");
//		update.append(",").append(COL_CREATEDTIME).append("=?");
		update.append(" WHERE ").append(COL_ID).append("=?");
		UPDATE_SCHEDULE = update.toString();
	}

	public ScheduleDao() throws SQLException {
		super();
	}

	public ScheduleDao(Connection connection) throws SQLException {
		super(connection);
	}

	public void insertSchedule(Schedule schedule) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(INSERT_SCHEDULE);
			ps.setString(1, schedule.getReportGroup().getId());
			ps.setString(2, schedule.getTitle());
			ps.setString(3, schedule.getCronExpr());
			ps.setString(4, schedule.getOutputFormat());
			ps.setInt(5, schedule.getStatus().getValue());
			ps.setInt(6, schedule.getRunLevel());
			if(schedule.getNotes() == null){
				ps.setNull(7, Types.VARCHAR);
			} else{
				ps.setString(7, schedule.getNotes());
			}
			if(schedule.getEmailFrom() == null){
				ps.setNull(8, Types.VARCHAR);
			} else{
				ps.setString(8, schedule.getEmailFrom());
			}
			if(schedule.getStartDate() == null){
				ps.setNull(9, Types.VARCHAR);
			} else{
				ps.setString(9, formatDate(schedule.getStartDate()));
			}
			if(schedule.getEndDate() == null){
				ps.setNull(10, Types.VARCHAR);
			} else{
				ps.setString(10, formatDate(schedule.getEndDate()));
			}
//			ps.setString(9, schedule.getCreatedBy().getUserName());
//			ps.setString(10, formatDate(schedule.getCreatedTime()));
			
			ps.executeUpdate();
		} finally{
			if(ps != null){
				ps.close();
			}
		}
	}
	public void updateSchedule(Schedule schedule) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(UPDATE_SCHEDULE);
			ps.setString(1, schedule.getReportGroup().getId());
			ps.setString(2, schedule.getTitle());
			ps.setString(3, schedule.getCronExpr());
			ps.setString(4, schedule.getOutputFormat());
			ps.setInt(5, schedule.getStatus().getValue());
			ps.setInt(6, schedule.getRunLevel());
			if(schedule.getNotes() == null){
				ps.setNull(7, Types.VARCHAR);
			} else{
				ps.setString(7, schedule.getNotes());
			}
			if(schedule.getEmailFrom() == null){
				ps.setNull(8, Types.VARCHAR);
			} else{
				ps.setString(8, schedule.getEmailFrom());
			}
			if(schedule.getStartDate() == null){
				ps.setNull(9, Types.VARCHAR);
			} else{
				ps.setString(9, formatDate(schedule.getStartDate()));
			}
			if(schedule.getEndDate() == null){
				ps.setNull(10, Types.VARCHAR);
			} else{
				ps.setString(10, formatDate(schedule.getEndDate()));
			}
//			ps.setString(9, schedule.getCreatedBy().getUserName());
//			ps.setString(10, formatDate(schedule.getCreatedTime()));
			ps.setLong(11, schedule.getId());
			
			ps.executeUpdate();
		} finally{
			if(ps != null){
				ps.close();
			}
		}
	}
	public List<Schedule> getAllActiveSchedules() throws SQLException{
		ArrayList<Schedule> schedules = new ArrayList<Schedule>();
		Statement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			rs = statement.executeQuery(SELECT_SCHEDULES_ACTIVE);
			while(rs.next()){		
				Schedule schedule = new Schedule();
				schedule.setId(rs.getLong(COL_ID));
				schedule.setTitle(rs.getString(COL_TITLE));
				schedule.setCronExpr(rs.getString(COL_CRONEXPR));
				schedule.setRunLevel(rs.getInt(COL_RUNLEVEL));
				schedule.setOutputFormat(rs.getString(COL_OUTPUTFORMAT));
				schedule.setNotes(rs.getString(COL_NOTES));
				schedule.setStatus(Status.getStatus(rs.getInt(COL_STATUS)));
				schedule.setEmailFrom(rs.getString(COL_EMAILFROM));
				
				ReportGroup reportGroup = new ReportGroup();
				reportGroup.setId(rs.getString(COL_REPORTGROUPID));
				schedule.setReportGroup(reportGroup);

//				User user = new User();
//				user.setUserName(rs.getString(COL_CREATEDBY));
//				schedule.setCreatedBy(user);
				
				SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
				Date startDate = null;
				Date endDate = null;
//				Date createdTime = null;
				try {
					String sTmp = rs.getString(COL_STARTDATE);
					if(sTmp != null){
						startDate = format.parse(sTmp);
					}
					sTmp = rs.getString(COL_ENDDATE);
					if(sTmp != null){
						endDate = format.parse(sTmp);
					}
//					createdTime = format.parse(rs.getString(COL_CREATEDTIME));
				} catch (ParseException e) {
					log.error("Invalid date format in DB: " + e.getMessage());
					e.printStackTrace();
				}
				schedule.setStartDate(startDate);
				schedule.setEndDate(endDate);
//				schedule.setCreatedTime(createdTime);
				schedules.add(schedule);
			}
		} finally{
			if(rs != null){
				rs.close();
			}
			if(statement != null){
				statement.close();
			}
		}
		return schedules;
	}
	
	public String formatDate(Date date){
		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
		return formater.format(date);
	}
	
	public static void main(String[] args){
		Schedule schedule = new Schedule();
//		schedule.setCreatedBy(new User()); schedule.getCreatedBy().setUserName("Admin");
//		schedule.setCreatedTime(new Date());
		schedule.setCronExpr("1 1 1 1 1 2001");
		schedule.setEmailFrom("");
		schedule.setNotes("adfadsf");
		schedule.setOutputFormat("1");
		schedule.setReportGroup(new ReportGroup()); schedule.getReportGroup().setId("8c6ee09c-c07b-11dd-9ed2-4fa1f1c236bf");
		schedule.setRunLevel(50);
		schedule.setStatus(Status.STOP);
		schedule.setTitle("Test1");
		try {
			Connection connection = null;
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/pensionline_prd_ext", "sa", "admin");
				connection.setAutoCommit(false);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			Savepoint savepoint = connection.setSavepoint();
			ScheduleDao dao = new ScheduleDao(connection);
			dao.insertSchedule(schedule);
			connection.commit();
			connection.releaseSavepoint(savepoint);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
