package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bp.pensionline.sqlreport.dto.db.Recipient;
import com.bp.pensionline.sqlreport.dto.db.User;

public class RecipientDao extends GenericDao {
	public static final String COL_USERNAME = "username";
	public static final String COL_SCHEDULEID = "schedule_id";
	public static final String INSERT;
	public static final String DELETE_BY_SCHEDULE;
	public static final String SELECT_BY_SCHEDULE;
	
	static{
		StringBuilder insert = new StringBuilder();
		insert.append("INSERT INTO sqlreport_recipient (");
		insert.append(COL_SCHEDULEID);
		insert.append(",").append(COL_USERNAME);
		insert.append(") VALUES(?,?)");
		INSERT = insert.toString();
		
		StringBuilder delBySchedule = new StringBuilder("DELETE FROM sqlreport_recipient WHERE ");
		delBySchedule.append(COL_SCHEDULEID).append("=?");
		DELETE_BY_SCHEDULE = delBySchedule.toString();

		StringBuilder selBySchedule = new StringBuilder("SELECT ");
		selBySchedule.append(COL_USERNAME).append(" FROM sqlreport_recipient WHERE ");
		selBySchedule.append(COL_SCHEDULEID).append("=?");
		SELECT_BY_SCHEDULE = selBySchedule.toString();
	}
	
	public RecipientDao() throws SQLException {
		super();
	}

	public RecipientDao(Connection connection) throws SQLException {
		super(connection);
	}

	public void insert(Recipient recipient) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(INSERT);
			ps.setLong(1, recipient.getScheduleId());
			ps.setString(2, recipient.getUserName());
			ps.executeUpdate();
		} finally{
			if(ps != null){
				ps.close();
			}
		}
	}
	public void deleteAllOfSchedule(long scheduleId) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(DELETE_BY_SCHEDULE);
			ps.setLong(1, scheduleId);
			ps.executeUpdate();
		} finally{
			if(ps != null){
				ps.close();
			}
		}
	}
	public List<User> selectAllOfSchedule(long scheduleId) throws SQLException{
		PreparedStatement ps = null;;
		ResultSet rs = null;;
		List<User> receivers = new ArrayList<User>();
		try {
			ps = connection.prepareStatement(SELECT_BY_SCHEDULE);
			ps.setLong(1, scheduleId);
			rs = ps.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setUserName(rs.getString(COL_USERNAME));
				receivers.add(user);
			}
		} finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}
		return receivers;
	}
}
