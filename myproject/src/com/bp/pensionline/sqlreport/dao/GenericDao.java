package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

public abstract class GenericDao {
	protected Connection connection = null;
	
	public GenericDao(Connection connection) throws SQLException {
		this.connection = connection;
		this.connection.setAutoCommit(false);
	} 

	public GenericDao() throws SQLException {
		DBConnector connector = DBConnector.getInstance();
		connection = connector.getDBConnFactory(Environment.PENSIONLINE);
		connection.setAutoCommit(false);
	} 

	public Connection getConnection(){
		return connection;
	}
	public void closeConnection(){
		
	}
	public long getLastInsertId() throws SQLException{
		long id = -1;
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("SELECT @@IDENTITY;");
		if(rs.next()){
			id = rs.getLong(1);
		}
		rs.close();
		return id;
	}
}
