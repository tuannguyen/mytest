package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportSection;

/**
 * This class is used to manage JRXML file's metadata which is stored in sqlreport_master and sqlreport_section
 * tables in pensionline database
 * @author ASPIRE 5920G
 *
 */
public class ReportDao
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	/**
	 * Get all master report
	 * @return
	 */
	public static Vector<Report> getAllMasterReports ()
	{
		String getAllMasterJRXMLSQL = "select id, name, description from sqlreport_master";
		Vector<Report> results = new Vector<Report>();
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllMasterJRXMLSQL);
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String id = rs.getString("id");
				String title = rs.getString("name");
				String description = rs.getString("description");
				Report report = new Report();
				report.setReportId(id);
				report.setReportTitle(title);
				report.setReportDescription(description);
				
				results.add(report);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getAllMasterReport: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return results;		
	}
	
	/**
	 * Get all master report
	 * @return
	 */
	public static Vector<Report> getAllMasterReportsInGroup (String groupId)
	{
		String getAllMasterJRXMLInGroupSQL = "select m.id, m.name, m.description, m.datasource " +
				"from sqlreport_master m, sqlreport_reportgroup rg " +
				"where rg.report_id = m.id and rg.report_group_id = ?";
		Vector<Report> results = new Vector<Report>();
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllMasterJRXMLInGroupSQL);
			pstm.setString(1, groupId);
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String id = rs.getString("id");
				String title = rs.getString("name");
				String description = rs.getString("description");
				String datasource = rs.getString("datasource");
				Report report = new Report();
				report.setReportId(id);
				report.setReportTitle(title);
				report.setReportDescription(description);
				report.setDatasource(datasource);
				
				results.add(report);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getAllMasterReport: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return results;		
	}	
	
	/**
	 * Get a master report
	 * @param masterId
	 * @return
	 */
	public static Report getMasterReport (String masterId)
	{
		String getAllMasterJRXMLSQL = "select id, name, description, datasource from sqlreport_master " +
				"where id = '" + masterId + "'";
		Report report = null;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllMasterJRXMLSQL);
			ResultSet rs = pstm.executeQuery();			
			if (rs.next())
			{
				String id = rs.getString("id");
				String title = rs.getString("name");
				String description = rs.getString("description");
				String datasource = rs.getString("datasource");
				report = new Report();
				report.setReportId(id);
				report.setReportTitle(title);
				report.setReportDescription(description);
				report.setDatasource(datasource);	
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getMasterReport: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return report;		
	}	
	
	/**
	 * Get all section of a master report
	 * @return
	 */
	public static Vector<ReportSection> getAllSectionOfMaster (String masterId)
	{
		String getAllSectionJRXMLSQL = "select id, name, description, query from sqlreport_section where master_id = '" + masterId + "' " +
				"order by creation_time asc";
		Vector<ReportSection> results = new Vector<ReportSection>();
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getAllSectionJRXMLSQL);
			ResultSet rs = pstm.executeQuery();			
			while (rs.next())
			{
				String id = rs.getString("id");
				String title = rs.getString("name");
				String description = rs.getString("description");
				String query = rs.getString("query");
				
				ReportSection sectionDTO = new ReportSection();
				sectionDTO.setSectionId(id);
				sectionDTO.setSectionTitle(title);
				sectionDTO.setSectionDescription(description);
				sectionDTO.setSectionQuery(query);
				sectionDTO.setMasterId(masterId);
				
				results.add(sectionDTO);
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getAllSectionOfMaster: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return results;		
	}
	
	/**
	 * Insert a new master JRXML metadata set
	 * @param id
	 * @param name
	 * @param datasource
	 * @return
	 */
	public static boolean insertMasterJRXML (String id, String name, String description, String datasource)
	{
		long currentMillis = System.currentTimeMillis();
		String insertMasterJRXMLSQL = "insert into sqlreport_master " +
				"(id, name, description, datasource, creation_time) " +
				"values (?, ?, ?, ?, ?)";

		// validation
		if (	id == null || id.trim().equals("") ||
				name == null || name.trim().equals("") ||
				datasource == null || datasource.trim().equals(""))
		{
			return false;
		}
		
		boolean result = true;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(insertMasterJRXMLSQL);
			pstm.setString(1, id);
			pstm.setString(2, name);
			pstm.setString(3, description);
			pstm.setString(4, datasource);
			pstm.setLong(5, currentMillis);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in insertMasterJRXML: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in insertMasterJRXML rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return result;		
	}
	
	/**
	 * Update a new master JRXML metadata set
	 * @param id
	 * @param name
	 * @param datasource
	 * @return
	 */
	public static void updateMasterJRXML (String id, String name, String description, String datasource)
		throws SQLException
	{
		String updateMasterJRXMLSQL = "Update sqlreport_master set name = ?, description = ?, datasource = ? " +
				"where id = '" + id + "'";

		// validation
		if (	id == null || id.trim().equals("") ||
				name == null || name.trim().equals("") ||
				datasource == null || datasource.trim().equals(""))
		{
			throw new SQLException("Input parameter is null or empty!");
		}		
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(updateMasterJRXMLSQL);
			pstm.setString(1, name);
			pstm.setString(2, description);
			pstm.setString(3, datasource);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in updateMasterJRXML: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in updateMasterJRXML rollback: " + e.toString());
			}
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
			
			throw sqle;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
	}	
	
	/**
	 * Insert a section JRXML metadata set
	 * @param id
	 * @param name
	 * @param datasource
	 * @return
	 */
	public static void insertSectionJRXML (String id, String name, String description, String query,
			String masterId) throws SQLException
	{
		//LOG.info("insertSectionJRXML: " + name);
		long currentMillis = System.currentTimeMillis();
		String insertSectionJRXMLSQL = "insert into sqlreport_section " +
				"(id, name, description, query, master_id, creation_time) " +
				"values (?, ?, ?, ?, ?, ?)";

		// validation
		if (	id == null || id.trim().equals("") ||
				name == null || name.trim().equals("") ||
				masterId == null || masterId.trim().equals(""))
		{
			throw new SQLException("Input parameter is null or empty!");
		}
		
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(insertSectionJRXMLSQL);
			pstm.setString(1, id);
			pstm.setString(2, name);
			pstm.setString(3, description);
			pstm.setString(4, query);
			pstm.setString(5, masterId);
			pstm.setLong(6, currentMillis);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in insertSectionJRXML: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in insertSectionJRXML rollback: " + e.toString());
			}
			throw sqle;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}	
	}
	/**
	 * Get a section information of a master report
	 * @param sectionId
	 * @param masterId
	 * @return
	 */
	public static ReportSection getSectionInfo (String sectionId)
	{
		String getSectionOfMasterSQL = "select id, name, description, query, master_id from sqlreport_section " +
				"where id = '" + sectionId + "'";
		ReportSection sectionDTO = null;
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstm = con.prepareStatement(getSectionOfMasterSQL);
			ResultSet rs = pstm.executeQuery();			
			if (rs.next())
			{
				String id = rs.getString("id");
				String title = rs.getString("name");
				String description = rs.getString("description");
				String query = rs.getString("query");
				String masterId = rs.getString("master_id");
				
				sectionDTO = new ReportSection();
				sectionDTO.setSectionId(id);
				sectionDTO.setSectionTitle(title);
				sectionDTO.setSectionDescription(description);
				sectionDTO.setSectionQuery(query);
				sectionDTO.setMasterId(masterId);
				
			}
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in getSectionOfMaster: " + sqle.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return sectionDTO;				
	}
	
	/**
	 * Delete a master report
	 * @param masterId
	 */
	public static void removeMasterJRXML (String masterId)
	{
		String deleteMasterJRXMLSQL = "delete from sqlreport_master where id = ?";
		String deleteReportGroupLinkSQL = "delete from sqlreport_reportgroup where report_id = ?";

		// validation
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(deleteMasterJRXMLSQL);
			pstm.setString(1, masterId);
			pstm.executeUpdate();
			
			pstm = con.prepareStatement(deleteReportGroupLinkSQL);
			pstm.setString(1, masterId);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in removeMasterJRXML: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in removeMasterJRXML rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}		
	}
	
	/**
	 * Insert a section JRXML metadata set
	 * @param id
	 * @param name
	 * @param datasource
	 * @return
	 */
	public static void removeAllSectionJRXMLOfMaster (String masterId)
	{
		String deleteAllSectionOfMasterSQL = "delete from sqlreport_section where master_id = ?";

		// validation
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(deleteAllSectionOfMasterSQL);
			pstm.setString(1, masterId);
			pstm.executeUpdate();			
			con.commit();
			
			con.setAutoCommit(true);		
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in removeAllSectionJRXMLOfMaster: " + sqle.toString());
			try
			{
				con.rollback();
			}
			catch (Exception e)
			{
				LOG.error("Error in removeAllSectionJRXMLOfMaster rollback: " + e.toString());
			}
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
	}	
	
}
