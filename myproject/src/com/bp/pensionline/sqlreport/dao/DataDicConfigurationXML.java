package com.bp.pensionline.sqlreport.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.apache.commons.logging.Log;
import org.apache.xml.serialize.XMLSerializer;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.publishing.util.CmsPageUtil;
import com.bp.pensionline.sqlreport.dto.db.DataMappingDTO;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

public class DataDicConfigurationXML
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String DATA_SCHEMA_XML = "/system/modules/com.bp.pensionline.test_members/data_schema.xml";
	public static final String DATA_DICTIONARY_XML = "/system/modules/com.bp.pensionline.test_members/data_dictionary.xml";

	
	/**
	 * Get all table names from a selected DB
	 * @param dbName
	 * @return
	 */
	public static Vector<String> getTablesFromDB (String dbName)
	{
		Vector<String> tables = new Vector<String>();
						
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			XmlReader reader = new XmlReader();
			
			byte[] xmlBytes = reader.readFile(DATA_SCHEMA_XML);
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xmlBytes);

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList dbNodes = root.getChildNodes();	
			
			int nlength = dbNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node dbNode = dbNodes.item(i);
				if (dbNode != null && dbNode.getNodeType() == Node.ELEMENT_NODE)
				{
					//System.out.println("Database name: " + dbNode.getNodeName());	
					if (dbNode.getNodeName() != null && dbNode.getNodeName().equals(dbName))
					{
						// get the table list
						NodeList tableNodes = dbNode.getChildNodes();
						for (int j = 0; j < tableNodes.getLength(); j++)
						{
							Node tbNode = tableNodes.item(j);
							if (tbNode != null && tbNode.getNodeType() == Node.ELEMENT_NODE)
							{
								NamedNodeMap atts = tbNode.getAttributes();
								Node tableNameNode = atts.getNamedItem("name");
								//System.out.println("Table name: " + tableNameNode.getNodeValue());
								tables.add(tableNameNode.getNodeValue());
							}
						}
						
						break;
					}
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in get tables from schema XML: " + e.toString());
		}
		
		return tables;
	}
	
	/**
	 * Return a map object contain all mappings of attribute and alias in a table
	 * @param dbName
	 * @param tbName
	 * @return
	 */
	public static Vector<DataMappingDTO> getDataDicOfTable (String dbName, String tbName)
	{
		Vector<DataMappingDTO> attAliasMap = new Vector<DataMappingDTO>();
				
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			XmlReader reader = new XmlReader();
			
			byte[] xmlBytes = reader.readFile(DATA_DICTIONARY_XML);			
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xmlBytes);

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList dbNodes = root.getChildNodes();	
			
			int nlength = dbNodes.getLength();

			for (int i = 0; i < nlength; i++)
			{
				Node dbNode = dbNodes.item(i);
				if (dbNode != null && dbNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (dbNode.getNodeName() != null && dbNode.getNodeName().equals(dbName))
					{
						//	Check if table of DB exists in data_dictionary.xml
						NodeList tableNodes = dbNode.getChildNodes();
						for (int j = 0; j < tableNodes.getLength(); j++)
						{
							Node tbNode = tableNodes.item(j);
							if (tbNode != null && tbNode.getNodeType() == Node.ELEMENT_NODE)
							{
								NamedNodeMap atts = tbNode.getAttributes();
								Node tableNameNode = atts.getNamedItem("name");

								if (tableNameNode != null && 
										tableNameNode.getNodeValue() != null && tableNameNode.getNodeValue().equals(tbName))
								{
									// populate all attributes and alias in the table
									NodeList mappingNodes = tbNode.getChildNodes();
									for (int k = 0; k < mappingNodes.getLength(); k++)
									{
										Node mappingNode = mappingNodes.item(k);
										if (mappingNode != null && mappingNode.getNodeType() == Node.ELEMENT_NODE)
										{
											Node attNode = ((Element)mappingNode).getElementsByTagName("attribute").item(0);
											Node aliasNode = ((Element)mappingNode).getElementsByTagName("alias").item(0);
											Node desNode = ((Element)mappingNode).getElementsByTagName("description").item(0);
											
											
											DataMappingDTO mappingDTO = new DataMappingDTO(
													attNode.getTextContent(),
													aliasNode.getTextContent(),
													desNode.getTextContent());	
											
											attAliasMap.add(mappingDTO);
										}
									}
									
									return attAliasMap;
								}
							}
						}
						
						// table not exists in data_dictionary.xml, get attribute from DB and update data_dictionary.xml
						// with blank alias
						Connection connection = DBConnector.getInstance().establishConnection(dbName);
						
						if (connection != null)
						{							
							// get table attributes based on select * query
							String getAllAttsQuery = "Select * from " + tbName;
							//System.out.println("query: " + getAllAttsQuery);
							PreparedStatement pstm = connection.prepareStatement(getAllAttsQuery);						
							
							ResultSet rs = pstm.executeQuery();
							// Create a table Node
							Element tbNode = document.createElement("table");
							
							tbNode.setAttribute("name", tbName);
							// get column names from ResutlSet MetaData
							ResultSetMetaData rsMD = rs.getMetaData();
							
							int numCols = rsMD.getColumnCount();
							//LOG.info("numCols = " + numCols);
							for (int j = 0; j < numCols; j++)
							{								
								// Create new mapping element
								Element mappingElement = document.createElement("mapping");
								Element attElement = document.createElement("attribute");
								Element aliasElement = document.createElement("alias");
								aliasElement.setTextContent("");
								Element descElement = document.createElement("description");
								descElement.setTextContent("");
								
								String colName = rsMD.getColumnName(j + 1);
								
								attElement.setTextContent(colName);
								//System.out.println("colName: " + colName);
								
								// for mat the XML layout
								mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
								mappingElement.appendChild(attElement);
								mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
								mappingElement.appendChild(aliasElement);
								mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
								mappingElement.appendChild(descElement);
								mappingElement.appendChild(document.createTextNode("\n\t\t\t"));
								
								tbNode.appendChild(document.createTextNode("\n\t\t\t"));
								tbNode.appendChild(mappingElement);
								
								// put to result map 
								attAliasMap.add(new DataMappingDTO(colName, "", ""));
							}
							
							tbNode.appendChild(document.createTextNode("\n\t\t"));
							dbNode.appendChild(document.createTextNode("\t"));
							dbNode.appendChild(tbNode);
							dbNode.appendChild(document.createTextNode("\n\t"));						
							
							// update data_dictionary.xml
							XMLSerializer serializer = new XMLSerializer();
							ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
						    serializer.setOutputByteStream(outputByteStream);
						    serializer.serialize(document);
						    
						    writeCmsFile(outputByteStream.toByteArray(), DATA_DICTIONARY_XML);
						}
						
						break;
					}
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in get data dictionary from XML: " + e.toString());
		}
		
		return attAliasMap;
	}
	
	/**
	 * Update data dictionary of a table
	 * @param dbName
	 * @param tbName
	 * @param attAliasMap
	 * @return
	 */
	public static boolean updateDataDicOfTable (String dbName, String tbName, Vector<DataMappingDTO> mappings)
	{
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			XmlReader reader = new XmlReader();
			
			byte[] xmlBytes = reader.readFile(DATA_DICTIONARY_XML);		
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xmlBytes);

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList dbNodes = root.getChildNodes();	
			
			int nlength = dbNodes.getLength();

			for (int i = 0; i < nlength; i++)
			{
				Node dbNode = dbNodes.item(i);
				if (dbNode != null && dbNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (dbNode.getNodeName() != null && dbNode.getNodeName().equals(dbName))
					{
						//	Check if table of DB exists in data_dictionary.xml
						NodeList tableNodes = dbNode.getChildNodes();
						for (int j = 0; j < tableNodes.getLength(); j++)
						{
							Node tbNode = tableNodes.item(j);
							if (tbNode != null && tbNode.getNodeType() == Node.ELEMENT_NODE)
							{
								NamedNodeMap atts = tbNode.getAttributes();
								Node tableNameNode = atts.getNamedItem("name");

								if (tableNameNode != null && 
										tableNameNode.getNodeValue() != null && tableNameNode.getNodeValue().equals(tbName))
								{
									// update attributes and alias in the table
									Element tableElement = (Element)tbNode;
									
									//remove old mappings before update
									Node tableFirstC = tableElement.getFirstChild();
									while (tableFirstC != null)
									{
										tableElement.removeChild(tableFirstC);
										tableFirstC = tableElement.getFirstChild();
									}

									for (int k = 0; k < mappings.size(); k++)
									{
										DataMappingDTO mappingDTO = mappings.elementAt(k);										
										String attribute = mappingDTO.getAttribute();
										String alias = mappingDTO.getAlias();
										String description = mappingDTO.getDescription();
										
										// Create new mapping element
										Element mappingElement = document.createElement("mapping");
										Element attElement = document.createElement("attribute");
										attElement.setTextContent(attribute);
										Element aliasElement = document.createElement("alias");
										aliasElement.setTextContent(alias);
										Element descElement = document.createElement("description");
										descElement.setTextContent(description);
										
										mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
										mappingElement.appendChild(attElement);
										mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
										mappingElement.appendChild(aliasElement);
										mappingElement.appendChild(document.createTextNode("\n\t\t\t\t"));
										mappingElement.appendChild(descElement);
										mappingElement.appendChild(document.createTextNode("\n\t\t\t"));
										
										tbNode.appendChild(document.createTextNode("\n\t\t\t"));
										tableElement.appendChild(mappingElement);
									}
									
									tableElement.appendChild(document.createTextNode("\n\t\t"));
									
									
									// update data_dictionary.xml
									// update data_dictionary.xml
									XMLSerializer serializer = new XMLSerializer();
									ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
								    serializer.setOutputByteStream(outputByteStream);
								    serializer.serialize(document);
								    
								    writeCmsFile(outputByteStream.toByteArray(), DATA_DICTIONARY_XML);					
									
									break;									
									
								}
							}
						}						
					}
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in update data dictionary XML: " + e.toString());
			
			return false;
		}
		
		return true;
	}
	
	public static Vector<DataMappingDTO> listMappingByAlias (String dbName, int alphaIndex)
	{
		Vector<DataMappingDTO> attAliasMap = new Vector<DataMappingDTO>();
		
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		
		// build up Alphabet condition array
		String prefixChar = (new Character((char)('A' + alphaIndex))).toString();
		
		try
		{
			XmlReader reader = new XmlReader();
			
			byte[] xmlBytes = reader.readFile(DATA_DICTIONARY_XML);		
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xmlBytes);

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList dbNodes = root.getChildNodes();	
			
			int nlength = dbNodes.getLength();

			for (int i = 0; i < nlength; i++)
			{
				Node dbNode = dbNodes.item(i);
				if (dbNode != null && dbNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (dbNode.getNodeName() != null && dbNode.getNodeName().equals(dbName))
					{
						//	Check if table of DB exists in data_dictionary.xml
						NodeList tableNodes = dbNode.getChildNodes();
						for (int j = 0; j < tableNodes.getLength(); j++)
						{
							Node tbNode = tableNodes.item(j);
							if (tbNode != null && tbNode.getNodeType() == Node.ELEMENT_NODE)
							{			
								// find table name
								NamedNodeMap atts = tbNode.getAttributes();
								Node tableNameNode = atts.getNamedItem("name");
								String tableName = tableNameNode.getNodeValue();
								
								// populate all attributes and alias in the table
								NodeList mappingNodes = tbNode.getChildNodes();
								for (int k = 0; k < mappingNodes.getLength(); k++)
								{
									Node mappingNode = mappingNodes.item(k);
									if (mappingNode != null && mappingNode.getNodeType() == Node.ELEMENT_NODE)
									{
										Node attNode = ((Element)mappingNode).getElementsByTagName("attribute").item(0);
										Node aliasNode = ((Element)mappingNode).getElementsByTagName("alias").item(0);
										Node desNode = ((Element)mappingNode).getElementsByTagName("description").item(0);
										
										String attribute = attNode.getTextContent();
										String alias = aliasNode.getTextContent();
										String description = desNode.getTextContent();
										if (isStartedWith(alias, prefixChar))
										{											
											DataMappingDTO mappingDTO = new DataMappingDTO(attribute,
													alias, description);
											mappingDTO.setTable(tableName);
											
											attAliasMap.add(mappingDTO);											
										}

									}
								}
								
								return attAliasMap;
							}
						}						
						
						break;
					}
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in get data dictionary from XML: " + e.toString());
		}
		
		return attAliasMap;		
	}
	
	/**
	 * Check if aString is started with one of the values in prexArr
	 * @param aString
	 * @param prefix
	 * @return
	 */
	public static boolean isStartedWith (String aString, String prefix)
	{
		if (aString != null && prefix != null)
		{
			aString = aString.toUpperCase();
			if (prefix != null && aString.startsWith(prefix))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static void main(String[] args)
	{
//		//getTablesFromDB("webstats");
//		Vector<DataMappingDTO> attAliasMap = getDataDicOfTable("AQUILLA", "BASIC");		
//		for (int i = 0; i < attAliasMap.size(); i++)
//		{
//			DataMappingDTO mappingDTO = attAliasMap.elementAt(i);
//			System.out.println("attribute: " + mappingDTO.getAttribute());
//			mappingDTO.setAlias("Alias_" + i);
//			mappingDTO.setDescription("description_" + i + i);
//		}
		
		Vector<DataMappingDTO> attAliasMap = listMappingByAlias("AQUILLA", ('D' - 'A'));		
		for (int i = 0; i < attAliasMap.size(); i++)
		{
			DataMappingDTO mappingDTO = attAliasMap.elementAt(i);
			System.out.println("attribute: " + mappingDTO.getAttribute());
			System.out.println("alias: " + mappingDTO.getAlias());
		}		
		
//		Set<String> keys = attAliasMap.keySet();
//		int i = 0;
//		for (Iterator iter = keys.iterator(); iter.hasNext();)
//		{
//			i += 1;
//			String key = (String) iter.next();
//			attAliasMap.put(key, "Alias_" + i);
//		}
//		
//		updateDataDicOfTable("AQUILLA", "BASIC", attAliasMap);
	}	
	
	private static boolean writeCmsFile(byte[] data, String filename)
	{
		try
		{
			if (data != null && filename != null)
			{
				CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
				if (cmsAdminObj != null)
				{
					cmsAdminObj.getRequestContext().setSiteRoot("/");
					cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));				
					boolean exist = cmsAdminObj.existsResource(filename);
					
					//check if the file is exist or not
					if(exist)
					{
						//read file and update with new data
						CmsFile xmlFile = (CmsFile)cmsAdminObj.readFile(filename);	
						CmsLock lock = cmsAdminObj.getLock(filename);

						//	resource is unlocked, so lock it
						if (!lock.isNullLock())
						{							
							cmsAdminObj.changeLock(filename);
						}
						cmsAdminObj.lockResource(filename);
						xmlFile.setContents(data);
						cmsAdminObj.writeFile(xmlFile);
						cmsAdminObj.unlockResource(filename);						
						
						ArrayList<String> outputFiles = new ArrayList<String>();
						outputFiles.add(filename);
						CmsPageUtil.publishPage(cmsAdminObj, outputFiles);
						return true;
					}
				}				
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in updating file to OpenCms: " + e);
		}
		return false;
	}
}
