/**
 * 
 */
package com.bp.pensionline.sqlreport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.ScheduleAudit;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.dto.db.ScheduleAudit.Action;
import com.bp.pensionline.sqlreport.util.LogUtil;

/**
 * @author anhtuan
 *
 */
public class ScheduleAuditDao extends GenericDao {
	public static final Log log = LogUtil.getLog(ScheduleDao.class);

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "HH:mm:ss";

	public static final String COL_ID = "id";
	public static final String COL_SCHEDULEID = "schedule_id";
	public static final String COL_ACTION = "action";
	public static final String COL_ACTOR = "actor";
	public static final String COL_ACTIONTIME = "action_time";
	
	public static final String INSERT;
	public static final String SELECT_BY_SCHEDULE;
	public static final String SELECT_CREATOR_BY_SCHEDULE;
	public static final String SELECT_CREATION_BY_SCHEDULE;
	//public static final String SELECT_BY_USER;
	static{
		StringBuilder insert = new StringBuilder();
		insert.append("INSERT INTO sqlreport_schedule_audit (");
		insert.append(COL_SCHEDULEID);
		insert.append(",").append(COL_ACTOR);
		insert.append(",").append(COL_ACTION);
		insert.append(",").append(COL_ACTIONTIME);
		insert.append(") VALUES(?,?,?,?)");
		INSERT = insert.toString();
		
		StringBuilder selBySchedule = new StringBuilder("SELECT * FROM sqlreport_schedule_audit WHERE ");
		selBySchedule.append(COL_SCHEDULEID).append("=?");
		SELECT_BY_SCHEDULE = selBySchedule.toString();

		StringBuilder selCreatorBySchedule = new StringBuilder("SELECT ").append(COL_ACTOR);
		selCreatorBySchedule.append(" FROM sqlreport_schedule_audit WHERE ");
		selCreatorBySchedule.append(COL_ACTION).append("='").append(ScheduleAudit.Action.CREATE.getValue()).append("' AND ");
		selCreatorBySchedule.append(COL_SCHEDULEID).append("=?");
		SELECT_CREATOR_BY_SCHEDULE = selCreatorBySchedule.toString();

		StringBuilder selCreationBySchedule = new StringBuilder("SELECT * ");
		selCreationBySchedule.append(" FROM sqlreport_schedule_audit WHERE ");
		selCreationBySchedule.append(COL_ACTION).append("='").append(ScheduleAudit.Action.CREATE.getValue()).append("' AND ");
		selCreationBySchedule.append(COL_SCHEDULEID).append("=?");
		SELECT_CREATION_BY_SCHEDULE = selCreationBySchedule.toString();
	}
	
	public ScheduleAuditDao(Connection connection) throws SQLException {
		super(connection);
	}

	public void insert(ScheduleAudit audit) throws SQLException{
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(INSERT);
			ps.setLong(1, audit.getSchedule().getId());
			ps.setString(2, audit.getActor().getUserName());
			ps.setString(3, audit.getAction().getValue());
			ps.setString(4, formatDate(audit.getActionTime()));
			ps.executeUpdate();
		}  finally{
			if(ps != null){
				ps.close();
			}
		}
	}
	public String getUsernameOfScheduleCreator(long scheduleId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String username = null;
		try {
			ps = connection.prepareStatement(SELECT_CREATOR_BY_SCHEDULE);
			ps.setLong(1, scheduleId);
			rs = ps.executeQuery();
			if(rs.next()){
				username = rs.getString(COL_ACTOR);
			}
		} finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}
		return username;
	}
	public ScheduleAudit getCreationAuditOfSchedule(long scheduleId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ScheduleAudit audit = null;
		try {
			ps = connection.prepareStatement(SELECT_CREATION_BY_SCHEDULE);
			ps.setLong(1, scheduleId);
			rs = ps.executeQuery();
			if(rs.next()){
				audit = new ScheduleAudit();
				audit.setId(rs.getLong(COL_ID));
				
				audit.setActor(new User());
				audit.getActor().setUserName(rs.getString(COL_ACTOR));

				audit.setSchedule(new Schedule());
				audit.getSchedule().setId(scheduleId);
				
				audit.setAction(Action.getAction(rs.getString(COL_ACTION)));
				
				Date actionTime;
				try {
					actionTime = parseDate(rs.getString(COL_ACTIONTIME));
				} catch (ParseException e) {
					log.error("Invalid Date value of Schedule Audit record " + audit.getId());
					actionTime = null;
				}
				audit.setActionTime(actionTime);
			}
		} finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}
		return audit;
	}
	public List<ScheduleAudit> getAllAuditsOfSchedule(long scheduleId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ScheduleAudit> audits = new ArrayList<ScheduleAudit>();
		try {
			ps = connection.prepareStatement(SELECT_BY_SCHEDULE);
			ps.setLong(1, scheduleId);
			rs = ps.executeQuery();
			while(rs.next()){
				ScheduleAudit audit = new ScheduleAudit();
				audit.setActor(new User());
				audit.setSchedule(new Schedule());
				
				audit.setId(rs.getLong(COL_ID));
				audit.getActor().setUserName(rs.getString(COL_ACTOR));
				audit.setAction(Action.getAction(rs.getString(COL_ACTION)));
				audit.getSchedule().setId(scheduleId);
				
				Date actionTime;
				try {
					actionTime = parseDate(rs.getString(COL_ACTIONTIME));
				} catch (ParseException e) {
					log.error("Invalid Date value of Schedule Audit record " + audit.getId());
					actionTime = null;
				}
				audit.setActionTime(actionTime);
				audits.add(audit);
			}
		} finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}
		return audits;
	}
	
	public String formatDate(Date date){
		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
		return formater.format(date);
	}
	public Date parseDate(String sDate) throws ParseException{
		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
		return formater.parse(sDate);
	}
}
