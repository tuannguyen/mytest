package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.DataDicConfigurationXML;
import com.bp.pensionline.sqlreport.dto.db.DataMappingDTO;


public class DataDicAdminHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private static final long serialVersionUID = 1L;		
	
	private String ddAction = null;
	private String ddDBName = null;
	private String ddTable = null;
	private Vector<String> tableNames = null;
	private Vector<DataMappingDTO> attAliasMap = null;
	private Vector<DataMappingDTO> updateAttAliasMap = new Vector<DataMappingDTO>();

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		if (parseRequestXml(xml) && ddAction != null)
		{

			// getting the attribute and alias if ddAction is 'Update'
			if (ddAction.equals("Init"))
			{
				tableNames = DataDicConfigurationXML.getTablesFromDB(ddDBName);
				xmlResponse = buildXmlResponseForInitAction(ddDBName, tableNames);
			}
			else if (ddAction.equals("Fetch"))
			{
				attAliasMap = DataDicConfigurationXML.getDataDicOfTable(ddDBName, ddTable);
				xmlResponse = buildXmlResponseForFetchAction(ddDBName, ddTable, attAliasMap);
			}
			else if (ddAction.equals("Update"))
			{				
				boolean updateResult = DataDicConfigurationXML.updateDataDicOfTable(ddDBName, ddTable, updateAttAliasMap);
				xmlResponse = buildXmlResponseForUpdateAction(updateResult);
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}		
		
		out.print(xmlResponse);
		out.close();

	}
	
	public static Vector<DataMappingDTO> sortMappingByAlias (String dbName, int alphabetIndex)
	{
		return DataDicConfigurationXML.listMappingByAlias(dbName, alphabetIndex);
	}

	public String buildXmlResponseForInitAction(String dbName, Vector<String> tableNames) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <DD_DBName>").append(dbName).append("</DD_DBName>\n");
		buffer.append("     <DD_Tables>\n");
		if (tableNames != null)
		{
			for (int i = 0; i < tableNames.size(); i++)
			{
				buffer.append("     	<DD_Table>").append(tableNames.elementAt(i)).append("</DD_Table>\n");
			}
		}
		buffer.append("     </DD_Tables>\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseForFetchAction(String dbName, String tableName, Vector<DataMappingDTO> attAliasMap) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <DD_DBName>").append(dbName).append("</DD_DBName>\n");
		buffer.append("     <DD_Table>").append(tableName).append("</DD_Table>\n");
		buffer.append("     <DD_Mappings>\n");
		if (attAliasMap != null)
		{
			for (int i = 0; i < attAliasMap.size(); i++)
			{
				buffer.append("     	<DD_Mapping>\n");
				DataMappingDTO mappingDTO = attAliasMap.elementAt(i);
				String att = mappingDTO.getAttribute();
				String alias = mappingDTO.getAlias();
				String description = mappingDTO.getDescription();
				buffer.append("     		<attribute>").append(att).append("</attribute>\n");
				buffer.append("     		<alias>").append(alias).append("</alias>\n");
				buffer.append("     		<description>").append(description).append("</description>\n");
				buffer.append("     	</DD_Mapping>\n");
			}			
		}
		buffer.append("     </DD_Mappings>\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}

	public String buildXmlResponseForUpdateAction(boolean result) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <Result>").append(result).append("</Result>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}	
	
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");	
		
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_ACTION))
					{
						ddAction = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_DBNAME))
					{
						ddDBName = paramNode.getTextContent();					
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_TABLE))
					{
						//LOG.info(" getting ptbPageURI: " + paramNode.getTextContent());
						ddTable = paramNode.getTextContent();						
					}	
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_MAPPINGS))
					{
						// Get the mappings
						NodeList mappingNodeList = paramNode.getChildNodes();
						if (mappingNodeList != null)
						{
							updateAttAliasMap.removeAllElements();
							
							for (int j = 0; j < mappingNodeList.getLength(); j++)
							{
								Node mappingNode = mappingNodeList.item(j);
								if (mappingNode != null && mappingNode.getNodeType() == Node.ELEMENT_NODE 
										&& mappingNode.getNodeName().equals(Constant.AJAX_SQL_DD_MAPPING))
								{
									String attribute = null;
									String alias = null;
									String description = null;									
									// get attribute, alias and description
									NodeList detailNodeList = mappingNode.getChildNodes();
									if (detailNodeList != null)
									{
										for (int k = 0; k < detailNodeList.getLength(); k++)
										{
											Node detailNode = detailNodeList.item(k);
											if (detailNode != null && detailNode.getNodeType() == Node.ELEMENT_NODE 
													&& detailNode.getNodeName().equals("attribute"))
											{
												attribute = detailNode.getTextContent();
											}
											if (detailNode != null && detailNode.getNodeType() == Node.ELEMENT_NODE 
													&& detailNode.getNodeName().equals("alias"))
											{
												alias = detailNode.getTextContent();
											}
											if (detailNode != null && detailNode.getNodeType() == Node.ELEMENT_NODE 
													&& detailNode.getNodeName().equals("description"))
											{
												description = detailNode.getTextContent();
											}											
										}
									}
									
									DataMappingDTO mappingDTO = new DataMappingDTO(attribute, alias, description);
									
									updateAttAliasMap.add(mappingDTO);
								}
							}
						}
					}
				}
			}
			
			LOG.info("ddAction: " + ddAction);
			LOG.info("ddDBName: " + ddDBName);
			LOG.info("ddTable: " + ddTable);
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing com.bp.pensionline.reporting.handler.DataDicAdminHandler request XML: " + ex.toString());
		}

		return false;

	}	
	
	public void debugMappingDTOs ()
	{
		for (int i = 0; i < attAliasMap.size(); i++)
		{
			DataMappingDTO mappingDTO = updateAttAliasMap.elementAt(i);
			LOG.info("attribute: " + mappingDTO.getAttribute());
			LOG.info("alias: " + mappingDTO.getAlias());
			LOG.info("description: " + mappingDTO.getDescription());
		}		
	}
		

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
