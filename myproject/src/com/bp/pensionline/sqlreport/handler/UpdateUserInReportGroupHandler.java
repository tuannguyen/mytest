package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;


public class UpdateUserInReportGroupHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(UpdateUserInReportGroupHandler.class);
	
	private static final long serialVersionUID = 1L;		
	
	private String groupId = null;
	private Vector<String> userGroupIds = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		boolean isReloadPage = true;

		if (parseRequestXml(xml))
		{
			if (userGroupIds != null)
			{
				if (groupId == null || groupId.trim().equals(""))
				{
					ReportGroupDao.removeAllUsersOfGroups();
					for (int i = 0; i < userGroupIds.size(); i++)
					{
						String userGroupId = userGroupIds.elementAt(i);
						//LOG.info("userGroupId: " + userGroupId);
						if (userGroupId != null && userGroupId.indexOf("_") > 0)
						{
							String userId = userGroupId.substring(0, userGroupId.indexOf("_"));
							String groupId = userGroupId.substring(userGroupId.indexOf("_") + 1);
							ReportGroupDao.addUserToReportGroup(groupId, userId);
						}					
					}
					actionResult = true;
				}
				else 
				{
					ReportGroupDao.removeAllUsersOfGroup(groupId);
					for (int i = 0; i < userGroupIds.size(); i++)
					{
						String userId = userGroupIds.elementAt(i);
						//LOG.info("userGroupId: " + userGroupId);
						if (userId != null)
						{
							ReportGroupDao.addUserToReportGroup(groupId, userId);
						}					
					}
					actionResult = true;
				}
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, isReloadPage);
		
		out.print(xmlResponse);
		out.close();

	}
	
	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <RpActionResult>").append(isActionOk).append("</RpActionResult>\n");
		buffer.append("     <RpPageReload>").append(willReloadPage).append("</RpPageReload>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		LOG.info("parseRequestXml xml: " + xml);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{										
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUP_ID))
					{
						this.groupId = paramNode.getTextContent();
					}							
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUP_USERS))
					{
						userGroupIds = new Vector<String>();
						NodeList groupIdNodes = ((Element)paramNode).getElementsByTagName(Constant.AJAX_SQL_REPORTGROUP_USER_ID);
						for (int j = 0; j < groupIdNodes.getLength(); j++)
						{
							Node groupIdNode = groupIdNodes.item(j);
							if (groupIdNode != null)
							{
								userGroupIds.add(groupIdNode.getTextContent());
							}
						}
					}										
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		
		return false;

	}	
	
	/**
	 * @return
	 */
	public static boolean isUserAddedToGroup(String userId, String groupId)
	{
		return ReportGroupDao.isUserAddedToGroup(userId, groupId);
	}	
	
	/**
	 * @return
	 */
	public static boolean isUserInCmsGroup(String userName, String cmsGroupName)
	{
		return ReportGroupDao.isUserInCmsGroup(userName, cmsGroupName);
	}	
		
	public static List getUsersOfCmsGroup(String cmsGroupName)
	{
		return ReportGroupDao.listAllUserInCmsGroup(cmsGroupName);
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}	
}

