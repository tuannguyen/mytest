package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.app.jasper.PLSectionProducer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.util.SecurityFilter;
import com.bp.pensionline.sqlreport.util.SecurityFilterFactory;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

public class PreviewSectionThreadHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_XML_FOLDER = "D:/report_xml/";
	
	private static final long serialVersionUID = 1L;		
	
	private String datasource = null;
	private String sectionTitle = null;
	private String sectionQuery = null;
	private String sectionDescription = null;
	private String sectionOutput = Constant.AJAX_SQL_REPORT_OUTPUT_XLS;
	//private byte[] reportContent = null;
	private Vector<SQLSectionParameterDTO> sectionParams = new Vector<SQLSectionParameterDTO>();
	
	public static Vector<PLSectionProducer> sectionProducers = new Vector<PLSectionProducer>();
	private String action = "";

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		if (parseRequestXml(xml))
		{		
			LOG.info("Preview action: " + action);
			if (action != null && action.equals("PREVIEW"))
			{
	//			for (int i = 0; i < sectionParams.size(); i++)
	//			{
	//				SQLSectionParameterDTO param = sectionParams.elementAt(i);
	//				LOG.info("param name: " + param.getName());
	//				LOG.info("param type: " + param.getType());
	//				LOG.info("param value: " + param.getValue());
	//				LOG.info("param default value: " + param.getDefaultValue());
	//				LOG.info("param description: " + param.getDescription());
	//			}
				
				// valiade query first
				String errorMesage = null;
				if (datasource == null || datasource.trim().equals(""))
				{
					errorMesage = "No datasource specified";
				}
//				else // take too long time for preview. Drop this step
//				{
//					errorMesage = QueryAnalizer.checkQueryError(sectionQuery, datasource);
//				}
				
				if (errorMesage != null)
				{
					xmlResponse = buildXmlResponse(false, 2, errorMesage);
				}
				else
				{
					PLSectionProducer sectionProducer = new PLSectionProducer();
					sectionProducer.setUserSession(request.getSession());
					sectionProducer.setDbName(datasource);
					sectionProducer.setSectionTitle(sectionTitle);
					sectionProducer.setSectionDescription(sectionDescription);
					
					String currentUserSIDescription = RunSQLReportHandler.getCurrentUserSecurityIndicatorDescription(request);
					int currentUserSI = PLReportProducer.getReportSIMapping(currentUserSIDescription);
					sectionQuery = SecurityFilterFactory.createSecurityFilter(SecurityFilter.SQL_SECURITY_FILTER).filter(sectionQuery, currentUserSI);
					//LOG.info("sectionQuery preview: " + sectionQuery);
					sectionProducer.setSectionQuery(sectionQuery);
					sectionProducer.setSectionParams(sectionParams);
					sectionProducer.setOutputType(sectionOutput);
					
					//add to the thread list
					sectionProducers.add(sectionProducer);
					sectionProducer.start();
					
					xmlResponse = buildXmlResponse(true, 1, sectionProducer.getOutputType());
				}
			}
			else if (action != null && action.equals("LOOKING"))
			{
				// finding the producer of this user session
				String sessionId = request.getSession().getId();
				for (int i = 0; i < sectionProducers.size(); i++)
				{
					PLSectionProducer sectionProducer = sectionProducers.elementAt(i);
					String producerId = sectionProducer.getProducerSessionId();
					if (sessionId.equals(producerId))
					{
						if (sectionProducer.getStatus() == 1)
						{
							xmlResponse = buildXmlResponse(true, 1, sectionProducer.getOutputType());
						}
						else if (sectionProducer.getStatus() == 2)
						{
							xmlResponse = buildXmlResponse(true, 2, sectionProducer.getOutputType());
							sectionProducers.remove(sectionProducer);
						}
						else if (sectionProducer.getStatus() == 0)
						{
							xmlResponse = buildXmlResponse(false, 2, sectionProducer.getErrorMessage());
							sectionProducers.remove(sectionProducer);							
						}
					}
				}
			}
			else if (action != null && action.equals("STOP"))
			{
				// finding the producer of this user session
				String sessionId = request.getSession().getId();
				for (int i = 0; i < sectionProducers.size(); i++)
				{
					PLSectionProducer sectionProducer = sectionProducers.elementAt(i);
					String producerId = sectionProducer.getProducerSessionId();
					if (sessionId.equals(producerId))
					{
						LOG.info("action: " + action);				
						sectionProducer.stop();	
						LOG.info("stop preview thread: " + sectionProducer.getStatus());
						sectionProducers.remove(sectionProducer);
					}
				}
			}
		}
		else
		{
			xmlResponse = buildXmlResponseError("Error in parsing XML param.");
		}
		
		//request.getSession().setAttribute("SQLReportContent", reportContent);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, int status, String reportOutput) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <rp_actionResult>").append(isActionOk).append("</rp_actionResult>\n");
		buffer.append("     <rp_reportStatus>").append(status).append("</rp_reportStatus>\n");
		buffer.append("     <rp_reportOutput>").append(reportOutput).append("</rp_reportOutput>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{	
		// remove all elements before parsing
		sectionParams.removeAllElements();
		//LOG.info("parseRequestXml xml: " + xml);
				
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ACTION))
					{
						action = paramNode.getTextContent();	
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_DATASOURCE))
					{
						datasource = paramNode.getTextContent();	
					}		
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_OUTPUT))
					{
						sectionOutput = paramNode.getTextContent();
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_TITLE))
					{
						sectionTitle = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_QUERY))
					{
						sectionQuery = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_DESCRIPTION))
					{
						sectionDescription = paramNode.getTextContent();
					}					
					
					// getting section parameters
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAMS))
					{
						NodeList sectionParamNodes = paramNode.getChildNodes();
						if (sectionParamNodes != null)
						{
							for (int j = 0; j < sectionParamNodes.getLength(); j++)
							{
								Node sectionParamNode = sectionParamNodes.item(j);
								if (sectionParamNode != null && sectionParamNode.getNodeType() == Node.ELEMENT_NODE)
								{
									String name = null;
									int type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
									String value = null;
									String defaultValue = null;
									String description = null;
									// populate name, type, default value and description
									NodeList paramDetailNodes = sectionParamNode.getChildNodes();
									for (int k = 0; k < paramDetailNodes.getLength(); k++)
									{
										Node paramDetailNode = paramDetailNodes.item(k);
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_NAME))
										{
											name = paramDetailNode.getTextContent();
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE))
										{
											String typeStr = paramDetailNode.getTextContent();
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_STRING))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
											}
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_NUMBER))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_NUMERIC;
											}
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_DATE))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_DATE;
											}
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_VALUE))
										{
											value = paramDetailNode.getTextContent();
											
											if (type == SQLSectionParameterDTO.PARAMETER_TYPE_DATE && value != null && !value.trim().equals(""))
											{
												if (datasource != null && datasource.equals(Constant.DATASOURCE_WEBSTATS))
												{
													SimpleDateFormat defaultDateFormat = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
													Date dateValue = defaultDateFormat.parse(value);
													SimpleDateFormat mySQLDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
													value = mySQLDateFormat.format(dateValue);
												}
											}
										}									
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DEFAULTVALUE))
										{
											defaultValue = paramDetailNode.getTextContent();
											if (type == SQLSectionParameterDTO.PARAMETER_TYPE_DATE && defaultValue != null && !defaultValue.trim().equals(""))
											{
												if (datasource != null && datasource.equals(Constant.DATASOURCE_WEBSTATS))
												{
													SimpleDateFormat defaultDateFormat = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
													Date dateValue = defaultDateFormat.parse(defaultValue);
													SimpleDateFormat mySQLDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
													defaultValue = mySQLDateFormat.format(dateValue);
												}
											}											
											defaultValue = "\"" + defaultValue + "\"";
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DESCRIPTION))
										{
											description = paramDetailNode.getTextContent();
										}																		
									}
									
									// add param to sectionParams vector
									if (name != null && !name.trim().equals(""))
									{
										SQLSectionParameterDTO reportParameter = new SQLSectionParameterDTO(name, type, defaultValue, description);
										reportParameter.setValue(value);
										sectionParams.add(reportParameter);
									}
								}
							}
						}
					}
				}
			}

			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
			
		}

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
