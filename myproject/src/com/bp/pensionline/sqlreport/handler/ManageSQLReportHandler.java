package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsUUID;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportSection;
import com.bp.pensionline.sqlreport.dto.db.SQLReportExtractFieldDTO;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.exception.PLQueryErrorException;
import com.bp.pensionline.sqlreport.util.AuditingUtil;
import com.bp.pensionline.sqlreport.util.QueryAnalizer;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

public class ManageSQLReportHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_BASE_FOLDER = CheckConfigurationKey.getStringValue("report.base");
	public static final String REPORT_WEB_FOLDER = CheckConfigurationKey.getStringValue("report.web");
	
	public static final int REPORT_ACTION_ADD = 0;
	public static final int REPORT_ACTION_EDIT = 1;
	public static final int REPORT_ACTION_DELETE = 2;
	
	private static final long serialVersionUID = 1L;		
	
	private int reportAction = REPORT_ACTION_ADD;
	private String reportId = null;
	private Report report = null;

	// commont error message
	private String errorMessage = null;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		boolean isReloadPage = true;	
		errorMessage = null;
		if (parseRequestXml(xml))
		{
			// validate query first
			if (report != null)
			{				
				Vector<ReportSection> sections = report.getSections();
				for (int i = 0; i < sections.size(); i++)
				{
					ReportSection section = sections.elementAt(i);
					if (section != null)
					{
						String sectionQuery = section.getSectionQuery();

						try
						{
							SQLReportExtractFieldDTO[] queryFields = QueryAnalizer.checkQueryError(sectionQuery, report.getDatasource());
							section.setFields(queryFields);
						}
						catch (PLQueryErrorException pqe)
						{
							errorMessage = pqe.getMessage();
							LOG.error("Error in checking query: " + pqe.getMessage());
						}
						if (errorMessage != null)
						{	
							errorMessage = section.getSectionTitle() + ": " + errorMessage;						
							break;
						}
						
						// Huy added to remove Null columns
						section.setSectionQuery(QueryAnalizer.removeNullAsBlankColumns(sectionQuery));
						
					}
				}
			}
			
			if (errorMessage == null)
			{
				String action = "EDIT"; //used for auditing
				if (reportAction == REPORT_ACTION_ADD && report != null)
				{
					action = "CREATE";
					report.setReportId(new CmsUUID().toString());
					try
					{
						actionResult = PLReportProducer.addReportToFileSystem(report);
					}
					catch (JRException jre)
					{
						LOG.error("Error while adding report to file system: " + jre);
						errorMessage = jre.toString();
					}
					catch (PLQueryErrorException qee)
					{
						LOG.error("Query error of sections while adding report: " + qee);
						errorMessage = qee.toString();
					}				
					catch (Exception e) 
					{
						LOG.error("General error while adding report: " + e);
						errorMessage = e.toString();
					}
				}	
				else if (reportAction == REPORT_ACTION_EDIT && report != null)
				{
					action = "UPDATE";
					try
					{
						actionResult = PLReportProducer.updateReportToFileSystem(report);
					}
					catch (JRException jre)
					{
						LOG.error("Error while updating report to file system: " + jre);
						errorMessage = jre.toString();
					}
					catch (PLQueryErrorException qee)
					{
						LOG.error("Query error of sections while updating report: " + qee);
						errorMessage = qee.getMessage();
					}				
					catch (Exception e) 
					{
						LOG.error("General error while updating report: " + e);
						errorMessage = e.toString();
					}								
				}
				else if (reportAction == REPORT_ACTION_DELETE && reportId != null)
				{
					action = "DELETE";
					actionResult = PLReportProducer.deleteMasterReport(reportId);				
				}
				/**
				 * Auditing here
				 */
				try {
					AuditingUtil.auditReport(report, action, SystemAccount.getCurrentUser(request));
				} catch (Exception e) {
					LOG.error("EXCEPTION while auditing SQL reports "+e.toString());
				}
				
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		if (actionResult)
		{
			xmlResponse = buildXmlResponse(actionResult, isReloadPage);
		}
		else
		{
			xmlResponse = buildXmlResponseError(errorMessage);
		}
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <RpActionResult>").append(isActionOk).append("</RpActionResult>\n");
		buffer.append("     <RpPageReload>").append(willReloadPage).append("</RpPageReload>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error><![CDATA[").append(message).append("]]></Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		xml = xml.replaceAll("\\(question\\)", "?");
		//LOG.info("parseRequestXml xml: " + xml);
		
		String reportActionStr = null;
		String reportTitle = null;
		String reportDescription = null;
		String datasource = null;
		Vector<ReportSection> reportSections = new Vector<ReportSection>();

		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ACTION))
					{
						reportActionStr = paramNode.getTextContent();
						// get the action with this report
						if (reportActionStr != null)
						{
							try
							{
								reportAction = Integer.parseInt(reportActionStr);
							}
							catch (NumberFormatException nfe)
							{
								reportAction = -1;
							}
						}						
					}						
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ID))
					{
						this.reportId = paramNode.getTextContent();
						if (reportAction == REPORT_ACTION_DELETE)
						{
							return true;
						}
					}	
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_DATASOURCE))
					{
						datasource = paramNode.getTextContent();						
					}						
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_TITLE))
					{
						reportTitle = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_DESCRIPTION))
					{
						reportDescription = paramNode.getTextContent();
					}				
					
					// get the section details
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTIONS))
					{
						NodeList sections = paramNode.getChildNodes();
						for (int j = 0; j < sections.getLength(); j++)
						{
							Node section = sections.item(j);
							if (section != null && section.getNodeType() == Node.ELEMENT_NODE && 
									section.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION))
							{
								// populate the section
								NodeList sectionDetails = section.getChildNodes();	
								String sectionTitle = null;
								String sectionQuery = null;
								String sectionDescription = null;
								Vector<SQLSectionParameterDTO> sectionParams = new Vector<SQLSectionParameterDTO>();
								for (int k = 0; k < sectionDetails.getLength(); k++)
								{
									Node sectionDetail = sectionDetails.item(k);
									if (sectionDetail != null && sectionDetail.getNodeType() == Node.ELEMENT_NODE)
									{									
										
										if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_TITLE))
										{
											sectionTitle = sectionDetail.getTextContent();
										}
										if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_QUERY))
										{
											// Huy added 06/10/2010. Remove comments
											sectionQuery = QueryAnalizer.removeQueryCommnets(sectionDetail.getTextContent(), QueryAnalizer.QUERY_COMMENT_PREFIX);
										}
										if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_DESCRIPTION))
										{
											sectionDescription = sectionDetail.getTextContent();
										}	
										
										// getting section parameters
										if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAMS))
										{
											NodeList sectionParamNodes = sectionDetail.getChildNodes();
											for (int l = 0; l < sectionParamNodes.getLength(); l++)
											{
												Node sectionParamNode = sectionParamNodes.item(l);
												if (sectionParamNode != null && sectionParamNode.getNodeType() == Node.ELEMENT_NODE)
												{
													String name = null;
													int type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
													String value = null;
													String defaultValue = null;
													String description = null;
													// populate name, type, default value and description
													NodeList paramDetailNodes = sectionParamNode.getChildNodes();
													for (int m = 0; m < paramDetailNodes.getLength();m++)
													{
														Node paramDetailNode = paramDetailNodes.item(m);
														if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_NAME))
														{
															name = paramDetailNode.getTextContent().toUpperCase();
														}
														if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE))
														{
															String typeStr = paramDetailNode.getTextContent();
															if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_STRING))
															{
																type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
															}
															if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_NUMBER))
															{
																type = SQLSectionParameterDTO.PARAMETER_TYPE_NUMERIC;
															}
															if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_DATE))
															{
																type = SQLSectionParameterDTO.PARAMETER_TYPE_DATE;
															}
														}
														if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_VALUE))
														{
															value = paramDetailNode.getTextContent();
														}									
														if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DEFAULTVALUE))
														{
															defaultValue = "\"" + paramDetailNode.getTextContent() + "\"";
														}
														if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DESCRIPTION))
														{
															description = paramDetailNode.getTextContent();
														}																		
													}
													
													// add param to sectionParams vector
													if (name != null && !name.trim().equals(""))
													{
														SQLSectionParameterDTO reportParameter = new SQLSectionParameterDTO(name, type, defaultValue, description);
														reportParameter.setValue(value);
														sectionParams.add(reportParameter);
													}
												}
											}
										}
									}	
								}
								
								
								ReportSection sectionDTO = new ReportSection();
								//LOG.info("Section title: " + sectionTitle);
								sectionDTO.setSectionTitle(sectionTitle);
								sectionDTO.setSectionDescription(sectionDescription);
								sectionDTO.setSectionQuery(sectionQuery);
								sectionDTO.setParams(sectionParams);
								
								reportSections.add(sectionDTO);									
							}						
						}
					}					
				}
			}
			
			report = new Report();
			
			this.report.setReportId(this.reportId);
			this.report.setDatasource(datasource);
			this.report.setReportTitle(reportTitle);
			this.report.setReportDescription(reportDescription);
			this.report.setSections(reportSections);
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
			this.errorMessage = ex.toString();
		}
		
		return false;

	}	
	
	/**
	 * List all available reports and sort the results
	 * @return
	 */
	public static Vector listAvailableReports()
	{
		Vector<Report> reports = new Vector<Report>();
		
		// get all master reports from sqlreport_master
		Vector<Report> tmpReports = ReportDao.getAllMasterReports();
				
				
		// sort the file name based on alphabet order
		TreeSet<Report> reportTree = new TreeSet<Report>();
		
		for (int i = 0; i < tmpReports.size(); i++)
		{
			reportTree.add(tmpReports.elementAt(i));
			
		}
		
		Iterator<Report> reportTreeIter = reportTree.iterator();
		while (reportTreeIter.hasNext())
		{			
			reports.add(reportTreeIter.next());					
		}
		
		return reports;
	}	
	
	public static Vector listAvailableReportsByChar(char startChar)
	{
		Vector<Report> reportsByChar = new Vector<Report>();
		
		// sort the file name based on alphabet order
		TreeSet<Report> reportTree = new TreeSet<Report>();
		
		
		// get all master reports from sqlreport_master
		Vector<Report> tmpReports = ReportDao.getAllMasterReports();
		for (int i = 0; i < tmpReports.size(); i++)
		{
			Report tmpReport = tmpReports.elementAt(i);
			String reportName = tmpReport.getReportTitle();
			if (reportName != null)
			{
				char firstChar = reportName.trim().toUpperCase().charAt(0);
				if (firstChar == startChar)
				{
					reportTree.add(tmpReport);
				}
			}

		}
		
		Iterator<Report> reportTreeIter = reportTree.iterator();
		while (reportTreeIter.hasNext())
		{			
			reportsByChar.add(reportTreeIter.next());					
		}
		
		return reportsByChar;
	}	
	
	/**
	 * List all available reports and sort the results
	 * @return
	 */
	public static Vector listAllReportsInGroup(String groupId)
	{
		Vector<Report> reports = new Vector<Report>();
		
		// get all master reports from sqlreport_master
		Vector<Report> tmpReports = ReportDao.getAllMasterReportsInGroup(groupId);
				
				
		// sort the file name based on alphabet order
		TreeSet<Report> reportTree = new TreeSet<Report>();
		
		for (int i = 0; i < tmpReports.size(); i++)
		{
			reportTree.add(tmpReports.elementAt(i));
			
		}
		
		Iterator<Report> reportTreeIter = reportTree.iterator();
		while (reportTreeIter.hasNext())
		{			
			reports.add(reportTreeIter.next());					
		}
		
		return reports;
	}	
	
	/**
	 * List all section of a master report
	 * @return
	 */
	public static Vector listSectionsOfMaster(String reportId)
	{
		return ReportDao.getAllSectionOfMaster(reportId);
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
