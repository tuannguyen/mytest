package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.util.CheckConfigurationKey;

public class ManageReportGroupHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_BASE_FOLDER = CheckConfigurationKey.getStringValue("report.base");
	public static final String REPORT_WEB_FOLDER = CheckConfigurationKey.getStringValue("report.web");
	
	public static final int REPORTGROUP_ACTION_ADD = 0;
	public static final int REPORTGROUP_ACTION_EDIT = 1;
	public static final int REPORTGROUP_ACTION_DELETE = 2;
	
	private static final long serialVersionUID = 1L;		
	
	private int reportAction = REPORTGROUP_ACTION_ADD;
	private String reportGroupId = null;
	private String reportGroupName = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		boolean isReloadPage = true;

		if (parseRequestXml(xml))
		{
			if (reportAction == REPORTGROUP_ACTION_ADD)
			{
				if (ReportGroupDao.insertReportGroup(reportGroupName))
				{
					actionResult = true;
				}				
			}	
			else if (reportAction == REPORTGROUP_ACTION_EDIT)
			{
				if (ReportGroupDao.updateReportGroup(reportGroupId, reportGroupName))
				{
					actionResult = true;
				}
			}
			else if (reportAction == REPORTGROUP_ACTION_DELETE)
			{
				ReportGroupDao.removeReportGroup(reportGroupId);
				actionResult = true;				
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, isReloadPage);
		
		out.print(xmlResponse);
		out.close();

	}
	
	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <RpActionResult>").append(isActionOk).append("</RpActionResult>\n");
		buffer.append("     <RpPageReload>").append(willReloadPage).append("</RpPageReload>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		LOG.info("parseRequestXml xml: " + xml);
		
		String reportActionStr = null;

		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ACTION))
					{
						reportActionStr = paramNode.getTextContent();
						// get the action with this report
						if (reportActionStr != null)
						{
							try
							{
								reportAction = Integer.parseInt(reportActionStr);
							}
							catch (NumberFormatException nfe)
							{
								reportAction = -1;
							}
						}						
					}						
					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUP_ID))
					{
						this.reportGroupId = paramNode.getTextContent();
						if (reportAction == REPORTGROUP_ACTION_DELETE)
						{
							return true;
						}
					}							
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUP_NAME))
					{
						reportGroupName = paramNode.getTextContent();						
					}										
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		
		return false;

	}	
	
	/**
	 * List all available reports and sort the results
	 * @return
	 */
	public static Vector listAllReportGroups()
	{
		Vector<ReportGroup> reports = new Vector<ReportGroup>();
		
		// get all master reports from sqlreport_master
		Vector<ReportGroup> tmpReports = ReportGroupDao.getAllReportGroups();
				
				
		// sort the file name based on alphabet order
		TreeSet<ReportGroup> reportTree = new TreeSet<ReportGroup>();
		
		for (int i = 0; i < tmpReports.size(); i++)
		{
			reportTree.add(tmpReports.elementAt(i));
			
		}
		
		Iterator<ReportGroup> reportTreeIter = reportTree.iterator();
		while (reportTreeIter.hasNext())
		{			
			reports.add(reportTreeIter.next());					
		}
		
		return reports;
	}	
	
	/**
	 * List all available reports and sort the results
	 * @return
	 */
	public static Vector listReportGroupsOfUser(String userId)
	{
		Vector<ReportGroup> reports = new Vector<ReportGroup>();
		
		// get all master reports from sqlreport_master
		Vector<ReportGroup> tmpReports = ReportGroupDao.getReportGroupsOfUser(userId);
				
				
		// sort the file name based on alphabet order
		TreeSet<ReportGroup> reportTree = new TreeSet<ReportGroup>();
		
		for (int i = 0; i < tmpReports.size(); i++)
		{
			reportTree.add(tmpReports.elementAt(i));
			
		}
		
		Iterator<ReportGroup> reportTreeIter = reportTree.iterator();
		while (reportTreeIter.hasNext())
		{			
			reports.add(reportTreeIter.next());					
		}
		
		return reports;
	}	
	
	public static ReportGroup getReportGroupInfo (String reportGroupId)
	{
		return ReportGroupDao.getReportGroupInfo(reportGroupId);
	}
	
	
	/**
	 * List all section of a master report
	 * @return
	 */
	public static Vector listSectionsOfMaster(String reportId)
	{
		return ReportDao.getAllSectionOfMaster(reportId);
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}	
}
