package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.DataDicConfigurationXML;
import com.bp.pensionline.sqlreport.dto.db.DataMappingDTO;

public class QueryDesignerFetchAttributeHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
	
	private static final long serialVersionUID = 1L;			


	private String datasource = null;
	private String table = null;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		String errorMesage = null;

		if (parseRequestXml(xml))
		{
			if (datasource != null && !datasource.trim().equals("") && table != null && !table.trim().equals(""))
			{		
				LOG.info("datasource: " + datasource);
				LOG.info("table: " + table);
				Vector<DataMappingDTO> attAliasMappings = DataDicConfigurationXML.getDataDicOfTable(datasource, table);
				//LOG.info("attAliasMappings size: " + attAliasMappings.size());
				xmlResponse = buildXmlResponse(datasource, table, attAliasMappings);						
			}
			else
			{
				errorMesage = "No datasource  or table specified!";
				xmlResponse = buildXmlResponseError(errorMesage);
			}	
		}
		else
		{
			errorMesage = "Error in parsing XML!";
			xmlResponse = buildXmlResponseError(errorMesage);
		}
		//LOG.info("xmlResponse = " + xmlResponse);
		out.print(xmlResponse);
		out.close();

	}	

	public String buildXmlResponse(String dbName, String tableName, Vector<DataMappingDTO> attAliasMappings) 
	{
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <DD_DBName>").append(dbName).append("</DD_DBName>\n");
		buffer.append("     <DD_Table>").append(tableName).append("</DD_Table>\n");
		buffer.append("     <DD_AttAliasMappings>\n");
		if (attAliasMappings != null)
		{
			for (int i = 0; i < attAliasMappings.size(); i++)
			{
				buffer.append("<DD_AttAliasMapping>");
				DataMappingDTO mappingDTO = attAliasMappings.elementAt(i);
				buffer.append("<DD_Att>").append(mappingDTO.getAttribute()).append("</DD_Att>\n");
				buffer.append("<DD_Alias>").append(mappingDTO.getAlias()).append("</DD_Alias>\n");
				buffer.append("</DD_AttAliasMapping>\n");
			}
		}
		buffer.append("     </DD_AttAliasMappings>\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");	
		
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_DBNAME))
					{
						datasource = paramNode.getTextContent();					
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_DD_TABLE))
					{
						//LOG.info(" getting ptbPageURI: " + paramNode.getTextContent());
						table = paramNode.getTextContent();						
					}	
				}
			}			
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing com.bp.pensionline.reporting.handler.DataDicAdminHandler request XML: " + ex.toString());
		}

		return false;

	}		
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}