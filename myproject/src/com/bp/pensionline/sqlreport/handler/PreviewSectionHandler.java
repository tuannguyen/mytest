package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.app.poi.PLReportPOIProducer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dto.db.SQLReportExtractFieldDTO;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.exception.PLQueryErrorException;
import com.bp.pensionline.sqlreport.util.QueryAnalizer;
import com.bp.pensionline.sqlreport.util.SecurityFilter;
import com.bp.pensionline.sqlreport.util.SecurityFilterFactory;

public class PreviewSectionHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_XML_FOLDER = "D:/report_xml/";
	
	private static final long serialVersionUID = 1L;		
	
	private String datasource = null;
	private String sectionTitle = null;
	private String sectionQuery = null;
	private String sectionDescription = null;
	private String sectionOutput = Constant.AJAX_SQL_REPORT_OUTPUT_XLS;
	private byte[] reportContent = null;
	private Vector<SQLSectionParameterDTO> sectionParams = new Vector<SQLSectionParameterDTO>();
	
	// common error message
	private String errorMessage = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		errorMessage = null;
		if (parseRequestXml(xml))
		{		
//			LOG.info("section title: " + sectionTitle);
			LOG.info("section query: " + sectionQuery);
//			LOG.info("section description: " + sectionDescription);
//			for (int i = 0; i < sectionParams.size(); i++)
//			{
//				SQLSectionParameterDTO param = sectionParams.elementAt(i);
//				LOG.info("param name: " + param.getName());
//				LOG.info("param type: " + param.getType());
//				LOG.info("param value: " + param.getValue());
//				LOG.info("param default value: " + param.getDefaultValue());
//				LOG.info("param description: " + param.getDescription());
//			}
			
			// valiade query first
			SQLReportExtractFieldDTO[] queryFields = null;
			
			if (datasource == null || datasource.trim().equals(""))
			{
				errorMessage = "No datasource specified";
			}
			else
			{				
				try
				{
					queryFields = QueryAnalizer.checkQueryError(sectionQuery, datasource);				
				}
				catch (PLQueryErrorException pqe)
				{
					errorMessage = pqe.getMessage();
					LOG.error("Error in checking query: " + pqe.getMessage());
				}
			}
			
			if (errorMessage != null)
			{
				xmlResponse = buildXmlResponse(false, errorMessage);
			}
			else
			{
				String currentUserSIDescription = RunSQLReportHandler.getCurrentUserSecurityIndicatorDescription(request);
				int currentUserSI = PLReportProducer.getReportSIMapping(currentUserSIDescription);
				
				sectionQuery = QueryAnalizer.removeNullAsBlankColumns(sectionQuery);
				
				// Process all name columns to alias.
//				QueryUtilizer aliasUtil = new QueryUtilizer();
//				sectionQuery = aliasUtil.queryToAlias(sectionQuery);
//				
				sectionQuery = SecurityFilterFactory.createSecurityFilter(SecurityFilter.SQL_SECURITY_FILTER).filter(sectionQuery, currentUserSI);
								
				if (sectionOutput != null && sectionOutput.equals(Constant.AJAX_SQL_REPORT_OUTPUT_PDF))
				{
					
					reportContent = PLReportProducer.generateSectionPDF(datasource, sectionTitle, sectionDescription, sectionQuery, queryFields, sectionParams);				
				}
				else if (sectionOutput != null && sectionOutput.equals(Constant.AJAX_SQL_REPORT_OUTPUT_XLS))
				{
					// Try using native POI, not Jasper
					reportContent = PLReportProducer.generateSectionXLS(datasource, sectionTitle, sectionDescription, sectionQuery, queryFields, sectionParams);
//					PLReportPOIProducer plReportPOIProducer = new PLReportPOIProducer();
//					HSSFWorkbook workbook = plReportPOIProducer.generateReportSection(datasource, sectionQuery);
//					if (workbook != null)
//					{
//						//reportContent = workbook.getBytes();
//						
//						outputToFileSystem("section_" + System.currentTimeMillis() + ".xls", workbook);
//					}
					if (reportContent != null)
					{
						outputToFileSystem("section_" + sectionTitle + "_"+ System.currentTimeMillis() + ".xls", reportContent);
					}
				}
				
				if (reportContent != null)
				{
					xmlResponse = buildXmlResponse(true, sectionOutput);
				}
				else
				{
					xmlResponse = buildXmlResponseError("Error in previewing the report. Please make sure your query was exactly filled in!");
				}				
			}
		}
		else
		{
			xmlResponse = buildXmlResponseError(errorMessage);
		}
		
		request.getSession().setAttribute("SQLReportContent", reportContent);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, String reportOutput) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <rp_actionResult>").append(isActionOk).append("</rp_actionResult>\n");
		buffer.append("     <rp_reportOutput>").append(reportOutput).append("</rp_reportOutput>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{	
		// remove all elements before parsing
		sectionParams.removeAllElements();
		//LOG.info("parseRequestXml xml: " + xml);
				
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		xml = xml.replaceAll("\\(question\\)", "?");
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_DATASOURCE))
					{
						datasource = paramNode.getTextContent();	
					}		
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_OUTPUT))
					{
						sectionOutput = paramNode.getTextContent();
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_TITLE))
					{
						sectionTitle = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_QUERY))
					{
						sectionQuery = QueryAnalizer.removeQueryCommnets(paramNode.getTextContent(), QueryAnalizer.QUERY_COMMENT_PREFIX);
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_DESCRIPTION))
					{
						sectionDescription = paramNode.getTextContent();
					}					
					
					// getting section parameters
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAMS))
					{
						NodeList sectionParamNodes = paramNode.getChildNodes();
						if (sectionParamNodes != null)
						{
							for (int j = 0; j < sectionParamNodes.getLength(); j++)
							{
								Node sectionParamNode = sectionParamNodes.item(j);
								if (sectionParamNode != null && sectionParamNode.getNodeType() == Node.ELEMENT_NODE)
								{
									String name = null;
									int type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
									String value = null;
									String defaultValue = null;
									String description = null;
									// populate name, type, default value and description
									NodeList paramDetailNodes = sectionParamNode.getChildNodes();
									for (int k = 0; k < paramDetailNodes.getLength(); k++)
									{
										Node paramDetailNode = paramDetailNodes.item(k);
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_NAME))
										{
											name = paramDetailNode.getTextContent();
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE))
										{
											String typeStr = paramDetailNode.getTextContent();
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_STRING))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
											}
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_NUMBER))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_NUMERIC;
											}
											if (typeStr != null && typeStr.equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_DATE))
											{
												type = SQLSectionParameterDTO.PARAMETER_TYPE_DATE;
											}
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_VALUE))
										{
											value = paramDetailNode.getTextContent();
											
											if (type == SQLSectionParameterDTO.PARAMETER_TYPE_DATE && value != null && !value.trim().equals(""))
											{
												if (datasource != null && datasource.equals(Constant.DATASOURCE_WEBSTATS))
												{
													SimpleDateFormat defaultDateFormat = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
													Date dateValue = defaultDateFormat.parse(value);
													SimpleDateFormat mySQLDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
													value = mySQLDateFormat.format(dateValue);
												}
											}
										}									
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DEFAULTVALUE))
										{
											defaultValue = paramDetailNode.getTextContent();
											if (type == SQLSectionParameterDTO.PARAMETER_TYPE_DATE && defaultValue != null && !defaultValue.trim().equals(""))
											{
												if (datasource != null && datasource.equals(Constant.DATASOURCE_WEBSTATS))
												{
													SimpleDateFormat defaultDateFormat = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
													Date dateValue = defaultDateFormat.parse(defaultValue);
													SimpleDateFormat mySQLDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
													defaultValue = mySQLDateFormat.format(dateValue);
												}
											}											
											defaultValue = "\"" + defaultValue + "\"";
										}
										if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
												paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_DESCRIPTION))
										{
											description = paramDetailNode.getTextContent();
										}																		
									}
									
									// add param to sectionParams vector
									if (name != null && !name.trim().equals(""))
									{
										SQLSectionParameterDTO reportParameter = new SQLSectionParameterDTO(name, type, defaultValue, description);
										reportParameter.setValue(value);
										sectionParams.add(reportParameter);
									}
								}
							}
						}
					}
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
			this.errorMessage = ex.toString();
		}
		

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public void outputToFileSystem(String fileName, HSSFWorkbook workbook) throws IOException
	{
		File outputFile = new File(PLReportProducer.REPORT_TEMP_FOLDER + "XLS/" + fileName);
		FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
		workbook.write(fileOutputStream);
		
		fileOutputStream.close();
	}	
	
	public void outputToFileSystem(String fileName, byte[] reportContent) throws IOException
	{
		File outputFile = new File(PLReportProducer.REPORT_TEMP_FOLDER + "XLS/" + fileName);
		FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
		fileOutputStream.write(reportContent);
		fileOutputStream.flush();
		fileOutputStream.close();
	}	
}
