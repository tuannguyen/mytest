package com.bp.pensionline.sqlreport.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


public class QueryUploadHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private static final long serialVersionUID = 1L;		
	

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		// Check that we have a file upload request
		byte[] uploadedContentBytes = null;
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (isMultipart)
		{
			try
			{
//				 Create a factory for disk-based file items
				FileItemFactory factory = new DiskFileItemFactory();

//				 Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);
//				upload.setFileSizeMax(2*1024*1024);
				upload.setHeaderEncoding("text/plain");
//				 Parse the request
				List  items = upload.parseRequest(request);	
				
//				 Process the uploaded items
				Iterator iter = items.iterator();
				while (iter.hasNext()) 
				{
				    FileItem item = (FileItem) iter.next();
				    if (!item.isFormField()) 
				    {
				        uploadedContentBytes = item.get();
				    }
				}			
			}
			catch (FileUploadException e)
			{
				LOG.error("Upload file failed: " + e);
			}
		}

		
		xmlResponse = buildXmlResponse(uploadedContentBytes);
		
		out.print(xmlResponse);
		out.close();

	}
	
	public String buildXmlResponse(byte[] uploadedContentBytes) 
	{
		String content = "";
		if (uploadedContentBytes != null)
		{
			try
			{
				content = new String(uploadedContentBytes, "UTF-8");
				
				// the content will be displayed in a text area, it is necessary to replace white space with \t\n\r
//				if (content != null)
//				{
//					content = content.replaceAll("\n", "\\\\n");
//					content = content.replaceAll("\t", "\\\\t");
//				}
			}
			catch (UnsupportedEncodingException e)
			{
				LOG.error("The file upload is not valid: " + e);
			}
		}

		return content;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}	
}

