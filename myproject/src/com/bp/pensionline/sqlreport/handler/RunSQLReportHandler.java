package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.ReportSection;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.util.AuditingUtil;
import com.bp.pensionline.util.SystemAccount;

public class RunSQLReportHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	
	private static final long serialVersionUID = 1L;		
	
	private String reportGroupId = null;
	private Vector<Report> runningReports = null;
	private String reportOutput = Constant.AJAX_SQL_REPORT_OUTPUT_XLS;
	private String reportRunner = "";
	private String reportStartDate = "";
	private String reportEndDate = "";
	
	// this output will be stored in session to avoid file creations
	byte[] reportContent = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;		
		

		if (parseRequestXml(xml) && runningReports != null && runningReports.size() > 0)
		{

			String groupName = "";
			ReportGroup reportGroupDTO = ManageReportGroupHandler.getReportGroupInfo(reportGroupId);
			if (reportGroupDTO != null)
			{
				groupName = reportGroupDTO.getName();
			}
			
			String siDescription = getCurrentUserSecurityIndicatorDescription(request);
			LOG.info("Report runner security description: " + siDescription);
			if (siDescription == null || siDescription.trim().equals(""))
			{
				String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: " + 
					"Your membership sensitivity setting is not recognised by the system. Please see a member of the PensionLine support team";
				xmlResponse = buildXmlResponseError(msg);
				out.print(xmlResponse);
				out.close();
				return;
			}	
			
			if (reportOutput != null && reportOutput.equals(Constant.AJAX_SQL_REPORT_OUTPUT_PDF))
			{

				try 
				{
					reportContent = PLReportProducer.generateRootReportPDF(reportGroupId, runningReports, 
							reportRunner, reportStartDate, reportEndDate, siDescription);
					long time = System.currentTimeMillis();
					if (reportContent != null)
					{
						xmlResponse = buildXmlResponse(true, "PDF", time);
					}
					else
					{
						
						String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON:" + " Error in generating PDF report.";
						xmlResponse = buildXmlResponseError(msg);
					}
				} 
				catch (Exception e) 
				{										
					String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: " + e.getMessage().toString();
					xmlResponse = buildXmlResponseError(msg);
					request.getSession().removeAttribute("SQLReportContent");
					out.print(xmlResponse);
					out.close();
				}
				
			}
			else if (reportOutput != null && reportOutput.equals(Constant.AJAX_SQL_REPORT_OUTPUT_XLS))
			{
				try {
					reportContent = PLReportProducer.generateRootReportXSL(reportGroupId, runningReports, 
							reportRunner, reportStartDate, reportEndDate, siDescription);
					long time = System.currentTimeMillis();
					if (reportContent != null)
					{
						xmlResponse = buildXmlResponse(true, "XLS", time);
						outputToFileSystem("pensionline_report.xls", reportContent);
					}
					else
					{
						String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \n FAILURE REASON:" + " Error in generating XSL report.";
						xmlResponse = buildXmlResponseError(msg);
					}	
				} catch (Exception e) {

					String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON:" + e.getMessage().toString();
					xmlResponse = buildXmlResponseError(msg);
					request.getSession().removeAttribute("SQLReportContent");
					out.print(xmlResponse);
					out.close();
				}
			}
			
			/**
			 * Auditing report here
			 */
			try {
				AuditingUtil.auditReport(runningReports, "RUN", SystemAccount.getCurrentUser(request));
			} catch (Exception e) {
				LOG.error("EXCEPTION while audting reports: "+e.toString());
				//e.printStackTrace();
			}
			
		}
		else
		{
			LOG.info("Error in parsing request XML");
			String msg = "REPORT {report group name}, " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: Error while parsing input parameters!";
			xmlResponse = buildXmlResponseError(msg);
		}
		
		request.getSession().setAttribute("SQLReportContent", reportContent);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, String outputType, long time) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <rp_actionResult>").append(isActionOk).append("</rp_actionResult>\n");
		buffer.append("     <rp_reportOutput>").append(outputType).append("</rp_reportOutput>\n");
		buffer.append("     <rp_reportTime>").append(time).append("</rp_reportTime>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{
		runningReports = new Vector<Report>();
		
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		xml = xml.replaceAll("\\(question\\)", "?");		
		LOG.info("parseRequestXml xml: " + xml);		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{	
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUP_ID))
					{
						reportGroupId = paramNode.getTextContent();
					}						
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_RUNNER))
					{
						reportRunner = paramNode.getTextContent();
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_OUTPUT))
					{
						reportOutput = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_STARTDATE))
					{
						reportStartDate = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ENDDATE))
					{
						reportEndDate = paramNode.getTextContent();
					}					
					// get the report running input parameters
					else if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT))
					{
						Report report = null;
						Vector<ReportSection> sectionDTOs = new Vector<ReportSection>();
						
						NodeList reportDetails = paramNode.getChildNodes();
						for (int j = 0; j < reportDetails.getLength(); j++)
						{
							Node reportDetail = reportDetails.item(j);							
							if (reportDetail != null && reportDetail.getNodeType() == Node.ELEMENT_NODE && 
									reportDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_ID))
							{
								String reportId = reportDetail.getTextContent();
								//LOG.info("reportId: " + reportId);
								report = ReportDao.getMasterReport(reportId);
								
								if (report == null)
								{
									LOG.error("Report with id " + reportId + " doesn't exist!;");
								}								
							}
//							 get the section details
							if (reportDetail != null && reportDetail.getNodeType() == Node.ELEMENT_NODE && 
									reportDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTIONS))
							{
								NodeList sections = reportDetail.getChildNodes();
								//LOG.info("Number of sections: " + sections.getLength());
								for (int k = 0; k < sections.getLength(); k++)
								{
									Node section = sections.item(k);
									if (section != null && section.getNodeType() == Node.ELEMENT_NODE && 
											section.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION))
									{
										// populate section id and its parameters
										NodeList sectionDetails = section.getChildNodes();
										ReportSection sectionDTO = new ReportSection();
										Vector<SQLSectionParameterDTO> sectionParams = new Vector<SQLSectionParameterDTO>();
										for (int l = 0; l < sectionDetails.getLength(); l++)
										{
											Node sectionDetail = sectionDetails.item(l);
											if (sectionDetail != null && sectionDetail.getNodeType() == Node.ELEMENT_NODE)
											{																					
												if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_ID))
												{
													String sectionId = sectionDetail.getTextContent();
													//LOG.info("sectionId: " + sectionId);
													sectionDTO.setSectionId(sectionId);
													
													// must query db for section title
													ReportSection sectionInfo = ReportDao.getSectionInfo(sectionId);	
													if (sectionInfo != null)
													{
														sectionDTO.setSectionTitle(sectionInfo.getSectionTitle());
													}
												}												
												
												// getting section parameters
												if (sectionDetail.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAMS))
												{
													NodeList sectionParamNodes = sectionDetail.getChildNodes();
													for (int m = 0; m < sectionParamNodes.getLength(); m++)
													{
														Node sectionParamNode = sectionParamNodes.item(m);
														if (sectionParamNode != null && sectionParamNode.getNodeType() == Node.ELEMENT_NODE &&
																sectionParamNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM))
														{
															String name = null;
															String value = null;
															int type = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
															// populate name, type, default value and description
															NodeList paramDetailNodes = sectionParamNode.getChildNodes();
															for (int n = 0; n < paramDetailNodes.getLength();n++)
															{
																Node paramDetailNode = paramDetailNodes.item(n);
																if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																		paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_NAME))
																{
																	name = paramDetailNode.getTextContent();
																}
																if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																		paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_VALUE))
																{
																	value = paramDetailNode.getTextContent();													
																}
																if (paramDetailNode != null && paramDetailNode.getNodeType() == Node.ELEMENT_NODE && 
																		paramDetailNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE))
																{
																	String typeStr = paramDetailNode.getTextContent();
																	try
																	{
																		type = Integer.parseInt(typeStr);
																	}
																	catch (NumberFormatException nfe)
																	{
																		LOG.error ("Running report: param type not valid! " + typeStr);
																	}												
																}																
															}
															
															// add param to sectionParams vector
															if (name != null && !name.trim().equals(""))
															{
																SQLSectionParameterDTO reportParameter = new SQLSectionParameterDTO();
																reportParameter.setName(name);
																reportParameter.setValue(value);
																reportParameter.setType(type);
																sectionParams.add(reportParameter);
															}
														}
													}
													
													sectionDTO.setParams(sectionParams);
												}
											}
										}
										
										// add the section
										sectionDTOs.add(sectionDTO);
									}						
								}
							}
						}
						
						// set sections to the report
						if (report != null)
						{
							report.setSections(sectionDTOs);							
							runningReports.add(report);
						}
					}															
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		
		return false;

	}
	
	/**
	 * Get the security indicator of current user. If user doesn't have one, return 0.
	 * Note: not return -1
	 * @param request
	 * @return
	 */
	public static String getCurrentUserSecurityIndicatorDescription (HttpServletRequest request)
	{
		String siDescription = "";	// Default to all report runner
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		if(currentUser != null)
		{			
			siDescription = getUserSecurityIndicatorDescription(currentUser);
		}
		
		LOG.info("Preview section: Member SI: " + siDescription);
		
		return siDescription;
	}	
	
	/**
	 * Get the security indicator of current user. If user doesn't have one, return 0.
	 * Note: not return -1
	 * @param request
	 * @return
	 */
	public static String getUserSecurityIndicatorDescription (CmsUser cmsUser)
	{
		String siDescription = "";	// Default to all report runner
		if(cmsUser != null && cmsUser.getDescription() != null)
		{			
			String accDescription = String.valueOf(cmsUser.getDescription()).trim(); 
			
			LOG.info("getCurrentUserSecurityIndicatorDescription: " + cmsUser.getName() + " has description: " + accDescription);
			
			if (accDescription == null || accDescription.trim().equals(""))
			{
				return "";
			}
			
			try {
				// Modified by Huy due to Jira: http://www.c-mg.info/jira/browse/REPORTING-96
				
				/*
				 * 	When setting up a user for superuser and reporting duties, the description field is used to determine the access level to member records - Default (standard records), 
				 * 	Execs, Senior Execs and Team. In the current design a blank description means the user has no level at all. Also, any whitespace means the user has no access level 
				 * 	either. 
					It was the original intention, that the decription would be trimmed of all white space and then compared with the level mapper. It was also the intention that after 
					trimming, if the description was an empty string "Default" would be assumed. 
					While in here, can we please add a new feature. As well as the above tokens, can we add in a user alignment token. for example "Aquila:BPJOHNSN" (without the quotes) 
					will take the DATASEC_HIGH column assigned to the BPJOHNSN user in the administrator database (USER_INFO table). A comma separated list should be supported and the 
					highest level from each element will be the users actual le	vel. For example, the following are all valid strings based on all of the above.
					 
					"" = default level 
					" D efa ult " = default level 
					"Default,Team" = Team level is effective 
					"Team, Aquila:BPTMPUSR" = probably team levels will be effective. 
					The concern in the business is that people's levels are not consistent across administrator and PensionLine because team leaders do not correctly request the accounts. 
				 */
				if (accDescription != null)
				{
					
					Connection aquilaConn = null;
					
					String selectSIQuery = "Select DATASEC_HIGH from USER_INFO where USER_NAME = ?";
					
					// split the security indicator
					String[] securityDescriptions = accDescription.split(",");
					
					int securityIndicatorTeam = PLReportProducer.getReportSIMapping(PLReportProducer.REPORT_SECURITY_DESCRIPTION_TEAM);
					int securityIndicatorExecs = PLReportProducer.getReportSIMapping(PLReportProducer.REPORT_SECURITY_DESCRIPTION_EXECS);
					int securityIndicatorDefault = PLReportProducer.getReportSIMapping(PLReportProducer.REPORT_SECURITY_DESCRIPTION_DEFAULT);
					
					int securityIndicator = -1;
					
					for (int i = 0; i < securityDescriptions.length; i++)
					{
						String securityDescription = securityDescriptions[i].trim();
						LOG.info("Security description: " + securityDescription);
						String siDescriptionTmp = "";
						int securityIndicatorTmp = -1;
						
						if (securityDescription.trim().equalsIgnoreCase(PLReportProducer.REPORT_SECURITY_DESCRIPTION_TEAM))
						{
							return PLReportProducer.REPORT_SECURITY_DESCRIPTION_TEAM;
						}
						else if (securityDescription.trim().equalsIgnoreCase(PLReportProducer.REPORT_SECURITY_DESCRIPTION_EXECS))
						{
							securityIndicatorTmp = securityIndicatorExecs;
							siDescriptionTmp = PLReportProducer.REPORT_SECURITY_DESCRIPTION_EXECS;
						}
						else if (securityDescription.trim().equalsIgnoreCase(PLReportProducer.REPORT_SECURITY_DESCRIPTION_DEFAULT))
						{
							securityIndicatorTmp = securityIndicatorDefault;
							siDescriptionTmp = PLReportProducer.REPORT_SECURITY_DESCRIPTION_DEFAULT;							
						}
						else if (securityDescription.trim().toLowerCase().indexOf("aquila:") == 0)
						{
							String aquilaUserID = securityDescription.substring("aquila:".length());
																	
							try
							{
								// get security indicator from Aquila database
								if (aquilaConn == null)
								{
									aquilaConn = DBConnector.getInstance().getDBConnFactory(Environment.AQUILA);
								}										
								
								if (aquilaConn != null)
								{
									PreparedStatement pstm = aquilaConn.prepareStatement(selectSIQuery);
									
									pstm.setString(1, aquilaUserID);
									
									ResultSet rs = pstm.executeQuery();
									if (rs.next())
									{
										securityIndicatorTmp = Integer.parseInt(rs.getString(1));
										LOG.info("From Aquila: Report runner " + cmsUser.getName() + " has security indicator = " + securityIndicatorTmp + " with " + securityDescription);
									}
									
									// if DATASEC_HIGH return 0, then the highest value
									if (securityIndicatorTmp == 0 || securityIndicatorTmp >= securityIndicatorTeam)
									{
										return PLReportProducer.REPORT_SECURITY_DESCRIPTION_TEAM;
									}
									else if (securityIndicatorTmp >= securityIndicatorExecs)
									{
										siDescriptionTmp = PLReportProducer.REPORT_SECURITY_DESCRIPTION_EXECS;
									}
									else if (securityIndicatorTmp >= securityIndicatorDefault)
									{
										siDescriptionTmp = PLReportProducer.REPORT_SECURITY_DESCRIPTION_DEFAULT;
									}
									else
									{
										siDescriptionTmp = "";
									}
								}
							}
							catch (Exception e) 
							{
								LOG.error("Error while getting security indicator from Aquila: " + e.toString());
							}
						}						
						
						// update siDescription if the new description field has SI number bigger;
						if (securityIndicator < securityIndicatorTmp)
						{
							securityIndicator = securityIndicatorTmp;
							siDescription = siDescriptionTmp;
						}
						
					}
					
					if (aquilaConn != null)
					{
						DBConnector.getInstance().close(aquilaConn);
					}
				}				
			}
			catch (Exception e) 
			{
				LOG.error("Error in getting report runner SI number: " + e.toString());
			}
			
			LOG.info("User " + cmsUser.getName() + " has SI description: " + siDescription);
		}

		return siDescription;
	}	
	
	/**
	 * List all section of a master report
	 * @return
	 */
	public static Vector listSectionsOfMaster(String reportId)
	{
		return ReportDao.getAllSectionOfMaster(reportId);
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public void outputToFileSystem(String fileName, byte[] reportContent) throws IOException
	{
		File outputFile = new File(PLReportProducer.REPORT_TEMP_FOLDER + "XLS/" + fileName);
		FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
		fileOutputStream.write(reportContent);
		fileOutputStream.flush();
		fileOutputStream.close();
	}	
}
