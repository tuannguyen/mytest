package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.util.AuditingUtil;
import com.bp.pensionline.util.SystemAccount;

public class UpdateGroupsForReportHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private static final long serialVersionUID = 1L;		
	
	private String reportId = null;
	private Vector<String> groupIds = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		boolean isReloadPage = true;

		if (parseRequestXml(xml))
		{
			if (reportId != null && groupIds != null)
			{
				ReportGroupDao.removeAllGroupsOfReport(reportId);
				for (int i = 0; i < groupIds.size(); i++)
				{
					ReportGroupDao.addGroupToReport(reportId, groupIds.elementAt(i));
				}
				actionResult = true;
				/**
				 * Do auditing here
				 */
				Report report = ReportDao.getMasterReport(reportId);
				report.setSections(ReportDao.getAllSectionOfMaster(reportId));
				if (report != null) {
					try {
						AuditingUtil.auditReport(report, "UPDATE", SystemAccount.getCurrentUser(request));
					} catch (Exception e) {
						LOG.error("Exception while auditing SQL reports "+e.toString());
					}
					
				}
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, isReloadPage);
		
		out.print(xmlResponse);
		out.close();

	}
	
	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <RpActionResult>").append(isActionOk).append("</RpActionResult>\n");
		buffer.append("     <RpPageReload>").append(willReloadPage).append("</RpPageReload>\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		LOG.info("parseRequestXml xml: " + xml);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{										
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_ID))
					{
						this.reportId = paramNode.getTextContent();
					}							
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORTGROUPS))
					{
						groupIds = new Vector<String>();
						NodeList groupIdNodes = ((Element)paramNode).getElementsByTagName(Constant.AJAX_SQL_REPORTGROUP_ID);
						for (int j = 0; j < groupIdNodes.getLength(); j++)
						{
							Node groupIdNode = groupIdNodes.item(j);
							if (groupIdNode != null)
							{
								groupIds.add(groupIdNode.getTextContent());
							}
						}
					}										
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		
		return false;

	}	
	
	/**
	 * List all available reports and sort the results
	 * @return
	 */
	public static boolean isReportAddedToGroup(String reportId, String groupId)
	{
		return ReportGroupDao.isReportAddedToGroup(reportId, groupId);
	}	
	
	
	/**
	 * List all section of a master report
	 * @return
	 */
	public static Vector listSectionsOfMaster(String reportId)
	{
		return ReportDao.getAllSectionOfMaster(reportId);
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}	
}

