package com.bp.pensionline.sqlreport.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.DataDicConfigurationXML;

public class QueryDesignerInitTableHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
	
	private static final long serialVersionUID = 1L;			


	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String datasource = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		String errorMesage = null;

		if (datasource != null && !datasource.trim().equals(""))
		{		
			Vector<String> tableNames = DataDicConfigurationXML.getTablesFromDB(datasource);			
			xmlResponse = buildXmlResponse(datasource, tableNames);						
		}
		else
		{
			errorMesage = "No datasource specified";
			xmlResponse = buildXmlResponseError(errorMesage);
		}		
		//LOG.info("xmlResponse = " + xmlResponse);
		out.print(xmlResponse);
		out.close();

	}	

	public String buildXmlResponse(String dbName, Vector<String> tableNames) 
	{
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		buffer.append("     <DD_DBName>").append(dbName).append("</DD_DBName>\n");
		buffer.append("     <DD_Tables>\n");
		if (tableNames != null)
		{
			for (int i = 0; i < tableNames.size(); i++)
			{
				buffer.append("     	<DD_Table>").append(tableNames.elementAt(i)).append("</DD_Table>\n");
			}
		}
		buffer.append("     </DD_Tables>\n");
		buffer.append("</").append(Constant.AJAX_SQL_DD_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}