/**
 * 
 */
package com.bp.pensionline.sqlreport.handler;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.jsp.util.CmsJspStatusBean;
import org.opencms.main.CmsLog;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog;
import com.bp.pensionline.sqlreport.dto.db.Schedule.Status;
import com.bp.pensionline.sqlreport.dto.io.QuerySchedulesRequest;

/**
 * @author anhtuan.nguyen
 *
 */
@SuppressWarnings("serial")
public class AccessSchedules extends HttpServlet {
	protected static Log LOG;
	@Override
	public void init() throws ServletException {
		if(LOG == null){
			LOG = CmsLog.getLog(this.getClass());
		}
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String reportGroupId = request.getParameter("reportGroupId");
		LOG.info("reportGroupId: " + reportGroupId);
		if(reportGroupId == null){
			responseError(request, response, "No report-group specified");
			return;
		}

		reportGroupId = reportGroupId.trim();
		List<Schedule> schedules =  null;
		
		try {
			schedules = querySchedule(reportGroupId);
			LOG.info("schedules size: " + schedules.size());
		} catch (Exception e) {
			responseError(request, response, e.getMessage());
			e.printStackTrace();
			return;
		}
		request.setAttribute("schedules", schedules);
		System.out.println("Forwarding to /content/pl/sql_report/access_schedules.jsp ...");
		this.getServletContext().getRequestDispatcher("/pl/sql_report/access_schedules.jsp").forward(request, response);
	}
	
	protected List<Schedule> querySchedule(String reportGroupId) throws Exception{
		SchedulingApp app = null;
		List<Schedule> schedules =  null;
		QuerySchedulesRequest qReq = new QuerySchedulesRequest();
		qReq.setStatus(Status.getActiveBitMask());//Query active schedules only
		qReq.setReportGroupIds(new String[]{reportGroupId});
		app = AppFactory.getSchedulingApp();
		schedules =  app.getSchedules(qReq);
		
		ReportScheduler scheduler = ReportScheduler.getInstance();
		for(Schedule schedule : schedules){			
			Date nextRun = scheduler.getNextRun(schedule);
			LOG.info("nextRun: " + nextRun);
			ScheduleRunLog runlog = new ScheduleRunLog();
			runlog.setSchedule(schedule);
			runlog.setRunningTime(nextRun);
			schedule.setNextRun(runlog);
			if(schedule.getEmailFrom() == null || schedule.getEmailFrom().length() == 0){
				schedule.setEmailFrom(schedule.getCreatedBy().getEmail());
			}
		}
		return schedules;
	}
	
	protected void responseError(HttpServletRequest request, HttpServletResponse response, String errMessage) 
		throws ServletException, IOException{
		LOG.error("Error: " + errMessage);
		request.setAttribute(CmsJspStatusBean.ERROR_MESSAGE, errMessage);
		request.setAttribute(CmsJspStatusBean.ERROR_STATUS_CODE, new Integer(400));
		this.getServletContext().getRequestDispatcher("/pl-errorhandler/").forward(request, response);
	}

}
