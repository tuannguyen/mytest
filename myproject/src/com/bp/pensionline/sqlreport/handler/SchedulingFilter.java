package com.bp.pensionline.sqlreport.handler;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.opencms.file.CmsUser;

import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.util.SystemAccount;

/**
 * Servlet Filter implementation class SchedulingFilter
 */
public class SchedulingFilter implements Filter {

    /**
     * Default constructor. 
     */
    public SchedulingFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("SchedulingFilter.doFilter...");
		CmsUser currentUser = SystemAccount.getCurrentUser((HttpServletRequest) request);
		if(currentUser == null || !ReportGroupDao.isUserInCmsGroup(currentUser.getName(), "PL_REPORT_SCHEDULER")){
			//((HttpServletResponse) response).sendRedirect("/content/pl/access_denied.html");
			request.getRequestDispatcher("/pl/access_deny.html").forward(request, response);
		} else{
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
