package com.bp.pensionline.sqlreport.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.jsp.util.CmsJspStatusBean;
import org.opencms.main.CmsLog;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.dto.io.RunScheduleRequest;
import com.bp.pensionline.util.SystemAccount;

/**
 * Servlet implementation class RunSchedule
 */
public class RunSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Log LOG;
	@Override
	public void init() throws ServletException {
		if(LOG == null){
			LOG = CmsLog.getLog(this.getClass());
		}
		super.init();
	}
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunSchedule() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String scheduleId = request.getParameter("scheduleId");
		if(scheduleId == null){
			LOG.error("No schedule specified");
			responseError(request, response, "No schedule specified");
			return;
		}
		scheduleId = scheduleId.trim();
		ReportScheduler scheduler = null;
		try {
			scheduler = ReportScheduler.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			responseError(request, response, e.getMessage());
			return;
		}
		Schedule schedule = scheduler.getSchedule(scheduleId);
		SchedulingApp app = null;
		try {
			app = AppFactory.getSchedulingApp();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			responseError(request, response, e.getMessage());
			return;
		}
		RunScheduleRequest runReq = new RunScheduleRequest();
		runReq.setSchedule(schedule);

		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		User user = new User();
		user.setUserName(currentUser.getName());
		user.setEmail(currentUser.getEmail());
		runReq.setRunBy(user);
		
		String error = app.runSchedule(runReq);
		if(error != null){
			LOG.error(error);
			responseError(request, response, error);
			return;
		}
		response.sendRedirect("/content/pl/sql_report/download_report.jsp?scheduleId=" + scheduleId);
//		request.setAttribute("scheduleId", scheduleId);
//		this.getServletContext().getRequestDispatcher("/content/pl/sql_report/download_report.jsp").forward(request, response);
	}
	
	protected void responseError(HttpServletRequest request, HttpServletResponse response, String errMessage) 
		throws ServletException, IOException{
		LOG.error(errMessage);
		request.setAttribute(CmsJspStatusBean.ERROR_MESSAGE, errMessage);
		request.setAttribute(CmsJspStatusBean.ERROR_STATUS_CODE, new Integer(400));
		this.getServletContext().getRequestDispatcher("/pl-errorhandler/").forward(request, response);
	}
}
