/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.handler;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog;
import com.bp.pensionline.sqlreport.dto.io.DownloadScheduleOutputRequest;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;

import org.opencms.jsp.util.CmsJspStatusBean;

import org.opencms.main.CmsLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DownloadScheduleOutput.
 */
public class DownloadScheduleOutput extends HttpServlet {

    //~ Static fields/initializers ---------------------------------------------

    private static final long serialVersionUID = 1L;
    private static Log LOG;

    //~ Constructors -----------------------------------------------------------

    /**
     * @see  HttpServlet#HttpServlet()
     */
    public DownloadScheduleOutput() {
        super();
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     */
    @Override public void init() throws ServletException {
        if (LOG == null) {
            LOG = CmsLog.getLog(this.getClass());
        }
        super.init();
    }

    /**
     * @see  HttpServlet#service(HttpServletRequest request, HttpServletResponse
     *       response)
     */
    protected void service(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        String scheduleId = request.getParameter("scheduleId");
        //String lastRun = request.getParameter("lastRun");
        if (scheduleId == null) {
            responseError(request, response, "Schedule is not specified!");
            return;
        }
        
//        if (lastRun == null || lastRun.equals("-1"))
//        {
//        	responseError(request, response, "Reports have not ran yet!");
//        	return;
//        }
        
        File dir = new File(SQLReportFileUtil.getReportOutputFolder());
        if (!dir.exists() || !dir.isDirectory()) {
            responseError(request, response, "The report have ran but the output file has been moved or deleted by an external system!");
            return;
        }
        
        ReportScheduler scheduler = null;
        try 
        {
            scheduler = ReportScheduler.getInstance();
        } catch (Exception e) {
            responseError(request, response, "Can not get file information: " + e.getMessage());
            return;
        }
        
        Schedule schedule = scheduler.getSchedule(scheduleId);
        if (schedule == null) {
            responseError(request, response, "This schedule does not exist");
            return;
        }
        
        ReportGroup reportGroup = ReportGroupDao.getReportGroupInfo(schedule.getReportGroup().getId());
        
        if (reportGroup == null) {
            String errorMsg = "Report group of schedule "
                + schedule.getTitle()
                + "["
                + schedule.getId() + "] has been removed";
            LOG.error(errorMsg);
            responseError(request, response, errorMsg);

            return;
        }
        
        CmsUser currentUser = SystemAccount.getCurrentUser(request);
        String[] siNames = { "default", "execs", "team" };
        int[] siValues = { 50, 75, 100 };
        
        //Huy modified
		String siDescriptions = RunSQLReportHandler.getUserSecurityIndicatorDescription(currentUser).toLowerCase().trim();
        int si = 50;
        for (int i = siNames.length - 1; i >= 0; i--) {
            if (siDescriptions != null && siDescriptions.toLowerCase().indexOf(siNames[i]) >= 0) {
                si = siValues[i];
                break;
            }
        }
        
        if (si < schedule.getRunLevel()) 
        {
            request.getRequestDispatcher("/pl/sql_report/access_report_deny.html").forward(request, response);
            return;
        }
        
        String userName = currentUser.getName();
        List<ScheduleRunLog> runlogs = schedule.getPrevRuns();
        ScheduleRunLog runlog = runlogs.get(runlogs.size() - 1);
        String fileName = String.valueOf(runlog.getId());
        String[] exts = { ".xls", ".pdf" };
        String ext = exts[0];

        // ((schedule.getOutputFormat() != null)
        // && (schedule.getOutputFormat().toLowerCase().indexOf("pdf")
        // >= 0)) ? ".pdf" : ".xls";
        File file = new File(dir, fileName + ext);
        for (int i = 1; (i < exts.length) && !file.exists(); i++) {
            ext = exts[i];
            file = new File(dir, fileName + ext);
        }
        if (!file.exists()) {
            responseError(request, response,
                "File not found! This file has been moved or deleted");

            return;
        }
        String action = request.getParameter("action");
        if ((action != null) && action.equals("getExt")) {
            request.setAttribute("ext", ext);
            this.getServletContext().getRequestDispatcher("/pl/sql_report/download_report.jsp").forward(request, response);

            return;
        }
        
        FileInputStream is = new FileInputStream(file);
        int length = (int) file.length();
        byte[] content = new byte[length];
        is.read(content);
        response.setHeader("Content-Type", ext.equals(".xls") ? "application/vnd.ms-excel": "application/pdf");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition",
            ext.equals(".xls")
            ? "attachment; filename=\"pensionline_report.xls\""
            : "attachment; filename=\"pensionline_report.pdf\"");
        response.setContentLength(length);
        response.getOutputStream().write(content);
        response.getOutputStream().flush();
        response.getOutputStream().close();
        is.close();
        DownloadScheduleOutputRequest req = new DownloadScheduleOutputRequest();
        req.setRunlogId(runlog.getId());
        req.setUserName(userName);
        try {
            SchedulingApp app = AppFactory.getSchedulingApp();
            app.logDownload(req);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param   request     DOCUMENT ME!
     * @param   response    DOCUMENT ME!
     * @param   errMessage  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    protected void responseError(HttpServletRequest request,
        HttpServletResponse response, String errMessage)
        throws ServletException, IOException {
        LOG.error(errMessage);
        request.setAttribute(CmsJspStatusBean.ERROR_MESSAGE, errMessage);
        request.setAttribute(CmsJspStatusBean.ERROR_STATUS_CODE,
            new Integer(400));
        this.getServletContext().getRequestDispatcher("/pl-errorhandler/").forward(request, response);
    }
}
