/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.handler;

import com.bp.pensionline.sqlreport.dto.db.Schedule;

import org.opencms.file.CmsUser;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author
 * @version
 */
public class ManageSchedules extends AccessSchedules {

    //~ Static fields/initializers ---------------------------------------------

    /**  */
    private static final long serialVersionUID = 1L;

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     */
    @Override public void init() throws ServletException {
        super.init();
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req   DOCUMENT ME!
     * @param   resp  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override protected void doGet(HttpServletRequest req,
        HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doGet(req, resp);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req   DOCUMENT ME!
     * @param   resp  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override protected void doPost(HttpServletRequest req,
        HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doPost(req, resp);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   request   DOCUMENT ME!
     * @param   response  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override
    @SuppressWarnings("unchecked")
    protected void service(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        String reportGroupId = request.getParameter("reportGroupId");
        LOG.info("reportGroupId: " + reportGroupId);
        System.out.println("reportGroupId: " + reportGroupId);
        if (reportGroupId == null) {
            responseError(request, response, "No report-group specified");

            return;
        }
        
        reportGroupId = reportGroupId.trim();
        List<Schedule> schedules = null;
        try {
            schedules = querySchedule(reportGroupId);
        } catch (Exception e) {
            responseError(request, response, e.getMessage());
            e.printStackTrace();

            return;
        }
        request.setAttribute("schedules", schedules);

        // List<CmsUser> recipients = new ArrayList<CmsUser>();
        List<CmsUser> reportRunners = (List<CmsUser>)
            UpdateUserInReportGroupHandler.getUsersOfCmsGroup(
                "PL_REPORT_RUNNER");

        // for (CmsUser user : reportRunners) {
        // recipients.add(user);
        // }
        // request.setAttribute("recipients", recipients);
        request.setAttribute("recipients", reportRunners);
        System.out.println(
            "Forwarding to /content/pl/sql_report/manage_schedules.jsp ...");
        this.getServletContext()
            .getRequestDispatcher("/pl/sql_report/manage_schedules.jsp")
            .forward(request, response);
    }
}
