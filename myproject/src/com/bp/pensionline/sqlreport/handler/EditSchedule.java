/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.handler;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.dto.db.Schedule.Status;
import com.bp.pensionline.sqlreport.dto.io.PersistScheduleRequest;
import com.bp.pensionline.util.SystemAccount;

import org.apache.commons.logging.Log;

import org.opencms.file.CmsUser;

import org.opencms.jsp.util.CmsJspStatusBean;

import org.opencms.main.CmsLog;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author
 * @version
 */
public class EditSchedule extends AccessSchedules {

    //~ Static fields/initializers ---------------------------------------------

    /**  */
    private static final long serialVersionUID = 1L;
    private static Log LOG;

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     */
    @Override public void init() throws ServletException {
        if (LOG == null) {
            LOG = CmsLog.getLog(this.getClass());
        }
        super.init();
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req   DOCUMENT ME!
     * @param   resp  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override protected void doGet(HttpServletRequest req,
        HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doGet(req, resp);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req   DOCUMENT ME!
     * @param   resp  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override protected void doPost(HttpServletRequest req,
        HttpServletResponse resp) throws ServletException, IOException {
        // TODO Auto-generated method stub
        super.doPost(req, resp);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   request   DOCUMENT ME!
     * @param   response  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    @Override
    @SuppressWarnings("unchecked")
    protected void service(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        int iRequestNo = 0;
        String requestNo = (String) request.getSession().getAttribute(
                "requestNo");
        if (requestNo == null) {
            requestNo = "0";
            request.getSession().setAttribute("requestNo", requestNo);
        }
        String postBackRequestNo = request.getParameter("requestNo");
        if ((postBackRequestNo == null)
                || !postBackRequestNo.equals(requestNo)) {
            this.getServletContext()
                .getRequestDispatcher("/ManageSchedulesHandler").forward(
                    request, response);

            return;
        }
        try {
            iRequestNo = Integer.parseInt(requestNo);
            iRequestNo++;
            request.getSession().setAttribute("requestNo",
                String.valueOf(iRequestNo));
        } catch (NumberFormatException e1) {
            System.out.println("Can not parse requestNo " + requestNo);
            LOG.error("Can not parse requestNo " + requestNo);
        }
        PersistScheduleRequest req = new PersistScheduleRequest();

        /*              String scheduleId = request.getParameter("id");
         *              if(scheduleId != null){                     Schedule
         * schedule = null;                     try { schedule =
         * ReportScheduler.getInstance().getSchedule(scheduleId);
         *  } catch (Exception e) { responseError(request, response,
         * e.getMessage());        e.printStackTrace();
         *    return;               }
         * req.setSchedule(schedule);      }
         */
        req.fromParamsMap(request.getParameterMap());
        CmsUser currentUser = SystemAccount.getCurrentUser(request);
        String userName = currentUser.getName();
        User user = new User();
        user.setUserName(userName);
        user.setEmail(currentUser.getEmail());
        req.setRequestedBy(user);
        if (req.getSchedule().getEmailFrom() == null) {
            req.getSchedule().setEmailFrom(user.getEmail());
        }
        StringBuilder errors = new StringBuilder(validateRequest(req));
        if (errors.length() == 0) {
            try {
                SchedulingApp app = AppFactory.getSchedulingApp();
                String result = app.persistSchedule(req);
                if (result != null) {
                    errors.append("\\n").append(result);
                }
            } catch (Exception e) {
                errors.append("\\n").append(e.getMessage());

                // responseError(request, response, e.getMessage());
                e.printStackTrace();
                // return;
            }
        }
        if (errors.length() > 0) {
            request.setAttribute("errors", errors.toString());
            if (req.getSchedule() != null) {
                req.getSchedule().setCronExpr(request.getParameter("cronExpr"));
                request.setAttribute("currentAction",
                    (req.getSchedule().getId() < 0) ? "add" : "edit");
                request.setAttribute("editingSchedule", req.getSchedule());
            }
        }

        //
        // this.getServletContext().getRequestDispatcher("/pl/sql_report/manage_schedules.jsp").forward(request,
        // response); String reportGroupId =
        // request.getParameter("reportGroupId"); String reportGroupName =
        // request.getParameter("reportGroupName"); String url =
        // "/content/ManageSchedulesHandler?reportGroupId=" + reportGroupId +
        // "&reportGroupName=" + reportGroupName; response.sendRedirect(url);
        this.getServletContext().getRequestDispatcher("/ManageSchedulesHandler")
            .forward(request, response);
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    protected String validateRequest(PersistScheduleRequest req) 
    {
        StringBuilder errors = new StringBuilder();
        
        // Huy modified on 15/12/2010 to exclude delete action checking        
        if (req != null && req.getSchedule() != null)
	    {
        	if (req.getSchedule().getStatus() != null && req.getSchedule().getStatus() == Status.DELETED)
        	{
        		return errors.toString();
        	}
	        	
	        if ((req.getSchedule().getCronExpr() == null)
	                || (req.getSchedule().getCronExpr().trim().length() == 0)) {
	            errors.append("\\n Cron expression is invalid.");
	        }
	        if (
	            !org.quartz.CronExpression.isValidExpression(
	                    req.getSchedule().getCronExpr())) {
	            errors.append("\\n Cron expression is invalid or not runable.");
	        }
	        if ((req.getSchedule().getOutputFormat() == null)
	                || (req.getSchedule().getOutputFormat().trim().length() == 0)) {
	            errors.append("\\n Output file format"
	                + req.getSchedule().getOutputFormat() + " is not supported.");
	        }
	        if ((req.getSchedule().getReceivers() == null)
	                || (req.getSchedule().getReceivers().size() == 0)) {
	            errors.append("\\n Please select at least one recipient.");
	        }
	        if ((req.getSchedule().getReportGroup() == null)
	                || (req.getSchedule().getReportGroup().getId() == null)) {
	            errors.append("\\n Report group of schedule is not specified.");
	        }
	        if (req.getSchedule().getRunLevel() < 0) {
	            errors.append("\\n Run-level is invalid.");
	        }
	        if ((req.getSchedule().getTitle() == null)
	                || (req.getSchedule().getTitle().trim().length() == 0)) {
	            errors.append("\\n Please input a title for this schedule.");
	        }
	    }
        else
        {
        	errors.append("\\n The schedule selected does not exist!.");
        }

        return errors.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @param   request     DOCUMENT ME!
     * @param   response    DOCUMENT ME!
     * @param   errMessage  DOCUMENT ME!
     *
     * @throws  ServletException  DOCUMENT ME!
     * @throws  IOException       DOCUMENT ME!
     */
    protected void responseError(HttpServletRequest request,
        HttpServletResponse response, String errMessage)
        throws ServletException, IOException {
        LOG.error(errMessage);
        request.setAttribute(CmsJspStatusBean.ERROR_MESSAGE, errMessage);
        request.setAttribute(CmsJspStatusBean.ERROR_STATUS_CODE,
            new Integer(400));
        this.getServletContext().getRequestDispatcher("/pl/pl-errorhandler/")
            .forward(request, response);
    }
}
