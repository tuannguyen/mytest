package com.bp.pensionline.sqlreport.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.util.QueryAnalizer;

public class ExtractQueryParamsHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_XML_FOLDER = "D:/report_xml/";
	
	private static final long serialVersionUID = 1L;		
	
	private String datasource = null;
	private String query = null;

	// common error message
	private String errorMessage = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		if (parseRequestXml(xml))
		{		
			ArrayList<String> params = QueryAnalizer.extractQueryParams(this.query);
			xmlResponse = buildXmlResponse(params);
		}
		else
		{
			xmlResponse = buildXmlResponseError(errorMessage);
		}		
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(ArrayList<String> queryParameters) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		for (int i = 0; i < queryParameters.size(); i++)
		{
			buffer.append("     <Param>").append(queryParameters.get(i)).append("</Param>").append("\n");
		}
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_SQL_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{	
		//LOG.info("parseRequestXml xml: " + xml);
				
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		xml = xml.replaceAll("\\(question\\)", "?");
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_DATASOURCE))
					{
						datasource = paramNode.getTextContent();	
					}		
					
					if (paramNode.getNodeName().equals(Constant.AJAX_SQL_REPORT_SECTION_QUERY))
					{
						query = QueryAnalizer.removeQueryCommnets(paramNode.getTextContent(), QueryAnalizer.QUERY_COMMENT_PREFIX);
					}									
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error while extracting parameters in query: " + ex.toString());
			this.errorMessage = ex.toString();
		}
		

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
