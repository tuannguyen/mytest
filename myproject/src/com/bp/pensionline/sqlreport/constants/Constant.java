/*
 * Constant.java
 *
 * Created on March 15, 2007, 2:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.sqlreport.constants;

/**
 *
 * @author mall
 */
public class Constant {
    public static final String PARAM = "xml";
    public static final String CONTENT_TYPE = "text/xml";
    
    public static final String DATASOURCE_AQUILLA = "AQUILLA";
    public static final String DATASOURCE_WEBSTATS = "webstats";
    public static final String DATASOURCE_CMS_DATA = "opencms";    
    public static final String DATASOURCE_PENSIONLINE = "pensionline";
    
    // Root report parameter name
    public static final String PL_DATASOURCE_AQUILLA_PARAM_NAME 	= "PL_REPORT_AQUILLA_CONNECTION";
    public static final String PL_DATASOURCE_WEBSTATS_PARAM_NAME 	= "PL_REPORT_WEBSTATS_CONNECTION";
    public static final String PL_REPORT_GROUPTITLE 				= "PL_REPORT_GROUPTITLE";
    public static final String PL_REPORT_RUNNER 					= "PL_REPORT_RUNNER";
    public static final String PL_REPORT_STARTEND_DATE 				= "PL_REPORT_STARTEND_DATE";
    //public static final String PL_REPORT_ENDDATE 					= "PL_REPORT_ENDDATE";
    public static final String PL_REPORT_CREATEDDATE 				= "PL_REPORT_CREATEDDATE";   
    public static final String PL_REPORT_OUTPUT_XLS 				= "XLS";
    public static final String PL_REPORT_OUTPUT_PDF 				= "PDF";
    
    /** Creates a new instance of Constant */
    public static final String AJAX_SQL_REPORT_REQUESTED 	= "AjaxReportParameterRequest";
    public static final String AJAX_SQL_REPORT_RESPONSE 	= "AjaxReportParameterResponse";
    
    public static final String AJAX_SQL_REPORT_ACTION 				= "rp_action";
    public static final String AJAX_SQL_REPORT_ID 					= "rp_id";
    public static final String AJAX_SQL_REPORT_DATASOURCE			= "rp_datasource";
    public static final String AJAX_SQL_REPORT_TITLE 				= "rp_title";
    public static final String AJAX_SQL_REPORT_DESCRIPTION 			= "rp_description";
    public static final String AJAX_SQL_REPORT_SECTIONS 			= "rp_sections";
    public static final String AJAX_SQL_REPORT_SECTION 				= "rp_section";   
    public static final String AJAX_SQL_REPORT_SECTION_ID 			= "rp_section_id";
    public static final String AJAX_SQL_REPORT_SECTION_TITLE 		= "rp_section_title";
    public static final String AJAX_SQL_REPORT_SECTION_DESCRIPTION	= "rp_section_description";
    public static final String AJAX_SQL_REPORT_SECTION_QUERY 		= "rp_section_query";
    public static final String AJAX_SQL_REPORT_SECTION_PARAMS 		= "rp_section_params";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM 		= "rp_section_param";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_NAME 			= "name";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_VALUE 			= "value";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_TYPE 			= "type";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_TYPE_STRING 		= "PARAM_TYPE_STRING";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_TYPE_NUMBER 		= "PARAM_TYPE_NUMBER";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_TYPE_DATE 			= "PARAM_TYPE_DATE";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_DEFAULTVALUE	= "defaultValue";
    public static final String AJAX_SQL_REPORT_SECTION_PARAM_DESCRIPTION 	= "description";
    public static final String AJAX_SQL_REPORT_SECTION_CHARTTYPE 	= "rp_section_charttype";  
    
    
    // Run report
    public static final String AJAX_SQL_REPORTS 				= "rp_reports";
    public static final String AJAX_SQL_REPORT 					= "rp_report";
    public static final String AJAX_SQL_REPORT_STARTDATE 		= "rp_startdate";
    public static final String AJAX_SQL_REPORT_ENDDATE 			= "rp_enddate";
    public static final String AJAX_SQL_REPORT_COPYDATE 		= "rp_copy_date";
    public static final String AJAX_SQL_REPORT_OUTPUT 			= "rp_output";
    public static final String AJAX_SQL_REPORT_RUNNER 			= "rp_runner";  
    public static final String AJAX_SQL_REPORT_CROND_EXP 			= "rp_crond_exp"; 
    public static final String AJAX_SQL_REPORT_DEST_EMAILS 			= "rp_dest_emails"; 
    public static final String AJAX_SQL_REPORT_OUTPUT_PDF 			= "DOWNLOAD_PDF";
    public static final String AJAX_SQL_REPORT_OUTPUT_XLS 			= "DOWNLOAD_XLS";      
    
    // Data dictionary
    public static final String AJAX_SQL_DD_REQUESTED 	= "AjaxDDParameterRequest";
    public static final String AJAX_SQL_DD_RESPONSE 	= "AjaxDDParameterResponse";
    
    public static final String AJAX_SQL_DD_ACTION 		= "dd_action";
    public static final String AJAX_SQL_DD_DBNAME 		= "dd_dbname";
    public static final String AJAX_SQL_DD_TABLE 		= "dd_table";  
    public static final String AJAX_SQL_DD_MAPPINGS 	= "dd_mappings";
    public static final String AJAX_SQL_DD_MAPPING 		= "dd_mapping";
    
    public static final String AJAX_SQL_REPORTGROUPS 					= "rp_groups";
    public static final String AJAX_SQL_REPORTGROUP 					= "rp_group";
    public static final String AJAX_SQL_REPORTGROUP_ID 					= "rp_group_id";
    public static final String AJAX_SQL_REPORTGROUP_NAME 				= "rp_group_name";
    public static final String AJAX_SQL_REPORTGROUP_USERS 					= "rp_group_users";
    public static final String AJAX_SQL_REPORTGROUP_USER 					= "rp_group_user";
    public static final String AJAX_SQL_REPORTGROUP_USER_ID 					= "rp_group_user_id";
    
    public static final String SQLREPORT_AJAX_DATE_FORMAT = "d-MMM-yy";
    public static final String SQLREPORT_DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
    
}
