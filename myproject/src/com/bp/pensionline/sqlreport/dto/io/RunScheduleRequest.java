package com.bp.pensionline.sqlreport.dto.io;

import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.User;

public class RunScheduleRequest extends GenericRequest {
	Schedule schedule = null;
	User runBy = null;
	@Override
	public void fromXml(String xml) throws Exception {
		// TODO Auto-generated method stub

	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public User getRunBy() {
		return runBy;
	}
	public void setRunBy(User runBy) {
		this.runBy = runBy;
	}

}
