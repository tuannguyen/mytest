/**
 * 
 */
package com.bp.pensionline.sqlreport.dto.io;

import java.util.Date;

/**
 * @author liemle
 *
 */
public class RunReportGroupRequest extends GenericRequest {

	String userName;
	String securityIndicator;
	String reportGroupId;
	String outputFormat;
	String fileName;
	Date startDate;
	Date endDate;
	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.sqlreport.dto.db.io.GenericRequest#fromXml(java.lang.String)
	 */
	@Override
	public void fromXml(String xml) throws Exception{
		// TODO Auto-generated method stub

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSecurityIndicator() {
		return securityIndicator;
	}

	public void setSecurityIndicator(String securityIndicator) {
		this.securityIndicator = securityIndicator;
	}

	public String getReportGroupId() {
		return reportGroupId;
	}

	public void setReportGroupId(String reportGroupId) {
		this.reportGroupId = reportGroupId;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
