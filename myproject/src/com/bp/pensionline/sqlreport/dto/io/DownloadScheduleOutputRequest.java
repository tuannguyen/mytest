package com.bp.pensionline.sqlreport.dto.io;

public class DownloadScheduleOutputRequest extends GenericRequest {
	long runlogId = -1;
	String userName = null;
	@Override
	public void fromXml(String xml) throws Exception {
		// TODO Auto-generated method stub

	}
	public long getRunlogId() {
		return runlogId;
	}
	public void setRunlogId(long runlogId) {
		this.runlogId = runlogId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
