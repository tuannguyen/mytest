package com.bp.pensionline.sqlreport.dto.io;

import com.bp.pensionline.sqlreport.dto.db.Schedule;

public class QuerySchedulesRequest extends GenericRequest {

	int status = Schedule.Status.getBitMask();//Defaut: Query all status
	
	int deleted = 3; //Default: don't care is_deleted column
	
	int minRunLevel = 0; //Default: query all run_level
	
	String[] reportGroupIds = null; //Default: query all reportGroupId
	
	@Override
	public void fromXml(String xml) throws Exception {
		// TODO Auto-generated method stub

	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDeleted() {
		return deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public int getMinRunLevel() {
		return minRunLevel;
	}

	public void setMinRunLevel(int minRunLevel) {
		this.minRunLevel = minRunLevel;
	}

	public String[] getReportGroupIds() {
		return reportGroupIds;
	}

	public void setReportGroupIds(String[] reportGroupIds) {
		this.reportGroupIds = reportGroupIds;
	}

}
