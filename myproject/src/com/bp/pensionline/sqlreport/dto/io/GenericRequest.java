/**
 * 
 */
package com.bp.pensionline.sqlreport.dto.io;

/**
 * @author anh.tuan.nguyen
 *
 */
public abstract class GenericRequest {
	public static final String DATE_FORMAT = "MM-dd-yyyy";

	public GenericRequest(){
	}
	public GenericRequest(String xml) throws Exception{
		fromXml(xml);
	}
	public abstract void fromXml(String xml) throws Exception;
}
