/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.sqlreport.dto.io;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.AuthoringApp;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.Schedule.Status;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.util.LogUtil;

import org.apache.commons.logging.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author  anhtuan
 */
public class PersistScheduleRequest extends GenericRequest {

    //~ Static fields/initializers ---------------------------------------------

    private static Log LOG = LogUtil.getLog(PersistScheduleRequest.class);

    //~ Instance fields --------------------------------------------------------

    /** DOCUMENT ME! */
    Schedule schedule = null;

    /** DOCUMENT ME! */
    User requestedBy = null;

    //~ Methods ----------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.bp.pensionline.sqlreport.dto.db.io.GenericRequest#fromXml(java.lang.String)
     */
    @Override public void fromXml(String xml) throws Exception {
        // TODO Auto-generated method stub
    }

    /**
     * DOCUMENT ME!
     *
     * @param  params  DOCUMENT ME!
     */
    public void fromParamsMap(Map<String, String[]> params) {
        if (schedule == null) {
            schedule = new Schedule();
        }
        String[] values = (String[]) params.get("id");
        String sId = ((values != null) && (values.length >= 1)) ? values[0]
                                                                : null;
        if (sId != null) {
            try {
                schedule.setId(Long.parseLong(sId.trim()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        values = (String[]) params.get("runLevel");
        String sRunLevel = ((values != null) && (values.length >= 1))
            ? values[0] : null;
        if (sRunLevel != null) {
            try {
                schedule.setRunLevel(Integer.parseInt(sRunLevel.trim()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        values = (String[]) params.get("title");
        schedule.setTitle(((values != null) && (values.length >= 1)) ? values[0]
                                                                     : null);
        values = (String[]) params.get("cronExpr");
        if ((values != null) && (values.length >= 1)) {
            schedule.setCronExpr(values[0]);
        }
        values = (String[]) params.get("outputFormat");
        schedule.setOutputFormat(((values != null) && (values.length >= 1))
            ? values[0] : null);
        values = (String[]) params.get("notes");
        schedule.setNotes(((values != null) && (values.length >= 1)) ? values[0]
                                                                     : null);
        values = (String[]) params.get("emailFrom");
        schedule.setEmailFrom(((values != null) && (values.length >= 1))
            ? values[0] : null);
        // schedule.setCreatedTime(new Date());
        values = (String[]) params.get("status");
        String sStatus = ((values != null) && (values.length >= 1)) ? values[0]
                                                                    : null;
        if (sStatus != null) {
            try {
                int iStatus = Integer.parseInt(sStatus.trim());
                schedule.setStatus(Status.getStatus(iStatus));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        schedule.setReportGroup(new ReportGroup());
        values = (String[]) params.get("reportGroupId");
        schedule.getReportGroup().setId(
            ((values != null) && (values.length >= 1)) ? values[0] : null);
        values = (String[]) params.get("startDate");
        String sStartDate = ((values != null) && (values.length >= 1))
            ? values[0] : null;
        values = (String[]) params.get("endDate");
        String sEndDate = ((values != null) && (values.length >= 1)) ? values[0]
                                                                     : null;
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        try {
            if (sStartDate != null) {
                schedule.setEndDate(formater.parse(sStartDate));
            }
            if (sEndDate != null) {
                schedule.setStartDate(formater.parse(sEndDate));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String[] recipients = (String[]) params.get("recipients");
        if (recipients != null) {
            List<User> users = new ArrayList<User>();
            AuthoringApp authoringApp;
            try {
                authoringApp = AppFactory.getAuthoringApp();
                for (String sRecipient : recipients) {
                    User user = authoringApp.getUser(sRecipient);
                    if (user != null) {
                        users.add(user);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error("Cannot get recipient info: " + e.getMessage());
            }
            schedule.setReceivers(users);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  schedule  DOCUMENT ME!
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public User getRequestedBy() {
        return requestedBy;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  requestedBy  DOCUMENT ME!
     */
    public void setRequestedBy(User requestedBy) {
        this.requestedBy = requestedBy;
    }
}
