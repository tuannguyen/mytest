package com.bp.pensionline.sqlreport.dto.db;

import java.util.List;

public class ReportGroup implements Comparable<ReportGroup>
{
	List<Schedule> schedules;
	protected String id = null;
	protected String name = null;
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	public int compareTo(ReportGroup o)
	{
		if (o == null || o.getName() == null)
		{
			return 1;
		}
		
		int comparedResult = this.getName().toLowerCase().compareTo(o.getName().toLowerCase());
		
		//LOG.debug("Report compate: " + comparedResult);
		return comparedResult;
	}
	public List<Schedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}	
}
