package com.bp.pensionline.sqlreport.dto.db;

import java.util.Vector;



public class Report implements Comparable<Report>
{
		
	private String reportId = null;
	private String datasource = null;
	private String reportTitle = null;
	private String reportDescription = null;
	Vector<ReportSection> sections = new Vector<ReportSection>();
	
	/**
	 * @return the reportDescription
	 */
	public String getReportDescription()
	{
		return reportDescription;
	}
	/**
	 * @param reportDescription the reportDescription to set
	 */
	public void setReportDescription(String reportDescription)
	{
		this.reportDescription = reportDescription;
	}
	/**
	 * @return the reportId
	 */
	public String getReportId()
	{
		return reportId;
	}
	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(String reportId)
	{
		this.reportId = reportId;
	}
	/**
	 * @return the reportTitle
	 */
	public String getReportTitle()
	{
		return reportTitle;
	}
	/**
	 * @param reportTitle the reportTitle to set
	 */
	public void setReportTitle(String reportTitle)
	{
		this.reportTitle = reportTitle;
	}
	/**
	 * @return the sections
	 */
	public Vector<ReportSection> getSections()
	{
		return sections;
	}
	/**
	 * @param sections the sections to set
	 */
	public void setSections(Vector<ReportSection> sections)
	{
		this.sections = sections;
	}
	/**
	 * @return the datasource
	 */
	public String getDatasource()
	{
		return datasource;
	}
	/**
	 * @param datasource the datasource to set
	 */
	public void setDatasource(String datasource)
	{
		this.datasource = datasource;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Report o)
	{
		if (o == null || o.getReportTitle() == null)
		{
			return 1;
		}
		
		int comparedResult = this.getReportTitle().toLowerCase().compareTo(o.getReportTitle().toLowerCase());
		
		//LOG.debug("Report compate: " + comparedResult);
		return comparedResult;
	}
	
	public String getSectionAsString() {
		String section = "";
		for (int i=0; i<sections.size(); i++) {
			section += ", "+sections.get(i).getSectionTitle();
		}
		return section.length()>1?section.substring(1):section;
	}
	
}
