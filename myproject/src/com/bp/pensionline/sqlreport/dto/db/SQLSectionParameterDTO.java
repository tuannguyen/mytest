package com.bp.pensionline.sqlreport.dto.db;

import java.io.Serializable;

/**
 * PensionLine report parameter object. Parameter is handled as a string.
 * 
 * @author Huy Tran
 *
 */
public class SQLSectionParameterDTO implements Serializable
{
	final static long serialVersionUID = System.currentTimeMillis();
	
	public static final int PARAMETER_TYPE_STRING = 1;
	public static final int PARAMETER_TYPE_NUMERIC = 2;
	public static final int PARAMETER_TYPE_DATE = 3;
	
	private String name = null;
	// default is string
	private int type = PARAMETER_TYPE_STRING;	// classify parameter to provide more ultilities such as date seletor
	private String defaultValue = null;			// default value must be included in double quote "", eg, "2008-08-01"
	private String description = null;
	
	private String value = null;
	
	public SQLSectionParameterDTO()
	{
	}	
	
	public SQLSectionParameterDTO(String name, int type, String defaultValue, String description)
	{
		this.name = name;
		this.type = type;
		this.defaultValue = defaultValue;
		this.description = description;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue()
	{
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public int getType()
	{
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type)
	{
		this.type = type;
	}
	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}
	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	
}
