package com.bp.pensionline.sqlreport.dto.db;

public class DataMappingDTO
{
	private String attribute = null;
	private String alias = null;
	private String description = null;
	
	// specify the table that attribute bellong to
	private String table = null;
	
	
	
	public DataMappingDTO(String attribute, String alias, String description)
	{
		this.attribute = attribute;
		this.alias = alias;
		this.description = description;
	}
	/**
	 * @return the alias
	 */
	public String getAlias()
	{
		return alias;
	}
	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias)
	{
		this.alias = alias;
	}
	/**
	 * @return the attribute
	 */
	public String getAttribute()
	{
		return attribute;
	}
	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute)
	{
		this.attribute = attribute;
	}
	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	/**
	 * @return the table
	 */
	public String getTable()
	{
		return table;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(String table)
	{
		this.table = table;
	}
	
	
}
