/**
 * 
 */
package com.bp.pensionline.sqlreport.dto.db;

import java.sql.Date;

/**
 * @author AS5920G
 *
 */
public class ReportAudit {
	private Integer seqno = null;
	private Date eventDate = null;
	private Integer eventHour = null;
	private Integer eventMinute = null;
	private Integer eventSecond = null;
	private String userName = null;
	private String firstName = null;
	private String lastName = null;
	private String fullName = null;
	private String description = null;
	private String userGroup = null;
	private String reportId = "";
	private String reportName = "";
	private String reportGroupId = "";
	private String reportGroupName = "";
	private String reportSection = "";
	private String action = null;
	
	/**
	 * 
	 * @param eventDate The date of event
	 * @param eventHour the hour of event
	 * @param eventMinute the minute of event
	 * @param eventSecond the second of event
	 * @param userName Account of the user fires event
	 * @param firstName First name
	 * @param lastName Last name
	 * @param description Description of the user
	 * @param userGroup Group in cms of user
	 * @param reportId Id of the report
	 * @param reportName Name of the report
	 * @param reportGroupId Id of the group which report belongs to
	 * @param reportGroupName Name of the group which report belongs to
	 * @param reportSection Sections of the report
	 * @param action Action of the user
	 * @param seqno identifier in table
	 */
	public ReportAudit(Date eventDate, Integer eventHour, Integer eventMinute, Integer eventSecond,
							 String userName, String firstName, String lastName, String description,
							 String userGroup,String reportId, String reportName, String reportGroupId,
							 String reportGroupName, String reportSection, String action, 
							 Integer seqno) {
		this.eventDate = eventDate;
		this.eventHour = eventHour;
		this.eventMinute = eventMinute;
		this.eventSecond = eventSecond;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.userGroup = userGroup;
		this.reportId = reportId;
		this.reportName = reportName;
		this.reportGroupId = reportGroupId;
		this.reportGroupName = reportGroupName;
		this.reportSection = reportSection;
		this.action = action;
		this.seqno = seqno;
		this.fullName = this.firstName + " " + this.lastName + " (" + this.description+")";
	}
	
	/**
	 * @param seqno the seqno to set
	 */
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	/**
	 * @return the seqno
	 */
	public Integer getSeqno() {
		return seqno;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventHour the eventHour to set
	 */
	public void setEventHour(Integer eventHour) {
		this.eventHour = eventHour;
	}
	/**
	 * @return the eventHour
	 */
	public Integer getEventHour() {
		return eventHour;
	}
	/**
	 * @param eventMinute the eventMinute to set
	 */
	public void setEventMinute(Integer eventMinute) {
		this.eventMinute = eventMinute;
	}
	/**
	 * @return the eventMinute
	 */
	public Integer getEventMinute() {
		return eventMinute;
	}
	/**
	 * @param eventSecond the eventSecond to set
	 */
	public void setEventSecond(Integer eventSecond) {
		this.eventSecond = eventSecond;
	}
	/**
	 * @return the eventSecond
	 */
	public Integer getEventSecond() {
		return eventSecond;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param userGroup the userGroup to set
	 */
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	/**
	 * @return the userGroup
	 */
	public String getUserGroup() {
		return userGroup;
	}
	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}
	/**
	 * @param reportName the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}
	/**
	 * @param reportGroupId the reportGroupId to set
	 */
	public void setReportGroupId(String reportGroupId) {
		this.reportGroupId = reportGroupId;
	}
	/**
	 * @return the reportGroupId
	 */
	public String getReportGroupId() {
		return reportGroupId;
	}
	/**
	 * @param reportGroupName the reportGroupName to set
	 */
	public void setReportGroupName(String reportGroupName) {
		this.reportGroupName = reportGroupName;
	}
	/**
	 * @return the reportGroupName
	 */
	public String getReportGroupName() {
		return reportGroupName;
	}
	/**
	 * @param reportSection the reportSection to set
	 */
	public void setReportSection(String reportSection) {
		this.reportSection = reportSection;
	}
	/**
	 * @return the reportSection
	 */
	public String getReportSection() {
		return reportSection;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
}
