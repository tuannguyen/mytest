package com.bp.pensionline.sqlreport.dto.db;

import java.util.Vector;

public class ReportSection
{
	private String sectionId = null;
	private String masterId = null;
	private String sectionTitle = null;
	private String sectionDescription = null;
	private String sectionQuery = null;	
	private String chartType = "CHART_TYPE_NONE";
	private Vector<SQLSectionParameterDTO> params = new Vector<SQLSectionParameterDTO>();
	private SQLReportExtractFieldDTO[] fields = null;
	
	/**
	 * @return the chartType
	 */
	public String getChartType()
	{
		return chartType;
	}
	/**
	 * @param chartType the chartType to set
	 */
	public void setChartType(String chartType)
	{
		this.chartType = chartType;
	}
	/**
	 * @return the params
	 */
	public Vector<SQLSectionParameterDTO> getParams()
	{
		return params;
	}
	/**
	 * @param params the params to set
	 */
	public void setParams(Vector<SQLSectionParameterDTO> params)
	{
		this.params = params;
	}
	/**
	 * @return the sectionDescription
	 */
	public String getSectionDescription()
	{
		return sectionDescription;
	}
	/**
	 * @param sectionDescription the sectionDescription to set
	 */
	public void setSectionDescription(String sectionDescription)
	{
		this.sectionDescription = sectionDescription;
	}
	/**
	 * @return the sectionId
	 */
	public String getSectionId()
	{
		return sectionId;
	}
	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(String sectionId)
	{
		this.sectionId = sectionId;
	}
	/**
	 * @return the sectionQuery
	 */
	public String getSectionQuery()
	{
		return sectionQuery;
	}
	/**
	 * @param sectionQuery the sectionQuery to set
	 */
	public void setSectionQuery(String sectionQuery)
	{
		this.sectionQuery = sectionQuery;
	}
	/**
	 * @return the sectionTitle
	 */
	public String getSectionTitle()
	{
		return sectionTitle;
	}
	/**
	 * @param sectionTitle the sectionTitle to set
	 */
	public void setSectionTitle(String sectionTitle)
	{
		this.sectionTitle = sectionTitle;
	}
	/**
	 * @return the masterId
	 */
	public String getMasterId()
	{
		return masterId;
	}
	/**
	 * @param masterId the masterId to set
	 */
	public void setMasterId(String masterId)
	{
		this.masterId = masterId;
	}
	public SQLReportExtractFieldDTO[] getFields()
	{
		return fields;
	}
	public void setFields(SQLReportExtractFieldDTO[] fields)
	{
		this.fields = fields;
	}				
}
