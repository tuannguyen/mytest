package com.bp.pensionline.sqlreport.dto.db;

public class Recipient extends GenericEntity {
	String userName;
	long scheduleId;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}
	
}
