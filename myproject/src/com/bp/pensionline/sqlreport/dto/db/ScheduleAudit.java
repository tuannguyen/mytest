/**
 * 
 */
package com.bp.pensionline.sqlreport.dto.db;

import java.util.Date;

/**
 * @author liemle
 *
 */
public class ScheduleAudit extends GenericEntity{
	public enum Action{
		CREATE("Create"), UPDATE("Update"), DELETE("Delete");
		String value;
		Action(String value){
			this.value = value;
		}
		public String getValue(){
			return this.value;
		}
		public static Action getAction(String value){
			if(value.equalsIgnoreCase(CREATE.value)){
				return CREATE;
			} else if(value.equalsIgnoreCase(UPDATE.value)){
				return UPDATE;
			} else if(value.equalsIgnoreCase(DELETE.value)){
				return DELETE;
			} 
			return null;
		}
	}
	Schedule schedule;
	User actor;
	Date actionTime;
	Action action;
	
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public User getActor() {
		return actor;
	}
	public void setActor(User auditor) {
		this.actor = auditor;
	}
	public Date getActionTime() {
		return actionTime;
	}
	public void setActionTime(Date auditTime) {
		this.actionTime = auditTime;
	}
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
}
