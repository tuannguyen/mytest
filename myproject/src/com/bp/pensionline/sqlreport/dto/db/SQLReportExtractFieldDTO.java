package com.bp.pensionline.sqlreport.dto.db;

public class SQLReportExtractFieldDTO
{
	private String columnName = null;
	private String className = null;
	private int maxLength = 0;
	
	public String getColumnName()
	{
		return columnName;
	}
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public String getClassName()
	{
		return className;
	}
	public void setClassName(String className)
	{
		this.className = className;
	}
	public int getMaxLength()
	{
		return maxLength;
	}
	public void setMaxLength(int maxLength)
	{
		this.maxLength = maxLength;
	}
	
}
