package com.bp.pensionline.sqlreport.dto.db;

import java.util.Date;
import java.util.List;

public class Schedule extends GenericEntity{
	public static enum Status{
		STOP(1), RUNNING(2), PAUSED(4), EXPIRED(8), DELETED(16), INVALID(32);
		
		private int _value;
		Status(int value){
			_value = value;
		}
		public int getValue(){
			return _value;
		}

		public static Status getStatus(int value){
			switch(value){
			case 1: return STOP;
			case 2: return RUNNING;
			case 4: return PAUSED;
			case 8: return EXPIRED;
			case 16: return DELETED;
			default: return INVALID;
			}
		}
		public static int getBitMask(){
			return 63; //STOP | RUNNING | PAUSED | EXPIRED | DELETED | INVALID
		} 
		public static int getActiveBitMask(){
			return 7; //STOP | RUNNING | PAUSED
		}
		public static int getInactiveBitMask(){
			return 56; //EXPIRED | DELETED | INVALID
		}
	}

	Status status;
	
	List<User> receivers;
	
	List<ScheduleRunLog> prevRuns;
	ScheduleRunLog nextRun;
	
	List<ScheduleAudit> audits;
	
	ReportGroup reportGroup;
	String title;
	String notes;
	String cronExpr;
	String outputFormat;
	Date startDate;
	Date endDate;
	int runLevel = -1;

	User createdBy;
	Date createdTime;
	String emailFrom;
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public List<User> getReceivers() {
		return receivers;
	}
	public void setReceivers(List<User> recipients) {
		this.receivers = recipients;
	}
	public List<ScheduleRunLog> getPrevRuns() {
		return prevRuns;
	}
	public void setPrevRuns(List<ScheduleRunLog> prevRuns) {
		this.prevRuns = prevRuns;
	}
	public ScheduleRunLog getNextRun() {
		return nextRun;
	}
	public void setNextRun(ScheduleRunLog nextRun) {
		this.nextRun = nextRun;
	}
	public List<ScheduleAudit> getAudits() {
		return audits;
	}
	public void setAudits(List<ScheduleAudit> audits) {
		this.audits = audits;
	}
	public ReportGroup getReportGroup() {
		return reportGroup;
	}
	public void setReportGroup(ReportGroup reportGroup) {
		this.reportGroup = reportGroup;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCronExpr() {
		return cronExpr;
	}
	public void setCronExpr(String cronExpr) {
		this.cronExpr = cronExpr;
	}
	public String getOutputFormat() {
		return outputFormat;
	}
	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getRunLevel() {
		return runLevel;
	}
	public void setRunLevel(int runLevel) {
		this.runLevel = runLevel;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public boolean isDeleted(){
		return (status == Status.DELETED);
	}
}	
