package com.bp.pensionline.sqlreport.dto.db;

public class GenericEntity {
	long id = -1;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
