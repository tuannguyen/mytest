package com.bp.pensionline.sqlreport.dto.db;

import java.util.Date;

public class DownloadLog  extends GenericEntity{
	ScheduleRunLog runLog;
	User downloadBy;
	Date downloadTime;
	
	public ScheduleRunLog getRunLog() {
		return runLog;
	}
	public void setRunLog(ScheduleRunLog runLog) {
		this.runLog = runLog;
	}
	public User getDownloadBy() {
		return downloadBy;
	}
	public void setDownloadBy(User downloadBy) {
		this.downloadBy = downloadBy;
	}
	public Date getDownloadTime() {
		return downloadTime;
	}
	public void setDownloadTime(Date downloadTime) {
		this.downloadTime = downloadTime;
	}
}
