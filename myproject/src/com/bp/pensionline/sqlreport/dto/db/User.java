/**
 * 
 */
package com.bp.pensionline.sqlreport.dto.db;

import java.util.List;

/**
 * @author anhtuan.nguyen
 *
 */
public class User extends GenericEntity{
	String userName;
	String password;
	String firstName;
	String lastName;
	String email;
	
	List<UserGroup> groups;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<UserGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<UserGroup> groups) {
		this.groups = groups;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
