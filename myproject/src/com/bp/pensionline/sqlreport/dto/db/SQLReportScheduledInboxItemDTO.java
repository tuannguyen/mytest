package com.bp.pensionline.sqlreport.dto.db;

public class SQLReportScheduledInboxItemDTO
{
	private String id;
	private String url;
	private String groupId;
	private String userId;
	private long creationTime;
	
	/**
	 * @return the creationTime
	 */
	public long getCreationTime()
	{
		return creationTime;
	}
	/**
	 * @param creationTime the creationTime to set
	 */
	public void setCreationTime(long creationTime)
	{
		this.creationTime = creationTime;
	}
	/**
	 * @return the groupId
	 */
	public String getGroupId()
	{
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId)
	{
		this.groupId = groupId;
	}
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}
	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}
	/**
	 * @return the userId
	 */
	public String getUserId()
	{
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	
}
