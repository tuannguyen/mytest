package com.bp.pensionline.sqlreport.dto.db;

import java.util.Date;
import java.util.List;

public class ScheduleRunLog extends GenericEntity{
	Schedule schedule;
	User manualRunner;
	Date runningTime;
	List<DownloadLog> downloadLogs;
	
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public User getManualRunner() {
		return manualRunner;
	}
	public void setManualRunner(User manualRunner) {
		this.manualRunner = manualRunner;
	}
	public Date getRunningTime() {
		return runningTime;
	}
	public void setRunningTime(Date runningTime) {
		this.runningTime = runningTime;
	}
	public List<DownloadLog> getDownloadLogs() {
		return downloadLogs;
	}
	public void setDownloadLogs(List<DownloadLog> downloadLogs) {
		this.downloadLogs = downloadLogs;
	}
}
