/**
 * 
 */
package com.bp.pensionline.sqlreport.util;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dao.SQLReportAuditDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportAudit;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.handler.UpdateUserInReportGroupHandler;

/**
 * @author AS5920G
 *
 */
public class AuditingUtil {
	public static final Log LOG = CmsLog.getLog(AuditingUtil.class);
	
	/**
	 * Implement auditing group of reports when user create, edit a group
	 * @param groupId The Id of group
	 * @param action The action run by user
	 * @param user The user firing action
	 */
	public static void auditReportGroup(String groupId, String action, CmsUser user) {
		LOG.info("auditReportGroup():BEGIN");
		ReportGroup group = ReportGroupDao.getReportGroupInfo(groupId);
		String userName = user.getName();
		String firstName = user.getFirstname();
		String lastName = user.getLastname();
		String description = (user.getDescription()==null?"Default":user.getDescription());
		String userGroup = getReportGroupByUsername(userName);
		String groupName = group.getName();
		Calendar today = Calendar.getInstance();
		int hour = today.get(Calendar.HOUR_OF_DAY);
		int minute = today.get(Calendar.MINUTE);
		int second = today.get(Calendar.SECOND);
		Date time = new Date(today.getTimeInMillis());
		ReportAudit bean = new ReportAudit(time, hour, minute, second,
													   userName, firstName, lastName, description,
													   userGroup, "", "", groupId, groupName,
													   "", action, null);
		boolean result = SQLReportAuditDao.auditReportGroup(bean);
		LOG.info("auditReportGroup():"+result);
		LOG.info("auditReportGroup():END");
	}
	
	/**
	 * Do the auditing when user run, update reports
	 * @param reports Reports which user run
	 * @param action Action of user
	 * @param user The user
	 */
	public static void auditReport(Vector<Report> reports, String action, CmsUser user) {
		LOG.info("auditReport():BEGIN");
		List<ReportAudit> audits = new ArrayList<ReportAudit>();
		String userName = user.getName();
		String firstName = user.getFirstname();
		String lastName = user.getLastname();
		String description = (user.getDescription()==null?"Default":user.getDescription());
		String userGroup = getReportGroupByUsername(userName);
		Calendar today = Calendar.getInstance();
		int hour = today.get(Calendar.HOUR_OF_DAY);
		int minute = today.get(Calendar.MINUTE);
		int second = today.get(Calendar.SECOND);
		Date time = new Date(today.getTimeInMillis());
		for (int i=0; i<reports.size(); i++) {
			Report report = reports.get(i);
			//build auditing for the report
			String groupId = ReportGroupDao.getGroupIdsOfReport(report.getReportId()).size()>0?
								   ReportGroupDao.getGroupIdsOfReport(report.getReportId()).get(0):null;
            String groupName = groupId!=null?ReportGroupDao.getReportGroupInfo(groupId).getName():null;
            ReportAudit bean = new ReportAudit(time, hour, minute, second,
					   									   userName, firstName, lastName, description,
					   									   userGroup, report.getReportId(), report.getReportTitle(), 
					   									   groupId, groupName, report.getSectionAsString(), 
					   									   action, null);
            audits.add(bean);
		}
		boolean result = SQLReportAuditDao.auditReport(audits);
		LOG.info("auditReport():"+result);
		LOG.info("auditReport():END");
	}
	
	/**
	 * Do the auditing when user create/edit/delete reports
	 * @param report The report
	 * @param action The action
	 * @param user The user
	 */
	public static void auditReport(Report report, String action, CmsUser user) {
		LOG.info("auditReport():BEGIN");
		String userName = user.getName();
		String firstName = user.getFirstname();
		String lastName = user.getLastname();
		String description = (user.getDescription()==null?"Default":user.getDescription());
		String userGroup = getReportGroupByUsername(userName);
		Calendar today = Calendar.getInstance();
		int hour = today.get(Calendar.HOUR_OF_DAY);
		int minute = today.get(Calendar.MINUTE);
		int second = today.get(Calendar.SECOND);
		Date time = new Date(today.getTimeInMillis());
		String groupId = ReportGroupDao.getGroupIdsOfReport(report.getReportId()).size()>0?
				   ReportGroupDao.getGroupIdsOfReport(report.getReportId()).get(0):null;
		String groupName = groupId!=null?ReportGroupDao.getReportGroupInfo(groupId).getName():null;
		ReportAudit bean = new ReportAudit(time, hour, minute, second,
				   									   userName, firstName, lastName, description,
				   									   userGroup, report.getReportId(), report.getReportTitle(), 
				   									   groupId, groupName, report.getSectionAsString(), 
				   									   action, null);
		boolean result = SQLReportAuditDao.auditReportGroup(bean);
		LOG.info("auditReport():"+result);
		LOG.info("auditReport():END");
	}
	
	private static String getReportGroupByUsername(String userName) {
		return UpdateUserInReportGroupHandler.isUserInCmsGroup(userName, "PL_REPORT_EDITOR")?
			   "PL_REPORT_EDITOR":"PL_REPORT_RUNNER";
	}
}
