package com.bp.pensionline.sqlreport.util;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.util.CheckConfigurationKey;
 
public class SchedulerReportMail
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static String[] getMailContentTemplate(String key){
		return new String[]{"Report group %s has been run", "A report file of Report-Group %s has been created. Log in to Pensionline system to download it."};
	}
    public static void sendMail(String[] toAddresses, String ccAddress, String subject, String text){
        String fromAddress = CheckConfigurationKey.getStringValue("sqlreport.email.from");
        sendMail(toAddresses, ccAddress, subject, text, fromAddress);
    }
    public static void sendMail(String[] toAddresses, String ccAddress, String subject, String text, String fromAddress)
    {
    	try
		{          
            String host = CheckConfigurationKey.getStringValue("sqlreport.email.host");
            String user = CheckConfigurationKey.getStringValue("sqlreport.email.userid");
            String password = CheckConfigurationKey.getStringValue("sqlreport.email.password");
            String authentication = CheckConfigurationKey.getStringValue("sqlreport.email.auth");
            
            Properties props = System.getProperties();
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.auth", authentication);
            props.put("mail.smtp.port", "25");
            props.put("mail.debug", "false");
            
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
     
            message.setFrom(new InternetAddress(fromAddress));
            if (toAddresses != null && toAddresses.length > 0)
            {
            	for (int i = 0; i < toAddresses.length; i++)
    			{
            		 message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddresses[i]));
    			}
            	if(ccAddress != null){
            		message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress));
            	}
            	
                message.setSubject(subject);
                message.setText(text);
                
                Transport transport = session.getTransport("smtp");
                transport.connect(host, user, password);
                message.saveChanges();
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();  
                LOG.info("Emails sent to recipients!");
            }
		}
		catch (Exception e)
		{
			LOG.error("Sending schedule report email failed: " + e);
		}
    }
}