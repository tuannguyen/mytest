package com.bp.pensionline.sqlreport.util;

public class SecurityFilterFactory
{
	public static SecurityFilter createSecurityFilter(int filterType)
	{
		if (filterType == SecurityFilter.SQL_SECURITY_FILTER)
		{
			return new SQLSecurityFilter();
		}
		
		return new SQLSecurityFilter();
	}
}
