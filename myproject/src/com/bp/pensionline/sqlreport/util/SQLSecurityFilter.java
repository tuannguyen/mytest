package com.bp.pensionline.sqlreport.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


public class SQLSecurityFilter implements SecurityFilter {
	
	public static String FROM_CLAUSE_TEMPLATE = "[\\s]+(from)";
	public static final String COMMA = ",".intern();
	public static final String BLANK = " ";
	public static final String AS_DOUBLEQUOTE_REGULAR_EXP = "[\\s+][Aa][Ss][\\s+]|[\"+]";
	public static final String FROM_REGULAR_EXP = "[\\s+][Ff][Rr][Oo][Mm][\\s+]";
    //public static final String SELECT_FROM_PATERN = "[Ss][Ee][Ll][Ee][Cc][Tt][^\\(\\)]*[Ff][Rr][Oo][Mm]"; 	
	public static final String UNDER_SCORE = "_".intern();
	public static final String SELECT_CLAUSE = " SELECT ".intern();
	public static final String FROM_CLAUSE = " FROM ".intern();
	public static final String AS_CLAUSE = " AS ".intern();
	public static final String HOLDER = "$HOLDER$_".intern();
	public static final String COLON = ".".intern();
	public static final String STAR = "*".intern();
	public static final Log LOG = CmsLog.getLog(SecurityFilter.class);
	
	/**
	 * Process query to add a query that check BASIC table with BD29X field <= securityIndicator with appearance of 'BASIC'.
	 * If securityIndicator < 0, don't check the query
	 */
	public String filter(String query, int securityIndicator)
	{		
		// String newQuery = QueryAnalizer.formatSql(query); // Bad function. Commented by Huy
		String newQuery = query;
		
		//LOG.info("before filtered: "+newQuery);
		if (newQuery != null)
		{
			try
			{
				newQuery = newQuery.replaceAll("[\\s]", " ");
				//newQuery = newQuery.replaceAll("\n", " ");
				//newQuery = newQuery.replaceAll("\t", " ");
				//newQuery = newQuery.replaceAll("\\)", ") ");
				
				final String REPLACER = "$REPLACER$_";				
				Vector<String> replacementHolders = new Vector<String>();
//				newQuery = newQuery.replaceAll("[ ]+\\(", " (");
//				newQuery = newQuery.replaceAll("\\([ ]+", "(");
//				newQuery = newQuery.replaceAll("[ ]+\\)", ")");
//				newQuery = newQuery.replaceAll("\\)[ ]+", ") ");
				String functionPartern = "[\\(][^\\(\\)]*[\\)]";
				
				int index = 0;
				boolean end = false;
				Pattern functionPattern = Pattern.compile(functionPartern);
				while (!end)
				{				
					Matcher matcher = functionPattern.matcher(newQuery);			
					while (matcher.find())
					{
						String match = matcher.group();						
						replacementHolders.add(match);
						newQuery = replaceFirst(newQuery, match, REPLACER + index + "$");
						index += 1;
					}
					
					matcher = functionPattern.matcher(newQuery);
					if (!matcher.find())
					{
						end = true;
					}
				}
				
				newQuery = processTables(newQuery, securityIndicator);
				
				boolean replacerExist = (newQuery.indexOf(REPLACER) > -1);
				
				while (replacerExist)
				{					
					for (int j = 0; j < replacementHolders.size(); j++)
					{						
						String replacement = replacementHolders.elementAt(j);
						if (newQuery.indexOf(REPLACER + j + "$") > -1)
						{
							String replacementTmp = replacement.toUpperCase().trim().replace("_", "");
//							replacementTmp = replacementTmp.replaceAll("[ ]+\\(", " (");
//							replacementTmp = replacementTmp.replaceAll("\\([ ]+", "(");
//							replacementTmp = replacementTmp.replaceAll("[ ]+\\)", ")");
//							replacementTmp = replacementTmp.replaceAll("\\)[ ]+", ") ");
							if (replacementTmp.indexOf("(SELECT ") == 0)
							{
								//LOG.info("Subquery-org: "+replacement);
								replacement = processTables(replacement, securityIndicator);
								//LOG.info("Subquery-rfd: "+replacement);
							}
							newQuery = newQuery.replace(REPLACER + j + "$", replacement);							
						}
					}
					replacerExist = (newQuery.indexOf(REPLACER) > -1);
				}				
			}
			catch (Exception e)
			{
				LOG.error("Error while process from clause to add security filter: " + e);
			}				
		}
		
		//LOG.info("query filtered: " + newQuery);
		return newQuery;
	}
	
	private static String processTables(String query, int securityIndicator)
	{
		//LOG.info("SQL before filter: "+query);
		final String BASIC_SECURITY_INDICATOR_REPLACER_1 = "(select * FROM BASIC where nvl(BD29X, 0) <= " + securityIndicator + ")";
		final String BASIC_SECURITY_INDICATOR_REPLACER_2 = "(select * FROM BASIC where nvl(BD29X, 0) <= " + securityIndicator + ") BASIC";
		
		StringBuffer newQuery = new StringBuffer();
		
		if (securityIndicator < 0) return query;
		
		if (query != null)
		{
			String tmpQuery = query.replaceAll("[\\s^ ]+", " ") + " ";
									
			while (true)
			{
//				 find the first FROM clause
				int fromIndex = tmpQuery.toUpperCase().indexOf(" FROM ");
				while (!isFromForSelect(tmpQuery, fromIndex))
				{
					fromIndex = tmpQuery.toUpperCase().indexOf(" FROM ", fromIndex + " FROM ".length());					
				}
				
				if (fromIndex < 0)
				{
					newQuery.append(tmpQuery);
					break;
				}
				
				// get the parts before and after 'from' of the query
				String beforeFromPart = tmpQuery.substring(0, fromIndex) + " FROM ";				
				
				// check the key word 'where' next to this 'from'
				newQuery.append(beforeFromPart);				
				
				// get the part to replace BASIC table
				String fromPart = getFromClause(tmpQuery);
				if (fromPart == null)
				{					
					break;					
				}
				
				String nextPart = tmpQuery.substring(fromIndex + " FROM ".length() + fromPart.length());
				
//				System.out.println("replace part: " + replacePart);
//				LOG.info("replace part: " + replacePart);
//				String[] tables = replacePart.split(",");
//				String updatedSI = "";
//				for (int i=0; i<tables.length; i++) {
//					updatedSI = tables[i].trim();
//					if (updatedSI.toUpperCase().indexOf("BASIC") !=-1 ) break;
//				}
//				String[] tmpArr = updatedSI.split(" ");
//				boolean haveAlias = tmpArr.length==1?false:true;
//				if (haveAlias) {
//					updatedSI = BASIC_SECURITY_INDICATOR_REPLACER_1 + tmpArr[1];
//				} else {
//					updatedSI = BASIC_SECURITY_INDICATOR_REPLACER_2;
//				}
//				replacePart = "";
//				for (int i=0; i<tables.length; i++) {
//					if (tables[i].toUpperCase().indexOf("BASIC") !=-1 ) {
//						replacePart += ", "+updatedSI; 
//					} else {
//						replacePart += ", "+tables[i];
//					}
//				}
//				replacePart = replacePart.substring(1); //remove "," at the first
			
				// Huy implemented but not correct
				/*
//				 replace all 'BASIC' with BASIC_SECURITY_INDICATOR_REPLACER
				// 1. BASIC at the first with no alias
				// replacePart = replacePart.replaceAll(" [bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1},", " _SI_HOLDER_,");
				replacePart = replacePart.replaceAll("[ ]*[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1}[ ]*,", " _SI_HOLDER_,");
				// 2. BASIC at the first with alias
				//replacePart = replacePart.replaceAll(" [bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1} ", " _SI_HOLDER_ ");
				replacePart = replacePart.replaceAll("[ ]*[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1}[ ]*", " _SI_HOLDER_ ");
				// 3. BASIC at the middle with no alias
				//replacePart = replacePart.replaceAll(",[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1},", ",_SI_HOLDER_,");
				replacePart = replacePart.replaceAll(",[ ]*[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1}[ ]*,", ",_SI_HOLDER_,");
				// 4. BASIC at the middle with alias
				//replacePart = replacePart.replaceAll(",[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1} ", ", _SI_HOLDER_ ");
				replacePart = replacePart.replaceAll(",[ ]*[bB]{1}[aA]{1}[sS]{1}[iI]{1}[cC]{1}[ ]*", ", _SI_HOLDER_ ");
				// 5. BASIC at the last: same as 4
				// replace BASIC BASIC to be come BASIC
				replacePart = replacePart.replaceAll("_SI_HOLDER_ _SI_HOLDER_", "_SI_HOLDER_ ");
				
				if (replacePart.trim().equals("_SI_HOLDER_"))
				{
					replacePart = " _SI_HOLDER_ BASIC ";
				}		
										
				
				
				LOG.info("replace part: " + replacePart);
				// replace the $SI_HOLDER with no alias first
				replacePart = replacePart.replaceAll("[ ]*_SI_HOLDER_[ ]*,", BASIC_SECURITY_INDICATOR_REPLACER_2);
				
				// replace the _SI_HOLDER with alias
				replacePart = replacePart.replaceAll(" _SI_HOLDER_ ", BASIC_SECURITY_INDICATOR_REPLACER_1);
				*/
				String[] tableNames = fromPart.split(",");
				for (int i = 0; i < tableNames.length; i++)
				{
					
					String tableName = tableNames[i];
					
					
					tableName = tableName.trim();
					
					//System.out.println(tableName);
					
					if (tableName.toUpperCase().equals("BASIC"))	// no alisas
					{
						tableNames[i] = BASIC_SECURITY_INDICATOR_REPLACER_2;
					}
					else if (tableName.toUpperCase().indexOf("BASIC ") == 0)
					{
						tableNames[i] = BASIC_SECURITY_INDICATOR_REPLACER_1 + tableName.substring(5);
					}
					else
					{
						tableNames[i] = tableName;
					}
				}
				
				// rebuild the replace Part
				fromPart = "";
				for (int i = 0; i < tableNames.length; i++)
				{
					if (i < tableNames.length - 1)
					{
						fromPart += (tableNames[i] + ", ");
					}
					else
					{
						fromPart += tableNames[i];
					}
				}				
				
				//System.out.println("replace: " + replacePart);
				newQuery.append(" ").append(fromPart).append(" ");
				
				// update tmpQuery
				tmpQuery = nextPart;
			}			
		}

		return newQuery.toString();		
	}
	
	/**
	 * Get query part which belong to FROM key word. This usually limited by FROM to WHERE/GROUP BY/HAVING/UNION/MINUS/INTERSECT/ODER BY. 
	 * @param query
	 * @return
	 */
	public static String getFromClause(String query)
	{
		if (query != null)
		{
			int fromIndex = query.toUpperCase().indexOf(" FROM ");
			
			while (!isFromForSelect(query, fromIndex))
			{
				fromIndex = query.toUpperCase().indexOf(" FROM ", fromIndex + " FROM ".length());				
			}
			
			while (fromIndex > 0)
			{
				//LOG.info("Query: "+query);
				String fromClause = query.substring(fromIndex + " FROM ".length());
					
				int whereIndex = fromClause.toUpperCase().indexOf(" WHERE ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (whereIndex > 0)
				{
					fromClause = fromClause.substring(0, whereIndex);
					//return fromClause;
				}
				
				int groupByIndex = fromClause.toUpperCase().indexOf(" GROUP BY ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (groupByIndex > 0)
				{
					fromClause = fromClause.substring(0, groupByIndex);
					//return fromClause;
				}
				
				int havingIndex = fromClause.toUpperCase().indexOf(" HAVING ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (havingIndex > 0)
				{
					fromClause = fromClause.substring(0, havingIndex);
					//return fromClause;
				}
				
				int unionIndex = fromClause.toUpperCase().indexOf(" UNION ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (unionIndex > 0)
				{
					fromClause = fromClause.substring(0, unionIndex);
					//return fromClause;
				}
				
				int intersectIndex = fromClause.toUpperCase().indexOf(" INTERSECT ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (intersectIndex > 0)
				{
					fromClause = fromClause.substring(0, intersectIndex);
					//return fromClause;
				}	
				
				int minusIndex = fromClause.toUpperCase().indexOf(" MINUS ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (minusIndex > 0)
				{
					fromClause = fromClause.substring(0, minusIndex);
					//return fromClause;
				}	
				
				int orderByIndex = fromClause.toUpperCase().indexOf(" ORDER BY ");
				
				// check if FROM here is used for SELECT by checking if it is in ( ) pair
				if (orderByIndex > 0)
				{
					fromClause = fromClause.substring(0, orderByIndex);
					//return fromClause;
				}	
				//LOG.info("from clause: "+fromClause);
				return fromClause;
			}		
		}
		
		return null;
	}		
	
	/**
	 * Check if FROM here is used for SELECT by checking if it is not in "" or '' pair
	 * @param query
	 * @param fromIndex
	 * @return
	 */
	public static boolean isFromForSelect (String query, int fromIndex)
	{
		boolean isForSelect = true;
		if (query != null && fromIndex >= 0)
		{
			int doubleQuoteCount = 0;
			int singleQuoteCount = 0;
			for (int i = 0; i < fromIndex; i++)
			{
				if (query.charAt(i) == '\"')
				{
					doubleQuoteCount += 1;
				}
				else if (query.charAt(i) == '\'')
				{
					singleQuoteCount += 1;
				}
			}
			
			if (doubleQuoteCount % 2 > 0)
			{
				isForSelect = false;
			}
			
			if (singleQuoteCount % 2 > 0)
			{
				isForSelect = false;
			}
		}
		
		return isForSelect;
	}
	
	public static void testQueryFilter(String inputFilename, String outputFilename) throws IOException
	{
		SQLSecurityFilter sqlFilter = new SQLSecurityFilter();
		File inputFile = new File(inputFilename);
		if (inputFile.exists())
		{
			FileInputStream fileInputStream = new FileInputStream(inputFile);
			byte[] content = new byte[(int)inputFile.length()];
			
			fileInputStream.read(content);
			
			String inputQuery = new String(content);
			String outputQuery = sqlFilter.filter(inputQuery, 75);
			
			File outFile = new File(outputFilename);
			System.out.println(outFile.getAbsoluteFile());
			outFile.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(outFile);
			fileOutputStream.write(outputQuery.getBytes());
			fileOutputStream.flush();
			fileOutputStream.close();
		}
	}		
	
	private static String replaceFirst(String source, String target, String replacement)
	{
		String newSource = source;
		if (source != null && target != null && replacement != null)
		{
			int start = source.indexOf(target);
			if (start > -1)
			{
				newSource = source.substring(0, start) + replacement + source.substring(start + target.length());
			}
		}
		return newSource;
	}
	
	public static void main(String[] args)
	{
		try
		{
//			testQueryFilter("d:\\test\\Huy.txt", "d:\\ready_Huy.txt");
			
			testQueryFilter("F:\\test2.txt", "f:\\ready_test2.txt");
			System.out.println("Done!");
//			testQueryFilter("d:\\test\\test1.txt", "d:\\ready_test1.txt");
			//testQueryFilter("F:\\before_se.txt", "F:\\after_se.txt");
			//String[] testArr = new String[] {"abc" , "def"};
			//System.out.println(testArr);
//			String[] tmpArr = "BASIC".split(",");
//			System.out.println(tmpArr.length);	
//			System.out.println(tmpArr[0]);
		}
		catch (Exception ioe)
		{
			ioe.printStackTrace();
		}
		
//		SQLSecurityFilter filter = new SQLSecurityFilter();
//		String filteredQuery = filter.filter("select count(*) as total_members FROM BASIC", 75);
//		System.out.println("filtered query: " + filteredQuery);
	}
}
