package com.bp.pensionline.sqlreport.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

/**
 * Class QueryUtilizer serve all query to query with alias.
 * @author lam.phan@c-mg.com
 * @version 1.0
 */
public class QueryUtilizer {
	
	/** Represent comma symbol  */
	public static final String COMMA = ",".intern();
	
	/** Represent blank symbol  */
	public static final String BLANK = " ";
	
	/** Represent whitespace symbol  */
	public static final String WHITESPACES = "[\\s]+";
	
	/** Represent regular expression for search by AS and double quote symbol  */
	public static final String AS_DOUBLEQUOTE_REGULAR_EXP = "[\\s]+[Aa][Ss][\\s+]|[\"+]";
	
	/** Represent regular expression  */
	public static final String FROM_REGULAR_EXP = "[\\s+][Ff][Rr][Oo][Mm][\\s+]";
	
	/** Represent regular expression for search by phrase of SELECT FROM  */
//    public static final String SELECT_FROM_PATERN = "[Ss][Ee][Ll][Ee][Cc][Tt][^\\?\\%]*[Ff][Rr][Oo][Mm]"; 	
//    public static final String SELECT_FROM_PATERN = "[Ss][Ee][Ll][Ee][Cc][Tt][^FROM]*"; 
	public static final String SELECT_FROM_PATERN = "[Ss][Ee][Ll][Ee][Cc][Tt][^\\(\\)]*[Ff][Rr][Oo][Mm]"; 
    /** Represent underscore symbol  */
	public static final String UNDER_SCORE = "_".intern();
	
	/** Represent SELECT keyword  */
	public static final String SELECT_CLAUSE = " SELECT ".intern();
	
	/** Represent FROM keyword  */
	public static final String FROM_CLAUSE = " FROM ".intern();
	
	/** Represent AS keyword  */
	public static final String AS_CLAUSE = " AS ".intern();
	
	/** Represent holder keyword  */
	public static final String HOLDER = "$HOLDER$_".intern();
	
	/** Represent colon keyword  */
	public static final String COLON = ".".intern();
	
	/** Represent star symbol  */
	public static final String STAR = "*".intern();
	
	/** Get log value for current class */
	public static final Log LOG = CmsLog.getLog(QueryUtilizer.class);
	
	/**
	 * Function that changes all of the query to alias query. <br>
	 * @param query string value
	 * @param securityIndicator
	 * @return String alias query
	 */
	public String queryToAlias(String query)
	{		
		
		String newQuery = query;
		if (newQuery != null)
		{
			try	{
				
				// Process to alias query
				newQuery = processToAlias(newQuery);
			}
			catch (Exception e)
			{
				LOG.error("Error while process from clause to alias query: " + e);
			}				
		}
		
		return newQuery;
	}
	
	/**
	 * Get select from clause and process adding alias for query. <br>
	 * 1. Do finding by select from pattern
	 * 2. Add query with alias to array
	 * 3. Marking and replace the founded query by new alias query
	 * @param newQuery the query
	 * @return the query with alias
	 */
	public static String processToAlias(String processQuery) {
		String newQuery = processQuery.replaceAll(WHITESPACES, BLANK);
		Vector<String> holders = new Vector<String>();
		Pattern selectPattern = Pattern.compile(SELECT_FROM_PATERN);
		Matcher selectMatcher = selectPattern.matcher(newQuery);
		int indexHolder = 0;
		String match = null;
		
		// Find by SELECT_FROM_PATERN pattern
		while (selectMatcher.find()) {
			match = selectMatcher.group();		
			holders.add(changeToAlias(match));
			
			// Replace the query with marking holder
			newQuery = replaceFirst(newQuery, match, HOLDER + indexHolder + "$");
			indexHolder += 1;
		}
		String replacedHolder = null;
		
		// Replace all the query by new query with alias
		for (int index = 0; index < holders.size(); index++)
		{						
			replacedHolder = holders.elementAt(index);
			if (newQuery.indexOf(HOLDER + index + "$") > -1)
				newQuery = newQuery.replace(HOLDER + index + "$", replacedHolder);							
		}
		return newQuery;
	}
	
	/**
	 * Method that change all columns name to alias. <br>
	 * @param clause the sql script
	 * @return string sql with alias
	 */
	public static String changeToAlias(String clause)
	{
		
		// Remove SELECT AND FROM from source
		String source = clause.toUpperCase().trim();
		int selectPos = source.indexOf(SELECT_CLAUSE.trim());
		int fromPos = source.lastIndexOf(FROM_CLAUSE.trim());
		if (selectPos >-1) 
			source = source.substring(selectPos + SELECT_CLAUSE.trim().length(), source.length());
		
		if (fromPos >-1) 
			source = source.substring(0, source.length() - FROM_CLAUSE.trim().length());
		
		StringBuffer elementBuffer = new StringBuffer();
		String[] elements = null;
		try {
			if (source != null) {
			  
			  // Get array of elements by split comma symbol
			  elements = source.split(COMMA);
			  
			  // Get all duplicate column name
			  Map<String, Integer > duplicateColons = getDuplicateNames(getColonName(elements));
			  Map<String, Integer > duplicateQuotes = getDuplicateNames(getNameByDoubleQuote(elements));
			  
			  int elementCount = 0;
			  int elementLeng = elements.length;
			  elementBuffer.append(SELECT_CLAUSE);
			  Integer numberOfElements;
			  Integer numberOfColon;
			  for (String element : elements) {
				 elementCount++;
				 
				 // In case of AS clause has exist
				 if (element.toUpperCase().lastIndexOf(AS_CLAUSE)> -1) {
					 
				   numberOfElements = duplicateQuotes.get(processChar(element));
				   
				   // If duplicate elements are founded
				   if (duplicateQuotes.containsKey(processChar(element))){	
					 elementBuffer.append(element.replaceAll(element, renameDoubleQuote(element, numberOfElements )));
					 int currentNumber = numberOfElements -1;
					 duplicateQuotes.put(processChar(element), currentNumber );
				   } else {
					 elementBuffer.append(element);
				   }   
				   if (elementCount < elementLeng)
					 elementBuffer.append(COMMA);
				   continue;
				 }
				
				 numberOfColon = duplicateColons.get(toColonName(element));
				 
				 // If element is replace name, append element with AS
				 if (duplicateColons.containsKey(toColonName(element))) {
					 elementBuffer.append(element.replaceAll(element, renameTo(element, numberOfColon )));
					 int updatedNumber = numberOfColon -1;
					 duplicateColons.put(toColonName(element), updatedNumber);
				 } else { // Append element without AS
					 elementBuffer.append(element);
				 }
				 if (elementCount < elementLeng )
					elementBuffer.append(COMMA);
			  }
			  elementBuffer.append(FROM_CLAUSE);
			} 
		} catch(Exception e) { LOG.error("Error in append query"); }
		return elementBuffer.toString();
	}
	
	/**
	 * Get string value in double quote symbol
	 * @param element String value 
	 * @return String value after split by double quote
	 */
	private static String processChar(String name) {
		String element = name.trim();
		String[] doubleQuotes = element.split("\"");
		if (doubleQuotes.length > 1) {
		  element=  doubleQuotes[1].toString();
		}
		else {
		  doubleQuotes = element.split(BLANK);
		  element =  doubleQuotes[doubleQuotes.length -1].toString();
		}	
		return element;	
	}
	
	/**
	 * Function get all duplicate name
	 * @param names array of names
	 * @return all duplicate name
	 */
	private static Map<String, Integer> getDuplicateNames(List<String> names) throws Exception {
		
		// An array for store uniquely duplicate elements
		Map<String, Integer> repeated = new LinkedHashMap<String, Integer>();
		Iterator<String> iterator = names.iterator();
		String name=null;
		while (iterator.hasNext()) {
			name = (String) iterator.next().trim();
			
			// Looking for a duplicate entry
			if (repeated.containsKey(name)) {
				repeated.put(name, repeated.get(name)+ 1);
				
			} else {
				
				// Just doing dummy put in the HashMap object
				repeated.put(name, 1);
			}
		}
		
		// Return uniquely elements
		return eliminateRedundance(repeated);
	}
	
	/**
	 * Get all elements with value > 1
	 * @param duplicates
	 * @return elements
	 */
	private static Map<String, Integer > eliminateRedundance(Map<String, Integer> duplicates) {
		
		// Remove all elements with value = 1
		Iterator<String> iteratorDuplicated = duplicates.keySet().iterator();
		
		List<String> listMap = new ArrayList<String>(duplicates.keySet());
		String keyName = null;
		try {
			while (iteratorDuplicated.hasNext()) {
				keyName = (String)iteratorDuplicated.next();
				
				// Remove all value is 1 
				if (duplicates.get(keyName)==1)
					duplicates.remove(keyName);
			}
			
		// Exception occur	
		} catch(Exception e) {
			if (iteratorDuplicated.hasNext()) {
				keyName = listMap.get(listMap.size() - 1 );
				
				// If Map only contain last element, remove it if it's value is 1 
				if (duplicates.get(keyName) == 1)
					duplicates.remove(keyName);
			}
			
		} 
		return duplicates;
	}
	
	/**
	 * Function which get all the column name with colon
	 * @param elements array of column names
	 * @param patternAs pattern for compile regular expression
	 * @return array of column names
	 */
	private static List<String> getNameByDoubleQuote(String[] elements) {
		List<String> listDoubleQuote = new ArrayList<String>();
		
		for (String element : elements) {
		  
		  // If As matching, skip adding to array
		  if (element.toUpperCase().lastIndexOf(AS_CLAUSE.trim()) > -1  ) {
			  
			  // In case of element has a colon
			  if (element.contains("\""))
				listDoubleQuote.add(processChar(element));
		  }
		}
		return listDoubleQuote;
	}
	
	/**
	 * Function which get all the column name with colon
	 * @param elements array of column names
	 * @param patternAs pattern for compile regular expression
	 * @return array of column names
	 */
	private static List<String> getColonName(String[] elements) {
		List<String> listNameWithColon = new ArrayList<String>();
		
		for (String element : elements) {
			  
			  // If As matching, skip adding to array
			  if (element.toUpperCase().lastIndexOf(AS_CLAUSE.trim()) > -1 ) 
				continue;
			  
			  // In case of element has a colon
			  if (hasColon(element))
				listNameWithColon.add(toColonName(element).trim());
		 }
		return listNameWithColon;
	}
	
	/**
	 * Method which check colon symbol existence
	 * @param name of the column
	 * @return boolean value
	 */
	private static boolean hasColon(String name) {
		int underscoreIndex = name.indexOf(COLON);
		if (underscoreIndex > -1 ) 
			return true;
		return false;
	}
	
	/**
	 * Get name after colon
	 * @param name value
	 * @return name
	 */
	private static String toColonName(String name) {
		int isFoundColon = name.indexOf(COLON);
		if (isFoundColon > -1 ) 
			return name.substring(isFoundColon+1, name.length()).trim();
		return name;
	}
	
	/**
	 * Function which rename column name to appropriate name
	 * @param name column name
	 * @param count dynamic value
	 * @return name a new name value
	 */
	private static String renameTo(String columnName, int count) {
		String name = columnName.trim();
		String filterName = toColonName(columnName).trim();
		int underscoreIndex = name.indexOf(UNDER_SCORE);
		StringBuffer bufferStr = new StringBuffer();
		
		// Case of underscore founded in string value
		if (underscoreIndex != -1)
			
			// Append name to buffer string
			bufferStr.append(name).append(AS_CLAUSE).append(filterName).append(UNDER_SCORE).append(count);
		else 
			bufferStr = bufferStr.append(name).append(AS_CLAUSE).append(filterName).append(UNDER_SCORE).append(count);
		return bufferStr.toString();
	}
	
	/**
	 * Rename column with double quote
	 * @param columnName name of column
	 * @param count a dynamic value
	 * @return the new name
	 */
	private static String renameDoubleQuote(String columnName, int count) {
		StringBuffer bufferStr = new StringBuffer();
		String[] elements = columnName.split("\"");
		if (elements.length > 1)
			bufferStr.append(elements[0]).append("\"").append(elements[elements.length - 1] ).append(UNDER_SCORE).append(count).append("\"");
		else {
			bufferStr.append(elements[elements.length - 1] ).append(UNDER_SCORE).append(count);
		}
		return bufferStr.toString();
	}
	
	/**
	 * @param source
	 * @param target
	 * @param replacement
	 * @return
	 */
	private static String replaceFirst(String source, String target, String replacement)
	{
		String newSource = source;
		if (source != null && target != null && replacement != null)
		{
			int start = source.indexOf(target);
			if (start > -1)
			{
				newSource = source.substring(0, start) + replacement + source.substring(start + target.length());
			}
		}
		return newSource;
	}
}
