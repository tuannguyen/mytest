package com.bp.pensionline.sqlreport.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsFolder;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.types.CmsResourceTypeBinary;
import org.opencms.file.types.CmsResourceTypeFolder;
import org.opencms.file.types.CmsResourceTypePlain;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.bp.pensionline.publishing.util.PLPublishThread;
import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.app.jasper.jrxml.SectionJRXMLConsumer;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

public class SQLReportFileUtil
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * Use this method with restriction
	 */
	public static void deleteResource(String resourceName)
	{
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
			cmsAdminObj.getRequestContext().setSiteRoot("/");
			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			if (cmsAdminObj.existsResource(resourceName, CmsResourceFilter.ONLY_VISIBLE_NO_DELETED))
			{
				cmsAdminObj.lockResource(resourceName);
				cmsAdminObj.deleteResource(resourceName, CmsResource.DELETE_PRESERVE_SIBLINGS);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in deleting CMS report resource: " + resourceName + ". Exception: " + e);
		}
	}
	
	public static ArrayList<String> getAllSubfolders(String parentFolder)
	{
		ArrayList<String> subfolders = new ArrayList<String>();
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
			cmsAdminObj.getRequestContext().setSiteRoot("/");
			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			List cmsSubfolders = cmsAdminObj.getSubFolders(parentFolder);
			for (int i = 0; i < cmsSubfolders.size(); i++)
			{
				CmsFolder cmsSubfolder = (CmsFolder) cmsSubfolders.get(i);
				subfolders.add(cmsSubfolder.getName());
			}
			
		}
		catch (Exception e)
		{
			LOG.error("Error in getting all sub-folders of " + parentFolder + ". Exception: " + e);
		}
		
		return subfolders;
	}	
	
	/**
	 * Read all file names in a folder
	 */
	public static ArrayList<String> getFilenamesInfolder(String folderName)
	{
		ArrayList<String> filenames = new ArrayList<String>();
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
			cmsAdminObj.getRequestContext().setSiteRoot("/");
			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			List listFilesNoDir = cmsAdminObj.getFilesInFolder(folderName, CmsResourceFilter.ALL);
			for (int i = 0; i < listFilesNoDir.size(); i++)
			{
				CmsFile cmsFile = (CmsFile) listFilesNoDir.get(i);
				filenames.add(cmsFile.getName());
			}
			
		}
		catch (Exception e)
		{
			LOG.error("Error in deleting CMS report folder: " + folderName + ". Exception: " + e);
		}
		
		return filenames;
	}	
	
	/**
	 * Use this method with restriction
	 */
	public static boolean createFolder(String folderName)
	{
		boolean folderCreated = false;
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();

			cmsAdminObj.getRequestContext().setSiteRoot("/");

			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));

			if (!cmsAdminObj.existsResource(folderName))
			{

				cmsAdminObj.createResource(folderName, CmsResourceTypeFolder.getStaticTypeId());
				CmsLock lock = cmsAdminObj.getLock(folderName);

				if (!lock.isNullLock())
				{
					// resource is unlocked, so lock it
					//cmsAdminObj.lockResource(filename);
					cmsAdminObj.changeLock(folderName);
					cmsAdminObj.unlockResource(folderName);					
				}

				ArrayList<String> newFolders = new ArrayList<String>();
				newFolders.add(folderName);						
			}
			
			folderCreated = true;
		}
		catch (CmsException cmse)
		{
			LOG.error("Error while creating resouces in CMS: " + folderName + " : " + cmse.toString());
		}
		
		return folderCreated;
	}
	
	/**
	 * Use this method with restriction
	 */
	public static boolean createTextFile(String filename, byte[] contentBytes)
	{
		boolean reportCreated = false;
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();

			cmsAdminObj.getRequestContext().setSiteRoot("/");

			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));

			if (!cmsAdminObj.existsResource(filename))
			{

				cmsAdminObj.createResource(filename, CmsResourceTypePlain.getStaticTypeId(), contentBytes, new ArrayList());
				CmsLock lock = cmsAdminObj.getLock(filename);

				if (!lock.isNullLock())
				{
					// resource is unlocked, so lock it
					//cmsAdminObj.lockResource(filename);
					cmsAdminObj.changeLock(filename);
					cmsAdminObj.unlockResource(filename);					
				}

//				cmsAdminObj.writeFile((CmsFile)file);
//				cmsAdminObj.changeLock(filename);
//				cmsAdminObj.unlockResource(filename);
				
				reportCreated = true;
			}
			else
			{
				CmsFile cmsFile = cmsAdminObj.readFile(filename, CmsResourceFilter.ALL);
				if (cmsFile != null)
				{
					cmsFile.setContents(contentBytes);
					CmsLock lock = cmsAdminObj.getLock(filename);

					if (lock.isNullLock())
					{
						// resource is unlocked, so lock it
						cmsAdminObj.lockResource(filename);					
					}
					else
					{
						cmsAdminObj.changeLock(filename);
					}
					cmsAdminObj.writeFile(cmsFile);
					cmsAdminObj.unlockResource(filename);					
				}
			}
		}
		catch (CmsException cmse)
		{
			LOG.error("Error while creating resouces in CMS: " + filename + " : " + cmse.toString());
		}
		
		return reportCreated;
	}	
	
	/**
	 * Use this method with restriction
	 */
	public static boolean createDataFile(String filename, byte[] contentBytes)
	{
		boolean reportCreated = false;
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();

			cmsAdminObj.getRequestContext().setSiteRoot("/");

			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));

			if (!cmsAdminObj.existsResource(filename))
			{

				cmsAdminObj.createResource(filename, CmsResourceTypeBinary.getStaticTypeId(), contentBytes, new ArrayList());
				CmsLock lock = cmsAdminObj.getLock(filename);

				if (!lock.isNullLock())
				{
					// resource is unlocked, so lock it
					//cmsAdminObj.lockResource(filename);
					cmsAdminObj.changeLock(filename);
					cmsAdminObj.unlockResource(filename);					
				}

//				cmsAdminObj.writeFile((CmsFile)file);
//				cmsAdminObj.changeLock(filename);
//				cmsAdminObj.unlockResource(filename);
				
				reportCreated = true;
			}
			else
			{
				CmsFile cmsFile = cmsAdminObj.readFile(filename, CmsResourceFilter.ALL);
				if (cmsFile != null)
				{
					cmsFile.setContents(contentBytes);
					CmsLock lock = cmsAdminObj.getLock(filename);

					if (lock.isNullLock())
					{
						// resource is unlocked, so lock it
						cmsAdminObj.lockResource(filename);					
					}
					else
					{
						cmsAdminObj.changeLock(filename);
					}
					cmsAdminObj.writeFile(cmsFile);
					cmsAdminObj.unlockResource(filename);					
				}
			}
		}
		catch (CmsException cmse)
		{
			LOG.error("Error while creating resouces in CMS: " + filename + " : " + cmse.toString());
		}
		
		return reportCreated;
	}
	
	/**
	 * Get report file content
	 * @param filename
	 * @return
	 */
	public static byte[] getFileData(String filename)
	{
		try
		{
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
			cmsAdminObj.getRequestContext().setSiteRoot("/");
			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			boolean exist = cmsAdminObj.existsResource(filename);
			//LOG.info("File " + filename + " existed: " + exist);
			// check if the file is exist or not
			if (exist)
			{// if true
				// read file and return a byte array
				CmsFile xmlFile = (CmsFile) cmsAdminObj.readFile(filename, CmsResourceFilter.ALL);
				byte[] arr = xmlFile.getContents();
				//LOG.info("File size" + arr.length);
				return arr;
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error in reading report file content: " + filename + ". Exception is: " + ex);
		}
		
		return null;
	}
	
	/**
	 * This method for getting all resources (folders, files) that are changed, deleted
	 * @param cms
	 * @param folderName
	 * @return
	 * @throws CmsException
	 */
	public static ArrayList<String> getAllChangedResourcesInFolder(CmsObject cms, String folderName) throws CmsException
	{
		ArrayList<String> resourceNames = new ArrayList<String>();
		CmsFolder cmsfolder = null;
		CmsFile cmsFile = null;

		String pathDir = "";
		
		if (cms != null && cms.existsResource(folderName))
		{
			List listFolders = cms.getSubFolders(folderName);		
			List listFilesNoDir = cms.getFilesInFolder(folderName, CmsResourceFilter.ALL);			
			
			for (int m = 0; m < listFilesNoDir.size(); m++)
			{
				cmsFile = (CmsFile) listFilesNoDir.get(m);
				String fileName = cmsFile.getName();
				if (cmsFile.getState() == CmsResource.STATE_CHANGED || cmsFile.getState() == CmsResource.STATE_NEW || cmsFile.getState() == CmsResource.STATE_DELETED)
				{
					resourceNames.add(folderName + fileName);					
				}			
			}
	
			for (int i = 0; i < listFolders.size(); i++)
			{
				cmsfolder = (CmsFolder) listFolders.get(i);
				pathDir = folderName + cmsfolder.getName() + "/";
				if (cmsfolder.getState() == CmsResource.STATE_CHANGED || cmsfolder.getState() == CmsResource.STATE_NEW
						|| cmsfolder.getState() == CmsResource.STATE_DELETED)
				{
					resourceNames.add(pathDir);					
				}					
				resourceNames.addAll(getAllChangedResourcesInFolder(cms, pathDir));			
			}
			
			
		}
		
		return resourceNames;
	}
	
	/**
	 * Publish all resources in a folder
	 * @param folderName
	 */
	public static void publishFolder (String folderName)
	{
		if (folderName != null)
		{
		
			try
			{
				CmsObject cms = SystemAccount.getAdminCmsObject();
				cms.getRequestContext().setSiteRoot("/");

				cms.getRequestContext().setCurrentProject(cms.readProject("Offline"));				

				// Process all folders, files are changed or deleted
				ArrayList<String> pageURIs = getAllChangedResourcesInFolder(cms, folderName);	
				publishResources(pageURIs);
                			
			}
			catch (Exception e)
			{
				LOG.error("Error in publishing files: " + e.toString());
			}
		}		
	}
	
	/**
	 * Publish all resource in a List
	 * @param resources
	 */
	public static void publishResources(List<String> resources){
		try
		{
			CmsObject cms = SystemAccount.getAdminCmsObject();
			cms.getRequestContext().setSiteRoot("/");

			cms.getRequestContext().setCurrentProject(cms.readProject("Offline"));				
			
            List publishResources = new ArrayList(resources.size());
			// get the offline resource(s) in direct publish mode
			Iterator i = resources.iterator();
			while (i.hasNext())
			{
				String resName = (String) i.next();
				LOG.info("Get resources that need to be published: " + resName);
				try
				{						
					if (cms.existsResource(resName, CmsResourceFilter.ALL))
					{							
						CmsResource res = cms.readResource(resName, CmsResourceFilter.ALL);
						publishResources.add(res);
						
						// check if the resource is locked
						CmsLock lock = cms.getLock(resName);
						if (!lock.isNullLock())
						{
							// resource is locked, so unlock it
							cms.changeLock(resName);
							cms.unlockResource(resName);
						}							
					}
				}
				catch (CmsException cmse)
				{
					LOG.error("Error in initilize publish resources: " + cmse);
				}
			}
			
			// create publish list for direct publish
            CmsPublishList publishList = OpenCms.getPublishManager().getPublishList(cms,
                publishResources,
                false,
                true);
            
            //cms.checkPublishPermissions(publishList);
            //LOG.info("Project type: " + cms.getRequestContext().currentProject().getType());
            PLPublishThread thread = new PLPublishThread(cms, publishList);
            LOG.info("Start publishing thread");
            // start the publish thread
            thread.start();
            // join() with current thread to avoid reading unpulishing pages from recent_changes.jsp
            thread.join();
            			
		}
		catch (Exception e)
		{
			LOG.error("Error in publishing files: " + e.toString());
		}
	}
	
	public static String getReportOutputFolder() {
		return PLReportProducer.REPORT_OUTPUT_FOLDER;
	}
}
