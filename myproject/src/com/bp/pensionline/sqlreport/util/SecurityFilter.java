package com.bp.pensionline.sqlreport.util;

public interface SecurityFilter
{
	public static final int SQL_SECURITY_FILTER = 1;
	
	public String filter(String query, int securityIndicator);
}
