package com.bp.pensionline.sqlreport.util;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

public class LogUtil {
	public static Log getLog(Class clazz){
		return CmsLog.getLog(clazz);
	}
}
