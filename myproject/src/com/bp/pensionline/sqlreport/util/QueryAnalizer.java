package com.bp.pensionline.sqlreport.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.dto.db.SQLReportExtractFieldDTO;
import com.bp.pensionline.sqlreport.exception.PLQueryErrorException;



public class QueryAnalizer
{	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
		
	public static final String COMMON_PARAMETER_PATTERN = 
//			"([<]{1}|[<]{1}[=]{1}|[>]{1}|[>]{1}[=]{1}|[=]{1}|[<]{1}[>]{1}|[!]{1}[=]{1}" +	// comparator
//			"|[Bb]{1}[Ee]{1}[Tt]{1}[Ww]{1}[Ee]{1}[Ee]{1}[Nn]{1}[ ]+|[Aa]{1}[Nn]{1}[Dd]{1}[ ]+" + // Between and
//			"|[Ll]{1}[Ii]{1}[Kk]{1}[Ee]{1}[ ]+" +	// Like
//			"|[Ii]{1}[Nn]{1}[ ]*[\\(]?[ ]*)[ ]*" +	// In
			"([ ]*[:]{1}[\\w]+[\\W]{1})";
	
	public static final String EXTENDED_PARAMETER_PARTERN = 
			"([']{1}[\\&]{1}[\\w]+[']{1})|([@]{1}[vV]{1}[aA]{1}[rR]{1}[iI]{1}[aA]{1}[bB]{1}[lL]{1}[eE]{1}[(]{1}[']{1}[\\w]+[']{1}[)]{1})";
	
	public static final String ALIASIER_PARTERN_ASEND 	= "([ ]{1}[A]{1}[S]{1}[ ]{1}(([\\w]+)|([']{1}[^'\"]+[']{1})|([\"]{1}[^\"]+[\"]{1}))[ ]*[,]{1})|([ ]{1}[E]{1}[N]{1}[D]{1}[ ]{1}(([\\w]+)|([']{1}.+[']{1})|([\"]{1}.+[\"]{1}))[ ]*[,]{1})";
	
	public static final String CONDITIONER_PARTERN_ANDOR = "([ ]{1}[A]{1}[N]{1}[D]{1}[ ]{1})|([ ]{1}[O]{1}[R]{1}[ ]{1})";
	
	public static final String KEYWORD_ENDCASE = "END";	
	public static final String KEYWORD_ENDCASE_MARKER = "$END_CASE$";

	public static final String PARAMETER_PATTERN_WITH_COMPARATOR = "([<]{1}|[<]{1}[=]{1}|[>]{1}|[>]{1}[=]{1}|[=]{1}|[<]{1}[>]{1}|[!]{1}[=]{1})[ ]*(([:]{1}[\\w]+)|([']{1}[\\&]{1}[\\w]+[']{1})|([@]{1}[vV]{1}[aA]{1}[rR]{1}[iI]{1}[aA]{1}[bB]{1}[lL]{1}[eE]{1}[(]{1}[']{1}[\\w]+[']{1}[)]{1}))";
	public static final String PARAMETER_PATTERN_WITH_FUNCTION = "([<]{1}|[<]{1}[=]{1}|[>]{1}|[>]{1}[=]{1}|[=]{1}|[<]{1}[>]{1}|[!]{1}[=]{1})[ ]*([\\w]+[(]{1})(([:]{1}[\\w]+)|([']{1}[\\&]{1}[\\w]+[']{1})|([@]{1}[vV]{1}[aA]{1}[rR]{1}[iI]{1}[aA]{1}[bB]{1}[lL]{1}[eE]{1}[(]{1}[']{1}[\\w]+[']{1}[)]{1}))[)]{1}";
	
	public static final String SET_OPERATOR_PARTERN 	= "([ ]{1}[Uu]{1}[Nn]{1}[Ii]{1}[Oo]{1}[Nn]{1}[ ]{1})|([ ]{1}[Mm]{1}[Ii]{1}[Nn]{1}[Uu]{1}[Ss]{1}[ ]{1})|([ ]{1}[Ii]{1}[Nn]{1}[Tt]{1}[Ee]{1}[Rr]{1}[Ss]{1}[Ee]{1}[Cc]{1}[Tt]{1}[ ]{1})";
	
	public static final String QUERY_COMMENT_PREFIX = "--";
	
	public static final String NULL_AS_PARTERN = "([Nn]{1}[Uu]{1}[Ll]{1}[Ll]{1}[\\s]+[Aa]{1}[Ss]{1}[\\s]+(([\"]{1}[\\s]+[\"]{1})|([\']{1}[\\s]+[\']{1}))[\\s]*[,]{1})";
	
	private String query;
	
	private ArrayList<String> setKeywords = new ArrayList<String>();
	private String[][] selectClauseParts = null;	// contains all the queries splited by UNION, MINUS, INTERSECT in the query after normalized
	
	

	public QueryAnalizer(String query)
	{
		this.query = query;
		normarlizeQuery();
	}
	
	
	
	public String getQuery()
	{
		return query;
	}



	public void setQuery(String query)
	{
		this.query = query;
	}
	
	


	public ArrayList<String> getSetKeywords()
	{
		return setKeywords;
	}



	public void setSetKeywords(ArrayList<String> setKeywords)
	{
		this.setKeywords = setKeywords;
	}



	public String[][] getSelectClauseParts()
	{
		return selectClauseParts;
	}



	public void setSelectClauseParts(String[][] selectClauseParts)
	{
		this.selectClauseParts = selectClauseParts;
	}



	/**
	 * Split query to parts to for later processes
	 */
	private void normarlizeQuery ()
	{
		if (isSelectQuery(this.query))
		{
			String newQuery = this.query;
			
			if (newQuery != null)
			{
				try
				{
					// Normalize the query first so that it has the format of
					// Select c1, c2,  from ... with out any double quotes and function
					newQuery = newQuery.replaceAll("[\\s]", " ");
					//newQuery = newQuery.replaceAll("\n", " ");
					//newQuery = newQuery.replaceAll("\t", " ");
					//newQuery = newQuery.replaceAll("\\)", ") ");
					
					// Normalize the double quotes first
					final String DOUBLEQUOTE_REPLACER = "$DQ_REPLACER$_";
					Vector<String> doubleQuoteReplacementHolders = new Vector<String>();
					int leftDoubleQuoteIndex = newQuery.indexOf('"');
					int rightDoubleQuoteIndex = newQuery.indexOf('"', leftDoubleQuoteIndex + 1);
					
					StringBuffer replacedStringBuffer = new StringBuffer();
					boolean isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
					int phaseIndex = 0;
					while (isDoubleQuoteToReplace)
					{
						// store the string inside double quotes in to the array
						String storePhase = newQuery.substring(leftDoubleQuoteIndex, rightDoubleQuoteIndex + 1);
						doubleQuoteReplacementHolders.add(storePhase);
						
						// append the left string to the replacement
						replacedStringBuffer.append(newQuery.substring(0, leftDoubleQuoteIndex)).append(DOUBLEQUOTE_REPLACER + phaseIndex + "$");
						phaseIndex += 1;
						
						// process the next part
						newQuery = newQuery.substring(rightDoubleQuoteIndex + 1);
						leftDoubleQuoteIndex = newQuery.indexOf('"');
						rightDoubleQuoteIndex = newQuery.indexOf('"', leftDoubleQuoteIndex + 1);
						
						isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
					}
					
					replacedStringBuffer.append(newQuery);
					
					newQuery = replacedStringBuffer.toString();	
					
					
					final String FUNCTION_REPLACER = "$FU_REPLACER$_";			
					Vector<String> functionReplacementHolders = new Vector<String>();
//					newQuery = newQuery.replaceAll("[ ]+\\(", " (");
//					newQuery = newQuery.replaceAll("\\([ ]+", "(");
//					newQuery = newQuery.replaceAll("[ ]+\\)", ")");
//					newQuery = newQuery.replaceAll("\\)[ ]+", ") ");
					String functionPartern = "[\\(][^\\(\\)]*[\\)]";
					
					int index = 0;
					boolean end = false;
					Pattern functionPattern = Pattern.compile(functionPartern);
					while (!end)
					{				
						Matcher matcher = functionPattern.matcher(newQuery);			
						while (matcher.find())
						{
							String match = matcher.group();						
							functionReplacementHolders.add(match);
							newQuery = replaceFirst(newQuery, match, FUNCTION_REPLACER + index + "$");
							index += 1;
						}
						
						matcher = functionPattern.matcher(newQuery);
						if (!matcher.find())
						{
							end = true;
						}
					}	
					
					
					String[] removedSetQueries = newQuery.split(SET_OPERATOR_PARTERN);
					Pattern setPattern = Pattern.compile(SET_OPERATOR_PARTERN);
					Matcher matcher = setPattern.matcher(newQuery);			
					while (matcher.find())
					{
						String match = matcher.group();						
						setKeywords.add(match);
					}
					
					selectClauseParts = new String[removedSetQueries.length][3];
					for (int i = 0; i < removedSetQueries.length; i++)
					{
						String removedSetQuery = removedSetQueries[i];
						// get the parts of the query
						String beforeSelectClauseTmp = getBeforeSelectClause(removedSetQuery);
						String selectClauseTmp = getSelectClause(removedSetQuery);
						String afterSelectClauseTmp = getAfterSelectClause(removedSetQuery);
						
						boolean beforeSelectFunctionReplacerExist = (beforeSelectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						while (beforeSelectFunctionReplacerExist)
						{					
							for (int j = 0; j < functionReplacementHolders.size(); j++)
							{						
								String replacement = functionReplacementHolders.elementAt(j);
								if (beforeSelectClauseTmp.indexOf(FUNCTION_REPLACER + j + "$") > -1)
								{				
									beforeSelectClauseTmp = beforeSelectClauseTmp.replace(FUNCTION_REPLACER + j + "$", replacement);							
								}
							}
							beforeSelectFunctionReplacerExist = (beforeSelectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						}	
						
						
						boolean beforeSelectDoubleQuoteReplacerExist = (beforeSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						while (beforeSelectDoubleQuoteReplacerExist)
						{					
							for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
							{						
								String replacement = doubleQuoteReplacementHolders.elementAt(j);
								if (beforeSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
								{				
									beforeSelectClauseTmp = beforeSelectClauseTmp.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
								}
							}	
							beforeSelectDoubleQuoteReplacerExist = (beforeSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						}
						
						boolean selectFunctionReplacerExist = (selectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						while (selectFunctionReplacerExist)
						{					
							for (int j = 0; j < functionReplacementHolders.size(); j++)
							{						
								String replacement = functionReplacementHolders.elementAt(j);
								if (selectClauseTmp.indexOf(FUNCTION_REPLACER + j + "$") > -1)
								{				
									selectClauseTmp = selectClauseTmp.replace(FUNCTION_REPLACER + j + "$", replacement);							
								}
							}
							selectFunctionReplacerExist = (selectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						}	
											
						boolean selectDoubleQuoteReplacerExist = (selectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						while (selectDoubleQuoteReplacerExist)
						{					
							for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
							{						
								String replacement = doubleQuoteReplacementHolders.elementAt(j);
								if (selectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
								{				
									selectClauseTmp = selectClauseTmp.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
								}
							}	
							selectDoubleQuoteReplacerExist = (selectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						}
						
						//System.out.println("select clause: " + selectClauseTmp);
						
						boolean afterSelectFunctionReplacerExist = (afterSelectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						while (afterSelectFunctionReplacerExist)
						{					
							for (int j = 0; j < functionReplacementHolders.size(); j++)
							{						
								String replacement = functionReplacementHolders.elementAt(j);
								if (afterSelectClauseTmp.indexOf(FUNCTION_REPLACER + j + "$") > -1)
								{				
									afterSelectClauseTmp = afterSelectClauseTmp.replace(FUNCTION_REPLACER + j + "$", replacement);							
								}
							}
							afterSelectFunctionReplacerExist = (afterSelectClauseTmp.indexOf(FUNCTION_REPLACER) > -1);
						}	
						
						
						boolean afterSelectDoubleQuoteReplacerExist = (afterSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						while (afterSelectDoubleQuoteReplacerExist)
						{					
							for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
							{						
								String replacement = doubleQuoteReplacementHolders.elementAt(j);
								if (afterSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
								{				
									afterSelectClauseTmp = afterSelectClauseTmp.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
								}
							}	
							afterSelectDoubleQuoteReplacerExist = (afterSelectClauseTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
						}							
						
						selectClauseParts[i][0] = beforeSelectClauseTmp;
						selectClauseParts[i][1] = selectClauseTmp;
						selectClauseParts[i][2] = afterSelectClauseTmp;

					}								
					
				}
				catch (Exception e)
				{
					//e.printStackTrace();
					LOG.error("Error while normalize query: " + e);
				}				
			}				
		}
	}

	public int[] extractMaxDataLengths (String dbName)
	{	
		int[] dataLengths = null;
		Connection connection = null;
		try
		{		
			connection = DBConnector.getInstance().establishConnection(dbName);
			
			if (connection != null)
			{				
				//LOG.info("numCols = " + numCols);
				// get the max length of data in columns
				String getMaxLengthQuery = formatToGetMaxLengthQuery();
				LOG.info("getMaxLengthQuery: " + getMaxLengthQuery);
				if (getMaxLengthQuery != null)
				{
					// get table attributes based on select * query				
					PreparedStatement pstm = connection.prepareStatement(getMaxLengthQuery);						
					
					ResultSet rs = pstm.executeQuery();
					// get column names from ResutlSet MetaData
					ResultSetMetaData rsMD = rs.getMetaData();
					
					if (rs.next())
					{
						int maxLengthNumCols = rsMD.getColumnCount();
						dataLengths = new int[maxLengthNumCols];
						for (int i = 0; i < maxLengthNumCols; i++)
						{
							dataLengths[i] = rs.getInt(i+1);						
						}
					}					
				}						

			}
		}
		catch (Exception e)
		{
			LOG.error("Error in extracting query max data length: " + e.toString());
		}
		finally
		{		
			try
			{
				if (connection != null)
				{
					connection.close();
				}				
			}
			catch (Exception e)
			{
				LOG.error("Error in closing connection: " + e.toString());
			}				
		}			
		
		return dataLengths;
	}	
	
	/**
	 * Validate a query by execute a penetration query to the data-store. 
	 * @return A array of SQLReportExtractFieldDTO objects
	 * @throws SQLException
	 */
	public static SQLReportExtractFieldDTO[] checkQueryError(String aQuery, String aDBName) throws PLQueryErrorException
	{
		String message = null;
		SQLReportExtractFieldDTO[] queryFields = null;
		if (aQuery != null)
		{						
			if (!isSelectQuery(aQuery))
			{
				message = "The query input is not a SELECT query!\nORT does not allow query that is NOT a SELECT query because these queries may damage the database.";
				
				throw new PLQueryErrorException(message);
			}
			
				
			// this cost
			Connection connection = null;
			
			try
			{		
				connection = DBConnector.getInstance().establishConnection(aDBName);
				
				if (connection != null)
				{
					// Set all parameter to null so that the query can be executed
					String penQuery = formatToPenatrationQuery(aQuery);
					LOG.info("Section penetration query: " + penQuery);
					PreparedStatement pstm = connection.prepareStatement(penQuery);						
					
					ResultSet rs = pstm.executeQuery();
					// get column names from ResutlSet MetaData
					ResultSetMetaData rsMD = rs.getMetaData();
					
					int numCols = rsMD.getColumnCount();
					queryFields = new SQLReportExtractFieldDTO[numCols];
					
					LOG.info("numCols = " + numCols);
					for (int i = 0; i < numCols; i++)
					{
						queryFields[i] = new SQLReportExtractFieldDTO();
						String colName = rsMD.getColumnName(i + 1);					
						String className = rsMD.getColumnClassName(i + 1);
						if (className != null){
							if(className.equals("java.math.BigInteger")) 
								className = "java.lang.Long";
							if(className.equals("java.sql.Clob")) 
								className = "java.lang.String";
						}
						queryFields[i].setColumnName(colName);
						queryFields[i].setClassName(className);
						//LOG.info("colName: " + colName);
					}					
				}
			}
			catch (SQLException se)
			{
				LOG.error("Error in checkQueryError: " + se.toString());
				message = se.toString();
				throw new PLQueryErrorException(message);
			}
			finally
			{		
				try
				{
					if (connection != null)
					{
						connection.close();
					}				
				}
				catch (Exception e)
				{
					LOG.error("Error in closing connection: " + e.toString());
				}				
			}	
		}
		
		if (queryFields == null || queryFields.length == 0)
		{
			message = "The query input does not contains any column to generate the report.";
			
			throw new PLQueryErrorException(message);
		}
		
		return queryFields;
	}	
	
	
	/**
	 * Format user input query to the runable JasperReport query. 
	 * This will replace the param :PARAM_A by $P{PARAM_A}
	 * @return
	 */
	public static String formatToJasperQuery (String aQuery)
	{
		String jrQuery = aQuery  + " ";
		if (jrQuery != null)
		{
						
			try
			{
				String tmpQuery = removeLiteral(jrQuery);
				String extractedParam = null;			
				
				Pattern compPattern = Pattern.compile(COMMON_PARAMETER_PATTERN);
				Matcher matcher = compPattern.matcher(tmpQuery);
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter

					// this partern find a parameter with a non-word character at the end so length - 1
					String extractedParamLiteral = extractedParam.substring(extractedParam.indexOf(':') + 1, extractedParam.length() - 1); 
					
					char lastChar = extractedParam.charAt(extractedParam.length() - 1);
					String replaceParamStr = extractedParam.substring(extractedParam.indexOf(':'));
					
					String jrParam = "$P{" + extractedParamLiteral.toUpperCase() + "}";
					jrQuery = jrQuery.replace(replaceParamStr, jrParam + lastChar);				
				}
				
				compPattern = Pattern.compile(EXTENDED_PARAMETER_PARTERN);
				matcher = compPattern.matcher(jrQuery);
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter

					if (extractedParam.indexOf("'&") > -1)
					{
						String extractedParamLiteral = extractedParam.substring(extractedParam.indexOf("'&") + 2, extractedParam.length() - 1);
						String jrParam = "$P{" + extractedParamLiteral.toUpperCase() + "}";
						//String operator = extractedParam.substring(0, extractedParam.indexOf("'&"));
						jrQuery = jrQuery.replace(extractedParam, jrParam);
					}
					else if (extractedParam.toLowerCase().indexOf("@variable('") > -1)
					{
						String extractedParamLiteral = extractedParam.substring(extractedParam.toLowerCase().indexOf("@variable('") + 11, extractedParam.length() - 2);
						String jrParam = "$P{" + extractedParamLiteral.toUpperCase() + "}";
						//String operator = extractedParam.substring(0, extractedParam.toLowerCase().indexOf("@variable('"));
						jrQuery = jrQuery.replace(extractedParam, jrParam);					
					}
				}								
			}
			catch (PatternSyntaxException pse)
			{
				LOG.error("PatternSyntaxException: " + pse.getMessage());
			}
		}
		
		
		return jrQuery;
	}
	
	/**
	 * Format user input query to the runable JasperReport query. 
	 * This will replace the param :PARAM_A by $P{PARAM_A}
	 * @return
	 */
	public static ArrayList<String> extractQueryParams (String aQuery)
	{
		ArrayList<String> params = new ArrayList<String>();
		if (aQuery != null)
		{				
			aQuery = aQuery  + " ";
			try
			{
				String tmpQuery = removeLiteral(aQuery);
				//System.out.println("tmpQuery: " + tmpQuery);
				String extractedParam = null;			
				
				Pattern compPattern = Pattern.compile(COMMON_PARAMETER_PATTERN);
				Matcher matcher = compPattern.matcher(tmpQuery + " ");
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter

					if (extractedParam.indexOf(':') > -1)
					{
						// this partern find a parameter with a non-word character at the end so length - 1
						String extractedParamLiteral = extractedParam.substring(extractedParam.indexOf(':') + 1, extractedParam.length() - 1); 						
						params.add(extractedParamLiteral);
					}
				}
				
				compPattern = Pattern.compile(EXTENDED_PARAMETER_PARTERN);
				matcher = compPattern.matcher(aQuery);
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter

					if (extractedParam.indexOf("'&") > -1)
					{
						String extractedParamLiteral = extractedParam.substring(extractedParam.indexOf("'&") + 2, extractedParam.length() - 1);
						params.add(extractedParamLiteral);
					}
					else if (extractedParam.toLowerCase().indexOf("@variable('") > -1)
					{
						String extractedParamLiteral = extractedParam.substring(extractedParam.toLowerCase().indexOf("@variable('") + 11, extractedParam.length() - 2);
						params.add(extractedParamLiteral);					
					}
				}								
			}
			catch (PatternSyntaxException pse)
			{
				LOG.error("PatternSyntaxException: " + pse.getMessage());
			}
		}
		
		
		return params;
	}	
	
	/**
	 * Return a list of parameters used in the query
	 * @param query
	 * @return
	 */
	private static String removeLiteral (String query)
	{
		if (query != null)
		{
			StringBuffer removedLiteralQuery = new StringBuffer();
			// remove literals
			int doubleQuoteNum = 0;
			boolean literalStart = false;
			boolean literalEnd = true;			
			for (int i = 0; i < query.length(); i++)
			{
				char c = query.charAt(i);
				
				if (c == '"')
				{	
					doubleQuoteNum += 1;
					// found the next '"'
					if (doubleQuoteNum % 2 == 1)
					{
						literalStart = true;
						literalEnd = false;
					}
					else
					{
						literalStart = false;
						literalEnd = true;						
					}									
					
					removedLiteralQuery.append("#");
				}
				else
				{		
					if (literalStart && !literalEnd)
					{
						
						removedLiteralQuery.append("#");
					}
					else
					{
						removedLiteralQuery.append(c);
					}
				}
			}
			
			int singleQuoteNum = 0;
			literalStart = false;
			literalEnd = true;
			String removedDoubleQuoteQuery = removedLiteralQuery.toString();
			removedLiteralQuery.delete(0, removedLiteralQuery.length());
			for (int i = 0; i < removedDoubleQuoteQuery.length(); i++)
			{
				char c = removedDoubleQuoteQuery.charAt(i);
				
				if (c == '\'')
				{	
					singleQuoteNum += 1;
					// found the next '"'
					if (singleQuoteNum % 2 == 1)
					{
						literalStart = true;
						literalEnd = false;
					}
					else
					{
						literalStart = false;
						literalEnd = true;						
					}									
					
					removedLiteralQuery.append("#");
				}
				else
				{		
					if (literalStart && !literalEnd)
					{
						
						removedLiteralQuery.append("#");
					}
					else
					{
						removedLiteralQuery.append(c);
					}
				}
			}
			
			return removedLiteralQuery.toString();		
			
		}
		
		return null;
	}
	
	/**
	 * Format user input query to the runable query with all params set to NULL. 
	 * This will replace the param :PARAM_A by NULL
	 * @return
	 */
	public static String formatToPenatrationQuery (String aQuery)
	{
		String penetrationQuery = aQuery + " ";
		if (penetrationQuery != null)
		{
			String tmpQuery = removeLiteral(penetrationQuery);
			
			try
			{
				String extractedParam = null;			
				
				Pattern compPattern = Pattern.compile(COMMON_PARAMETER_PATTERN);
				Matcher matcher = compPattern.matcher(tmpQuery);
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter

					// this partern find a parameter with a non-word character at the end so length - 1 
					char lastChar = extractedParam.charAt(extractedParam.length() - 1);
					String replaceParamStr = extractedParam.substring(extractedParam.indexOf(':'));
					
					penetrationQuery = penetrationQuery.replace(replaceParamStr, "NULL" + lastChar);
					
				}
				
				compPattern = Pattern.compile(EXTENDED_PARAMETER_PARTERN);
				matcher = compPattern.matcher(penetrationQuery);
				while (matcher.find())
				{
					extractedParam = matcher.group();
					// Modified by Huy from 16/12/2010 to add operator to parameter
					penetrationQuery = penetrationQuery.replace(extractedParam, "NULL");
				}				
			}
			catch (PatternSyntaxException pse)
			{
				LOG.error("PatternSyntaxException: " + pse.getMessage());
			}
		}
		
		return penetrationQuery;
	}	
	
	public String formatToGetMaxLengthQuery()
	{
		String aQuery = this.query;
		if (aQuery != null)
		{
			StringBuffer getMaxLengthQueryBuff = new StringBuffer();
			getMaxLengthQueryBuff.append("SELECT ");
			
			String tempQuery = aQuery.replaceAll("\\s+", " ");
			tempQuery = tempQuery.replaceAll("\\)", ") ");
			tempQuery = tempQuery.toUpperCase();
			tempQuery = tempQuery.trim();
			
			tempQuery = removeOrderByClause(tempQuery);
			tempQuery = removeUnionMiniusIntersectQuery(tempQuery);
			
			String selectClause = getSelectClause(tempQuery) + ",";
			
			// mark any special keyword that ends a select field without AS such as END. This's then used to
			// replace back the keyword
			selectClause = selectClause.replace(" " + KEYWORD_ENDCASE + " ", " " + KEYWORD_ENDCASE_MARKER + " " + KEYWORD_ENDCASE + " ");
			
			//System.out.println("selectClause: " + selectClause);
			
			// remove all Alias from select clause by split
			Pattern aliasPattern = Pattern.compile(ALIASIER_PARTERN_ASEND);
			String[] selectFields = aliasPattern.split(selectClause);
	
			// process extracted fields by 2 steps
			if (selectFields != null && selectFields.length > 0)
			{
				for (int i = 0; i < selectFields.length; i++)
				{
					//System.out.println("selectFields: " + selectFields[i]);
					// replace back special keyword					
					String selectField = selectFields[i].replace(KEYWORD_ENDCASE_MARKER, KEYWORD_ENDCASE);
					String[] fineGrainedSelectFields = proccessSelectFields(selectField);
					if (fineGrainedSelectFields != null)
					{
						for (int j = 0; j < fineGrainedSelectFields.length; j++)
						{
							StringBuffer maxLengthFunctionBuilder = new StringBuffer();
							maxLengthFunctionBuilder.append("max(length(");
							maxLengthFunctionBuilder.append(fineGrainedSelectFields[j]).append("))");
							if (i == selectFields.length - 1 && j == fineGrainedSelectFields.length -1)
							{
								getMaxLengthQueryBuff.append(maxLengthFunctionBuilder);
							}
							else
							{
								getMaxLengthQueryBuff.append(maxLengthFunctionBuilder).append(",");								
							}
							
							//System.out.println("maxLengthFunctionBuilder: " + maxLengthFunctionBuilder);
						}
					}
									
				}
			}
							
			// process FROM clause
			String fromClause = getFromClause(tempQuery);
			//System.out.println("fromClause: " + fromClause);
			String newFromClause = proccessFromClause(fromClause);
			//System.out.println("newFromClause: " + newFromClause);
			getMaxLengthQueryBuff.append(" ").append(newFromClause);
			
			// Process WHERE clause with parameters
			String whereClause = getWhereClause(tempQuery);
			//System.out.println("whereClause: " + whereClause);			
			String newWhereClause = proccessWhereHavingClause(whereClause);
			//System.out.println("newWhereClause: " + newWhereClause);
			getMaxLengthQueryBuff.append(" ").append(newWhereClause);
			
			// Process HAVING clause with parameters
			String havingClause = getHavingClause(tempQuery);
			//System.out.println("havingClause: " + havingClause);			
			String newHavingClause = proccessWhereHavingClause(havingClause);
			//System.out.println("newHavingClause: " + newHavingClause);
			getMaxLengthQueryBuff.append(" ").append(newHavingClause);			

			String result = getMaxLengthQueryBuff.toString();
			//System.out.println("getMaxLengthQueryBuff: " + getMaxLengthQueryBuff);
			
			return result;
		}
		
		return null;
	}
	
	// further process select fields to be more fine grained fields
	public static String[] proccessSelectFields(String selectFields)
	{
		String[] newSelectFields = null;
		if (selectFields != null)
		{
			try
			{
				final String REPLACER = "$REPLACER$_";
				String tmpSelectFields = selectFields;
				Vector<String> replacementHolders = new Vector<String>();
				tmpSelectFields = tmpSelectFields.replaceAll("[ ]+\\(", "(");
				tmpSelectFields = tmpSelectFields.replaceAll("\\([ ]+", "(");
				tmpSelectFields = tmpSelectFields.replaceAll("[ ]+\\)", ")");
				//tmpSelectFields = tmpSelectFields.replaceAll("\\)[ ]+", ")");
				String functionPartern = "[\\(][^\\(\\)]*[\\)]";
	
				int index = 0;
				boolean end = false;
				Pattern functionPattern = Pattern.compile(functionPartern);
				while (!end)
				{				
					Matcher matcher = functionPattern.matcher(tmpSelectFields);			
					while (matcher.find())
					{
						String match = matcher.group();						
						replacementHolders.add(match);
						tmpSelectFields = replaceFirst(tmpSelectFields, match, REPLACER + index + "$");
						index += 1;
						//System.out.println("match " + index + ": " + match );
					}
					matcher = functionPattern.matcher(tmpSelectFields);	
					if (!matcher.find())
					{
						end = true;
					}
				}
				
				
				newSelectFields = tmpSelectFields.split(",");
				//System.out.println("select fields: " + tmpSelectFields + " . newSelectFields size = " + newSelectFields.length);
				for (int i = 0; i < newSelectFields.length; i++)
				{
					// remove any alias with out AS here
					String newSelectField = newSelectFields[i].trim();
					
					// handle the set-funtions. First going with ALL and DISTINCT
					if (newSelectField.indexOf("ALL ") == 0 || newSelectField.indexOf("ALL" + REPLACER) == 0)
					{
						newSelectField = newSelectField.substring("ALL".length());
					}
					else if (newSelectField.indexOf("DISTINCT ") == 0 || newSelectField.indexOf("DISTINCT" + REPLACER) == 0)
					{
						newSelectField = newSelectField.substring("DISTINCT".length());
					}
					newSelectField = newSelectField.trim();
					// AVG | MAX | MIN | SUM | COUNT 
					if (newSelectField.indexOf("COUNT"+ REPLACER) == 0 ||
							newSelectField.indexOf("AVG"+ REPLACER) == 0 ||
							newSelectField.indexOf("MAX"+ REPLACER) == 0 ||
							newSelectField.indexOf("MIN"+ REPLACER) == 0 ||
							newSelectField.indexOf("SUM"+ REPLACER) == 0 ||
							newSelectField.indexOf("TO_NUMBER"+ REPLACER) == 0)
					{
						newSelectField = "'##########'";
					}					
					if (newSelectField.indexOf("CASE ") == 0)	// this i Case statement
					{
						
					}
					else 
					{
						
						// if newSelectField is the last field, then no change as it has been gone with AS
						if (i < newSelectFields.length - 1)
						{
							// incase of alias with out AS, find the alias by checking if it is in "" or ''
							//newSelectField = newSelectField.substring(0, newSelectField.indexOf(" "));
							if(newSelectField.charAt(newSelectField.length() - 1) == '\'')
							{
								String tmpNewSelectField = newSelectField.substring(0, newSelectField.length() - 1);
								if (tmpNewSelectField.lastIndexOf(" '") > 0)
								{
									newSelectField = tmpNewSelectField.substring(0, tmpNewSelectField.lastIndexOf(" '"));
								}
							}
							else if (newSelectField.charAt(newSelectField.length() - 1) == '"')
							{
								String tmpNewSelectField = newSelectField.substring(0, newSelectField.length() - 1);
								if (tmpNewSelectField.lastIndexOf(" \"") > 0)
								{
									newSelectField = tmpNewSelectField.substring(0, tmpNewSelectField.lastIndexOf(" \""));
								}								
							}
							else
							{
								if (newSelectField.lastIndexOf(" ") > 0)
								{
									newSelectField = newSelectField.substring(0, newSelectField.lastIndexOf(" "));
								}						
							}
						}
					}

					
					//if (newSelectField)
					boolean replacerExist = (newSelectField.indexOf(REPLACER) > -1);
					
					while (replacerExist)
					{					
						for (int j = 0; j < replacementHolders.size(); j++)
						{						
							String replacement = replacementHolders.elementAt(j);
							if (newSelectField.indexOf(REPLACER + j + "$") > -1)
							{
								newSelectField = newSelectField.replace(REPLACER + j + "$", replacement);
								//System.out.println("replace " + (REPLACER + j + "$") + " by " + replacement);
							}
						}
						replacerExist = (newSelectField.indexOf(REPLACER) > -1);
					}
					//System.out.println("newSelectField: " +newSelectField);
					// update to array
					newSelectFields[i] = newSelectField;
				}
			}
			catch (Exception e)
			{
				LOG.error("Error while process select fields: " + e);
			}
	
			
		}
		
		return newSelectFields;
	}
	
	/**
	 * Replace any occurence of paramter in condtion by TRUE (1=1) condition
	 * @param fromClause
	 * @return
	 */
	public String proccessFromClause(String fromClause)
	{
		String tmpFromClause = fromClause;
		StringBuffer newFromClauseBuff = new StringBuffer();
		if (fromClause != null && fromClause.trim().length() > 0)
		{
			try
			{
				fromClause = fromClause.trim();
				if (fromClause.indexOf("FROM ") == 0)
				{
					tmpFromClause = fromClause.substring("FROM ".length());
					newFromClauseBuff.append("FROM ");
				}
					
				final String REPLACER = "$REPLACERFROM$_";
				Vector<String> replacementHolders = new Vector<String>();
				 
//				tmpFromClause = tmpFromClause.replaceAll("[ ]+\\(", "(");
//				tmpFromClause = tmpFromClause.replaceAll("\\([ ]+", "(");
//				tmpFromClause = tmpFromClause.replaceAll("[ ]+\\)", ")");
				//tmpFromClause = tmpFromClause.replaceAll("\\)[ ]+", ")");
				String functionPartern = "[\\(][^\\(\\)]*[\\)]";
				Pattern parameterPattern = Pattern.compile(COMMON_PARAMETER_PATTERN);
				int index = 0;
				boolean end = false;
				Pattern functionPattern = Pattern.compile(functionPartern);
				while (!end)
				{				
					Matcher matcher = functionPattern.matcher(tmpFromClause);			
					while (matcher.find())
					{
						String match = matcher.group();						
						String tmpMatch = match.trim();
						Matcher paramMatcher = parameterPattern.matcher(tmpMatch);
						// process the match here if it is a subquery - start with SELECT
						// process only where and having clause
						if (tmpMatch.indexOf("(SELECT ") == 0)
						{
							String insideTmpMatch = tmpMatch.substring(1, tmpMatch.length() - 1);
							//System.out.println("Subquery enter ---> " + insideTmpMatch);
							String whereClause = getWhereClause(insideTmpMatch);
							//System.out.println("whereClause ---> " + whereClause);
							String havingClause = getHavingClause(insideTmpMatch);
							if (whereClause != null && whereClause.trim().length() > 0)
							{
								String newWhereClause = proccessWhereHavingClause(whereClause);
								//System.out.println("newWhereClause ---> " + newWhereClause);
								tmpMatch = tmpMatch.replace(whereClause.trim(), newWhereClause);
							}
							if (havingClause != null && havingClause.trim().length() > 0)
							{
								String newHavingClause = proccessWhereHavingClause(havingClause);
								tmpMatch = tmpMatch.replace(havingClause.trim(), newHavingClause);
							}							

							replacementHolders.add(tmpMatch);
							tmpFromClause = replaceFirst(tmpFromClause, match, REPLACER + index + "$");
							index += 1;
						}
						else if (paramMatcher.find())
						{
							//System.out.println("Parameter enter --->");
							tmpFromClause = replaceFirst(tmpFromClause, match, " " + tmpMatch.substring(1, tmpMatch.length() - 1) + " ");
						}
						else
						{
							//System.out.println("General enter --->");
							replacementHolders.add(match);
							tmpFromClause = replaceFirst(tmpFromClause, match, REPLACER + index + "$");
							index += 1;
						}
						
						
						//System.out.println("match " + index + ": " + match );
					}
					matcher = functionPattern.matcher(tmpFromClause);	
					if (!matcher.find())
					{
						end = true;
					}
				}
				
				//System.out.println("tmpFromClause: " + tmpFromClause);
				//System.out.println("Holder size = " + replacementHolders.size());				
				
				String[] tableParts = tmpFromClause.split(",");
				for (int i = 0; i < tableParts.length; i++)
				{
					String tablePart = tableParts[i].trim();					
					//System.out.println("tablePart: " + tablePart);
					
					//if (newSelectField)
					boolean replacerExist = (tablePart.indexOf(REPLACER) > -1);
					while (replacerExist)
					{					
						for (int j = 0; j < replacementHolders.size(); j++)
						{						
							String replacement = replacementHolders.elementAt(j);
							
							if (tablePart.indexOf(REPLACER + j + "$") > -1)
							{
								//System.out.println("replacement: " + j + " - " + replacement);
								tablePart = tablePart.replace(REPLACER + j + "$", replacement);
								//System.out.println("replace " + (REPLACER + j + "$") + " by " + replacement);
							}
						}
						replacerExist = (tablePart.indexOf(REPLACER) > -1);
					}
					
					if (i < tableParts.length - 1)
					{						
						newFromClauseBuff.append(tablePart).append(",");						
					}
					else
					{
						newFromClauseBuff.append(tablePart);
					}
				}
			}
			catch (Exception e)
			{
				LOG.error("Error while process select fields: " + e);
			}
			
		}
		
		return newFromClauseBuff.toString();
	}	
	
	/**
	 * Replace any occurence of paramter in condtion by TRUE (1=1) condition
	 * @param whereClause
	 * @return
	 */
	public String proccessWhereHavingClause(String whereClause)
	{
		String tmpWhereClause = whereClause;
		StringBuffer newWhereClauseBuff = new StringBuffer();
		if (whereClause != null && whereClause.trim().length() > 0)
		{
//			Pattern variableComparePattern = Pattern.compile(REGEX_PARAM_PATTERN_WITH_COMPARATOR);		
//			Matcher matcher1 = variableComparePattern.matcher(newFromClause);			
//			while (matcher1.find())
//			{
//				String match = matcher1.group();						
//				newFromClause = newFromClause.replace(match, " LIKE '%'");
//			}	
//			
//			// check if paramter exists in a function
//			Pattern variableFunctionPattern = Pattern.compile(REGEX_PARAM_PATTERN_WITH_FUNCTION);		
//			Matcher matcher2 = variableFunctionPattern.matcher(newFromClause);			
//			while (matcher2.find())
//			{
//				String match = matcher2.group();						
//				newFromClause = newFromClause.replace(match, " LIKE '%'");
//			}	
//			
//			// replace any left paramters with ''
//			// check if paramter exists in a function
//			Pattern variablePattern = Pattern.compile(REGEX_PARAM_PATTERN);		
//			Matcher matcher3 = variablePattern.matcher(newFromClause);			
//			while (matcher3.find())
//			{
//				String match = matcher3.group();						
//				newFromClause = newFromClause.replace(match, "''");
//			}	
			try
			{
				whereClause = whereClause.trim();
				if (whereClause.indexOf("WHERE ") == 0)
				{
					tmpWhereClause = whereClause.substring("WHERE ".length());
					newWhereClauseBuff.append("WHERE ");
				}
				else if (whereClause.indexOf("HAVING ") == 0)
				{
					tmpWhereClause = whereClause.substring("HAVING ".length());
					newWhereClauseBuff.append("HAVING ");
				}
				
				final String FUNCTION_REPLACER = "$FU_REPLACER$_";
				Vector<String> replacementHolders = new Vector<String>();
				 
//				tmpNewFromClause = tmpNewFromClause.replaceAll("[ ]+\\(", "(");
//				tmpNewFromClause = tmpNewFromClause.replaceAll("\\([ ]+", "(");
//				tmpNewFromClause = tmpNewFromClause.replaceAll("[ ]+\\)", ")");
//				tmpNewFromClause = tmpNewFromClause.replaceAll("\\)[ ]+", ")");
				String functionPartern = "[\\(][^\\(\\)]*[\\)]";
				Pattern parameterPattern = Pattern.compile(COMMON_PARAMETER_PATTERN);
				int index = 0;
				boolean end = false;
				Pattern functionPattern = Pattern.compile(functionPartern);
				while (!end)
				{				
					Matcher matcher = functionPattern.matcher(tmpWhereClause);			
					while (matcher.find())
					{
						String match = matcher.group();
						replacementHolders.add(match);
						
						tmpWhereClause = replaceFirst(tmpWhereClause, match, FUNCTION_REPLACER + index + "$");
						index += 1;
						//System.out.println("match " + index + ": " + match );
					}
					matcher = functionPattern.matcher(tmpWhereClause);	
					if (!matcher.find())
					{
						end = true;
					}
				}
				
				
				Vector<String> conditionerHolders = new Vector<String>(); // store AND and OR in the sequence of displayed
				Pattern conditionerPattern = Pattern.compile(CONDITIONER_PARTERN_ANDOR);
				Matcher conditionerMatcher = conditionerPattern.matcher(tmpWhereClause);			
				while (conditionerMatcher.find())
				{
					String match = conditionerMatcher.group();						
					conditionerHolders.add(match);
				}
				
				//System.out.println("tmpWhereClause: " + tmpWhereClause);
				//System.out.println("Holder size = " + replacementHolders.size()+ " . Conditioner size = " + conditionerHolders.size());				
				
				String[] conditionParts = tmpWhereClause.split(CONDITIONER_PARTERN_ANDOR);
				for (int i = 0; i < conditionParts.length; i++)
				{
					// remove any NOT
					String conditionPart = conditionParts[i].trim();
					// handle the set-funtions. First going with ALL and DISTINCT
					if (conditionPart.indexOf("NOT ") == 0 || conditionPart.indexOf("NOT" + FUNCTION_REPLACER) == 0)
					{
						conditionPart = conditionPart.substring("NOT".length());
					}

					conditionPart = conditionPart.trim();
					
					
					//if (newSelectField)
					boolean replacerExist = (conditionPart.indexOf(FUNCTION_REPLACER) > -1);
					while (replacerExist)
					{					
						for (int j = 0; j < replacementHolders.size(); j++)
						{						
							String replacement = replacementHolders.elementAt(j);
							if (conditionPart.indexOf(FUNCTION_REPLACER + j + "$") > -1)
							{
								conditionPart = conditionPart.replace(FUNCTION_REPLACER + j + "$", replacement);
								//System.out.println("replace " + (REPLACER + j + "$") + " by " + replacement);
							}
						}
						replacerExist = (conditionPart.indexOf(FUNCTION_REPLACER) > -1);
					}
					
					Matcher condtionMatcher = parameterPattern.matcher(conditionPart);
					if (i < conditionerHolders.size())
					{						
						// if parmater found in a conditionPart, replaced by 1=1
						if (condtionMatcher.find())
						{
							newWhereClauseBuff.append("(1=1)").append(conditionerHolders.elementAt(i));
						}
						else
						{
							newWhereClauseBuff.append(conditionPart).append(conditionerHolders.elementAt(i));
						}						
					}
					else
					{
						// if parmater found in a conditionPart, replaced by 1=1
						if (condtionMatcher.find())
						{
							newWhereClauseBuff.append("(1=1)");
						}
						else
						{
							newWhereClauseBuff.append(conditionPart);
						}
					}
				}
			}
			catch (Exception e)
			{
				LOG.error("Error while process select fields: " + e);
			}
			
		}
		
		return newWhereClauseBuff.toString();
	}
	
	private static String replaceFirst(String source, String target, String replacement)
	{
		String newSource = source;
		if (source != null && target != null && replacement != null)
		{
			int start = source.indexOf(target);
			if (start > -1)
			{
				newSource = source.substring(0, start) + replacement + source.substring(start + target.length());
			}
		}
		
		return newSource;
	}
	
	public static String getSelectClause(String query)
	{
		String selectClause = null;

		if (query != null)
		{
			String tmpQuery = " " + query;
			int selectIndex = tmpQuery.toUpperCase().indexOf(" SELECT ");
			int fromIndex = tmpQuery.toUpperCase().indexOf(" FROM ");
			
			boolean selectClauseExist = (selectIndex > -1 && fromIndex > -1);
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (selectClauseExist)
			{
				selectClause = tmpQuery.substring(selectIndex + " SELECT ".length(), fromIndex);
				
//				tmpQuery = tmpQuery.substring(fromIndex + " FROM ".length());
//				
//				selectIndex = tmpQuery.toUpperCase().indexOf(" SELECT ");
//				fromIndex = tmpQuery.toUpperCase().indexOf(" FROM ");
//				
//				selectClauseExist = (selectIndex > -1 && fromIndex > -1);				
			}
		
		}	
		
		return selectClause;
	}
	
	public static String getBeforeSelectClause(String query)
	{
		String beforeSelectClause = null;

		if (query != null)
		{
			String tmpQuery = " " + query;
			int selectIndex = tmpQuery.toUpperCase().indexOf(" SELECT ");
			
			boolean selectClauseExist = (selectIndex > -1);
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (selectClauseExist)
			{
				beforeSelectClause = tmpQuery.substring(0, selectIndex);							
			}
		
		}	
		
		return beforeSelectClause;
	}	
	
	public static String getAfterSelectClause(String query)
	{
		String afterSelectClause = null;

		if (query != null)
		{
			String tmpQuery = " " + query;
			int selectIndex = tmpQuery.toUpperCase().indexOf(" SELECT ");
			int fromIndex = tmpQuery.toUpperCase().indexOf(" FROM ");
			
			boolean selectClauseExist = (selectIndex > -1 && fromIndex > -1);
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (selectClauseExist)
			{
				afterSelectClause = tmpQuery.substring(fromIndex + " FROM ".length());			
			}
		
		}	
		
		return afterSelectClause;
	}	
	
	/**
	 * Check if a clause is valid that is it has welformed with ( ), " ", ' '. This function is not correct 100% for all queries
	 * @param clause
	 * @return
	 */
	private static boolean isValidClause(String clause)
	{
		if (clause != null)
		{
			int numberOfLeftPathen = 0;
			int numberOfRightPathen = 0;
			
			for (int i = 0; i < clause.length(); i++)
			{
				char c = clause.charAt(i);
				if (c == '(') numberOfLeftPathen += 1;
				else if (c == ')') numberOfRightPathen += 1;
			}
			
//			int numberOfDoubleQuote = 0;
//			
//			for (int i = 0; i < clause.length(); i++)
//			{
//				char c = clause.charAt(i);
//				if (c == '"') numberOfDoubleQuote += 1;
//			}
//			
//			int numberOfSingleQuote = 0;
//			
//			for (int i = 0; i < clause.length(); i++)
//			{
//				char c = clause.charAt(i);
//				if (c == '\'') numberOfSingleQuote += 1;
//			}			
					
//			return ((numberOfLeftPathen == numberOfRightPathen) && (numberOfDoubleQuote % 2 == 0) && (numberOfSingleQuote % 2 == 0));
			return (numberOfLeftPathen == numberOfRightPathen);
		}
		
		return false;
	}
	
	/**
	 * Get query part which belong to FROM key word. This usually limited by FROM to WHERE/GROUP BY/HAVING. The query
	 * input should be cut off of ORDER BY and UNION/MINUS/INTERSECT first.
	 * @param query
	 * @return
	 */
	public static String getFromClause(String query)
	{
		if (query != null)
		{
			int fromIndex = query.indexOf(" FROM ");
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (fromIndex > 0)
			{
				String beforeFromText = query.substring(0, fromIndex);
				boolean isValidRootKeyword = isValidClause(beforeFromText);
				while (!isValidRootKeyword)
				{
					fromIndex = query.indexOf(" FROM ", fromIndex + " FROM ".length());
					if (fromIndex > 0)
					{					
						beforeFromText = query.substring(0, fromIndex);
						isValidRootKeyword = isValidClause(beforeFromText);
					}
					else
					{
						break;
					}					
				}	
				if (isValidRootKeyword && fromIndex > 0)
				{
					String fromClause = query.substring(fromIndex);
					
					boolean foundWhere = false;
					int whereIndex = fromClause.indexOf(" WHERE ");
					
					// check if FROM here is used for SELECT by checking if it is in ( ) pair
					if (whereIndex > 0)
					{
						String beforeWhereText = fromClause.substring(0, whereIndex);
						boolean isValidRootKeyword2 = isValidClause(beforeWhereText);
						while (!isValidRootKeyword2)
						{
							whereIndex = fromClause.indexOf(" WHERE ", whereIndex + " WHERE ".length());
							if (whereIndex > 0)
							{					
								beforeWhereText = fromClause.substring(0, whereIndex);
								isValidRootKeyword2 = isValidClause(beforeWhereText);
							}
							else
							{
								break;
							}					
						}
						if (isValidRootKeyword2 && whereIndex > 0)						
						{
							foundWhere = true;
							fromClause = fromClause.substring(0, whereIndex);
						}
					}
					
					if (foundWhere)
					{
						return fromClause;
					}
					
					boolean foundGroupBy = false;
					int groupByIndex = fromClause.indexOf(" GROUP BY ");
					
					// check if FROM here is used for SELECT by checking if it is in ( ) pair
					if (groupByIndex > 0)
					{
						String beforeGroupByText = fromClause.substring(0, groupByIndex);
						boolean isValidRootKeyword2 = isValidClause(beforeGroupByText);
						while (!isValidRootKeyword2)
						{
							groupByIndex = fromClause.indexOf(" GROUP BY ", groupByIndex + " GROUP BY ".length());
							if (groupByIndex > 0)
							{					
								beforeGroupByText = fromClause.substring(0, groupByIndex);
								isValidRootKeyword2 = isValidClause(beforeGroupByText);
							}
							else
							{
								break;
							}					
						}
						if (isValidRootKeyword2 && groupByIndex > 0)						
						{
							foundGroupBy = true;
							fromClause = fromClause.substring(0, groupByIndex);
						}
					}
					
					if (foundGroupBy)
					{
						return fromClause;
					}
					
					int havingIndex = fromClause.indexOf(" HAVING ");
					
					// check if FROM here is used for SELECT by checking if it is in ( ) pair
					if (havingIndex > 0)
					{
						String beforeGroupByText = fromClause.substring(0, havingIndex);
						boolean isValidRootKeyword2 = isValidClause(beforeGroupByText);
						while (!isValidRootKeyword2)
						{
							havingIndex = fromClause.indexOf(" HAVING ", havingIndex + " HAVING ".length());
							if (havingIndex > 0)
							{					
								beforeGroupByText = fromClause.substring(0, havingIndex);
								isValidRootKeyword2 = isValidClause(beforeGroupByText);
							}
							else
							{
								break;
							}					
						}
						if (isValidRootKeyword2 && havingIndex > 0)						
						{
							fromClause = fromClause.substring(0, havingIndex);
						}
					}
					
					return fromClause;
				}
			}		
		}
		
		return null;
	}
	
	/**
	 * Get query part which belong to WHERE key word. This usually limited by WHERE to GROUP BY/HAVING. The query
	 * input should be cut off of ORDER BY and UNION/MINUS/INTERSECT first.
	 * @param query
	 * @return
	 */
	public static String getWhereClause(String query)
	{
		if (query != null)
		{
			int whereIndex = query.indexOf(" WHERE ");
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (whereIndex > 0)
			{
				String beforeWhereText = query.substring(0, whereIndex);
				boolean isValidRootKeyword = isValidClause(beforeWhereText);
				while (!isValidRootKeyword)
				{
					whereIndex = query.indexOf(" WHERE ", whereIndex + " WHERE ".length());
					if (whereIndex > 0)
					{					
						beforeWhereText = query.substring(0, whereIndex);
						isValidRootKeyword = isValidClause(beforeWhereText);
					}
					else
					{
						break;
					}					
				}	
				if (isValidRootKeyword && whereIndex > 0)
				{
					String whereClause = query.substring(whereIndex);					
					
					boolean foundGroupBy = false;
					int groupByIndex = whereClause.indexOf(" GROUP BY ");
					
					// check if FROM here is used for SELECT by checking if it is in ( ) pair
					if (groupByIndex > 0)
					{
						String beforeGroupByText = whereClause.substring(0, groupByIndex);
						boolean isValidRootKeyword2 = isValidClause(beforeGroupByText);
						while (!isValidRootKeyword2)
						{
							groupByIndex = whereClause.indexOf(" GROUP BY ", groupByIndex + " GROUP BY ".length());
							if (groupByIndex > 0)
							{					
								beforeGroupByText = whereClause.substring(0, groupByIndex);
								isValidRootKeyword2 = isValidClause(beforeGroupByText);
							}
							else
							{
								break;
							}					
						}
						if (isValidRootKeyword2 && groupByIndex > 0)						
						{
							foundGroupBy = true;
							whereClause = whereClause.substring(0, groupByIndex);
						}
					}
					
					if (foundGroupBy)
					{
						return whereClause;
					}
					
					int havingIndex = whereClause.indexOf(" HAVING ");
					
					// check if FROM here is used for SELECT by checking if it is in ( ) pair
					if (havingIndex > 0)
					{
						String beforeGroupByText = whereClause.substring(0, havingIndex);
						boolean isValidRootKeyword2 = isValidClause(beforeGroupByText);
						while (!isValidRootKeyword2)
						{
							havingIndex = whereClause.indexOf(" HAVING ", havingIndex + " HAVING ".length());
							if (havingIndex > 0)
							{					
								beforeGroupByText = whereClause.substring(0, havingIndex);
								isValidRootKeyword2 = isValidClause(beforeGroupByText);
							}
							else
							{
								break;
							}					
						}
						if (isValidRootKeyword2 && havingIndex > 0)						
						{
							whereClause = whereClause.substring(0, havingIndex);
						}
					}
					
					return whereClause;
				}
			}		
		}
		
		return "";
	}	
	
	/**
	 * Get the part of the query with HAVING conditions. Usually, this is the last part of the query after removing
	 * any ORDER BY and UNION/MINUS/INTERSECT
	 * @param query
	 * @return
	 */
	public static String getHavingClause(String query)
	{
		if (query != null)
		{
			int havingIndex = query.indexOf(" HAVING ");
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (havingIndex > 0)
			{
				String beforeHavingText = query.substring(0, havingIndex);
				
				boolean isValidRootKeyword = isValidClause(beforeHavingText);
				while (!isValidRootKeyword)
				{
					havingIndex = query.indexOf(" HAVING ", havingIndex + " HAVING ".length());
					if (havingIndex > 0)
					{					
						beforeHavingText = query.substring(0, havingIndex);
						isValidRootKeyword = isValidClause(beforeHavingText);
					}
					else
					{
						break;
					}					
				}	
				if (isValidRootKeyword && havingIndex > 0)
				{
					return query.substring(havingIndex);
				}
			}		
		}
		
		return "";
	}	
	
	public static String removeGroupByClause(String query)
	{
		String newQuery = query;
		if (newQuery != null)
		{
			int groupByIndex = newQuery.indexOf(" GROUP BY ");
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (groupByIndex > 0)
			{
				String beforeGroupByText = newQuery.substring(0, groupByIndex);
				boolean isValidRootKeyword = isValidClause(beforeGroupByText);
				while (!isValidRootKeyword)
				{
					groupByIndex = newQuery.indexOf(" GROUP BY ", groupByIndex + " GROUP BY ".length());
					if (groupByIndex > 0)
					{					
						beforeGroupByText = newQuery.substring(0, groupByIndex);
						isValidRootKeyword = isValidClause(beforeGroupByText);
					}
					else
					{
						break;
					}
				}	
				
				if (isValidRootKeyword && groupByIndex > 0)
				{
					newQuery =  newQuery.substring(0, groupByIndex);
				}
			}	
		}
		
		return newQuery;
	}	
	
	public static String removeOrderByClause(String query)
	{
		String newQuery = query;
		if (newQuery != null)
		{
			int orderByIndex = newQuery.indexOf(" ORDER BY ");
			
			// check if FROM here is used for SELECT by checking if it is in ( ) pair
			if (orderByIndex > 0)
			{
				String beforeOrderByText = newQuery.substring(0, orderByIndex);
				boolean isValidRootKeyword = isValidClause(beforeOrderByText);
				while (!isValidRootKeyword)
				{
					orderByIndex = newQuery.indexOf(" ORDER BY ", orderByIndex + " ORDER BY ".length());
					if (orderByIndex > 0)
					{					
						beforeOrderByText = newQuery.substring(0, orderByIndex);
						isValidRootKeyword = isValidClause(beforeOrderByText);
					}
					else
					{
						break;
					}					
				}	
				if (isValidRootKeyword  && orderByIndex > 0)
				{
					newQuery =  newQuery.substring(0, orderByIndex);
				}							
			}				
		}
		
		return newQuery;
	}		
	
	public static String formatSql(String query){
		StringBuilder sbFormated = new StringBuilder();
		String[] lines = query.split("\n");
		for(String line : lines){
			line = line.trim();
			if(line.isEmpty() || line.startsWith("--")){
				continue;
			}
			line = line.replaceAll("\t", " ");
			sbFormated.append(line).append(" ");
		}
		String formated = sbFormated.toString();
/*		if(!formated.isEmpty() && !formated.endsWith(";")){
			formated += ";";
		}
*/		return formated;
	}
	
	// Change the query which has UNION, MINUS, INTERSECT to a more simple query
	public static String removeUnionMiniusIntersectQuery(String query)
	{
		if (query != null)
		{
			String tempQuery = query.replaceAll("\\s+", " ");
			tempQuery = tempQuery.trim();
			
			boolean end = false;
			Pattern functionPattern = Pattern.compile(SET_OPERATOR_PARTERN);
			while (!end)
			{				
				Matcher matcher = functionPattern.matcher(tempQuery);			
				while (matcher.find())
				{
					String match = matcher.group();						
					tempQuery = replaceFirst(tempQuery, match, " $SET_OPERATOR$ ");
					//System.out.println("match " + match );
				}
				matcher = functionPattern.matcher(tempQuery);	
				if (!matcher.find())
				{
					end = true;
				}
			}
									
			//System.out.println("tempQuery: " + tempQuery);
			int setOperIndex = tempQuery.indexOf(" $SET_OPERATOR$ ");
			
			// check if SET OPERATOR  here is root by checking if it is in ( ) pair
			if (setOperIndex > 0)
			{
				String beforeSetOperText = tempQuery.substring(0, setOperIndex);
				boolean isValidRootKeyword = isValidClause(beforeSetOperText);
				while (!isValidRootKeyword)
				{
					setOperIndex = tempQuery.indexOf(" $SET_OPERATOR$ ", setOperIndex + " $SET_OPERATOR$ ".length());
					if (setOperIndex > 0)
					{					
						beforeSetOperText = tempQuery.substring(0, setOperIndex);
						isValidRootKeyword = isValidClause(beforeSetOperText);
					}
					else
					{
						break;
					}
				}	
						
				if (isValidRootKeyword  && setOperIndex > 0)
				{
					String result = tempQuery.substring(0, setOperIndex);
					result = result.replace(" $SET_OPERATOR$ ", " UNION ");	// all converted back to UNION. 
					
					return result;
				}

			}		
		}
		
		return query;
	}
	
	public static void main(String[] args) throws IOException
	{
		System.out.println(System.currentTimeMillis());
		File inputFile = new File("F:/test2.txt");
		FileInputStream inputStream = new FileInputStream(inputFile);
		byte[] contentBytes = new byte[(int)inputFile.length()];
		inputStream.read(contentBytes);
		String query = new String(contentBytes);
		
//		QueryAnalizer queryAnalizer = new QueryAnalizer(query);
//		
////		String selectClause = "SELECT " + queryAnalizer.getSelectClauseParts()[0][1] + " FROM ";
////		System.out.println("selectClause:\n" + selectClause);
//		ArrayList<String> params = extractQueryParams(query);
//		for (int i = 0; i < params.size(); i++)
//		{
//			System.out.println("Param: " + params.get(i));
//		}
//		
//		String jasperQuery = formatToJasperQuery(query);
//		System.out.println("jasperQuery: " + jasperQuery);
		String penetrationQuery = QueryAnalizer.formatToPenatrationQuery(query);
		System.out.println("penetrationQuery: " + penetrationQuery);
//		
//		String newQuery = queryAnalizer.getRemovedDuplicateColQuery();
//		String[][] colNamesAndAliases = queryAnalizer.getFormarlizedColumns();
//		for (int i = 0; i < colNamesAndAliases.length; i++)
//		{
//			System.out.println("col name: " + colNamesAndAliases[i][0]);
//			System.out.println("col name: " + colNamesAndAliases[i][1]);
//		}
		
		//queryAnalizer.remo
//		System.out.println("Removed comments:\n" + removedComments);
//		System.out.println("IS SELECT QUERY: " + isSelectQuery(removedComments));
		
		//System.out.println(removeNullAsBlankColumns(removedComments));
		
		//String updatedSelectClause = QueryUtilizer.processToAlias(selectClause);
		
		//System.out.println("UPDATE QUERY:\n" + removedComments);
		
		long lastRunLong = System.currentTimeMillis();
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss");
		String lastRunFormatted = formater.format(new Date(lastRunLong)) + " at " + formater2.format(new Date(lastRunLong));
		
		System.out.println("date: " + lastRunFormatted);
		
	}
	
//	public void testFromClause (String query)
//	{
//		
//		String tempQuery = query.replaceAll("\\s+", " ");
//		tempQuery = tempQuery.replaceAll("\\)", ") ");
//		tempQuery = tempQuery.toUpperCase();
//		tempQuery = tempQuery.trim();
//		
//		tempQuery = removeOrderByClause(tempQuery);
//		tempQuery = removeUnionMiniusIntersectQuery(tempQuery);
//		
//		String fromClause = getFromClause(tempQuery);
//		System.out.println("fromClause: " + fromClause);
//		String newFromClause = proccessFromClause(fromClause);
//		
//		System.out.println("newFromClause: " + newFromClause);
//	}
	
	/**
	 * Extract the column set that is contained in SELECT ... FROM.. clause
	 */
	private String[] extractColumnSets (String aSelectClause)
	{
		String[] columns = null;
		
		if (aSelectClause != null)
		{
			try
			{
				// Normalize the query first so that it has the format of
				// Select c1, c2,  from ... with out any double quotes and function
				aSelectClause = aSelectClause.replaceAll("[\\s]", " ");
				//newQuery = newQuery.replaceAll("\n", " ");
				//newQuery = newQuery.replaceAll("\t", " ");
				//newQuery = newQuery.replaceAll("\\)", ") ");
				
				// Normalize the double quotes first
				final String DOUBLEQUOTE_REPLACER = "$DQ_REPLACER$_";
				Vector<String> doubleQuoteReplacementHolders = new Vector<String>();
				int leftDoubleQuoteIndex = aSelectClause.indexOf('"');
				int rightDoubleQuoteIndex = aSelectClause.indexOf('"', leftDoubleQuoteIndex + 1);
				
				StringBuffer replacedStringBuffer = new StringBuffer();
				boolean isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
				int phaseIndex = 0;
				while (isDoubleQuoteToReplace)
				{
					// store the string inside double quotes in to the array
					String storePhase = aSelectClause.substring(leftDoubleQuoteIndex, rightDoubleQuoteIndex + 1);
					doubleQuoteReplacementHolders.add(storePhase);
					
					// append the left string to the replacement
					replacedStringBuffer.append(aSelectClause.substring(0, leftDoubleQuoteIndex)).append(DOUBLEQUOTE_REPLACER + phaseIndex + "$");
					phaseIndex += 1;
					
					// process the next part
					aSelectClause = aSelectClause.substring(rightDoubleQuoteIndex + 1);
					leftDoubleQuoteIndex = aSelectClause.indexOf('"');
					rightDoubleQuoteIndex = aSelectClause.indexOf('"', leftDoubleQuoteIndex + 1);
					
					isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
				}
				
				replacedStringBuffer.append(aSelectClause);
				
				aSelectClause = replacedStringBuffer.toString();				
				
				final String FUNCTION_REPLACER = "$FU_REPLACER$_";			
				Vector<String> functionReplacementHolders = new Vector<String>();
//				newQuery = newQuery.replaceAll("[ ]+\\(", " (");
//				newQuery = newQuery.replaceAll("\\([ ]+", "(");
//				newQuery = newQuery.replaceAll("[ ]+\\)", ")");
//				newQuery = newQuery.replaceAll("\\)[ ]+", ") ");
				String functionPartern = "[\\(][^\\(\\)]*[\\)]";
				
				int index = 0;
				boolean end = false;
				Pattern functionPattern = Pattern.compile(functionPartern);
				while (!end)
				{				
					Matcher matcher = functionPattern.matcher(aSelectClause);			
					while (matcher.find())
					{
						String match = matcher.group();						
						functionReplacementHolders.add(match);
						aSelectClause = replaceFirst(aSelectClause, match, FUNCTION_REPLACER + index + "$");
						index += 1;
					}
					
					matcher = functionPattern.matcher(aSelectClause);
					if (!matcher.find())
					{
						end = true;
					}
				}
				
				// get the string between Select and from
				columns = splitColumns(aSelectClause);
				
				for (int i = 0; i < columns.length; i++)
				{
					String column = columns[i];
					
					
					boolean functionReplacerExist = (column.indexOf(FUNCTION_REPLACER) > -1);
					while (functionReplacerExist)
					{					
						for (int j = 0; j < functionReplacementHolders.size(); j++)
						{						
							String replacement = functionReplacementHolders.elementAt(j);
							if (column.indexOf(FUNCTION_REPLACER + j + "$") > -1)
							{				
								column = column.replace(FUNCTION_REPLACER + j + "$", replacement);							
							}
						}
						functionReplacerExist = (column.indexOf(FUNCTION_REPLACER) > -1);
					}						
					
					boolean dobleQuoteReplacerExist = (column.indexOf(DOUBLEQUOTE_REPLACER) > -1);
					while (dobleQuoteReplacerExist)
					{					
						for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
						{						
							String replacement = doubleQuoteReplacementHolders.elementAt(j);
							if (column.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
							{				
								column = column.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
							}
						}
						dobleQuoteReplacerExist = (column.indexOf(DOUBLEQUOTE_REPLACER) > -1);
					}
					
					columns[i] = column;
				}
			}
			catch (Exception e)
			{
				//e.printStackTrace();
				LOG.error("Error while process from clause to add security filter: " + e);
			}				
		}	
		
		return columns;
	}
	
	/**
	 * Remove duplicate columns by appeding number after the column name
	 * @param columns
	 * @return a array of column and alias name
	 */
	private String[][] handleDuplicateColumnNames (String[] columns)
	{
		String[][] newColumns = null;
		if (columns != null)
		{
			newColumns = new String[columns.length][2];
			// Create 2 array to contains 2 parts of each column: Name and Alias
			String[] colNames = new String[columns.length];
			String[] aliases = new String[columns.length];
			
			for (int i = 0; i < columns.length; i++)
			{
				String column = columns[i];
				String[] colNameAndAlias = getColNameAndAlias(column);
				
				if (colNameAndAlias != null)
				{					
					// store the column name and alias
					colNames[i] = colNameAndAlias[0];			
					aliases[i] = colNameAndAlias[1];
				}
			}
			
			// process the aliases to remove first and end double quotes
			for (int i = 0; i < aliases.length; i++)
			{
				String alias = aliases[i];

				if  (alias != null && alias.length() > 0)
				{					
					if ((alias.charAt(0) == '"' && alias.charAt(alias.length() - 1) == '"') || alias.charAt(0) == '\'' && alias.charAt(alias.length() - 1) == '\'')
					{
						aliases[i] = alias.substring(1, alias.length() - 1).trim();
					}					
				}
				
			}
			
			// process the alias to remove duplications
			int[] duplicateIndices = new int[aliases.length];
			for (int i = 0; i < duplicateIndices.length; i++)
			{
				duplicateIndices[i] = 0;
			}
			
			for (int i = 0; i < aliases.length; i++)
			{
				String alias = aliases[i];
				int duplicateTimes = 0;
				if  (alias != null)
				{
					for (int j = 0; j < i; j++)
					{
						// duplicate
						if (aliases[j] != null && aliases[j].trim().equals(alias.trim()))
						{
							duplicateTimes = duplicateTimes + 1;
							duplicateIndices[j] = duplicateTimes;
							duplicateIndices[i] = duplicateTimes + 1;
						}
					}
				}

			}
			
			// Update to newAliases arrary
			String[] newAliases = new String[aliases.length];
			for (int i = 0; i < newAliases.length; i++)
			{
				newAliases[i] = aliases[i];
				if (duplicateIndices[i] > 0)
				{
					newAliases[i] = aliases[i] + "_" + duplicateIndices[i];
				}
			}
			
			// build up the new columns
			for (int i = 0; i < newAliases.length; i++)
			{
				newColumns[i][0] = colNames[i];
				newColumns[i][1] = newAliases[i];				
				System.out.println("newColumns: " + newColumns[i][0] + " AS " + '"' + newColumns[i][1] + '"');
			}
		}
		
		return newColumns;
	}
	
	/**
	 * Split the column string input to get appropriate alias and column
	 * @param column
	 * @return
	 */
	private String[] getColNameAndAlias(String column)
	{
		String[] colNameAndAlias = null;
		if (column != null && column.trim().length() > 0)
		{
			// normalize the column first	
			final String SINGLEQUOTE_REPLACER = "$SQ_REPLACER$_";
			Vector<String> singleQuoteReplacementHolders = new Vector<String>();
			int leftSingleQuoteIndex = column.indexOf('\'');
			int rightSingleQuoteIndex = column.indexOf('\'', leftSingleQuoteIndex + 1);
			
			StringBuffer replacedSQStringBuffer = new StringBuffer();
			boolean isSingleQuoteToReplace = leftSingleQuoteIndex > -1 && rightSingleQuoteIndex > -1;
			int phaseSQIndex = 0;
			while (isSingleQuoteToReplace)
			{
				// store the string inside double quotes in to the array
				String storePhase = column.substring(leftSingleQuoteIndex, rightSingleQuoteIndex + 1);
				singleQuoteReplacementHolders.add(storePhase);
				
				// append the left string to the replacement
				replacedSQStringBuffer.append(column.substring(0, leftSingleQuoteIndex)).append(SINGLEQUOTE_REPLACER + phaseSQIndex + "$");
				phaseSQIndex += 1;
				
				// process the next part
				column = column.substring(rightSingleQuoteIndex + 1);
				leftSingleQuoteIndex = column.indexOf('\'');
				rightSingleQuoteIndex = column.indexOf('\'', leftSingleQuoteIndex + 1);
				
				isSingleQuoteToReplace = leftSingleQuoteIndex > -1 && rightSingleQuoteIndex > -1;
			}
			
			replacedSQStringBuffer.append(column);
			
			column = replacedSQStringBuffer.toString();			
			
			final String DOUBLEQUOTE_REPLACER = "$DQ_REPLACER$_";
			Vector<String> doubleQuoteReplacementHolders = new Vector<String>();
			int leftDoubleQuoteIndex = column.indexOf('"');
			int rightDoubleQuoteIndex = column.indexOf('"', leftDoubleQuoteIndex + 1);
			
			StringBuffer replacedDQStringBuffer = new StringBuffer();
			boolean isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
			int phaseDQIndex = 0;
			while (isDoubleQuoteToReplace)
			{
				// store the string inside double quotes in to the array
				String storePhase = column.substring(leftDoubleQuoteIndex, rightDoubleQuoteIndex + 1);
				doubleQuoteReplacementHolders.add(storePhase);
				
				// append the left string to the replacement
				replacedDQStringBuffer.append(column.substring(0, leftDoubleQuoteIndex)).append(DOUBLEQUOTE_REPLACER + phaseDQIndex + "$");
				phaseDQIndex += 1;
				
				// process the next part
				column = column.substring(rightDoubleQuoteIndex + 1);
				leftDoubleQuoteIndex = column.indexOf('"');
				rightDoubleQuoteIndex = column.indexOf('"', leftDoubleQuoteIndex + 1);
				
				isDoubleQuoteToReplace = leftDoubleQuoteIndex > -1 && rightDoubleQuoteIndex > -1;
			}
			
			replacedDQStringBuffer.append(column);
			
			column = replacedDQStringBuffer.toString();	
			
			
			final String FUNCTION_REPLACER = "$FU_REPLACER$_";			
			Vector<String> functionReplacementHolders = new Vector<String>();
//			column = column.replaceAll("[ ]+\\(", " (");
//			column = column.replaceAll("\\([ ]+", "(");
//			column = column.replaceAll("[ ]+\\)", ")");
//			column = column.replaceAll("\\)[ ]+", ") ");
			String functionPartern = "[\\(][^\\(\\)]*[\\)]";
			
			int index = 0;
			boolean end = false;
			Pattern functionPattern = Pattern.compile(functionPartern);
			while (!end)
			{				
				Matcher matcher = functionPattern.matcher(column);			
				while (matcher.find())
				{
					String match = matcher.group();						
					functionReplacementHolders.add(match);
					column = replaceFirst(column, match, FUNCTION_REPLACER + index + "$");
					index += 1;
				}
				
				matcher = functionPattern.matcher(column);
				if (!matcher.find())
				{
					end = true;
				}
			}									
			
			colNameAndAlias = new String[] {"", ""};
			// check if column contains alias
			int aliasIndex = column.toUpperCase().lastIndexOf(" AS ");
			
			String colName = null;
			String alias = null;
			
			if (aliasIndex > -1)
			{
				// get the column name
				alias = column.substring(aliasIndex + " AS ".length());
				
				// check if alias is valid. If not, move to the first of the string
				while (!isValidAlias(alias) && aliasIndex > -1)
				{
					String beforeAsString = column.substring(0, aliasIndex + 1);							
					aliasIndex = beforeAsString.toUpperCase().lastIndexOf(" AS ");
					alias = column.substring(aliasIndex + " AS ".length()) + alias;
					alias = alias.trim();
				}											
			}
			
			if (aliasIndex > -1)
			{
				colName = column.substring(0, aliasIndex);
				alias = column.substring(aliasIndex + " AS ".length());
			}
			else if (column.trim().toUpperCase().indexOf("DISTINCT ") == 0) // no AS but has DISTINCT keyword
			{
				column = column.substring("DISTINCT ".length());
								
				// check if alias with out as like a.refno ref. Note, some key words like distinct						
				colName = column.trim();
				int spaceIndex = colName.lastIndexOf(' ');
				if (spaceIndex > -1)
				{
					colName = column.substring(0, spaceIndex);
					alias = column.substring(spaceIndex + 1);
												
				}
				else
				{
					alias = '"' + colName + '"';
					if (colName.indexOf('.') > 0)
					{
						alias = '"' + colName.substring(colName.indexOf('.') + 1) + '"';;
					}
				}
				
				colName = "DISTINCT " + colName;
			}
			else if ((column.trim().toUpperCase().indexOf("CASE ") == 0 )&& 
					(column.trim().toUpperCase().indexOf(" END") + " END".length() == column.trim().length()))
			{
				colName = column.trim();
				alias = "CASE_FIELD";
			}
			else
			{
				// check if alias with out as like a.refno ref. Note, some key words like distinct						
				colName = column.trim();
				int spaceIndex = colName.lastIndexOf(' ');
				if (spaceIndex > -1)
				{
					colName = column.substring(0, spaceIndex);
					alias = column.substring(spaceIndex + 1);
												
				}
				else
				{
					alias = '"' + colName + '"';
					if (colName.indexOf('.') > 0)
					{
						alias = '"' + colName.substring(colName.indexOf('.') + 1) + '"';;
					}
				}				
			}

				
			// Update back the parts of the column
			String colNameTmp = colName;
			String aliasTmp = alias;
				
				
			boolean colNameFunctionReplacerExist = (colNameTmp.indexOf(FUNCTION_REPLACER) > -1);
			while (colNameFunctionReplacerExist)
			{					
				for (int j = 0; j < functionReplacementHolders.size(); j++)
				{						
					String replacement = functionReplacementHolders.elementAt(j);
					if (colNameTmp.indexOf(FUNCTION_REPLACER + j + "$") > -1)
					{				
						colNameTmp = colNameTmp.replace(FUNCTION_REPLACER + j + "$", replacement);							
					}
				}
				colNameFunctionReplacerExist = (colNameTmp.indexOf(FUNCTION_REPLACER) > -1);
			}	
			
			
			boolean colNameDoubleQuoteReplacerExist = (colNameTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
			while (colNameDoubleQuoteReplacerExist)
			{					
				for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
				{						
					String replacement = doubleQuoteReplacementHolders.elementAt(j);
					if (colNameTmp.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
					{				
						colNameTmp = colNameTmp.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
					}
				}	
				colNameDoubleQuoteReplacerExist = (colNameTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
			}
			
			boolean colNameSingleQuoteReplacerExist = (colNameTmp.indexOf(SINGLEQUOTE_REPLACER) > -1);
			while (colNameSingleQuoteReplacerExist)
			{					
				for (int j = 0; j < singleQuoteReplacementHolders.size(); j++)
				{						
					String replacement = singleQuoteReplacementHolders.elementAt(j);
					if (colNameTmp.indexOf(SINGLEQUOTE_REPLACER + j + "$") > -1)
					{				
						colNameTmp = colNameTmp.replace(SINGLEQUOTE_REPLACER + j + "$", replacement);							
					}
				}	
				colNameSingleQuoteReplacerExist = (colNameTmp.indexOf(SINGLEQUOTE_REPLACER) > -1);
			}	
			
			boolean aliasFunctionReplacerExist = (aliasTmp.indexOf(FUNCTION_REPLACER) > -1);
			while (aliasFunctionReplacerExist)
			{					
				for (int j = 0; j < functionReplacementHolders.size(); j++)
				{						
					String replacement = functionReplacementHolders.elementAt(j);
					if (aliasTmp.indexOf(FUNCTION_REPLACER + j + "$") > -1)
					{				
						aliasTmp = aliasTmp.replace(FUNCTION_REPLACER + j + "$", replacement);							
					}
				}
				aliasFunctionReplacerExist = (aliasTmp.indexOf(FUNCTION_REPLACER) > -1);
			}	
			
			
			boolean aliasDoubleQuoteReplacerExist = (aliasTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
			while (aliasDoubleQuoteReplacerExist)
			{					
				for (int j = 0; j < doubleQuoteReplacementHolders.size(); j++)
				{						
					String replacement = doubleQuoteReplacementHolders.elementAt(j);
					if (aliasTmp.indexOf(DOUBLEQUOTE_REPLACER + j + "$") > -1)
					{				
						aliasTmp = aliasTmp.replace(DOUBLEQUOTE_REPLACER + j + "$", replacement);							
					}
				}	
				aliasDoubleQuoteReplacerExist = (aliasTmp.indexOf(DOUBLEQUOTE_REPLACER) > -1);
			}
			
			boolean aliasSingleQuoteReplacerExist = (aliasTmp.indexOf(SINGLEQUOTE_REPLACER) > -1);
			while (aliasSingleQuoteReplacerExist)
			{					
				for (int j = 0; j < singleQuoteReplacementHolders.size(); j++)
				{						
					String replacement = singleQuoteReplacementHolders.elementAt(j);
					if (aliasTmp.indexOf(SINGLEQUOTE_REPLACER + j + "$") > -1)
					{				
						aliasTmp = aliasTmp.replace(SINGLEQUOTE_REPLACER + j + "$", replacement);							
					}
				}	
				aliasSingleQuoteReplacerExist = (aliasTmp.indexOf(SINGLEQUOTE_REPLACER) > -1);
			}			
				
				
			
			// store the column name and alias
			colNameAndAlias[0] = colNameTmp;			
			colNameAndAlias[1] = aliasTmp;
		}
		
		return colNameAndAlias;
	}
	
	/**
	 * Split to columns by comma from a string that stays between a Select ... from clause
	 * @param selectClause
	 * @return
	 */
	private String[] splitColumns (String selectClause)
	{
		ArrayList<String> columns = new ArrayList<String>();
		if (selectClause != null && selectClause.trim().length() > 0)
		{
			String tmpClause = selectClause.trim();
			int commaIndex = tmpClause.indexOf(',');
			
			while (commaIndex > -1)
			{
				String beforeCommaString = tmpClause.substring(0, commaIndex);
				
				while (!isValidColumn(beforeCommaString) && commaIndex > -1)
				{
					commaIndex = tmpClause.indexOf(',', commaIndex + 1);
					beforeCommaString = tmpClause.substring(0, commaIndex);
				}
				
				if (commaIndex > -1)
				{
					columns.add(beforeCommaString.trim());
					tmpClause = tmpClause.substring(commaIndex + 1);
					commaIndex = tmpClause.indexOf(',');
				}
			}
			
			if (commaIndex == -1)
			{
				columns.add(tmpClause.trim());
			}			
		}
		
		String[] columnArr = new String[columns.size()];
		for (int i = 0; i < columns.size(); i++)
		{
			columnArr[i] = columns.get(i);
		}
		
		return columnArr;
	}
	
	/**
	 * Get the query after removed duplicate columns
	 * @return
	 */
	public String getRemovedDuplicateColQuery()
	{
		String newQuery = this.query;
		StringBuffer newQueryBuff = new StringBuffer();
		try
		{
			if (setKeywords.size() > 0)
			{
				int i = 0;
				for (i = 0; i < setKeywords.size(); i++)
				{
					String setKeyword = setKeywords.get(i);
					
					if (selectClauseParts != null && selectClauseParts[i] != null)
					{
						if (selectClauseParts[i][0] != null && selectClauseParts[i][0].trim().length() > 0)
						{
							newQueryBuff.append(selectClauseParts[0][0].trim());
							newQueryBuff.append(" SELECT ");
						}
						else
						{
							newQueryBuff.append("SELECT ");
						}
					
						if (selectClauseParts[i][1] != null)
						{
							// build up select clause from colName and alias set
							String[][] colNamesAndAliases = handleDuplicateColumnNames(extractColumnSets(selectClauseParts[i][1]));
							for (int j = 0; j < colNamesAndAliases.length; j++)
							{
								if (colNamesAndAliases[j][0] != null)
								{
									newQueryBuff.append(colNamesAndAliases[j][0]);
								}
								if (colNamesAndAliases[j][1] != null)
								{
									newQueryBuff.append(" AS ").append('"').append(colNamesAndAliases[j][1]).append('"');
								}
								
								if (j < colNamesAndAliases.length - 1)
								{
									newQueryBuff.append(", ");
								}
							}
							
							newQueryBuff.append(" FROM ");
						}
						
						if (selectClauseParts[i][2] != null && selectClauseParts[i][2].trim().length() > 0)
						{				
							newQueryBuff.append(selectClauseParts[0][2].trim());
						}
						
						
					}
					
					if (selectClauseParts[i+1] != null)
					{
						newQueryBuff.append(setKeyword);
					}
				}
				if (selectClauseParts != null && selectClauseParts[i] != null)
				{
					if (selectClauseParts[i][0] != null && selectClauseParts[i][0].trim().length() > 0)
					{
						newQueryBuff.append(selectClauseParts[0][0].trim());
						newQueryBuff.append(" SELECT ");
					}
					else
					{
						newQueryBuff.append("SELECT ");
					}
				
					if (selectClauseParts[i][1] != null)
					{
						// build up select clause from colName and alias set
						String[][] colNamesAndAliases = handleDuplicateColumnNames(extractColumnSets(selectClauseParts[i][1]));
						for (int j = 0; j < colNamesAndAliases.length; j++)
						{
							if (colNamesAndAliases[j][0] != null)
							{
								newQueryBuff.append(colNamesAndAliases[j][0]);
							}
							if (colNamesAndAliases[j][1] != null)
							{
								newQueryBuff.append(" AS ").append('"').append(colNamesAndAliases[j][1]).append('"');
							}
							
							if (j < colNamesAndAliases.length - 1)
							{
								newQueryBuff.append(", ");
							}
						}
						
						newQueryBuff.append(" FROM ");
					}
					
					if (selectClauseParts[i][2] != null && selectClauseParts[i][2].trim().length() > 0)
					{				
						newQueryBuff.append(selectClauseParts[0][2].trim());
					}
					
					
				}				
			}
			else
			{
				if (selectClauseParts != null && selectClauseParts.length > 0)
				{
					if (selectClauseParts[0][0] != null && selectClauseParts[0][0].trim().length() > 0)
					{
						newQueryBuff.append(selectClauseParts[0][0].trim());
						newQueryBuff.append(" SELECT ");
					}
					else
					{
						newQueryBuff.append("SELECT ");
					}
				
					if (selectClauseParts[0][1] != null)
					{
						// build up select clause from colName and alias set
						String[][] colNamesAndAliases = handleDuplicateColumnNames(extractColumnSets(selectClauseParts[0][1]));
						for (int i = 0; i < colNamesAndAliases.length; i++)
						{
							if (colNamesAndAliases[i][0] != null)
							{
								newQueryBuff.append(colNamesAndAliases[i][0]);
							}
							if (colNamesAndAliases[i][1] != null)
							{
								newQueryBuff.append(" AS ").append('"').append(colNamesAndAliases[i][1]).append('"');
							}
							
							if (i < colNamesAndAliases.length - 1)
							{
								newQueryBuff.append(", ");
							}
						}
						
						newQueryBuff.append(" FROM ");
					}
					
					if (selectClauseParts[0][2] != null && selectClauseParts[0][2].trim().length() > 0)
					{				
						newQueryBuff.append(selectClauseParts[0][2].trim());
					}
				}
			}
			newQuery = newQueryBuff.toString();
		}
		catch (Exception e)
		{
			LOG.error("Error while get removed duplicate column query: " + e.toString());
		}
		
		return newQuery;		
	}
	
	/**
	 * Get the query after removed duplicate columns
	 * @return
	 */
	public String[][] getFormarlizedColumns()
	{
		String[][] colNamesAndAliases = null;
		if (selectClauseParts != null && selectClauseParts.length > 0 && selectClauseParts[0][1] != null)
		{
			// build up select clause from colName and alias set
			colNamesAndAliases = handleDuplicateColumnNames(extractColumnSets(selectClauseParts[0][1]));			
		}
		
		return colNamesAndAliases;		
	}	
	
	/**
	 * Check if a column in select clause is valid
	 * @param clause
	 * @return
	 */
	private static boolean isValidColumn(String columnStr)
	{
		if (columnStr != null)
		{
			int numberOfDoubleQuote = 0;
			for (int i = 0; i < columnStr.length(); i++)
			{
				char c = columnStr.charAt(i);
				if (c == '"') numberOfDoubleQuote += 1;
			}
			
	
			return (numberOfDoubleQuote % 2 == 0);
		}
		
		return false;
	}	
	
	/**
	 * Check if a column in select clause is valid
	 * @param clause
	 * @return
	 */
	private static boolean isValidAlias(String alias)
	{
		if (alias != null)
		{
			int numberOfDoubleQuote = 0;
			for (int i = 0; i < alias.length(); i++)
			{
				char c = alias.charAt(i);
				if (c == '"') numberOfDoubleQuote += 1;
			}
			
	
			return (numberOfDoubleQuote % 2 == 0);
		}
		
		return false;
	}	
	
	/**
	 * 
	 * @param query
	 * @return
	 */
	private static boolean isSelectQuery (String aQuery)
	{
		if (aQuery != null)
		{
			aQuery = aQuery.replaceAll("[\\s]", " ");
			if (aQuery.trim().toUpperCase().indexOf("SELECT ") == 0 || aQuery.trim().toUpperCase().indexOf("WITH ") == 0)
			{
				return true;
			}
		}
		
		return false;		
	}	
	
	/**
	 * Remove any comments in the query which indicated by comment prefix
	 * @param query
	 * @return
	 */
	public static String removeQueryCommnets (String aQuery, String commentPrefix)
	{
		String result = aQuery;
		
		if (result != null && commentPrefix != null)
		{
			StringBuffer removedCommnetQueryBuffer = new StringBuffer();
			
			BufferedReader bufferedReader = new BufferedReader(new StringReader(result));
			
			try
			{
				String strLine = null;
			    //Read File Line By Line
			    while ((strLine = bufferedReader.readLine()) != null)   
			    {
			    	if (commentPrefix != null && strLine.indexOf(commentPrefix) > -1)
			    	{
			    		strLine = strLine.substring(0, strLine.indexOf(commentPrefix));
			    	}
			    	removedCommnetQueryBuffer.append(strLine).append("\n");
			    }
			    
			    // remove last \n character
			    removedCommnetQueryBuffer.deleteCharAt(removedCommnetQueryBuffer.length() - 1);
			    
			    result = removedCommnetQueryBuffer.toString();
			}
			catch (IOException ioe)
			{
				LOG.error("Error while removing comments in the query: " + ioe.toString());
			}
		}
		
		return result;
	}	
	
	/**
	 * Remove columns with Null as " "
	 * @param query
	 * @return
	 */
	public static String removeNullAsBlankColumns (String query)
	{
		String result = query;
		
//		if (query != null)
//		{
//			//String tempQuery = query.replaceAll("\\s", " ");
//			result = query.trim().replaceAll(NULL_AS_PARTERN, " ");
//		}
		
		return result;
	}
}
