package com.bp.pensionline.sqlreport.exception;

public class PLQueryErrorException extends Exception
{
	private String errorMessage = null;
	public PLQueryErrorException(String errorMessage)
	{
		this.errorMessage = errorMessage; 
	}
	
	public String getMessage()
	{
		return errorMessage;
	}
}
