package com.bp.pensionline.sqlreport.app.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.util.SchedulerReportMail;

public class RunScheduleJobListener implements JobListener {
	public static final String GLOBAL_NAME = "RunScheduleJobListener";
	
	public String getName() {
		return GLOBAL_NAME;
	}

	public void jobExecutionVetoed(JobExecutionContext arg0) {
		// TODO Auto-generated method stub

	}

	public void jobToBeExecuted(JobExecutionContext arg0) {
		// TODO Auto-generated method stub

	}

	public void jobWasExecuted(JobExecutionContext jobExecutionContext,
			JobExecutionException exception) {
		
		String scheduleName = jobExecutionContext.getJobDetail().getName();
		ReportScheduler scheduler = null;
		try {
			scheduler = ReportScheduler.getInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		Schedule schedule = scheduler.getSchedule(scheduleName);
		
		String[] toAddresses = new String[schedule.getReceivers().size()];
		for(int i = 0; i < toAddresses.length; i++){
			toAddresses[i] = schedule.getReceivers().get(i).getEmail();
		}
		
		String[] texts = SchedulerReportMail.getMailContentTemplate("");
		SchedulerReportMail.sendMail(toAddresses, null, texts[0], texts[1], schedule.getEmailFrom());
	}

}
