/**
 * 
 */
package com.bp.pensionline.sqlreport.app.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.io.RunScheduleRequest;

/**
 * @author anhtuan
 *
 */
public class RunScheduleJob implements Job {
	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext executionContext) throws JobExecutionException {
		System.out.println("Running schedule " + executionContext.getJobDetail().getName());
		
		try {
			RunScheduleRequest req = new RunScheduleRequest();
			ReportScheduler scheduler = ReportScheduler.getInstance();
			Schedule schedule = scheduler.getSchedule(executionContext.getJobDetail().getName());
			req.setSchedule(schedule);
			SchedulingApp app = AppFactory.getSchedulingApp();
			app.runSchedule(req);
			// TODO Auto-generated method stub
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
