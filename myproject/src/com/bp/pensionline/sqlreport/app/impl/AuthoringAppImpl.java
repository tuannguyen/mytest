package com.bp.pensionline.sqlreport.app.impl;

import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsException;

import com.bp.pensionline.sqlreport.app.api.AuthoringApp;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.util.SystemAccount;

public class AuthoringAppImpl implements AuthoringApp {

	public User getUser(String username) {
		CmsObject adminObj = SystemAccount.getAdminCmsObject();
		CmsUser cmsUser = null;
		try {
			cmsUser = adminObj.readUser(username);
		} catch (CmsException e) {
			
			e.printStackTrace();
		}
		User user = null;
		if(cmsUser != null){
			user = new User();
			user.setEmail(cmsUser.getEmail());
			user.setFirstName(cmsUser.getFirstname());
			user.setLastName(cmsUser.getLastname());
			user.setUserName(cmsUser.getName());
		}
		return user;
	}

}
