package com.bp.pensionline.sqlreport.app.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;

import com.bp.pensionline.sqlreport.app.api.ReportingApp;
import com.bp.pensionline.sqlreport.app.jasper.PLReportProducer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.ReportSection;
import com.bp.pensionline.sqlreport.dto.io.RunReportGroupRequest;
import com.bp.pensionline.sqlreport.util.AuditingUtil;
import com.bp.pensionline.sqlreport.util.LogUtil;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.util.SystemAccount;

public class ReportingAppImpl implements ReportingApp {
	public static final Log LOG = LogUtil.getLog(ReportingAppImpl.class);
	
	public String runReportGroup(RunReportGroupRequest req){
		String reportRunner = req.getUserName();
		String reportOutput = req.getOutputFormat();
		String reportGroupId = req.getReportGroupId();
		String siDescription = req.getSecurityIndicator();
		LOG.info("Report runner security description: " + siDescription);
		if (siDescription == null || siDescription.trim().equals(""))
		{
			String msg = "REPORT RUN FAILURE. \nFAILURE REASON: " +req.getUserName() +  
				" membership sensitivity setting is not recognised by the system. Please see a member of the PensionLine support team";
			LOG.error(msg);
			return msg;
		}	

		SimpleDateFormat formater = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
		String reportStartDate = formater.format(req.getStartDate());
		String reportEndDate = formater.format(req.getEndDate());
		
		String groupName = "";
		ReportGroup reportGroupDTO = ReportGroupDao.getReportGroupInfo(req.getReportGroupId());
		if (reportGroupDTO != null){
			groupName = reportGroupDTO.getName();
		}
		Vector<Report> runningReports = ReportDao.getAllMasterReportsInGroup(reportGroupId);
		if(runningReports != null){
			for(Report report : runningReports){
				Vector<ReportSection> sections = ReportDao.getAllSectionOfMaster(report.getReportId());
				report.setSections(sections);
			}
		}
		byte[] reportContent = null;
		if(reportOutput != null){
			try {
				if(reportOutput.toLowerCase().indexOf("pdf") >=0){
					reportContent = PLReportProducer.generateRootReportPDF(reportGroupId, runningReports, 
							reportRunner, reportStartDate, reportEndDate, siDescription);
				} else if(reportOutput.indexOf("XLS") >=0){
					reportContent = PLReportProducer.generateRootReportXSL(reportGroupId, runningReports, 
							reportRunner, reportStartDate, reportEndDate, siDescription);
				} else{
					String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: INVALID OUTPUT FILE FORMAT";
					LOG.error(msg);
					return msg;
				}
			} catch (Exception e) {
				e.printStackTrace();
				String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: " + e.getMessage();
				LOG.error(msg);
				return msg;
			}
		} else{
			String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \nFAILURE REASON: OUTPUT FILE FORMAT NOT SPECIFIED";
			LOG.error(msg);
			return msg;
		}
		if (reportContent == null){
			String msg = "REPORT " + groupName + ", " +  reportRunner + " RUN FAILURE. \n FAILURE REASON:" + " Error in generating XSL report.";
			LOG.error(msg);
			return msg;
		}	

		File dir = new File(SQLReportFileUtil.getReportOutputFolder());
		if(!dir.exists() || !dir.isDirectory()){
			dir.mkdir();
		}
		String ext = (req.getOutputFormat() != null && req.getOutputFormat().toLowerCase().indexOf("pdf") >= 0) ? ".pdf" : ".xls";
		File output = new File(dir, req.getFileName() + ext);
		if(output.exists()){
			output.delete();
		}
		FileOutputStream fout = null;
		ByteArrayOutputStream bArrayOut = null;
		try {
			output.createNewFile();
			fout = new FileOutputStream(output);

			bArrayOut = new ByteArrayOutputStream();
			bArrayOut.write(reportContent);
			bArrayOut.writeTo(fout);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			LOG.error(e1.getMessage());
			return e1.getMessage();
		} catch (IOException e1) {
			e1.printStackTrace();
			LOG.error(e1.getMessage());
			return e1.getMessage();
		} finally{
			try {
				if(bArrayOut != null){
					bArrayOut.close();
				}
				if(fout != null){
					fout.close();
				}
			} catch (IOException e) {
				LOG.error(e.getMessage());
			}
		}
		
		/**
		 * Auditing report here
		 */
		try {
			CmsObject adminObj = SystemAccount.getAdminCmsObject();
			CmsUser cmsUser = adminObj.readUser(reportRunner);

			AuditingUtil.auditReport(runningReports, "RUN", cmsUser);
		} catch (Exception e) {
			String msg = "EXCEPTION while audting reports: "+e.toString();
			LOG.error(msg);
			e.printStackTrace();
		}
		

		return null;
	}
}
