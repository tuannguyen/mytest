/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.impl;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.AuthoringApp;
import com.bp.pensionline.sqlreport.app.api.ReportingApp;
import com.bp.pensionline.sqlreport.app.api.SchedulingApp;
import com.bp.pensionline.sqlreport.app.daemons.EmailSender;
import com.bp.pensionline.sqlreport.app.daemons.ReportScheduler;
import com.bp.pensionline.sqlreport.dao.DownloadLogDao;
import com.bp.pensionline.sqlreport.dao.RecipientDao;
import com.bp.pensionline.sqlreport.dao.ReportGroupDao;
import com.bp.pensionline.sqlreport.dao.ScheduleAuditDao;
import com.bp.pensionline.sqlreport.dao.ScheduleDao;
import com.bp.pensionline.sqlreport.dao.ScheduleRunLogDao;
import com.bp.pensionline.sqlreport.dto.db.DownloadLog;
import com.bp.pensionline.sqlreport.dto.db.Recipient;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.Schedule.Status;
import com.bp.pensionline.sqlreport.dto.db.ScheduleAudit;
import com.bp.pensionline.sqlreport.dto.db.ScheduleAudit.Action;
import com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.dto.io.DownloadScheduleOutputRequest;
import com.bp.pensionline.sqlreport.dto.io.PersistScheduleRequest;
import com.bp.pensionline.sqlreport.dto.io.QuerySchedulesRequest;
import com.bp.pensionline.sqlreport.dto.io.RunReportGroupRequest;
import com.bp.pensionline.sqlreport.dto.io.RunScheduleRequest;
import com.bp.pensionline.sqlreport.util.LogUtil;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.util.CheckConfigurationKey;

import org.apache.commons.logging.Log;

import org.quartz.SchedulerException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author
 * @version
 */
public class SchedulingAppImpl implements SchedulingApp {

    //~ Static fields/initializers ---------------------------------------------

    private static final Log log = LogUtil.getLog(SchedulingAppImpl.class);
    
    public static final String IT_SUPPORT_MAIL_BOX = CheckConfigurationKey.getStringValue("sqlreport.email.from");

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @param   req  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String persistSchedule(PersistScheduleRequest req) {
        // ----------- Checking assumptions--------------
        StringBuilder sbErr = new StringBuilder();
        Schedule schedule = req.getSchedule();

        // Assumption 1: schedule in input param must not be null
        if (schedule == null) {
            return "Schedule is null";
        }

        // Assumption 2: Report group of this schedule must not be null:
        if ((schedule.getReportGroup() == null)
                || (schedule.getReportGroup().getId() == null)) {
            String errorMsg = "Report group of schedule "
                + schedule.getTitle()
                + "["
                + schedule.getId() + "] is not specified";
            log.error(errorMsg);

            // When persisSchedule return a string, and req.getSchedule() is
            // null then the requested schedule is not persistable
            req.setSchedule(null);

            return errorMsg;
        }

        // Assumption 3: Report group of this schedule must exists in DB:
        try {
            ReportGroup reportGroup = ReportGroupDao.getReportGroupInfo(
                    schedule.getReportGroup().getId());
            if (reportGroup == null) {
                removeAllSchedulesOfReportGroup(
                    schedule.getReportGroup().getId());
                String errorMsg = "Report group of schedule "
                    + schedule.getTitle()
                    + "["
                    + schedule.getId() + "] has been removed";
                log.error(errorMsg);
                req.setSchedule(null);

                return errorMsg;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            log.error(e2.getMessage());
        }
        // -----Finish checking assumptions--------------

        // Id of schedule in DB is auto generated.
        // If a schedule is new, its Id == -1
        boolean insert = (schedule.getId() < 0);
        Connection connection = null;
        try {
            ReportScheduler scheduler = ReportScheduler.getInstance();
            ScheduleDao dao = new ScheduleDao();
            connection = dao.getConnection();
            if (insert) { // CREATE SCHEDULE
                if (log.isDebugEnabled()) {
                    // 1. Insert schedule into Schedule table in DB
                    log.debug("Begin Insert schedule into DB");
                }
                dao.insertSchedule(schedule);

                // Update new ID for schedule object
                long id = dao.getLastInsertId();
                if (log.isDebugEnabled()) {
                    log.debug("Inserted new schedule into DB with ID="
                        + String.valueOf(id));
                }
                schedule.setId(id);

                // 2. Insert all recipients of this schedule into Recipients
                // table
                RecipientDao recipientDao = new RecipientDao(connection); // Reuse connection inited before
                List<User> receivers = schedule.getReceivers();
                if (log.isDebugEnabled()) {
                    log.debug("Begin insert " + receivers.size()
                        + " records into Recipients table");
                }
                for (User receiver : receivers) {
                    Recipient recipient = new Recipient();
                    recipient.setScheduleId(schedule.getId());
                    recipient.setUserName(receiver.getUserName());
                    recipientDao.insert(recipient);
                    if (log.isDebugEnabled()) {
                        log.debug("Inserted new recipient {schedule_id="
                            + recipient.getScheduleId() + ", username="
                            + recipient.getUserName() + "}");
                    }
                }

                // 3. Add 1 record into Audit log
                ScheduleAuditDao auditDao = new ScheduleAuditDao(connection); // Reuse connection initiated before
                ScheduleAudit audit = new ScheduleAudit();
                audit.setSchedule(req.getSchedule());
                audit.setActor(req.getRequestedBy());
                audit.setAction(Action.CREATE);
                audit.setActionTime(new Date());
                auditDao.insert(audit);
                if (schedule.getAudits() == null) {
                    schedule.setAudits(new ArrayList<ScheduleAudit>());
                }
                schedule.getAudits().add(audit);
                AuthoringApp authoringApp = AppFactory.getAuthoringApp();
                User creator = authoringApp.getUser(audit.getActor()
                        .getUserName());
                schedule.setCreatedBy(creator);
                schedule.setCreatedTime(audit.getActionTime());

                // 4. Add 1 schedule to scheduler daemon If Persisting success:
                // Add this schedule to ReportScheduler daemon
                scheduler.addSchedule(schedule);
                connection.commit();
            } else { // MODIFY SCHEDULE
                if (log.isDebugEnabled()) {
                    // 1. Update schedule into Schedule table in DB
                    log.debug("Begin Update schedule into DB");
                }
                dao.updateSchedule(schedule);
                if (log.isDebugEnabled()) {
                    log.debug("Updated scheduleD=" + schedule.getId());
                }

                // 2. Update receivers
                List<User> receivers = schedule.getReceivers();
                if (receivers != null) {
                    RecipientDao recipientDao = new RecipientDao(connection); // Reuse connection initiated before
                    if (log.isDebugEnabled()) {
                        // Delete all recipients of this schedule in
                        // Recipients table
                        log.debug(
                            "Begin recipient-records of this schedule in Recipients table");
                    }
                    recipientDao.deleteAllOfSchedule(schedule.getId());
                    if (log.isDebugEnabled()) {
                        // Insert all recipients of this schedule into
                        // Recipients table
                        log.debug("Begin insert " + receivers.size()
                            + " records into Recipients table");
                    }
                    for (User receiver : receivers) {
                        Recipient recipient = new Recipient();
                        recipient.setScheduleId(schedule.getId());
                        recipient.setUserName(receiver.getUserName());
                        recipientDao.insert(recipient);
                        if (log.isDebugEnabled()) {
                            log.debug("Inserted new recipient {schedule_id="
                                + recipient.getScheduleId() + ", username="
                                + recipient.getUserName());
                        }
                    }
                }

                // 3. Add 1 record into Audit log
                ScheduleAuditDao auditDao = new ScheduleAuditDao(connection); // Reuse connection initiated before
                ScheduleAudit audit = new ScheduleAudit();
                audit.setSchedule(req.getSchedule());
                audit.setActor(req.getRequestedBy());
                if (schedule.getStatus() == Status.DELETED) {
                    audit.setAction(Action.DELETE);
                } else {
                    audit.setAction(Action.UPDATE);
                }
                audit.setActionTime(new Date());
                auditDao.insert(audit);
                if (schedule.getAudits() == null) {
                    schedule.setAudits(new ArrayList<ScheduleAudit>());
                }
                schedule.getAudits().add(audit);

                // 4. Update schedule in Scheduler daemon If Persisting success:
                // Add this schedule to ReportScheduler daemon
                if (schedule.getStatus() == Status.DELETED) {
                    scheduler.removeSchedule(schedule);
                } else {
                    scheduler.updateSchedule(schedule);
                }
                connection.commit();
            }
        } catch (Exception e) {
            sbErr.append("\\n").append(e.getMessage());
            log.fatal(e.getMessage());
            if (insert) {
                schedule.setId(-1);
            }
            try {
                // connection.releaseSavepoint(savepoint);
                connection.rollback();
            } catch (SQLException e1) {
                sbErr.append("\\n").append(e.getMessage());
                log.fatal("Cannot close transaction: " + e.getMessage());
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                // connection.releaseSavepoint(savepoint);
                connection.close();
            } catch (SQLException e) {
                sbErr.append("\\n").append(e.getMessage());
                log.fatal("Cannot close transaction: " + e.getMessage());
                e.printStackTrace();
            }
        }

        return (sbErr.length() == 0) ? null : sbErr.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @param   reportGroupId  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  Exception  DOCUMENT ME!
     */
    protected int removeAllSchedulesOfReportGroup(String reportGroupId)
        throws Exception {
        if (reportGroupId == null) {
            return 0;
        }
        ReportScheduler scheduler = ReportScheduler.getInstance();
        List<Schedule> schedules = scheduler.getSchedulesOfReportGroup(
                reportGroupId);
        if ((schedules == null) || (schedules.size() == 0)) {
            return 0;
        }
        int count = 0;
        ScheduleDao dao = new ScheduleDao();
        Connection connection = dao.getConnection();
        log.info("Removing schedules of Report group " + reportGroupId);
        for (Schedule schedule : schedules) {
            if (schedule == null) {
                continue;
            }
            try {
                scheduler.removeSchedule(schedule);
            } catch (Exception e1) {
                log.error("Fail to remove schedule " + schedule.getId()
                    + " from scheduler: " + e1.getMessage());
                e1.printStackTrace();

                continue;
            }
            schedule.setStatus(Status.DELETED);
            try {
                dao.updateSchedule(schedule);
                connection.commit();
            } catch (SQLException e) {
                log.error("Fail to update schedule " + schedule.getId() + ": "
                    + e.getMessage());
                e.printStackTrace();

                continue;
            }
            count++;
        }
        connection.close();

        return count;
    }

    /*      @Override
     *      public String deleteSchedule(DeleteScheduleRequest req){ // TODO
     * Auto-generated method stub             return null;     }
     */
    /**
     * DOCUMENT ME!
     *
     * @param   req  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String logDownload(DownloadScheduleOutputRequest req) {
        String error = null;
        Connection connection = null;
        DownloadLog downloadLog = new DownloadLog();
        downloadLog.setRunLog(new ScheduleRunLog());
        downloadLog.setDownloadBy(new User());
        downloadLog.setDownloadTime(new Date());
        downloadLog.getRunLog().setId(req.getRunlogId());
        downloadLog.getDownloadBy().setUserName(req.getUserName());
        try {
            DownloadLogDao dao = new DownloadLogDao();
            connection = dao.getConnection();
            dao.insert(downloadLog);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            error = "Can not INSERT into DownloadLog table" + e.getMessage();
            log.error(error);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                log.error("Can not roll back transaction" + e1.getMessage());
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    log.error("Can not close connection" + e.getMessage());
                }
            }
        }

        return error;
    }

    /*      @Override
     *      public Schedule getSchedule(String scheduleId) {             // TODO
     * Auto-generated method stub             return null;     }
     */
    /**
     * DOCUMENT ME!
     *
     * @param   request  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  Exception  DOCUMENT ME!
     */
    public List<Schedule> getSchedules(QuerySchedulesRequest request)
        throws Exception {
        List<Schedule> schedules = new ArrayList<Schedule>();
        if ((request.getReportGroupIds() != null)
                && ((request.getStatus() & Status.getActiveBitMask()) > 0)) {
            // All active schedules is cached in ReportScheduler singleton
            // object
            ReportScheduler scheduler = ReportScheduler.getInstance();
            String[] reportGroupIds = request.getReportGroupIds();
            for (String id : reportGroupIds) {
                List<Schedule> rgSchedules = scheduler
                    .getSchedulesOfReportGroup(id);
                if ((rgSchedules == null) || rgSchedules.isEmpty()) {
                    continue;
                }
                schedules.addAll(rgSchedules);
            }
        }
        if ((request.getStatus() & Status.getInactiveBitMask()) > 0) {
            // Inactive bit-mask

            // TODO: init DAO to load inactive schedules
            // Get last RunLog for each inactive schedule
            /*                      ScheduleRunLogDao runlogDao = null;
             *                      try {                             runlogDao
             * = new ScheduleRunLogDao(); for(Schedule schedule : schedules){
             * ScheduleRunLog runlog =
             * runlogDao.getLastRunOfSchedule(schedule.getId());
             * schedule.getPrevRuns().add(runlog);                  } } finally{
             * if(runlogDao != null){ runlogDao.getConnection().close(); } }
             */
        }

        return schedules;
    }

    /**
     * DOCUMENT ME!
     *
     * @param   req  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String runSchedule(RunScheduleRequest req) {
        Schedule schedule = req.getSchedule();
        if (schedule == null) {
            return "Schedule not exists";
        }
        System.out.println(this.getClass().getName() + ".runSchedule("
            + req.getSchedule().getId() + ")");
        StringBuilder errors = new StringBuilder();
        List<ScheduleRunLog> runlogs = schedule.getPrevRuns();
        ReportScheduler scheduler = null;
        try {
            scheduler = ReportScheduler.getInstance();
            schedule = scheduler.getSchedule(String.valueOf(schedule.getId()));
            if (schedule == null) {
                return "Schedule not exists";
            }
            req.setSchedule(schedule);
            if (runlogs == null) {
                runlogs = new ArrayList<ScheduleRunLog>();
                schedule.setPrevRuns(runlogs);
            }
        } catch (Exception e2) {
            return handleError(null, e2, errors);
        }

        // Check if report group exists in DB
        ReportGroup reportGroup = ReportGroupDao.getReportGroupInfo(schedule
                .getReportGroup().getId());
        if (reportGroup == null) {
            String errorMsg = "Report group of schedule " + schedule.getTitle()
                + "["
                + schedule.getId() + "] has been removed";
            log.error(errorMsg);
            try {
                removeAllSchedulesOfReportGroup(schedule.getReportGroup()
                    .getId());
            } catch (Exception e) {
                log.error("Can not remove schedule " + schedule.getTitle() + "["
                    + schedule.getId() + "]: " + e.getMessage());

                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return errorMsg;
        }

        // Update report group info
        schedule.setReportGroup(reportGroup);

        // 1. Add 1 record into runlog
        ScheduleRunLog runLog = new ScheduleRunLog();
        runLog.setSchedule(schedule);
        runLog.setRunningTime(new Date());
        runLog.setManualRunner(req.getRunBy());
        ScheduleRunLogDao runLogDao;
        Connection conn = null;
        try {
            runLogDao = new ScheduleRunLogDao();
            conn = runLogDao.getConnection();
            runLogDao.insert(runLog);

            // Get id of inserted record
            runLog.setId(runLogDao.getLastInsertId());
        } catch (SQLException e) {
            return handleError(conn, e, errors);
        }
        if (runLog.getId() < 0) {
            return handleError(conn,
                    new Exception("Fail to insert a runlog into DB"), errors);
        }

        // 2. Run report group
        RunReportGroupRequest reportRunReq = new RunReportGroupRequest();
        reportRunReq.setReportGroupId(schedule.getReportGroup().getId());
        reportRunReq.setUserName((req.getRunBy() == null)
            ? req.getSchedule().getCreatedBy().getUserName()
            : req.getRunBy().getUserName());
        reportRunReq.setFileName(String.valueOf(runLog.getId()));
        reportRunReq.setOutputFormat(schedule.getOutputFormat());
        reportRunReq.setSecurityIndicator(getSI(schedule.getRunLevel()));
        Date startDate = null;
        try {
            startDate = scheduler.getPrevRun(schedule);
        } catch (SchedulerException e2) {
            log.info("Schedule " + schedule.getId()
                + " has not ever run before");
        }
        if (startDate == null) {
            startDate = schedule.getStartDate();
        }
        if (startDate == null) {
            startDate = schedule.getCreatedTime();
        }
        reportRunReq.setStartDate(startDate);
        reportRunReq.setEndDate(new Date()); // Today
        ReportingApp reportingApp = null;
        try {
            reportingApp = AppFactory.getReportingApp();
        } catch (Exception e) {
            return handleError(conn, e, errors);
        }

        // Run report group:
        String error = reportingApp.runReportGroup(reportRunReq);
        if (error != null) {
            return handleError(conn, new Exception(error), errors);
        }

        // No errors: commit transaction
        try {
            conn.commit();
            runlogs.add(runLog);
        } catch (SQLException e1) {
            e1.printStackTrace();
            log.fatal("COMMIT FAILED:" + e1.getMessage());
            errors.append(e1.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                log.error("CANNOT CLOSE CONNECTION:" + e1.getMessage());
                errors.append(e1.getMessage());
            }
        }

        // 3. Create a new thread to send email to receivers
        mailToReceivers(schedule);

        return (errors.length() == 0) ? null : errors.toString();
    }

    private String getSI(int siValue) {
        switch (siValue) {
            case 50: {
                return "Default";
            }

            case 75: {
                return "Execs";
            }

            case 100: {
                return "Team";
            }

            default: {
                return null;
            }
        }
    }

    private String handleError(Connection conn, Exception e,
        StringBuilder previosErrors) {
        if (e != null) {
            e.printStackTrace();
            log.error(e.getMessage());
            previosErrors.append(e.getMessage());
        }
        if (conn != null) {
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                log.error("ROLLBACK FAILED:" + e1.getMessage());
                previosErrors.append(e1.getMessage());
            }
            try {
                conn.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                log.error("CANNOT CLOSE CONNECTION:" + e1.getMessage());
                previosErrors.append(e1.getMessage());
            }
        }

        return previosErrors.toString();
    }

    /*      @Override
     *      public String updateSchedule(PersistScheduleRequest req){ // TODO
     * Auto-generated method stub             return null;     }
     */
    /**
     * DOCUMENT ME!
     *
     * @param   schedule  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String mailToReceivers(Schedule schedule) {
        String[] texts = getMailContent(schedule);

        // String subject = String.format(texts[0], reportGroupName);
        // String content = String.format(texts[1], reportGroupName);
        String subject = texts[0]; // String.format(texts[0], reportGroupName);
        String content = texts[1]; // String.format(texts[1], reportGroupName);
        String from = schedule.getEmailFrom();
        if ((from == null) || from.isEmpty()) {
            from = schedule.getCreatedBy().getEmail();
        }
        if ((from == null) || from.isEmpty()) {
            from = IT_SUPPORT_MAIL_BOX;
        }
        String runnerMail = null;
        List<ScheduleRunLog> runlogs = schedule.getPrevRuns();
        if (runlogs != null) {
            ScheduleRunLog lastRun = runlogs.get(runlogs.size() - 1);
            if ((lastRun != null) && (lastRun.getManualRunner() != null)
                    && (lastRun.getManualRunner().getEmail() != null)) {
                runnerMail = lastRun.getManualRunner().getEmail();
            }
        }
        log.info("Runner mail: " + runnerMail);
        String[] cc = null;
        if (runnerMail != null) {
            cc = new String[] { runnerMail }; // Only send mail to runner
        } else {
            cc = new String[schedule.getReceivers().size()];
            for (int i = 0; i < schedule.getReceivers().size(); i++) {
                cc[i] = schedule.getReceivers().get(i).getEmail();
            }
        }
        // Huy debug
        if (cc != null)
        {
	        for (int i = 0; i < cc.length; i++)
			{
	        	log.info("Receiver mail: " + cc[i]);
			}
        }
        
        EmailSender mailer = EmailSender.getInstance();
        mailer.setFrom(from);
//        mailer.setTo(new String[] { from });
//        mailer.setCc(cc);
        
        // Modifid by Huy: 11/11/2010
        mailer.setCc(cc);
        if (from != null && !from.equals(IT_SUPPORT_MAIL_BOX))
        {
        	mailer.setTo(new String[] { from });
        }

        mailer.setSubject(subject);
        mailer.setContent(content);
        Thread thread = new Thread(mailer);
        thread.start();

        return null;
    }

    /**
     * DOCUMENT ME!
     *
     * @param   schedule  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String[] getMailContent(Schedule schedule) {
        String reportGroupName = "(This report group has just been deleted)";
        if (schedule.getReportGroup() == null) {
            log.error("Report group of schedule " + schedule.getTitle() + "["
                + schedule.getId() + "] has been removed");
            try {
                ReportScheduler.getInstance().removeSchedule(schedule);
            } catch (Exception e) {
                log.error("Can not remove schedule " + schedule.getTitle() + "["
                    + schedule.getId() + "]: " + e.getMessage());

                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            reportGroupName = schedule.getReportGroup().getName();
        }
        String title = CheckConfigurationKey.getStringValue(
                "sqlreport.email.template.title");
        if (title == null) {
            title = "Automated Scheduler Report from PensionLine";
        }
        String downloadUrl = CheckConfigurationKey.getStringValue(
                "report.output.url");
        if (downloadUrl == null) {
            downloadUrl =
                "https://pensionline.bp.com/content/pl/sql_report/download_report.jsp";
        }
        downloadUrl += "?scheduleId=" + schedule.getId();
        String body = null;
        String path = CheckConfigurationKey.getStringValue("report.base")
            + "templates/";
        String file = CheckConfigurationKey.getStringValue(
                "sqlreport.email.template.file");
        if ((path != null) && (file != null)) {
            log.info("Getting email template from file...");
            byte[] barr = SQLReportFileUtil.getFileData(path + file);
            if (barr != null) {
                body = new String(barr);
            }
        } else {
            log.info("Email template file " + path + file + " not found!");
        }
        if (body == null) {
            body =
                "Schedule: %s ran at %s.<br>Report: %s is now available for viewing<br><br>View the reports <a href='%s'>%s</a>.";
        }
        String sRunTime = "";
        List<ScheduleRunLog> runLogs = schedule.getPrevRuns();
        if ((runLogs != null) && (runLogs.size() > 0)) {
            ScheduleRunLog lastRun = runLogs.get(runLogs.size() - 1);
            Date runTime = lastRun.getRunningTime();
            SimpleDateFormat formater = new SimpleDateFormat(
                    "dd/MM/yyyy HH:mm:ss");
            sRunTime = formater.format(runTime);
        }
        body = String.format(body, schedule.getTitle(), sRunTime,
                reportGroupName, downloadUrl,
                schedule.getTitle());

        return new String[] { title, body };
    }
}
