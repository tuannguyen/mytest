/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.jasper.jrxml;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.util.CheckConfigurationKey;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRStaticText;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignBreak;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JRDesignSubreportParameter;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

import org.apache.commons.logging.Log;

import org.opencms.main.CmsLog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import java.util.Vector;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author
 * @version
 */
public class MasterJRXMLConsumer {

    //~ Static fields/initializers ---------------------------------------------

    /** DOCUMENT ME! */
    public static final Log LOG = CmsLog.getLog(
            org.opencms.jsp.CmsJspLoginBean.class);

    // IO
    /** DOCUMENT ME! */
    public static final String PLMASTER_JRXML_TEMPLATE = CheckConfigurationKey
        .getStringValue("report.base")
        + "resources/master/templates/master.jrxml";

    // Keys
    /** DOCUMENT ME! */
    public static final String MASTER_JRXML_TITLE_KEY = "report_title";

    /** DOCUMENT ME! */
    public static final String MASTER_JRXML_DESCRIPTION_KEY =
        "report_description";

    // displaying
    /** DOCUMENT ME! */
    public static final int SUB_REPORT_DEFAULT_WIDTH = 782;

    /** DOCUMENT ME! */
    public static final int SUB_REPORT_DEFAULT_HEIGHT = 60;

    // dummy queries to get only 1 row of the report
    /** DOCUMENT ME! */
    public static final String ONEROW_DUMMY_QUERY_AQUILLA =
        "SELECT 1 FROM DUAL";

    /** DOCUMENT ME! */
    public static final String ONEROW_DUMMY_QUERY_OTHERS = "SELECT 1";

    //~ Instance fields --------------------------------------------------------

    // Jasper design object representing this JRXML
    private JasperDesign jDesign = null;
    private String dbName = null;
    private String reportFile = null; // file named after created
    private String title = null;
    private String description = null;
    private int pageWidth = SUB_REPORT_DEFAULT_WIDTH + 60;

    //~ Constructors -----------------------------------------------------------

    /**
     * Create a SectionJRXMLConsumer from default XML file.
     *
     * @param  dbName  Database name to which the JRXML will connect to
     */
    public MasterJRXMLConsumer(String dbName) {
        this.dbName = dbName;
        load(PLMASTER_JRXML_TEMPLATE);
    }

    /**
     * Create a SectionJRXMLConsumer from existing XML file.
     *
     * @param  reportFile  Existing report XML file
     * @param  dbName      Database name to which the JRXML will connect to
     */
    public MasterJRXMLConsumer(String reportFile, String dbName) {
        this.reportFile = reportFile;
        this.dbName = dbName;
        load(reportFile);
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Load an instance of JRXML from file.
     *
     * @param  reportFile
     */
    private void load(String reportFile) {
        try {
            // jrxml compiling process
            ByteArrayInputStream inputStream = new ByteArrayInputStream(
                    SQLReportFileUtil.getFileData(reportFile));
            jDesign = JRXmlLoader.load(inputStream);
            // JasperReport jDesign = (JasperReport)JRLoader.loadObject(new
            // File(reportFile));
            if (jDesign != null) {
                if (dbName != null) {
                    JRDesignQuery jQuery = new JRDesignQuery();
                    if (dbName.equals(Constant.DATASOURCE_AQUILLA)) 
                    {
                        jQuery.setText(ONEROW_DUMMY_QUERY_AQUILLA);
//                    } else if (dbName.equals(Constant.DATASOURCE_WEBSTATS)
//                            || dbName.equals(Constant.DATASOURCE_CMS_DATA)
//                            || dbName.equals(Constant.DATASOURCE_PENSIONLINE)) {
//                        jQuery.setText(ONEROW_DUMMY_QUERY_OTHERS);
                    }
                    else 
                    {
                        jQuery.setText(ONEROW_DUMMY_QUERY_OTHERS);
                    }
                    jDesign.setQuery(jQuery);
                }

                // Get section title which is stored as key 'section_title'
                JRBand titleBand = jDesign.getTitle();
                if (titleBand != null) {
                    JRElement titleJRElement = titleBand.getElementByKey(
                            MASTER_JRXML_TITLE_KEY);
                    if (titleJRElement != null) {
                        this.title = ((JRStaticText) titleJRElement).getText();
                    }
                    JRElement descriptionJRElement = titleBand.getElementByKey(
                            MASTER_JRXML_DESCRIPTION_KEY);
                    if (descriptionJRElement != null) {
                        this.description = ((JRStaticText) descriptionJRElement)
                            .getText();
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error in loading JRXML file: " + e.toString());
            jDesign = null;
        }
    }

    /**
     * Add a subreport element to detail band.
     *
     * @param  subreportId              DOCUMENT ME!
     * @param  subreportJasperFilename  the subreportJasperFiles to set
     * @param  subreportParamNames      DOCUMENT ME!
     * @param  isPageBreak              DOCUMENT ME!
     */
    public void addSubreport(String subreportId, String subreportJasperFilename,
        Vector<String> subreportParamNames, boolean isPageBreak) {
        try {
            // update sub-reports
            if ((jDesign != null) && (subreportId != null)) {
                JRBand jrDetailBand = jDesign.getDetail();
                JRDesignBand jrDetailDesignBand = (JRDesignBand) jrDetailBand;
                int subreportY = jrDetailDesignBand.getHeight();

                // format subreportJasperFile to OS compatible file path
                File subreportJasperFile = new File(subreportJasperFilename);
                if (subreportJasperFile.exists()) {
                    int subreportWidth = SUB_REPORT_DEFAULT_WIDTH;
                    JasperReport subreportJapserObject = (JasperReport) JRLoader
                        .loadObject(new File(subreportJasperFilename));
                    if (subreportJapserObject != null) {
                        subreportWidth = subreportJapserObject.getPageWidth();
                        if (subreportWidth < SUB_REPORT_DEFAULT_WIDTH) {
                            subreportWidth = SUB_REPORT_DEFAULT_WIDTH;
                        }
                    }
                    if (subreportWidth > pageWidth) {
                        pageWidth = subreportWidth;
                    }
                    jDesign.setPageWidth(pageWidth);
                    JRDesignSubreport subreport = createSubreportElement(
                            subreportId, subreportJasperFilename,
                            subreportY, subreportWidth, subreportParamNames);
                    if (subreport != null) {
                        jrDetailDesignBand.addElement(subreport);

                        // create a parameter with convention
                        // subreportId_PARAM_NAME for each subreport params
                        if (subreportParamNames != null) {
                            for (int i = 0; i < subreportParamNames.size();
                                    i++) {
                                String paramName = subreportParamNames
                                    .elementAt(i);
                                JRDesignParameter copiedMasterParam =
                                    new JRDesignParameter();
                                copiedMasterParam.setName(subreportId + "_"
                                    + paramName);
                                copiedMasterParam.setForPrompting(false);
                                copiedMasterParam.setValueClassName(
                                    "java.lang.String");
                                jDesign.addParameter(copiedMasterParam);
                            }
                        }
                    }

                    // create a page break element
                    if (isPageBreak) {
                        JRDesignBreak jrDesignBreak = new JRDesignBreak();
                        jrDesignBreak.setPositionType(
                            JRElement.POSITION_TYPE_FLOAT);
                        jrDesignBreak.setY(subreportY
                            + SUB_REPORT_DEFAULT_HEIGHT + 1);
                        jrDetailDesignBand.addElement(jrDesignBreak);
                    }
                    jrDetailDesignBand.setHeight(subreportY
                        + SUB_REPORT_DEFAULT_HEIGHT + 2);
                }
            }
        } catch (JRException jre) {
            LOG.error("Error while adding subreport to master jrxml: " + jre);
        }
    }

    /**
     * Update jDesign object with new properties. It also update the columns of
     * table with the query input extracted fields.
     *
     * @throws  JRException  DOCUMENT ME!
     */
    public void update() throws JRException {
        if (jDesign != null) {
            // update title
            JRBand jrTitleBand = jDesign.getTitle();
            JRElement titleEle = jrTitleBand.getElementByKey(
                    MASTER_JRXML_TITLE_KEY);
            if (titleEle != null) {
                ((JRStaticText) titleEle).setText(this.getTitle());
            }

            // update description
            JRElement descriptionEle = jrTitleBand.getElementByKey(
                    MASTER_JRXML_DESCRIPTION_KEY);
            if (descriptionEle != null) {
                ((JRStaticText) descriptionEle).setText(this.getDescription());
            }
        }
    }

    /**
     * Save this object to an JRXML file specified by reportFile.
     *
     * @return  DOCUMENT ME!
     *
     * @throws  JRException  DOCUMENT ME!
     */
    public boolean saveToCms() throws JRException {
        try {
            if (jDesign != null) {
                ByteArrayOutputStream outputByteStream =
                    new ByteArrayOutputStream();
                JRXmlWriter.writeReport(jDesign, outputByteStream, "UTF-8");
                SQLReportFileUtil.createTextFile(reportFile, outputByteStream.toByteArray());

                return true;
            }
        } catch (Exception ioe) {
            LOG.error("Error in saving jDesign to JRXML file: "
                + ioe.toString());
        }

        return false;
    }

    /**
     * Save this object to an JRXML file specified by reportFile.
     *
     * @return  DOCUMENT ME!
     *
     * @throws  JRException  DOCUMENT ME!
     */
    public boolean saveToFileSystem() throws JRException {
        try {
            if (jDesign != null) {
                FileOutputStream outputFile = new FileOutputStream(reportFile);
                JRXmlWriter.writeReport(jDesign, outputFile, "UTF-8");
                outputFile.close();

                return true;
            }
        } catch (Exception ioe) {
            LOG.error("Error in saving jDesign to JRXML file: "
                + ioe.toString());
        }

        return false;
    }

    private JRDesignSubreport createSubreportElement(String subreportId,
        String jasperFile,
        int y, int subreportWidth, Vector<String> subreportParamNames)
        throws JRException {
        JRDesignSubreport subreport = null;
        if (jDesign != null) {
            subreport = new JRDesignSubreport(jDesign);
            subreport.setKey(subreportId);
            subreport.setPositionType(JRElement.POSITION_TYPE_FLOAT);

            // position
            subreport.setX(0);
            subreport.setY(y);
            subreport.setWidth(subreportWidth);
            subreport.setHeight(SUB_REPORT_DEFAULT_HEIGHT);

            // Connection
            JRDesignExpression jrConExpression = new JRDesignExpression();
            jrConExpression.setText("$P{REPORT_CONNECTION}");
            subreport.setConnectionExpression(jrConExpression);

            // jasper file
            JRDesignExpression jrSubExpression = new JRDesignExpression();
            jrSubExpression.setText("\"" + jasperFile + "\"");
            subreport.setExpression(jrSubExpression);

            // parameters: Use the values from master parameters
            if (subreportParamNames != null) {
                for (int i = 0; i < subreportParamNames.size(); i++) {
                    String paramName = subreportParamNames.elementAt(i);
                    JRDesignExpression paramExpression =
                        new JRDesignExpression();
                    paramExpression.setText("$P{" + subreportId + "_"
                        + paramName + "}");
                    JRDesignSubreportParameter subreportParameter =
                        new JRDesignSubreportParameter();
                    subreportParameter.setName(paramName);
                    subreportParameter.setExpression(paramExpression);
                    subreport.addParameter(subreportParameter);
                }
            }
        }

        return subreport;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  description  the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the reportFile
     */
    public String getReportFile() {
        return reportFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  reportFilePath  the reportFile to set
     */
    public void setReportFile(String reportFilePath) {
        this.reportFile = reportFilePath;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  title  the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * DOCUMENT ME!
     */
    public void debug() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Title: " + this.getTitle());
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Description: " + this.getDescription());
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the dbName
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  dbName  the dbName to set
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  the jDesign
     */
    public JasperDesign getJDesign() {
        return jDesign;
    }
}
