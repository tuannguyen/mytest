package com.bp.pensionline.sqlreport.app.jasper.jrxml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignBreak;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JRDesignSubreportParameter;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.util.CheckConfigurationKey;

public class RootJRXMLConsumer
{

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	// IO
	public static final String PLROOT_JRXML_TEMPLATE_PDF = CheckConfigurationKey.getStringValue("report.base") 
		+ "resources/root/templates/root_pdf.jrxml";
	
	// IO
	public static final String PLROOT_JRXML_TEMPLATE_XLS = CheckConfigurationKey.getStringValue("report.base") 
		+ "resources/root/templates/root_xls.jrxml";	
	
	public static final int PL_REPORT_OUTPUT_TYPE_PDF = 1;
	public static final int PL_REPORT_OUTPUT_TYPE_XLS = 2;
	
	// displaying
	public static final int SUB_REPORT_DEFAULT_WIDTH = 782;
	public static final int SUB_REPORT_DEFAULT_HEIGHT = 60;
	
	
	// Jasper design object representing this JRXML
	private JasperDesign jDesign = null;

	private String reportFile = null;	// file named after created 
	
	private String title = null;
	private String description = null;
	private int pageWidth = SUB_REPORT_DEFAULT_WIDTH;
	
	/**
	 * Create a SectionJRXMLConsumer from default XML file
	 * @param dbName: Database name to which the JRXML will connect to
	 *
	 */
	public RootJRXMLConsumer(int outputType)
	{
		if (outputType == PL_REPORT_OUTPUT_TYPE_PDF)
		{
			load(PLROOT_JRXML_TEMPLATE_PDF);	
		}
		else if (outputType == PL_REPORT_OUTPUT_TYPE_XLS)
		{
			load(PLROOT_JRXML_TEMPLATE_XLS);
		}
		
	}
	
	/**
	 * Create a SectionJRXMLConsumer from existing XML file
	 * @param reportFile: Existing report XML file
	 * @param dbName: Database name to which the JRXML will connect to
	 */
	public RootJRXMLConsumer(String reportFile)
	{
		this.reportFile = reportFile;
		load(reportFile);		
	}	
	
	/**
	 * Load an instance of JRXML from file
	 * @param reportFile
	 */
	private void load (String reportFile)
	{
		try
		{
			ByteArrayInputStream inputStream = new ByteArrayInputStream(SQLReportFileUtil.getFileData(reportFile));
			// jrxml compiling process  
			jDesign = JRXmlLoader.load(inputStream);
			
		}
		catch (Exception e) 
		{
			LOG.error("Error in loading JRXML file: " + e.toString());
			jDesign = null;
		}		
	}

	/**
	 * @param subreportJasperFiles the subreportJasperFiles to set
	 */
	public void addSubreport(String subreportId, String subreportJasperFilename, String dbName, 
			Vector<String> subreportParamNames, boolean isPageBreak)
	{
		try
		{		
			// update sub-reports
			if (jDesign != null)
			{
				JRBand jrDetailBand = jDesign.getDetail();
				JRDesignBand jrDetailDesignBand = (JRDesignBand) jrDetailBand;
				int subreportY = jrDetailDesignBand.getHeight();			
	
				//format subreportJasperFile to OS compatible file path
				File subreportJasperFile = new File(subreportJasperFilename);
				if (subreportJasperFile.exists())
				{
					int subreportWidth = SUB_REPORT_DEFAULT_WIDTH;
					JasperReport subreportJapserObject = (JasperReport)JRLoader.loadObject(new File(subreportJasperFilename));
					if (subreportJapserObject != null)
					{
						subreportWidth = subreportJapserObject.getPageWidth();
						if (subreportWidth < SUB_REPORT_DEFAULT_WIDTH)
						{
							subreportWidth = SUB_REPORT_DEFAULT_WIDTH;
						}						
					}
					
					if (subreportWidth > pageWidth)
					{
						subreportWidth +=   jDesign.getLeftMargin() +  jDesign.getRightMargin();
						pageWidth = subreportWidth;
						JRBand jrTitleBand = jDesign.getTitle();
						JRElement title =jrTitleBand.getElementByKey("image-3");
						title.setX(pageWidth - (jDesign.getRightMargin() + jDesign.getLeftMargin() +  title.getWidth()));
						JRElement titleDate = jrTitleBand.getElementByKey("textField-1");
						titleDate.setX(pageWidth  - ( jDesign.getRightMargin()+ jDesign.getLeftMargin() + titleDate.getWidth()));		
						JRElement  titleGroup = jrTitleBand.getElementByKey("textField-6");
						titleGroup.setX(pageWidth - ( jDesign.getRightMargin() + jDesign.getLeftMargin()+ titleGroup.getWidth()));
						JRBand jrPageHeaderBand= jDesign.getPageHeader();
						JRElement customerLogo = jrPageHeaderBand.getElementByKey("image-4");
						customerLogo.setX(pageWidth  -( jDesign.getRightMargin() + jDesign.getLeftMargin()+ customerLogo.getWidth()));
						jrPageHeaderBand.getElementByKey("line-1").setWidth(pageWidth - (jDesign.getRightMargin() + jDesign.getLeftMargin()));
						JRBand jrPageFooterBand= jDesign.getPageFooter();
						JRElement  cmgLogo = jrPageFooterBand.getElementByKey("image-5");
						cmgLogo.setX(pageWidth -( jDesign.getRightMargin() + jDesign.getLeftMargin() + cmgLogo.getWidth()));
						jrPageFooterBand.getElementByKey("line-2").setWidth(pageWidth - (jDesign.getRightMargin() + jDesign.getLeftMargin()));
						JRElement textReportDate = jrPageFooterBand.getElementByKey("textField-4");
						textReportDate.setX((pageWidth/2) - textReportDate.getWidth());
						JRElement textPageNumber = jrPageFooterBand.getElementByKey("textField-5");
						textPageNumber.setX(pageWidth - (jDesign.getRightMargin() + jDesign.getLeftMargin() + textPageNumber.getWidth()) );
					}
					jDesign.setPageWidth(pageWidth);
					
					JRDesignSubreport subreport = createSubreportElement(subreportId, subreportJasperFilename, 
							dbName, subreportY, subreportWidth, subreportParamNames);
					if (subreport != null)
					{
						jrDetailDesignBand.addElement(subreport);
					}
					
					// create a parameter with convention subreportId_PARAM_NAME for each subreport params
					if (subreportParamNames != null)
					{
						for (int i = 0; i < subreportParamNames.size(); i++)
						{
							String paramName = subreportParamNames.elementAt(i);
							
							JRDesignParameter copiedMasterParam = new JRDesignParameter();								
							copiedMasterParam.setName(subreportId + "_" + paramName);
							copiedMasterParam.setForPrompting(false);
							copiedMasterParam.setValueClassName("java.lang.String");
							
							jDesign.addParameter(copiedMasterParam);
						}
					}				
					
					//create a page break element
					if (isPageBreak)
					{
						JRDesignBreak jrDesignBreak = new JRDesignBreak();
						jrDesignBreak.setPositionType(JRElement.POSITION_TYPE_FLOAT);
						jrDesignBreak.setY(subreportY + SUB_REPORT_DEFAULT_HEIGHT + 1);
						jrDetailDesignBand.addElement(jrDesignBreak);
					}
					
					jrDetailDesignBand.setHeight(subreportY + SUB_REPORT_DEFAULT_HEIGHT + 2);
				}				
			}
		}
		catch (JRException jre)
		{
			LOG.error("Error while adding subreport to root jrxml: " + jre);
		}			
	}

	/**
	 * Update jDesign object with new properties. It also update the columns of table with the query
	 * input extracted fields.
	 *
	 */
	public void update()
	{
		try
		{
			if (jDesign != null)
			{							
				// TODO: Do nothing at the moment
			}
		}
		catch (Exception e) 
		{
			System.err.println("Error in updating jDesign object: " + e.toString());
		}		
	}
	
	/**
	 * Save this object to an JRXML file specified by reportFile
	 */
	public boolean saveToCms ()
	{
		try
		{
			if (jDesign != null)
			{
				ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
				JRXmlWriter.writeReport(jDesign, outputByteStream, "UTF-8");
				SQLReportFileUtil.createTextFile(reportFile, outputByteStream.toByteArray());
				return true;
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in saving jDesign to JRXML file: " + e.toString());			
		}		
		
		return false;
	}	
	
	/**
	 * Save this object to an JRXML file specified by reportFile
	 */
	public boolean saveToFileSystem ()
	{
		try
		{
			if (jDesign != null)
			{
				FileOutputStream outputFile = new FileOutputStream(reportFile);
				JRXmlWriter.writeReport(jDesign, outputFile, "UTF-8");
				outputFile.close();
				return true;
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in saving jDesign to JRXML file: " + e.toString());			
		}		
		
		return false;
	}
	
	private JRDesignSubreport createSubreportElement (String subreportId, String jasperFile, 
			String dbName, int y, int subreportWidth,
			Vector<String> subreportParamNames) throws JRException
	{
		JRDesignSubreport subreport = null;
		if (jDesign != null && jasperFile != null && dbName != null)
		{
			subreport = new JRDesignSubreport(jDesign);
			subreport.setKey(subreportId);
			subreport.setPositionType(JRElement.POSITION_TYPE_FLOAT);
						
			// position
			subreport.setX(0);
			subreport.setY(y);
			subreport.setWidth(subreportWidth);
			subreport.setHeight(SUB_REPORT_DEFAULT_HEIGHT);	
			
			// Connection
			JRDesignExpression jrConExpression = new JRDesignExpression();
			String connectionText = "$P{REPORT_CONNECTION}";
			if (dbName.equalsIgnoreCase(Constant.DATASOURCE_AQUILLA))
			{
				connectionText = "$P{" + Constant.PL_DATASOURCE_AQUILLA_PARAM_NAME + "}";
			}
			else 
			{
				connectionText = "$P{" + Constant.PL_DATASOURCE_WEBSTATS_PARAM_NAME + "}";
			}
			
			jrConExpression.setText(connectionText);
			subreport.setConnectionExpression(jrConExpression);
			
			// jasper file
			JRDesignExpression jrSubExpression = new JRDesignExpression();
			jrSubExpression.setText("\"" + jasperFile + "\"");			
			subreport.setExpression(jrSubExpression);
			
			// parameters: Use the values from master parameters
			if (subreportParamNames != null)
			{
				for (int i = 0; i < subreportParamNames.size(); i++)
				{
					String paramName = subreportParamNames.elementAt(i);
					JRDesignExpression paramExpression = new JRDesignExpression();
					paramExpression.setText("$P{" + subreportId + "_" + paramName + "}");
					
					JRDesignSubreportParameter subreportParameter = new JRDesignSubreportParameter();
					subreportParameter.setName(paramName);
					subreportParameter.setExpression(paramExpression);
					
					subreport.addParameter(subreportParameter);
				}
			}			
	
		}

		return subreport;
	}	

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @return the reportFile
	 */
	public String getReportFile()
	{
		return reportFile;
	}

	/**
	 * @param reportFile the reportFile to set
	 */
	public void setReportFile(String reportFilePath)
	{
		this.reportFile = reportFilePath;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void debug ()
	{
		LOG.debug("Title: " + this.getTitle());
		LOG.debug("Description: " + this.getDescription());
	}

	/**
	 * @return the jDesign
	 */
	public JasperDesign getJDesign()
	{
		return jDesign;
	}
}

