package com.bp.pensionline.sqlreport.app.jasper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsFolder;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.types.CmsResourceTypeFolder;
import org.opencms.file.types.CmsResourceTypePlain;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsUUID;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.app.jasper.jrxml.MasterJRXMLConsumer;
import com.bp.pensionline.sqlreport.app.jasper.jrxml.RootJRXMLConsumer;
import com.bp.pensionline.sqlreport.app.jasper.jrxml.SectionJRXMLConsumer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dao.ReportDao;
import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;
import com.bp.pensionline.sqlreport.dto.db.ReportSection;
import com.bp.pensionline.sqlreport.dto.db.SQLReportExtractFieldDTO;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.exception.PLQueryErrorException;
import com.bp.pensionline.sqlreport.handler.ManageReportGroupHandler;
import com.bp.pensionline.sqlreport.util.QueryAnalizer;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

public class PLReportProducer extends Thread
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String SQLREPORT_SECURITYMAPPER_FILE ="/system/modules/com.bp.pensionline.test_members/SQLReportSecurityMapper.xml";
	public static final String REPORT_TEMP_FOLDER = CheckConfigurationKey.getStringValue("report.base.tmp");
	public static final String REPORT_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "resources/";
	public static final String REPORT_OUTPUT_FOLDER = CheckConfigurationKey.getStringValue("report.output.folder");
	
	public static final String REPORT_SECURITY_DESCRIPTION_DEFAULT = "Default";
	public static final String REPORT_SECURITY_DESCRIPTION_EXECS = "Execs";
	public static final String REPORT_SECURITY_DESCRIPTION_TEAM = "Team";
	public String ErrorDescription = "";
	/*
	 * JasperPrint is the object contains report after result filling process
	 */
	public static JasperPrint jasperPrint; 

	/**
	 * Generate a array of bytes represent the PDF output of the section running.
	 * @param dbName
	 * @param sectionTitle
	 * @param sectionDescription
	 * @param sectionQuery
	 * @param sectionParams JRXML parameters with values which is used both for design and for running.
	 * @return the pdf file name
	 */
	public static byte[] generateSectionPDF (String dbName, String sectionTitle, String sectionDescription, 
			String sectionQuery, SQLReportExtractFieldDTO[] fields, Vector<SQLSectionParameterDTO> sectionParams)
	{
		sectionQuery = QueryAnalizer.formatSql(sectionQuery);
		Connection connection = null;
		byte[] resultBytes = null;
		
		// Initialize and SectionJRXMLConsumer object from default template
		try
		{
			long currentMillis = System.currentTimeMillis();
			String jrXMLFilename = sectionTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".jrxml";
			
			SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
			sectionJRXML.setDbName(dbName);
			sectionJRXML.setTitle(sectionTitle);
			sectionJRXML.setDescription(sectionDescription);
			sectionJRXML.setQuery(sectionQuery);
			sectionJRXML.setFields(fields);
			sectionJRXML.setParams(sectionParams);
			sectionJRXML.setForPreview(true);
			
			sectionJRXML.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
			sectionJRXML.update();
			sectionJRXML.saveToFileSystem();
			

			// filling report with data from data source  
			connection = DBConnector.getInstance().establishConnection(dbName);
			HashMap<String, String> runningParameters = new HashMap<String, String>();
			HashMap<String, String> backupParameters = new HashMap<String, String>();
			
			if (sectionParams != null)
			{
				for (int i = 0; i < sectionParams.size(); i++)
				{
					SQLSectionParameterDTO sectionParameter = sectionParams.elementAt(i);
					if (sectionParameter != null)
					{
						String name = sectionParameter.getName();
						String value = sectionParameter.getValue();
						if (value != null && !value.trim().equals(""))
						{
							runningParameters.put(name, value);
							backupParameters.put(name, value);
						}
					}
					
				}
			}
			
			try
			{
				// generate the PDF output from sectionJRXML design
				JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
				jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, runningParameters, connection);
											
														
			}
			catch (JRException e)
			{
				LOG.info("Error while fill report with parameters. Reload the report with manually replaced parameters hehe!");
				String updateQuery = updateQueryWithParameters(sectionJRXML.getJDesign().getQuery().getText(), backupParameters);
				LOG.info("updateQuery: " + updateQuery);
				JRDesignQuery jQuery = new JRDesignQuery();
//				
				jQuery.setText(updateQuery);
				sectionJRXML.getJDesign().setQuery(jQuery);			
				sectionJRXML.saveToFileSystem();
				
				// generate the XLS output from sectionJRXML design
				JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
				jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, null, connection);
				LOG.info("Recompile done!");				
			}
			
			// export to PDF
			ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);
			
			resultBytes = pdfOutputStream.toByteArray(); 
		}
		catch (Exception e)
		{
			LOG.error("Error in generating PDF section: " + e.toString());
		}
		finally
		{
			if (connection != null)
			{
				try
				{
					connection.close();	
				}
				catch (SQLException sqle)
				{
					LOG.error("Generating report: Error in closing connection: " + sqle.toString());
				}
			}			
		}
		
		return resultBytes;
	}
	
	/**
	 * Generate a array of bytes represent the PDF output of the section running.
	 * @param dbName
	 * @param sectionTitle
	 * @param sectionDescription
	 * @param sectionQuery
	 * @param sectionParams JRXML parameters with values which is used both for design and for running.
	 * @return the pdf file name
	 */
	public static byte[] generateSectionXLS (String dbName, String sectionTitle, String sectionDescription, 
			String sectionQuery, SQLReportExtractFieldDTO[] fields, Vector<SQLSectionParameterDTO> sectionParams)
	{
		sectionQuery = QueryAnalizer.formatSql(sectionQuery);
		byte[] resultBytes = null;
		Connection connection = null;
		
		// Initialize and SectionJRXMLConsumer object from default template
		try
		{
			long currentMillis = System.currentTimeMillis();
			String jrXMLFilename = sectionTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".jrxml";
			
			SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
			sectionJRXML.setDbName(dbName);
			sectionJRXML.setTitle(sectionTitle);
			sectionJRXML.setDescription(sectionDescription);
			sectionJRXML.setQuery(sectionQuery);
			sectionJRXML.setFields(fields);
			sectionJRXML.setParams(sectionParams);
			sectionJRXML.setForPreview(true);
			sectionJRXML.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
			
			sectionJRXML.update();			
			sectionJRXML.saveToFileSystem();			
			
			
			// filling report with data from data source  
			connection = DBConnector.getInstance().establishConnection(dbName);
			HashMap<String, String> runningParameters = new HashMap<String, String>();
			HashMap<String, String> backupParameters = new HashMap<String, String>();
			if (sectionParams != null)
			{
				for (int i = 0; i < sectionParams.size(); i++)
				{
					SQLSectionParameterDTO sectionParameter = sectionParams.elementAt(i);
					if (sectionParameter != null)
					{
						String name = sectionParameter.getName();
						String value = sectionParameter.getValue();
						if (value != null && !value.trim().equals(""))
						{
							runningParameters.put(name, value);
							backupParameters.put(name, value);
						}
					}
					
				}
			}
			
			
			try
			{
				// generate the XLS output from sectionJRXML design
				JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
				jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, runningParameters, connection);
											
														
			}
			catch (JRException e)
			{
				LOG.info("Error while fill report with parameters. Reload the report with manually replaced parameters hehe!");
				String updateQuery = updateQueryWithParameters(sectionJRXML.getJDesign().getQuery().getText(), backupParameters);
				LOG.info("updateQuery: " + updateQuery);
				JRDesignQuery jQuery = new JRDesignQuery();
//				
				jQuery.setText(updateQuery);
				sectionJRXML.getJDesign().setQuery(jQuery);			
				sectionJRXML.saveToFileSystem();
				
				// generate the XLS output from sectionJRXML design
				JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
				jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, null, connection);
				LOG.info("Recompile done!");				
			}
			
			// export to PDF
	        ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();

	        // coding For Excel:
	         //JRXlsExporter exporterXLS = new JRXlsExporter();
	         //JRXlsExporter exporterXLS = new JRXlsExporter();
	        JExcelApiExporter exporterXLS = new JExcelApiExporter();
	        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
	        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
	        //exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, false);			         
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, false);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, true);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
	        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, false);
	         //exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, true);
	        exporterXLS.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
	        exporterXLS.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, -1);	// unlimit
	        exporterXLS.exportReport();
	         
	        resultBytes = outputByteArray.toByteArray();				
		}
		catch (Exception e)
		{
			LOG.error("Error in generating XLS section: " + e.toString());
		}
		finally
		{
			if (connection != null)
			{
				try
				{
					connection.close();	
				}
				catch (SQLException sqle)
				{
					LOG.error("Generating report: Error in closing connection: " + sqle.toString());
				}
			}			
		}
		return resultBytes;
	}	
	
	/**
	 * Extract and save report section, master report to separate jrxml and jasper file and save them to file system
	 * @param report
	 * @return
	 */
	public static boolean addReportToFileSystem(Report report) 
		throws JRException, PLQueryErrorException, SQLException
	{
		boolean results = false;
		
		if (report != null)
		{
			results = true;
			// create a GUID for the report if it is not set
			String reportId = report.getReportId();

			// new file name
			String masterJRXMLFilename = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";
			
//			 create report folders for section jrxml and jasper 
			String sectionJRXMLFolder = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId;
			SQLReportFileUtil.createFolder(sectionJRXMLFolder);
			String sectionJasperFolder = REPORT_WORKING_FOLDER + "section/jasper/" + reportId;
			SQLReportFileUtil.createFolder(sectionJasperFolder);
			
			// create report folders for section jrxml and jasper 
			String sectionJRXMLFolderTeam = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_TEAM;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderTeam);
			String sectionJRXMLFolderExecs = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_EXECS;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderExecs);
			String sectionJRXMLFolderDefault = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderDefault);
			

			String sectionJasperFolderTeam = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_TEAM;
			SQLReportFileUtil.createFolder(sectionJasperFolderTeam);	
			String sectionJasperFolderExecs = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_EXECS;
			SQLReportFileUtil.createFolder(sectionJasperFolderExecs);
			String sectionJasperFolderDefault = new String(REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT);
			SQLReportFileUtil.createFolder(sectionJasperFolderDefault);		
						
			// extract master report without subreport information
			MasterJRXMLConsumer masterJRXML = new MasterJRXMLConsumer(report.getDatasource());
			masterJRXML.setTitle(report.getReportTitle());
			masterJRXML.setDescription(report.getReportDescription());
			
			masterJRXML.update();
			masterJRXML.setReportFile(masterJRXMLFilename);
			
			if (masterJRXML.saveToCms())
			{
				// insert into sqlreport_master table
				ReportDao.insertMasterJRXML(reportId, report.getReportTitle(), report.getReportDescription(),
						report.getDatasource());
			}	
			else
			{
				LOG.error("Error while saving master JRXML...");				
				results = false;
			}			
			
			
			// extract and save the sections to working/section folder
			Vector<ReportSection> sections = report.getSections();
			
			int securityIndicatorTeam = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_TEAM);
			int securityIndicatorExecs = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_EXECS);
			int securityIndicatorDefault = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_DEFAULT);
			
			if (sections != null)
			{
				for (int i = 0; i < sections.size(); i++)
				{					
					ReportSection section = sections.elementAt(i);
					
					//generate a SectionJRXMLConsumer object and save them to folder with folder name is reportId
					SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
					sectionJRXML.setDbName(report.getDatasource());
					sectionJRXML.setTitle(section.getSectionTitle());
					sectionJRXML.setDescription(section.getSectionDescription());					
					sectionJRXML.setParams(section.getParams());
					
					// build up section jrxml file name
					String sectionId = new CmsUUID().toString();
					String sectionJRXMLFilename = sectionJRXMLFolder + "/" + sectionId + ".jrxml";
					String sectionJasperFilename = sectionJasperFolder + "/" + sectionId + ".jasper";
					
					String sectionJRXMLFilenameTeam = sectionJRXMLFolderTeam + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameTeam = sectionJasperFolderTeam + "/" + sectionId + ".jasper";
					String sectionJRXMLFilenameExecs = sectionJRXMLFolderExecs + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameExecs = sectionJasperFolderExecs + "/" + sectionId + ".jasper";
					String sectionJRXMLFilenameDefault = sectionJRXMLFolderDefault + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameDefault = sectionJasperFolderDefault + "/" + sectionId + ".jasper";					
					
					try
					{						
						// create standard section file. Huy modified to transfer column fields from parser
						sectionJRXML.setQuery(section.getSectionQuery());
						sectionJRXML.setFields(section.getFields());
						sectionJRXML.setReportFile(sectionJRXMLFilename);
						sectionJRXML.update();
						
						if (sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilename, sectionJasperFilename);
							
							ReportDao.insertSectionJRXML(sectionId, section.getSectionTitle(), 
									section.getSectionDescription(), section.getSectionQuery(), reportId);								
						}
						else
						{
							LOG.error("Error while saving section JRXMLs...");
							results = false;
						}
						
						
						// create jrxml and jasper files for other security indicator
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorTeam);
						sectionJRXML.setReportFile(sectionJRXMLFilenameTeam);
						sectionJRXML.update();
						if (sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameTeam, sectionJasperFilenameTeam);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Team");
						}		
						
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorExecs);
						sectionJRXML.setReportFile(sectionJRXMLFilenameExecs);
						sectionJRXML.update();
						if (sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameExecs, sectionJasperFilenameExecs);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Execs");
						}
						
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorDefault);
						sectionJRXML.setReportFile(sectionJRXMLFilenameDefault);
						sectionJRXML.update();
						if (sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameDefault, sectionJasperFilenameDefault);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Default");
						}						
						
					}
					catch (JRException jre)
					{
						LOG.error("Error while updating section JRXML(" + sectionId + "): " + jre);
						throw jre;
					}
					catch (PLQueryErrorException qee)
					{
						LOG.error("Query error while updating section JRXML(" + sectionId + "): " + qee);
						throw qee;
					}
								
				}
			}
			
			
			SQLReportFileUtil.publishFolder(REPORT_WORKING_FOLDER);
			
		}
		
		return results;
	}
	
	/**
	 * Extract and save report section, master report to separate jrxml and jasper file and save them to file system
	 * @param report
	 * @return
	 */
	public static boolean updateReportToFileSystem(Report report) 
		throws JRException, PLQueryErrorException, SQLException
	{
		boolean results = false;
		
		if (report != null)
		{
			results = true;
			// create a GUID for the report if it is not set
			String reportId = report.getReportId();
			
			String masterJRXMLFilename = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";
			
//			 create report folders for section jrxml and jasper 
			String sectionJRXMLFolder = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId;
			SQLReportFileUtil.createFolder(sectionJRXMLFolder);
			
			String sectionJasperFolder = REPORT_WORKING_FOLDER + "section/jasper/" + reportId;
			SQLReportFileUtil.createFolder(sectionJasperFolder);
									
			// create report folders for section jrxml and jasper 
			String sectionJRXMLFolderTeam = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_TEAM;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderTeam);
			
			String sectionJRXMLFolderExecs = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_EXECS;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderExecs);
			
			String sectionJRXMLFolderDefault = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT;
			SQLReportFileUtil.createFolder(sectionJRXMLFolderDefault);
			
			String sectionJasperFolderTeam = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_TEAM;
			SQLReportFileUtil.createFolder(sectionJasperFolderTeam);	
			String sectionJasperFolderExecs = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_EXECS;
			SQLReportFileUtil.createFolder(sectionJasperFolderExecs);
			String sectionJasperFolderDefault = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT;
			SQLReportFileUtil.createFolder(sectionJasperFolderDefault);							
			
			// extract and save the sections to working/section folder
			Vector<ReportSection> sections = report.getSections();
			Vector<ReportSection> newSections = new Vector<ReportSection>();
			Vector<String> newSectionIds = new Vector<String>();
			
			int securityIndicatorTeam = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_TEAM);
			int securityIndicatorExecs = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_EXECS);
			int securityIndicatorDefault = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_DEFAULT);
			
			if (sections != null)
			{
				for (int i = 0; i < sections.size(); i++)
				{					
					ReportSection section = sections.elementAt(i);
					
					//generate a SectionJRXMLConsumer object and save them to folder with folder name is reportId
					SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
					sectionJRXML.setDbName(report.getDatasource());
					sectionJRXML.setTitle(section.getSectionTitle());
					sectionJRXML.setDescription(section.getSectionDescription());					
					sectionJRXML.setParams(section.getParams());
					
					// build up section jrxml file name
					String sectionId = new CmsUUID().toString();
					section.setSectionId(sectionId);
					newSections.add(section);
					newSectionIds.add(sectionId);
					
					String sectionJRXMLFilename = sectionJRXMLFolder + "/" + sectionId + ".jrxml";
					String sectionJasperFilename = sectionJasperFolder + "/" + sectionId + ".jasper";					
					
					String sectionJRXMLFilenameTeam = sectionJRXMLFolderTeam + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameTeam = sectionJasperFolderTeam + "/" + sectionId + ".jasper";
					String sectionJRXMLFilenameExecs = sectionJRXMLFolderExecs + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameExecs = sectionJasperFolderExecs + "/" + sectionId + ".jasper";
					String sectionJRXMLFilenameDefault = sectionJRXMLFolderDefault + "/" + sectionId + ".jrxml";
					String sectionJasperFilenameDefault = sectionJasperFolderDefault + "/" + sectionId + ".jasper";					
					
					try
					{				
						// Huy modified to transfer column fields from parser
						sectionJRXML.setQuery(section.getSectionQuery());
						sectionJRXML.setFields(section.getFields());
						sectionJRXML.setReportFile(sectionJRXMLFilename);
						sectionJRXML.update();
						if (sectionJRXML.saveToCms())
						{
							LOG.info("Save JRXML ok: " + sectionJRXMLFilename + "  " + sectionJasperFilename);
							compileToJasperFromCmsToCms(sectionJRXMLFilename, sectionJasperFilename);						
						}
						else
						{
							LOG.error("Error while saving section JRXMLs...");
							results = false;
						}
						
						// create jrxml and jasper files for other security indicator
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorTeam);
						sectionJRXML.setReportFile(sectionJRXMLFilenameTeam);
						sectionJRXML.update();
						if (results && sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameTeam, sectionJasperFilenameTeam);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Team");
						}		
						
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorExecs);
						sectionJRXML.setReportFile(sectionJRXMLFilenameExecs);
						sectionJRXML.update();
						if (results && sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameExecs, sectionJasperFilenameExecs);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Execs");
						}
						
						sectionJRXML.updateQueryWithSI(section.getSectionQuery(), securityIndicatorDefault);
						sectionJRXML.setReportFile(sectionJRXMLFilenameDefault);
						sectionJRXML.update();
						if (results && sectionJRXML.saveToCms())
						{
							compileToJasperFromCmsToCms(sectionJRXMLFilenameDefault, sectionJasperFilenameDefault);							
						}
						else
						{
							LOG.error("Error while saving section JRXMLs with SI for Default");
						}							
						
					}
					catch (JRException jre)
					{
						LOG.error("Error while updating section JRXML(" + sectionId + "): " + jre);
						throw jre;
					}
					catch (PLQueryErrorException qee)
					{
						LOG.error("Query error while updating section JRXML(" + sectionId + "): " + qee);
						throw qee;
					}
				}
			}
			
			LOG.info("Report sections saved");
			
			// extract master report without subreport information
			MasterJRXMLConsumer masterJRXML = new MasterJRXMLConsumer(report.getDatasource());
			masterJRXML.setTitle(report.getReportTitle());
			masterJRXML.setDescription(report.getReportDescription());
			
			masterJRXML.update();
			masterJRXML.setReportFile(masterJRXMLFilename);
			
			// make sure everything run fine before remove old sections
			if (results)
			{								
				if (masterJRXML.saveToCms())
				{				
					//don't compile to jasper file as the sub-report will be added at run-time
					
					// insert into sqlreport_master table
					ReportDao.updateMasterJRXML(reportId, report.getReportTitle(), report.getReportDescription(), 
							report.getDatasource());				
				}				
				
				// delete no longer used section files
				ArrayList<String> sectionJRXMLFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJRXMLFolder);
				for (int i = 0; i < sectionJRXMLFiles.size(); i++)
				{
					String fileName = sectionJRXMLFiles.get(i);
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJRXMLFolder  + "/" + fileName);
					}
					
				}
				
				ArrayList<String> sectionJRXMLTeamFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJRXMLFolderTeam);
				for (int i = 0; i < sectionJRXMLTeamFiles.size(); i++)
				{
					String fileName = sectionJRXMLTeamFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJRXMLFolderTeam + "/" + fileName);
					}
					
				}
				
				ArrayList<String> sectionJRXMLExecsFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJRXMLFolderExecs);
				for (int i = 0; i < sectionJRXMLExecsFiles.size(); i++)
				{
					String fileName = sectionJRXMLExecsFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJRXMLFolderExecs + "/" + fileName);
					}
					
				}
				ArrayList<String> sectionJRXMLDefaultFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJRXMLFolderDefault);
				for (int i = 0; i < sectionJRXMLDefaultFiles.size(); i++)
				{
					String fileName = sectionJRXMLDefaultFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJRXMLFolderDefault + "/" + fileName);
					}
					
				}	
				
				ArrayList<String> sectionJasperFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJasperFolder);
				for (int i = 0; i < sectionJasperFiles.size(); i++)
				{
					String fileName = sectionJasperFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJasperFolder + "/" + fileName);
					}
					
				}					
				ArrayList<String> sectionJasperTeamFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJasperFolderTeam);
				for (int i = 0; i < sectionJasperTeamFiles.size(); i++)
				{
					String fileName = sectionJasperTeamFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJasperFolderTeam + "/" + fileName);
					}
					
				}
				ArrayList<String> sectionJasperExecsFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJasperFolderExecs);
				for (int i = 0; i < sectionJasperExecsFiles.size(); i++)
				{
					String fileName = sectionJasperExecsFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJasperFolderExecs + "/" + fileName);
					}
					
				}
				ArrayList<String> sectionJasperDefaultFiles =  SQLReportFileUtil.getFilenamesInfolder(sectionJasperFolderDefault);
				for (int i = 0; i < sectionJasperDefaultFiles.size(); i++)
				{
					String fileName = sectionJasperDefaultFiles.get(i);
					
					// check if this is new file
					boolean isNewFile = false;
					for (int j = 0; j < newSectionIds.size(); j++)
					{
						String newSectionFileName = newSectionIds.elementAt(j);
						if (fileName.indexOf(newSectionFileName) > -1)
						{
							isNewFile = true;
							break;
						}
					}
					if (!isNewFile)
					{
						SQLReportFileUtil.deleteResource(sectionJasperFolderDefault + "/" + fileName);
					}									
				}		
				
				//sectionJasperFolder.mkdir();
				
				// remove all old sections of the master in sqlreport_section table
				ReportDao.removeAllSectionJRXMLOfMaster(reportId);	
				
				for (int i = 0; i < newSections.size(); i++)
				{
					ReportSection newSection = newSections.elementAt(i);
					
					// update sqlreport_section table for sections
					ReportDao.insertSectionJRXML(newSection.getSectionId(), newSection.getSectionTitle(), 
							newSection.getSectionDescription(), newSection.getSectionQuery(), reportId);						
				}	
				
				SQLReportFileUtil.publishFolder(REPORT_WORKING_FOLDER);				
			}
		}
		
		return results;
	}
	
	public static Report getMasterReportAllInfo (String reportId)
	{
		Report report = ReportDao.getMasterReport(reportId);
		
		// get section information
		Vector<ReportSection> sections = ReportDao.getAllSectionOfMaster(reportId);
		// get more section information from the jrxml
		for (int i = 0; i < sections.size(); i++)
		{
			ReportSection section = sections.elementAt(i);
			String sectionXmlPath = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + section.getSectionId() + ".jrxml";
			
			SectionJRXMLConsumer sectionJRXMLConsumer = new SectionJRXMLConsumer(sectionXmlPath);
			sectionJRXMLConsumer.setDbName(report.getDatasource());
			section.setParams(sectionJRXMLConsumer.getParams());
		}
		
		report.setSections(sections);
		
		return report;
	}
	
	public static boolean compileToJasperFromCmsToCms(String sourceFilename, String destFilename) throws JRException
	{
		// generate the PDF output from sectionJRXML design
		try
		{
			ByteArrayInputStream inputByteStream = new ByteArrayInputStream(SQLReportFileUtil.getFileData(sourceFilename));
			ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
			JasperCompileManager.compileReportToStream(inputByteStream, outputByteStream);
			
			if (outputByteStream != null)
			{
				SQLReportFileUtil.createDataFile(destFilename, outputByteStream.toByteArray());
				return true;
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in compiling report to Jasper: " + e);
		}
		
		return false;

	}
	
	public static boolean compileToJasperFromCmsToFileSystem(String sourceFilename, String destFilename, HashMap<String, String> reportParams) throws JRException
	{
		// generate the PDF output from sectionJRXML design
		try
		{
			ByteArrayInputStream inputByteStream = new ByteArrayInputStream(SQLReportFileUtil.getFileData(sourceFilename));
					
			JasperDesign jDesign = JRXmlLoader.load(inputByteStream);
			String sectionQuery = jDesign.getQuery().getText();
			Set<String> paramNames = reportParams.keySet();
			Iterator<String> paramNameItor = paramNames.iterator();
			while (paramNameItor.hasNext())
			{
				String paramName = paramNameItor.next();
				String paramValue = reportParams.get(paramName);
				LOG.info("PDF Param: " + paramName + "-" + paramValue);
				
			}
			String sectionQueryUpdate = updateQueryWithParameters(sectionQuery, reportParams);
			LOG.info("compileToJasperFromCmsToFileSystem sectionQueryUpdate: " + sectionQueryUpdate);
			JRDesignQuery jQuery = new JRDesignQuery();
//			
			jQuery.setText(sectionQueryUpdate);
			jDesign.setQuery(jQuery);
			
			ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
			JasperCompileManager.compileReportToStream(jDesign, outputByteStream);
			
			if (outputByteStream != null)
			{
				FileOutputStream outputFileStream = new FileOutputStream (destFilename);
				outputFileStream.write(outputByteStream.toByteArray());
				outputByteStream.flush();
				outputByteStream.close();
				
				return true;
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in compiling report to Jasper: " + e);
		}
		return false;
	}	
	
	/**
	 * Generate a array of bytes represent the PDF output of the master report with its sections.
	 * @param sectionTitle
	 * @param sectionDescription
	 * @param sectionQuery
	 * @param sectionParams JRXML parameters with values which is used both for design and for running.
	 * @return the pdf file name
	 */
	public static String generateMasterReportPDF (String reportId, Vector<String> sectionIds)
	{
		String pdfFilename = null;
//		Connection connection = null;
//		
//		// Initialize and SectionJRXMLConsumer object from default template
//		try
//		{
//			if (reportId != null && sectionIds != null && sectionIds.size() > 0)
//			{
//				// get report information from sqlreport_master table
//				Report reportDTO = ReportDao.getMasterReport(reportId);
//				if (reportDTO == null)
//				{
//					LOG.error("Report with id " + reportId + " doesn't exist!;");
//					return null;
//				}
//				
//				String reportTitle = reportDTO.getReportTitle();
//				long currentMillis = System.currentTimeMillis();
//				pdfFilename = reportTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".pdf";
//				String jrXMLFilename = reportTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".jrxml";
//				
//				String dbName = reportDTO.getDatasource();
//				String xmlPath = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";
//
//				MasterJRXMLConsumer masterJRXMLConsumer = new MasterJRXMLConsumer(xmlPath, dbName);
//				String subreportJasperFile = null;
//				
//				// get the section jasper files from sectionIds
//				for (int i = 0; i < sectionIds.size(); i++)
//				{
//					String sectionId = sectionIds.elementAt(i);
//					subreportJasperFile = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/"+  sectionId + ".jasper";
//					masterJRXMLConsumer.addSubreport(sectionId, subreportJasperFile, );
//				}
//								
//				masterJRXMLConsumer.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
//				
//				if (masterJRXMLConsumer.save())
//				{
//					LOG.info("save report ok...");
//					//generate the PDF output from sectionJRXML design
//					JasperReport jasperReport = JasperCompileManager.compileReport(masterJRXMLConsumer.getReportFile());
//					
//					//filling report with data from data source  
//					connection = DBConnector.getInstance().establishConnection(dbName);
//					
//					
//					jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, String>(), connection);
//					
////					 export to PDF
//					
//					JasperExportManager.exportReportToPdfFile(jasperPrint, REPORT_TEMP_FOLDER + "PDF/" + pdfFilename);
//				}
//				
//			}
//		}
//		catch (Exception e)
//		{
//			LOG.error("Error in generating PDF report: " + e.toString());
//			pdfFilename = null;
//		}
//		finally
//		{
//			if (connection != null)
//			{
//				try
//				{
//					connection.close();	
//				}
//				catch (SQLException sqle)
//				{
//					LOG.error("Generating report: Error in closing connection: " + sqle.toString());
//				}
//			}			
//		}		
		
		return pdfFilename;
	}
	
	/**
	 * Generate a array of bytes represent the PDF output of the master report with its sections.
	 * @param sectionTitle
	 * @param sectionDescription
	 * @param sectionQuery
	 * @param sectionParams JRXML parameters with values which is used both for design and for running.
	 * @return the pdf file name
	 */
	public static String generateMasterReportJasper (String reportId, 
			Vector<ReportSection> runningSections, String siDescription, HashMap<String, String> reportParams) throws Exception
	{
		String jasperOutputFilename = null;
		
		// Initialize and SectionJRXMLConsumer object from default template
		try
		{
			if (reportId != null && runningSections != null && runningSections.size() > 0)
			{
				// get report information from sqlreport_master table
				Report report = ReportDao.getMasterReport(reportId);
				if (report == null)
				{
					LOG.error("Report with id " + reportId + " doesn't exist!;");
					return null;
				}
				
				long currentMillis = System.currentTimeMillis();
				String jrXMLFilename = reportId + "_" + siDescription + "_" + currentMillis + ".jrxml";
				String jasperFilename = reportId + "_" + siDescription  + "_" + currentMillis + ".jasper";
				
				String dbName = report.getDatasource();
				String xmlPath = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";

				MasterJRXMLConsumer masterJRXMLConsumer = new MasterJRXMLConsumer(xmlPath, dbName);
				String subreportXmlFile = null;
				String subreportJasperFile = null;
				boolean isPageBreak = true;
				// get the section jasper files from sectionIds
				for (int i = 0; i < runningSections.size(); i++)
				{
					ReportSection runningSection = runningSections.elementAt(i);
					String sectionId = runningSection.getSectionId();
					
					subreportXmlFile = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + 
					siDescription + "/" +  sectionId + ".jrxml";
					subreportJasperFile = REPORT_TEMP_FOLDER + "Jasper/" + reportId + "_" + 
						siDescription + "_" +  sectionId + ".jasper";
					
					if (compileToJasperFromCmsToFileSystem(subreportXmlFile, subreportJasperFile, reportParams))
					{						
						if (i >= runningSections.size() -1)
						{
							isPageBreak = false;
						}
						masterJRXMLConsumer.addSubreport(sectionId, subreportJasperFile, 
								extractToSectionParamNames(runningSection.getParams()), isPageBreak);
					}

				}
								
				masterJRXMLConsumer.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
				
				if (masterJRXMLConsumer.saveToFileSystem())
				{
					LOG.info("save master report to file system ok... " + reportId);
					jasperFilename = REPORT_TEMP_FOLDER + "Jasper/" + jasperFilename;
					//generate the jasper file from design
					JasperCompileManager.compileReportToFile(masterJRXMLConsumer.getReportFile(), jasperFilename);
					
					jasperOutputFilename = jasperFilename;
				}
				
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in generating master jasper file: " + e.toString());
			jasperOutputFilename = null;
			throw new Exception(e.getMessage());
		}	
		
		return jasperOutputFilename;
	}	
	
	/**
	 * Get parameters of specified section
	 * @param reportId
	 * @param sectionId
	 * @return
	 */
	public static Vector<SQLSectionParameterDTO> getSectionParams (String reportId, String sectionId)
	{ 
		Vector<SQLSectionParameterDTO> sectionParams = new Vector<SQLSectionParameterDTO>();
		
		String sectionXmlPath = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + sectionId + ".jrxml";
		SectionJRXMLConsumer sectionJRXMLConsumer = new SectionJRXMLConsumer(sectionXmlPath);
		sectionParams = sectionJRXMLConsumer.getParams();
		
		return sectionParams;
	}
	
	public static byte[] generateRootReportPDF (String groupId, Vector<Report> runningReports, 
			String reportRunner, String reportStartDate, String reportEndDate, String siDescription) throws Exception
	{
		byte[] result = null;
		Connection defaultConnection = null;
		
		long now = System.currentTimeMillis(); 
		
		// Initialize and SectionJRXMLConsumer object from default template
		if (runningReports != null)
		{		
			String groupName = "";
			ReportGroup reportGroupDTO = ManageReportGroupHandler.getReportGroupInfo(groupId);
			if (reportGroupDTO != null)
			{
				groupName = reportGroupDTO.getName();
			}
			//filling report with data from data source and params gathered from sections
			HashMap rootReportParams = new HashMap();	
			Connection[] subreportConnections = new Connection[runningReports.size()];
			try
			{
				// create a root object
				RootJRXMLConsumer rootJRXMLConsumer = new RootJRXMLConsumer(RootJRXMLConsumer.PL_REPORT_OUTPUT_TYPE_PDF);
				boolean isPageBreak = true;
				for (int i = 0; i < runningReports.size(); i++)
				{
					// get the report id from running report
					Report runningReportDTO = runningReports.elementAt(i);
					String reportId = runningReportDTO.getReportId();
					Vector<ReportSection> runningSections = runningReportDTO.getSections();										
					
					String dbName = runningReportDTO.getDatasource();
					LOG.info("Getting connections to fill in as parameter: " + dbName);
					LOG.info("Fill in as start date: " + reportStartDate);	
					LOG.info("Fill in as end date: " + reportEndDate);
					
					Connection subreportConnection = DBConnector.getInstance().establishConnection(dbName);
					if (subreportConnection == null)
					{
						//If connection cann't established throw exception 
						LOG.error("DB cann't connect.");
						throw new Exception("DB cann't connect.");
					}
					subreportConnections[i] = subreportConnection;
					if (dbName != null)
					{			
						if (dbName.equals(Constant.DATASOURCE_AQUILLA))
						{
							rootReportParams.put(Constant.PL_DATASOURCE_AQUILLA_PARAM_NAME, subreportConnection);
						}
						else
						{
							rootReportParams.put(Constant.PL_DATASOURCE_WEBSTATS_PARAM_NAME, subreportConnection);
						}
					}
					
					
					// adding more report parameters
					rootReportParams.put(Constant.PL_REPORT_GROUPTITLE, groupName);
					rootReportParams.put(Constant.PL_REPORT_RUNNER, reportRunner);
					String reportDate = "";
					Date startDate = (new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT)).parse(reportStartDate);
					Date endDate = (new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT)).parse(reportEndDate);
					if (startDate != null && reportEndDate != null && !startDate.equals(endDate))
					{					
						reportDate = (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(startDate) + " to " + 
							(new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(endDate);
					}
					else if (startDate != null && reportEndDate != null && startDate.equals(endDate))
					{
						reportDate = (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(startDate);
					}
					
					LOG.info("ReportDate: " + reportDate);
					
					rootReportParams.put(Constant.PL_REPORT_STARTEND_DATE, reportDate);
					rootReportParams.put(Constant.PL_REPORT_CREATEDDATE, (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(new Date()));

					//extract to the master parameter names
					Vector<String> masterParamNames = extractToMasterParamNames(runningSections);
					
					HashMap<String, String> inputParams = populateInputParams(runningReports);
					String masterJasperFilename = generateMasterReportJasper(reportId, runningSections, siDescription, inputParams);
					if (masterJasperFilename==null || masterJasperFilename.equals(""))
					{
						//If error happen when compile jxml report to jasper throw exception 
						LOG.error("Error when save report file.");
						throw new Exception("Compile master report error.");
					}
					
					LOG.info("master jasper file generated: " + masterJasperFilename);
					if (i >= runningReports.size() - 1)
					{
						isPageBreak = false;
					}
					if (masterJasperFilename != null)
					{
						rootJRXMLConsumer.addSubreport(reportId, masterJasperFilename, 
								dbName, masterParamNames, isPageBreak);
					}
				}
				
				// update the report
				rootJRXMLConsumer.update();
				String jrxmlFilename = "pensionline_report_" + siDescription  + "_" + now + ".jrxml";
				rootJRXMLConsumer.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrxmlFilename);
				if (rootJRXMLConsumer.saveToFileSystem())
				{
					// compile to jasper file and generate the PDF
					LOG.info("save report ok...");					
					
					//generate the PDF output from sectionJRXML design
					JasperReport jasperReport = JasperCompileManager.compileReport(rootJRXMLConsumer.getReportFile());
					jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
					
					//populate the input parameters 
					HashMap<String, String> inputParams = populateInputParamsForRoot(runningReports);
					
					rootReportParams.putAll(inputParams);
					
					defaultConnection = DBConnector.getInstance().establishConnection("webstats");
					jasperPrint = JasperFillManager.fillReport(jasperReport, rootReportParams, defaultConnection);
					
					ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
					//  export to PDF					
					JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);		
					
					result = pdfOutputStream.toByteArray();
				}	
				else
				{ 
					throw new Exception("Error happen when save report file.");
				}
			}
			catch (Exception e)
			{
				LOG.error("Error in generating PDF report: " + e.toString());
				result = null;
				throw new Exception("Error in generating PDF report: " + e.getMessage());
			}
			finally
			{
				if (defaultConnection != null)
				{
					try
					{
						defaultConnection.close();	
					}
					catch (SQLException sqle)
					{
						LOG.error("Generating report: Error in closing connection: " + sqle.toString());
						throw new Exception("Error in closing connection: " + sqle.toString());
					}
				}
				if (subreportConnections != null)
				{					
					for (int i = 0; i < subreportConnections.length; i++)
					{
						if (subreportConnections[i] != null)
						{
							try
							{
								subreportConnections[i].close();	
							}
							catch (SQLException sqle)
							{
								LOG.error("Generating report: Error in closing connection: " + sqle.toString());
								throw new Exception("Error in closing connection: " + sqle.toString());
							}
						}
					}
				}			
			}
		}
		
		return result;
	}
	
	public static byte[] generateRootReportXSL (String groupId, Vector<Report> runningReports, 
			String reportRunner, String reportStartDate, String reportEndDate, String siDescription) throws Exception
	{
		byte[] result = null;
		Connection defaultConnection = null;
		
		long now = System.currentTimeMillis(); 
		
		// Initialize and SectionJRXMLConsumer object from default template
		if (runningReports != null)
		{		
			String groupName = "";
			ReportGroup reportGroupDTO = ManageReportGroupHandler.getReportGroupInfo(groupId);
			if (reportGroupDTO != null)
			{
				groupName = reportGroupDTO.getName();
			}
			
			//filling report with data from data source and params gathered from sections
			HashMap rootReportParams = new HashMap();
			Connection[] subreportConnections = new Connection[runningReports.size()];
			Vector<String> sheetNames = new Vector<String>();
			try
			{
				List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
				// create a root object
				RootJRXMLConsumer rootJRXMLConsumer = new RootJRXMLConsumer(RootJRXMLConsumer.PL_REPORT_OUTPUT_TYPE_XLS);
				// adding more report parameters
				rootReportParams.put(Constant.PL_REPORT_GROUPTITLE, groupName);
				rootReportParams.put(Constant.PL_REPORT_RUNNER, reportRunner);
				String reportDate = "";
				Date startDate = (new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT)).parse(reportStartDate);
				Date endDate = (new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT)).parse(reportEndDate);
				if (startDate != null && reportEndDate != null && !startDate.equals(endDate))
				{					
					reportDate = (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(startDate) + " to " + 
						(new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(endDate);
				}
				else
				{
					reportDate = (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(startDate);
				}
				rootReportParams.put(Constant.PL_REPORT_STARTEND_DATE, reportDate);
				rootReportParams.put(Constant.PL_REPORT_CREATEDDATE, (new SimpleDateFormat(Constant.SQLREPORT_DEFAULT_DATE_FORMAT)).format(new Date()));
				
				String jrxmlRootFilename = "pensionline_report_" + groupName + "_"+ now + ".jrxml";
				rootJRXMLConsumer.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrxmlRootFilename);
				if (rootJRXMLConsumer.saveToFileSystem())
				{
					sheetNames.add(groupName);
					//generate the XSL output from sectionJRXML design
					JasperReport jasperReport = JasperCompileManager.compileReport(rootJRXMLConsumer.getReportFile());
					jasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
					defaultConnection = DBConnector.getInstance().establishConnection("webstats");
					JasperPrint jasperPrintRoot = JasperFillManager.fillReport(jasperReport, rootReportParams, defaultConnection);
					
					jasperPrintList.add(jasperPrintRoot);
				} 
				else
				{
					LOG.error("Error when save report file.");
					throw new Exception("Error when save report file.");
				}
				
				for (int i = 0; i < runningReports.size(); i++)
				{
					// get the report id from running report
					Report runningReportDTO = runningReports.elementAt(i);					
					String reportId = runningReportDTO.getReportId();				
					Vector<ReportSection> runningSections = runningReportDTO.getSections();
										
					String dbName = runningReportDTO.getDatasource();
					LOG.info("Getting connections to fill in as parameter: " + dbName);
					Connection masterConnection = DBConnector.getInstance().establishConnection(dbName);
					if (masterConnection == null)
					{
						LOG.error("DB connection can not open. ");
						throw new Exception("DB connection can not open.");
					}
					subreportConnections[i] = masterConnection;
					// for excel output dont add sub-reports to master. Consisder the master as a single report
					String jrXMLMasterFilename = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";
					byte[] jrXMLMasterContentBytes = SQLReportFileUtil.getFileData(jrXMLMasterFilename);
					
					//generate the jasper file from master design and add to print list
					if (jrXMLMasterContentBytes != null)
					{
						JasperReport masterJasperReport = JasperCompileManager.compileReport(new ByteArrayInputStream(jrXMLMasterContentBytes));
						masterJasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
						JasperPrint masterJasperPrint = JasperFillManager.fillReport( masterJasperReport, 
								new HashMap<String, String>(), masterConnection);
						
						
						LOG.info("Add master report to print list: " + reportId + " " + masterJasperPrint);
						jasperPrintList.add(masterJasperPrint);
						sheetNames.add(runningReportDTO.getReportTitle());
						
						if (masterConnection != null && runningSections != null)
						{
							for (int j = 0; j < runningSections.size(); j++)
							{
								ReportSection runningSection = runningSections.elementAt(j);
								String sectionId = runningSection.getSectionId();

								HashMap<String, String> masterParams = populateInputParamsForSection(dbName, runningSection);
								HashMap<String, String> backupMasterParams = populateInputParamsForSection(dbName, runningSection);
								
								// get the report jasper and fill in the parameter
								String sectionJRXMLFilename = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId + "/" + siDescription + "/" + sectionId + ".jrxml";
								String sectionJasperFilename = REPORT_WORKING_FOLDER + "section/jasper/" + reportId + "/" + siDescription + "/" + sectionId + ".jasper";
								byte[] sectionJasperContentBytes = SQLReportFileUtil.getFileData(sectionJasperFilename);
								if (sectionJasperContentBytes != null)
								{
									
									try
									{
										JasperReport sectionJasperReport = (JasperReport)JRLoader.loadObject(new ByteArrayInputStream(sectionJasperContentBytes));
										sectionJasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
										JasperPrint sectionJasperPrint = JasperFillManager.fillReport( sectionJasperReport
												, masterParams, masterConnection);	
										
										

										LOG.info("Add section report to print list: " + sectionId + " : " + siDescription);
										jasperPrintList.add(sectionJasperPrint);
																				
									}
									catch (JRException e)
									{
										LOG.info("Error while fill report with parameters. Reload the report with manually replaced parameters!");
										// if failed, compile the section from JRXML file with 
										byte[] jrXMLSectionContentBytes = SQLReportFileUtil.getFileData(sectionJRXMLFilename);
										if (jrXMLSectionContentBytes != null)
										{
											JasperDesign jDesign = JRXmlLoader.load(new ByteArrayInputStream(jrXMLSectionContentBytes));
											String sectionQuery = jDesign.getQuery().getText();
											String sectionQueryUpdate = updateQueryWithParameters(sectionQuery, backupMasterParams);
											LOG.info("sectionQueryUpdate: " + sectionQueryUpdate);
											JRDesignQuery jQuery = new JRDesignQuery();
//											
											jQuery.setText(sectionQueryUpdate);
											jDesign.setQuery(jQuery);
											
											// compile report from design
											ByteArrayOutputStream sectionOutputByteStream = new ByteArrayOutputStream();
											JasperCompileManager.compileReportToStream(jDesign, sectionOutputByteStream);
											ByteArrayInputStream sectionInputByteStream = new ByteArrayInputStream(sectionOutputByteStream.toByteArray());
											
											JasperReport sectionJasperReport = (JasperReport)JRLoader.loadObject(sectionInputByteStream);
											sectionJasperReport.setProperty("net.sf.jasperreports.print.keep.full.text", "true");
											JasperPrint sectionJasperPrint = JasperFillManager.fillReport( sectionJasperReport, null, masterConnection);																							

											jasperPrintList.add(sectionJasperPrint);
										}
										else
										{
											LOG.error("Report section JRXML could not be found!");
										}
									}
									
									sheetNames.add(runningSection.getSectionTitle() + "-" + runningReportDTO.getReportTitle());

								}
							}														
						}						
					}
				}
				
				String[] sheetNamesInput = new String[sheetNames.size()];
				for (int i = 0; i < sheetNamesInput.length; i++)
				{
					sheetNamesInput[i] = sheetNames.elementAt(i);
				}
				
		        ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();

		        // coding For Excel:
		         //JRXlsExporter exporterXLS = new JRXlsExporter();
		        JExcelApiExporter exporterXLS = new JExcelApiExporter();
		        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
		        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
		        //exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, false);			         
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, false);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, true);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
		        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, false);
		        //exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, true);
		        exporterXLS.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, false);
		        exporterXLS.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, -1);	// unlimit
		        exporterXLS.setParameter(JRXlsExporterParameter.SHEET_NAMES, sheetNamesInput);
		        exporterXLS.exportReport();
		        result = outputByteArray.toByteArray(); 		
			}
			catch (Exception e)
			{
				LOG.error("Error in generating XLS report: " + e.toString());
				result = null;
				throw new Exception("Error in generating XLS report: " + e.toString());
			}
			finally
			{
				if (defaultConnection != null)
				{
					try
					{
						defaultConnection.close();	
					}
					catch (SQLException sqle)
					{
						LOG.error("Generating report: Error in closing connection: " + sqle.toString());
						throw new Exception("Error in generating XLS report: " + sqle.toString());
					}
				}
				if (subreportConnections != null)
				{					
					for (int i = 0; i < subreportConnections.length; i++)
					{
						if (subreportConnections[i] != null)
						{
							try
							{
								subreportConnections[i].close();	
							}
							catch (SQLException sqle)
							{
								LOG.error("Generating report: Error in closing connection: " + sqle.toString());
								throw new Exception("Error in generating XLS report: Error in closing connection: " + sqle.toString());
							}
						}
					}
				}			
			}
		}
		
		return result;
	}	
	
	public static Vector<String> extractToSectionParamNames (Vector<SQLSectionParameterDTO> params)
	{
		Vector<String> sectionParamNames = new Vector<String>();
		if (params != null)
		{
			for (int i = 0; i < params.size(); i++)
			{
				SQLSectionParameterDTO sectionParam = params.elementAt(i);
				if (sectionParam != null)
				{
					sectionParamNames.add(sectionParam.getName());
				}
			}
		}
		
		return sectionParamNames;
	}	
	
	public static Vector<String> extractToMasterParamNames (Vector<ReportSection> sections)
	{
		Vector<String> masterParamNames = new Vector<String>();
		if (sections != null)
		{
			for (int i = 0; i < sections.size(); i++)
			{
				ReportSection section = sections.elementAt(i);
				String sectionId = section.getSectionId();
				
				Vector<SQLSectionParameterDTO> sectionParams = section.getParams();
				if (sectionParams != null)
				{
					for (int j = 0; j < sectionParams.size(); j++)
					{
						SQLSectionParameterDTO sectionParam = sectionParams.elementAt(j);
						if (sectionParam != null)
						{
							masterParamNames.add(sectionId + "_" + sectionParam.getName());
						}
					}
				}

				
			}
		}
		
		return masterParamNames;
	}
	
	/**
	 * Populate the parameters input by user from running reports
	 * @param runningReports
	 * @return
	 */
	public static HashMap<String, String> populateInputParamsForRoot (Vector<Report> runningReports)
	{
		HashMap<String, String> reportInputParams = new HashMap<String, String>();
		if (runningReports != null)
		{
			for (int i = 0; i < runningReports.size(); i++)
			{
				Report runningReport = runningReports.elementAt(i);
				String reportId = runningReport.getReportId();
				String datasource = runningReport.getDatasource();
				LOG.info("populateInputParams for report on " + datasource);
				Vector<ReportSection> sections = runningReport.getSections();
				for (int j = 0; j < sections.size(); j++)
				{
					ReportSection section = sections.elementAt(j);
					String sectionId = section.getSectionId();
					
					Vector<SQLSectionParameterDTO> sectionParams = section.getParams();
					if (sectionParams != null)
					{
						for (int k = 0; k < sectionParams.size(); k++)
						{
							SQLSectionParameterDTO sectionParam = sectionParams.elementAt(k);
							String reportInputParamName = reportId + "_" + sectionId + "_" + sectionParam.getName();
							int reportInputParamType = sectionParam.getType();
							String reportInputParamValue = sectionParam.getValue();
							LOG.info("reportInputParamType: " + reportInputParamType);
							// if datasource is MySQL database, and param type is Date							
							if (reportInputParamType == SQLSectionParameterDTO.PARAMETER_TYPE_DATE)
							{
								reportInputParamValue = formatToDatasourceDateFormat(datasource, reportInputParamValue);
							}											
							reportInputParams.put(reportInputParamName, reportInputParamValue);
						}
					}
				}				
			}
		}
		
		return reportInputParams;
	}
	
	/**
	 * Populate the parameters input by user from running reports
	 * @param runningReports
	 * @return
	 */
	public static HashMap<String, String> populateInputParams(Vector<Report> runningReports)
	{
		HashMap<String, String> reportInputParams = new HashMap<String, String>();
		if (runningReports != null)
		{
			for (int i = 0; i < runningReports.size(); i++)
			{
				Report runningReport = runningReports.elementAt(i);
				String reportId = runningReport.getReportId();
				String datasource = runningReport.getDatasource();
				LOG.info("populateInputParams for report on " + datasource);
				Vector<ReportSection> sections = runningReport.getSections();
				for (int j = 0; j < sections.size(); j++)
				{
					ReportSection section = sections.elementAt(j);
					String sectionId = section.getSectionId();
					
					Vector<SQLSectionParameterDTO> sectionParams = section.getParams();
					if (sectionParams != null)
					{
						for (int k = 0; k < sectionParams.size(); k++)
						{
							SQLSectionParameterDTO sectionParam = sectionParams.elementAt(k);
							String reportInputParamName = sectionParam.getName();
							int reportInputParamType = sectionParam.getType();
							String reportInputParamValue = sectionParam.getValue();
							LOG.info("reportInputParamType: " + reportInputParamType);
							// if datasource is MySQL database, and param type is Date							
							if (reportInputParamType == SQLSectionParameterDTO.PARAMETER_TYPE_DATE)
							{
								reportInputParamValue = formatToDatasourceDateFormat(datasource, reportInputParamValue);
							}											
							reportInputParams.put(reportInputParamName, reportInputParamValue);
						}
					}
				}				
			}
		}
		
		return reportInputParams;
	}	
	
	/**
	 * Populate the parameters input by user from running reports
	 * @param runningReports
	 * @return
	 */
	public static HashMap<String, String> populateInputParamsForMaster (String datasource, Vector<ReportSection> runningSections)
	{
		HashMap<String, String> reportInputParams = new HashMap<String, String>();
		if (runningSections != null)
		{
			for (int i = 0; i < runningSections.size(); i++)
			{
				ReportSection runningSection = runningSections.elementAt(i);
				String sectionId = runningSection.getSectionId();
				Vector<SQLSectionParameterDTO> sectionParams = runningSection.getParams();
				if (sectionParams != null)
				{
					for (int j = 0; j < sectionParams.size(); j++)
					{
						SQLSectionParameterDTO sectionParam = sectionParams.elementAt(j);
						String reportInputParamName = sectionId + "_" + sectionParam.getName();
						int reportInputParamType = sectionParam.getType();
						String reportInputParamValue = sectionParam.getValue();
						LOG.info("reportInputParamType: " + reportInputParamType);
						// if datasource is MySQL database, and param type is Date							
						if (reportInputParamType == SQLSectionParameterDTO.PARAMETER_TYPE_DATE)
						{
							reportInputParamValue = formatToDatasourceDateFormat(datasource, reportInputParamValue);
						}											
						reportInputParams.put(reportInputParamName, reportInputParamValue);
					}
				}				
			}
		}
		
		return reportInputParams;
	}	
	
	/**
	 * Populate the parameters input by user from running reports
	 * @param runningReports
	 * @return
	 */
	public static HashMap<String, String> populateInputParamsForSection (String datasource, ReportSection runningSection)
	{
		HashMap<String, String> sectionInputParams = new HashMap<String, String>();
		if (runningSection != null)
		{
			Vector<SQLSectionParameterDTO> sectionParams = runningSection.getParams();
			if (sectionParams != null)
			{
				for (int i = 0; i < sectionParams.size(); i++)
				{
					SQLSectionParameterDTO sectionParam = sectionParams.elementAt(i);
					String sectionInputParamName = sectionParam.getName();
					int sectionInputParamType = sectionParam.getType();
					String sectionInputParamValue = sectionParam.getValue();
					//LOG.info("reportInputParamType: " + reportInputParamType);
					// if datasource is MySQL database, and param type is Date							
					if (sectionInputParamType == SQLSectionParameterDTO.PARAMETER_TYPE_DATE)
					{
						sectionInputParamValue = formatToDatasourceDateFormat(datasource, sectionInputParamValue);
					}											
					sectionInputParams.put(sectionInputParamName, sectionInputParamValue);
				}
			}
		}
		
		return sectionInputParams;
	}	
	
	public static boolean deleteMasterReport (String reportId)
	{
		
		String jrxmlResources = REPORT_WORKING_FOLDER + "section/jrxml/" + reportId;
		String jasperResources = REPORT_WORKING_FOLDER + "section/jasper/" + reportId;
		if (reportId != null)
		{
			try
			{
				// remove record in tables first
				ReportDao.removeAllSectionJRXMLOfMaster(reportId);
				ReportDao.removeMasterJRXML(reportId);				
				
				// remove all section JRXML existed belong to this report
				SQLReportFileUtil.deleteResource(jrxmlResources);
				
				// remove all section JRXML and jasper existed belong to this report
				SQLReportFileUtil.deleteResource(jasperResources);
				
				// remove the old master JRXML file
				String masterJRXMLFilename = REPORT_WORKING_FOLDER + "master/jrxml/" + reportId + ".jrxml";
				File masterJRXMLFile = new File(masterJRXMLFilename);
				if (masterJRXMLFile.exists())
				{
					masterJRXMLFile.delete();
				}
				
				//Publish resources that need to be deleted
				
				ArrayList<String> pageURIs = new ArrayList<String>();
				pageURIs.add(masterJRXMLFilename);
				pageURIs.add(jrxmlResources);
				pageURIs.add(jasperResources);
				SQLReportFileUtil.publishResources(pageURIs);
				
				return true;
				
			}
			catch (Exception e)
			{
				LOG.error("Error in deleteMasterReport: " + e);
			}
		}
		return false;
	}

	/**
	 * The date format for SQL server and MySQL need to be yyyy-MM-dd. Aquila is fine with d-MMM-yy format (which is taken from browser).
	 * @param datasource
	 * @param dateStr
	 * @return
	 */
	public static String formatToDatasourceDateFormat (String datasource, String dateStr)
	{
		String datasourceDateStr = dateStr;
		//LOG.info("formatToDatasourceDateFormat: " + datasource);
		if (dateStr != null && !dateStr.trim().equals(""))
		{
			// MySQL, SQL Server date data format
//			if (datasource != null && datasource.equals(Constant.DATASOURCE_WEBSTATS) ||
//					datasource != null && datasource.equals(Constant.DATASOURCE_CMS_DATA) ||
//					datasource != null && datasource.equals(Constant.DATASOURCE_PENSIONLINE))
			if (datasource != null && !datasource.equals(Constant.DATASOURCE_AQUILLA))				
			{
				SimpleDateFormat defaultDateFormat = new SimpleDateFormat(Constant.SQLREPORT_AJAX_DATE_FORMAT);
				try
				{
					Date dateValue = defaultDateFormat.parse(dateStr);
					SimpleDateFormat mySQLDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
					datasourceDateStr = mySQLDateFormat.format(dateValue);
				}
				catch (java.text.ParseException pe)
				{
					LOG.error(dateStr + " is not well formed with type DATE: " + pe);
				}									
			}
		}
		
		return datasourceDateStr;
	}
	
	public static int getReportSIMapping(String securityDescription)
	{
		int securityIndicator = -1;
		try
		{
			// Read security indicator from a mapper file
			XmlReader reader = new XmlReader();
			
			byte[] arr = reader.readFile(SQLREPORT_SECURITYMAPPER_FILE);

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document document = null;

			ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
			
			document = builder.parse(bIn);

			NodeList accessLevelNodeList = document.getElementsByTagName(securityDescription);

			if (accessLevelNodeList == null || accessLevelNodeList.getLength() == 0  || securityDescription == null || securityDescription.equals(""))
			{
				// if the description of the superUser acc dose not exist in
				// the file SecurityMapper
				LOG.error("getReportSIMapping from mapper file: SecurityIndicator not set for this report runner member: " + securityDescription);
			} 
			else 
			{
				// if exist
				Node accessLevelNode = accessLevelNodeList.item(0);
				if (accessLevelNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					securityIndicator = Integer.parseInt(accessLevelNode.getTextContent());				
				}
			}							
		}
		catch (Exception e) 
		{
			LOG.error("Error while getting security indicator from mapper file: " + e.toString());
		}
		
		return securityIndicator;
	}
	
	// update all reports in the system with new SI mapping logic
	public static void updateFixSIForReport ()
	{
		final String REPORT_SECTION_JRXML_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "resources/section/jrxml";
		final String REPORT_SECTION_JASPER_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "resources/section/jasper";
		
		try
		{
			int securityIndicatorTeam = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_TEAM);
			int securityIndicatorExecs = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_EXECS);
			int securityIndicatorDefault = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_DEFAULT);
			
			LOG.info("securityIndicatorTeam: " + securityIndicatorTeam);
			LOG.info("securityIndicatorExecs: " + securityIndicatorExecs);
			LOG.info("securityIndicatorDefault: " + securityIndicatorDefault);
			
			// get all sub-folders in Jasper which is the report ids
			List<String> cmsReportJasperFolders = SQLReportFileUtil.getAllSubfolders(REPORT_SECTION_JASPER_WORKING_FOLDER);
			for (int i = 0; i < cmsReportJasperFolders.size(); i++)
			{		
				String cmsReportJasperFolder = cmsReportJasperFolders.get(i);
				String reportJasperFolderPath = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsReportJasperFolder;
									
				try
				{			
					List<String> cmsSectionFolders = SQLReportFileUtil.getAllSubfolders(reportJasperFolderPath);	
					
					// delete all old section folders, just keep the file
					for (int j = 0; j < cmsSectionFolders.size(); j++)
					{
						String cmsSectionFolder = cmsSectionFolders.get(j);
						SQLReportFileUtil.deleteResource(reportJasperFolderPath  + "/" + cmsSectionFolder);
					}								
					
					// create Team, Execs and Default folder
					String teamFolderPath =  reportJasperFolderPath + "/Team";
					String execsFolderPath =  reportJasperFolderPath + "/Execs";
					String defaultFolderPath =  reportJasperFolderPath + "/Default";
					
					SQLReportFileUtil.createFolder(teamFolderPath);
					SQLReportFileUtil.createFolder(execsFolderPath);
					SQLReportFileUtil.createFolder(defaultFolderPath);	
				}
				catch (Exception e)
				{
					LOG.error("Error while deleting old Jasper folder: " + reportJasperFolderPath + ". Exception: " + e.toString());
				}																						
			}	
			
			LOG.info("Delete old jasper file and folder completes!");
			
			// get all sub-folders in JRXML which is the report ids
			List<String> cmsReportJRXMLFolders = SQLReportFileUtil.getAllSubfolders(REPORT_SECTION_JRXML_WORKING_FOLDER);		
	
			for (int i = 0; i < cmsReportJRXMLFolders.size(); i++)
			{
				
				String cmsJRXMLFolder = cmsReportJRXMLFolders.get(i);
				String reportJrxmlFolderPath = REPORT_SECTION_JRXML_WORKING_FOLDER + "/" + cmsJRXMLFolder;
				
				try
				{
					List<String> cmsSectionFolders = SQLReportFileUtil.getAllSubfolders(reportJrxmlFolderPath);	
					
					// delete all old section folders, just keep the file
					for (int j = 0; j < cmsSectionFolders.size(); j++)
					{
						String cmsSectionFolder = cmsSectionFolders.get(j);
						SQLReportFileUtil.deleteResource(reportJrxmlFolderPath  + "/" + cmsSectionFolder);
					}								
					
					// create Team, Execs and Default folder
					String teamFolderPath =  reportJrxmlFolderPath + "/Team";
					String execsFolderPath =  reportJrxmlFolderPath + "/Execs";
					String defaultFolderPath =  reportJrxmlFolderPath + "/Default";
					
					SQLReportFileUtil.createFolder(teamFolderPath);
					SQLReportFileUtil.createFolder(execsFolderPath);
					SQLReportFileUtil.createFolder(defaultFolderPath);
					
													
					List<String> cmsSectionFiles = SQLReportFileUtil.getFilenamesInfolder(reportJrxmlFolderPath);
					
					// create new jrxml file with updated query
					for (int j = 0; j < cmsSectionFiles.size(); j++)
					{
						String cmsSectionFile = cmsSectionFiles.get(j);					
						
						String sectionXmlPath = reportJrxmlFolderPath +  "/" + cmsSectionFile;						
						try
						{
							SectionJRXMLConsumer sectionJRXMLConsumer = new SectionJRXMLConsumer(sectionXmlPath);
							
							String sectionQuery = sectionJRXMLConsumer.getQuery();
							
							// Team
							// update query with new security indicator
							sectionJRXMLConsumer.updateQueryWithSI(sectionQuery, securityIndicatorTeam);
							
							// store the updated jrxml to Team folder
							sectionJRXMLConsumer.setReportFile(teamFolderPath + "/" + cmsSectionFile);
							
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathTeam = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" +  REPORT_SECURITY_DESCRIPTION_TEAM + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							if (sectionJRXMLConsumer.saveToCms())
							{
								compileToJasperFromCmsToCms(teamFolderPath + "/" + cmsSectionFile, sectionJasperPathTeam);	
							}
														
							
							// Execs
							// update query with new security indicator
							sectionJRXMLConsumer.updateQueryWithSI(sectionQuery, securityIndicatorExecs);
							
							// store the updated jrxml to Team folder
							sectionJRXMLConsumer.setReportFile(execsFolderPath + "/" + cmsSectionFile);
							
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathExecs = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" + REPORT_SECURITY_DESCRIPTION_EXECS + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							if (sectionJRXMLConsumer.saveToCms())
							{
								compileToJasperFromCmsToCms(execsFolderPath + "/" + cmsSectionFile, sectionJasperPathExecs);	
							}
							
							// Default
							// update query with new security indicator
							sectionJRXMLConsumer.updateQueryWithSI(sectionQuery, securityIndicatorDefault);
							
							// store the updated jrxml to Team folder
							sectionJRXMLConsumer.setReportFile(defaultFolderPath + "/" + cmsSectionFile);
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathDefault = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							if (sectionJRXMLConsumer.saveToCms())
							{
								compileToJasperFromCmsToCms(defaultFolderPath + "/" + cmsSectionFile, sectionJasperPathDefault);	
							}					
														
						}
						catch (Exception e)
						{
							LOG.error("Error while updating section jrxml " + sectionXmlPath + ". Exception: " + e.toString());
						}	
					}
				}
				catch (Exception e)
				{
					LOG.error("Error while updating new jrxml sections for report folder: " + reportJrxmlFolderPath + ". Exception: " + e.toString());
				}

				LOG.info("Report" + (i+1) + " with id: " + cmsJRXMLFolder + " updated!");
			}
			
			
		
		}
		catch (Exception e)
		{
			LOG.error("Error in updating report files: " + e.toString());
		}		
	}	
	
	// update all reports in the system with new SI mapping logic
	public static void updateFixSIForReportNew ()
	{
		final String REPORT_SECTION_JRXML_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "resources/section/jrxml";
		final String REPORT_SECTION_JASPER_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "resources/section/jasper";
		
		try
		{
//			int securityIndicatorTeam = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_TEAM);
//			int securityIndicatorExecs = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_EXECS);
//			int securityIndicatorDefault = getReportSIMapping(REPORT_SECURITY_DESCRIPTION_DEFAULT);
//			
//			LOG.info("securityIndicatorTeam: " + securityIndicatorTeam);
//			LOG.info("securityIndicatorExecs: " + securityIndicatorExecs);
//			LOG.info("securityIndicatorDefault: " + securityIndicatorDefault);
			
			// get all sub-folders in Jasper which is the report ids
//			List<String> cmsReportJasperFolders = SQLReportFileUtil.getAllSubfolders(REPORT_SECTION_JASPER_WORKING_FOLDER);
//			for (int i = 0; i < cmsReportJasperFolders.size(); i++)
//			{		
//				String cmsReportJasperFolder = cmsReportJasperFolders.get(i);
//				String reportJasperFolderPath = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsReportJasperFolder;
//									
//				try
//				{			
//					List<String> cmsSectionFolders = SQLReportFileUtil.getAllSubfolders(reportJasperFolderPath);	
//					
//					// delete all old section folders, just keep the file
//					for (int j = 0; j < cmsSectionFolders.size(); j++)
//					{
//						String cmsSectionFolder = cmsSectionFolders.get(j);
//						SQLReportFileUtil.deleteResource(reportJasperFolderPath  + "/" + cmsSectionFolder);
//					}								
//					
//					// create Team, Execs and Default folder
//					String teamFolderPath =  reportJasperFolderPath + "/Team";
//					String execsFolderPath =  reportJasperFolderPath + "/Execs";
//					String defaultFolderPath =  reportJasperFolderPath + "/Default";
//					
//					SQLReportFileUtil.createFolder(teamFolderPath);
//					SQLReportFileUtil.createFolder(execsFolderPath);
//					SQLReportFileUtil.createFolder(defaultFolderPath);	
//				}
//				catch (Exception e)
//				{
//					LOG.error("Error while deleting old Jasper folder: " + reportJasperFolderPath + ". Exception: " + e.toString());
//				}																						
//			}	
//			
//			LOG.info("Delete old jasper file and folder completes!");
			
			// get all sub-folders in JRXML which is the report ids
			List<String> cmsReportJRXMLFolders = SQLReportFileUtil.getAllSubfolders(REPORT_SECTION_JRXML_WORKING_FOLDER);		
	
			for (int i = 0; i < cmsReportJRXMLFolders.size(); i++)
			{
				
				String cmsJRXMLFolder = cmsReportJRXMLFolders.get(i);
				String reportJrxmlFolderPath = REPORT_SECTION_JRXML_WORKING_FOLDER + "/" + cmsJRXMLFolder;
				
				try
				{
//					List<String> cmsSectionFolders = SQLReportFileUtil.getAllSubfolders(reportJrxmlFolderPath);	
//					
					// delete all old section folders, just keep the file
//					for (int j = 0; j < cmsSectionFolders.size(); j++)
//					{
//						String cmsSectionFolder = cmsSectionFolders.get(j);
//						SQLReportFileUtil.deleteResource(reportJrxmlFolderPath  + "/" + cmsSectionFolder);
//					}								
					
					// create Team, Execs and Default folder
					String teamFolderPath =  reportJrxmlFolderPath + "/Team";
					String execsFolderPath =  reportJrxmlFolderPath + "/Execs";
					String defaultFolderPath =  reportJrxmlFolderPath + "/Default";
					
//					SQLReportFileUtil.createFolder(teamFolderPath);
//					SQLReportFileUtil.createFolder(execsFolderPath);
//					SQLReportFileUtil.createFolder(defaultFolderPath);
					
													
					List<String> cmsSectionFiles = SQLReportFileUtil.getFilenamesInfolder(reportJrxmlFolderPath);
					
					// create new jrxml file with updated query
					for (int j = 0; j < cmsSectionFiles.size(); j++)
					{
						String cmsSectionFile = cmsSectionFiles.get(j);					
						
						String sectionXmlPath = reportJrxmlFolderPath +  "/" + cmsSectionFile;						
						try
						{
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathTeam = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" +  REPORT_SECURITY_DESCRIPTION_TEAM + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							compileToJasperFromCmsToCms(teamFolderPath + "/" + cmsSectionFile, sectionJasperPathTeam);	
														
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathExecs = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" + REPORT_SECURITY_DESCRIPTION_EXECS + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							compileToJasperFromCmsToCms(execsFolderPath + "/" + cmsSectionFile, sectionJasperPathExecs);
							
							// compile to Jasper file and store to correct jasper folder
							String sectionJasperPathDefault = REPORT_SECTION_JASPER_WORKING_FOLDER + "/" + cmsJRXMLFolder + "/" + REPORT_SECURITY_DESCRIPTION_DEFAULT + "/"
								+ cmsSectionFile.substring(0, cmsSectionFile.indexOf('.')) + ".jasper";
							compileToJasperFromCmsToCms(defaultFolderPath + "/" + cmsSectionFile, sectionJasperPathDefault);					
														
						}
						catch (Exception e)
						{
							LOG.error("Error while updating section jrxml " + sectionXmlPath + ". Exception: " + e.toString());
						}	
					}
				}
				catch (Exception e)
				{
					LOG.error("Error while updating new jrxml sections for report folder: " + reportJrxmlFolderPath + ". Exception: " + e.toString());
				}

				LOG.info("Report" + (i+1) + " with id: " + cmsJRXMLFolder + " updated!");
			}
			
			
		
		}
		catch (Exception e)
		{
			LOG.error("Error in updating report files: " + e.toString());
		}		
	}	
	
	private static String updateQueryWithParameters (String query, HashMap<String, String> reportParams)
	{
		String updateQuery = query;
		if (query != null)
		{
			Set<String> paramNames = reportParams.keySet();
			Iterator<String> paramNameItor = paramNames.iterator();
			while (paramNameItor.hasNext())
			{
				String paramName = paramNameItor.next();
				String paramValue = reportParams.get(paramName);
				updateQuery = updateQuery.replace("$P{" + paramName.toUpperCase() + "}", "'" + paramValue +"'");
				
			}
		}
		
		return updateQuery;
	}
}
