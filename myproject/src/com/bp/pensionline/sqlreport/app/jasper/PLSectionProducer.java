package com.bp.pensionline.sqlreport.app.jasper;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;

import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.sqlreport.app.jasper.jrxml.SectionJRXMLConsumer;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.util.CheckConfigurationKey;

public class PLSectionProducer implements Runnable
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_TEMP_FOLDER = CheckConfigurationKey.getStringValue("report.base.tmp");
	public static final String REPORT_WORKING_FOLDER = CheckConfigurationKey.getStringValue("report.base") + "working/";
	
	private String outputType;
	private String dbName;
	private String sectionTitle;
	private String sectionDescription;	
	private String sectionQuery;
	private Vector<SQLSectionParameterDTO> sectionParams;
	
	private HttpSession userSession;
	
	private Thread thread;
	private int status = 0; 
	private String errorMessage = null;
	
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName()
	{
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName)
	{
		this.dbName = dbName;
	}

	/**
	 * @return the outputType
	 */
	public String getOutputType()
	{
		return outputType;
	}

	/**
	 * @param outputType the outputType to set
	 */
	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}

	/**
	 * @return the sectionDescription
	 */
	public String getSectionDescription()
	{
		return sectionDescription;
	}

	/**
	 * @param sectionDescription the sectionDescription to set
	 */
	public void setSectionDescription(String sectionDescription)
	{
		this.sectionDescription = sectionDescription;
	}

	/**
	 * @return the sectionParams
	 */
	public Vector<SQLSectionParameterDTO> getSectionParams()
	{
		return sectionParams;
	}

	/**
	 * @param sectionParams the sectionParams to set
	 */
	public void setSectionParams(Vector<SQLSectionParameterDTO> sectionParams)
	{
		this.sectionParams = sectionParams;
	}

	/**
	 * @return the sectionQuery
	 */
	public String getSectionQuery()
	{
		return sectionQuery;
	}

	/**
	 * @param sectionQuery the sectionQuery to set
	 */
	public void setSectionQuery(String sectionQuery)
	{
		this.sectionQuery = sectionQuery;
	}

	/**
	 * @return the sectionTitle
	 */
	public String getSectionTitle()
	{
		return sectionTitle;
	}

	/**
	 * @param sectionTitle the sectionTitle to set
	 */
	public void setSectionTitle(String sectionTitle)
	{
		this.sectionTitle = sectionTitle;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */	
	public void run()
	{
		userSession.setAttribute("SQLReportContent", null);
		LOG.info("params: " + sectionParams.size());
		if (outputType != null && outputType.equals(Constant.AJAX_SQL_REPORT_OUTPUT_XLS))
		{
			LOG.info("Section producer start...: " + sectionParams.size());			
			byte[] reportContent = generateSectionXLS(this.getDbName(), this.getSectionTitle(), this.getSectionDescription(),
					this.getSectionQuery(), sectionParams);
			
			LOG.info("reportContent byte: " + reportContent.length);
			userSession.setAttribute("SQLReportContent", reportContent);
			if (reportContent != null && reportContent.length > 0)
			{
				status = 2;
			}
			else
			{
				status = 0;
			}
			
			LOG.info("status = " + status);
		}
		else if (outputType != null && outputType.equals(Constant.AJAX_SQL_REPORT_OUTPUT_PDF))
		{
			LOG.info("Section producer start...");
			byte[] reportContent = generateSectionPDF(this.getDbName(), this.getSectionTitle(), this.getSectionDescription(),
					this.getSectionQuery(), this.getSectionParams());
			
			LOG.info("reportContent byte: " + reportContent.length);
			userSession.setAttribute("SQLReportContent", reportContent);
			if (reportContent != null && reportContent.length > 0)
			{
				status = 2;
			}
			else
			{
				status = 0;
			}
			
			LOG.info("status = " + status);			
		}
		
	}
	
    
    public void start()
    {
    	status = 1;
    	this.thread = new Thread(this);
    	this.thread.start();
    }	
    
    public void stop()
    {
    	LOG.info("Stop the thread...");
        if (thread != null && thread.isAlive())
        {
            try  {
                 thread.interrupt();
            } catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        status = 0;
        System.gc();
    }
	
	public synchronized byte[] generateSectionPDF (String dbName, String sectionTitle, String sectionDescription, 
			String sectionQuery, Vector<SQLSectionParameterDTO> sectionParams)
	{
		Connection connection = null;
		byte[] resultBytes = null;
		
		// Initialize and SectionJRXMLConsumer object from default template
		try
		{
			long currentMillis = System.currentTimeMillis();
			String jrXMLFilename = sectionTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".jrxml";
			
			SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
			sectionJRXML.setDbName(dbName);
			sectionJRXML.setTitle(sectionTitle);
			sectionJRXML.setDescription(sectionDescription);
			sectionJRXML.setQuery(sectionQuery);
			sectionJRXML.setParams(sectionParams);
			sectionJRXML.setForPreview(true);
			
			sectionJRXML.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
			sectionJRXML.update();
			sectionJRXML.saveToFileSystem();
			
			// generate the PDF output from sectionJRXML design
			JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
			
			// filling report with data from data source  
			connection = DBConnector.getInstance().establishConnection(dbName);
			HashMap<String, String> runningParameters = new HashMap<String, String>();
			if (sectionParams != null)
			{
				for (int i = 0; i < sectionParams.size(); i++)
				{
					SQLSectionParameterDTO sectionParameter = sectionParams.elementAt(i);
					if (sectionParameter != null)
					{
						String name = sectionParameter.getName();
						String value = sectionParameter.getValue();
						if (value != null && !value.trim().equals(""))
						{
							runningParameters.put(name, value);
						}
					}
					
				}
			}
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, runningParameters, connection);
			
			// export to PDF
			ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);
			
			resultBytes = pdfOutputStream.toByteArray(); 
		}
		catch (Exception e)
		{
			LOG.error("Error in generating PDF section: " + e.toString());
			errorMessage = e.toString();
			status = 0;
		}
		finally
		{
			if (connection != null)
			{
				try
				{
					connection.close();	
				}
				catch (SQLException sqle)
				{
					LOG.error("Generating report: Error in closing connection: " + sqle.toString());
				}
			}			
		}
		
		return resultBytes;
	}
	
	/**
	 * Generate a array of bytes represent the PDF output of the section running.
	 * @param dbName
	 * @param sectionTitle
	 * @param sectionDescription
	 * @param sectionQuery
	 * @param sectionParams JRXML parameters with values which is used both for design and for running.
	 * @return the pdf file name
	 */
	public synchronized byte[] generateSectionXLS (String dbName, String sectionTitle, String sectionDescription, 
			String sectionQuery, Vector<SQLSectionParameterDTO> sectionParams)
	{
		byte[] resultBytes = null;
		Connection connection = null;
		
		// Initialize and SectionJRXMLConsumer object from default template
		try
		{
			//LOG.info("generateSectionXLS Number of params: " + sectionParams.size());
			long currentMillis = System.currentTimeMillis();
			String jrXMLFilename = sectionTitle.replaceAll("[ \\t]+", "_") + currentMillis + ".jrxml";
			
			SectionJRXMLConsumer sectionJRXML = new SectionJRXMLConsumer();
			sectionJRXML.setDbName(dbName);
			sectionJRXML.setTitle(sectionTitle);
			sectionJRXML.setDescription(sectionDescription);
			sectionJRXML.setQuery(sectionQuery);			
			sectionJRXML.setParams(sectionParams);
			sectionJRXML.setForPreview(true);
			sectionJRXML.setReportFile(REPORT_TEMP_FOLDER + "JRXML/" + jrXMLFilename);
			
			sectionJRXML.update();			
			sectionJRXML.saveToFileSystem();
			//System.out.println("generateSectionXLS Number of params 2: " + sectionParams.size());
			// generate the XLS output from sectionJRXML design
			JasperReport jasperReport = JasperCompileManager.compileReport(sectionJRXML.getReportFile());
			
			// filling report with data from data source  
			connection = DBConnector.getInstance().establishConnection(dbName);
			HashMap<String, String> runningParameters = new HashMap<String, String>();
			if (sectionParams != null)
			{
				for (int i = 0; i < sectionParams.size(); i++)
				{
					SQLSectionParameterDTO sectionParameter = sectionParams.elementAt(i);
					if (sectionParameter != null)
					{
						String name = sectionParameter.getName();
						String value = sectionParameter.getValue();
						if (value != null && !value.trim().equals(""))
						{
							runningParameters.put(name, value);
						}
					}
					
				}
			}
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, runningParameters, connection);
			
			// export to PDF
			String[] sheetNamesInput = new String[] {sectionTitle};
	        ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();

	        // coding For Excel:
	         //JRXlsExporter exporterXLS = new JRXlsExporter();
	         JExcelApiExporter exporterXLS = new JExcelApiExporter();
	         exporterXLS.setParameter(JExcelApiExporterParameter.JASPER_PRINT, jasperPrint);
	         exporterXLS.setParameter(JExcelApiExporterParameter.OUTPUT_STREAM, outputByteArray);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, true);
	         //exporterXLS.setParameter(JExcelApiExporterParameter.IS_ONE_PAGE_PER_SHEET, false);			         
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_IGNORE_CELL_BORDER, false);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, true);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, true);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, true);
	         //exporterXLS.setParameter(JExcelApiExporterParameter.IS_IGNORE_GRAPHICS, true);
	         exporterXLS.setParameter(JExcelApiExporterParameter.IGNORE_PAGE_MARGINS, true);
	         exporterXLS.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET, -1);	// unlimit
	         exporterXLS.setParameter(JExcelApiExporterParameter.SHEET_NAMES, sheetNamesInput);
	         exporterXLS.exportReport();
	         resultBytes = outputByteArray.toByteArray(); 
		}
		catch (Exception e)
		{
			LOG.error("Error in generating XLS section: " + e.toString());
			errorMessage = e.toString();
			status = 0;
		}
		finally
		{
			if (connection != null)
			{
				try
				{
					connection.close();	
				}
				catch (SQLException sqle)
				{
					LOG.error("Generating report: Error in closing connection: " + sqle.toString());
				}
			}			
		}
		
		return resultBytes;
	}

	/**
	 * @return the userSession
	 */
	public HttpSession getUserSession()
	{
		return userSession;
	}

	/**
	 * @param userSession the userSession to set
	 */
	public void setUserSession(HttpSession userSession)
	{
		this.userSession = userSession;
	}

	public String getProducerSessionId ()
	{
		if (userSession != null)
		{
			return userSession.getId();
		}
		
		return null;
	}
}
