package com.bp.pensionline.sqlreport.app.jasper.jrxml;


import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.Vector;


import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

import com.bp.pensionline.sqlreport.dto.db.SQLReportExtractFieldDTO;
import com.bp.pensionline.sqlreport.dto.db.SQLSectionParameterDTO;
import com.bp.pensionline.sqlreport.exception.PLQueryErrorException;
import com.bp.pensionline.sqlreport.util.QueryAnalizer;
import com.bp.pensionline.sqlreport.util.SQLReportFileUtil;
import com.bp.pensionline.sqlreport.util.SecurityFilter;
import com.bp.pensionline.sqlreport.util.SecurityFilterFactory;
import com.bp.pensionline.util.CheckConfigurationKey;

/**
 * Handle the create, update, delete Jasper report XML file based on input properties for each section. The content
 * is initialized from a default template which contains a table with columns and will be adjust based on the query
 * supllied.
 * 
 * @author Huy Tran
 *
 */
public class SectionJRXMLConsumer
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
//	public static final String PATTERN_FOR_FUNCTIONS = "(^\\s*)((SUM) | (AVG) | (COUNT) | (MAX) | (MIN) | (GETDATE) | (DATEADD) | (DATEDIFF) |(DATEPART))\\s*\\(\\s*(\\*)|[a-zA-Z]*\\)";
	
	// IO
	public static final String PLSECTION_JRXML_TEMPLATE = CheckConfigurationKey.getStringValue("report.base") 
		+ "resources/section/templates/section.jrxml";
	
	
	// Keys
	public static final String PLSECTION_JRXML_TITLE_KEY = "section_title";
	public static final String PLSECTION_JRXML_DESCRIPTION_KEY = "section_description";
	public static final String PLSECTION_JRXML_PARAMTYPE_PROPERTY_KEY = "pl_parameter_type";
	
	
	public static final int DEFAULT_PAGE_WIDTH = 782;
	public static final int DEFAULT_PAGE_HEIGHT = 590;
	
	
	
	public static final int TITLE_CHAR_WIDTH = 10;
	public static final int DESCRIPTION_CHAR_WIDTH = 6;
	public static final int DESCRIPTION_LINE_HEIGHT = 15;
	
	public static final int DEFAULT_COLUMN_WIDTH = 100;
	public static final int COLUMN_HEADER_CHAR_WIDTH = 7;	// this value stand for upper case character
	public static final int COLUMN_DATA_CHAR_WIDTH = 7;	// this value stand for upper case character
	
	public static final int COLUMN_HEADER_HEIGHT = 20;
	
	
	// Jasper design object representing this JRXML
	JasperDesign jDesign = null;
	String dbName = null;
	
	
	private String reportFile = null;	// file named after created 
	
	private String title = null;
	private String description = null;
	private String query = null;
	private Vector<SQLSectionParameterDTO> params = new Vector<SQLSectionParameterDTO>();
	private SQLReportExtractFieldDTO[] fields = null;
		
	// Displaying
	public static final int PLSECTION_JRXML_TOTAL_WIDTH = 534;
	private int[] columnWidths = new int[]{100};
	
	private boolean isForPreview = false;
	
	private SecurityFilter securityFilter = SecurityFilterFactory.createSecurityFilter(SecurityFilter.SQL_SECURITY_FILTER);
	
//	private int pageWidth = DEFAULT_PAGE_WIDTH;
//	private int pageHeight = DEFAULT_PAGE_HEIGHT;


	/**
	 * Create a SectionJRXMLConsumer from default XML file
	 * @param dbName: Database name to which the JRXML will connect to
	 *
	 */
	public SectionJRXMLConsumer()
	{
		load(PLSECTION_JRXML_TEMPLATE);	
		
	}
	
	/**
	 * Create a SectionJRXMLConsumer from existing XML file
	 * @param reportFile: Existing report XML file
	 * @param dbName: Database name to which the JRXML will connect to
	 */
	public SectionJRXMLConsumer(String reportFile)
	{
		this.reportFile = reportFile;
		load(reportFile);		
	}	
	
	/**
	 * Load an instance of JRXML from file
	 * @param reportFile
	 */
	private void load (String reportFile)
	{
		try
		{
			ByteArrayInputStream inputStream = new ByteArrayInputStream(SQLReportFileUtil.getFileData(reportFile));
			//LOG.info("Input byte stream: " + inputStream);
			// jrxml compiling process  
			jDesign = JRXmlLoader.load(inputStream);
			//JasperReport jDesign = (JasperReport)JRLoader.loadObject(new File(reportFile));
			
			if (jDesign != null)
			{
				// get query
				this.query = jDesign.getQuery().getText();
				
				// load params
				JRParameter[] jrParams = jDesign.getParameters();
				for (int i = 0; i < jrParams.length; i++)
				{
					JRParameter jrParam = jrParams[i];
					if (jrParam != null && !jrParam.isSystemDefined())
					{
						String name = jrParam.getName();
						String description = jrParam.getDescription();
						
						// PL report param type is stored as a pl_parameter_type property
						JRPropertiesMap paramPropertiesMap = jrParam.getPropertiesMap();
						String plParamType = paramPropertiesMap.getProperty(PLSECTION_JRXML_PARAMTYPE_PROPERTY_KEY);
						int paramType = SQLSectionParameterDTO.PARAMETER_TYPE_STRING;
						if (plParamType != null)
						{
							paramType = Integer.parseInt(plParamType);
						}
						
						String defaultValue = (jrParam.getDefaultValueExpression() != null ? 
								jrParam.getDefaultValueExpression().getText() : null);
						
						SQLSectionParameterDTO plReportParam = new SQLSectionParameterDTO(name, paramType, defaultValue, description);
						
						params.add(plReportParam);
					}
				}
				
				// Get section title which is stored as key 'section_title'
//				JRBand titleBand = jDesign.getTitle();				
//				if (titleBand != null)
//				{
//					JRElement titleJRElement = titleBand.getElementByKey(PLSECTION_JRXML_TITLE_KEY);
//					if (titleJRElement != null)
//					{
//						this.title = ((JRDesignTextField)titleJRElement).getExpression().getText(); 
//					}
//					
//					JRElement descriptionJRElement = titleBand.getElementByKey(PLSECTION_JRXML_DESCRIPTION_KEY);
//					if (descriptionJRElement != null)
//					{
//						this.description = ((JRDesignTextField)descriptionJRElement).getExpression().getText(); 
//					}					
//				}
				
			}
			
		}
		catch (Exception e) 
		{
			LOG.error("Error in loading JRXML file: " + e.toString());
			jDesign = null;
		}		
	}
	
	/**
	 * Update jDesign object with new properties. It also update the columns of table with the query
	 * input extracted fields.
	 *
	 */
	public void update() throws JRException, PLQueryErrorException
	{
		if (dbName == null || dbName.trim().equals(""))
		{
			LOG.error("Data source for the section not set!");
			return;
		}
		if (jDesign != null)
		{		
			// update query
			// check if query has duplicate columns first
			SQLReportExtractFieldDTO[] updateFields = getFields();
			boolean hasDuplicateColumns = false;
			if (updateFields != null)
			{
				for (int i = 0; i < updateFields.length; i++)
				{
					SQLReportExtractFieldDTO updateField = updateFields[i];
					if (updateField != null && updateField.getColumnName() != null)
					{
						String updateFileName = updateField.getColumnName();
						for (int j = 0; j < i; j++)
						{
							SQLReportExtractFieldDTO comparedUpdateField = updateFields[j];
							if (comparedUpdateField != null && updateFileName.trim().equals(comparedUpdateField.getColumnName().trim()))
							{
								hasDuplicateColumns = true;
								break;
							}
						}
						if (hasDuplicateColumns)
						{
							break;
						}
					}
				}
			}
			
			String[][] columnNamesAndAlias = null;
			if (hasDuplicateColumns)
			{
				QueryAnalizer queryAnalizer = new QueryAnalizer(this.query);
				this.query = queryAnalizer.getRemovedDuplicateColQuery();	
				columnNamesAndAlias = queryAnalizer.getFormarlizedColumns();
			}
			
			JRDesignQuery jQuery = new JRDesignQuery();
//			String errorMessage = QueryAnalizer.checkQueryError(this.getQuery(), this.getDbName());
//			if (errorMessage != null)
//			{
//				throw new PLQueryErrorException(errorMessage);
//			}
			jQuery.setText(QueryAnalizer.formatToJasperQuery(this.getQuery()));
			jDesign.setQuery(jQuery);
			
			// update parameters
			// remove old params
			JRParameter[] jrParams = jDesign.getParameters();
			for (int i = 0; i < jrParams.length; i++)
			{
				JRParameter jrParam = jrParams[i];					
				if (jrParam != null && !jrParam.isSystemDefined())
				{
					jDesign.removeParameter(jrParam);						
				}
			}	
			LOG.info("Number of params: " + this.getParams().size());
			for (int i = 0; i < this.getParams().size(); i++)
			{
				SQLSectionParameterDTO plReportParam = this.getParams().elementAt(i);
				if (plReportParam != null)
				{
					String paramName = plReportParam.getName();
					int paramType = plReportParam.getType();
					String paramDefaultValue = plReportParam.getDefaultValue();
					String paramDescription = plReportParam.getDescription();
					
					JRDesignParameter newParam = new JRDesignParameter();
					newParam.setSystemDefined(false);
					newParam.setForPrompting(false);
					newParam.setName(paramName);
					newParam.setValueClassName("java.lang.String");
					
					JRDesignExpression jrExpression = new JRDesignExpression();
					jrExpression.setText(paramDefaultValue); // all paramerters are handled as String type
					newParam.setDefaultValueExpression(jrExpression);
					
					newParam.setDescription(paramDescription);
					
					JRPropertiesMap newParamPropMap = newParam.getPropertiesMap();
					newParamPropMap.setProperty(PLSECTION_JRXML_PARAMTYPE_PROPERTY_KEY, new Integer(paramType).toString());
					//LOG.info("new param added: " + paramName);
					jDesign.addParameter(newParam);
				}
			}
			
			// page width also table width
			int pageWidth = 0;
			int pageHeight = DEFAULT_PAGE_HEIGHT;
			int deltaHeight = 0;
			
			// the title width cell with its fontsize
			
			int titleWidth = 0;
			if (this.getTitle() != null)
			{
				titleWidth = (this.getTitle().length() * TITLE_CHAR_WIDTH) +  20;
				LOG.info("Title width: " + titleWidth);
			}
			
			// update fields
			// remove the existing fields
			JRField[] existingFields = jDesign.getFields();
			for (int i = 0; i < existingFields.length; i++)
			{
				jDesign.removeField(existingFields[i]);
			}
			
			// add new fields			
			int[] columnWidths = null;
			
			LOG.info("hasDuplicateColumns: " + hasDuplicateColumns);
			
			//String[] updateFiledClassNames = QueryAnalizer.extractFieldClassNames(this.getQuery(), this.getDbName());
			if (updateFields != null)
			{
				columnWidths = new int[updateFields.length];
				if (hasDuplicateColumns && columnNamesAndAlias!= null && columnNamesAndAlias.length == updateFields.length)
				{
					for (int i = 0; i < updateFields.length; i++)
					{
						
						if(updateFields[i] != null && columnNamesAndAlias[i] != null)
						{
							// Only add field if not exists
							boolean fieldExist = false;
							JRField[] addedFields = jDesign.getFields();
							for (int j = 0; j < addedFields.length; j++)
							{
								JRField addedField = addedFields[j];
								if (columnNamesAndAlias[i][1] != null && columnNamesAndAlias[i][1].trim().equals(addedField.getName()))
								{
									fieldExist = true;
									break;
								}
							}
							
							if (!fieldExist)
							{
								JRDesignField newField = new JRDesignField();
								LOG.info("Add field: " + columnNamesAndAlias[i][1].trim());
								newField.setName(columnNamesAndAlias[i][1].trim());
								newField.setValueClassName(updateFields[i].getClassName());
								jDesign.addField(newField);
							}
							
							// get the widths based on column names
							int columnHeaderWidth = updateFields[i].getColumnName().length() * COLUMN_HEADER_CHAR_WIDTH;
							int columnDataWidth = updateFields[i].getMaxLength() * COLUMN_DATA_CHAR_WIDTH;
							int columnWidth = (columnDataWidth > columnHeaderWidth) ? columnDataWidth : columnHeaderWidth;
							
							if (columnWidth < DEFAULT_COLUMN_WIDTH)
							{
								columnWidth = DEFAULT_COLUMN_WIDTH;
							}					
							columnWidth += 15;	// add 20 pixel padding
							pageWidth += columnWidth;
							
							//LOG.info("columnWidth " + i + ": " + columnWidth);
							// if table width (also page width) < title width, update the last column width to fit
							if (i == updateFields.length - 1 && pageWidth < titleWidth)
							{							
								columnWidth = columnWidth + (titleWidth - pageWidth);
								pageWidth = titleWidth;
							}
							
							columnWidths[i] = columnWidth;
						}
					}
				}
				else	// in any failure of processing query, use the updateFields to create fields
				{
					for (int i = 0; i < updateFields.length; i++)
					{
						
						if(updateFields[i] != null)
						{
							// Only add field if not exists
							boolean fieldExist = false;
							JRField[] addedFields = jDesign.getFields();
							for (int j = 0; j < addedFields.length; j++)
							{
								JRField addedField = addedFields[j];
								if (updateFields[i].getColumnName().trim().equals(addedField.getName()))
								{
									fieldExist = true;
									break;
								}
							}
							
							if (!fieldExist)
							{
								JRDesignField newField = new JRDesignField();
								newField.setName(updateFields[i].getColumnName().trim());
								newField.setValueClassName(updateFields[i].getClassName());
								jDesign.addField(newField);
							}
							
							// get the widths based on column names
							int columnHeaderWidth = updateFields[i].getColumnName().length() * COLUMN_HEADER_CHAR_WIDTH;
							int columnDataWidth = updateFields[i].getMaxLength() * COLUMN_DATA_CHAR_WIDTH;
							int columnWidth = (columnDataWidth > columnHeaderWidth) ? columnDataWidth : columnHeaderWidth;
							
							if (columnWidth < DEFAULT_COLUMN_WIDTH)
							{
								columnWidth = DEFAULT_COLUMN_WIDTH;
							}					
							columnWidth += 15;	// add 20 pixel padding
							pageWidth += columnWidth;
							
							//LOG.info("columnWidth " + i + ": " + columnWidth);
							// if table width (also page width) < title width, update the last column width to fit
							if (i == updateFields.length - 1 && pageWidth < titleWidth)
							{							
								columnWidth = columnWidth + (titleWidth - pageWidth);
								pageWidth = titleWidth;
							}
							
							columnWidths[i] = columnWidth;
						}
					}
				}				

			}
			
			// update title
			JRBand jrTitleBand = jDesign.getTitle();
			// update title and description
			LOG.info("page width: " + pageWidth);
			if (jrTitleBand != null)
			{
				JRElement titleEle = jrTitleBand.getElementByKey("section_title");
				if (titleEle != null && titleEle instanceof JRDesignStaticText)
				{			
//					JRDesignExpression jrExpression = new JRDesignExpression();
//					jrExpression.setValueClassName("java.lang.String");
//					jrExpression.setText("\"" + this.getTitle() + "\"");
//
//					((JRDesignTextField)titleEle).setExpression(jrExpression);
					((JRDesignStaticText)titleEle).setText(this.getTitle());
					((JRDesignStaticText)titleEle).setWidth(pageWidth);
				}
				
				// update description
				JRElement descriptionEle = jrTitleBand.getElementByKey("section_description");
				if (descriptionEle != null && descriptionEle instanceof JRDesignStaticText)
				{			
					String description = this.getDescription();
//					JRDesignExpression jrExpression = new JRDesignExpression();
//					jrExpression.setValueClassName("java.lang.String");
//					jrExpression.setText("\"" + description + "\"");
//					
//					((JRDesignTextField)descriptionEle).setExpression(jrExpression);
					((JRDesignStaticText)descriptionEle).setText(description);
					((JRDesignStaticText)descriptionEle).setWidth(pageWidth);
					
					// adjust the description height based on the length of description. Default
					// description height can store 2 line of text.					
//					if (description != null)
//					{
//						// Calculate number of lines in description
//						int numLines = (description.length() * DESCRIPTION_CHAR_WIDTH) / pageWidth + 1;
//						if ((numLines * DESCRIPTION_LINE_HEIGHT) > descriptionEle.getHeight())
//						{
//							deltaHeight = (numLines * DESCRIPTION_LINE_HEIGHT) - descriptionEle.getHeight();
//						}
//						//LOG.info("numLines: " + numLines);
//						//LOG.info("deltaHeight: " + deltaHeight);
//					}
//					((JRDesignTextField)descriptionEle).setHeight(descriptionEle.getHeight() + deltaHeight);
				}
				
				// update band height
				JRDesignBand jrTitleDesignBand = (JRDesignBand) jrTitleBand;
				jrTitleDesignBand.setHeight(jrTitleDesignBand.getHeight() + deltaHeight);
				
				// Update column header with the new fields. Also adjust the column by
				// the number of fields
				if (updateFields != null && columnWidths != null)
				{
					int colHeaderX = 0;
					for (int i = 0; i < updateFields.length; i++)
					{
						if (updateFields[i] != null)
						{
							// add new colHeader, +10 pixel padding
							JRDesignStaticText colHeader = createColumnHeaderElement(updateFields[i].getColumnName(), 
									colHeaderX, jrTitleDesignBand.getHeight() - COLUMN_HEADER_HEIGHT, 
									columnWidths[i], COLUMN_HEADER_HEIGHT);
							jrTitleDesignBand.addElement(colHeader);
							
							colHeaderX += columnWidths[i];						
						}
					}								
				}				
				
			}						

//			if (pageWidth < DEFAULT_PAGE_WIDTH)
//			{
//				pageWidth = DEFAULT_PAGE_WIDTH;
//			}
			
			
			// Update column value text field with the new fields. Also adjust the field value
			// the number of fields
			JRBand jrDetailBand = jDesign.getDetail();
			if (jrDetailBand != null && updateFields != null && columnWidths != null)
			{
				JRDesignBand jrDetailDesignBand = (JRDesignBand) jrDetailBand;
				JRElement[] jrColValueEles = jrDetailDesignBand.getElements();
				for (int i = 0; i < jrColValueEles.length; i++)
				{
					if (jrColValueEles[i] instanceof JRDesignElement)
					{
						JRDesignElement jrColValueDesignEle = (JRDesignElement) jrColValueEles[i];
						jrDetailDesignBand.removeElement(jrColValueDesignEle);							
					}						
				}
				
				int colValueX = 0;
				for (int i = 0; i < updateFields.length; i++)
				{
					if (updateFields[i] != null)
					{										
						if (hasDuplicateColumns)
						{
							JRDesignTextField colValue = createColumnValueElement(columnNamesAndAlias[i][1].trim(), updateFields[i].getClassName(),
									colValueX, columnWidths[i], jrDetailDesignBand.getHeight());
							jrDetailDesignBand.addElement(colValue);
						}
						else
						{
							// add new data row
							JRDesignTextField colValue = createColumnValueElement(updateFields[i].getColumnName().trim(), updateFields[i].getClassName(),
									colValueX, columnWidths[i], jrDetailDesignBand.getHeight());
							jrDetailDesignBand.addElement(colValue);
						}
						
						colValueX += columnWidths[i];						
					}
				}
			}
			
			jDesign.setWhenNoDataType(JasperDesign.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL);
			
			
			if (isForPreview)
			{
//				jDesign.setLeftMargin(30);
//				jDesign.setRightMargin(30);
//				jDesign.setBottomMargin(30);
//				jDesign.setTopMargin(30);
//				jDesign.setPageWidth(pageWidth + 60);
//				jDesign.setColumnWidth(pageWidth);
//				jDesign.setPageHeight(pageHeight + deltaHeight);
				jDesign.setLeftMargin(0);
				jDesign.setRightMargin(0);
				jDesign.setBottomMargin(0);
				jDesign.setTopMargin(0);
				jDesign.setPageWidth(pageWidth);
				jDesign.setColumnWidth(pageWidth);
				jDesign.setPageHeight(pageHeight + deltaHeight);				
			}
			else
			{
				jDesign.setLeftMargin(0);
				jDesign.setRightMargin(0);
				jDesign.setBottomMargin(0);
				jDesign.setTopMargin(0);
				jDesign.setPageWidth(pageWidth);
				jDesign.setColumnWidth(pageWidth);
				jDesign.setPageHeight(pageHeight + deltaHeight);			
			}			
		}	
	}
	
	/**
	 * Save this object to an JRXML file specified by reportFile
	 */
	public boolean saveToCms () throws JRException
	{
		try
		{
			if (jDesign != null)
			{
				ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
				JRXmlWriter.writeReport(jDesign, outputByteStream, "UTF-8");
				SQLReportFileUtil.createTextFile(reportFile, outputByteStream.toByteArray());
				return true;				
			}
		}
		catch (Exception ioe) 
		{
			LOG.error("Error in saving jDesign to JRXML file: " + ioe.toString());			
		}		
		
		return false;
	}
	
	/**
	 * Save this object to an JRXML file specified by reportFile
	 */
	public boolean saveToFileSystem () throws JRException
	{
		try
		{
			if (jDesign != null)
			{
				FileOutputStream outputFile = new FileOutputStream(reportFile);
				JRXmlWriter.writeReport(jDesign, outputFile, "UTF-8");
				outputFile.close();	
				return true;				
			}
		}
		catch (Exception ioe) 
		{
			LOG.error("Error in saving jDesign to JRXML file: " + ioe.toString());			
		}		
		
		return false;
	}	
	
	private JRDesignStaticText createColumnHeaderElement (String columnLabel, int x, int y, int width, int height)
	{
		JRDesignStaticText colHeader = new JRDesignStaticText();
		
		// label
		colHeader.setText(columnLabel);
		
		// position
		colHeader.setX(x);
		colHeader.setY(y);	
		colHeader.setWidth(width);
		colHeader.setHeight(height);		
		colHeader.setVerticalAlignment(JRDesignTextField.VERTICAL_ALIGN_MIDDLE);
		
		// color & font
		colHeader.setMode(JRElement.MODE_OPAQUE);
		colHeader.setBackcolor(new Color(0x33, 0x99, 0x00));
		colHeader.setForecolor(new Color(0xff, 0xff, 0xff));
		colHeader.setPdfFontName("Helvetica");
		colHeader.setFontSize(10);
		colHeader.setBold(true);
		
		// border
		colHeader.getLineBox().setLeftPadding(4);
		colHeader.getLineBox().getTopPen().setLineWidth(0.5f);
		colHeader.getLineBox().getTopPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colHeader.getLineBox().getLeftPen().setLineWidth(0.5f);
		colHeader.getLineBox().getLeftPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colHeader.getLineBox().getRightPen().setLineWidth(0.5f);
		colHeader.getLineBox().getRightPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colHeader.getLineBox().getBottomPen().setLineWidth(0.5f);
		colHeader.getLineBox().getBottomPen().setLineColor(new Color(0x66, 0x66, 0x66));
		

		
		return colHeader;
	}
	
	private JRDesignTextField createColumnValueElement (String fieldName, String fieldClassName, int x, int width, int height)
	{
		JRDesignTextField colValue = new JRDesignTextField();
		
		// label
		colValue.setPrintWhenDetailOverflows(true);

		JRDesignExpression jrExpression = new JRDesignExpression();
		//colValue.setStretchWithOverflow(true);
		//colValue.setStretchType(JRDesignTextField.STRETCH_TYPE_RELATIVE_TO_BAND_HEIGHT);
		colValue.setBlankWhenNull(true);
		jrExpression.setValueClassName(fieldClassName);
		jrExpression.setText("$F{" + fieldName + "}");		
		if (fieldClassName.equals("java.sql.Timestamp") || fieldClassName.equals("java.sql.Date"))
		{
			jrExpression.setValueClassName("java.util.Date");
			//jrExpression.setText("new java.text.SimpleDateFormat(\"dd-MMM-yyyy\").format($F{" + fieldName + "})");
			colValue.setPattern("dd-MMM-yyyy");
		}
		else if (fieldClassName.equals("java.lang.Double") || fieldClassName.equals("java.lang.Float") || fieldClassName.equals("java.math.BigDecimal")
				|| fieldClassName.equals("java.lang.Long") || fieldClassName.equals("java.lang.Integer") || fieldClassName.equals("java.lang.Short"))
		{
			colValue.setPattern("#,##0.00");
		}		
		colValue.setExpression(jrExpression);				
		colValue.setMode(JRDesignTextField.MODE_TRANSPARENT);
		
		
		// position
		colValue.setX(x);
		colValue.setY(0);
		colValue.setWidth(width);
		colValue.setHeight(height);
		colValue.setVerticalAlignment(JRDesignTextField.VERTICAL_ALIGN_MIDDLE);
		
		// color & font
		colValue.setForecolor(new Color(0x00, 0x00, 0x00));
		colValue.setFontSize(10);
		
		// border
		colValue.getLineBox().setLeftPadding(4);
		colValue.getLineBox().getTopPen().setLineWidth(0.5f);
		colValue.getLineBox().getTopPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colValue.getLineBox().getLeftPen().setLineWidth(0.5f);
		colValue.getLineBox().getLeftPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colValue.getLineBox().getRightPen().setLineWidth(0.5f);
		colValue.getLineBox().getRightPen().setLineColor(new Color(0x66, 0x66, 0x66));
		colValue.getLineBox().getBottomPen().setLineWidth(0.5f);
		colValue.getLineBox().getBottomPen().setLineColor(new Color(0x66, 0x66, 0x66));
		

		
		return colValue;
	}	

	/**
	 * @return the columnWidths
	 */
	public int[] getColumnWidths()
	{
		return columnWidths;
	}

	/**
	 * @param columnWidths the columnWidths to set
	 */
	public void setColumnWidths(int[] columnWidths)
	{
		this.columnWidths = columnWidths;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @return the params
	 */
	public Vector<SQLSectionParameterDTO> getParams()
	{
		return params;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(Vector<SQLSectionParameterDTO> params)
	{
		this.params = params;
	}

	/**
	 * @return the query
	 */
	public String getQuery()
	{
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query)
	{
//		String subStrOld = query.substring(query.toUpperCase()
//				.indexOf("SELECT") + 7,
//				query.toUpperCase().indexOf("FROM") > 0 ? query.toUpperCase()
//						.indexOf("FROM") - 1 : query.length());
//		if (!"*".equals(subStrOld)) {
//			String fields[] = subStrOld.split(",");
//			String subStrNew = "";
//			int j = 1;
//			for (int i = 0; i < fields.length; i++) {
//				Pattern compPattern = Pattern
//						.compile(PATTERN_FOR_FUNCTIONS);
//				Matcher matcher = compPattern.matcher(fields[i].toUpperCase());
//				System.out.println(fields[i]);
//				if(matcher.find()) {
//					if(!fields[i].toUpperCase().contains(" AS ")) fields[i] = fields[i] + " AS " + "[No column name "
//							+ j++ + " ]";
//					if (fields.length == 1 || i == (fields.length - 1))
//						subStrNew += fields[i];
//					else
//						subStrNew += fields[i] + ", ";
//				} else subStrNew = subStrOld;
//
//			}
//
//			query = query.replace(subStrOld, subStrNew);
//			System.out.println(query);
//		}

		this.query = query;
	}
	
	public void updateQueryWithSI(String query, int securityIndicator) throws PLQueryErrorException
	{
		if (query == null || query.trim().equals(""))
		{
			throw new PLQueryErrorException("JRXML query is null or empty");
		}		
		
		String updateQuery = securityFilter.filter(query, securityIndicator);
		LOG.info("Updated query with SI: " + updateQuery);
		if (jDesign != null)
		{
			JRDesignQuery jQuery = new JRDesignQuery();
//			String errorMessage = QueryAnalizer.checkQueryError(this.getQuery(), this.getDbName());
//			if (errorMessage != null)
//			{
//				throw new PLQueryErrorException(errorMessage);
//			}
			jQuery.setText(QueryAnalizer.formatToJasperQuery(updateQuery));
			jDesign.setQuery(jQuery);			
		}
	}

	/**
	 * @return the reportFile
	 */
	public String getReportFile()
	{
		return reportFile;
	}

	/**
	 * @param reportFile the reportFile to set
	 */
	public void setReportFile(String reportFilePath)
	{
		this.reportFile = reportFilePath;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void debug ()
	{
		LOG.debug("Title: " + this.getTitle());
		LOG.debug("Description: " + this.getDescription());
		LOG.debug("Query: " + this.getQuery());
		
		LOG.debug("------------- PARAMETERS -------------");
		for (int i = 0; i < this.getParams().size(); i++)
		{
			SQLSectionParameterDTO reportParameter = (SQLSectionParameterDTO)this.getParams().elementAt(i);
			LOG.debug("\tName: " + reportParameter.getName());
			LOG.debug("\tType: " + reportParameter.getType());
			LOG.debug("\tDefaultValue: " + reportParameter.getDefaultValue());
			LOG.debug("\tDescription: " + reportParameter.getDescription());
		}
	}

	/**
	 * @return the dbName
	 */
	public String getDbName()
	{
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName)
	{
		this.dbName = dbName;
	}

	/**
	 * @return the jDesign
	 */
	public JasperDesign getJDesign()
	{
		return jDesign;
	}

	/**
	 * @return the isForPreview
	 */
	public boolean isForPreview()
	{
		return isForPreview;
	}

	/**
	 * @param isForPreview the isForPreview to set
	 */
	public void setForPreview(boolean isForPreview)
	{
		this.isForPreview = isForPreview;
	}

	public SQLReportExtractFieldDTO[] getFields()
	{
		return fields;
	}

	public void setFields(SQLReportExtractFieldDTO[] fields)
	{
		this.fields = fields;
	}	
}
