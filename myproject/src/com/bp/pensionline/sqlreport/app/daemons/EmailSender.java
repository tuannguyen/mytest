/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.daemons;

import com.bp.pensionline.sqlreport.util.LogUtil;
import com.bp.pensionline.util.CheckConfigurationKey;

import org.apache.commons.logging.Log;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author
 * @version
 */
public class EmailSender implements Runnable {

    //~ Static fields/initializers ---------------------------------------------

    private static final Log log = LogUtil.getLog(EmailSender.class);
    private static String host = null;
    private static String port = null;
    private static String user = null;
    private static String password = null;
    private static String authentication = null;

    // private static String protocol = null;
    private static String tlsEnable = null;
    private static Properties props = null;

    //~ Instance fields --------------------------------------------------------

    /** DOCUMENT ME! */
    String from;

    /** DOCUMENT ME! */
    String[] to;

    /** DOCUMENT ME! */
    String[] cc;

    /** DOCUMENT ME! */
    String subject;

    /** DOCUMENT ME! */
    String content;

    //~ Methods ----------------------------------------------------------------

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public static EmailSender getInstance() {
        if (props == null) {
            host = CheckConfigurationKey.getStringValue("sqlreport.email.host");
            port = CheckConfigurationKey.getStringValue("sqlreport.email.port");
            user = CheckConfigurationKey.getStringValue(
                    "sqlreport.email.userid");
            password = CheckConfigurationKey.getStringValue(
                    "sqlreport.email.password");
            authentication = CheckConfigurationKey.getStringValue(
                    "sqlreport.email.auth");

            // protocol =
            // CheckConfigurationKey.getStringValue("sqlreport.email.protocol");
            tlsEnable = CheckConfigurationKey.getStringValue(
                    "sqlreport.email.tls.enable");
            props = System.getProperties();

            // props.put("mail.transport.protocol", protocol);
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.localhost", host);
            props.put("mail.smtp.auth", authentication);
            props.put("mail.debug", "false");
            if ((tlsEnable != null) && tlsEnable.equals("true")) {
                props.put("mail.smtp.socketFactory.port", port);
                props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
                props.put("mail.smtp.socketFactory.fallback", "false");
            } else {
                props.put("mail.smtp.port", port);
            }
        }

        return new EmailSender();
    }

    /**
     * DOCUMENT ME!
     */
    public void run() {
        try {
            send();
            System.out.println("Emails sent to recipients!");
            log.info("Emails sent to recipients!");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Sending schedule report email failed: "
                + e.getMessage());
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @throws  AddressException    DOCUMENT ME!
     * @throws  MessagingException  DOCUMENT ME!
     */
    public void send() {
    	// Huy modified 23/11/2010. Seperate To and Cc conditions
    	Session session = Session.getDefaultInstance(props, null);
    	if (session != null)
    	{
	        MimeMessage message = new MimeMessage(session);
	        if (message != null)
	        {
	        
	        	try {
	                message.setFrom(new InternetAddress(from));
	            } catch (AddressException e) {
	                log.error("Send mail failed: " + e.getMessage() + ". Ref: "
	                    + e.getRef());
	
	                return;
	            } catch (MessagingException e) {
	                log.error("Send mail failed: " + e.getMessage());
	
	                return;
	            }
	            
		        if ((to != null) && (to.length > 0)) {		            		            
		            for (String address : to) {
		                if (address != null) {
		                    try {
		                        message.addRecipient(Message.RecipientType.TO,
		                            new InternetAddress(address));
		                        log.info("Add Mail recipient: " + address);
		                    } catch (AddressException e) {
		                        log.error("Mail address: " + address + " invalid: "
		                            + e.getMessage() + ". Ref: " + e.getRef());
		                    } catch (MessagingException e) {
		                        log.error("Mail address: " + address + " invalid: "
		                            + e.getMessage());
		                    }
		                }
		            }
		        }
	        
		        if (cc != null) {
		            // message.addRecipient(Message.RecipientType.CC, new
		            // InternetAddress(cc));
		            for (String address : cc) {
		                if (address != null) {
		                    try {
		                        message.addRecipient(Message.RecipientType.CC,
		                            new InternetAddress(address));
		                        log.info("Add Mail recipient: " + address);
		                    } catch (AddressException e) {
		                        log.error("Mail address: " + address + " invalid: "
		                            + e.getMessage() + ". Ref: " + e.getRef());
		                    } catch (MessagingException e) {
		                        log.error("Mail address: " + address + " invalid: "
		                            + e.getMessage());
		                    }
		                }
		            }
		        }
		        
	            try {
	                message.setSubject(subject);
	            } catch (MessagingException e) {
	                log.error("Cannot set subject: " + subject + ": "
	                    + e.getMessage());
	            }
	            
	            try {
	                message.setContent(content, "text/html");
	            } catch (MessagingException e) {
	                log.error("Cannot set content: " + content + ": "
	                    + e.getMessage());
	            }
	            
	            try {
	                message.saveChanges();
	            } catch (MessagingException e) {
	                log.error("Cannot save mail content: " + content + ": "
	                    + e.getMessage());
	            }
	            
	            Transport transport = null;
	            try {
	                transport = session.getTransport("smtp");
	                if ((user == null) || user.trim().isEmpty()
	                        || (password == null)
	                        || password.isEmpty()) {
	                    transport.connect();
	                } else {
	                    transport.connect(host, user, password);
	                }
	                transport.sendMessage(message, message.getAllRecipients());
	                transport.close();
	            } catch (NoSuchProviderException e) {
	                log.error("Cannot get Transport: " + e.getMessage());
	
	                return;
	            } catch (MessagingException e) {
	                log.error("Cannot send email: " + e.getMessage());
	
	                return;
	            }
	        }
    	}
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String getFrom() {
        return from;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  from  DOCUMENT ME!
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String[] getTo() {
        return to;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  to  DOCUMENT ME!
     */
    public void setTo(String[] to) {
        this.to = to;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String[] getCc() {
        return cc;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  cc  DOCUMENT ME!
     */
    public void setCc(String[] cc) {
        this.cc = cc;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String getSubject() {
        return subject;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  subject  DOCUMENT ME!
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     */
    public String getContent() {
        return content;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  content  DOCUMENT ME!
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * DOCUMENT ME!
     *
     * @param  args  DOCUMENT ME!
     */
    @SuppressWarnings("static-access")
    public static void main(String[] args) {
        EmailSender sender = new EmailSender();
        props = System.getProperties();

        /*        props.put("mail.smtp.host", "smtp.gmail.com");
         *      props.put("mail.smtp.auth", "false");
         * props.put("mail.smtp.port", "587");     props.put("mail.debug",
         * "false");     props.put("mail.smtp.starttls.enable","true");
         */
        // props.setProperty("mail.transport.protocol", "smtp");
        // props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");

        // props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
            "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        /*        sqlreport.email.host=smtp.gmail.com
         *      sqlreport.email.port=587
         * sqlreport.email.userid=ort.noreply@c-mg.com
         * sqlreport.email.password=w3lcom3
         * sqlreport.email.from=ort.noreply@c-mg.com sqlreport.email.auth=true
         */
        sender.from = "tuan.a.nguyen@c-mg.com";
        sender.to = new String[] {
                "tuan.a.nguyen@c-mg.com", "tuan.a.nguyen@c-mg.com",
                "tuan.a.nguyen@c-mg.com"
            };
        sender.subject = "Send from Eclipse";
        sender.content = "Test message";

        // sender.host = "smtp.gmail.com";
        sender.host = "smtp.gmail.com";
        sender.user = "ort.noreply@c-mg.com";
        sender.password = "w3lcom3";
        sender.send();
    }
}
