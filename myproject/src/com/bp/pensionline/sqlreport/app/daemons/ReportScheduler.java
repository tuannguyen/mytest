/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
/**
 *
 */
package com.bp.pensionline.sqlreport.app.daemons;

import com.bp.pensionline.sqlreport.app.api.AppFactory;
import com.bp.pensionline.sqlreport.app.api.AuthoringApp;
import com.bp.pensionline.sqlreport.app.quartz.RunScheduleJob;

// import com.bp.pensionline.sqlreport.app.quartz.RunScheduleJobListener;
import com.bp.pensionline.sqlreport.dao.RecipientDao;
import com.bp.pensionline.sqlreport.dao.ScheduleAuditDao;
import com.bp.pensionline.sqlreport.dao.ScheduleDao;
import com.bp.pensionline.sqlreport.dao.ScheduleRunLogDao;
import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.db.ScheduleAudit;
import com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog;
import com.bp.pensionline.sqlreport.dto.db.User;
import com.bp.pensionline.sqlreport.util.LogUtil;

import org.apache.commons.logging.Log;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

// import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

// import org.quartz.SchedulerFactory;
import org.quartz.Trigger;

import org.quartz.impl.StdSchedulerFactory;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Manage all running schedules.
 *
 * <p>Using singleton pattern. Instance of this class should have global scope.
 * </p>
 *
 * @author  anhtuan.nguyen
 */
public final class ReportScheduler {

    //~ Static fields/initializers ---------------------------------------------

    private static final Log log = LogUtil.getLog(ReportScheduler.class);
    private static ReportScheduler instance = null;

    //~ Instance fields --------------------------------------------------------

    /**
     * Store all schedules of application.
     */
    ConcurrentHashMap<String, Schedule> schedules =
        new ConcurrentHashMap<String, Schedule>();

    //~ Constructors -----------------------------------------------------------

    private ReportScheduler() throws Exception {
        log.info("Loading all schedule objects...");
        String initErr = loadSchedules();
        if (initErr != null) {
            Exception e = new Exception(
                    "Init scheduler: Fail to load schedules from DB. "
                    + initErr);
            throw e;
        }

        /* START ALL ACTIVE SCHEDULES */
        
        log.info("Starting all schedule objects...");
        // initiate quartz and jobs
        // SchedulerFactory schdlerFactory = new StdSchedulerFactory();
        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
        quartz.start();

        // JobListener jobListener = new RunScheduleJobListener();
        // quartz.addGlobalJobListener(jobListener);
        for (Schedule schedule : schedules.values()) 
        {
        	// Delete the current job if exists first
            try {
                quartz.unscheduleJob(String.valueOf(schedule.getId()),  schedule.getReportGroup().getId());
                quartz.deleteJob(String.valueOf(schedule.getId()), schedule.getReportGroup().getId());
            } catch (SchedulerException e) {
                //e.printStackTrace();
                log.error("Error while unschedule current job: " + schedule.getTitle() + " - " + schedule.getReportGroup().getName());
            }
            
            // Added by Huy to validate the schedule before starting
            if (validateSchedule(schedule))
            {
            	System.out.println("Schedule is validated!");
	            JobDetail jobDetail = createJobDetail(schedule);
	            System.out.println("JobDetail created!");
	            CronTrigger cronTrigger = new CronTrigger(String.valueOf( 
	            		schedule.getId()), schedule.getReportGroup().getId(),
	                    schedule.getCronExpr());
	            try
				{
	            	System.out.println("Schedule job!");
	            	quartz.scheduleJob(jobDetail, cronTrigger);
				}
				catch (SchedulerException se)
				{
					System.err.println("Error while start the scheduled job: " + se.toString());
					// Ignore
					log.warn("Error while start the scheduled job: " + se.toString());
				}	            
            }
        }                
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * If instance is null, create an instance.
     *
     * @return  instance of this class
     *
     * @throws  Exception  if any error occur
     */
    public static ReportScheduler getInstance() throws Exception {
    	       
        if (instance != null) {
            return instance;
        }
                
        instance = new ReportScheduler();

        return instance;
    }

    /**
     * Add a schedule to system and schedule it with quartz.
     *
     * @param   schedule  Schedule object
     *
     * @return  null if success, an error message if fail
     *
     * @throws  Exception  if any error occur
     */
    public String addSchedule(Schedule schedule) throws Exception {
    	if (validateSchedule(schedule))
    	{
    		// Start this new schedules
	        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
	        JobDetail jobDetail = createJobDetail(schedule);
	        CronTrigger cronTrigger = new CronTrigger(String.valueOf(
	                    schedule.getId()), schedule.getReportGroup().getId(), schedule.getCronExpr());
	        quartz.scheduleJob(jobDetail, cronTrigger);
	        
	        // put new schedule to the list
	        schedules.put(String.valueOf(schedule.getId()), schedule);
    	}

        return null;
    }

    /**
     * Update a schedule to system and reschedule it with quartz.
     *
     * @param   schedule  Schedule object
     *
     * @return  null if success, an error message if fail
     *
     * @throws  Exception  if any error occur
     */
    public String updateSchedule(Schedule schedule) throws Exception {
        rescheduleJob(schedule);
        Schedule dest = schedules.get(String.valueOf(schedule.getId()));
        if (schedule.getAudits() != null) {
            int size = schedule.getAudits().size();
            if (size > 0) {
                if (dest.getAudits() == null) {
                    dest.setAudits(new ArrayList<ScheduleAudit>());
                }
                dest.getAudits().add(schedule.getAudits().get(size - 1));
            }
        }
        dest.setCronExpr(schedule.getCronExpr());
        dest.setEmailFrom(schedule.getEmailFrom());
        dest.setNextRun(schedule.getNextRun());
        dest.setNotes(schedule.getNotes());
        dest.setOutputFormat(schedule.getOutputFormat());
        dest.setReceivers(schedule.getReceivers());
        dest.setRunLevel(schedule.getRunLevel());
        dest.setStatus(schedule.getStatus());
        dest.setTitle(schedule.getTitle());

        return null;
    }

    /**
     * Remove a schedule from system and unschedule it with quartz.
     *
     * @param   schedule  Schedule object
     *
     * @return  null if success, an error message if fail
     *
     * @throws  Exception  if any error occur
     */
    public String removeSchedule(Schedule schedule) throws Exception {
        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
        quartz.unscheduleJob(String.valueOf(schedule.getId()),
            schedule.getReportGroup().getId());
        quartz.deleteJob(String.valueOf(schedule.getId()),
            schedule.getReportGroup().getId());
        schedules.remove(String.valueOf(schedule.getId()));

        return null;
    }

    /**
     * Load all schedules from DB and store in this instance.
     *
     * @return  null if success, an error message if fail
     */
    public String loadSchedules() {
        String err = null;
        Connection connection = null;
        try {
            ScheduleDao scheduleDao = new ScheduleDao();
            connection = scheduleDao.getConnection();
            List<Schedule> scheduleList = scheduleDao.getAllActiveSchedules();
            ScheduleRunLogDao runlogDao = new ScheduleRunLogDao(connection);
            ScheduleAuditDao auditDao = new ScheduleAuditDao(connection); // Reuse connection
            RecipientDao recipientDao = new RecipientDao(connection); // Reuse connection
            AuthoringApp authoringApp = AppFactory.getAuthoringApp();
            for (Schedule schedule : scheduleList) {
                // Load all receivers for each schedule
                List<User> receivers = new ArrayList<User>();
                List<User> dbos = recipientDao.selectAllOfSchedule(schedule
                        .getId());
                for (User o : dbos) {
                    o = authoringApp.getUser(o.getUserName());
                    receivers.add(o);
                }
                schedule.setReceivers(receivers);

                // Load creator and create time of schedule
                ScheduleAudit audit = auditDao.getCreationAuditOfSchedule(
                        schedule.getId());
                User creator = authoringApp.getUser(audit.getActor()
                        .getUserName());
                schedule.setCreatedBy(creator);
                schedule.setCreatedTime(audit.getActionTime());

                // Load last run for each schedule
                schedule.setPrevRuns(new ArrayList<ScheduleRunLog>());
                ScheduleRunLog runLog = runlogDao.getLastRunOfSchedule(
                        schedule);
                if (runLog != null) {
                    schedule.getPrevRuns().add(runLog);
                }
                schedules.put(String.valueOf(schedule.getId()), schedule);
            }
        } catch (Exception e) {
            err = "Load schedule info failed: " + e.getMessage();
            log.fatal(err);
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                err = "Load schedule info failed: " + e.getMessage();
                log.fatal("Cannot close connection to DB: " + e.getMessage()
                    + ". SQL Error Code = " + e.getErrorCode());
                e.printStackTrace();
            }
        }

        return err;
    }

    /**
     * Get all running schedules of given report group.
     *
     * @param   reportGroupId  Id of report group
     *
     * @return  a list of found schedules (or a empty list if no schedules
     *          found)
     */
    public List<Schedule> getSchedulesOfReportGroup(String reportGroupId) {
        reportGroupId = reportGroupId.trim();
        ArrayList<Schedule> ret = new ArrayList<Schedule>();
        for (Schedule schedule : schedules.values()) {
            if (schedule.getReportGroup().getId().equals(reportGroupId)) {
                ret.add(schedule);
            }
        }

        return ret;
    }

    /**
     * Get a running schedule by given Id.
     *
     * @param   scheduleId  Id of schedule
     *
     * @return  a schedule object if found, or null if not found
     */
    public Schedule getSchedule(String scheduleId) {
        return schedules.get(scheduleId);
    }

    /**
     * Get URL of latest physical report file of this schedule.
     *
     * @param   scheduleId
     *
     * @return  path/name of the file
     */
    @Deprecated public String getUrlOfReportFile(String scheduleId) {
        return "";
    }

    /*      public String pauseSchedule(Schedule schedule) throws Exception{
     *              quartz.pauseJob(arg0, arg1)             return null;     }
     * public String resumseSchedule(Schedule schedule) throws Exception{ return
     * null;     }
     */
    /**
     * This method is to be invoked after a schedule is triggered Currently not
     * used.
     *
     * @param   schedule
     *
     * @return
     *
     * @throws  Exception
     */
    @Deprecated public String onScheduleRunStarted(Schedule schedule)
        throws Exception {
        return null;
    }

    /**
     * This method is to be invoked after a schedule finish running Currently
     * not used.
     *
     * @param   schedule  DOCUMENT ME!
     *
     * @return  DOCUMENT ME!
     *
     * @throws  Exception  DOCUMENT ME!
     */
    @Deprecated public String onScheduleRunFinished(Schedule schedule)
        throws Exception {
        return null;
    }

    /**
     * Calculate next run of schedule.
     *
     * @param   schedule  object to calculate
     *
     * @return  next run date/time as a Date object
     *
     * @throws  SchedulerException  if quartz trigger or scheduler of this
     *                              schedule get problems
     */
    public Date getNextRun(Schedule schedule) throws SchedulerException {
    	
    	// Modified by Huy to fix the issue when trigger does not exist because the schedule is not started
    	Date nextRun = null;
    	
        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
        String id = String.valueOf(schedule.getId());
        String groupId = String.valueOf(schedule.getReportGroup().getId());
        Trigger trigger = quartz.getTrigger(id, groupId);
        if (trigger != null)
        {
        	nextRun = trigger.getNextFireTime();
        }

        return nextRun;
    }

    /**
     * Calculate last run of schedule.
     *
     * @param   schedule  object to calculate
     *
     * @return  last run date/time as a Date object
     *
     * @throws  SchedulerException  if quartz trigger or scheduler of this
     *                              schedule get problems
     */
    public Date getPrevRun(Schedule schedule) throws SchedulerException {
        List<ScheduleRunLog> prevRuns = schedule.getPrevRuns();
        if ((prevRuns != null) && (prevRuns.size() > 0)) {
            ScheduleRunLog lastRunLog = schedule.getPrevRuns().get(schedule
                    .getPrevRuns().size() - 1);
            Date lastRunDate = lastRunLog.getRunningTime();

            return lastRunDate;
        }
        
        // Modified by Huy to fix the issue when trigger does not exist because the schedule is not started
    	Date prevRun = null;
    	
        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
        String id = String.valueOf(schedule.getId());
        String groupId = String.valueOf(schedule.getReportGroup().getId());
        Trigger trigger = quartz.getTrigger(id, groupId);          
        if (trigger != null)
        {
        	prevRun = trigger.getPreviousFireTime();
        }

        return prevRun;
    }

    private void rescheduleJob(Schedule schedule) throws SchedulerException, ParseException 
    {
    	if (validateSchedule(schedule))
    	{
	        Scheduler quartz = StdSchedulerFactory.getDefaultScheduler();
	        CronTrigger cronTrigger = (CronTrigger) quartz.getTrigger(String.valueOf(schedule.getId()), schedule.getReportGroup().getId());
	        
	        String oldCronExpr = cronTrigger.getCronExpression();
	
	        log.info("Current cron expression: " + oldCronExpr);
	        log.info("New cron expression: " + schedule.getCronExpr());
	        if ((schedule.getCronExpr() != null)
	                && (oldCronExpr != null)
	                && !oldCronExpr.equalsIgnoreCase(schedule.getCronExpr())) {
	            try {
	//                cronTrigger = new CronTrigger(
	//                        String.valueOf(schedule.getId()),
	//                        schedule.getReportGroup().getId(),
	//                        schedule.getCronExpr());
	            	cronTrigger.setCronExpression(schedule.getCronExpr());
	            	cronTrigger.setStartTime(new Date());
	                quartz.rescheduleJob(String.valueOf(schedule.getId()), schedule.getReportGroup().getId(), cronTrigger);
	                
	            } catch (SchedulerException e) {
	                cronTrigger.setCronExpression(oldCronExpr);
	                cronTrigger.setStartTime(new Date());
	                quartz.rescheduleJob(String.valueOf(schedule.getId()),
	                              schedule.getReportGroup().getId(),
	                              cronTrigger);
	                 
	                throw e;
	            }
	        }
        }
    }

    private JobDetail createJobDetail(Schedule schedule)
        throws SchedulerException {
        JobDetail jobDetail = new JobDetail(String.valueOf(schedule.getId()),
                schedule.getReportGroup().getId(), RunScheduleJob.class);

        // jobDetail.addJobListener(RunScheduleJobListener.GLOBAL_NAME);
        return jobDetail;
    }

    // Scheduler quartz = null;
    /**
     * DOCUMENT ME!
     *
     * @throws  Throwable  DOCUMENT ME!
     */
    @Override protected void finalize() throws Throwable {
        // quartz.shutdown();
        super.finalize();
    }
    
    /**
     * Validate the schedule based on cron expression, receivers 
     * @param schedule
     * @return
     */
    protected boolean validateSchedule(Schedule schedule)
    {
    	boolean isValidated = true;
    	if (schedule != null)
    	{
    		if (schedule.getCronExpr() == null || !org.quartz.CronExpression.isValidExpression(schedule.getCronExpr())) 
    		{
    			log.error("The schedule " + schedule.getTitle() + " DOES NOT have valid cron expression: " + schedule.getCronExpr());
    			isValidated = false;
    	    }
    		else if (schedule.getReceivers() == null || schedule.getReceivers().size() == 0)
    		{
    			log.error("The schedule " + schedule.getTitle() + " DOES NOT have any recipients!");
    			isValidated = false;
    		}
    	}
    	
    	return isValidated;
    }
}
