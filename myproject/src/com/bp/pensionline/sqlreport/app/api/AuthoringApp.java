/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.api;

import com.bp.pensionline.sqlreport.dto.db.User;

/**
 * This interface contains methods to authorizing users and querying user info.
 *
 * @author  tuan.anh.nguyen
 */
public interface AuthoringApp {

    //~ Methods ----------------------------------------------------------------

    /**
     * Get user details by username.
     *
     * @param   username  of user to login to system
     *
     * @return  an instance of User
     */
    public User getUser(String username);
}
