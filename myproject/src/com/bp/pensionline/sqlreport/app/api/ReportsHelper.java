package com.bp.pensionline.sqlreport.app.api;

import com.bp.pensionline.sqlreport.dto.db.Report;
import com.bp.pensionline.sqlreport.dto.db.ReportGroup;

public interface ReportsHelper {
	public String createReportGroup(ReportGroup dto);
	public String updateReportGroup(ReportGroup dto);
	public String deleteReportGroup(ReportGroup dto);
	public String runReportGroup(ReportGroup dto, String output);
	public ReportGroup getReportGroup(String groupId);

	public String createReport(Report dto);
	public String updateReport(Report dto);
	public String deleteReport(Report dto);
	public String groupReport(Report report, ReportGroup group);

}
