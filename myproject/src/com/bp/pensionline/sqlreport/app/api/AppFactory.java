package com.bp.pensionline.sqlreport.app.api;

import com.bp.pensionline.sqlreport.app.impl.AuthoringAppImpl;
import com.bp.pensionline.sqlreport.app.impl.ReportingAppImpl;
import com.bp.pensionline.sqlreport.app.impl.SchedulingAppImpl;

public final class AppFactory {
//	public static final Properties appImplementProps = new Properties();
	static{
		//Load config file then insert into appImplementProps
	}
	
	public static SchedulingApp getSchedulingApp() 
	throws ClassNotFoundException, InstantiationException, IllegalAccessException{
//		Class<SchedulingApp> clazz = (Class<SchedulingApp>)
//			Class.forName(appImplementProps.getProperty("scheduling.implement.class"));
		return new SchedulingAppImpl();
	}
	public static AuthoringApp getAuthoringApp() 
	throws ClassNotFoundException, InstantiationException, IllegalAccessException{
//		Class<SchedulingApp> clazz = (Class<SchedulingApp>)
//			Class.forName(appImplementProps.getProperty("scheduling.implement.class"));
		return new AuthoringAppImpl();
	}
	
	public static ReportingApp getReportingApp() 
	throws ClassNotFoundException, InstantiationException, IllegalAccessException{
//		Class<SchedulingApp> clazz = (Class<SchedulingApp>)
//			Class.forName(appImplementProps.getProperty("scheduling.implement.class"));
		return new ReportingAppImpl();
	}
}
