/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.api;

import com.bp.pensionline.sqlreport.dto.db.Schedule;
import com.bp.pensionline.sqlreport.dto.io.DownloadScheduleOutputRequest;
import com.bp.pensionline.sqlreport.dto.io.PersistScheduleRequest;
import com.bp.pensionline.sqlreport.dto.io.QuerySchedulesRequest;
import com.bp.pensionline.sqlreport.dto.io.RunScheduleRequest;

import java.util.List;

/**
 * This interface includes all methods relating to report scheduling.
 *
 * <p>This interface includes all methods relating to report scheduling. To
 * retrieve an instance of this interface, use AppFactory class.</p>
 *
 * @author  tuan.anh.nguyen
 */
public interface SchedulingApp {

    //~ Methods ----------------------------------------------------------------

    /**
     * Trigger a schedule of a report group.
     *
     * @param   req  a wrapper object, include information of schedule and the
     *               user who run it
     *
     * @return  null if schedule run successfully, or a error message if failed
     */
    public String runSchedule(RunScheduleRequest req);

    /**
     * Log a record into DB to trace what report has been downloaded, and who
     * has downloaded.
     *
     * @param   req  a wrapper object content information about download action
     *
     * @return  null if success, or a error message if failed
     */
    public String logDownload(DownloadScheduleOutputRequest req);

    /**
     * Query schedules available in system.
     *
     * @param   request  an object contains query information
     *
     * @return  a list of schedules matches the query criteria
     *
     * @throws  Exception  when any error occurs
     */
    public List<Schedule> getSchedules(QuerySchedulesRequest request)
        throws Exception;

    /**
     * Modify schedule information. This method can be use to edit or delete a
     * schedule
     *
     * @param   req  an object stores information of schedule to edit, new
     *               values, and user who doing edit
     *
     * @return  null if success, or a error message if failed
     */
    public String persistSchedule(PersistScheduleRequest req);

    // public String updateSchedule(PersistScheduleRequest req);
    // public String deleteSchedule(DeleteScheduleRequest req);
    /**
     * Send an alert email to selected users.
     *
     * @param   schedule  the schedule which has been run and in needs of
     *                    alerting users to download its output
     *
     * @return  null if success, or a error message if failed
     */
    public String mailToReceivers(Schedule schedule);
}
