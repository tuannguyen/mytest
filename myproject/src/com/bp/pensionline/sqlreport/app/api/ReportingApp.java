/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.bp.pensionline.sqlreport.app.api;

import com.bp.pensionline.sqlreport.dto.io.RunReportGroupRequest;

/**
 * This interface contains methods relating to report.
 *
 * @author  tuan.anh.nguyen
 */
public interface ReportingApp {

    //~ Methods ----------------------------------------------------------------

    /**
     * Run a group of report. This method will run all reports in report group,
     * with all sections in each report
     *
     * @param   req  a wrapper object containing report group to run and other
     *               instruction info to run that report
     *
     * @return  null if success, or an error message if failed
     */
    public String runReportGroup(RunReportGroupRequest req);
}
