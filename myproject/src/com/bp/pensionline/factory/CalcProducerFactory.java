package com.bp.pensionline.factory;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.calc.CalcMapper;
import com.bp.pensionline.calc.producer.BPCalcProducer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;

public class CalcProducerFactory extends Thread{
	private MemberDao memberDao = null;
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run()
	{
		if (memberDao != null)
		{
			// Modified by HUY to move calc to proper page
			if (memberDao.get("CalcStarted") == null || memberDao.get("CalcStarted").equals("") || memberDao.get("CalcStarted").equals("false"))
			{
				memberDao.set("CalcStarted", "true");
				try
				{
					String sessionId = memberDao.get("SessionId");
					CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);

					synchronized (this.memberDao)
					{
						cmsuser.setAdditionalInfo(Environment.MEMBER_KEY, this.memberDao);
					}					
				}
				catch (Exception ex)
				{
					LOG.error("Store extenstion data for member failed! " + ex.toString());
				}
				
				LOG.info("Calculations start!");
				executeCalcProducer();			
			}
			else
			{
				LOG.info("Calculations already started!");
			}
		}
	}

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	/*
	 * get CalcProducer 
	 * @return : an instance of CalcProducer, now is BpAcCalcProducer 
	 */
	private void executeCalcProducer(){
		

		BPCalcProducer producer = null;
		//create new CalcMapper object
		CalcMapper calcMapper = new CalcMapper();
		
		String calcProducerName = StringUtil.EMPTY_STRING;

		List <CalcMapper.CalcInfo> lstProducer=calcMapper.getProducers(memberDao.getMemberData(Environment.MEMBER_BGROUP), memberDao.getMemberData(Environment.MEMBER_SCHEME_RAW), memberDao.getMemberData(Environment.MEMBER_STATUSRAW));
		
		if (lstProducer !=null && lstProducer.size() >0){
			
			
			Iterator<CalcMapper.CalcInfo> it=lstProducer.iterator();
			
			CalcMapper.CalcInfo obj =null;
			while (it.hasNext()){
				obj=it.next();
				
				if (obj!=null){
					
					
										
					//do something to load instance of CalcProducer
					try{
						calcProducerName=obj.getCalcProducer();
						Class producerClass =  BPCalcProducer.class.getClassLoader().loadClass(calcProducerName);
						
//						set calculator type to member dao
						MemberDao memberTemp=(MemberDao)memberDao.clone();
						
						
						
							memberTemp.set(Environment.MEMBER_CALCTYPE, obj.getPl());
							
							//set Agcode to member dao
							memberTemp.set(Environment.MEMBER_AGCODE, obj.getAgcode());
							//load class
							
							
							//create new instance
							
							producer = (BPCalcProducer)producerClass.newInstance();
							memberTemp.setCmsUUID(memberDao.getCmsUUID());
							memberTemp.setCurrentUser(memberDao.getCurrentUser());
							LOG.info("runCalc from CalcProducerFactory: " + String.valueOf(memberDao.get("CalcType")));
							producer.runCalc(memberTemp);
							

																		 
						//return producer;
						
						
					}catch(Exception ie){
						
						
						LOG.error("com.bp.pensionline.factory.CalcProducerFactory.getCalcProducer", ie);
						
					}
					
				}
			}
		}
		
	}

	/**
	 * @param memberDao the memberDao to set
	 */
	public void setMemberDao(MemberDao memberDao)
	{
		this.memberDao = memberDao;
	}

}
