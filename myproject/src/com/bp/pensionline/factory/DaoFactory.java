package com.bp.pensionline.factory;

import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.exception.MemberNotFoundException;

public interface DaoFactory {
	
	public MemberDao getMemberDao(String bGroup, String refNo, String description);

	public RecordDao getRecordDao(String tmpBgroup, String tmpRefNo) throws MemberNotFoundException;
	
	
}
