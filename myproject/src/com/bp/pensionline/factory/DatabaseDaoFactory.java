package com.bp.pensionline.factory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.dao.database.DatabaseMemberDao;
import com.bp.pensionline.dao.database.DatabaseRecordDao;
import com.bp.pensionline.exception.MemberNotFoundException;

public class DatabaseDaoFactory implements DaoFactory {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	

	public MemberDao getMemberDao(String bGroup, String refNo,
			String description) {
		if (description == null || description.equals("")) {// if description is
															// null or blank
			LOG.info("DatabaseDaoFactory.getMemberDao: " + bGroup + " - " + refNo);
			return new DatabaseMemberDao(bGroup, refNo);// return a DatabaseMemberDao
													// with 2 parameters
		} 
		else 
		{
			LOG.info("DatabaseDaoFactory.getMemberDao: " + description);
			return new DatabaseMemberDao(description);// return a DatabaseMemberDao
													// with 1 parameters
		}
	}

	public MemberDao getTestMember(String description) {

		return new DatabaseMemberDao(description);
		
	}

	public RecordDao getRecordDao(String tmpBgroup, String tmpRefNo) throws MemberNotFoundException {
		//LOG.info("DatabaseDaoFactory.getMemberDao: " + tmpBgroup + " - " + tmpRefNo);
		return new DatabaseRecordDao(tmpBgroup, tmpRefNo);
	}

}
