package com.bp.pensionline.factory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.util.CheckConfigurationKey;

public class DataAccess
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	public static DaoFactory getDaoFactory()
	{

		boolean isTestHarness = false;
		isTestHarness = CheckConfigurationKey.getBooleanValue("testHarness.enable");

		if (isTestHarness)
		{// if tesHarness is enabled
			LOG.info("=====================>********************<=====================");
			LOG.info("||                                                             ||");
			LOG.info("||                      TestHarness = TRUE                     ||");
			LOG.info("||                                                             ||");
			LOG.info("=====================>********************<=====================");
			return new XmlDaoFactory(); // return a XmlDaoFactory instance

		}
		else
		{
			LOG.info("=====================>********************<=====================");
			LOG.info("||                                                             ||");
			LOG.info("||                      TestHarness = FALSE                    ||");
			LOG.info("||                                                             ||");
			LOG.info("=====================>********************<=====================");

			return new DatabaseDaoFactory();// return a DatabaseDaoFactory
											// instance
		}

	}
}
