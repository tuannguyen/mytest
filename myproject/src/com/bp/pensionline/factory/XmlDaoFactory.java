package com.bp.pensionline.factory;

import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.dao.xml.XmlMemberDao;
import com.bp.pensionline.dao.xml.XmlRecordDao;
import com.bp.pensionline.exception.MemberNotFoundException;

public class XmlDaoFactory implements DaoFactory {


	public MemberDao getMemberDao(String bGroup, String refNo, String description) {
		
		if(description == null || description.equals("")){// if description is null or blank
			
			return new XmlMemberDao(bGroup,refNo);//return a XmlMemberDao with 2 parameters
		}
		else{
			
			return new XmlMemberDao(description);//return a XmlMemberDao with 1 parameters
		}		
	}

	public RecordDao getRecordDao(String tmpBgroup,String tmpRefNo) throws MemberNotFoundException {		
		
		return new XmlRecordDao( tmpBgroup, tmpRefNo);
	}

}
