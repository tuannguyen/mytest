package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;

import javax.servlet.http.HttpServlet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;


/**
 * @author SonNT
 * @version 1.1
 * @since 2007/06/04 
 * This class is used to validate if request param is xml well-fromed or not
 */
public abstract class Handler extends HttpServlet{
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public boolean validate(String xml){		
		
		boolean isValidated = false;

		try {
			//try to build xml document 
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			Document document = builder.parse(bais);
			
			bais.close();
			
			//if done return true
			if(document != null)
			isValidated = true;

		} catch (Exception ex) {
			//if fail return false
			LOG.info(ex.getMessage() + ex.getCause());
		}		
		return isValidated;
	}
}