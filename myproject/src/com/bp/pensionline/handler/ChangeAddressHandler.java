package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.ChangeAddressConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.ClearMemberCache;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/8
 * Servlet that handlers changing address
 *
 */
public class ChangeAddressHandler extends Handler {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap requestMap = new HashMap();

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(Constant.CONTENT_TYPE);

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String requestXml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;
		

		String bGroup = null;
		String refNo = null;	
		
		/*
		 * Try to get BGROUP and REFNO from currentUser
		 */
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
		}	

		if (validate(requestXml) && bGroup !=null && refNo != null) {
		
			parseRequestXml(requestXml);//parse XML Request				
			boolean isUpdated = false;
			
			requestMap.put("bGroup", bGroup);
			requestMap.put("refNo", refNo);
			requestMap.put("Form", requestXml);
			requestMap.put("userName", currentUser.getName());
			
			String postcode = (String)requestMap.get("Postcode");
			if(com.bp.pensionline.util.ValidPostCode.validPostcode(postcode)){				
				/*
				 * If Postcode is valid then Call Consumer
				 */
				ChangeAddressConsumer consumer = new ChangeAddressConsumer(requestMap);
				isUpdated = consumer.processData();		
									
				if(isUpdated){
					xmlResponse = responseSuccess();
					ClearMemberCache.clearCache(currentUser);
				}
				else{
					xmlResponse = responseError("We were unable to process your request at this time");
				}	
			}
			else{
				xmlResponse = responseError("Postcode is not valid");
			}
					
		}
		out.print(xmlResponse);
		out.close();
		requestMap.clear();
	}
	
	/**
	 * Build respone with successful message 
	 * @return
	 */
	private String responseSuccess(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append("Thank you your details have been updated").append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * Build respone with error message 
	 * @param mess
	 * @return
	 */
	private String responseError(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append(mess).append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * parse Xml request sent from AJAX to get values
	 * @param xml
	 */
	private void parseRequestXml(String xml) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
		
					if (node.getNodeName().equals("Home")) {
						requestMap.put("Home",node.getTextContent());						
					}
					if (node.getNodeName().equals("Street")) {
						requestMap.put("Street",node.getTextContent());
					}
					if (node.getNodeName().equals("Town")) {
						requestMap.put("Town",node.getTextContent());
					}
					if (node.getNodeName().equals("County")) {
						requestMap.put("County",node.getTextContent());
					}
					if (node.getNodeName().equals("Postcode")) {
						requestMap.put("Postcode",node.getTextContent());
					}
					if (node.getNodeName().equals("Country")) {
						requestMap.put("Country",node.getTextContent());
					}
					if (node.getNodeName().equals("District")) {
						requestMap.put("District",node.getTextContent());
					}
				}
			}
			bais.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
