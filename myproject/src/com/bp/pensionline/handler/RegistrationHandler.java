package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.jsp.CmsJspLoginBean;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.ImailConsumer;
import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.test.Constant;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/6/13
 * Servlet that handlers registration
 */

public class RegistrationHandler extends Handler {

	public static final Log LOG = CmsLog.getLog(CmsJspLoginBean.class.getName());
	private HashMap valueMap = new HashMap();

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String requestXml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;

		if (validate(requestXml)) {
		
			parseRequestXml(requestXml);//parse XML Request				
			boolean isUpdated = false;
			
			//get values from ajax
			String name = (String)valueMap.get("Name");
			String ni = (String)valueMap.get("Ni");
			String dob = (String)valueMap.get("Dob");
			String tel = (String)valueMap.get("Telephone");
			String email = (String)valueMap.get("Email");
			String serCode = (String)valueMap.get("SecurityCode");		
			String refNo = (String)valueMap.get("Refno");
			String payRoll = (String)valueMap.get("Payroll");

			
			if(refNo == null || refNo.length() == 0){
				//if refno is null then set it to UNKNOWN
				refNo = "UNKNOWN";
			}
			else{
			}
			
			//get security code from captcha servlet
			HttpSession session = request.getSession();
			String validSC = (String)session.getAttribute(nl.captcha.servlet.Constants.SIMPLE_CAPCHA_SESSION_KEY) ;

			if(validSC == null || validSC.length() == 0){
				xmlResponse = responseError("Invalid Session - Please refresh the page");
				
			}
			else{			
			
				if(serCode.compareTo(validSC) != 0){
					xmlResponse = responseError(" Wrong SecurityCode\n You can hit F5 to get new one");
				}
				else{
					//if valid security code - send Imail 
					StringBuffer buf = new StringBuffer();
					buf.append("<Register>\n");
					buf.append("\t<Name>").append(name).append("</Name>\n");
					buf.append("\t<Ni>").append(ni).append("</Ni>\n");
					buf.append("\t<Dob>").append(dob).append("</Dob>\n");
					buf.append("\t<Telephone>").append(tel).append("</Telephone>\n");
					buf.append("\t<Email>").append(email).append("</Email>\n");
					buf.append("\t<SecurityCode>").append(serCode).append("</SecurityCode>\n");
					buf.append("\t<Refno>").append(refNo).append("</Refno>\n");
					buf.append("\t<Payroll>").append(payRoll).append("</Payroll>\n");
					buf.append("</Register>");

					StringBuffer bufMessage = new StringBuffer();
					bufMessage.append("Name: ").append(name);
					bufMessage.append("\nNi: ").append(ni);
					bufMessage.append("\nDob: ").append(dob);
					bufMessage.append("\nTelephone: ").append(tel);
					bufMessage.append("\nEmail: ").append(email);
					bufMessage.append("\nSecurityCode: ").append(serCode);
					bufMessage.append("\nRefno: ").append(refNo);
					bufMessage.append("\nPayroll: ").append(payRoll);

					//CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
					// Huy modified here
					ImailSQLHandler imailSQLHandler = new ImailSQLHandler();
						
					int maxIMailNo = imailSQLHandler.getMaxIMailNo();
					int newIMailNo = maxIMailNo + 1;
					
				
					valueMap.put(Environment.IMAIL_BGROUP, "UNK");
					valueMap.put(Environment.IMAIL_REFNO, refNo);
					valueMap.put(Environment.IMAIL_TEMP, String.valueOf(newIMailNo));
					valueMap.put(Environment.IMAIL_NAME, name);
					valueMap.put(Environment.IMAIL_ADDRESS, email);
					
					valueMap.put(Environment.IMAIL_NAME, name);
					valueMap.put(Environment.IMAIL_NI, ni);
					valueMap.put(Environment.IMAIL_EMAILADRESS, email);
					valueMap.put(Environment.IMAIL_SUBJECT, "Registration Request");
					valueMap.put(Environment.IMAIL_QUERY, payRoll);
					valueMap.put(Environment.IMAIL_MESSAGE, bufMessage.toString());
					valueMap.put(Environment.IMAIL_CREATIONDATE, new java.sql.Date(System.currentTimeMillis()));
					valueMap.put(Environment.IMAIL_CASEWORKID, String.valueOf(newIMailNo));
					valueMap.put(Environment.IMAIL_FORMDATA, buf.toString());
					
					//CaseWorkSQLHandler.debugValues(valueMap);
					//call Imail consumer
					//System.out.println("call Imail consumer");
					ImailConsumer imailConsumer = new ImailConsumer(valueMap);
					try {
						//System.out.println("imailConsumer.processData");
						imailConsumer.processData();
						isUpdated = true;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//System.out.println("Error in creating IMail: " + e.toString());
					}	
					
					//respone success or fail to send Imail
					if(isUpdated){
						xmlResponse = responseSuccess();						
					}
					else{
						xmlResponse = responseError(" Service is not availble now\n Please try again later");

					}
				}			
			}

		}
		out.print(xmlResponse);
		out.close();
		valueMap.clear();
	}
	
	/**
	 * Build respone with successful message 
	 * @return
	 */
	private String responseSuccess(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append("Thank you your details have been updated").append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * Build respone with error message 
	 * @param mess
	 * @return
	 */
	private String responseError(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append(mess).append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * parse Xml request sent from AJAX to get values
	 * @param xml
	 */
	private void parseRequestXml(String xml) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
			document = builder.parse(bais);

			Node root = document.getElementsByTagName("Register").item(0);
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE && node.hasChildNodes()) {
					Node cNode = node.getFirstChild();
					if(cNode.getNodeType() == Node.TEXT_NODE){
						valueMap.put(node.getNodeName(),cNode.getTextContent());
					}	
				}
			}
			bais.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
