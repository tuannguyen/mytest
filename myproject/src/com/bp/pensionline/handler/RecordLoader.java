package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.exception.MemberNotFoundException;
import com.bp.pensionline.factory.DaoFactory;
import com.bp.pensionline.factory.DataAccess;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * This class use to load records from a member base on bgroup and crefno ( not refno)
 *
 */
public class RecordLoader {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	private List<RecordDao> recList = new LinkedList<RecordDao>();
	
	static final Comparator <RecordDao> ORDER_BYDATE=new Comparator<RecordDao>(){
		
		public int compare(RecordDao temp1,RecordDao temp2){
			Date date1=DateUtil.getNormalDate(temp1.getPosStart());
			Date date2=DateUtil.getNormalDate(temp2.getPosStart());
			int dateCmp =date1.compareTo(date2);// dateCmp<0 : date1 < date2; dateCmp=0: date1=date2; dateCmp>0 : date1>date2
			return -dateCmp;// normal this order by ascending, so we use (-) it to order by descending
			
		}
	};
	
	private DaoFactory daoFac = DataAccess.getDaoFactory();

	//Record_bGroup_cRefNo.xml virtual path
	final String filePath = "/system/modules/com.bp.pensionline.test_members/Record_";
	
	//each cRefNo has multiple refNo
	String cRefNo = null;
	String bGroup = null;

	/**Load records of a user by user's description
	 * @param username
	 * @param recordDescription
	 * @param request
	 */
	public RecordLoader(String recordDescription,
			HttpServletRequest request) {

		LOG.info(this.getClass().toString() + " RecordLoader constructor begin-->: " + recordDescription);
		//get testharness 
		boolean isTestHarness = true;		
		isTestHarness = CheckConfigurationKey.getBooleanValue("testHarness.enable");
				
		if (recordDescription != null && recordDescription.trim() != "") {

			//create string tokenizer from description of user
			StringTokenizer st = new StringTokenizer(recordDescription.trim(), ",");

			//loop through each tokens
			//1 token has the format: BGROUP-CREFNO
			while (st.hasMoreTokens()) {
				
				String bgroup_crefno = st.nextToken();
				LOG.info(this.getClass().toString() + " RecordLoader constructor: " + bgroup_crefno +  "; " + isTestHarness);				
				StringTokenizer stSplit = new StringTokenizer(bgroup_crefno,"-");

				//get the bGroup
				bGroup = stSplit.nextToken();
				
				//get the cRefNo
				cRefNo = stSplit.nextToken();
								
				//load the records
				if(isTestHarness){					
					
					loadRecordForTestHarness(bGroup, cRefNo);
					
				}else{
					LOG.info(this.getClass().toString() + " RecordLoader constructor: startLoad record: " + bgroup_crefno);					
					loadRecordForDatabase(bGroup, cRefNo);
					
				}

				
			}
		}


	}

	/**
	 * @param username
	 * @param request
	 * @return List records of user store in Http Session with key=username
	 */
	public List getRecords(String username, HttpServletRequest request) {

		//Object obj = request.getSession().getAttribute(username);
		
		Object obj = null;
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		if(currentUser != null){
		
			
			
			obj = currentUser.getAdditionalInfo(username);
			
		}

		if (obj instanceof List) {

			List list = ((List) obj);

			return list;

		}else{
			
			
		}
		
		return null;
	}
	
	public List getRecords(){
		Collections.sort(this.recList,ORDER_BYDATE);// sort list by record.getPostStart() (date) descending
		return this.recList;		
	}
	/**
	 * load the RecordDao and store it to list
	 * @param bGroup
	 * @param refNo
	 */
	
		
	void loadARecord(String bGroup, String refNo){
		try {
			
			//load record associated with bgroup and refno (not crefno)
			RecordDao record = daoFac.getRecordDao(bGroup, refNo);

			if (record != null) {				
				recList.add(record);
				LOG.info("record added to record list for member: " + bGroup + "-" + refNo); 
			}
			else
			{
				LOG.info("Record is null!");
			}
		} catch (MemberNotFoundException mnfe) {
			LOG.error("Error in loading POS record for member: " + bGroup + "-" + refNo);
			LOG.error("Exception in loading POS record for member: " + mnfe.toString());
			
		}
	}
	
	void loadRecordForTestHarness(String bGroup, String cRefNo){

		String refNo = null;
		
		//build file path
		StringBuffer tmpPath = new StringBuffer(filePath);
		tmpPath
			.append(bGroup)
			.append("_")
			.append(cRefNo)
			.append(".xml");
		
		//read Record_bGroup_cRefNo.xml file to get refNo
		try{
			//create new reader
			XmlReader reader = new XmlReader();
			
			//get file in ByteArrayInputStream
			ByteArrayInputStream bais = new ByteArrayInputStream(reader.readFile(tmpPath.toString()));

			//build the document
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			
			Document doc = docBuilder.parse(bais);
			
			doc.getDocumentElement().normalize();
			
			//get Refno nodes
			NodeList refNoList = doc.getElementsByTagName("Refno");
			
			int length = refNoList.getLength();
			
			for(int i=0; i< length; i++){
				
				Node refNoNode = refNoList.item(i);
				
				if(refNoNode.hasChildNodes() ){

					refNo = String.valueOf(refNoNode.getFirstChild().getNodeValue());
					
					//call methoad to load the RecordDao
					loadARecord(bGroup, refNo);

				}else 
				{
					refNo = null;
				}
			}		
			
			bais.close();
			
			
		}catch(Exception ex){
			LOG.error(ex.getMessage() +ex.getCause());
			
		}
	}
	
	void loadRecordForDatabase(String bGroup, String cRefNo){
		
		String refNo = null;
		StringBuffer sql = new StringBuffer();
		
		sql.append("Select REFNO From BASIC Where CREFNO='")
			.append(cRefNo)
			.append("' And BGROUP='")
			.append(bGroup)
			.append("'")
			//.append("And REFNO NOT IN (Select REFNO FROM BASIC WHERE BASIC.BD19A ='NL')");  // exclude member with NL status
			.append(" And REFNO NOT IN (Select REFNO FROM BASIC WHERE BASIC.BD19A ='NL'")
			.append(" And BASIC.BGROUP = '")
			.append(bGroup)
			.append("')");
		
		
			//.append("' order by BASIC.BD18D DESC");
		Connection conn = null;
		
		try{
			
			conn = getConnection();
			
			Statement stmt = conn.createStatement();
			
			ResultSet rs = null;
			
			rs = stmt.executeQuery(sql.toString());
			System.out.println(sql.toString());
			
			if(! rs.next()){
				
				throw new MemberNotFoundException("Can not found member with bGroup=" + bGroup + " cRefNo=" + cRefNo);
									
			}
			
			do{
				
				refNo = String.valueOf(rs.getObject("Refno"));
				LOG.info(this.getClass().toString() + ": Add new POS record for the member: " + refNo);
				//call methoad to load the RecordDao
				loadARecord(bGroup, refNo);
								
			} while(rs.next());
			
			rs.close();
			stmt.close();
			DBConnector connector = DBConnector.getInstance();
			connector.close(conn);//conn.close();

			
		} catch(Exception ex){
			LOG.error(ex.getMessage() +ex.getCause());
			ex.printStackTrace();
		}
		
	}
	
	static Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {

			DBConnector dbConn = DBConnector.getInstance();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);

		} catch (Exception ex) {
			LOG.error(ex.getMessage() +ex.getCause());
		}
		
		return conn;
	}

}
