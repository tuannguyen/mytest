package com.bp.pensionline.handler;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsGroup;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.security.CmsOrgUnitManager;
import org.opencms.security.CmsOrganizationalUnit;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * 
 */
public class SwapUserHandler
{

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	/**
	 * Default contructor
	 */
	protected SwapUserHandler()
	{
	}

	private static SwapUserHandler instance = null;

	public static synchronized SwapUserHandler getInstance()
	{
		if (instance == null)
		{
			instance = new SwapUserHandler();
		}
		return instance;
	}

	/**
	 * Assing user to group
	 * 
	 * @param username
	 * @param group
	 */
	public void swapUser(String username, String group, CmsObject obj)
	{
		try
		{
			/* CmsObject obj = SystemAccount.getAdminCmsObject(); */
			if (obj != null)
			{
				obj.addUserToGroup(username, group);
			}

		}
		catch (Exception e)
		{

			LOG.error("com.bp.pensionline.handler.SwapUserHandler swapUser error ", e);
			// e.printStackTrace();
		}
	}

	/**
	 * Add user to group (has name) in groupName[]
	 * 
	 * @param username
	 * @param groupName
	 * @param obj
	 */
	public void swapUser(String username, String[] groupName, CmsObject obj)
	{
		try
		{
			/* CmsObject obj = SystemAccount.getAdminCmsObject(); */
			if (groupName != null && groupName.length > 0 && obj != null)
			{
				int groupLength = groupName.length;
				for (int i = 0; i < groupLength; i++)
				{
					if (groupName[i] != null && !groupName[i].trim().equals(""))
					{
						obj.addUserToGroup(username, groupName[i]);
					}
				}

			}
			
			//moveUsers(obj);
			//deleteWebUsers(obj);
			
//			System.out.println("There are "+cnt+" users being moved to Web unit");
//			LOG.info("There are "+cnt+" users being moved to Web unit");
		}
		catch (Exception e)
		{

			LOG.error("com.bp.pensionline.handler.SwapUserHandler swapUser error ", e);
			// e.printStackTrace();
		}
	}
	
	private void moveUsers(CmsObject obj) throws CmsException {
		LOG.info("moveUsers():BEGIN");
		//Temporary script for moving web users
		CmsOrgUnitManager unitManager = OpenCms.getOrgUnitManager();
		List ouList = unitManager.getOrganizationalUnits(obj,"/", true);
		CmsOrganizationalUnit webUnit = null;
		for (int idx=0; idx<ouList.size(); idx++) {
			if (((CmsOrganizationalUnit)ouList.get(idx)).hasFlagWebuser()) {
				webUnit = (CmsOrganizationalUnit)ouList.get(idx);
				break;
			}
		}
		List userList = unitManager.getUsers(obj, "/", false);
		List webUserList = unitManager.getUsers(obj, "/Webusers/", true);
		//List userList = unitManager.getUsers(obj, "PensionLine_WebUsers", false);
		
		LOG.info("User list: "+userList==null?0:userList.size());
		LOG.info(webUnit.getName()+"-"+webUserList==null?0:webUserList.size());
		
		int cnt = 0;
		boolean isExist = false;
		String ouName = webUnit!=null?webUnit.getName():"Webusers/";
		if (userList != null && userList.size() > 0) {
			for (int i=0; i<=userList.size(); i++) {
				if (cnt==1000) break;
				try {
					CmsUser user = (CmsUser)userList.get(i);
					isExist = checkExist(webUserList, ouName+user.getName());
					if (!isExist && 
						("test@mail.com".equals(user.getEmail().trim()) ||
						"test@email.com".equals(user.getEmail().trim()) ||
						"test@abc.com".equals(user.getEmail().trim()))) {
						//obj.deleteUser(user.getName());
						
						List groupList = obj.getGroupsOfUser(user.getName(), true);
						if (groupList!=null && groupList.size()>0) {
							for (int j=0; j<groupList.size(); j++) {
								CmsGroup group = (CmsGroup)groupList.get(j);
								obj.removeUserFromGroup(user.getName(), group.getName());
							}
						}						
						//obj.
						user.setFlags(32768);
						obj.writeUser(user);
						System.out.println("Move "+user.getName()+" to "+ouName);
						LOG.info("Move "+user.getName()+" to "+ouName);
						unitManager.setUsersOrganizationalUnit(obj, ouName, user.getName());
						obj.addUserToGroup("Webusers/"+user.getName(), "Members");	
						cnt++;
					} else if (isExist) {
						LOG.info("Delete user "+user.getName());
						obj.deleteUser(user.getName());
					}
				} catch(Exception e) {
					LOG.error("Continue "+e.toString());
					e.printStackTrace();
					break;
				}
			}
		}
		
		LOG.info("MOVED: "+cnt);
		LOG.info("moveUsers():END");
	}
	
	private void deleteWebUsers(CmsObject obj) throws CmsException {
		LOG.info("deleteUsers():BEGIN");
		//Temporary script for moving web users
		CmsOrgUnitManager unitManager = OpenCms.getOrgUnitManager();
		List ouList = unitManager.getOrganizationalUnits(obj,"/", true);
		CmsOrganizationalUnit webUnit = null;
		for (int idx=0; idx<ouList.size(); idx++) {
			if (((CmsOrganizationalUnit)ouList.get(idx)).hasFlagWebuser()) {
				webUnit = (CmsOrganizationalUnit)ouList.get(idx);
				break;
			}
		}
		
		int cnt = 0;
		String ouName = webUnit!=null?webUnit.getName():"Webusers/";
		List userList = unitManager.getUsers(obj, "/"+ouName, true);
		LOG.info("User list: "+userList==null?0:userList.size());
		
		if (userList != null && userList.size() > 0) {
			for (int i=0; i<=userList.size(); i++) {
				if (cnt==1000) break;
				try {
					CmsUser user = (CmsUser)userList.get(i);
					LOG.info("Delete user "+user.getName());
					obj.deleteUser(user.getName());
					cnt++;
				} catch(Exception e) {
					LOG.error("Continue "+e.toString());
					e.printStackTrace();
					break;
				}
			}
		}
		
		LOG.info("DELETED: "+cnt);
		LOG.info("deleteUsers():END");		
	}
	
	private boolean checkExist(List uList, String uName){
		if (uList==null || uList.size()==0) return false;
		LOG.info("Check exist: "+uName +"-"+((CmsUser)uList.get(0)).getName());
		for (int i=0; i<uList.size(); i++) {
			CmsUser user = (CmsUser)uList.get(i);
			if (uName.equals(user.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Remove user from group
	 * 
	 * @param username
	 * @param group
	 */
	public void unSwapUser(String username, String group, CmsObject obj)
	{
		try
		{
			/* CmsObject obj = SystemAccount.getAdminCmsObject(); */
			if (obj != null && group != null && !group.trim().equals("") && obj.userInGroup(username, group))
			{
				// Modify by Huy to exclude Publishing groups
				if (!group.equals("Administrators") && !group.equals("Projectmanagers") &&
						!group.equals(Environment.PUBLISHING_EDITOR_GROUP) &&
						!group.equals(Environment.PUBLISHING_REVIEWER_GROUP) &&
						!group.equals(Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) &&
						!group.equals(Environment.PUBLISHING_AUTHORISER_GROUP) &&
						!group.equals(Environment.PUBLISHING_DEPLOYER_GROUP)  &&
						!group.equals(Environment.PL_REPORT_RUNNER) && 
						!group.equals(Environment.PL_REPORT_EDITOR) &&
						!group.equals(Environment.GROUP_BP1_USER) &&
						!group.equals(Environment.GROUP_FORCE_SELF_SERVICE_USER))				
					obj.removeUserFromGroup(username, group);
			}

		}
		catch (Exception e)
		{

			LOG.error("com.bp.pensionline.handler.SwapUserHandler unSwapUser error ", e);
			// e.printStackTrace();
		}
	}

	/**
	 * Remove user from any group accept group (has name) in acceptGroup[]
	 * 
	 * @param username
	 * @param groupName
	 * @param obj
	 */
	public void unSwapUser(String username, String[] acceptGroup, CmsObject obj)
	{
		try
		{
			List groups = obj.getGroupsOfUser(username, true);
			Iterator it = groups.iterator();
			CmsGroup aGroup = null;
			
			// Remove user from any groups
			while (it.hasNext())
			{
				aGroup = (CmsGroup) it.next();
				if (obj.userInGroup(username, aGroup.getName()))
				{
					// Modify by Huy to exclude Publishing groups
					if (aGroup.getName() != null && 
							!aGroup.getName().equals("Administrators") && 
							!aGroup.getName().equals("Projectmanagers") &&
							!aGroup.getName().equals(Environment.PUBLISHING_EDITOR_GROUP) &&
							!aGroup.getName().equals(Environment.PUBLISHING_REVIEWER_GROUP) &&
							!aGroup.getName().equals(Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) &&
							!aGroup.getName().equals(Environment.PUBLISHING_AUTHORISER_GROUP) &&
							!aGroup.getName().equals(Environment.PUBLISHING_DEPLOYER_GROUP) &&
							!aGroup.getName().equals(Environment.PL_REPORT_RUNNER) && 
							!aGroup.getName().equals(Environment.PL_REPORT_EDITOR) &&
							!aGroup.getName().equals(Environment.GROUP_BP1_USER) &&
							!aGroup.getName().equals(Environment.GROUP_FORCE_SELF_SERVICE_USER))
						obj.removeUserFromGroup(username, aGroup.getName());
				}

			}
			// Add user to addtional groups
			if (acceptGroup != null && acceptGroup.length > 0 && obj != null)
			{
				for (int i = 0; i < acceptGroup.length; i++)
				{
					System.out.println("ACCEPT G: "+acceptGroup[i]);
					if (acceptGroup[i] != null && !acceptGroup[i].trim().equals(""))
					{
						obj.addUserToGroup(username, acceptGroup[i]);
					}					
				}
			}

		}
		catch (Exception e)
		{

			LOG.error("com.bp.pensionline.handler.SwapUserHandler unSwapUser error ", e);
		}

	}
}
