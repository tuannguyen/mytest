package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

public class GetHTMLTableHandler extends Handler
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private static final long serialVersionUID = 1L;
	
	// Harded code customized table header
	private static final String TABLE_CONTRIBUTION_HIS = "ContributionHistory";
	private static final String TABLE_AVC_INFO = "AvcInformation";
	private static final String TABLE_AVER_INFO = "AverInformation";
	private static final String TABLE_TVIN_INFO = "TvinInformation";
	
	private static final String TABLE_CONTRIBUTION_HIS_HEADER = "Historic contributions";
	
	private String type;

	private String tagName;
	private String updateField;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		//get request
		String xml = request.getParameter(Constant.PARAM);

		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		//System.out.println("XML = " + xml);

		String xmlResponse = null;

		if (parseRequestXml(xml) || currentUser == null)
		{
			// Get the memberDao of current user
			MemberDao memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);

			if (memberDao != null)
			{
				if (type.equals(Constant.TYPE_HTMLTABLE))
				{
					String memberValueXML = buildMemberValueXML(tagName, memberDao.get(tagName));
					//LOG.info("memberValueXML: " + memberValueXML);
					String htmlTable = buildHtmlTableFromXml(memberValueXML);
					//LOG.info("htmlTable: " + htmlTable);
					xmlResponse = this.getXmlHtmlTableData(tagName, updateField, htmlTable);
				}
			}
			else
			{
				xmlResponse = this.getXmlTableDataError(tagName, updateField);
			}
		}
		else
		{
			System.out.println("Parsing XML error!");
			xmlResponse = this.getXmlTableDataError(tagName, updateField);

		}

		out.print(xmlResponse);
		out.close();

	}
	
	public String getXmlHtmlTableData(String tagName, String updateField, String htmlTable)
	{

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.TYPE).append(">")
				.append(Constant.TYPE_HTMLTABLE).append("</").append(Constant.TYPE)
				.append(">\n");		
		buffer.append("         <").append(Constant.VALUE).append(">").append("<![CDATA[")
				.append(htmlTable).append("]]>").append("</").append(Constant.VALUE)
				.append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();
	}	
	
	private String getXmlTableDataError(String tagName, String updateField)
	{

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE)
				.append(">" + tagName + "</").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();

	}	

	private boolean parseRequestXml(String xml)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{
					if (node.getNodeName().equals(Constant.TYPE))
					{
						type = node.getTextContent();
					}
					if (node.getNodeName().equals(Constant.REQUESTED_TAG))
					{	
						tagName = node.getTextContent();
					}					
					if (node.getNodeName().equals(Constant.UPDATE_FIELD))
					{
						updateField = node.getTextContent();
					}
				}
			}
			bais.close();

			return true;

		}
		catch (Exception ex)
		{
			tagName = "";
			updateField = "";
		}

		return false;

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	public String getServletInfo()
	{
		return "Short description";
	}
	
	public static String buildMemberValueXML(String memberProperty, String memberValue)
	{
		StringBuffer memberValueXML = new StringBuffer();
		memberValueXML.append("<").append(memberProperty).append(">");
		memberValueXML.append(memberValue);
		memberValueXML.append("</").append(memberProperty).append(">");
		
		return memberValueXML.toString();
	}
	
	public String buildHtmlTableFromXml(String xmlString)
	{
		int numRows = 0, numCols = 0;
		ArrayList<String> headers = new ArrayList<String>();
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		
		StringBuffer table = new StringBuffer("");
		
		if (xmlString != null && !xmlString.trim().equals(""))
		{
			//buildDocument(xmlString);
			byte[] arr = xmlString.getBytes();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			try
			{
				builder = factory.newDocumentBuilder();

				ByteArrayInputStream is = new ByteArrayInputStream(arr);

				document = builder.parse(is);
			}
			catch (Exception e)
			{
				return "";
			}

			Element root = document.getDocumentElement();
			NodeList rowNodes = root.getChildNodes();
			String rowName = "";
			// Get rows
			int numRowsTmp = rowNodes.getLength();		
			for (int i = 0; i < numRowsTmp; i++)
			{
				Node rowNode = rowNodes.item(i);
				if (rowNode.getNodeType() == Node.ELEMENT_NODE)
				{
					numRows += 1;
					if (numRows == 1)
					{
						rowName = rowNode.getNodeName();
					}
					ArrayList<String> rowValues = new ArrayList<String>();
					
					if (rowName.equals(rowNode.getNodeName()))
					{
						// Get headers
						NodeList colNodes = rowNode.getChildNodes();
						int numColsTmp = colNodes.getLength();
						for (int j = 0; j < numColsTmp; j++)
						{
							Node colNode = colNodes.item(j);
							if (colNode != null && colNode.getNodeType() == Node.ELEMENT_NODE)
							{
								if (numRows == 1)	// get the headers
								{
									numCols += 1;
									headers.add(colNode.getNodeName());
								}
								rowValues.add(colNode.getTextContent());
							}
						}
					}
					else	// if a special row, add consisder this as special row with 2 columms
					{
						rowValues.add(rowNode.getNodeName());
						rowValues.add(rowNode.getTextContent());
					}
					
					values.add(rowValues);
				}

			}			
			
			if (numRows == 0 || numCols == 0)
			{
				return "";
			}
			
			// Build html output table. This consider each type of table based on table name
			table.append("<table class='datatable'><tbody>\n");
			
			// create header			
			if (tagName.equals(TABLE_CONTRIBUTION_HIS))
			{
				table.append("<tr><th colspan='" + numCols + "' class='label'>").append(TABLE_CONTRIBUTION_HIS_HEADER);
				table.append("</th></tr>\n");
			
				// create the first data row that contains header values
				table.append("<tr>\n");
				for (int i = 0; i < numCols; i++) 	// this is considered as the first data row
				{
					table.append("\t<td align='center'>").append(headers.get(i)).append("</td>\n");
				}
				table.append("</tr>\n");
			}
			else if (tagName.equals(TABLE_AVC_INFO) || tagName.equals(TABLE_AVER_INFO) || tagName.equals(TABLE_TVIN_INFO))
			{
				table.append("<tr>\n");
				for (int i = 0; i < numCols; i++) 	// this is considered as the first data row
				{
					table.append("\t<th class='label'>").append(headers.get(i)).append("</th>\n");
				}
				table.append("</tr>\n");				
			}
			
			// create rows
			for (int i = 0; i < numRows; i++)
			{
				ArrayList<String> rowValues = values.get(i);
				table.append("<tr>");
				int colspan = numCols - rowValues.size();
				for (int j = 0; j < rowValues.size(); j++)
				{		
					String tdFormat = "<td>";
					String data = rowValues.get(j);
					if (tagName.equals(TABLE_CONTRIBUTION_HIS))
					{
						tdFormat = "<td align='center'>";
					}
					else if (tagName.equals(TABLE_AVC_INFO) || tagName.equals(TABLE_AVER_INFO) || tagName.equals(TABLE_TVIN_INFO))
					{
						if (i == numRows - 1) // bold up the last row
						{
							if (rowValues.get(j).equalsIgnoreCase("Total"))
							{
								tdFormat = "<td align='left' colspan=" + (colspan+1) + ">";								
							}
							else
							{
								tdFormat = "<td align='right' class='value number'>";	
							}
							data = "<strong>" + data + "</strong>";
						}
						else
						{
							if (headers.get(j).equalsIgnoreCase("Provider"))
							{
								tdFormat = "<td align='left' class='label'>";
							}							
							else if (headers.get(j).equalsIgnoreCase("Date"))
							{
								tdFormat = "<td align='center' class='label'>";
							}
							else if (headers.get(j).equalsIgnoreCase("Value"))
							{
								tdFormat = "<td align='right' class='value number'>";
							}							
						}
					}
					
					table.append(tdFormat).append(data).append("</td>");
				}
				table.append("</tr>\n"); 
			}
			
			table.append("</tbody></table>");
		}
		
		return table.toString();
	}
}
