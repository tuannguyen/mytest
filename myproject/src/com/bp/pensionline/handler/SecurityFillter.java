package com.bp.pensionline.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opencms.file.CmsUser;

import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 28/05/2007
 * This class use to against calling direct javascript without login
 *
 */
public class SecurityFillter implements Filter {

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		
		// TODO Auto-generated method stub
		HttpServletRequest request=(HttpServletRequest)req;
		HttpServletResponse response=(HttpServletResponse) res;
		try {
			CmsUser cmsu=SystemAccount.getCurrentUser(request);
			/*System.out.println("\n\n SecurityFillter cmsu = "+cmsu);*/
			if (cmsu!=null){// user has been logined so do nothing
				chain.doFilter(req, res);
				return;
			}else {// user is guest so he cann't access resource
				response.setContentType("text/html");
				PrintWriter out=response.getWriter();
				out.print("<html>");
				out.print("<body>");
				out.print("Access dine");
				out.print("</body>");
				out.print("</html>");
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	

}
