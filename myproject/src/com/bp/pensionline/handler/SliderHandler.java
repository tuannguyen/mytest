/**
 * 
 */
package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.opencms.file.CmsUser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 22/05/2007
 * 
 */
public class SliderHandler extends HttpServlet {
	private static final String MaxCashSlider = "MaxCashSlider";

	private static final String MinAgeSlider = "MinAgeSlider";
	private static final String AjaxRequestXml ="AjaxRequestXml";

	/**
	 * 
	 */
	public SliderHandler() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			proccessRequest(request, response);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			proccessRequest(request, response);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void proccessRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xmlout=null;
		String xml = request.getParameter(Constant.PARAM);
		System.out.println("SilderHanlder xml value= " +xml);
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();

		ByteArrayInputStream byteArr = new ByteArrayInputStream(xml.getBytes());
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(byteArr);
		doc.getDocumentElement().normalize();
		NodeList listOfMembers = doc.getElementsByTagName(AjaxRequestXml);
		if (listOfMembers !=null && listOfMembers.getLength()>0){
			int _minAge=getMinAge(request, response);
			int _maxCash=getMaxCash(request, response);
			
			String []_tagName={MaxCashSlider,MinAgeSlider};
			String []_tagValue={String.valueOf(_maxCash),String.valueOf(_minAge)};
			xmlout=generateXmlValue(_tagName, _tagValue);
			/*for (int s = 0; s < listOfMembers.getLength(); s++) {
				Node firstNode = listOfMembers.item(s);
				NodeList ajaxList=firstNode.getChildNodes();
				if (ajaxList !=null && ajaxList.getLength()>0){
					for (int i=0;i<ajaxList.getLength();i++){
						Node ajaxNode=ajaxList.item(i);
						if (ajaxNode.getNodeType()==Node.ELEMENT_NODE){
							
							System.out.println("//n/n/n SliderHandler ajaxNode.getNodeName() = " +ajaxNode.getNodeName());
							if (ajaxNode.getNodeName().equalsIgnoreCase(MinAgeSlider)){
								System.out.println("/n/n=========Ajaxrequest MinAgeSlider/n/n");
								
								String tagName[]={MinAgeSlider};
								
								int minAge=getMinAge(request, response);
								
								String tagValue[]={String.valueOf(minAge)};
								
								xmlout=generateXmlValue(tagName, tagValue);
								
							}else if (ajaxNode.getNodeName().equalsIgnoreCase(MaxCashSlider)){
								System.out.println("/n/n=========Ajaxrequest MaxCashSlider/n/n");
								
								String tagName[]={MaxCashSlider};
								
								int minAge=getMaxCash(request, response);
								
								String tagValue[]={String.valueOf(minAge)};
								
								xmlout=generateXmlValue(tagName, tagValue);
								
							}else {
								System.out.println("\n\n\n Tagname not match !!! \n\n\n");
							}
								
							
						}
					}
				}
				
			}*/
		}else {
			String []_tagError={"Error"};
			String []_tagErrorValue={"True"};
			xmlout=generateXmlValue(_tagError, _tagErrorValue);
		}
		System.out.println("\n\n SilderHandler xmlout = " +xmlout +"\n\n");
		out.print(xmlout);
		out.flush();
		out.close();
	}

	private int getMinAge(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CmsUser cmsUser = SystemAccount.getCurrentUser(request);
		MemberDao dao = (MemberDao) cmsUser.getAdditionalInfo(Environment.MEMBER_KEY);
		int memberNpa = NumberUtil.getInt(String.valueOf(dao.get("Npa")));
		if (memberNpa > 10) {
			return memberNpa - 10;
		} else {
			return memberNpa;
		}
		//return 1;

		
	}

	private int getMaxCash(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CmsUser cmsUser = SystemAccount.getCurrentUser(request);
		MemberDao dao = (MemberDao) cmsUser.getAdditionalInfo(Environment.MEMBER_KEY);
		int memberNpa = NumberUtil.getInt(String.valueOf(dao.get("NpaMaximumCashLumpSum")));
		return memberNpa ;
		//return 1;

	}

	/**
	 * @param tagName
	 * @param tagValue
	 * @return String value as Xml. tagName[] and tagValue[] have the same size
	 *         (length)
	 */
	private String generateXmlValue(String tagName[], String[] tagValue) {
		StringBuffer buf = new StringBuffer();
		buf.append("<AjaxResponseXml>");
		int tagnameLength = tagName.length;
		int tagvalueLength = tagName.length;
		if (tagnameLength == tagvalueLength) {
			for (int i = 0; i < tagnameLength; i++) {
				buf.append("<").append(tagName[i]).append(">").append(
						tagValue[i]).append("</").append(tagName[i])
						.append(">");

			}

		} else {
			buf.append("<Error>True</Error>");
		}
		buf.append("</AjaxResponseXml>");
		return buf.toString();

	}

}
