package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


import com.bp.pensionline.consumer.CaseworkConsumer;

import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/15
 * Servlet that handlers Casework view
 */

public class CaseworkHandler extends Handler {
	
	public static final Log LOG = CmsLog.getLog(CaseworkHandler.class);
	private HashMap requestMap;
	private CmsUser currentUser;
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		
		requestMap = new HashMap();
		
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;
		
		currentUser = SystemAccount.getCurrentUser(request);
		
		if (validate(xml)) {

			try {				
				
				parseRequestXml(xml);
				
				//if AJAX call RequestResult - then try to get result which has been stored in currentUser to return
				String requestResult = (String)requestMap.get("RequestResult");
				
				if(requestResult != null && requestResult.compareTo("true") == 0){
					try{
						xmlResponse = (String)currentUser.getAdditionalInfo("result");
					}
					catch (Exception e) {
						LOG.info(e.getMessage() +e.getCause());
						xmlResponse = responseError("Error while returning result");
					}
					
				}
				else{
					/*
					 * Call Consumer
					 */
					CaseworkConsumer consumer = new CaseworkConsumer(requestMap);
					HashMap map = consumer.selectData();
					
					/*
					 * Get return values to display
					 */
					if(map != null && map.size() > 0){
						String member = (String)map.get("member");
						String submitted = (String)map.get("subject");					
						String subject = (String)map.get("submitted");
						String formData = (String)map.get("formData");
						String caseworkId = (String)requestMap.get("CaseWorkID");
						xmlResponse = responeCasework(member, caseworkId, submitted, subject, formData);
						
						//if found new result then delete old one if it already existes
						if(currentUser.getAdditionalInfo("result") != null){
							currentUser.deleteAdditionalInfo("result");
						}
						//store search result in currentUser to get back when AJAX calls
						currentUser.setAdditionalInfo("result", xmlResponse);
						
					}
					else{
						xmlResponse = responseError("Casework not found");
					}
				}
				
					
			} 
			catch (Exception ex) {
				LOG.info(ex.getMessage() + ex.getCause());
			}
		}
		
		out.print(xmlResponse);		
		out.close();
		requestMap.clear();
	}

	
	/**
	 * Generate xml respone for the Casework
	 * @param member
	 * @param caseworkId
	 * @param submitted
	 * @param subject
	 * @param formData
	 * @return
	 */
	private String responeCasework(String member, String caseworkId, String submitted, String subject, String formData){
				
		StringBuffer buffer = new StringBuffer();	
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");			
		buffer.append("		<Member>").append(member).append("</Member>\n");
		buffer.append("		<Submitted>").append(submitted).append("</Submitted>\n");
		buffer.append("		<Subject>").append(subject).append("</Subject>\n");
		buffer.append("		<CaseworkId>").append(caseworkId).append("</CaseworkId>\n");
		buffer.append("		<FormData>").append(formData).append("</FormData>\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");	
		
		return buffer.toString();		
	}	
	
	/**
	 * generate error response 
	 * @param mess
	 * @return
	 */
	private String responseError(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append(mess).append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}

	/**
	 * Parse xml request sent by AJAX
	 * @param xml
	 */
	private void parseRequestXml(String xml) {		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);
			
			Node node = document.getElementsByTagName("CaseWorkID").item(0);
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("CaseWorkID", content.getTextContent());
						
					}
					
				}						
			}
			
			node = document.getElementsByTagName("Bgroup").item(0);
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("Bgroup", content.getTextContent());
						
					}
					
				}						
			}
			
			node = document.getElementsByTagName("RequestResult").item(0);
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("RequestResult", "true");
						
					}
					
				}						
			}
			
			bais.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
