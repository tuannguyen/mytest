package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.EOWConsumer;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.ClearMemberCache;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * Servlet that handlers EOW actions
 */

public class EOWHandler extends Handler {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap requestMap;
	private String temp;
	private CmsUser currentUser;
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		
		requestMap = new HashMap();
		
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;		
		String bGroup = null;
		String refNo = null;
		
		currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
		}
		
		if (validate(xml)==true && bGroup != null && refNo != null) {
			boolean isUpdated = false;
			try {
				
				requestMap.put("bGroup", bGroup);
				requestMap.put("refNo", refNo);
				requestMap.put("Form", xml);
				
				parseRequestXml(xml);
				
				String store  = (String)requestMap.get("tempdata");
				if(store != null){
					/*
					 * if the store tag exist in the request then perform store temp data
					 */
					
					String content = xml.replaceAll("<AjaxParameterRequest>", "");
					content = content.replaceAll("</AjaxParameterRequest>", "");
					content = content.replaceAll("<Store>", "");
					content = content.replaceAll("</Store>", "");					
					content = content.concat("<Store>true</Store>");					

					
					currentUser.setAdditionalInfo(Environment.MEMBER_EOW, content);
					
					xmlResponse = this.responseSuccess();
				}
				
				if(store == null){
					
					String select  = (String)requestMap.get("select");
					if(select != null)
					{
						/*
						 * if the select tag exist in the request then perform select
						 */
						if(select.equals("true")){
							/*
							 * if the value of the select tag is true = select from database
							 */

							/*
							 * Call Consumer
							 */
							EOWConsumer consumer = new EOWConsumer(requestMap);
							HashMap showMap = consumer.selectData();
							
							/*
							 * Get tow maps, one from SqlDB and one from OracleDB
							 */
							HashMap sqlMap = (HashMap)showMap.get("AdditionalInformation");
							HashMap oracleMap = (HashMap)showMap.get("OracleData");
							
							
							StringBuffer buffer = new StringBuffer();
							
							buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
							
							/*
							 *Get AdditionalInformation and append in String buffer 
							 */
							String addtionalInformation = (String)sqlMap.get("message");
							if(addtionalInformation != null){								
								buffer.append(responeAdditionalInformation(addtionalInformation));
							}
							
							
							/*
							 * Get map of lump sum death benefit from OracleDB result map
							 */
							HashMap lumpMap = (HashMap)oracleMap.get("LumpSumMap");					
							for(int i = 1; i <= lumpMap.size(); i++){
								
								HashMap map = (HashMap)lumpMap.get(i);								
								
								String title = (String)map.get("Title");
								String forenames = (String)map.get("Forenames");
								String initials = (String)map.get("Initials");
								String surname = (String)map.get("Surname");
								String street = (String)map.get("Street");
								String postalTown = (String)map.get("PostalTown");
								String district = (String)map.get("District");
								String county = (String)map.get("County");
								String postcode = (String)map.get("Postcode");
								String country = (String)map.get("Country");
								String relationship = (String)map.get("Relationship");
								String percentage = (String)map.get("Percentage");
								
								String lump = responeLumpSum(title, forenames, initials, surname, street, postalTown, district, county, postcode, country, relationship, percentage);
								buffer.append(lump);
								
							}
							
							/*
							 * Get map of survivor pension from OracleDB result map
							 */
							HashMap surMap = (HashMap)oracleMap.get("SurvivorMap");
							for(int i = 1; i <= surMap.size(); i++){
								HashMap map = (HashMap)surMap.get(i);
								String title = (String)map.get("Title");
								String forenames = (String)map.get("Forenames");
								String initials = (String)map.get("Initials");
								String surname = (String)map.get("Surname");
								String street = (String)map.get("Street");
								String postalTown = (String)map.get("PostalTown");
								String district = (String)map.get("District");
								String county = (String)map.get("County");
								String postcode = (String)map.get("Postcode");								
								String country = (String)map.get("Country");
								String relationship = (String)map.get("Relationship");								
								String sur = responeSurvivorsPension(title, forenames, initials, surname, street, postalTown, district, county, postcode, country, relationship);
								buffer.append(sur);						
							}					
							buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
							xmlResponse = buffer.toString();
						}
						if(select.equals("false"))
						{
							/*
							 * if the value of the select tag is true = select temp data
							 */

							StringBuffer buffer  = new StringBuffer();
							buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
							buffer.append((String)currentUser.getAdditionalInfo(Environment.MEMBER_EOW));
							buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
							xmlResponse = buffer.toString();
						}
						
					}
					
					if(select == null)
					{
						/*
						 * if the update tag exists = update data
						 */

						EOWConsumer consumer = new EOWConsumer(requestMap);
						isUpdated = consumer.processData();	
						
						/*
						 * Respone success or error
						 */
						if(isUpdated == true)
						{					
							xmlResponse = responseSuccess();
							//Remove temp data 
							currentUser.deleteAdditionalInfo(Environment.MEMBER_EOW);
							ClearMemberCache.clearCache(currentUser);
						}				
						if(isUpdated == false){
							xmlResponse = responseError();
						}
					}
				}
			} 
			catch (Exception ex) {
				LOG.info(ex.getMessage() + ex.getCause());
			}
		}
		
		out.print(xmlResponse);		
		out.close();
		requestMap.clear();

	}

	/**
	 * Generate xml respone for the lump sum members
	 * @param title
	 * @param forenames
	 * @param initials
	 * @param surname
	 * @param street
	 * @param postalTown
	 * @param district
	 * @param county
	 * @param postcode
	 * @param country
	 * @param relationship
	 * @param percentage
	 * @return
	 */
	private String responeLumpSum(String title, String forenames, String initials, String surname, 
							     String street, String postalTown, String district, String county, 
								 String postcode, String country, String relationship, String percentage){
		
		
		StringBuffer buffer = new StringBuffer();		
	
		buffer.append("<").append("LumpSum").append(">\n");			
		buffer.append("		<").append("Title").append(">").append(title).append("</Title").append(">\n");
		buffer.append("		<").append("Forenames").append(">").append(forenames).append("</Forenames").append(">\n");
		buffer.append("		<").append("Initials").append(">").append(initials).append("</Initials").append(">\n");
		buffer.append("		<").append("Surname").append(">").append(surname).append("</Surname").append(">\n");
		buffer.append("		<").append("Street").append(">").append(street).append("</Street").append(">\n");
		buffer.append("		<").append("PostalTown").append(">").append(postalTown).append("</PostalTown").append(">\n");
		buffer.append("		<").append("District").append(">").append(district).append("</District").append(">\n");
		buffer.append("		<").append("County").append(">").append(county).append("</County").append(">\n");
		buffer.append("		<").append("Postcode").append(">").append(postcode).append("</Postcode").append(">\n");
		buffer.append("		<").append("Country").append(">").append(country).append("</Country").append(">\n");
		buffer.append("		<").append("Relationship").append(">").append(relationship).append("</Relationship").append(">\n");
		buffer.append("		<").append("Percentage").append(">").append(percentage).append("</Percentage").append(">\n");
		buffer.append("</").append("LumpSum").append(">\n");			
		
		return buffer.toString();
	}
	
	/**
	 * Generate xml respone for the survivor members
	 * @param title
	 * @param forenames
	 * @param initials
	 * @param surname
	 * @param street
	 * @param postalTown
	 * @param district
	 * @param county
	 * @param postcode
	 * @param country
	 * @param relationship
	 * @return
	 */
	private String responeSurvivorsPension(String title,String forenames, String initials, String surname, 
									      String street, String postalTown, String district, String county, 
										  String postcode, String country, String relationship){
		
		StringBuffer buffer = new StringBuffer();		
		
		buffer.append("<").append("SurvivorsPension").append(">\n");			
		buffer.append("		<").append("Title").append(">").append(title).append("</Title").append(">\n");
		buffer.append("		<").append("Forenames").append(">").append(forenames).append("</Forenames").append(">\n");
		buffer.append("		<").append("Initials").append(">").append(initials).append("</Initials").append(">\n");
		buffer.append("		<").append("Surname").append(">").append(surname).append("</Surname").append(">\n");
		buffer.append("		<").append("Street").append(">").append(street).append("</Street").append(">\n");
		buffer.append("		<").append("PostalTown").append(">").append(postalTown).append("</PostalTown").append(">\n");
		buffer.append("		<").append("District").append(">").append(district).append("</District").append(">\n");
		buffer.append("		<").append("County").append(">").append(county).append("</County").append(">\n");
		buffer.append("		<").append("Postcode").append(">").append(postcode).append("</Postcode").append(">\n");
		buffer.append("		<").append("Country").append(">").append(country).append("</Country").append(">\n");
		buffer.append("		<").append("Relationship").append(">").append(relationship).append("</Relationship").append(">\n");			
		buffer.append("</").append("SurvivorsPension").append(">\n");			

				
		return buffer.toString();
	}
	
	private String responeAdditionalInformation(String mess){
		
		/*
		 * Generate xml respone for the AdditionalInformation
		 */
		StringBuffer buffer = new StringBuffer();	
		
		buffer.append("<").append("AdditionalInformation").append(">\n");			
		buffer.append("		<").append("Message").append("><![CDATA[").append(mess).append("]]></Message").append(">\n");			
		buffer.append("</").append("AdditionalInformation").append(">\n");	
		
		return buffer.toString();		
	}	

	
	private String responseSuccess(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append("DONE").append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	private String responseError(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append("FAIL").append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}	
	

	private void parseRequestXml(String xml) {
		/*
		 * Parse XML Request
		 */
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(lt\\)", "&lt;");
		xml = xml.replaceAll("\\(gt\\)", "&gt;");
		xml = xml.replaceAll("\\(plus\\)", "+");
		xml = xml.replaceAll("\\(percent\\)", "%");
		//xml = xml.replaceAll("\\(quot\\)", "?");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);
			
			NodeList select = null;			
			NodeList update = null;
			NodeList store = null;			
			
			HashMap updateMap = new HashMap();
			store = document.getElementsByTagName("Store");	
			if(store.getLength() > 0){

				requestMap.put("tempdata", "true");
			}
			else{
				select = document.getElementsByTagName("Select");
				if(select.getLength() > 0 ){
					/*
					 * Select data from database to display
					 */
					temp = (String)currentUser.getAdditionalInfo(Environment.MEMBER_EOW);
					if(temp != null && temp.length() > 0){

						requestMap.put("select", "false");
					}
					else{

						requestMap.put("select", "true");
					}
					
				}
				else{			
					
					update = document.getElementsByTagName("Update");
					int survivorQuantity = 0;
					survivorQuantity = document.getElementsByTagName("SurvivorsPension").getLength();
					if(update != null){
						/*
						 * Update data
						 */
						int z = 1;

						NodeList cList = update.item(0).getChildNodes();
						for(int i = 0; i < cList.getLength(); i++){							
							//Check through the xml request
							Node cNode = cList.item(i);
							if(cNode.getNodeType() == Node.ELEMENT_NODE){
								//if the node name equals one of belows
								if(cNode.getNodeName().equals("LumpSum") 
										|| cNode.getNodeName().equals("SurvivorsPension")
											|| cNode.getNodeName().equals("AdditionalInformation")){
									//create a map, store its name and child nodes
									if(cNode.hasChildNodes()){
										HashMap objMap = new  HashMap();
										objMap.put("objName", cNode.getNodeName());
										
										
										NodeList list = cNode.getChildNodes();
										for(int x = 0; x < list.getLength(); x++){
											Node node = list.item(x);
											if(node.getNodeType() == Node.ELEMENT_NODE && node.hasChildNodes()){
												Node tNode = node.getFirstChild();
												if(node.getNodeType() == Node.ELEMENT_NODE || node.getNodeType() == Node.CDATA_SECTION_NODE){
													objMap.put(node.getNodeName(), tNode.getTextContent());
													
												}
											}
										}
										updateMap.put(z, objMap);										
										z += 1;
									}									
								}								
							}							
						}					
					}
					int SurvivorPercentage = 100;
					if(survivorQuantity > 0){
						SurvivorPercentage = 100/survivorQuantity;						
					}
					requestMap.put("SurvivorPercentage", SurvivorPercentage);
					requestMap.put("updateMap", updateMap);
				}
			}
			bais.close();
		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
