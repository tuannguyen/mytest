package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.PaySlipConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.xml.XmlPaySlipDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

/**
 * 
 * @author SonNT
 * @version 2.0
 * @since 2007/5/5
 * Servlet that handlers showing PaySlip
 *
 */
public class PaySlipHandler extends Handler {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private String tagName;
	private String updateField;
	private String fiscalYear;
	public static boolean isDebugged = false;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		
		
		//get bGroup and refNo from session
		String bGroup = null;
		String refNo = null;
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP));
			refNo = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_REFNO));
			//LOG.info("PaySlip handler current user: " + bGroup + "-" + refNo);
		}
		
		String xmlResponse = null;
		XmlPaySlipDao paySlip = null;
		Hashtable<String, String> map = null;
		
		
		//Parse Xml Request from AJAX
		if (parseRequestXml(xml)) {
			if (bGroup != null && refNo != null && !bGroup.equals("") && !refNo.equals("") && currentUser != null) 
			{
				//LOG.info("PaySlip handler BEGIN! " + fiscalYear);
				// If xml is not null and bGroup, refNo is not null or blank
				try {
					boolean isTestHarness = false;
					isTestHarness = CheckConfigurationKey.getBooleanValue("testHarness.enable");
					//Check tesherness is enabled or not
					
					if(!isTestHarness){
						//Check if the PaySlip Hashtable for the current year dose exist or not
						//LOG.info("Payslip handler proccess request map: " + currentUser.getAdditionalInfo(bGroup + refNo + fiscalYear));
						map = (Hashtable<String, String>)currentUser.getAdditionalInfo("PaySlip_" + bGroup + "_" + refNo);
						String lastFiscalYear = (String)currentUser.getAdditionalInfo("PaySlip_FiscalYear");
						//LOG.info("Payslip handler proccess request map 2: " + map);
						if(map == null || (fiscalYear != null && !fiscalYear.equals(lastFiscalYear)))
						{
							//if not create new						
							// Huy added
							MemberDao memberDao = (MemberDao)currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
							LOG.info("PaySlip handler BEGIN! " + bGroup + "-" + refNo + "-" + fiscalYear);
							LOG.info(this.getClass().toString() + ".PENSION_START_DATE: " + memberDao.get(Environment.PENSION_START_DATE));
							PaySlipConsumer consumer = new PaySlipConsumer(memberDao.get(Environment.PENSION_START_DATE));
							map = consumer.getPaySlip(bGroup, refNo, fiscalYear);
							
							debugPayslipDao(map);
							//Store in CmsUser							
							currentUser.setAdditionalInfo("PaySlip_" + bGroup + "_" + refNo, map);
							currentUser.setAdditionalInfo("PaySlip_FiscalYear", fiscalYear);
							
						}
//						if (!isDebugged)
//						{
//							debugPayslipDao(map);
//							isDebugged = true;
//						}
						if (map != null)
						{
							//Get the request tag's value 
							String valueItem = (String)map.get(tagName);
							String startYear = (String)map.get("StartYear");
							String startMonth = (String)map.get("StartMonth");
							if(valueItem == null){
								
								valueItem = "";
								
							}
							if(map.containsKey(tagName)){
								
								xmlResponse = this.getXmlData(tagName, updateField,startYear,startMonth,valueItem);
								
							}
							else{
								xmlResponse = this.getXmlDataError(tagName, updateField, tagName + " not found in member dao object!");
							}
						}
						else
						{
							LOG.error("Error while getting map object of payslip: " + bGroup + "-" + refNo);
							xmlResponse = this.getXmlDataError(tagName, updateField, tagName + " not found in member dao object!");
						}
						
					}
					else{
						//use XML
						paySlip = new XmlPaySlipDao(bGroup, refNo, fiscalYear, tagName);
						// Create new instance of PaySlipDao
						String valueItem = paySlip.getMemberData(tagName);
						// Get value of the request tag
						if(map.containsKey(tagName)){
							String startYear = (String)map.get("StartYear");
							String startMonth = (String)map.get("StartMonth");
							xmlResponse = this.getXmlData(tagName, updateField, startYear, startMonth, valueItem);
						}
						else{
							xmlResponse = this.getXmlDataError(tagName, updateField, "Test harness is still enabled!");
						}
						// Write respone
					}
					
				} catch (Exception ex) 
				{
					LOG.error("Error while calculating pay slip for member: " + bGroup + "-" + refNo + ": " + ex.toString());
					xmlResponse = this.getXmlDataError(tagName, updateField, bGroup + "-" + refNo + ": " + ex.toString());// Write respone
					
				}
			}
		} else 
		{
			LOG.error("Error while parsing XML request parameter: " + bGroup + "-" + refNo);
			xmlResponse = this.getXmlDataError(tagName, updateField, "Error while parsing XML request parameter: " + bGroup + "-" + refNo);
		}
		
		out.print(xmlResponse);
		out.close();
	}

	public String getXmlData(String tagName, String updateField, String startYear, String startMonth,
			String valueItem) {

			StringBuffer buffer = new StringBuffer();

			buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
					">\n");
			buffer.append("     <").append(Constant.DATA).append(">\n");
			buffer.append("         <").append(Constant.REQUESTED_TAG).append(
					">").append(tagName).append("</").append(
					Constant.REQUESTED_TAG).append(">\n");
			buffer.append("         <").append(Constant.UPDATE_FIELD).append(
					">").append(updateField).append("</").append(
					Constant.UPDATE_FIELD).append(">\n");
			buffer.append("         <").append("StartYear").append(
			">").append(startYear).append("</").append(
			"StartYear").append(">\n");
			buffer.append("         <").append("StartMonth").append(
			">").append(startMonth).append("</").append(
			"StartMonth").append(">\n");			
			buffer.append("         <").append(Constant.TYPE).append(">\n");
			buffer.append("             <").append("Text").append("/>\n");
			buffer.append("         </").append(Constant.TYPE).append(">\n");
			buffer.append("         <").append(Constant.VALUE).append(">")
					.append(valueItem).append("</").append(Constant.VALUE)
					.append(">\n");
			buffer.append("     </").append(Constant.DATA).append(">\n");
			buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE)
					.append(">\n");

			return buffer.toString();
		
	}

	private String getXmlDataError(String tagName, String updateField, String errorMessage) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE).append(
				">" + errorMessage + "</").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();

	}

	private boolean parseRequestXml(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equals(Constant.FISCAL_YEAR)) {
						fiscalYear = node.getTextContent();
					}
					if (node.getNodeName().equals(Constant.REQUESTED_TAG)) {
						tagName = node.getTextContent();
					}
					if (node.getNodeName().equals(Constant.UPDATE_FIELD)) {
						updateField = node.getTextContent();
					}
				}
			}
			
			bais.close();
			
			return true;

		} catch (Exception ex) {
			tagName = "";
			updateField = "";
			fiscalYear = "";
		}

		return false;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public void debugPayslipDao (Map<String, String> payslip)
	{
		if (payslip != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");

			Set<String> keys = payslip.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = (String) keyIterator.next();
				
				LOG.info("DEBUG Payslip DAO: " + key + " = " + payslip.get(key));
				
			}
			
			LOG.info("****************** END OF DEBUG MEMBER DAO *****************");
		}
	}	
}
