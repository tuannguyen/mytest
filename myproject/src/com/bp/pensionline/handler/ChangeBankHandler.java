package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.ChangeBankConsumer;
import com.bp.pensionline.consumer.ImailConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.letter.producer.MemberLoader;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.Member;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/15
 * Servlet that handlers changing bank details
 *
 */
public class ChangeBankHandler extends Handler {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap requestMap = new HashMap();

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(Constant.CONTENT_TYPE);

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String requestXml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;
		

		String bGroup = null;
		String refNo = null;
		String ni = "";
		String scheme= "";
		String pensionerName = "";
		
		/*
		 * Try to get BGROUP and REFNO from currentUser
		 */
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null){
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			
			// Added by Huy to get Ni
			MemberDao memberDao = (MemberDao)currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
			if (memberDao != null && memberDao.get("Nino") != null)
			{
				ni = memberDao.get("Nino").toLowerCase();
			}
			if (memberDao != null && memberDao.get("SchemeName") != null)
			{
				scheme = memberDao.get("SchemeName");
			}
			if (memberDao != null && memberDao.get("Name") != null)
			{
				pensionerName = memberDao.get("Name");
			}
		}	

		if (validate(requestXml) == true && bGroup != null && refNo != null) {
		
			parseRequestXml(requestXml);//parse XML Request				
			boolean isUpdated = false;
			
			requestMap.put("bGroup", bGroup);
			requestMap.put("refNo", refNo);
			requestMap.put("Form", requestXml);
			requestMap.put("userName", currentUser.getName());			
			
			String name = (String)requestMap.get("AccountName");
			String accNumber = (String)requestMap.get("AccountNumber");
			String bank = (String)requestMap.get("Bank");
			String sortCode = (String)requestMap.get("SortCode");
			String rollNumber = (String)requestMap.get("RollNumber");		
			String isBank = (String)requestMap.get("IsBank");
			
			boolean isBankBool = (isBank != null && isBank.equals("checked"));
			
			//if valid security code - send Imail 
			StringBuffer buf = new StringBuffer();
			buf.append("<ChangeBank>\n");
			buf.append("\t<AccountName>").append(name).append("</AccountName>\n");
			buf.append("\t<Ni>").append(ni).append("</Ni>\n");			
			buf.append("\t<FinancialInstitution>").append(bank).append("</FinancialInstitution>\n");
			buf.append("\t<AccountNumber>").append(accNumber).append("</AccountNumber>\n");
			buf.append("\t<SortCode>").append(sortCode).append("</SortCode>\n");
			buf.append("\t<RollNumber>").append(rollNumber).append("</RollNumber>\n");
			buf.append("\t<IsBank>").append(isBankBool).append("</IsBank>\n");
			buf.append("</ChangeBank>");

			// Update message with REPORTING-1208
			StringBuffer bufMessage = new StringBuffer();
			bufMessage.append("Account Name: ").append(name);
			bufMessage.append("\nRef no: ").append(refNo);
			bufMessage.append("\nScheme: ").append(scheme);
			bufMessage.append("\nPensioner Name: ").append(pensionerName);
			bufMessage.append("\nDate change requested by member: ").append(DateUtil.toStringDDMMMYYYY(new Date()));
			
			bufMessage.append("\nNi: ").append(ni);			
			bufMessage.append("\nName of financial institution: ").append(bank);
						
			if (isBankBool)
			{				
				bufMessage.append("\nPaid by: ").append("Bank");
				bufMessage.append("\nSort code: ").append(sortCode);
				bufMessage.append("\nAccount number: ").append(accNumber);
			}
			else
			{
				bufMessage.append("\nPaid by: ").append("Building society");
				bufMessage.append("\nRoll number: ").append(rollNumber);
			}
			

			//CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
			// Huy modified here
			ImailSQLHandler imailSQLHandler = new ImailSQLHandler();
				
			int maxIMailNo = imailSQLHandler.getMaxIMailNo();
			int newIMailNo = maxIMailNo + 1;

			requestMap.put(Environment.IMAIL_BGROUP, "UNK");
			requestMap.put(Environment.IMAIL_REFNO, refNo);
			requestMap.put(Environment.IMAIL_TEMP, String.valueOf(newIMailNo));
			requestMap.put(Environment.IMAIL_NAME, name);
			
			requestMap.put(Environment.IMAIL_NAME, name);
			requestMap.put(Environment.IMAIL_NI, ni);
			requestMap.put(Environment.IMAIL_SUBJECT, "Change Bank Request");
			requestMap.put(Environment.IMAIL_MESSAGE, bufMessage.toString());
			requestMap.put(Environment.IMAIL_CREATIONDATE, new java.sql.Date(System.currentTimeMillis()));
			requestMap.put(Environment.IMAIL_CASEWORKID, String.valueOf(newIMailNo));
			requestMap.put(Environment.IMAIL_FORMDATA, buf.toString());
			
			if(isBankBool)	//if paid by bank
			{
				//check sort code and acc no and bank name
				int checkResult = com.bp.pensionline.util.ValidChangeBank.validChangeBank(accNumber, sortCode, bank);
				if(checkResult == 4){				
					
					// Remove casework, use IMAIL
					//If details are valid then Call Consumer		
					/*
					ChangeBankConsumer consumer = new ChangeBankConsumer(requestMap);
					isUpdated = consumer.processData();						
					*/
					
					ImailConsumer imailConsumer = new ImailConsumer(requestMap);
					try {
						//System.out.println("imailConsumer.processData");
						imailConsumer.processData();
						isUpdated = true;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//System.out.println("Error in creating IMail: " + e.toString());
					}
					
					if(isUpdated){
						xmlResponse = responseSuccess();				
					}
					else{
						xmlResponse = responseError("We were unable to process your request at this time");
					}	
				}
				else{
					if(checkResult == 1)
						xmlResponse = responseError(" Account Number is not valid \n Must be 8 digits");
					if(checkResult == 2)
						xmlResponse = responseError(" SortCode is not valid \n Must be 6 digits");
					if(checkResult == 3)
						xmlResponse = responseError(" Bank Name is not valid \n Must be 0 - 30 chars");
				}
			}
			else
			{	//if paid by building society
				//check bank name
				int checkResult = com.bp.pensionline.util.ValidChangeBank.validChangeBank("12345678", "12-34-56", bank);
				if(checkResult == 4){				
					
					//If details are valid then Call Consumer				 
//					ChangeBankConsumer consumer = new ChangeBankConsumer(requestMap);
//					isUpdated = consumer.processData();	
						
					ImailConsumer imailConsumer = new ImailConsumer(requestMap);
					try {
						//System.out.println("imailConsumer.processData");
						imailConsumer.processData();
						isUpdated = true;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//System.out.println("Error in creating IMail: " + e.toString());
					}
					
					if(isUpdated){
						xmlResponse = responseSuccess();				
					}
					else{
						xmlResponse = responseError("We were unable to process your request at this time");
					}	
				}
				else{
					if(checkResult == 3)
						xmlResponse = responseError(" Bank Name is not valid \n Must be 0 - 30 chars");					
				}
			}			
					
		}
		
		
		out.print(xmlResponse);
		out.close();
	}
	
	/**
	 * Build respone with successful message 
	 * @return
	 */
	private String responseSuccess(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append("Thank you your details have been updated").append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * Build respone with error message 
	 * @param mess
	 * @return
	 */
	private String responseError(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append(mess).append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * parse Xml request sent from AJAX to get values
	 * @param xml
	 */
	private void parseRequestXml(String xml) {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
			document = builder.parse(bais);

			NodeList root = document.getElementsByTagName("ChangeBank");
			NodeList list = root.item(0).getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
		
					if (node.getNodeName().equals("AccountName")) {
						requestMap.put("AccountName",node.getTextContent());	
						
					}
					if (node.getNodeName().equals("AccountNumber")) {
						requestMap.put("AccountNumber",node.getTextContent());
						
					}
					if (node.getNodeName().equals("Bank")) {
						requestMap.put("Bank",node.getTextContent());
						
					}
					if (node.getNodeName().equals("SortCode")) {
						requestMap.put("SortCode",node.getTextContent());
						
					}
					if (node.getNodeName().equals("RollNumber")) {
						requestMap.put("RollNumber",node.getTextContent());
						
					}				
					if (node.getNodeName().equals("IsBank")) {
						requestMap.put("IsBank",node.getTextContent());
						
					}
					if (node.getNodeName().equals("IsBuildingSociety")) {
						requestMap.put("IsBuildingSociety",node.getTextContent());
						
					}										
				}
			}
			bais.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
