package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.CompanySchemeDao;
import com.bp.pensionline.test.XmlReader;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 07/05/2007
 * This class use to combination companygroup base on member's bgroup and membershipschemeraw
 *
 */
public class CompanySchemeRawHandler {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	private static CompanySchemeDao cpnyDao = new CompanySchemeDao();

	private static CompanySchemeRawHandler instance = null;

	protected CompanySchemeRawHandler() {
	}

	/**
	 * @return one instance of CompanySchemeRawHandler
	 */
	public static CompanySchemeRawHandler getInstance() {

		if (instance == null) {

			instance = new CompanySchemeRawHandler();
		}
		
		loadCompanySchemeRaw();

		return instance;
	}

	/**
	 * Load CompanySchemeMap.xml file and store it in CmsUser additoninfo
	 */
	private static void loadCompanySchemeRaw() {

		try {
			XmlReader reader = new XmlReader();

			ByteArrayInputStream btArr = new ByteArrayInputStream(reader
					.readFile(Environment.COMPANY_SCHEME_FILE));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			NodeList listOfCompanyScheme = doc
					.getElementsByTagName(Environment.COMPANY_SCHEME_TAG);

			if (listOfCompanyScheme.getLength() > 0) {

				NodeList schemeList = listOfCompanyScheme.item(0)
						.getChildNodes();
				
				int length = schemeList.getLength();				

				for (int i = 0; i < length; i++) {
					Node firstNode = schemeList.item(i);

					if (firstNode.getNodeType() == Node.ELEMENT_NODE
							&& firstNode.hasChildNodes()) {
						Node textNode = firstNode.getFirstChild();
						if (textNode.getNodeType() == Node.TEXT_NODE) {
							String nodeValue = textNode.getNodeValue();
							String nodeName = firstNode.getNodeName();
							
							
							cpnyDao.set(nodeName, nodeValue);

						}
					}
				}
			}
		} catch (Exception e) {

			/* System.out.println("CompanySchemeRawHandler error :"); */
			LOG.error("com.bp.pensionline.handler.CompanySchemeRawHandler.loadCompanySchemeRaw", e);

		}

	}

	/**
	 * Modified by HUY
	 * @param tmpBGroup
	 * @param tmpSchemeRaw
	 * @return combination company group
	 */
	public String getCompanyGroup(String tmpBGroup, String tmpSchemeRaw) {

		if (tmpBGroup == null || tmpSchemeRaw == null || tmpBGroup.trim().equals("") || tmpSchemeRaw.equals(""))
		{
			LOG.error("BGroup or tmpScheme is NULL or EMPTY: " + tmpBGroup + "-" + tmpSchemeRaw);
			return null;
		}
		return cpnyDao.getSuitableScheme(new String(tmpBGroup + "-"
				+ tmpSchemeRaw));

	}
}
