package com.bp.pensionline.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bp.pensionline.database.WallSQLHandler;
import com.bp.pensionline.test.Constant;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 07/05/2007
 * This class use to prepare data before update user blocking
 *
 */
public class WallBeforeUpdate extends HttpServlet{
	
	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * 
	 */
	public void processRequest (HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String gName=String.valueOf(request.getParameter("gName"));
		try {
			StringBuffer buf=new StringBuffer();
			WallSQLHandler wallSqlHandler=new WallSQLHandler();
			List<String> result=wallSqlHandler.getDataByGname(gName);
			if (result!=null && result.size()>0){
				Iterator<String> it=result.iterator();
				while (it.hasNext()){
					String tem=it.next();
					if (tem!=null && tem.length()>0){
						buf.append(tem+"\n");
					}
					
				}
			}else {
				buf.append("");
			}
			
			out.print(buf);
			out.flush();
			out.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		
	}
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
