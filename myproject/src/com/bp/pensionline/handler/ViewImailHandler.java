package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Node;



import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.test.Constant;

/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/17
 * Servlet that handlers Imail view
 */

public class ViewImailHandler extends Handler {
	
	private HashMap requestMap;
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		
		requestMap = new HashMap();
		
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;
		
		if (validate(xml)) {

			try {				
				
				parseRequestXml(xml);				
				
				//try to get request values
				String viewImail = (String)requestMap.get("ViewImail");
				String viewArchivedImail = (String)requestMap.get("ViewArchivedImail");
				String caseworkId = (String)requestMap.get("UpdateImail");	
				String total = null;
				  
				if(viewImail != null){
					/*
					 * Select Actived Imails to display
					 */
					String requestPage = (String)requestMap.get("RequestPage");

					int from = (Integer.parseInt(requestPage)-1)*20;
					
					HashMap map = ImailSQLHandler.selectMySqlImail(viewImail, from);
					//get total
					total = String.valueOf((Integer)map.get("total"));					
					

					//Build response
					StringBuffer buff = new StringBuffer();
					buff.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
					buff.append("		<Total>").append(total).append("</Total>\n");
					
					//get each imail from map
					for(int i = 1; i <= 20; i++){
						HashMap imailMap = (HashMap)map.get(i);
						if(imailMap != null){

							String member = (String)imailMap.get("member");				
							String submitted = (String)imailMap.get("submitted");
							String id = (String)imailMap.get("caseworkId");
							String subject = (String)imailMap.get("subject");
							String form = (String)imailMap.get("form");
							
							
							buff.append(responseImail(member, id, subject, submitted, form));
						}
					}					
					
					buff.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
					
					xmlResponse = buff.toString();
				}
				
				if(viewArchivedImail != null){
					/*
					 * Select Archived Imails to display
					 */
					String requestPage = (String)requestMap.get("RequestPage");					
					int from = (Integer.parseInt(requestPage)-1)*20;
					
					HashMap map = ImailSQLHandler.selectMySqlImail(viewArchivedImail,from);					
					//get total
					total = String.valueOf((Integer)map.get("total"));
					
					//Build response
					StringBuffer buff = new StringBuffer();
					buff.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
					buff.append("		<Total>").append(total).append("</Total>\n");
					
					//get each imail from map
					for(int i = 1; i <= 20; i++){
						HashMap imailMap = (HashMap)map.get(i);
						if(imailMap != null){
							String member = (String)imailMap.get("member");				
							String submitted = (String)imailMap.get("submitted");
							String id = (String)imailMap.get("caseworkId");
							String subject = (String)imailMap.get("subject");				
							String archived = (String)imailMap.get("archived");
							String form = (String)imailMap.get("form");
							buff.append(responseArchivedImail(member, id, subject, submitted, archived, form));
						}
					}					
					
					buff.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
					
					xmlResponse = buff.toString();
				}
				
				if(caseworkId != null){
					/*
					 * Update Imail
					 */
					boolean isUpdated = ImailSQLHandler.updateMySqlImail(caseworkId);
					if(isUpdated){
						xmlResponse = responseSuccess("Imail Updated!");
					}
					else{
						xmlResponse = responseError("Update Imail Fail to Complete!");
					}
				}
			
			} 
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
		out.print(xmlResponse);		
		out.close();
		requestMap.clear();

	}
	
	/**
	 * Generate respone for Actived Imails
	 * @param member
	 * @param id
	 * @param subject
	 * @param submitted
	 * @param form
	 * @return
	 */
	private String responseImail(String member, String id, String subject, 
			String submitted, String form){
		StringBuffer buff = new StringBuffer();
		buff.append("<Imail>\n");
		buff.append("	<Subject>").append(subject).append("</Subject>\n");
		buff.append("	<Member>").append(member).append("</Member>\n");
		buff.append("	<Submitted>").append(submitted).append("</Submitted>\n");
		buff.append("	<CaseworkId>").append(id).append("</CaseworkId>\n");
		buff.append("	<FormData>").append(form).append("</FormData>\n");		
		buff.append("</Imail>\n");
		return buff.toString();
	}
	
	/**
	 * Generate respone for Archived Imails
	 * @param member
	 * @param id
	 * @param subject
	 * @param submitted
	 * @param archived
	 * @param form
	 * @return
	 */
	private String responseArchivedImail(String member, String id, String subject, 
			String submitted, String archived, String form){
		StringBuffer buff = new StringBuffer();
		buff.append("<Imail>\n");
		buff.append("	<Subject>").append(subject).append("</Subject>\n");
		buff.append("	<Member>").append(member).append("</Member>\n");
		buff.append("	<Submitted>").append(submitted).append("</Submitted>\n");
		buff.append("	<CaseworkId>").append(id).append("</CaseworkId>\n");
		buff.append("	<Archived>").append(archived).append("</Archived>\n");
		buff.append("	<FormData>").append(form).append("</FormData>\n");		
		buff.append("</Imail>\n");
		return buff.toString();
	}
	
	/**
	 * generate success response
	 * @param mess
	 * @return
	 */
	private String responseSuccess(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append(mess).append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	/**
	 * generate error response
	 * @param mess
	 * @return
	 */
	private String responseError(String mess){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append(mess).append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}

	/**
	 * Parse xml request
	 * @param xml
	 */
	private void parseRequestXml(String xml) {		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);
			
			Node node = document.getElementsByTagName("ViewActiveImail").item(0);
			if(node != null){

				requestMap.put("ViewImail", "ViewImail");						
				
				node = document.getElementsByTagName("RequestPage").item(0);
				if(node != null){
					Node child = node.getFirstChild();
					if(child != null){
						if(child.getNodeType() == Node.TEXT_NODE){
							requestMap.put("RequestPage", child.getTextContent());							
						}						
					}						
				}
			}
			else{
				node = document.getElementsByTagName("ViewArchivedImail").item(0);
				if(node != null){
					
					requestMap.put("ViewArchivedImail", "ViewArchivedImail");							
					
					node = document.getElementsByTagName("RequestPage").item(0);
					if(node != null){
						Node child = node.getFirstChild();
						if(child != null){
							if(child.getNodeType() == Node.TEXT_NODE){
								requestMap.put("RequestPage", child.getTextContent());							
							}						
						}						
					}
				}
				else{
					node = document.getElementsByTagName("UpdateImail").item(0);
					if(node != null){
						Node content = node.getFirstChild();
						if(content != null){
							if(content.getNodeType() == Node.TEXT_NODE){
								requestMap.put("UpdateImail", content.getTextContent());								
							}							
						}						
					}
					else{						
					}
				}
			}
			bais.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
