package com.bp.pensionline.handler;

import org.apache.log4j.Logger;





/**
 * @author Thao Duy Nguyen
 * @version 1.0
 * @since 15-06-2007
 *
 */
public class UserExpriedHandler {
	static Logger log = Logger.getLogger(UserExpriedHandler.class.getName());
	private static com.bp.pensionline.database.UserExpriedHandler handler =new com.bp.pensionline.database.UserExpriedHandler();
	
	public static void insert(String data,String gname,String system) {
		try {
			handler.insert(data, gname, system);
			return;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			return;
		}
		
	}
	public static void delete(String data,String gname){
		
		try {
			handler.delete(data, gname);
			return;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			return;
		}
	}
	
	public static void update(String data, String plName){
		try {
			handler.update(data, plName);
			return;
		} catch (Exception e) {
			log.error(e);
			return;
		}
	}

	public static boolean checkUserExit (String data){
		boolean isExit =false;
		try {
			isExit =handler.checkUserExit(data);
			return isExit ;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			isExit =false;
			return isExit ;
		}
		
	}

}
