package com.bp.pensionline.handler;

import org.opencms.i18n.A_CmsMessageBundle;
import org.opencms.i18n.I_CmsMessageBundle;
public final class Messages extends A_CmsMessageBundle{
	private static final String BUNDLE_NAME = "com.bp.pensionline.handler.messages";
	public static final String PWD_TOO_SHORT="PWD_TOO_SHORT";
	public static final String PWD_INVALID="PWD_INVALID";
	public static final String PWD_INVALID_CHARS="PWD_INVALID_CHARS";

    /** Static instance member. */
    private static final I_CmsMessageBundle INSTANCE = new Messages();
    
    /**
     * Hides the public constructor for this utility class.<p>
     */
    private Messages() {

        // hide the constructor
    }

    /**
     * Returns an instance of this localized message accessor.<p>
     * 
     * @return an instance of this localized message accessor
     */
    public static I_CmsMessageBundle get() {

        return INSTANCE;
    }

    /**
     * Returns the bundle name for this .<p>
     * 
     * @return the bundle name for this 
     */
    public String getBundleName() {

        return BUNDLE_NAME;
    }
	

}
