package com.bp.pensionline.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.database.WallSQLHandler;
import com.bp.pensionline.util.SystemAccount;


/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 07/05/2007
 * 
 *
 */
public class WallHandler {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * Default contructor
	 */
	public WallHandler(){}
	/**
	 * @param data
	 * @param gName
	 * @param superUser
	 * @param system
	 * Insert to MySql database
	 */
	public  void insert(String  data,String gName,String superUser,String system){
		try {
			WallSQLHandler sqlhandler=new WallSQLHandler();
			sqlhandler.insert(data, gName, superUser, system);
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.handler.WallHandler.insert", e);
			
		}
		
		
	}
	public void deleteByGname(String gName){
		try {
			WallSQLHandler sqlhandler=new WallSQLHandler();
			sqlhandler.deleteByGname(gName);
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.handler.WallHandler.deleteByGname", e);
			
		}
	}
	/**
	 * @param username
	 * @param bgroup
	 * @param refno
	 * Block user to any wall them belong
	 */
	public static void addUserToWallBlock(String username,String bgroup,String refno){
		List gName=new ArrayList();
		try {
			gName=WallSQLHandler.getGName(bgroup, refno);
			if (gName!=null && gName.size()>0){// if user with bgroup and refno belong any Wall groups
				Iterator  it=gName.iterator();
				while (it.hasNext()){
					String wallName=String.valueOf(it.next());
					System.out.println("\n\n\n Assign user with bgroup +refno " +bgroup +" " +refno +" to wall " +wallName);
					SwapUserHandler.getInstance().swapUser(username, wallName,SystemAccount.getAdminCmsObject());
				}
			}else {
				return;//do nothing
			}
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.handler.WallHandler.addUserToWallBlock", e);
			
			
		}
		
	}
	/**
	 * @param username
	 * @param bgroup
	 * @param refno
	 * Remove user from any wall them belong
	 */
	public static void removeUserFromWallBlock(String username,String bgroup,String refno){
		List gName=new ArrayList();
		try {
			gName=WallSQLHandler.getGName(bgroup, refno);
			if (gName!=null && gName.size()>0){// if user with bgroup and refno belong any Wall groups
				Iterator  it=gName.iterator();
				while (it.hasNext()){
					String wallName=String.valueOf(it.next());
					SwapUserHandler.getInstance().unSwapUser(username, wallName,SystemAccount.getAdminCmsObject());
				}
			}else {
				return;//do nothing
			}
		} catch (Exception e) {
			// TODO: handle exception
			
			LOG.error("com.bp.pensionline.handler.WallHandler.removeUserFromWallBlock", e);
		}
		
	}
	

}
