package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.exception.InvalidFormatException;
import com.bp.pensionline.exception.ValueNotInitializedException;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;


public class GetConditionalValueHandler extends Handler
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private static final long serialVersionUID = 1L;
	private String type;	
	
	private String tagName;

	private String updateField;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		//get request
		String xml = request.getParameter(Constant.PARAM);

		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		List record = null;

		if (currentUser != null) {
			record = (List)currentUser.getAdditionalInfo(Environment.RECORD_LIST);
		}		

		String xmlResponse = null;

		if (parseRequestXml(xml)) 
		{

			if (currentUser != null) 
			{
				try 
				{									
					MemberDao memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
					
					if (memberDao != null)
					{
						//check for multiple period of service	
						if (record != null && record.size() > 1) 
						{
							memberDao.set("MultiplePeriodsOfService", "true");
						} 
						else 
						{
							memberDao.set("MultiplePeriodsOfService", "false");
						}							
						if (type.equals(Constant.TYPE_CONDITION))
						{
							String normalizedTagName = tagName.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
							boolean result = evaluateExpression(normalizedTagName, memberDao);
							xmlResponse = this.getXmlConditionalData(tagName, updateField, result);
							//if (updateField.equals("conditional_13")) debugMemberDao(memberDao);							
						}
					}
					else
					{
						xmlResponse = this.getXmlCondDataError(tagName, updateField);
					}
				}
				catch (ValueNotInitializedException vnie) 
				{
					xmlResponse = this.getXmlCondDataError(tagName, updateField);
					LOG.info("Value in MemberDao not found: " + vnie.getMessage());
				}
			}
			else 
			{
				xmlResponse = this.getXmlCondDataError(tagName, updateField);

			}
		} 
		else 
		{
			xmlResponse = this.getXmlCondDataError(tagName, updateField);
		}
		out.print(xmlResponse);
		out.close();

	}
	
	public String getXmlConditionalData(String tagName, String updateField, boolean result)
	{

		String valueItem = result ? "TRUE" : "FALSE";
		// valueItem = valueItem.trim();

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">").append("<![CDATA[")
				.append(tagName).append("]]>").append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.TYPE).append(">")
				.append(Constant.TYPE_CONDITION).append("</").append(Constant.TYPE)
				.append(">\n");		
		buffer.append("         <").append(Constant.VALUE).append(">").append(
				valueItem).append("</").append(Constant.VALUE).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();
	}	

	private String getXmlCondDataError(String tagName, String updateField)
	{

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">").append("<![CDATA[")
				.append(tagName).append("]]>").append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE).append(
				"><![CDATA[" + tagName + "]]></").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();

	}

	private boolean parseRequestXml(String xml)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{

					if (node.getNodeName().equals(Constant.TYPE))
					{
						type = node.getTextContent();
					}
					if (node.getNodeName().equals(Constant.REQUESTED_TAG))
					{
						NodeList requestTagNodes = node.getChildNodes();
						for (int j = 0; j < requestTagNodes.getLength(); j++)
						{
							Node current = requestTagNodes.item(j);
							if(current.getNodeType() == Node.CDATA_SECTION_NODE)
							{
								tagName = ((CDATASection)current).getData();
								break;
							}
						}
					}
					if (node.getNodeName().equals(Constant.UPDATE_FIELD))
					{
						updateField = node.getTextContent();
					}
				}
			}
			bais.close();

			return true;

		}
		catch (Exception ex)
		{
			tagName = "";
			updateField = "";
		}

		return false;

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public static final String conditionPartern = getString("ConditionalPattern");
	public static final String compOperatorPattern = getString("CompOperatorPattern");
	
	public static final String[] datePattern = {"d MMM yyyy", "d-MMM-yyyy", "dd/MM/yyyy", "dd-MM-yyyy"};
	
	/**
	 * Evaluate a simple conditional expression
	 * @param expression
	 * @return
	 */
	public static boolean evaluateExpression(String expression, MemberDao memberDao) throws ValueNotInitializedException
	{
		//LOG.info("Expression: " + expression);
		// valid expression format
		try
		{
			String compOperator = null;
			String[] operants = null;
			
			boolean isExpValid = Pattern.matches(conditionPartern, expression);	
			if (!isExpValid)
			{	
				return false;
			}
			
			Pattern compPattern = Pattern.compile(compOperatorPattern);
			Matcher matcher = compPattern.matcher(expression);
			if (matcher.find())
			{
				compOperator = matcher.group();
			}
			else 
			{
				return false;
			}

			
			// Get left and right operant. Currently, only simple expressions are supported.
			operants = expression.split(compOperatorPattern, 2);
			if (operants!= null && operants.length == 2 && compOperator != null)
			{

				return compare2Value(getOperantValue(operants[0], memberDao), 
						getOperantValue(operants[1], memberDao), compOperator);
		
			}	
		}
		catch (PatternSyntaxException pse)
		{
			LOG.info("PatternSyntaxException: " + pse.getMessage());
		}
		catch (InvalidFormatException ife)
		{
			LOG.info(ife.getMessage());
		}		
		catch (ValueNotInitializedException vnie) {
			// TODO: handle exception
			throw vnie;
		}
		
		return false;
	}
	
	/**
	 * Compare 2 values based on their types and comparator
	 * @param leftOperant
	 * @param rightOperant
	 * @param compOperator
	 * @return
	 */
	private static boolean compare2Value(Object leftOperant, Object rightOperant, String compOperator)
	{
		//LOG.info(leftOperant + " " + compOperator + " " + rightOperant);
		// Check for null value first
		if (leftOperant == null && rightOperant == null)
		{
			if (compOperator.trim().equals("=") || compOperator.trim().equals("=="))
			{
				return true;
			}
		}
		
		if ((leftOperant != null && rightOperant == null) || (leftOperant == null && rightOperant != null))
		{
			if (compOperator.trim().equals("!="))
			{
				return true;
			}
		}
		
		if (leftOperant != null && rightOperant != null && leftOperant.getClass().equals(rightOperant.getClass()))
		{
			if (compOperator.trim().equals("=") || compOperator.trim().equals("=="))
			{
				return leftOperant.equals(rightOperant);
			}
			else if (compOperator.trim().equals("!="))
			{
				return !leftOperant.equals(rightOperant);
			}
			else if (compOperator.trim().equals("<"))
			{
				if ((leftOperant instanceof Boolean) || (leftOperant instanceof String))
				{
					// Invalid comparator, return false
					return false;
				}
				else if (leftOperant instanceof Integer)
				{
					Integer leftInteger = (Integer)leftOperant;
					Integer rightInteger = (Integer)rightOperant;
					return (leftInteger.intValue() < rightInteger.intValue());
				}
				else if (leftOperant instanceof Float)
				{
					Float leftFloat= (Float)leftOperant;
					Float rightFloat = (Float)rightOperant;
					return (leftFloat.floatValue() < rightFloat.floatValue());
				}	
				else if (leftOperant instanceof Date)
				{
					Date leftDate= (Date)leftOperant;
					Date rightDate = (Date)rightOperant;
					return (leftDate.compareTo(rightDate) < 0);
				}					
			}
			else if (compOperator.trim().equals("<="))
			{
				if ((leftOperant instanceof Boolean) || (leftOperant instanceof String))
				{
					// Invalid comparator, return false
					return false;
				}
				else if (leftOperant instanceof Integer)
				{
					Integer leftInteger = (Integer)leftOperant;
					Integer rightInteger = (Integer)rightOperant;
					return (leftInteger.intValue() <= rightInteger.intValue());
				}
				else if (leftOperant instanceof Float)
				{
					Float leftFloat= (Float)leftOperant;
					Float rightFloat = (Float)rightOperant;
					return (leftFloat.floatValue() <= rightFloat.floatValue());
				}	
				else if (leftOperant instanceof Date)
				{
					Date leftDate= (Date)leftOperant;
					Date rightDate = (Date)rightOperant;
					return (leftDate.compareTo(rightDate) <= 0);
				}					
			}
			else if (compOperator.trim().equals(">"))
			{
				if ((leftOperant instanceof Boolean) || (leftOperant instanceof String))
				{
					// Invalid comparator, return false
					return false;
				}
				else if (leftOperant instanceof Integer)
				{
					Integer leftInteger = (Integer)leftOperant;
					Integer rightInteger = (Integer)rightOperant;
					return (leftInteger.intValue() > rightInteger.intValue());
				}
				else if (leftOperant instanceof Float)
				{
					Float leftFloat= (Float)leftOperant;
					Float rightFloat = (Float)rightOperant;
					return (leftFloat.floatValue() > rightFloat.floatValue());
				}	
				else if (leftOperant instanceof Date)
				{
					Date leftDate= (Date)leftOperant;
					Date rightDate = (Date)rightOperant;
					return (leftDate.compareTo(rightDate) > 0);
				}					
			}	
			else if (compOperator.trim().equals(">="))
			{
				if ((leftOperant instanceof Boolean) || (leftOperant instanceof String))
				{
					// Invalid comparator, return false
					return false;
				}
				else if (leftOperant instanceof Integer)
				{
					Integer leftInteger = (Integer)leftOperant;
					Integer rightInteger = (Integer)rightOperant;
					return (leftInteger.intValue() >= rightInteger.intValue());
				}
				else if (leftOperant instanceof Float)
				{
					Float leftFloat= (Float)leftOperant;
					Float rightFloat = (Float)rightOperant;
					return (leftFloat.floatValue() >= rightFloat.floatValue());
				}	
				else if (leftOperant instanceof Date)
				{
					Date leftDate= (Date)leftOperant;
					Date rightDate = (Date)rightOperant;
					return (leftDate.compareTo(rightDate) >= 0);
				}					
			}			
		}
		
		return false;
	}
	/**
	 * @author Huy
	 * Get the value that the operant stands for:
	 * - If operant is enquoted in " and ", operant is a String
	 * - If operant is enquoted in ' and ', operant is Date value
	 * - If operant is enquoted in [[ and ]], operant is a memberDao value (String)
	 * - If operant is true or false, operant is a Boolean value
	 * - If operant is number, operant is number: Integer or Double
	 * @param operant The string represent the operant value
	 * @return
	 */
	private static Object getOperantValue(String operant, MemberDao memberDao) throws InvalidFormatException, ValueNotInitializedException
	{
		
		if (operant != null)
		{
			// Check if operant is NULL
			if (operant.equalsIgnoreCase("null"))
			{
				return null;
			}
			
			// operant is enquoted in " "
			if (operant.length() > 2 && operant.charAt(0) == '\"' && operant.charAt(operant.length()-1) == '\"')
			{
				// operant is String type. Eg. "AC", "NP"
				return operant.substring(1, operant.length() - 1);
			}
			// operant is enquoted in ' '
			else if (operant.length() > 2 && operant.charAt(0) == '\'' && operant.charAt(operant.length()-1) == '\'')
			{
				//operant is Date type. Fixed Date format: d MMM yyyy. Eg. '1 Sep 2006'
				SimpleDateFormat dateFormat = new SimpleDateFormat();
				Date dateValue = null;
				for (int i = 0; i < datePattern.length; i++)
				{
					dateFormat.applyPattern(datePattern[i]);
					try
					{
						dateValue = dateFormat.parse(operant.substring(1, operant.length()-1));
						break;
					}
					catch (ParseException pe)
					{
						continue;
					}	
				}
				if (dateValue == null)
				{
					throw new InvalidFormatException("Date format not accepted");
				}
				return dateValue;
			}
			// operant is enquoted in [[ ]]
			else if (operant.length() > 4 && operant.charAt(0) == '[' && operant.charAt(1) == '['
				&& operant.charAt(operant.length()-1) == ']' && operant.charAt(operant.length()-2) == ']')
			{
				// operant is MemberDao attribute value. Check the type of value
				String key = operant.substring(2, operant.length() - 2);
				String value = memberDao.get(key);
				if (value == null)
				{
					throw new ValueNotInitializedException("Value not initialized: " + key);
				}
				
				if (value != null && value.trim().equals(""))	// Return null if value is empty or null
				{					
					return null;
				}
				
				if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) // Boolean value
				{
					return new Boolean(value);
				}
				
				try
				{
					Integer intValue = new Integer(Integer.parseInt(value));	// Integer
					
					return intValue;
				}
				catch (NumberFormatException nfe)
				{
					// Check for float value
					try
					{
						Float floatValue = new Float(Float.parseFloat(value));	// Float
						
						return floatValue;
					}
					catch (NumberFormatException e)
					{
						try
						{
							Date dateValue = new SimpleDateFormat("d MMM yyyy").parse(value); //Fixed
							
							return dateValue;
						}
						catch (ParseException pe)
						{
							// return as litteral string
							return value;
						}						
					}
				}
			}
			// operant is not quoted in any meta character
			else
			{
				operant = operant.trim();				
				if (operant.equalsIgnoreCase("null")) // Check if NULL value
				{
					return null;
				}				
				else if (operant.equalsIgnoreCase("true") || operant.equalsIgnoreCase("false")) // Check if it is boolean value
				{
					return new Boolean(operant);
				}
				else // Check if it is Integer or Float
				{
					try
					{
						Integer intValue = new Integer(Integer.parseInt(operant));
						
						return intValue;
					}
					catch (NumberFormatException nfe)
					{
						// Check for float value
						try
						{
							Float floatValue = new Float(Float.parseFloat(operant));
							
							return floatValue;
						}
						catch (NumberFormatException e)
						{
							throw new InvalidFormatException("Operant format is not of type: Boolean, Integer nor Float");
						}
					} 
				}
				
			}
		}
		else
		{
			throw new InvalidFormatException("Operant is null");
		}		
	}
	
	public static String getString(String key)
	{
		try
		{
			ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("com.bp.pensionline.handler.messages");
			return RESOURCE_BUNDLE.getString(key);
		}
		catch (MissingResourceException e)
		{
			return '!' + key + '!';
		}
	}	
	
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				//LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
			xmlString.append("</MemberData>\n");
			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}
	
	public void exportToFile(String fileName, String content)
	{
		try
		{
			File xmlFile = new File(fileName);
			FileOutputStream xmlOutputStream = new FileOutputStream(xmlFile);
			xmlOutputStream.write(content.getBytes());
			xmlOutputStream.flush();
			xmlOutputStream.close();
		}
		catch (Exception e)
		{
			LOG.info(this.getClass().toString()+ ". Error in exporting file to " + fileName + ": " + e.getMessage());
		}
	}
}
