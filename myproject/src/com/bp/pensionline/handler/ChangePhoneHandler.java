package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.bp.pensionline.constants.Environment;

import com.bp.pensionline.consumer.ChangePhoneConsumer;
import com.bp.pensionline.consumer.ImailConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.ClearMemberCache;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * Servlet that handlers changing phone
 */

public class ChangePhoneHandler extends Handler {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap requestMap = new HashMap();
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		String xmlResponse = null;		
		String bGroup = null;
		String refNo = null;
		String ni = "";
		String name = "";
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null)
		{
			bGroup = (String)currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP);
			refNo = (String)currentUser.getAdditionalInfo(Environment.MEMBER_REFNO);
			
			// Added by Huy to get Ni
			MemberDao memberDao = (MemberDao)currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
			if (memberDao != null)
			{
				if (memberDao.get("Nino") != null)
				{
					ni = memberDao.get("Nino").toLowerCase();
				}
				if (memberDao.get("Name") != null)
				{
					name = memberDao.get("Name");
				}
			}			
		}
		
		
		if (validate(xml)==true && bGroup != null && refNo != null) {
			
			boolean isUpdated = false;
			
			requestMap.put("bGroup", bGroup);
			requestMap.put("refNo", refNo);
			requestMap.put("Form", xml);
			requestMap.put("userName", currentUser.getName());
			
			try {				

				parseRequestXml(xml);
				
				String phone = (String)requestMap.get("phone");
				String mobile = (String)requestMap.get("mobile");
				String fax = (String)requestMap.get("fax");

				//if valid security code - send Imail 
				StringBuffer buf = new StringBuffer();
				buf.append("<ChangePhone>\n");
				buf.append("\t<Name>").append(ni).append("</Name>\n");
				buf.append("\t<Ni>").append(ni).append("</Ni>\n");		
				buf.append("\t<Telephone>").append(phone).append("</Telephone>\n");
				buf.append("\t<Mobile>").append(mobile).append("</Mobile>\n");	
				buf.append("\t<Fax>").append(fax).append("</Fax>\n");	
				buf.append("</ChangePhone>");

				StringBuffer bufMessage = new StringBuffer();
				bufMessage.append("Name: ").append(name);
				bufMessage.append("\nNi: ").append(ni);			
				bufMessage.append("\nTelephone: ").append(phone);
				bufMessage.append("\nMobile: ").append(mobile);
				bufMessage.append("\nFax: ").append(fax);


				//CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
				// Huy modified here
				ImailSQLHandler imailSQLHandler = new ImailSQLHandler();
					
				int maxIMailNo = imailSQLHandler.getMaxIMailNo();
				int newIMailNo = maxIMailNo + 1;

				requestMap.put(Environment.IMAIL_BGROUP, "UNK");
				requestMap.put(Environment.IMAIL_REFNO, refNo);
				requestMap.put(Environment.IMAIL_TEMP, String.valueOf(newIMailNo));
				requestMap.put(Environment.IMAIL_NAME, name);
				
				requestMap.put(Environment.IMAIL_NAME, name);
				requestMap.put(Environment.IMAIL_NI, ni);
				requestMap.put(Environment.IMAIL_SUBJECT, "Change Phone Request");
				requestMap.put(Environment.IMAIL_MESSAGE, bufMessage.toString());
				requestMap.put(Environment.IMAIL_CREATIONDATE, new java.sql.Date(System.currentTimeMillis()));
				requestMap.put(Environment.IMAIL_CASEWORKID, String.valueOf(newIMailNo));
				requestMap.put(Environment.IMAIL_FORMDATA, buf.toString());	
				
				/*
				 * Call Consumer
				 */
				/*
				* Replaced casework by using IMail
				
				ChangePhoneConsumer consumer = new ChangePhoneConsumer(requestMap);
				isUpdated = consumer.processData();
				*/
				
				
				ImailConsumer imailConsumer = new ImailConsumer(requestMap);
				try {
					//System.out.println("imailConsumer.processData");
					imailConsumer.processData();
					isUpdated = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//System.out.println("Error in creating IMail: " + e.toString());
				}	
						
				/*
				 * Respone success or error
				 */
				if(isUpdated == true){					
					xmlResponse = responseSuccess();
					//ClearMemberCache.clearCache(currentUser);
				}				
				if(isUpdated == false){
					xmlResponse = responseError();
				}
				
			} 
			catch (Exception ex) {
				LOG.info(ex.getMessage() + ex.getCause());
			}
		}	

		out.print(xmlResponse);
		out.close();
		requestMap.clear();

	}

	
	public String responseSuccess(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Success").append(">").append("**DONE**").append("</Success").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}
	
	public String responseError(){
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("		<").append("Error").append(">").append("**FAIL**").append("</Error").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();	
	}

	private void parseRequestXml(String xml) {
		/*
		 * Parse XML Request
		 */
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
			document = builder.parse(bais);			
			
			
			Node node = document.getElementsByTagName("TelephoneNumber").item(0);			
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("phone", content.getTextContent());
					}
				}
				else{
					requestMap.put("phone", "");
				}
			}
			
			node = document.getElementsByTagName("MobileNumber").item(0);			
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("mobile", content.getTextContent());					}
				}
				else{
					requestMap.put("mobile", "");
				}
			}
			
			node = document.getElementsByTagName("FaxNumber").item(0);			
			if(node != null){
				Node content = node.getFirstChild();
				if(content != null){
					if(content.getNodeType() == Node.TEXT_NODE){
						requestMap.put("fax", content.getTextContent());
					}
				}	
				else{
					requestMap.put("fax", "");
				}
			}
			bais.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
