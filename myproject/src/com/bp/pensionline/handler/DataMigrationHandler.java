package com.bp.pensionline.handler;

import java.sql.Timestamp;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.MigrationDataHandler;
import com.bp.pensionline.sqlreport.app.jasper.PLSectionProducer;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 11-06-2007
 * 
 */
public class DataMigrationHandler
{
	public static final Log log = CmsLog.getLog(DataMigrationHandler.class);

	private static MigrationDataHandler handler = new MigrationDataHandler();
	
	public static final String LDAP_PREFIX = "bp1ldap@";

	public int migrateUserCheck(String username, String password,
			String surname, String payrollnum)
	{
		int result = 0;
		try
		{
			result = handler.migrateUserDetailsWithCheck(username, password, surname, payrollnum);
			return result;
		}
		catch (Exception e)
		{
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			result = 0;
			return result;
		}

	}

	/**
	 * Migrate user details for existing and new webuser (not system users)
	 * @param username
	 * @param password
	 * @return true if webuser is migrated
	 * false if other
	 */
	public boolean migrateUserDetails(String username, String password, String ninoName) throws Exception
	{
		if (!SystemAccount.checkWebuserExist(username))
		{
			return migrateUserData(username, password, ninoName);
		}
		else
		{
			String pwdTemp = handler.getPasswordForuser(ninoName);
			String pwdTemp2 = handler.encryptPassword(password);

			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null"))
			{
				// if user's password is not null in WE_AUTHENTICATE oracle
				log.info("Check matched password for PL ID " + username);
				if (pwdTemp2.trim().equalsIgnoreCase(pwdTemp.trim()))	
				{
					// table ( in case reset password)
					log.info("Update password for PL ID " + username);
					SystemAccount.updateWebuserPassword(username, password);	// Update password for this user in OpenCms
					handler.setNullPassword(ninoName);					// set password null in WE_AUTHENTICATE table for this user
					
					markWebuserExpried(username);							// Assign this user into FORCE_CHANGE_PASSWORD group	
				}
				else
				{
					throw new Exception("Password not matched");
				}				
			}
			
			// Huy: Refresh user's "RECORDs" in description fields			
			String des = handler.getUserDescription(ninoName);				
			CmsUser user = SystemAccount.getWebuserByName(username);	
			log.info("Description for user " + username + ": " + des);				
			if(user != null && user.isWebuser() && des != null && !des.equals("")) 
			{
				SystemAccount.updateWebuserDescription(username, des);
				// webuser login
				return true;
			}			
		}
		
		return false;
	}
	
	/**
	 * Migrate user details for existing and new webuser (not system users)
	 * @param username
	 * @param password
	 * @return true if webuser is migrated
	 * false if other
	 */
	public boolean migrateUserDetails(String username, String password) throws Exception
	{
		if (!SystemAccount.checkWebuserExist(username))
		{
			return migrateUserData(username, password);
		}
		else
		{
			String pwdTemp = handler.getPasswordForuser(username);
			String pwdTemp2 = handler.encryptPassword(password);

			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null"))
			{
				if (pwdTemp2.trim().equalsIgnoreCase(pwdTemp.trim()))	// if user's password is not null in WE_AUTHENTICATE oracle
				{
					// table ( in case reset password)
					SystemAccount.updateWebuserPassword(username, password);	// Update password for this user in OpenCms
					handler.setNullPassword(username);					// set password null in WE_AUTHENTICATE table for this user
					
					markWebuserExpried(username);							// Assign this user into FORCE_CHANGE_PASSWORD group	
				}
				else
				{
					throw new Exception("Password not matched");
				}				
			}
			
			// Huy: Refresh user's "RECORDs" in description fields			
			String des = handler.getUserDescription(username);				
			CmsUser user = SystemAccount.getWebuserByName(username);	
			log.info("Description for user " + username + ": " + des);				
			if(user != null && user.isWebuser() && des != null && !des.equals("")) 
			{
				SystemAccount.updateWebuserDescription(username, des);
				// webuser login
				return true;
			}			
		}
		
		return false;
	}
	
	/**
	 * Updated by Binh Nguyen on migrating a NINO user that linked 
	 * from BP1 LDAP ID
	 * @param username the NINO gotten from Administrator database
	 * @param password the encrypted password sent from WE_AUTHENTICATE
	 * @return	true if the migration process succeed
	 * @throws Exception the normal exception thrown
	 */
	public boolean migrateUserDetailsBP1User(String username, String password, String ninoName) throws Exception
	{
		if (!SystemAccount.checkWebuserExist(username)) {
			return migrateUserDataBP1User(username, password);
		}
		else {
			String pwdTemp = handler.getPasswordForuser(ninoName);
			String enteredPasswd = password;
			
			if (username.contains(LDAP_PREFIX)) {
				username = username.replace(LDAP_PREFIX, "");
			}
			
			log.info("User name: " + username);
			
			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null")) {
				if ((enteredPasswd.trim().equalsIgnoreCase(pwdTemp.trim()))
						&& (enteredPasswd.length() > 0)) {
					SystemAccount.updateWebuserPassword(username, password);						
				}
			}
					
			String des = handler.getUserDescription(ninoName);				
			CmsUser user = SystemAccount.getWebuserByName(username);	
			log.info("Description for user " + username + ": " + des);				
			if(user != null && user.isWebuser() && des != null && !des.equals("")) {
				log.info("Update user description");
				SystemAccount.updateWebuserDescription(username, des);
				return true;
			}			
		}
		
		return false;
	}
	
	
	
	/**
	 * Migrate user data for webuser only
	 * @param username
	 * @param password
	 * @return
	 */
	private boolean migrateUserData(String username, String password, String ninoName) throws Exception
	{
		if (handler.checkUserAuthenticate(ninoName))
		{
			String pwdTemp = handler.getPasswordForuser(ninoName);	// get user's password from WE_AUTHENTICATE table
			String pwdTemp2 = handler.encryptPassword(password);	// password typed
			String description = handler.getUserDescription(ninoName);
			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null"))
			{
				if (pwdTemp2.trim().equalsIgnoreCase(pwdTemp.trim()))
				{
					Hashtable<String, Timestamp> params = new Hashtable<String, Timestamp>();
					params.put("Add new web users", new Timestamp(System.currentTimeMillis()));
					SystemAccount.createWebUser(username, password, "Members", description, params);
					
					markWebuserExpried(username);			// Assign this user into FORCE_CHANGE_PASSWORD group
					handler.setNullPassword(ninoName);	// set password is null in WE_AUTHENTICATE table for this user
				}
				else
				{
					throw new Exception("Password not matched");
				}				
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Migrates user
	 * @param username the username entered
	 * @param password the password entered
	 * @return true if a web user found
	 * @throws Exception the generic exception
	 */
	private boolean migrateUserData(String username, String password) throws Exception
	{
		if (handler.checkUserAuthenticate(username))
		{
			String pwdTemp = handler.getPasswordForuser(username);	// get user's password from WE_AUTHENTICATE table
			String pwdTemp2 = handler.encryptPassword(password);	// password typed
			String description = handler.getUserDescription(username);
			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null"))
			{
				if (pwdTemp2.trim().equalsIgnoreCase(pwdTemp.trim()))
				{
					Hashtable<String, Timestamp> params = new Hashtable<String, Timestamp>();
					params.put("Add new web users", new Timestamp(System.currentTimeMillis()));
					SystemAccount.createWebUser(username, password, "Members", description, params);
					
					markWebuserExpried(username);			// Assign this user into FORCE_CHANGE_PASSWORD group
					handler.setNullPassword(username);	// set password is null in WE_AUTHENTICATE table for this user
				}
				else
				{
					throw new Exception("Password not matched");
				}				
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Added by Binh Nguyen on migrating a NINO when entering 
	 * system with BP1 LDAP account
	 * @param username the NINO linked to the BP1 ID
	 * @param password the encrypted password pass from Administrator database
	 * @return	true if the migration process is successfully
	 * @throws Exception the normal exception
	 */
	private boolean migrateUserDataBP1User(String username, String password) 
	throws Exception {
		if (handler.checkUserAuthenticate(username)) {
			// get user's password from WE_AUTHENTICATE table
			String pwdTemp = handler.getPasswordForuser(username);
			
			// password typed
			String enteredPasswd = password;
			String description = handler.getUserDescription(username);
			if (pwdTemp != null && !pwdTemp.equalsIgnoreCase("Null")) {
				if ((enteredPasswd != null) 
						&& (!enteredPasswd.equalsIgnoreCase("Null"))) {
					if ((enteredPasswd.trim().equalsIgnoreCase(pwdTemp.trim()))
							&& (enteredPasswd.length() > 0)){
						Hashtable<String, Timestamp> params = new Hashtable<String, Timestamp>();
						params.put("Add new web users", new Timestamp(System.currentTimeMillis()));
						
						if (username.contains(LDAP_PREFIX)) {
							username = username.replace(LDAP_PREFIX, "");
						}
						
						SystemAccount.createWebUser(username, password, "Members", description, params);
					}
					else {
						throw new Exception("Password not matched");
					}	
				} else {
					throw new Exception("User's password is not accepted");
				}		
			}
			
			return true;
		}
		
		return false;
	}

	private void markUserExpried(String username)
	{

		String system = CheckConfigurationKey.getStringValue("systemname");
		/*
		 * String FORCE_PASSWORD_CHANGE
		 * =CheckConfigurationKey.getStringValue("force_password_change_group");
		 */
		UserExpriedHandler.insert(username, Environment.FORCE_PASSWORD_CHANGE,system);
		//CheckUserExpriedHandler.insert(username, FORCE_PASSWORD_CHANGE, system);

	}
	
	private void markWebuserExpried(String username)
	{

		String system = CheckConfigurationKey.getStringValue("systemname");
		/*
		 * String FORCE_PASSWORD_CHANGE
		 * =CheckConfigurationKey.getStringValue("force_password_change_group");
		 */
		UserExpriedHandler.insert(SystemAccount.webUnitName + username, Environment.FORCE_PASSWORD_CHANGE,system);
		//CheckUserExpriedHandler.insert(username, FORCE_PASSWORD_CHANGE, system);

	}	

}
