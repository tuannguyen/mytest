package com.bp.pensionline.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.ImailConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.CaseWorkSQLHandler;
import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

/**
 * This class use to handler Imail Request
 * 
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 04/05/2007
 * 
 * 
 * 
 */
public class ImailHandler extends Handler {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private HashMap valueMap = new HashMap();

	private CmsUser cmsUser = null;

	private String error = null;

	public void vaidate(String param) {
		super.validate(param);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		doProcessRequest(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcessRequest(request, response);

	}

	/**
	 * Process data request
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void doProcessRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String xml = request.getParameter(Constant.PARAM);
		
//		CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
//		int caseMaxNo=caseWorkSQL.getMaxCaseNo()+1;
		// Replace
		ImailSQLHandler imailSQLHandler = new ImailSQLHandler();		
		int maxIMailNo = imailSQLHandler.getMaxIMailNo() + 1;		

		try {
			cmsUser = SystemAccount.getCurrentUser(request);
			String bGroup = null;
			String refNo = null;
			String cmsUserName=null;
			StringBuffer cmsuAddress=new StringBuffer(0);
			if (cmsUser != null) {
				MemberDao dao=(MemberDao)cmsUser.getAdditionalInfo(Environment.MEMBER_KEY);
				cmsUserName=cmsUser.getFullName();
				bGroup = String.valueOf(dao.get(Environment.MEMBER_BGROUP));
				refNo = String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_REFNO));
				
				cmsuAddress.append(dao.get("Address_House")).append(dao.get("Address_Street")).append(dao.get("Address_Town")).append(dao.get("Address_County")).append(dao.get("Address_Postcode")).append(dao.get("Address_Country"));
				
				valueMap.put(Environment.IMAIL_BGROUP, bGroup);

				valueMap.put(Environment.IMAIL_REFNO, refNo);
				valueMap.put(Environment.IMAIL_TEMP, maxIMailNo);
				valueMap.put(Environment.IMAIL_NAME, cmsUserName);
				valueMap.put(Environment.IMAIL_ADDRESS, cmsuAddress.toString());

			}

			if (prepareData(xml)){
				ImailConsumer imailConsumer = new ImailConsumer(valueMap);

				imailConsumer.processData();
			}

			

		} catch (Exception e) {
			// TODO: handle exception
			

			LOG.error(e.getMessage() +e.getCause());
		}
		if (error == null) {

			out
					.print("<div id=\"SUCCESS\"> Thank you for your response, your query will be processed</div>");
			out.close();

		} else {
			
			out
					.print("<div id=\"ERROR\"> We were unable to process your request at this time</div>");
			out.close();

		}

	}

	/**
	 * Prepared data to valueMap
	 * 
	 * @param param
	 * @throws Exception
	 */
	protected boolean  prepareData(String param) throws Exception {
		boolean isTrue=false;
		try {
//			CaseWorkSQLHandler caseWorkSQL = new CaseWorkSQLHandler();
//			int caseMaxNo=caseWorkSQL.getMaxCaseNo()+1;
			// Replace by using only IMail
			ImailSQLHandler imailSQLHandler = new ImailSQLHandler();			
			int maxIMailNo = imailSQLHandler.getMaxIMailNo();

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();

			ByteArrayInputStream byteArr = new ByteArrayInputStream(param
					.getBytes());

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(byteArr);

			doc.getDocumentElement().normalize();

			NodeList listOfMembers = doc.getElementsByTagName("Imail");

			for (int s = 0; s < listOfMembers.getLength(); s++) {

				Node firstNode = listOfMembers.item(s);
				if (firstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element firstElement = (Element) firstNode;
					valueMap.put(Environment.IMAIL_NAME, getNodeValue(firstElement,
							Environment.IMAIL_NAME));

					valueMap.put(Environment.IMAIL_NI, getNodeValue(firstElement,
							Environment.IMAIL_NI));

					
					valueMap.put(Environment.IMAIL_EMAILADRESS, getNodeValue(
							firstElement, Environment.IMAIL_EMAILADRESS));

					valueMap.put(Environment.IMAIL_SUBJECT, getNodeValue(
							firstElement, Environment.IMAIL_SUBJECT));

					valueMap.put(Environment.IMAIL_QUERY, getNodeValue(
							firstElement, Environment.IMAIL_QUERY));

					valueMap.put(Environment.IMAIL_MESSAGE, getNodeValue(
							firstElement, Environment.IMAIL_QUERY));

					valueMap.put(Environment.IMAIL_CREATIONDATE, new java.sql.Date(
							System.currentTimeMillis()));

					valueMap.put(Environment.IMAIL_CASEWORKID, maxIMailNo);

					valueMap.put(Environment.IMAIL_FORMDATA,
							prepareFormDataFieldValue());

				}
			}
			isTrue=true;
		}catch (Exception e) {
			// TODO: handle exception
			isTrue=false;
			
			LOG.error(e.getMessage() +e.getCause());
		}
		return isTrue;
		

	}

	protected String getNodeValue(Element element, String tagName)
			throws Exception {
		NodeList nodeList = element.getElementsByTagName(tagName);
		Element tagNameElement = (Element) nodeList.item(0);
		NodeList textList = tagNameElement.getChildNodes();

		if (((Node) textList.item(0)) != null) {

			return ((Node) textList.item(0)).getNodeValue().trim();
		} else {
			return new String("null");
		}

	}

	private String prepareFormDataFieldValue() throws Exception {
		StringBuffer buf = new StringBuffer();
		buf.append("<iMail>");
		buf.append("<Bgroup>").append(valueMap.get(Environment.IMAIL_BGROUP))
				.append("</Bgroup>");
		buf.append("<Refno>").append(valueMap.get(Environment.IMAIL_REFNO))
				.append("</Refno>");
		buf.append("<NI>").append(valueMap.get(Environment.IMAIL_NI)).append(
				"</NI>");
		buf.append("<CreationDate>").append(
				valueMap.get(Environment.IMAIL_CREATIONDATE)).append(
				"</CreationDate>");
		buf.append("<CaseworkId>").append(
				valueMap.get(Environment.IMAIL_CASEWORKID)).append(
				"</CaseworkId>");
		buf.append("<Subject>").append(valueMap.get(Environment.IMAIL_SUBJECT))
				.append("</Subject>");
		buf.append("<Message>").append(valueMap.get(Environment.IMAIL_MESSAGE))
				.append("</Message>").append("</iMail>");
		return buf.toString();

	}

}
