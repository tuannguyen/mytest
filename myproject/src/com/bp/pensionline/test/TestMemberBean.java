package com.bp.pensionline.test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TestMemberBean {
	
	public TestMemberBean(byte[] arr){	
		loadMembers(arr);		
	}
	
	private ArrayList<TestMember> members = null;
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private void loadMembers(byte[] arr){
		
		members = new ArrayList<TestMember>();

		TestMember tester = null;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
		DocumentBuilder builder = null;        
        
        Document document = null;

	
			try{
				ByteArrayInputStream is = new ByteArrayInputStream(arr);
				
				if(is != null){

				builder = factory.newDocumentBuilder();
				
				document = builder.parse(is);
				
				Element root = document.getDocumentElement();
				
				NodeList children = root.getChildNodes();				
				
				int cLength = children.getLength();
				
				for(int i=0; i< cLength; i++){
					
					Node mem = children.item(i);
					if(mem.getNodeType() == Node.ELEMENT_NODE){
						
						NodeList atts = mem.getChildNodes();
						
						int aLength = atts.getLength();
						tester = new TestMember();

						for (int j = 0; j < aLength; j++) {

							Node att = atts.item(j);
							if (att.getNodeType() == Node.ELEMENT_NODE
									&& att.getNodeName() == "Filename") {

								tester.setFileName(att.getTextContent().trim());

							} else if (att.getNodeType() == Node.ELEMENT_NODE
									&& att.getNodeName() == "Description") {

								tester.setDescription(att.getTextContent().trim());

							} else if (att.getNodeType() == Node.ELEMENT_NODE
									&& att.getNodeName() == "Bgroup") {

								tester.setBgroup(att.getTextContent().trim());

							} else if (att.getNodeType() == Node.ELEMENT_NODE
									&& att.getNodeName() == "Refno") {

								tester.setRefno(att.getTextContent().trim());

							}
						}

						members.add(tester);
					}
				}
				}
			}catch(Exception ex){				
				LOG.info(ex.getMessage() + ex.getCause());
			}			
	}
	public ArrayList getMembers() {
		return members;
	}
}
