package com.bp.pensionline.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opencms.file.CmsUser;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.factory.XmlDaoFactory;
import com.bp.pensionline.util.SystemAccount;


public class TestMemberLoader extends HttpServlet {
	
	String description = null;
	String fileName = null;
	String translatedDescription = null;	
	
	
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {    	   	
    	XmlReader reader = new XmlReader();    	
    	byte[] cont = reader.readFile(Environment.TESTMEMBERMAPPER_FILE);    	
    	TestMemberBean memberBean = new TestMemberBean(cont);
    	ArrayList members = memberBean.getMembers();    	
    	description = request.getParameter("description");
    	
    	CmsUser currentUser = SystemAccount.getCurrentUser(request);
    	
    	if(description == null)
    		return;
    	description = description.trim();	
    	//description is not point out previously, then take it on session
    	if(description.length() == 0){    		
    		///Object obj = request.getSession().getAttribute(Environment.MEMBER_DESCRIPTION);
    		Object obj = currentUser != null? currentUser.getAdditionalInfo(Environment.MEMBER_DESCRIPTION) : null;
    		description = obj != null ? String.valueOf(obj) : null;    		
    	}
    	else{	    		
    		//request.getSession().setAttribute(Environment.MEMBER_DESCRIPTION, description);
    		//request.getSession().setAttribute(Environment.MEMBER_BGROUP, null);
    		//request.getSession().setAttribute(Environment.MEMBER_REFNO, null);
    		if(currentUser != null) currentUser.setAdditionalInfo(Environment.MEMBER_DESCRIPTION, description) ;
    		if( currentUser != null) currentUser.setAdditionalInfo(Environment.MEMBER_BGROUP, null);
    		if( currentUser != null) currentUser.setAdditionalInfo(Environment.MEMBER_REFNO, null);
    	}    	
    	//get file name for MemberDAO, then store MememberDAO to the session
    	if(description == null && members != null && members.size() > 0){
    		description = ((TestMember)members.get(0)).getDescription();
    		fileName = ((TestMember)members.get(0)).getFileName();
    	}
    	else{
    		
    		Iterator iter = members.iterator();
    		for(;iter.hasNext(); ){
    			
    			TestMember mem = (TestMember)iter.next();
    			
    			if(mem.getDescription().equals(description)){  				

    				// test  member data would be load from xml file
    				// so that do not call DataAccess, would call directly XmlDaoFactory to get XmlMemberDao
    				
    				//MemberDao memberDao =  DataAccess.getDaoFactory().getMemberDao(null, null, description);
    				MemberDao memberDao =  new XmlDaoFactory().getMemberDao(null, null, description);
    				
    				//request.getSession().setAttribute(Environment.MEMBER_KEY, memberDao);
    				if(currentUser != null) currentUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
    				
    				break;
    			}	    			
    		}	    		
    	}
    	//save selected description to session for later use    	
    	//request.getSession().setAttribute(Environment.MEMBER_DESCRIPTION, description);
    	if(currentUser != null) currentUser.setAdditionalInfo(Environment.MEMBER_DESCRIPTION, description);
    	
    	response.setContentType(Constant.CONTENT_TYPE);    	
    	String xml = "";    	
    	//take the xml return
    	if(members != null){
    		xml = generateXml(members);
    		response.getWriter().println(xml);
    	}
    }    
    
    private String generateXml(ArrayList list){
    	
		StringBuffer buffer = new StringBuffer();
        buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");    	
    	Iterator iter = list.iterator();    	
    	for(;iter.hasNext();){
    		
    		TestMember mem = (TestMember)iter.next();
    		buffer.append("     <").append(Constant.MEMBER).append(">\n");
    		if(mem.getDescription().equalsIgnoreCase(description)){
    			
    			buffer.append("         <").append(Constant.SELECTED).append(">").append(Constant.SELECTED_TRUE).append("</").append(Constant.SELECTED).append(">\n");
    			
    		}else{
    			
    			buffer.append("         <").append(Constant.SELECTED).append(">").append(Constant.SELECTED_FALSE).append("</").append(Constant.SELECTED).append(">\n");
    			
    		}
            buffer.append("         <").append(Constant.FILENAME).append(">").append(mem.getFileName()).append("</").append(Constant.FILENAME).append(">\n");
            buffer.append("         <").append(Constant.DESCRIPTION).append(">").append(mem.getDescription()).append("</").append(Constant.DESCRIPTION).append(">\n");
            buffer.append("         <").append(Constant.BGROUP).append(">").append(mem.getBgroup()).append("</").append(Constant.BGROUP).append(">\n");
            buffer.append("         <").append(Constant.REFNO).append(">").append(mem.getRefno()).append("</").append(Constant.REFNO).append(">\n");
            buffer.append("     </").append(Constant.MEMBER).append(">\n");
    	}
        buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
    	return buffer.toString();    	
    }    
   
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }    
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Short description";
    } 
	
}
