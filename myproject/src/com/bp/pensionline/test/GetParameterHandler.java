package com.bp.pensionline.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;

import com.bp.pensionline.util.SystemAccount;

/**
 * 
 * @author SonNT
 * @version 2.0
 * @since 2007/5/5
 *
 */
public class GetParameterHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private static final long serialVersionUID = 1L;
	
	private static final String AJAX_REQUEST_PAGE_TAG = "Page";
	
	private String tagName;	

	private String updateField;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		//get request
		String xml = request.getParameter(Constant.PARAM);

		CmsUser currentUser = SystemAccount.getCurrentUser(request);

		List record = null;

		if (currentUser != null) {
			record = (List)currentUser.getAdditionalInfo(Environment.RECORD_LIST);
		}

		String xmlResponse = null;

		if (parseRequestXml(xml)) {

			if (currentUser != null) {
				try {
					// check for multiple period of service
					if (tagName.equals("MultiplePeriodsOfService")) {
						if (record != null && record.size() > 1) {
							xmlResponse = this.getXmlData(tagName, updateField, "true");

						} else {
							xmlResponse = this.getXmlData(tagName, updateField, "false");
						}
					} 
					else{

						String valueItem = "";
						MemberDao memberDao = null;

						if (currentUser != null) {
							memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);							
						}

						if (memberDao != null) {							
							
							valueItem = memberDao.getMemberData(tagName);							
							
							//LOG.info(this.getClass().toString() + ".processRequest: " + tagName+ " = " + valueItem);
							if (valueItem != null && !valueItem.trim().equals("")) { // HUY: checking empty value
								xmlResponse = this.getXmlData(tagName,updateField, valueItem);
							} else {
								xmlResponse = this.getXmlDataError(tagName, updateField);
							}
						} 
						else{
							xmlResponse = this.getXmlDataError(tagName, updateField);
						}
					}

				} catch (Exception ex) {
					xmlResponse = this.getXmlDataError(tagName, updateField);
					LOG.info(ex.getMessage() + ex.getCause());
				}
			} else {
				xmlResponse = this.getXmlDataError(tagName, updateField);

			}
		} else {
			xmlResponse = this.getXmlDataError(tagName, updateField);
		}

		out.print(xmlResponse);
		out.close();

	}

	public String getXmlData(String tagName, String updateField,
			String valueItem) {

		//valueItem = valueItem.trim();
		if (valueItem == null)
		{
			valueItem = "";
		}

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.TYPE).append(">\n");
		buffer.append("             <").append("Text").append("/>\n");
		buffer.append("         </").append(Constant.TYPE).append(">\n");
		buffer.append("         <").append(Constant.VALUE).append(">").
				append("<![CDATA[").append(valueItem).append("]]>").
				append("</").append(Constant.VALUE).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		
		return buffer.toString();
	}

	private String getXmlDataError(String tagName, String updateField) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE).append(
				">[[" + tagName + "]]</").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();

	}

	private boolean parseRequestXml(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equals(Constant.REQUESTED_TAG)) {
						tagName = node.getTextContent();
					}
					if (node.getNodeName().equals(Constant.UPDATE_FIELD)) {
						updateField = node.getTextContent();
					}
				}
			}
			bais.close();

			return true;

		} catch (Exception ex) {
			tagName = "";
			updateField = "";
		}

		return false;

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
			xmlString.append("</MemberData>\n");
			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}
	
	public void exportToFile(String fileName, String content)
	{
		try
		{
			File xmlFile = new File(fileName);
			FileOutputStream xmlOutputStream = new FileOutputStream(xmlFile);
			xmlOutputStream.write(content.getBytes());
			xmlOutputStream.flush();
			xmlOutputStream.close();
		}
		catch (Exception e)
		{
			LOG.info(this.getClass().toString()+ ". Error in exporting file to " + fileName + ": " + e.getMessage());
		}
	}
}
