/*
 * Constant.java
 *
 * Created on March 15, 2007, 2:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.test;

/**
 *
 * @author mall
 */
public class Constant {
    
    /** Creates a new instance of Constant */
    public static final String REQUESTED_TAG = "RequestedTag";
    public static final String UPDATE_FIELD = "UpdateField";   
    public static final String AJAX_PARAMETER_REQUEST = "AjaxParameterRequest";
    public static final String AJAX_PARAMETER_RESPONSE = "AjaxParameterResponse";
    public static final String DATA = "Data";
    public static final String MEMBER = "Member";
    public static final String BGROUP = "Bgroup";
    public static final String REFNO = "Refno";
    public static final String TYPE = "Type";
    public static final String TYPE_DATE = "Date";
    public static final String TYPE_CURRENCY = "Currency";
    public static final String TYPE_TEXT = "Text";
    public static final String TYPE_NUMBER = "Number";
    public static final String VALUE = "Value";
    public static final String ERROR = "Error";
    public static final String MESSAGE = "Message";
    
    public static final String TYPE_CONDITION = "CONDITIONAL";
    public static final String TYPE_HTMLTABLE = "TABLE";   
    
    public static final String PARAM = "xml";
    public static final String CONTENT_TYPE = "text/xml";
    
    public static final String DESCRIPTION = "Description";
    public static final String FILENAME = "Filename";
    public static final String REFPAGE = "Refpage";
    public static final String SELECTED = "Selected";
    public static final String SELECTED_TRUE = "True";
    public static final String SELECTED_FALSE = "False";
    public static final String DELIMITER = " `~!@#$%^&*()-+=\\|{}[]:;'\"<,>.?/";
    public static final String FISCAL_YEAR = "FiscalYear";
    
    
    public static final String OLDPASSWORD_NODE = "OldPassword";
    public static final String NEWPASSWORD_NODE = "NewPassword";
    public static final String CONFIRMPASSWORD_NODE = "ConfirmPassword";
    

    public static final String SUBCRIBEOPTION_NOTE = "SubscribeOption";
    
    

}
