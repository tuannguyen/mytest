package com.bp.pensionline.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.factory.DataAccess;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;
/**
 * 
 * @author SonNT
 * @version 2.0
 * @since 2007/5/5
 *
 */
public class BookmarkMember extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		
		boolean f = false;

		String description = request.getParameter("description");
		String refPage = request.getParameter("refpage");
		response.setContentType(Constant.CONTENT_TYPE);

		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String bGroup = null;
		String refNo = null;
		if(currentUser != null){
			bGroup = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_BGROUP));
			refNo = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_REFNO));
		}
		//String bGroup = (String)request.getSession().getAttribute(Environment.MEMBER_BGROUP);
		//String refNo = (String)request.getSession().getAttribute(Environment.MEMBER_REFNO);

		//Create new instance of MemberDao to get file content
		MemberDao memberDao = DataAccess.getDaoFactory().getMemberDao(bGroup,
				refNo, null);

		try {// Append Member's info to TestMemberMapper.xml					
			f = AppendNewMember(bGroup, refNo, description, refPage);
		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}

		//Write Member's info to xml file in VFS folder:			
		description = StringUtil.Escape(description) + ".xml";
		String fileName = description;
		try {
			int type = 1;
			// 1 = page, 3 = plain, 5 = binary, 8 = jsp	        	
			CmsObject obj = SystemAccount.getAdminCmsObject();
			obj.getRequestContext().setSiteRoot(
					OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			obj.getRequestContext().setCurrentProject(
					obj.readProject("Offline"));
			
			
			
			CmsFile newFile = (CmsFile) obj.createResource(Environment.XMLFILE_DIR			
					+ fileName, type, memberDao.toXML().getBytes(), null);
			
			
			
			obj.writeFile(newFile);
		} catch (Exception ex) {
			
		}

		//Alert to user that the mapper file is updated or not 
		if (f == true) {
			response.getWriter().print("true");
		} else {
			response.getWriter().print("false");
		}

	}

	private boolean AppendNewMember(String _bgroup, String _refno,
			String _description, String _refpage) {
		boolean isUpdated = false;
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = null;

			//Check if the file existes for not
			CmsObject obj = SystemAccount.getAdminCmsObject();
			obj.getRequestContext().setSiteRoot(
					OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			obj.getRequestContext().setCurrentProject(
					obj.readProject("Offline"));
			boolean boo = obj.existsResource(Environment.TESTMEMBERMAPPER_FILE);

			if (boo) {//if true
				try {

					//Load XML file from VFS Folder						

					CmsFile mapperFile = obj.readFile(Environment.TESTMEMBERMAPPER_FILE);

					byte[] arr = mapperFile.getContents();
					ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
					document = builder.parse(bIn);

					NodeList rootList = document
							.getElementsByTagName("TestMemberMapper");

					Node root = rootList.item(0);
					NodeList memList = root.getChildNodes();

					//Check if the memeber already exist in the map or not   
					for (int i = 0; i < memList.getLength(); i++) {

						org.w3c.dom.Node node = memList.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {

							NodeList cList = node.getChildNodes();
							org.w3c.dom.Node BG = cList.item(0);
							org.w3c.dom.Node RN = cList.item(1);
							org.w3c.dom.Node DE = cList.item(3);

							if (BG.getTextContent().equals(_bgroup)
									&& RN.getTextContent().equals(_refno)
									|| DE.getTextContent().equals(_description)) {
								isUpdated = true;
								root.removeChild(node);
								String oldDescription = StringUtil.Escape(DE
										.getTextContent());
								delXML(Environment.XMLFILE_DIR + oldDescription + ".xml");
								i = memList.getLength();
							} else {//do nothing									
							}
						} else {
						}
					}
					//try to append member
					Node newMem = document.createElement("TestMember");
					root.appendChild(newMem);

					Node bgroup = document.createElement("Bgroup");
					newMem.appendChild(bgroup);
					Node BG = document.createTextNode(_bgroup);
					bgroup.appendChild(BG);

					Node refno = document.createElement("Refno");
					newMem.appendChild(refno);
					Node RN = document.createTextNode(_refno);
					refno.appendChild(RN);

					Node refpage = document.createElement("Filename");
					newMem.appendChild(refpage);
					Node FN = document.createTextNode(_refpage);
					refpage.appendChild(FN);

					Node description = document.createElement("Description");
					newMem.appendChild(description);
					Node DE = document.createTextNode(_description);
					description.appendChild(DE);

					//Replace fileMapper to VFS folder
					delXML(Environment.TESTMEMBERMAPPER_FILE);
					toXML(document, Environment.TESTMEMBERMAPPER_FILE);
				} catch (Exception exc) {

					LOG.info(exc.getMessage() + exc.getCause());
				}
			} else {//if false

				//Create new document					            
				document = builder.newDocument();

				Node root = document.createElement("TestMemberMapper");
				document.appendChild(root);

				Node newMem = document.createElement("TestMember");
				root.appendChild(newMem);

				Node bgroup = document.createElement("Bgroup");
				newMem.appendChild(bgroup);
				Node BG = document.createTextNode(_bgroup);
				bgroup.appendChild(BG);

				Node refno = document.createElement("Refno");
				newMem.appendChild(refno);
				Node RN = document.createTextNode(_refno);
				refno.appendChild(RN);

				Node refpage = document.createElement("Filename");
				newMem.appendChild(refpage);
				Node FN = document.createTextNode(_refpage);
				refpage.appendChild(FN);

				Node description = document.createElement("Description");
				newMem.appendChild(description);
				Node DE = document.createTextNode(_description);
				description.appendChild(DE);

				toXML(document, Environment.TESTMEMBERMAPPER_FILE);
				isUpdated = true;
			}

		} catch (Exception cms) {

		}
		return isUpdated;
	}

	public static void delXML(String fileName) {
		//    	Delete old XML file from VFS folder
		try {
			CmsObject obj = SystemAccount.getAdminCmsObject();
			obj.getRequestContext().setSiteRoot(
					OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			obj.getRequestContext().setCurrentProject(
					obj.readProject("Offline"));
			obj.lockResource(fileName);
			obj.deleteResource(fileName, 0);
		} catch (Exception ex) {
		}
	}

	public static void toXML(Document document, String fileName) {
		try {
			Source source = new DOMSource(document);

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Result result = new StreamResult(out);

			Transformer xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.transform(source, result);

			byte[] cont = out.toByteArray();

			//          Write new XML file to VFS folder
			int type = 1;// 1 = page, 3 = plain, 5 = binary, 8 = jsp
			CmsObject obj = SystemAccount.getAdminCmsObject();
			obj.getRequestContext().setSiteRoot(
					OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			obj.getRequestContext().setCurrentProject(
					obj.readProject("Offline"));
			CmsFile newFile = (CmsFile) obj.createResource(fileName, type,
					cont, null);
			obj.writeFile(newFile);
		} catch (Exception ex) {
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
