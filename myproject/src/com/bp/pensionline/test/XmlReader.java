package com.bp.pensionline.test;

import java.io.FileNotFoundException;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.bp.pensionline.util.SystemAccount;

public class XmlReader{
	
	public static final Log LOG = CmsLog.getLog(XmlReader.class);
	
	public  XmlReader(){		
	}
	public byte[] readFile(String fileName) throws FileNotFoundException {
		//LOG.info("readFile():BEGIN");

		try{
			//LOG.info("FILE: "+fileName);
			//get Admin permission to read file from VFS folder
			CmsObject cmsObj = SystemAccount.getAdminCmsObject();
			//System.out.println("CmsObj: " + cmsObj);//just for trace
			cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			
			cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
			
			boolean exist = cmsObj.existsResource(fileName);
			
			//check if the file is exist or not
			if(exist){//if true
				
				//read file and return a byte array
				CmsFile xmlFile = (CmsFile)cmsObj.readFile(fileName);		
				byte[] arr = xmlFile.getContents();
				//LOG.info("readFile():END : " + arr);
				return arr;
				
			}
			else{
				//throw exception file not found
				FileNotFoundException ex = new FileNotFoundException("com.bp.pensionline.test.XMLReader can not find " + fileName);
				throw ex;				
			}
		}
		catch(FileNotFoundException fnfe){
			
			throw fnfe;
			
		}
		catch(Exception ex){
			LOG.error("Error in reading file from CMS: " + ex.toString());
			ex.printStackTrace();
			return null;
		}
	}
}
