package com.bp.pensionline.test;

public class TestMember {

	public TestMember(){
		
	}
	public TestMember(String fileName, String description, String bgroup, String refno){		
		this.fileName = fileName;
		this.description = description;
		this.bgroup = bgroup;
		this.refno = refno;
	}
	private String fileName;
	private String description;
	private String bgroup;
	private String refno;
	public String getBgroup() {
		return bgroup;
	}
	public void setBgroup(String bgroup) {
		this.bgroup = bgroup;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRefno() {
		return refno;
	}
	public void setRefno(String refno) {
		this.refno = refno;
	}
	
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("\nMember:");
		buffer.append("\n\tFileName = ").append(fileName);
		buffer.append("\n\tDescription = ").append(description);
		buffer.append("\n\tBgroup = ").append(bgroup);
		buffer.append("\n\tRefno = ").append(refno);
		return buffer.toString();
	}
	
}
