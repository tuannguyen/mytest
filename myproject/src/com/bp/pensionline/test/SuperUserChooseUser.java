package com.bp.pensionline.test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.opencms.main.CmsSessionInfo;
import org.opencms.util.CmsUUID;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.database.DBMemberConnectorExt;
import com.bp.pensionline.exception.MemberNotFoundException;
import com.bp.pensionline.factory.DataAccess;
import com.bp.pensionline.handler.CompanySchemeRawHandler;
import com.bp.pensionline.handler.SwapUserHandler;
import com.bp.pensionline.handler.WallHandler;
import com.bp.pensionline.util.SystemAccount;

/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 *
 */
public class SuperUserChooseUser extends HttpServlet {

	private static final long serialVersionUID = 1L;	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private String bGroup = "";

	private String refNo = "";

	/**
	 * Load a member based on current user access level. Modified by HuyTran 12/07/2010
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		LOG.info("SuperUserChooseUser.processRequest():BEGIN");
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		String xml = request.getParameter(Constant.PARAM);
		
//		//String accDescription = (String)request.getSession().getAttribute(Environment.MEMBER_ACCESS_LEVEL);
//		String accDescription = null;
		String cmsusnName=null;
		
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		if(currentUser != null)
		{
			//accDescription = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL)); 
			cmsusnName=currentUser.getName();			
		}

		if (parseRequestXml(xml)) {
			LOG.info("Start retrieving user");
			int accessLevel = getCurrentUserSecurityIndicator(request);

			// Try to load Security Indicators of member and superUser
			RecordDao recordDao = null;
			
			try {
				recordDao = DataAccess.getDaoFactory().getRecordDao(bGroup, refNo);
				
				int usrIndicator = -1;
				try
				{
					usrIndicator = Integer.parseInt(recordDao.getSI());
				}
				catch (NumberFormatException nfe)
				{
					LOG.info("Member SI is null or empty!");
				}
				
				if (accessLevel < 0)
				{
					// Alert to superUser that he/she dose not have the
					// permission to load this member
					LOG.info("Description for superuser not valid. Access Level: " + accessLevel);
					doReturn(out, getXmlMess(Constant.ERROR,"Your membership sensitivity setting is not recognised by the system.\nPlease see a member of the PensionLine support team"));
					return;
				}
				
				//    						
				// Compare Security Indicators
				if (accessLevel >= usrIndicator) 
				{

					// If superuser has enough access level - set bGroup
					// and refNo
					
					//request.getSession().setAttribute(Environment.MEMBER_BGROUP, bGroup);
					//request.getSession().setAttribute(Environment.MEMBER_REFNO, refNo);
					currentUser.setAdditionalInfo(Environment.MEMBER_BGROUP, bGroup);
					currentUser.setAdditionalInfo(Environment.MEMBER_REFNO, refNo);
					
					MemberDao memberDao = DataAccess.getDaoFactory().getMemberDao(bGroup, refNo, null);
					
					LOG.info("MEMBER_STATUSRAW: " + memberDao.get(Environment.MEMBER_STATUSRAW));
					
					if (memberDao.get(Environment.MEMBER_STATUSRAW).equalsIgnoreCase("NL")){// if member has status as NL asign to WALL_ALL_ACCESS and Superusers group then return
						String acceptGroup[]={"Superusers","WALL_ALL_ACCESS"};
						SwapUserHandler.getInstance().unSwapUser(cmsusnName, acceptGroup, SystemAccount.getAdminCmsObject());
						doReturn(out, getXmlMess(Constant.MESSAGE, "GO"));
						
						//Put member data for NL records
						
						memberDao.set("SessionId", ((CmsUUID)request.getSession().getAttribute(CmsSessionInfo.ATTRIBUTE_SESSION_ID)).getStringValue());
						currentUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
						
//						CalcProducerFactory producerFactory = new CalcProducerFactory();
//						producerFactory.setMemberDao(memberDao);
//						producerFactory.start();
						
						return;
						
					}else {// else assign to groups that member with bgroup,refno belong then load data for this member
						//store sessionId key in memberDao, it is important to get current user from OpenCMS
						memberDao.set("SessionId", ((CmsUUID)request.getSession().getAttribute(CmsSessionInfo.ATTRIBUTE_SESSION_ID)).getStringValue());
						currentUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
						
//						 run calculation 
						// Huy modified
						//CalcProducerFactory.getCalcProducer(memberDao);
						
//						CalcProducerFactory producerFactory = new CalcProducerFactory();
//						producerFactory.setMemberDao(memberDao);
//						producerFactory.start();
						//--- end of modify
						
						// BEGIN NEW: Assign SuperUser to Member's group (modify By Nguyen Duy Thao)
						String schemeRaw=memberDao.get(Environment.MEMBER_SCHEME_RAW);
						String companyGroup=CompanySchemeRawHandler.getInstance().getCompanyGroup(bGroup, schemeRaw);/*combination from Member's Bgroup and SchemeRaw 
						eg:BPF and 001 then companygroup is BP, see more on /system/modules/com.bp.pensionline.test_members/CompanySchemeMap.xml*/																															
						
						if (companyGroup!=null && companyGroup.length()>0){
							String fgToAssign="MEM_"+companyGroup+"_"+memberDao.get(Environment.MEMBER_STATUSRAW);//eg: MEM_BP_AC
							String sgToAssign="MEM_" +memberDao.get(Environment.MEMBER_STATUSRAW);//eg: MEM_AC
							String tgToAssign="MEM_" +companyGroup;//eg: MEM_BP
							String [] acceptGroup={"Superusers",fgToAssign,sgToAssign,tgToAssign};// all groups asign to superuser
							SwapUserHandler.getInstance().unSwapUser(cmsusnName, acceptGroup, SystemAccount.getAdminCmsObject());
							WallHandler.removeUserFromWallBlock(cmsusnName, bGroup, refNo);
							WallHandler.addUserToWallBlock(cmsusnName, bGroup, refNo);
							
							
						}else {
							
								String sgToAssign="MEM_" +memberDao.get(Environment.MEMBER_STATUSRAW);
								String [] acceptGroup={"Superusers",sgToAssign};
								SwapUserHandler.getInstance().unSwapUser(cmsusnName, acceptGroup, SystemAccount.getAdminCmsObject());
								WallHandler.removeUserFromWallBlock(cmsusnName, bGroup, refNo);
								WallHandler.addUserToWallBlock(cmsusnName, bGroup, refNo);
						}
						
						try
						{
							MemberDao memberDaoExt = (MemberDao)memberDao.clone();
							
							memberDaoExt.setCmsUUID(memberDao.getCmsUUID());
							memberDaoExt.setCurrentUser(memberDao.getCurrentUser());
							
							LOG.info("Superuser load extension data of member start...");
							DBMemberConnectorExt dbMemberConnectorExt = new DBMemberConnectorExt();
							dbMemberConnectorExt.setMemberDao(memberDaoExt);
							dbMemberConnectorExt.start();
						}
						catch (Exception e)
						{
							LOG.error("Error while getting extension data for this member: " + bGroup + "-" + refNo);
						}
						
						LOG.info("Superuser load member done");
						
						doReturn(out, getXmlMess(Constant.MESSAGE, "GO"));
						return;
					}

					//END NEW
					
					
				} else {

					// Alert to superUser that he/she dose not have the
					// permission to load this member
					LOG.info("Access Level is not enough to load this Member. accessLevel: " + accessLevel + ". userIndicator: " + usrIndicator);
					//doReturn(out, getXmlMess(Constant.ERROR,"Access Level is not enough to load this Member"));
					doReturn(out, getXmlMess(Constant.ERROR,"The sensitivity indicator for this member is higher than the level you have been granted.\nPlease see a member of the PensionLine support team or a team leader for more details"));
					return;
					
				}

			}catch(MemberNotFoundException mnfe){
				
				LOG.error("Can not find this member: " + mnfe.toString());
				// Alert to superUser that the member not found
				doReturn(out, getXmlMess(Constant.ERROR, "This member does not exist."));								
				
			}
			catch (Exception e) {
				LOG.error("General error in loading member: " + e);
				//e.printStackTrace();
				doReturn(out, getXmlMess(Constant.ERROR, "Error while loading the member! Please try again later."));
			}
		}
	}
	
	private void doReturn(PrintWriter out, String message){
//		LOG.info(message);
//		LOG.info("SuperUserChooseUser.processRequest():END");
//		LOG.info(message);
//		LOG.info("SuperUserChooseUser.processRequest():END");
		out.write(message);
		out.close();
	}
	
	public String getXmlMess(String type, String mess) {

		if (type.length() > 0 && mess.length() > 0) {

			StringBuffer buffer = new StringBuffer();

			buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
					">\n");
			buffer.append("	<").append(type).append(">").append(mess).append(
					"</").append(type).append(">\n");
			buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE)
					.append(">\n");

			return buffer.toString();
		} else {
			return null;
		}
	}

	private boolean parseRequestXml(String xml) {
		LOG.info("parseRequestXml():BEGIN");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equals(Constant.BGROUP)) {
						bGroup = node.getTextContent();
					} else if (node.getNodeName().equals(Constant.REFNO)) {
						refNo = node.getTextContent();
					}
				}
			}
			bais.close();
			LOG.info("parseRequestXml():END");
			return true;

		} catch (Exception ex) {
			bGroup="";
			refNo="";
		}
		return false;

	}
	
	public int getCurrentUserSecurityIndicator (HttpServletRequest request)
	{
		int securityIndicator = -1;	// Default to all report runner
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		
		if(currentUser != null)
		{			
			String accDescription = String.valueOf(currentUser.getAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL)).trim(); 
			LOG.info("accDescription: " + accDescription);
			
			// if description is blank then return 0
			if (accDescription == null || accDescription.trim().equals(""))
			{
				return -1;
			}
			
			try {
				// Modified by Huy due to Jira: http://www.c-mg.info/jira/browse/REPORTING-96
				
				/*
				 * 	When setting up a user for superuser and reporting duties, the description field is used to determine the access level to member records - Default (standard records), 
				 * 	Execs, Senior Execs and Team. In the current design a blank description means the user has no level at all. Also, any whitespace means the user has no access level 
				 * 	either. 
					It was the original intention, that the decription would be trimmed of all white space and then compared with the level mapper. It was also the intention that after 
					trimming, if the description was an empty string "Default" would be assumed. 
					While in here, can we please add a new feature. As well as the above tokens, can we add in a user alignment token. for example "Aquila:BPJOHNSN" (without the quotes) 
					will take the DATASEC_HIGH column assigned to the BPJOHNSN user in the administrator database (USER_INFO table). A comma separated list should be supported and the 
					highest level from each element will be the users actual le	vel. For example, the following are all valid strings based on all of the above.
					 
					"" = default level 
					" D efa ult " = default level 
					"Default,Team" = Team level is effective 
					"Team, Aquila:BPTMPUSR" = probably team levels will be effective. 
					The concern in the business is that people's levels are not consistent across administrator and PensionLine because team leaders do not correctly request the accounts. 
				 */
				if (accDescription != null)
				{
					
					Connection aquilaConn = null;
					
					String selectSIQuery = "Select DATASEC_HIGH from USER_INFO where USER_NAME = ?";
					
					// split the security indicator
					String[] securityDescriptions = accDescription.split(",");
					
					for (int i = 0; i < securityDescriptions.length; i++)
					{
						String securityDescription = securityDescriptions[i].trim();
						LOG.info("Security description: " + securityDescription);
						int securityIndicatorTmp = -1;
						
						if (securityDescription.trim().toLowerCase().indexOf("aquila:") == 0)
						{
							String aquilaUserID = securityDescription.substring("aquila:".length());
																	
							try
							{
								// get security indicator from Aquila database
								if (aquilaConn == null)
								{
									aquilaConn = DBConnector.getInstance().getDBConnFactory(Environment.AQUILA);
								}										
								
								if (aquilaConn != null)
								{
									PreparedStatement pstm = aquilaConn.prepareStatement(selectSIQuery);
									
									pstm.setString(1, aquilaUserID);
									
									ResultSet rs = pstm.executeQuery();
									if (rs.next())
									{
										securityIndicatorTmp = Integer.parseInt(rs.getString(1));
										LOG.info("From Aquila: Report runner " + currentUser.getName() + " has security indicator = " + securityIndicatorTmp + " with " + securityDescription);
									}
									
									// if DATASEC_HIGH return 0, then the highest value
									if (securityIndicatorTmp == 0)
									{
										securityIndicatorTmp = 101;
									}
								}
							}
							catch (Exception e) 
							{
								LOG.error("Error while getting security indicator from Aquila: " + e.toString());
							}
						}
						else
						{
							try
							{
								// Read security indicator from a mapper file
								XmlReader reader = new XmlReader();
								
								byte[] arr = reader.readFile(Environment.SECURITYMAPPER_FILE);
					
								DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
								
								DocumentBuilder builder = factory.newDocumentBuilder();
								
								Document document = null;
					
								ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
								
								document = builder.parse(bIn);
					
								NodeList accessLevelNodeList = document.getElementsByTagName(securityDescription);
					
								if (accessLevelNodeList == null || securityDescription == null || securityDescription.equals(""))
								{
									// if the description of the superUser acc dose not exist in
									// the file SecurityMapper
									LOG.error("From mapper file: SecurityIndicator not set for this report runner member: " + securityDescription);
								} 
								else 
								{
									// if exist
									Node accessLevelNode = accessLevelNodeList.item(0);
									if (accessLevelNode.getNodeType() == Node.ELEMENT_NODE) 
									{
										securityIndicatorTmp = Integer.parseInt(accessLevelNode.getTextContent());
										LOG.info("From mapper file: Report runner " + currentUser.getName() + " has security indicator = " + securityIndicatorTmp + " with " + securityDescription);
									}
								}							
							}
							catch (Exception e) 
							{
								LOG.error("Error while getting security indicator from mapper file: " + e.toString());
							}
						}
						
						if (securityIndicator < securityIndicatorTmp)
						{
							securityIndicator = securityIndicatorTmp;
						}
					}
					
					if (aquilaConn != null)
					{
						DBConnector.getInstance().close(aquilaConn);
					}
				}				
			}
			catch (Exception e) 
			{
				LOG.error("Error in getting report runner SI number: " + e.toString());
			}
		}
		
		LOG.info("Preview section: Member SI: " + securityIndicator);
		
		return securityIndicator;
	}	

	public void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
	
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
			xmlString.append("</MemberData>\n");
//			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
//					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}	
}