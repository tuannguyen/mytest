/**
 * @(#)Member.java
 * 
 * This member was missing and only existed in a module now added here.
 *
 * @author Dominic Carlyle
 * @version 1.00 2007/3/9
 */

package com.bp.pensionline.test;

public class Member {
	//public static final String PROP_REF_NO = "refNo";
	private String refNo;
	private String group;
	private String name;
	private int age;
	private String dateOfBirth;
	private double salary;
	
	public Member(){
	}
    
    public Member(String refNo, String group, String name, int age, String dateOfBirth, Double salary) {
    	setRefNo(refNo);
    	setGroup(group);
    	setName(name);
    	setAge(age);
    	setDateOfBirth(dateOfBirth);
    	setSalary(salary);
    }
    
    public String getRefNo(){
    	return refNo;
    }
    public void setRefNo(String refNo){
    	this.refNo = refNo;
    }
    
    public String getGroup(){
    	return group;
    }
    public void setGroup(String group){
    	this.group = group;
    }
    
    public String getName(){
    	return name;
    }
    public void setName(String name){
    	this.name = name;
    }
    
    public Integer getAge(){
    	return age;
    }
    public void setAge(Integer age){
    	this.age = age;
    }
    
    public String getDateOfBirth(){
    	return dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth){
    	this.dateOfBirth = dateOfBirth;
    }
    
    public Double getSalary(){
    	return salary;
    }
    public void setSalary(Double salary){
    	this.salary = salary;
    }
    
    public String toString(){
    	return refNo + "|" + name + "|" + age + "|" + dateOfBirth + "|" + salary;
    }
}