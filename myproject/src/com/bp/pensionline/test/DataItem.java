/*
 * DataItem.java
 *
 * Created on March 13, 2007, 12:54 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.test;

/**
 *
 * @author mall
 */
public class DataItem {
    
    /** Creates a new instance of DataItem */
    public DataItem() {
    }
    
    public DataItem(String v, String t){
        this._value = v;
        this._type = t;
    }
    
    private String _value;
    private String _type;

    public String getValue() {
        return _value;
    }

    public void setValue(String value) {
        this._value = value;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }
    
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("{DataItem Value=").append(_value)
            .append(" Type=").append(_type)
            .append(" ]");
        return buffer.toString();
    }
    
}
