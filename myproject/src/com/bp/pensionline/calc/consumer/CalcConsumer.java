package com.bp.pensionline.calc.consumer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

//import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.CheckConfigurationKey;

public abstract class CalcConsumer extends Thread {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	protected long startTime = System.currentTimeMillis();
	
	public abstract void setMemberDao(MemberDao memberCopy);
	  
	public abstract void run();

	public abstract void storeInfoOnSession();

	public abstract MemberDao calculate(Date DoR, int accrual_rate, double cash);

	public abstract MemberDao runCalculate(Date DoR, int accrual_rate,
			double cash);

	public abstract MemberDao runCalculate(Date DoR);

	//DC calculation
	public abstract MemberDao calculateDC();

	//WC calculation
	public abstract MemberDao calculateWC();

	public abstract void deleteAdministratorLocks(String refNo, String bGroup);

	/**
	 * Quick debug method for info from calcs 
	 * 
	 * @param ref - to allow a message to be added about the debug
	 * @param objRef
	 * @param value
	 * @param attrib
	 */
	public abstract void memDebug(String ref, CalcConsumer objRef,
			String value, String attrib);

	/**
	 * @author HUY
	 * Check if a lock has been set for this calculation of given user.
	 * @param bGroup
	 * @param userName
	 * @param refNo
	 * @param calcType
	 * @param agcode
	 * @return STATUS of the lock: 	0 - lock is released; 
	 * 								1 - lock has been used; 
	 * 								-1 - lock has been time out
	 * 								
	 */
	public int getCalcLockStatus(Connection cmsCon, String bGroup, String refNo, String calcType, String agcode)
	{
		int status = 0;
		//String sqlSelect = "Select started from bp_calc_lock where BGroup=? and Refno=? and CalcType=?";
		
		// Modify so that lock is used to make only 1 calc running of 1 user at a time
		String sqlSelect = "Select started from bp_calc_lock where BGroup=? and Refno=?";
		try
		{
			startTime = System.currentTimeMillis();
			PreparedStatement pstm = cmsCon.prepareStatement(sqlSelect);

			// set all the parameters into the insert query
			/** TODO complete the rest of setting parameters. Be aware of the type */

			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			//pstm.setString(3, calcType);

			ResultSet rs = pstm.executeQuery();
			if (rs.next())
			{
				String startedStr = rs.getString("started");
				Long started = new Long(startedStr);
				long now = System.currentTimeMillis();
				Long calcOld = new Long (CheckConfigurationKey.getStringValue(Environment.MEMBER_CALCOLD));
				if ((now - started.longValue()) > calcOld.longValue())
				{
					status = -1;
				}
				else
				{
					status = 1;
				}
			}
			else
			{
				status = 0;
			}
			rs.close();
			pstm.close();
		}
		catch (SQLException e)
		{
			status = 0;
			LOG.error(this.getClass().toString() + ".getCalcLockStatus error: " + e.getMessage());
		}
		return status;
	}

	/**
	 * @author Huy
	 * Set lock for new calculation by removing the old record and insert a new one
	 * @param bGroup
	 * @param userName
	 * @param refNo
	 * @param calcType
	 * @param agcode
	 * @param status: Status of the lock
	 */
	public void setCalcLock(String bGroup, String refNo, String calcType, String agcode)
	{
		
//		String sqlDelete = "delete from bp_calc_lock where BGroup=? and Refno=? and CalcType=?";
//		String sqlInsert = "insert into bp_calc_lock (BGroup,Refno,CalcType,started,Agcode) values(?,?,?,?,?)";
		
		// Modify so that lock is used to make only 1 calc running of 1 user at a time
		String sqlDelete = "delete from bp_calc_lock where BGroup=? and Refno=?";
		String sqlInsert = "insert into bp_calc_lock (BGroup,Refno,CalcType,started,Agcode) values(?,?,?,?,?)";		
		Connection cmsCon = null;
		
		try
		{
			//cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.SQL);
			cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.PENSIONLINE);
			
			PreparedStatement pstmDelete = cmsCon.prepareStatement(sqlDelete);
			PreparedStatement pstmInsert = cmsCon.prepareStatement(sqlInsert);
			cmsCon.setAutoCommit(false);

			pstmDelete.setString(1, bGroup);
			pstmDelete.setString(2, refNo);
			//pstmDelete.setString(3, calcType);
			pstmDelete.execute();
			cmsCon.commit();

			pstmInsert.setString(1, bGroup);
			pstmInsert.setString(2, refNo);
			pstmInsert.setString(3, calcType);
			long started = System.currentTimeMillis();
			String startedStr = new Long(started).toString();
			pstmInsert.setString(4, startedStr);
			pstmInsert.setString(5, agcode);
			pstmInsert.execute();
			cmsCon.commit();

			cmsCon.setAutoCommit(true);
			
			LOG.info(this.getClass().toString() + ": LOCK set for " + bGroup + ", " + refNo + ", " + calcType);

		}
		catch (SQLException e)
		{
			LOG.error(this.getClass().toString() + ".setCalcLock error: " + e.getMessage());
		}
		finally
		{
			try
			{
				DBConnector.getInstance().close(cmsCon);
			}
			catch (Exception e)
			{
				LOG.error(this.getClass().toString() + ".releaseCMSConnection error: " + e.getMessage());
			}
		}		
	}

	/**
	 * @author Huy
	 * Release lock of current calculation by removing its record from table.
	 * @param bGroup
	 * @param userName
	 * @param refNo
	 * @param calcType
	 */
	public void releaseLock(String bGroup, String refNo, String calcType)
	{
		
		//String sqlDelete = "delete from bp_calc_lock where BGroup=? and Refno=? and CalcType=?";
		String sqlDelete = "delete from bp_calc_lock where BGroup=? and Refno=?";

		Connection cmsCon = null;
		try
		{
			//cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.SQL);
			cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.PENSIONLINE);

			PreparedStatement pstmDelete = cmsCon.prepareStatement(sqlDelete);

			cmsCon.setAutoCommit(false);
			pstmDelete.setString(1, bGroup);
			pstmDelete.setString(2, refNo);
			//pstmDelete.setString(3, calcType);
			pstmDelete.execute();
			cmsCon.commit();

			cmsCon.setAutoCommit(true);
			
			LOG.info(this.getClass().toString() + ": LOCK released for " + bGroup + ", " + refNo + ", " + calcType);
		}
		catch (SQLException e)
		{
			LOG.error(this.getClass().toString() + ".releaseLock error: "
					+ e.getMessage());
		}
		finally
		{
			try
			{
				DBConnector.getInstance().close(cmsCon);
			}
			catch (Exception e)
			{
				LOG.error(this.getClass().toString() + ".releaseCMSConnection error: " + e.getMessage());
			}
		}
	}	  
	
	/**
	 * Update calculation inputs before running the process. Changed at 12/09/2008.
	 * @param calType
	 * @param refNo
	 * @param bGroup
	 * @param DoR
	 */
	protected void insertMoreCalInputAdjust(String calType, String refNo, String bGroup, Date doc) 
	{
		Connection con = null;
		CallableStatement cs = null;
		try {
            String procedureQuery = 
            		"DECLARE " +
            		"BEGIN " +
            		"adm_bp_peak_salary.main (?, ?, ?,to_char(to_date(?),'DD/MM/YYYY')); " +
            		"adm_bp_dert.main (?, ?, ?,to_char(to_date(?),'DD/MM/YYYY')); " +
				    "END;";	
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			
			cs = con.prepareCall(procedureQuery);
			cs.setString(1, bGroup);
			cs.setString(2, refNo);
			cs.setString(3, calType);
			// format to DD-MMM-YY
			SimpleDateFormat dateFormat = new SimpleDateFormat("d-MMM-yy");
			LOG.info("insertMoreCalInputAdjust: doc " + dateFormat.format(doc));
			cs.setString(4, dateFormat.format(doc));	// date of calculation
			cs.setString(5, bGroup);
			cs.setString(6, refNo);
			cs.setString(7, calType);
			// format to DD-MMM-YY
			cs.setString(8, dateFormat.format(doc));	// date of calculation			
			   						
			cs.execute();
			con.commit();
			con.setAutoCommit(true);

		} catch (Exception e) {
			//releaseLock(bGroup, refNo, calType);
			LOG.debug("insertMoreCalInput error : " + e.toString());
			// TODO: handle exception
			try 
			{
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
			
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	}		
}
