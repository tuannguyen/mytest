package com.bp.pensionline.calc.consumer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.database.DatabaseMemberDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;


/**
 * @author CuongDV
 * @date May 10, 2007
 * @modified May 10, 2007
 * @version 1.0
 * 
 * @author Tu Nguyen
 * @modified May 23, 2007
 * @version 1.1
 * 
 *
 *
 */
public class BpAcDcConsumer extends CalcConsumer {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private MemberDao memberDao = null;
	private String calType = "";
	// Added by HUY: QUICK FIX
	private long calcRunStart;	
	/**
	 * @param memberDao
	 */
	public void setMemberDao(MemberDao memberDao) {

		this.memberDao = memberDao;
		calType = String.valueOf(memberDao.get("CalcType"));
		System.out.println("calType for DC consumer");
		System.out.println(calType);

	}

	public MemberDao getMemberDao() {
		return this.memberDao;
	}

     /*
	 * do caculations
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#run() run calculation
	 *      in thread
	 */
	public void run() {

		// do caculations
		System.out.println("===================== DC DC DC DC DC DC DC DC ==================");

		try {

			//synchronized (this) {
			//MemberDao memberTemp = calculateDC();
			//memberDao.set(MemberDao.DeathInServiceCash, memberTemp.get(MemberDao.DeathInServiceCash));
			//memberDao.set(MemberDao.DeathInServicePension, memberTemp.get(MemberDao.DeathInServicePension));
			
			
			// Updated by HUY Tran to remove DeathInService Calcs (BpAcDcConsumer)
			/*
			 * 
			 * If (systemDate() > 31/03/2011) { 

		      DeathInServiceLumpSum = 4 x FinalPensionableSalary 
		
				} else { 
				
				      If (CurrentAccrualRate == 60ths) { 
				
				            DeathInServiceLumpSum = 3 x FinalPensionableSalary 
				
				      } else { 
				
				            DeathInServiceLumpSum = 4 x FinalPensionableSalary 
				
				      } 
				
				} 
			 */
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, 2011);
			calendar.set(Calendar.DAY_OF_MONTH, 31);
			calendar.set(Calendar.MONTH, Calendar.MARCH);	
			calendar.set(Calendar.HOUR, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			
			double deathInServiceLumpSum = 0.0;
			// Updated by Huy Tran - 5-09-2011. The Death in service cash should be 4 x Actual Annual Pensionable which is miscellaneous_history.mh04c or BasicSalary
			String finalPensionableSalary = this.memberDao.get("BasicSalary");
			double finalPensionableSalaryDouble = NumberUtil.CurrencyToDecimal(finalPensionableSalary);			
			
			int CINN91 = NumberUtil.getInt(memberDao.get("CINN91"));
			
			System.out.println("CINN91: " + CINN91);
			
			if (System.currentTimeMillis() > calendar.getTimeInMillis()) 
			{ 
				deathInServiceLumpSum = 4 * finalPensionableSalaryDouble;
			} 
			else 
			{ 
				if (CINN91 == 60) 
				{ 
					deathInServiceLumpSum = 3 * finalPensionableSalaryDouble;
				}
				else 
				{ 
					deathInServiceLumpSum = 4 * finalPensionableSalaryDouble ;
				} 
			} 
			// Updated by HUY Tran to remove DeathInService Calcs (BpAcDcConsumer)
			//memberDao.set(MemberDao.DeathInServicePension, memberDao.get(MemberDao.SpousesPension));
			memberDao.set(MemberDao.DeathInServiceCash, NumberUtil.toNearestPound(deathInServiceLumpSum));
			storeInfoOnSession();
			//}
			System.out.println("BpAcDcConsumer: All calculation has been done!!!");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("run error : ");
			// e.printStackTrace();
		}

	}

	/**
	 * Thread safe across sessions
	 * 
	 * store the member dao with updated data to the session
	 * 
	 */
	public void storeInfoOnSession() {
		try {
			String sessionId = memberDao.get("SessionId");
			CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);

			MemberDao sessDao = (MemberDao)(cmsuser.getAdditionalInfo().get(Environment.MEMBER_KEY));
			synchronized(sessDao){
				if (memberDao.get(MemberDao.DeathInServiceCash) != null && memberDao.get(MemberDao.DeathInServiceCash) != ""){
					sessDao.set(MemberDao.DeathInServiceCash, memberDao.get(MemberDao.DeathInServiceCash));
					memDebug("BpAcDcConsumer.storeInfoOnSession: ", this, MemberDao.DeathInServiceCash, memberDao.get(MemberDao.DeathInServiceCash));
				}
//				if (memberDao.get(MemberDao.DeathInServicePension) != null && memberDao.get(MemberDao.DeathInServicePension) != ""){
//					sessDao.set(MemberDao.DeathInServicePension, memberDao.get(MemberDao.DeathInServicePension));
//					memDebug("BpAcDcConsumer.storeInfoOnSession: ", this, MemberDao.DeathInServicePension, memberDao.get(MemberDao.DeathInServicePension));
//				}
			}			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	/**
	 * @param userid
	 * @param calctype
	 * @param bgroup
	 *            Delete from calc_input table
	 */
	private void deleteCal(String userid, String calctype, String bgroup) {
		String sqlDelete = "Delete from calc_input where refno =? and  calctype = ? and  BGROUP = ? ";

			
		Connection con = null;
		PreparedStatement pstm = null;
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm = con.prepareStatement(sqlDelete);
			pstm.setString(1, userid);
			pstm.setString(2, calctype);
			pstm.setString(3, bgroup);
			//LOG.info("BpAcDcConsumer.deleteCal: Delete from calc_input where refno ="+userid+" and  calctype = "+calctype+" and  BGROUP = "+bgroup+" ");				
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			System.out.println("deleteCal has been done!!!");

			
		} catch (Exception e) {
			// TODO: handle exception
			//releaseLock(bgroup, userid, calctype);
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
			System.out.println("deleteCal error ");
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}

	}

	/**
	 * @param calType
	 * @param refNo
	 * @param bGroup
	 * @param DoR
	 * @param cash
	 *            Insert into cac_input table
	 */
	private void insertCal(String calType, String refNo, String bGroup) {
		String sqlInsert = "insert into calc_input(CalcType,Refno,BGroup,CIND01,CINI01) " +
				"values(?,?,?,(SELECT TO_CHAR(SYSDATE,'DD-MON-YYYY') systemdate FROM dual),"
				+ "?) ";
		Connection con = null;
		PreparedStatement pstm = null;
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm = con.prepareStatement(sqlInsert);
			pstm.setString(1, calType);
			pstm.setString(2, refNo);
			pstm.setString(3, bGroup);
			pstm.setInt(4, 1); // Number of dependants - will always be 1
	
			pstm.executeUpdate();
			con.commit();
			con.setAutoCommit(true);
			System.out.println("insertCal has been done!!!");

		} catch (Exception e) {
			//releaseLock(bGroup, refNo, calType);
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
			System.out.println("insertCal error : ");
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	}

	/** check whether Session row has been created */
	private boolean checkCentralSession(String userid, String password, String bGroup, String agcode){
		boolean sessionExist = false;
		
		String sqlInsert = " select * from central_session where userid = '"+userid+"' and password = '"+password+"' and " +
				"bgroup = '"+bGroup+"' and agcode = '"+agcode+"'";
		
		Connection con = null;
		Statement pstm = null;
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
						
			pstm = con.createStatement();	
			
			
			ResultSet rs;
			rs = pstm.executeQuery(sqlInsert);
			
			if (rs.next()) {
				sessionExist = true;
			}

		} catch (Exception e) {
					
			LOG.error("checkCentralSession error:" + e);
			
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					LOG.error("Error in CloseConnection in CheckCentralSession Method "+ e);
				}
			}
		}
	
		
	return sessionExist;
		
	}
	
	/**
	 * @param userid
	 * @param password
	 * @param bGroup
	 * @param agcode
	 *            Insert into centeral_session table
	 */
	private void inserIntoSession(String userid, String password,
			String bGroup, String agcode) {
		String sqlInsert = " insert into central_session (userid,password,session_number,start_date,start_time, bgroup,logon_id,AGCODE)"
				+ "values (?,?,0,(SELECT TO_CHAR(SYSDATE,'DD-MON-YYYY') systemdate FROM dual),'00:00:00',?,'INTERNAL',?)";
		
			
		
		Connection con = null;
		PreparedStatement pstm = null;
		
		boolean sessionExist = checkCentralSession(userid, password, bGroup, agcode);
		
		if (sessionExist ==false ){
				
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm = con.prepareStatement(sqlInsert);
			pstm.setString(1, userid);
			pstm.setString(2, password);
			pstm.setString(3, bGroup);
			pstm.setString(4, agcode);
			pstm.executeUpdate();

			con.commit();
			con.setAutoCommit(true);

		} catch (Exception e) {
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				ex.printStackTrace();
			}
			System.out.println("inserIntoSession error:");
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		}
	}

	

	/**
	 * @param bGroup
	 * @param userName
	 * @param refNo
	 * @param calcType
	 * @param password
	 * @param status
	 */
	
	private void runProcess(String bGroup, String userName, String refNo,
			String calcType, String password, Date DoC) {
		
		
		CallableStatement cs = null;
		Connection con = null;
		try {
		
            String procedureQuery = "declare returnStatus NUMBER; errorInfo BP1.pmsapi_err.error_info_tab_type;"
            	                 
                                    +"begin BP1.PMSAPI_CALC.run_process(?, ?, ?,?, ?,returnStatus, errorInfo); end;";
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			cs = con.prepareCall(procedureQuery);
			cs.setString(1, bGroup);
			cs.setString(2, userName);
			cs.setString(3, password);
			cs.setString(4, refNo);
			cs.setString(5, calcType);
					   	
			cs.execute();
			con.commit();
			con.setAutoCommit(true);
			System.out.println("runProcess has been done !!!");
		} catch (Exception e) {
			//releaseLock(bGroup, refNo, calcType);
			e.printStackTrace();
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}

	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#calculate(java.sql.Date,
	 *      int, double)
	 */
	public MemberDao calculateDC() {
	
		
		String bGroup = String
				.valueOf(this.getMemberDao().get(Environment.MEMBER_BGROUP));

		String refNo = String.valueOf(this.getMemberDao().get(Environment.MEMBER_REFNO));

		String calType = String.valueOf(this.getMemberDao().get("CalcType"));

		//calType = calType == null ? new String("") : calType;
//
//		System.out.println("calType for DC consumer inside the method");
//		System.out.println(calType);
		
		
		String userid = String.valueOf(this.getMemberDao().get(Environment.MEMBER_REFNO));

		userid = userid == null ? new String("") : userid;

		String username = CheckConfigurationKey.getStringValue("calcUserName");

		username = username == null ? new String("") : username;

		String password = CheckConfigurationKey.getStringValue("calcPassword");

		password = password == null ? new String("") : password;

		String agCode = String
				.valueOf(this.getMemberDao().get(Environment.MEMBER_AGCODE));

		agCode = agCode == null ? new String("") : agCode;
		
		/************************* Checking lock for calc: HUY ***************/
		// Create a cms connection
		Connection cmsCon = null;
		
		try
		{
			LOG.info(this.getClass().toString() + ": CHECK LOG BEGIN ----->");
			//cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.SQL);
			cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.PENSIONLINE);
					
			int lockStatus = getCalcLockStatus(cmsCon, bGroup, refNo, calType, agCode);
			boolean isLocked = false;
			long start = System.currentTimeMillis();
			Long calcTimeout = new Long (CheckConfigurationKey.getStringValue(Environment.MEMBER_CALCTIMEOUT));
			
			if (lockStatus == 0 || lockStatus == -1) // start new calculation if no lock or lock is time out
			{				
				LOG.info(this.getClass().toString() + ": Lock free " + lockStatus);
				isLocked = false;
			}
			else	// wait for calc_timeout
			{
				LOG.info(this.getClass().toString() + ": Lock used " + lockStatus);
				isLocked = true;
				while (isLocked)
				{
					lockStatus = getCalcLockStatus(cmsCon, bGroup, refNo, calType, agCode);
					if (lockStatus == 0 || lockStatus == -1)
					{
						isLocked = false;
					}
					long now = System.currentTimeMillis();
					if ((now - start) > calcTimeout.longValue()) break;
				}
			}
			// replace the idle calc by the new calc
			setCalcLock(bGroup, refNo, calType, agCode);				
		}
		catch (Exception e) {
			LOG.error(this.getClass().toString() + ".calculate error while opening cms conn: " + e.getMessage());
		}
		finally
		{
			if (cmsCon != null)
			{
				try
				{
					DBConnector.getInstance().close(cmsCon);
				}
				catch (Exception e)
				{
					LOG.error(this.getClass().toString() + ".calculate error while close cms conn: " + e.getMessage());
				}
			}
		}
		
		LOG.info(this.getClass().toString() + ": CHECK LOG END ----->");
		/***** Checking lock finished ****/	
				
		deleteCalOutput(userid, calType, bGroup);
		
		//prepare the input and output tables
		deleteCal(userid, calType, bGroup); //correct
		insertCal(calType, refNo, bGroup); //working after being corrected
		Date DoC = new Date(System.currentTimeMillis());
		
		inserIntoSession(username, password, bGroup, agCode); //correct
		
		deleteAdministratorLocks(refNo, bGroup);
		
		LOG.info("Insert more calc input: DC");
		insertMoreCalInputAdjust(calType, refNo, bGroup, DoC);
		runProcess(bGroup, username, refNo, calType, password, DoC); //working after being corrected
				       
		// HUY: Set start tim
		calcRunStart = System.currentTimeMillis();
		
		String sqlSelect = "SELECT CALC_OUTPUT.Bgroup,CALC_OUTPUT.refno,CALC_OUTPUT.CALCTYPE,"
			+ " CALC_OUTPUT.COUN44 DeathInServiceCash,"
			+ "CALC_OUTPUT.COUN43 DeathInServicePension FROM CALC_OUTPUT "
			+ "WHERE CALC_OUTPUT.Bgroup=? AND CALC_OUTPUT.Refno=? AND CALC_OUTPUT.CALCTYPE=? ";
			

		PreparedStatement pstm = null;
		Connection con = null;
		ResultSet rs = null;
		
		MemberDao memberTemp = new DatabaseMemberDao();
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm = con.prepareStatement(sqlSelect);
			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			pstm.setString(3, calType);
			rs = pstm.executeQuery();
			
			boolean calcResultFound = false;
			String sqlSelectError = "SELECT message FROM CALC_ERRORS WHERE bgroup=? and refno=? and calctype=? and errtype='F'";
			PreparedStatement pstmError = null;
			ResultSet rsError = null;			
			pstmError = con.prepareStatement(sqlSelectError);
			pstmError.setString(1, bGroup);
			pstmError.setString(2, refNo);
			pstmError.setString(3, calType);
			rsError = pstmError.executeQuery();				
			
			while (!calcResultFound)
			{ 			
				if (rs.next()) {
					
					memberTemp.set(MemberDao.DeathInServiceCash, StringUtil
							.getString(NumberUtil.toLowestPound(rs.getDouble("DeathInServiceCash"))));
	
					memberTemp.set(MemberDao.DeathInServicePension, StringUtil
							.getString(NumberUtil.toLowestPound(rs.getDouble("DeathInServicePension"))));
					
					calcResultFound = true;
					LOG.info(this.getClass().toString() + ": SMOOTH");
				} 
				else if (rsError.next())
				{
					//TODO:
					LOG.info(this.getClass().toString() + ". There is error in calc_error table.");
					memberTemp.set(MemberDao.DeathInServiceCash, StringUtil.EMPTY_STRING);
					memberTemp.set(MemberDao.DeathInServicePension, StringUtil.EMPTY_STRING);					
					calcResultFound = true;
					
					String message = rsError.getString("message");
					LOG.error(this.getClass().toString() + " - 1st Error message: " + message);					
				}				
				else {

					long now = System.currentTimeMillis();
					Long waitTimeout = new Long(CheckConfigurationKey.getStringValue(Environment.MEMBER_CALCOUTPUTWAIT));
					
					if ((now - calcRunStart) > waitTimeout.longValue())
					{
						LOG.info(this.getClass().toString() + ": No data found in both calc_output and calc_eror");
						
						memberTemp.set(MemberDao.DeathInServiceCash, StringUtil.EMPTY_STRING);
						memberTemp.set(MemberDao.DeathInServicePension, StringUtil.EMPTY_STRING);
												
						break;
					}
					
					rs = pstm.executeQuery();
					rsError = pstmError.executeQuery();
				}
			}
			pstm.close();
			rs.close();
			rsError.close();
			pstmError.close();		
			LOG.info("com.bp.pensionline.calc.consumerBpAcDcConsumer.calculateDC end");
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Calculation error:");
			e.printStackTrace();
		} finally {
			// HUY: Release lock when finish
			releaseLock(bGroup, refNo, calType);
			
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
		}
		return memberTemp;

	}

	/**
	    * Delete from all the clac output tables before running
	    *
	    *     CALC_OUTPUT
	    *     CALC_OUTPUT_2
	    *     CALC_OUTPUT_3
	    *     CALC_OUTPUT_4
	    *     CALC_OUTPUT_5
	    *
	    * @param userid
	    * @param calctype
	    * @param bgroup
	    *            Delete from calc_input table
	    */
	   private void deleteCalOutputRecord( String tablename, String userid, String calctype, String bgroup) {
	             String sqlDelete = "Delete from "+tablename+" where refno =? and  calctype = ? and  Bgroup = ? ";
	       Connection con = null;
	       PreparedStatement pstm = null;
	       try {

	           DBConnector connector = DBConnector.getInstance();
	           con = connector.getDBConnFactory(Environment.AQUILA);
	           con.setAutoCommit(false);
	           pstm = con.prepareStatement(sqlDelete);
	           pstm.setString(1, userid);
	           pstm.setString(2, calctype);
	           pstm.setString(3, bgroup);
	           pstm.execute();
	           con.commit();
	           con.setAutoCommit(true);
	           //System.out.println("deleteCal has been done!!!");
	           //LOG.error("deleteCal has been done!!! for user: "+userid+", bgroup:"+bgroup+", calctype:"+calctype);
	       } catch (Exception e) {
	    	   //releaseLock(bgroup, userid, calctype);
	    	   
	           // TODO: handle exception
	           try {
	               con.rollback();
	           } catch (Exception ex) {
	               // TODO: handle exception
	           }
	           System.out.println("deleteCal error ");
	           e.printStackTrace();
	       } finally {
	           if (con != null) {
	               try {
	            	   DBConnector connector = DBConnector.getInstance();
					   connector.close(con);
	               } catch (Exception e) {
	            	   LOG.error("Unable to close DB connection for user: "+userid+", bgroup:"+bgroup+", calctype:"+calctype, e);
	               }
	           }
	       }

	   }
	 
	   /**
	    * Delete from all the clac output tables before running
	    *
	    *     CALC_OUTPUT
	    *     CALC_OUTPUT_2
	    *     CALC_OUTPUT_3
	    *     CALC_OUTPUT_4
	    *     CALC_OUTPUT_5
	    *
	    * @param userid
	    * @param calctype
	    * @param bgroup
	    *            Delete from calc_input table
	    */
	   private void deleteCalOutput( String userid, String calctype, String bgroup) 
	   {
		   // Modify by Huy: remove try catch block. Add deleteCalOutputRecord( "CALC_ERRORS", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_2", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_3", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_4", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_5", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_ERRORS", userid, calctype, bgroup);
	   } 
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#runCalculate(java.sql.Date)
	 *      This calculation is not used in this case
	 */
	public MemberDao runCalculate(Date DoR) {
		return null;
	}


	public MemberDao calculate(Date DoR, int accrual_rate, double cash) {
		// TODO Auto-generated method stub
		return null;
	}


	public MemberDao runCalculate(Date DoR, int accrual_rate, double cash) {
		// TODO Auto-generated method stub
		return null;
	}


	public MemberDao calculateWC() {
		// TODO Auto-generated method stub
		return null;
	}


	public void deleteAdministratorLocks(String refNo, String bGroup) {
		   // delete from lock_table where bgroup = $bgroup and lock_refno = (select crefno from basic where refno = refno and bgroup = bgroup);
	       String sqlInsert = "delete from lock_table where bgroup = ? and lock_refno = (select crefno from basic where refno = ? and bgroup = ?)";

	       Connection con = null;
	       PreparedStatement pstm = null;
	       try {

	           DBConnector connector = DBConnector.getInstance();
	           con = connector.getDBConnFactory(Environment.AQUILA);
	           con.setAutoCommit(false);
	           pstm = con.prepareStatement(sqlInsert);
	           pstm.setString(1, bGroup);
	           pstm.setString(2, refNo);
	           pstm.setString(3, bGroup);
	           pstm.execute();
	           con.commit();
	           con.setAutoCommit(true);

	           LOG.debug("deleteFromSession has been done !!! refno: "+refNo+", bgroup:"+bGroup);
	       } catch (Exception e) {
	           // TODO: handle exception
	           try {
	               con.rollback();
	           } catch (Exception ex) {
	               // TODO: handle exception
	               ex.printStackTrace();
	           }
	           LOG.debug("deleteFromSession issue !!! refno: "+refNo+", bgroup:"+bGroup, e);
	           e.printStackTrace();
	       } finally {
	           if (con != null) {
	               try {
	            	   DBConnector connector = DBConnector.getInstance();
	            	   connector.close(con);//con.close();
	               } catch (Exception e) {
	                   // TODO: handle exception
	               }
	           }
	       }
	   }


	/**
	 * Quick debug method for info from calcs 
	 * 
	 * @param objRef
	 * @param value
	 * @param attrib
	 */
	public void memDebug(String ref, CalcConsumer objRef, String attrib, String value){
		if (attrib == null || attrib == ""){
			LOG.info( objRef.getName()+"("+ref+")"+"[1]CR-ref: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb: IS NULL or EMPTY STRING" );
		}else if (value == null || value == ""){
			LOG.info( objRef.getName()+"("+ref+")"+"[2]CR-ref:: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb:"+ attrib+ ", Val= IS NULL or EMPTY STRING" );
		}else{
			LOG.info( objRef.getName()+"("+ref+")"+"[3]CR-ref: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb:"+ attrib+ ", Val= "+ value );
		}
	}
	
	
}



