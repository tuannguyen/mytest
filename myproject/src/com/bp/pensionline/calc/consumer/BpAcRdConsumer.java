/**
 * 
 */
package com.bp.pensionline.calc.consumer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.database.DatabaseMemberDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 25/05/2007
 *
 */
public class BpAcRdConsumer extends CalcConsumer {

	private MemberDao memberDao = null;
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	// Added by HUY: QUICK FIX
	private long calcRunStart;	
	/**
	 * @param memberDao
	 */

	public MemberDao getMemberDao() {
		return this.memberDao;
	}

	public void setMemberDao(MemberDao memberCopy) {
		// TODO Auto-generated method stub
		this.memberDao = memberCopy;				
	}	

	public MemberDao calculateDC() {
		// TODO Auto-generated method stub
		return null;
	}

	public MemberDao calculateWC() {
		// TODO Auto-generated method stub
		return null;
	}

	public BpAcRdConsumer() {
		// TODO Auto-generated constructor stub
	}

	public MemberDao calculate(Date DoR, int accrual_rate, double cash) {
		// TODO Auto-generated method stub
		return null;
	}

	public void run() {
		try {
				LOG.info("BpAcRdConsumer.run begin");
				java.util.Date day=new Date(System.currentTimeMillis());
				java.sql.Date day2Calc=new java.sql.Date(day.getTime());
				MemberDao temp=	calculateRD(day2Calc);
				
//				 copy memberDao attribute
				memberDao.set(MemberDao.AccruedPension, temp.get(MemberDao.AccruedPension));
				memberDao.set(MemberDao.EgpCash, temp.get(MemberDao.EgpCash));
				memberDao.set(MemberDao.SrpCash, temp.get(MemberDao.SrpCash));
				memberDao.set(MemberDao.TaxFreeCash, temp.get(MemberDao.TaxFreeCash));
				memberDao.set(MemberDao.TaxableCash, temp.get(MemberDao.TaxableCash));
				memberDao.set(MemberDao.TaxPayable, temp.get(MemberDao.TaxPayable));
				memberDao.set(MemberDao.RrReducedPension, temp.get(MemberDao.RrReducedPension));
				memberDao.set(MemberDao.RrSpousesPension,  temp.get(MemberDao.RrSpousesPension));
				memberDao.set(MemberDao.RrMaxLumpSum, temp.get(MemberDao.RrMaxLumpSum));
				memberDao.set(MemberDao.RrResidualPension, temp.get(MemberDao.RrResidualPension));
				memberDao.set("CalcRR", temp.get("CalcRR"));
				memberDao.set("RedundancyDate", DateUtil.toStringDDMMMYYYY(day));

			LOG.info("com.bp.pensionline.calc.consumer.BpAcRdConsumer.run end");
		} catch (Exception e) {
			// TODO: handle exception
			
			LOG.error("com.bp.pensionline.calc.consumer.BpAcRdConsumer.run error: ",e);
			
		}

	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#runCalculate(java.sql.Date, int, double)
	 * 
	 * This calculattion is not used in this case
	 */	
	public MemberDao runCalculate(Date DoR, int accrual_rate, double cash) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#runCalculate(java.sql.Date)
	 */
	public MemberDao runCalculate(Date rDate) {
		try {
			// calculate using RR, return MemberDao
			return calculateRD(rDate);
					
		} catch (Exception e) {
			LOG.error("com.bp.pensionline.calc.consumer.BpAcRrConsumer.runCalculate error:  ",e);
			return null;
		}
	}
	/**
	 * @param rdDate
	 * @return memberDao
	 * Calculate at RR
	 */
	private MemberDao  calculateRD(Date rdDate){
		LOG.info("com.bp.pensionline.calc.consumerBpAcRdConsumer.calculateRR begin");
		
		String sqlSelect = " SELECT CALC_OUTPUT.COUN1L ACCRUEDPENSION, CALC_OUTPUT.COUN0N EGPCASH," +
				"CALC_OUTPUT.COUN0M SRPCASH,CALC_OUTPUT.COUN0S TAXFREECASH,CALC_OUTPUT.COUN0O TAXABLECASH," +
				"CALC_OUTPUT.COUN0R TAXPAYABLE FROM CALC_OUTPUT" +
				" WHERE CALC_OUTPUT.BGROUP=? AND CALC_OUTPUT.REFNO=? AND CALC_OUTPUT.CALCTYPE=?";
				
		String bGroup = String.valueOf(this.getMemberDao().get(Environment.MEMBER_BGROUP));

		String refNo = String.valueOf(this.getMemberDao().get(Environment.MEMBER_REFNO));

		String calType = String.valueOf(this.getMemberDao().get("CalcType"));
		LOG.info("calType: " +  calType);
		System.out.println("RDate: "+rdDate.getTime());
		String userid = refNo;

		userid = userid == null ? new String("") : userid;

		String username = CheckConfigurationKey.getStringValue("calcUserName");// get cmsuser name from opencms.properties file

		username = username == null ? new String("") : username;

		String password = CheckConfigurationKey.getStringValue("calcPassword");

		password = password == null ? new String("") : password;

		String agCode = String.valueOf(memberDao.get(Environment.MEMBER_AGCODE));

		agCode = agCode == null ? new String("") : agCode;
		
		/************************* Checking lock for calc: HUY ***************/
		// Create a cms connection
		Connection cmsCon = null;
		
		try
		{
			LOG.info(this.getClass().toString() + ": CHECK LOG BEGIN ----->");
			//cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.SQL);
			cmsCon = DBConnector.getInstance().getDBConnFactory(Environment.PENSIONLINE);
					
			int lockStatus = getCalcLockStatus(cmsCon, bGroup, refNo, calType, agCode);
			boolean isLocked = false;
			long start = System.currentTimeMillis();
			Long calcTimeout = new Long (CheckConfigurationKey.getStringValue(Environment.MEMBER_CALCTIMEOUT));
			
			if (lockStatus == 0 || lockStatus == -1) // start new calculation if no lock or lock is time out
			{				
				LOG.info(this.getClass().toString() + ": Lock free " + lockStatus);
				isLocked = false;
			}
			else	// wait for calc_timeout
			{
				LOG.info(this.getClass().toString() + ": Lock used " + lockStatus);
				isLocked = true;
				while (isLocked)
				{
					lockStatus = getCalcLockStatus(cmsCon, bGroup, refNo, calType, agCode);
					if (lockStatus == 0 || lockStatus == -1)
					{
						isLocked = false;
					}
					long now = System.currentTimeMillis();
					if ((now - start) > calcTimeout.longValue()) break;
				}
			}
			// replace the idle calc by the new calc
			setCalcLock(bGroup, refNo, calType, agCode);
		}
		catch (Exception e) {
			LOG.error(this.getClass().toString() + ".calculate error while opening cms conn: " + e.getMessage());
		}
		finally
		{
			if (cmsCon != null)
			{
				try
				{
					DBConnector.getInstance().close(cmsCon);
				}
				catch (Exception e)
				{
					LOG.error(this.getClass().toString() + ".calculate error while close cms conn: " + e.getMessage());
				}
			}
		}
		
		LOG.info(this.getClass().toString() + ": CHECK LOG END ----->");
		/***** Checking lock finished ****/		
		
		LOG.info(this.getClass().toString() + " deleteCalOutput Begin");		
		deleteCalOutput(userid, calType, bGroup);
		LOG.info(this.getClass().toString() + " deleteCalOutput Ok");
		deleteCal(userid, calType, bGroup);// delete from calc_input table from previous
		LOG.info(this.getClass().toString() + " deleteCal Ok");
		insertCalcInput(calType, refNo, bGroup, rdDate); // Insert new row into calc_input table
		LOG.info(this.getClass().toString() + " insertCalcInput Ok");
		inserIntoSession(username, password, bGroup, agCode); // Insert new row in to centeral_session table
		LOG.info(this.getClass().toString() + " inserIntoSession Ok");
		deleteAdministratorLocks(refNo, bGroup);
		LOG.info(this.getClass().toString() + " deleteAdministratorLocks Ok");
		
		LOG.info("Insert more cal input: RD");
		insertMoreCalInputAdjust(calType, refNo, bGroup, rdDate);
		runProcess(bGroup, username, refNo, calType, password); //working after being corrected // Run package in Oracle database
		LOG.info(this.getClass().toString() + " runProcess Ok");
		// HUY: Set start tim
		calcRunStart = System.currentTimeMillis();
		
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		
		MemberDao memberTemp = new DatabaseMemberDao();
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm = con.prepareStatement(sqlSelect);
			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			pstm.setString(3, calType);
			rs = pstm.executeQuery();
			
			//Initialise
			memberTemp.set(MemberDao.AccruedPension, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.EgpCash, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.SrpCash, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.TaxFreeCash, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.TaxableCash, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.TaxPayable, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.RrReducedPension, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.RrSpousesPension, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.RrMaxLumpSum, StringUtil.EMPTY_STRING);
			memberTemp.set(MemberDao.RrResidualPension, StringUtil.EMPTY_STRING);
						
			boolean calcResultFound = false;
			String sqlSelectError = "SELECT message FROM CALC_ERRORS WHERE bgroup=? and refno=? and calctype=? and errtype='F'";
			PreparedStatement pstmError = null;
			ResultSet rsError = null;			
			pstmError = con.prepareStatement(sqlSelectError);
			pstmError.setString(1, bGroup);
			pstmError.setString(2, refNo);
			pstmError.setString(3, calType);
			rsError = pstmError.executeQuery();	
			
			while (!calcResultFound)
			{			
				if (rs.next()){// if calculate have record set it in to memberDao attribute
					memberTemp.set(MemberDao.AccruedPension, StringUtil.getString(rs.getString("ACCRUEDPENSION")));
					memberTemp.set(MemberDao.EgpCash, NumberUtil.toLowestPound(rs.getDouble("EGPCASH")));
					memberTemp.set(MemberDao.SrpCash, NumberUtil.toLowestPound(rs.getDouble("SRPCASH")));
					memberTemp.set(MemberDao.TaxFreeCash, NumberUtil.toLowestPound(rs.getDouble("TAXFREECASH")));
					memberTemp.set(MemberDao.TaxableCash,NumberUtil.toLowestPound(rs.getDouble("TAXABLECASH")));
					memberTemp.set(MemberDao.TaxPayable, NumberUtil.toLowestPound(rs.getDouble("TAXPAYABLE")));
					
					calcResultFound = true;
					LOG.info(this.getClass().toString() + ": SMOOTH");
				}//if no results then do nothing.
				else if (rsError.next())
				{
					//TODO:
					LOG.info(this.getClass().toString() + ". There is error in calc_error table.");
					calcResultFound = true;
					String message = rsError.getString("message");
					LOG.error(this.getClass().toString() + " - 1st Error message: " + message);					
				}				
				else
				{
					long now = System.currentTimeMillis();
					Long waitTimeout = new Long(CheckConfigurationKey.getStringValue(Environment.MEMBER_CALCOUTPUTWAIT));
					
					if ((now - calcRunStart) > waitTimeout.longValue())
					{
						LOG.info(this.getClass().toString() + ": No data found in both calc_output and calc_eror");						
						break;
					}
					
					rs = pstm.executeQuery();	
					rsError = pstmError.executeQuery();
				}
			}
			
			
			LOG.info("com.bp.pensionline.calc.consumerBpAcRdConsumer.calculateRD end");
			rs.close();
			pstm.close();
			rsError.close();
			pstmError.close();			
			
		} catch (Exception e) {
			LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.calculateRR error :",e);
		}finally{
			// HUY: Release lock when finish
			releaseLock(bGroup, refNo, calType);
			
			if (con!=null){
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.calculateRR can not close connection cause :",e);
				}
			}
		}
		return memberTemp;		
	}
	
	

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.consumer.CalcConsumer#storeInfoOnSession()
	 * Sotore memberDao in CmsUser addtional info in case run with thread
	 */
	
	@Override
	public void storeInfoOnSession() {
		LOG.info("BpAcRdConsumer: storeInfoOnSession end");
		try {
			String sessionId = memberDao.get("SessionId");
			CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);
			
			//test for super user:
			java.util.Map tempMap = cmsuser.getAdditionalInfo();			
			
			Object tempData = tempMap.get(Environment.MEMBER_KEY);
								
			MemberDao sessDao = (MemberDao)tempData;  //(MemberDao)(cmsuser.getAdditionalInfo().get(Environment.MEMBER_KEY));
			
			synchronized(sessDao){
				
				if (memberDao.get(MemberDao.AccruedPension) != null && memberDao.get(MemberDao.AccruedPension) != ""){
					sessDao.set(MemberDao.AccruedPension, StringUtil.getString(memberDao.get(MemberDao.AccruedPension)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.AccruedPension, memberDao.get(MemberDao.AccruedPension));
				}
				if (memberDao.get(MemberDao.EgpCash) != null && memberDao.get(MemberDao.EgpCash) != ""){
					sessDao.set(MemberDao.EgpCash, StringUtil.getString(memberDao.get(MemberDao.EgpCash)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.EgpCash, memberDao.get(MemberDao.EgpCash));
				}
				if (memberDao.get(MemberDao.SrpCash) != null && memberDao.get(MemberDao.SrpCash) != ""){
					sessDao.set(MemberDao.SrpCash, StringUtil.getString(memberDao.get(MemberDao.SrpCash)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.SrpCash, memberDao.get(MemberDao.SrpCash));
				}
				if (memberDao.get(MemberDao.TaxFreeCash) != null && memberDao.get(MemberDao.TaxFreeCash) != ""){
					sessDao.set(MemberDao.TaxFreeCash, StringUtil.getString(memberDao.get(MemberDao.TaxFreeCash)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.TaxFreeCash, memberDao.get(MemberDao.TaxFreeCash));
				}
				if (memberDao.get(MemberDao.TaxableCash) != null && memberDao.get(MemberDao.TaxableCash) != ""){
					sessDao.set(MemberDao.TaxableCash,StringUtil.getString(memberDao.get(MemberDao.TaxableCash)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.TaxableCash, memberDao.get(MemberDao.TaxableCash));
				}
				if (memberDao.get(MemberDao.TaxPayable) != null && memberDao.get(MemberDao.TaxPayable) != ""){
					sessDao.set(MemberDao.TaxPayable, StringUtil.getString(memberDao.get(MemberDao.TaxPayable)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.TaxPayable, memberDao.get(MemberDao.TaxPayable));
				}
				if (memberDao.get(MemberDao.RrReducedPension) != null && memberDao.get(MemberDao.RrReducedPension) != ""){
					sessDao.set(MemberDao.RrReducedPension, StringUtil.getString(memberDao.get(MemberDao.RrReducedPension)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.RrReducedPension, memberDao.get(MemberDao.RrReducedPension));
				}
				if (memberDao.get(MemberDao.RrSpousesPension) != null && memberDao.get(MemberDao.RrSpousesPension) != ""){
					sessDao.set(MemberDao.RrSpousesPension, StringUtil.getString(memberDao.get(MemberDao.RrSpousesPension)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.RrSpousesPension, memberDao.get(MemberDao.RrSpousesPension));
				}
				if (memberDao.get(MemberDao.RrMaxLumpSum) != null && memberDao.get(MemberDao.RrMaxLumpSum) != ""){
					sessDao.set(MemberDao.RrMaxLumpSum, StringUtil.getString(memberDao.get(MemberDao.RrMaxLumpSum)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.RrMaxLumpSum, memberDao.get(MemberDao.RrMaxLumpSum));
				}
				if (memberDao.get(MemberDao.RrResidualPension) != null && memberDao.get(MemberDao.RrResidualPension) != ""){
					sessDao.set(MemberDao.RrResidualPension, StringUtil.getString(memberDao.get(MemberDao.RrResidualPension)));
					memDebug("BpAcRdConsumer.storeInfoOnSession: ", this, MemberDao.RrResidualPension, memberDao.get(MemberDao.RrResidualPension));
				}
			}
		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.storeInfoOnSession error:",ex);			
		}
	}

	/** 
	 * check whether Session row has been created 
	 */
	private boolean checkCentralSession(String userid, String password, String bGroup, String agcode){
		boolean sessionExist = false;
		
		String sqlInsert = " select * from central_session where userid = '"+userid+"' and password = '"+password+"' and " +
				"bgroup = '"+bGroup+"' and agcode = '"+agcode+"'";
		
		Connection con = null;
		Statement pstm = null;
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
						
			pstm = con.createStatement();	
			
			ResultSet rs;
			rs = pstm.executeQuery(sqlInsert);
			
			if (rs.next()) {
				sessionExist = true;
			}

		} catch (Exception e) {
			LOG.info("BpAcRdConsumer: checkCentralSession error:" + e.getMessage());
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					LOG.debug("BpAcRdConsumer: CheckCentralSession can not close connection cause by :- ",e);
			        LOG.error("BpAcRdConsumer: CheckCentralSession can not close connection cause by :- "+e.getMessage());
				}
			}
		}
		return sessionExist;
	}
	
	/**
	 * Insert into central_session table
	 * @param userid - userid
	 * @param password - password
	 * @param bGroup - Bgroup
	 * @param agcode - AGCODE
	 */
	private void inserIntoSession(String userid, String password,
			String bGroup, String agcode) 
	{
		String sqlInsert = " insert into central_session (userid,password,session_number,start_date,start_time, bgroup,logon_id,AGCODE)"
				+ "values (?,?,0,(SELECT TO_CHAR(SYSDATE,'DD-MON-YYYY') systemdate FROM dual),'00:00:00',?,'INTERNAL',?)";
		
		
		Connection con = null;
		PreparedStatement pstm = null;
		boolean sessionExist = checkCentralSession(userid, password, bGroup, agcode);
		
		if (sessionExist ==false )
		{
			try {
	
				DBConnector connector = DBConnector.getInstance();
				con = connector.getDBConnFactory(Environment.AQUILA);
				con.setAutoCommit(false);
				pstm = con.prepareStatement(sqlInsert);
				pstm.setString(1, userid);
				pstm.setString(2, password);
				pstm.setString(3, bGroup);
				pstm.setString(4, agcode);
				pstm.executeUpdate();
				con.commit();
				con.setAutoCommit(true);
	
			} catch (Exception e) {
				LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.inserIntoSession error:",e);
				
				try {
					con.rollback();
				} catch (Exception ex) {
					LOG.debug("BpAcRdConsumer: inserIntoSession can not close connection cause by :- ",e);
			        LOG.error("BpAcRdConsumer: inserIntoSession can not close connection cause by :- "+e.getMessage());
				}
			} finally {
				if (con != null) {
					try {
						DBConnector connector = DBConnector.getInstance();
						connector.close(con);//con.close();
					} catch (Exception e) {
						LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.inserIntoSession can not close connection cause by:",e);
					}
				}
			}
		}
	}

	

	/**
	 *  Insert into calc_input table
	 * @param calType - CalcType
	 * @param refNo - Refno
	 * @param bGroup - Bgroup
	 * @param rdDate - CIND01
	 */
	private void insertCalcInput(String calType, String refNo, String bGroup, java.sql.Date rdDate) 
	{
		String sqlInsert = "insert into calc_input(CalcType,Refno,BGroup,CIND01, CINN50, CINN58) values(?,?,?,?,0, 1) ";
		//System.out.println("insertCalcInput():BEGIN");
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			//con = DBConnector.getInstance().getDirectConnection();
			//con = DBConnector.getInstance().getJNDIConnection("AquilaDB");
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);

			pstm = con.prepareStatement(sqlInsert);
			pstm.setString(1, calType);
			pstm.setString(2, refNo);
			pstm.setString(3, bGroup);
			pstm.setDate(4, rdDate);
			//System.out.println("insertCalcInput():EXECUTE");
			pstm.executeUpdate();
			//boolean ok = pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			//System.out.println("insertCalcInput():END ("+ok+")");
		} catch (Exception e) {
			//releaseLock(bGroup, refNo, calType);
			e.printStackTrace();
			LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.insertCalcInput error :",e);
			
			try {
				con.rollback();
			} catch (Exception ex) {
				LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.insertCalcInput can not rollback cause by :",ex);
			}
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					LOG.debug("BpAcRdConsumer: insertCalcInput can not close connection cause by :- ",e);
			        LOG.error("BpAcRdConsumer: insertCalcInput can not close connection cause by :- "+e.getMessage());
				}
			}
		}
	}

	/**
	 * Delete from calc_input table
	 * @param userid - Refno
	 * @param calctype - Calctype
	 * @param bgroup - Bgroup
	 */
	private void deleteCal(String userid, String calctype, String bgroup) {
		String sqlDelete = "Delete from calc_input where refno =? and  calctype = ? and  BGROUP = ? ";
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm = con.prepareStatement(sqlDelete);
			pstm.setString(1, userid);
			pstm.setString(2, calctype);
			pstm.setString(3, bgroup);
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
			//releaseLock(bgroup, userid, calctype);
			LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.deleteCal error :",e);
			try {
				con.rollback();
			} catch (Exception ex) {
				LOG.error("com.bp.pensionline.calc.consumerBpAcRdConsumer.deleteCal can not rollback cause by :",e);
			}
			
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					LOG.debug("BpAcRdConsumer: deleteCal can not close connection cause by :- ",e);
			        LOG.error("BpAcRdConsumer: deleteCal can not close connection cause by :- "+e.getMessage());
				}
			}
		}

	}
	
	/**
	    * Delete from all the clac output tables before running
	    *
	    *     CALC_OUTPUT
	    *    CALC_OUTPUT_2
	    *    CALC_OUTPUT_3
	    *     CALC_OUTPUT_4
	    *     CALC_OUTPUT_5
	    *
	    * @param userid
	    * @param calctype
	    * @param bgroup
	    *            Delete from calc_input table
	    */
	   private void deleteCalOutputRecord( String tablename, String userid, String calctype, String bgroup) {
	             String sqlDelete = "Delete from "+tablename+" where refno =? and  calctype = ? and  Bgroup = ? ";
	       Connection con = null;
	       PreparedStatement pstm = null;
	       try {

	           DBConnector connector = DBConnector.getInstance();
	           con = connector.getDBConnFactory(Environment.AQUILA);
	           con.setAutoCommit(false);
	           pstm = con.prepareStatement(sqlDelete);
	           pstm.setString(1, userid);
	           pstm.setString(2, calctype);
	           pstm.setString(3, bgroup);
	           pstm.execute();
	           con.commit();
	           con.setAutoCommit(true);
	           //System.out.println("deleteCal has been done!!!");
	       } catch (Exception e) {
	    	   //releaseLock(bgroup, userid, calctype);
	    	   
	           // TODO: handle exception
	           try {
	               con.rollback();
	           } catch (Exception ex) {
	               // TODO: handle exception
	        	   LOG.error("BpAcRdConsumer: deleteCalOutputRecord - "+ex.getMessage()); 
	           }
	           
	           LOG.debug("BpAcRdConsumer: deleteCalOutputRecord - ",e);
	           LOG.error("BpAcRdConsumer: deleteCalOutputRecord - "+e.getMessage());
	       } finally {
	           if (con != null) {
	               try {
	            	   DBConnector connector = DBConnector.getInstance();
					   connector.close(con);
	               } catch (Exception e) {
	            	   LOG.error("Unable to close DB connection for user: "+userid+", bgroup:"+bgroup+", calctype:"+calctype, e);
	               }
	           }
	       }

	   }
	 
	   /**
	    * Delete from all the clac output tables before running
	    *
	    *    CALC_OUTPUT
	    *    CALC_OUTPUT_2
	    *    CALC_OUTPUT_3
	    *    CALC_OUTPUT_4
	    *    CALC_OUTPUT_5
	    *
	    * @param userid
	    * @param calctype
	    * @param bgroup
	    *            Delete from calc_input table
	    */
	   private void deleteCalOutput( String userid, String calctype, String bgroup) 
	   {
		   // Modify by Huy: remove try catch block. Add deleteCalOutputRecord( "CALC_ERRORS", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_2", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_3", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_4", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_OUTPUT_5", userid, calctype, bgroup);
		   deleteCalOutputRecord( "CALC_ERRORS", userid, calctype, bgroup);
	   }  
	/**
	 * @param bGroup
	 * @param userName
	 * @param refNo
	 * @param calcType
	 * @param password
	 * @param status
	 * Run Package in Oracle Databse
	 */
	private void runProcess(String bGroup, String userName, String refNo,
			String calcType, String password) {
		
		
		CallableStatement cs = null;
		Connection con = null;
		try {

            String procedureQuery = "declare returnStatus NUMBER; errorInfo BP1.pmsapi_err.error_info_tab_type;"
                                    +"begin BP1.PMSAPI_CALC.run_process(?, ?, ?,?, ?,returnStatus, errorInfo); end;";
            LOG.info("GOING TO RUN PMSAPI_CALC.run_process("+bGroup+", "+userName+", "+password+", "+refNo+","+calcType);			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			cs = con.prepareCall(procedureQuery);
			cs.setString(1, bGroup);
			cs.setString(2, userName);
			cs.setString(3, password);
			cs.setString(4, refNo);
			cs.setString(5, calcType);
					   	
			System.out.println("begin  BP1.PMSAPI_CALC.run_process("+bGroup+", "+userName+", "+password+", "+refNo+","+calcType+",?,?); end;");			
			cs.execute();
			con.commit();
			con.setAutoCommit(true);
			//System.out.println("runProcess has been done !!!");
		} catch (Exception e) {
			//releaseLock(bGroup, refNo, calcType);
			
			e.printStackTrace();
			LOG.info("BpAcRdConsumer, runProcess:"+e.getMessage());
			try {
				con.rollback();
			} catch (Exception ex) {
				LOG.info("BpAcRdConsumer, runProcess (rollback):"+ex.getMessage());
			}
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
					
				} catch (Exception e) {
					// TODO: handle exception
					LOG.info("BpAcRdConsumer, runProcess (finally): "+e.getMessage());
				}
			}
		}

	}

	
	@Override
	public void deleteAdministratorLocks(String refNo, String bGroup) {
		   // delete from lock_table where bgroup = $bgroup and lock_refno = (select crefno from basic where refno = refno and bgroup = bgroup);
	       String sqlInsert = "delete from lock_table where bgroup = ? and lock_refno = (select crefno from basic where refno = ? and bgroup = ?)";

	       Connection con = null;
	       PreparedStatement pstm = null;
	       try {

	           DBConnector connector = DBConnector.getInstance();
	           con = connector.getDBConnFactory(Environment.AQUILA);
	           con.setAutoCommit(false);
	           pstm = con.prepareStatement(sqlInsert);
	           pstm.setString(1, bGroup);
	           pstm.setString(2, refNo);
	           pstm.setString(3, bGroup);
	           pstm.execute();
	           con.commit();
	           con.setAutoCommit(true);

	           LOG.debug("deleteFromSession has been done !!! refno: "+refNo+", bgroup:"+bGroup);
	       } catch (Exception e) {
	           // TODO: handle exception
	           try {
	               con.rollback();
	           } catch (Exception ex) {
	               // TODO: handle exception
	               ex.printStackTrace();
	           }
	           LOG.debug("deleteFromSession issue !!! refno: "+refNo+", bgroup:"+bGroup, e);
	           e.printStackTrace();
	       } finally {
	           if (con != null) {
	               try {
	            	   DBConnector connector = DBConnector.getInstance();
	            	   connector.close(con);//con.close();
	               } catch (Exception e) {
	                   // TODO: handle exception
	               }
	           }
	       }
	   }

	/**
	 * Quick debug method for info from calcs 
	 * 
	 * @param objRef
	 * @param value
	 * @param attrib
	 */
	public void memDebug(String ref, CalcConsumer objRef, String attrib, String value){
		if (attrib == null || attrib == ""){
			LOG.info( objRef.getName()+"("+ref+")"+"[1]CR-ref: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb: IS NULL or EMPTY STRING" );
		}else if (value == null || value == ""){
			LOG.info( objRef.getName()+"("+ref+")"+"[2]CR-ref:: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb:"+ attrib+ ", Val= IS NULL or EMPTY STRING" );
		}else{
			LOG.info( objRef.getName()+"("+ref+")"+"[3]CR-ref: "+memberDao.get(""+Environment.MEMBER_REFNO)+", "+memberDao.get(""+Environment.MEMBER_BGROUP)+"  Attb:"+ attrib+ ", Val= "+ value );
		}
	}

}

