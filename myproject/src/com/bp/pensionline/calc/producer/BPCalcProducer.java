package com.bp.pensionline.calc.producer;

import java.sql.Date;

import com.bp.pensionline.dao.MemberDao;

public abstract class BPCalcProducer {
	
	/*
	 * Default java bean constructor
	 */
	public BPCalcProducer(){}
	
	public abstract void runCalc(MemberDao member);
	public abstract MemberDao runCalc(MemberDao member, Date DoR, int accrual_rate, double cash);
	public abstract MemberDao runCalc(MemberDao member, Date DoR);
	
	
}
