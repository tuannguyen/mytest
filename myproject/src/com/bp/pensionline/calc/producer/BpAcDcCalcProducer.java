package com.bp.pensionline.calc.producer;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.calc.consumer.BpAcDcConsumer;
import com.bp.pensionline.dao.MemberDao;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 15/05/2007
 *
 */
public class BpAcDcCalcProducer extends BPCalcProducer {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	@Override
	public void runCalc(MemberDao member) {

		try {
			
			BpAcDcConsumer thread = new BpAcDcConsumer();

			/*MemberDao mem = (MemberDao) member.clone();*/

			thread.setMemberDao(member);

			thread.start();
			
			thread.join();

		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.calc.producer.BpAcDcCalcProducer.runCalc error: ",ex);
		}

	}

	/*
	 * Default Java Bean Constructor
	 */
	public BpAcDcCalcProducer() {
		super();

	}
	/*
	* TODO IT IS NOT CORRECT
	*/
	public MemberDao runCalc(MemberDao member, Date DoR, int accrual_rate,
			double cash) {
		BpAcDcConsumer consumer = new BpAcDcConsumer();
		consumer.setMemberDao(member);
		MemberDao memberTemp=consumer.runCalculate(DoR, accrual_rate, cash);
		return memberTemp;

	}
	public MemberDao runCalc(MemberDao member, Date DoR){return null;};
}
