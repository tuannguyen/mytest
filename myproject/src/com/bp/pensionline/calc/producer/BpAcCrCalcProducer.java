package com.bp.pensionline.calc.producer;

import java.io.ByteArrayInputStream;
import java.sql.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.calc.consumer.BpAcCrConsumer;
import com.bp.pensionline.calc.consumer.BpAcCrHeadroomConsumer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 15/05/2007
 * 
 * @author Huy Tran
 * @version 1.3
 * @since 22/06/2012
 * Replace Aquila CR Calc by using Headroom
 *
 */
public class BpAcCrCalcProducer extends BPCalcProducer {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao)
	 * Run calculate by thread
	 */
	@Override
	public void runCalc(MemberDao member) {

		try {
			BpAcCrHeadroomConsumer thread = new BpAcCrHeadroomConsumer();

			thread.setMemberDao(member);
			
			String sessionId = member.get("SessionId");
			CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);
			if (cmsuser != null)
			{
				MemberDetail memberDetail = (MemberDetail)cmsuser.getAdditionalInfo().get(Environment.MEMBER_DETAIL_KEY);
				thread.setMemberDetail(memberDetail);
			}			

			thread.start();
			
			thread.join();

		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.calc.producer.BpAcCrCalcProducer.runCalc error: ",ex);
			
		}

	}

	/*
	 * Default Java Bean Constructor
	 */
	public BpAcCrCalcProducer() {
		super();

	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao, java.sql.Date, int, double)
	 * Run calculate directly
	 */
	@Override
	public MemberDao runCalc(MemberDao member, Date DoR, int accrualRate, double cash) 
	{
		MemberDao memberTemp = null;
		try 
		{
			BpAcCrHeadroomConsumer acCrHeadroomConsumer = new BpAcCrHeadroomConsumer();

			acCrHeadroomConsumer.setMemberDao(member);
			
			String sessionId = member.get("SessionId");
			CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);
			if (cmsuser != null)
			{
				MemberDetail memberDetail = (MemberDetail)cmsuser.getAdditionalInfo().get(Environment.MEMBER_DETAIL_KEY);
				acCrHeadroomConsumer.setMemberDetail(memberDetail);
			}			

			memberTemp = acCrHeadroomConsumer.calculate(DoR, accrualRate, cash);
			

		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.calc.producer.BpAcCrCalcProducer.runCalc error: ",ex);
			
		}
		
		return memberTemp;

	}
	
	public MemberDao runAdjustCalcUnitTest (String bGroup, String refNo, int currentAccRate,
			Date DoR, double lta, Date overrideAccDate, int overrideAccRate, double cash, boolean isAdjust) 
	{
		BpAcCrConsumer consumer = new BpAcCrConsumer();		
		
		MemberDao memberTemp = null;
		
		if (isAdjust)
		{
			memberTemp = consumer.calculateAdjustUnitTest(
					bGroup, refNo, Environment.AGCODE, DoR, currentAccRate, lta, 
					cash, overrideAccDate, overrideAccRate, true, BpAcCrCalcProducer.getConfiguredReducedPensionColName());
		}
		else
		{
			memberTemp = consumer.calculateAdjustUnitTest(
					bGroup, refNo, Environment.AGCODE, DoR, overrideAccRate, lta, 
					cash, overrideAccDate, overrideAccRate, false);			
		}

		
		return memberTemp;

	}	
	
	public MemberDao runCalc(MemberDao member, Date DoR){return null;};
	
	/**
	 * Check if global calc adjust is set in XML
	 * @return
	 */
	public static boolean isCalcAdjustUsed() 
	{
		boolean calcAdjustUsed = false;
		try 
		{
			XmlReader reader = new XmlReader();// Create object read xml file (CalculationMapping.xml)

			ByteArrayInputStream btArr = new ByteArrayInputStream(
					reader.readFile(Environment.CALC_ADJUST_FLAG_FILE));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();

			// get calc list
			NodeList calcList = root.getElementsByTagName("Adjust");
			
			int listLength = calcList.getLength();
			if (listLength > 0)
			{
				Node calcAdjustNode = calcList.item(0);
				if (calcAdjustNode != null && calcAdjustNode.getNodeType() == Node.ELEMENT_NODE)
				{
					String adjust = calcAdjustNode.getTextContent();
					LOG.info("adjust: " + adjust);
					if (adjust != null && adjust.equalsIgnoreCase("TRUE"))
					{
						return true;
					}
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in getting global cacl adjust flag: " + e);
		}
		
		return calcAdjustUsed;
	}
	
	/**
	 * Check if global calc adjust is set in XML
	 * @return
	 */
	public static String getConfiguredReducedPensionColName() 
	{
		String configuredReducedPensionColName = null;
		try 
		{
			XmlReader reader = new XmlReader();// Create object read xml file (CalculationMapping.xml)

			ByteArrayInputStream btArr = new ByteArrayInputStream(
					reader.readFile(Environment.CALC_ADJUST_FLAG_FILE));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();

			// get calc list
			NodeList calcList = root.getElementsByTagName("ReducedPensionColName");
			
			int listLength = calcList.getLength();
			if (listLength > 0)
			{
				Node calcAdjustNode = calcList.item(0);
				if (calcAdjustNode != null && calcAdjustNode.getNodeType() == Node.ELEMENT_NODE)
				{
					configuredReducedPensionColName = calcAdjustNode.getTextContent();					
				}
			}
		}
		catch (Exception e) 
		{
			LOG.error("Error in getting getConfiguredReducedPensionColName: " + e);
		}
		
		return configuredReducedPensionColName;
	}	
}
