/**
 * 
 */
package com.bp.pensionline.calc.producer;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.calc.consumer.BpAcRdConsumer;
import com.bp.pensionline.calc.consumer.BpAcRrConsumer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;

/**
 * @author Tu Nguyen
 *
 */
public class BpAcRdCalcProducer extends BPCalcProducer {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * 
	 */
	public BpAcRdCalcProducer() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao)
	 */
	@Override
	public void runCalc(MemberDao member) {
		// TODO Auto-generated method stub
		try {
			BpAcRdConsumer consumer =new BpAcRdConsumer();
			MemberDao mem=(MemberDao)member.clone();
			consumer.setMemberDao(mem);
			consumer.start();
			consumer.join();
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.calc.producer.BpAcRdCalcProducer.runCalc error: ",e);
		}
		

	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao, java.sql.Date, int, double)
	 */
	@Override
	public MemberDao runCalc(MemberDao member, Date DoR, int accrual_rate,
			double cash) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao, java.sql.Date)
	 */
	@Override
	public MemberDao runCalc(MemberDao member, Date DoR) {
		// TODO Auto-generated method stub
		try {
			BpAcRdConsumer consumer=new BpAcRdConsumer();
			// set Calc Type for Redundancy Planner
			member.set(Environment.MEMBER_CALCTYPE, Environment.RD_CALCTYPE);
			//set Agcode to member dao
			member.set(Environment.MEMBER_AGCODE, Environment.AGCODE);
			consumer.setMemberDao(member);
			/*
			Huy modified
			consumer.start();
			consumer.join();
			MemberDao result=consumer.getMemberDao();
			*/
			MemberDao result=consumer.runCalculate(DoR);
			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}

}
