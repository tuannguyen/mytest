/**
 * 
 */
package com.bp.pensionline.calc.producer;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.calc.consumer.BpAcWcConsumer;
import com.bp.pensionline.dao.MemberDao;

/**
 * @author Duy Thao Nguyen
 * @since 15/5/2007
 * @version 1.0
 *
 */
public class BpAcWcCalcProducer extends BPCalcProducer {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao)
	 */
	@Override
	public void runCalc(MemberDao member) {

		try {
			
			
			BpAcWcConsumer thread = new BpAcWcConsumer();

			/*MemberDao mem = (MemberDao) member.clone();*/

			thread.setMemberDao(member);

			thread.start();
			thread.join();

		} catch (Exception ex) {
			LOG.error("com.bp.pensionline.calc.producer.BpAcRrCalcProducer.runCalc error: ",ex);
			
		}

	}

	/* (non-Javadoc)
	 * @see com.bp.pensionline.calc.producer.BPCalcProducer#runCalc(com.bp.pensionline.dao.MemberDao, java.sql.Date, int, double)
	 */
	@Override
	public MemberDao runCalc(MemberDao member, Date DoR, int accrual_rate,
			double cash) {
		BpAcWcConsumer consumer = new BpAcWcConsumer();
		consumer.setMemberDao(member);
		MemberDao memberTemp=consumer.runCalculate(DoR, accrual_rate, cash);
		return memberTemp;

	}

	public BpAcWcCalcProducer() {
		super();
	}
	public MemberDao runCalc(MemberDao member, Date DoR){return null;};

}
