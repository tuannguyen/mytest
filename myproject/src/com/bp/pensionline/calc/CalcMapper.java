package com.bp.pensionline.calc;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.StringUtil;

/*
 * 
 * Author: CuongDV
 * Date: 10/05/2007
 * Load data in the file CalculationMapping.xml and store it in objects of CalcInfo class
 */

public class CalcMapper {

	static final String CALC_LIST = "CalcList";

	static final String CALCULATION = "Calculation";

	static final String CALC = "Calc";

	static final String SCHEME_CODE = "SchemeCode";

	static final String BGROUP = "Bgroup";

	static final String MEMBERSHIP_STATUS = "MembershipStatus";

	static final String ADMIN = "Admin";

	static final String PL = "PL";

	static final String DESC = "Desc";

	static final String MEMBER_DAO = "MemberDAO";

	static final String CALC_PRODUCER = "CalcProducer";

	static final String AGCODE = "Agcode";

	private ArrayList<CalcInfo> calcInfos;

	/**
	 * public contructor
	 */
	public CalcMapper() {
		// create array list to store calcs
		calcInfos = new ArrayList<CalcInfo>();
		// read xml data
		this.loadCalc();
	}

	/*
	 * load calc data from CalculationMapping.xml file and store it in list of calcInfo object.
	 * calcInfo object has properties:
	 * 	+schemeCode : member's shemeCode
	 *  +bgroup : meber's bgroup
	 *  +membershipStatus : member's membershipStatus
	 *  +admin: admin code (CR)
	 *  +pl: pl code (CR)
	 *  +desc: description of calculation node
	 *  +memberDao : memberDao class ( com.bp.pensionline.dao.MemberDao)
	 *  +calcProducer : producer of each calculation node ( com.bp.pensionline.calc.producer.BpAcCrCalcProducer)
	 *  +agcode: member's agcode
	 */
	private void loadCalc() {
		try {
			XmlReader reader = new XmlReader();// Create object read xml file (CalculationMapping.xml)

			ByteArrayInputStream btArr = new ByteArrayInputStream(reader
					.readFile(Environment.CALCMAPPER_FILE));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();

			// get calc list
			NodeList calcList = root.getElementsByTagName(CALC_LIST);// get calculation list (CaclList)
			

			int listLength = calcList.getLength();
			// 

			for (int p = 0; p < listLength; p++) {// iterate calcList

				Node list = calcList.item(p);

				if (list != null) {

					NodeList childs = list.getChildNodes();

					int childLength = childs.getLength();
			

					String _bgroup = new String("");
					String _schemecode = new String("");
					String _status = new String("");
					
					//

					for (int m = 0; m < childLength; m++) {// iterate clacList's child

						Node childNode = childs.item(m);

						if (childNode.getNodeType() == Node.ELEMENT_NODE) {

							if (childNode.getNodeName().equals(SCHEME_CODE)
									&& childNode.hasChildNodes()) {

								_schemecode = childNode.getFirstChild()
										.getNodeValue().trim();

							} else if (childNode.getNodeName().equals(BGROUP)
									&& childNode.hasChildNodes()) {

								_bgroup = childNode.getFirstChild()
										.getNodeValue().trim();

							} else if (childNode.getNodeName().equals(
									MEMBERSHIP_STATUS)
									&& childNode.hasChildNodes()) {

								_status = childNode.getFirstChild()
										.getNodeValue().trim();

							} else if (childNode.getNodeName().equals(
									CALCULATION)
									&& childNode.hasChildNodes()) {
								CalcInfo calcIn = new CalcInfo();//create new CalcInfo object to stored data

								calcIn.setSchemeCode(_schemecode);

								calcIn.setBgroup(_bgroup);

								calcIn.setMembershipStatus(_status);

								NodeList itemList = childNode.getChildNodes();

								int itemLength = itemList.getLength();

								// traverse Calc node to get items value
								for (int i = 0; i < itemLength; i++) {

									Node item = itemList.item(i);

									if (item.getNodeType() == Node.ELEMENT_NODE) {

										if (item.getNodeName().equals(ADMIN)&& item.hasChildNodes()) { // if node equal Admin node and have child

											calcIn.setAdmin(item
													.getFirstChild()
													.getNodeValue().trim());

										} else if (item.getNodeName().equals(PL)
												&& item.hasChildNodes()) {

											calcIn.setPl(item.getFirstChild()
													.getNodeValue().trim());

										} else if (item.getNodeName().equals(
												DESC)
												&& item.hasChildNodes()) {

											calcIn.setDesc(item.getFirstChild()
													.getNodeValue().trim());

										} else if (item.getNodeName().equals(
												MEMBER_DAO)
												&& item.hasChildNodes()) {

											calcIn.setMemberDao(item
													.getFirstChild()
													.getNodeValue().trim());

										} else if (item.getNodeName().equals(
												CALC_PRODUCER)
												&& item.hasChildNodes()) {

											calcIn.setCalcProducer(item
													.getFirstChild()
													.getNodeValue().trim());

										} else if (item.getNodeName().equals(
												AGCODE)
												&& item.hasChildNodes()) {

											calcIn.setAgcode(item
													.getFirstChild()
													.getNodeValue().trim());

										}
									}

									

								}
								
								// add new CalcInfo object to list
								calcInfos.add(calcIn);
							}

						}

					}

				}

				// close stream
				btArr.close();

			}
			
		} catch (SAXParseException err) {

			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {

			Exception x = e.getException();

			((x == null) ? e : x).printStackTrace();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	/**
	 * use the calcInfos list to get list of producer
	 * @param bgroup
	 * @param schemeCode
	 * @param membershipStatus
	 * @return list of producers of each member's record base on bgroup, schemecode and memberShipStatus (BPF,0001,PP)
	 */
	public List <CalcMapper.CalcInfo> getProducers(String bgroup,
			String schemeCode, String membershipStatus){
		List <CalcMapper.CalcInfo> result=new ArrayList <CalcMapper.CalcInfo>();
		try {

			if (calcInfos.size() > 0) {
				Iterator<CalcInfo> iter = calcInfos.iterator();
				for (; iter.hasNext();) {

					CalcInfo calc = (CalcInfo)iter.next();

					// check if match condition
					if (!calc.getBgroup().equals(StringUtil.EMPTY_STRING)
							&& calc.getBgroup().equals(bgroup)
							&& !calc.getSchemeCode().equals(
									StringUtil.EMPTY_STRING)
							&& calc.getSchemeCode().equals(schemeCode)
							&& !calc.getMembershipStatus().equals(
									StringUtil.EMPTY_STRING)
							&& calc.getMembershipStatus().equals(
									membershipStatus)) {
						
			
						result.add((CalcInfo) calc.clone());

					} 

				}
			}
			
			return result;
			
		} catch (Exception ex) {

			ex.printStackTrace();
			return null;

		}
		
		
	}

	/*
	 * 
	 * this function get bgroup, schemecode, membership status, agcode to
	 * compare with the one in CaculationMapping.xml file @return: a String
	 * contains qualified class name of CalcProducer ex:
	 * com.bp.pensionline.calc.producer.BPCalcProducer
	 * 
	 */
	public CalcMapper.CalcInfo getCalcProducer(String bgroup,
			String schemeCode, String membershipStatus) {
		try {

			
			if (calcInfos.size() > 0) {

				Iterator<CalcInfo> iter = calcInfos.iterator();

				for (; iter.hasNext();) {

					CalcInfo calc = iter.next();

					// check if match condition
					if (!calc.getBgroup().equals(StringUtil.EMPTY_STRING)
							&& calc.getBgroup().equals(bgroup)
							&& !calc.getSchemeCode().equals(
									StringUtil.EMPTY_STRING)
							&& calc.getSchemeCode().equals(schemeCode)
							&& !calc.getMembershipStatus().equals(
									StringUtil.EMPTY_STRING)
							&& calc.getMembershipStatus().equals(
									membershipStatus)) {
						/*System.out.println("\n\n\n ************* calc.getCalcProducer() = "+calc.getCalcProducer()+"************\n\n\n");*/

						return (CalcInfo) calc.clone();

					} else {

						continue;

					}

				}
			}
		} catch (Exception ex) {

			ex.printStackTrace();

		}

		return null;
	}

	/*
	 * 
	 * Information class to stored data in the CalculationMapping.xml file
	 * 
	 */
	public class CalcInfo implements Cloneable {

		@Override
		public Object clone() throws CloneNotSupportedException {
			// TODO Auto-generated method stub
			return super.clone();
		}

		/*
		 * instantiate empty value for all variables
		 */
		public CalcInfo() {

			schemeCode = StringUtil.EMPTY_STRING;
			bgroup = StringUtil.EMPTY_STRING;
			membershipStatus = StringUtil.EMPTY_STRING;
			admin = StringUtil.EMPTY_STRING;
			pl = StringUtil.EMPTY_STRING;
			desc = StringUtil.EMPTY_STRING;
			memberDao = StringUtil.EMPTY_STRING;
			calcProducer = StringUtil.EMPTY_STRING;
			agcode = StringUtil.EMPTY_STRING;

		}

		private String schemeCode;

		private String bgroup;

		private String membershipStatus;

		private String admin;

		private String pl;

		private String desc;

		private String memberDao;

		private String calcProducer;

		private String agcode;

		public String getAgcode() {
			return agcode;
		}

		public void setAgcode(String agcode) {
			this.agcode = agcode;
		}

		public String getAdmin() {
			return admin;
		}

		public void setAdmin(String admin) {
			this.admin = admin;
		}

		public String getBgroup() {
			return bgroup;
		}

		public void setBgroup(String bgroup) {
			this.bgroup = bgroup;
		}

		public String getCalcProducer() {
			return calcProducer;
		}

		public void setCalcProducer(String calcProducer) {
			this.calcProducer = calcProducer;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public String getMemberDao() {
			return memberDao;
		}

		public void setMemberDao(String memberDao) {
			this.memberDao = memberDao;
		}

		public String getMembershipStatus() {
			return membershipStatus;
		}

		public void setMembershipStatus(String membershipStatus) {
			this.membershipStatus = membershipStatus;
		}

		public String getPl() {
			return pl;
		}

		public void setPl(String pl) {
			this.pl = pl;
		}

		public String getSchemeCode() {
			return schemeCode;
		}

		public void setSchemeCode(String schemeCode) {
			this.schemeCode = schemeCode;
		}

		public String toString() {

			StringBuffer buffer = new StringBuffer();
			buffer.append("'").append(bgroup).append("' '").append(schemeCode)
					.append("' '").append(membershipStatus).append("' '")
					.append(admin).append("' '").append(pl).append("' '")
					.append(desc).append("' '").append(memberDao).append("' '")
					.append(calcProducer).append("' '").append(agcode).append(
							"'");

			return buffer.toString();

		}

	}

}
