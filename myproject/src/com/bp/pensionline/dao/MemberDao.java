package com.bp.pensionline.dao;

/* updated on 9_april_2007 by DC
 public interface MemberDao {	
 public String getMemberData(String key);
 
 public String toXML();
 }
 */
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.opencms.file.CmsUser;
import org.opencms.util.CmsUUID;

import com.bp.pensionline.constants.Environment;

/**
 * This object stores the information for a given Member, 
 * it does not perfom an operations directly.
 * it can convert itself to XML by Marshalling and unMarshalling
 *
 * @ToDO this should extend a universal DAO which implements:
 *   toString()
 *   toXML()
 *   get(KEY)
 *   set(KEY, VALUE)
 *   
 *   This should have no statics as it will be serializable
 *   This should have a private
 *   
 * @author Dominic Carlyle
 *
 */
public abstract class MemberDao implements Serializable, Cloneable {
	
	public static String NraPension = "NraPension";
    public static String NpaPension = "NpaPension";
    public static String Pension = "Pension";
    public static String PensionToDate = "PensionToDate";
    public static String NraUnreducedPension = "NraUnreducedPension";
    public static String NpaUnreducedPension = "NpaUnreducedPension";
    public static String UnreducedPension = "UnreducedPension";
    public static String NraReducedPension = "NraReducedPension";
    public static String NpaReducedPension = "NpaReducedPension";
    public static String ReducedPension = "ReducedPension";
    public static String NraSpousesPension = "NraSpousesPension";
    public static String NpaSpousesPension = "NpaSpousesPension";
    public static String SpousesPension = "SpousesPension";
    public static String NraCashLumpSum = "NraCashLumpSum";
    public static String NraCashLumpSumCurrency = "NraCashLumpSumCurrency";
    public static String NpaCashLumpSum = "NpaCashLumpSum";
    public static String NpaCashLumpSumCurrency = "NpaCashLumpSumCurrency";
    public static String CashLumpSum = "CashLumpSum";
    public static String CashLumpSumCurrency = "CashLumpSumCurrency";
    public static String NraMaximumCashLumpSum = "NraMaximumCashLumpSum";
    public static String NpaMaximumCashLumpSum = "NpaMaximumCashLumpSum";
    public static String NraMaximumCashLumpSumExact = "NraMaximumCashLumpSumExact";
    public static String NpaMaximumCashLumpSumExact = "NpaMaximumCashLumpSumExact";
    public static String MaximumCashLumpSum = "MaximumCashLumpSum";
    public static String MaximumCashLumpSumExact = "MaximumCashLumpSumExact";
    public static String PreCapPostReductionPension = "PreCapPostReductionPension";
    
    public static String NraPensionWithChosenCash = "NraPensionWithChosenCash";
    public static String NpaPensionWithChosenCash = "NpaPensionWithChosenCash";
    public static String PensionWithChosenCash = "PensionWithChosenCash";
    public static String NraPensionWithMaximumCash = "NraPensionWithMaximumCash";
    public static String NpaPensionWithMaximumCash = "NpaPensionWithMaximumCash";
    public static String PensionWithMaximumCash = "PensionWithMaximumCash";

    public static String Lta = "Lta";
    public static String PensionVsLta = "PensionVsLta";
    public static String TotalVsLta = "TotalVsLta";
    public static String NpaPensionvsSalary = "NpaPensionvsSalary";
    public static String NraPensionvsSalary = "NraPensionvsSalary";
    public static String UnreducedPensionVsSalary = "UnreducedPensionVsSalary";
    public static String ReducedPensionVsSalary = "ReducedPensionVsSalary";
    public static String Fps = "FPS";
    public static String BasicPs = "BasicPs";
    public static String PensionVsSalary = "PensionVsSalary";
    public static String PensionableSalary="PensionableSalary";
    public static String AvcVsLta ="AvcVsLta";
    public static String totalAvc ="totalAvc";
    public static String overfundIndicator ="overfundIndicator";
    public static String veraIndicator = "veraIndicator";
    public static String NraOverfundIndicator ="NraOverfundIndicator";
    public static String NpaOverfundIndicator ="NpaOverfundIndicator";
    public static String NraVeraIndicator = "NraVeraIndicator";
    public static String NpaVeraIndicator = "NpaVeraIndicator";
    
    
    public static String AccruedPension="AccruedPension";
    public static String EgpCash="EgpCash";
    public static String SrpCash="SrpCash";
    public static String TaxFreeCash="TaxFreeCash";
    public static String TaxableCash="TaxableCash";
    public static String TaxPayable="TaxPayable";
    public static String RrReducedPension="RrReducedPension";
    public static String RrSpousesPension="RrSpousesPension";
    public static String RrMaxLumpSum="RrMaxLumpSum";
    public static String RrResidualPension="RrResidualPension";
    public static String RrAccruedPension="RrAccruedPension";
    public static String RedundancyDate="RedundancyDate";
    public static String OverMinRetireAge="OverMinRetireAge";
    
    
    public static String DeathInServiceCash = "DeathInServiceCash";
    public static String DeathInServicePension = "DeathInServicePension";

	private static final long serialVersionUID = 1L;

	/** Attribute values stored in this Member */
	protected Map<String, String> valueMap;
	
	private CmsUUID cmsUUID = null;
	private CmsUser currentUser = null;

	/** default constructor */
	public MemberDao() {
		valueMap = new HashMap<String, String>();
	}

	/**
	 * constructor 
	 * @param refno
	 * @param bgroup
	 */
	public MemberDao(String refno, String bgroup) {

		valueMap = new HashMap<String, String>(); //Collections.synchronizedMap(new HashMap<String, String>());

		valueMap.put(Environment.MEMBER_REFNO, refno);
		valueMap.put(Environment.MEMBER_GROUP, bgroup);
	}

	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * @param valueMap
	 */
	public void replaceValueMap(Map<String, String> valueMap) {
		this.valueMap = valueMap;
	}

	
	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * NOTE: This does a deep copy of the contents.
	 * 
	 * @param valueMap
	 */
	public Map<String, String> getValueMap() {
		return this.valueMap;
	}
	
	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * NOTE: This does a deep copy of the contents.
	 * 
	 * @param valueMap
	 */
	public Map<String, String> getCopyOfValueMap() {
		Map deepCopyValueMap = Collections.synchronizedMap(new HashMap<String, String>());
		
		//copy each of the items in the map not the references to memory
		//http://www.java2s.com/Code/Java/Language-Basics/DeepCopyTest.htm
		
        Iterator iterator = this.valueMap.keySet().iterator();
        while (iterator.hasNext()) {        	
        	Object key = iterator.next();
        	deepCopyValueMap.put(new String(""+key), new String(""+ valueMap.get(key)));
        }
		
		return deepCopyValueMap;
	}

	/**
	 * get a value for an Attribute, this is named this way as we have migrated the ofshore code
	 * 
	 * @param key
	 * @return String value for the attribute
	 */
	public String getMemberData(String key) {

		return get(key);

	}

	/**
	 * get a value for an Attribute
	 * 
	 * @param key
	 * @return String value for the attribute
	 */

	public String get(String key) {

		Object obj = null;
		
		if(valueMap.containsKey(key)){
			
			obj = valueMap.get(key);	
			
			return obj == null ? new String("") : String.valueOf(obj);
			
		}else{
			
			
			return null;
			
		}
		
	}

	/**
	 * store a value for a given key
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		valueMap.put(key, value);
	}

	/**
	 * get the Refno
	 * 
	 * @return refno as a String
	 */
	public String getRefno() {
		return get(Environment.MEMBER_REFNO);
	}

	/**
	 * get the bgroup
	 * 
	 * @return bgroup
	 */
	public String getBgroup() {
		return get(Environment.MEMBER_GROUP);
	}

	/**
	 * Marshall this object into XML
	 * 
	 * @return
	 */
	public abstract String toXML();

	/**
	 * build Document creates a new instance of this object from an XML
	 * file
	 * 
	 * @param arr
	 */
	public abstract void buildDocument(byte[] arr);

	/**
	 * over ride to show the attribute value contents
	 */
	public String toString() {
		return this.valueMap.toString();
	}
	


	/**
	 * This is a shallow copy, but should be deep do a deep copy not shallow
	 */
	public abstract Object clone() throws CloneNotSupportedException; 
	
	
	
	/**
	 * deep copy the values of one map into another
	 * 
	 */
	public void addValues(Map tmpValues) {

		synchronized(this){
							
			//copy each of the items in the map not the references to memory
			//http://www.java2s.com/Code/Java/Language-Basics/DeepCopyTest.htm
			Iterator iterator = tmpValues.keySet().iterator();
			
	       // Iterator iterator = this.valueMap.keySet().iterator();
	        while (iterator.hasNext()) {        	
	        	Object key = iterator.next();
	        	valueMap.put(new String(""+key), new String(""+tmpValues.get(key)));
	        	//System.out.println(" MemberDao addValues ["+key+"], this="+this.get(""+key)+", memTemp="+tmpValues.get(""+key));
	        }
	        //System.out.println(" >>>>>> ["+Environment.MEMBER_REFNO+"], this="+this.get(""+Environment.MEMBER_REFNO)+", memTemp="+memTemp.get(""+Environment.MEMBER_REFNO));
			//deep copy the contents
		}
	}

	/**
	 * @param cmsUUID the cmsUUID to set
	 */
	public void setCmsUUID(CmsUUID cmsUUID) {
		this.cmsUUID = cmsUUID;
	}

	/**
	 * @return the cmsUUID
	 */
	public CmsUUID getCmsUUID() {
		return cmsUUID;
	}

	/**
	 * @param currentUser the currentUser to set
	 */
	public void setCurrentUser(CmsUser currentUser) {
		this.currentUser = currentUser;
	}

	/**
	 * @return the currentUser
	 */
	public CmsUser getCurrentUser() {
		return currentUser;
	}
}