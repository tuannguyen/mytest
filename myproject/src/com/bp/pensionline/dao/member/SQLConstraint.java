package com.bp.pensionline.dao.member;

import com.bp.pensionline.database.CaseWorkSQLHandler;

public class SQLConstraint {
	
	private String[] args;
	private String[] title;
	private String refno;
	private String bgroup;
	private StringBuffer whereClause=new StringBuffer("");
	
	public SQLConstraint(String refno, String bgroup){
		this.bgroup=bgroup;
		this.refno=refno;
	}
	
	/**
	 * Specify contrain for each given tableName
	 * 
	 * @param tableName
	 * 
	 * */
	
	public void constraintDefiner(String tableName){
		//for BASIC table
		if (tableName.equals("BASIC")
				||tableName.equals("WELFARE_DETAIL")){
			args=new String[2];
			title=new String[2];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
		}
		
		
		/**Add_static_data*/
		if (tableName.equals("ADD_STATIC_DATA")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'STATBEN'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="DATA_TYPE";
			
		}
		
		/**Add_static_data ---- new item added*/
		
		if (tableName.equals("ASD")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'PENLINE'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="DATA_TYPE";
			
		}
		
		
		/**
		 * @update 5.46pm 8May07
		 * tableName = CATEGORY_DETAIL
		 * 
		 * SAMPLE QUERY 
		 * select ConfigurationXML.ca03i as "NRA", nvl(CA71I, CA03I) as "NPA"
		 * from basic b, category_detail ConfigurationXML
		 * where ConfigurationXML.ca26x = b.ca26x
		 * and ConfigurationXML.bgroup = b.bgroup
		 * and b.bgroup = 'BPF'
		 * and b.refno = '0007264'

		 * 
		 * */
		if (tableName.equalsIgnoreCase("CATEGORY_DETAIL")){
			args=new String[4];
			title=new String[4];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="CATEGORY_DETAIL.BGROUP";
			args[3]="CATEGORY_DETAIL.CA26X";
			
			title[0]="BASIC.REFNO";
			title[1]="BASIC.BGROUP";
			title[2]="BASIC.BGROUP";
			title[3]="BASIC.CA26X";
			
		}
			
		/**
		 * where extuserid= (select LOWER(basic.bd08x)
		 * from basic where refno='0007144' and bgroup='BPF')
		 * extuserID is the lowercase of NiNo in Basic table
		 * 
		 * */
		
		if(tableName.equals("TS_EXTUSERREQ")){
			args=new String[1];
			title=new String[1];
			
			args[0]="(select LOWER(basic.BD08X) from basic " +
					"where refno='"+refno+"'AND bgroup='"+bgroup+"')";
			
			title[0]="TS_EXTUSERREQ.extuserid";
		}
		
		/**
		 * current salary or contribution must have the maximum value of seqno.
		 * 
		 * select *
		 * from old_scheme_detail
		 * where refno='0000648' and ca26x not in ('BP60','6060')
		 * */
		
		if( tableName.equals("OLD_SCHEME_DETAIL")){
			args=new String[5];
			title=new String[5];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'BP60'";
			args[3]="'6060'";
			args[4]="(select max(SEQNO) from "+tableName+" where REFNO='"+refno+"' " +
					"AND BGROUP='"+bgroup+"')";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="CA26X!";
			title[3]="CA26X!";
			title[4]="SEQNO";
			
		}

		/**
		 * SALARY_HISTORY, OLD_SCHEME_DETAIL, CONTRIBUTION_HISTORY, 
		 * 
		 * */
		
		if(tableName.equals("SALARY_HISTORY") || tableName.equals("OLD_SCHEME_DETAIL")||
				tableName.equals("CONTRIBUTION_HISTORY")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="(select max(SEQNO) from "+tableName+" where REFNO='"+refno+"' " +
					"AND BGROUP='"+bgroup+"')";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="SEQNO";
			
		}
		/**
		 * TvinBenefit
		 * select sum(ti22i) as days
		 * from transfer_in
		 * where bgroup=:BGROUP
		 * and refno=:REFNO
		 * and SUB in ('0014', '0015')

		 * */
		
		if (tableName.equals("TRANSFER_IN")){
			args=new String[2];
			title=new String[2];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"' and SUB in ('0014','0015')";
			
			title[0]="REFNO";
			title[1]="BGROUP";		
		}
		
		/**
		 * [[HeritageBenefit]]
		 *select sum(ti22i) as days
		 *from transfer_in
		 *where bgroup=:BGROUP
		 *and refno=:REFNO
		 *and SUB in ('0021')
		 * */
		if (tableName.equals("HTI")){
			args=new String[2];
			title=new String[2];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"' and SUB in ('0021')";
			
			title[0]="REFNO";
			title[1]="BGROUP";
		
		}
		/**
		 * [[AugmentedBenefit]] 
		 *select sum(ab06i) as days
		 *from augmentation_benefit
		 *where bgroup = :BGROUP
		 *and refno = :REFNO
		 *and ca26x = 'OWST'
		 * */
		
		if (tableName.equals("AAB")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'OWST'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="CA26X";
		
		}
		
		/**
		 * [[EasternServiceBenefit]] 
		 * select sum(ab06i) as days
		 * from augmentation_benefit
		 * where bgroup = :BGROUP
		 * and refno = :REFNO
		 * and ca26x = 'EAST'
		 * */
		if (tableName.equals("AUGMENTATION_BENEFIT")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'EAST'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="CA26X";
		
		}
		
		/**
		 * Check if EoWLastChangedDate is the latest change EoW
		 * seqno is the maximum value whilst ND19D is not NULL
		 * 
		 */
		 
		 if(tableName.equals("NOMINATION_DETAIL")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="(select max(SEQNO) from "+tableName+" where REFNO='"+refno+"' " +
					"AND BGROUP='"+bgroup+"' and ND19D is null)";
			
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="SEQNO";
			
		 }
		 
		 /**
		 * PS_ADDRESSDETAILS
		 * from ps_address psa, ps_addressdetails psad
		 * where psa.bgroup=psad.bgroup
		 * and psa.addno=psad.addno
		 * and psa.bgroup = XXX
		 * and psa.refno = XXX
		 * and psa.endd=null
		 * */

		if(tableName.equals("PS_ADDRESSDETAILS")){
			args=new String[5];
			title=new String[5];
			
			args[0]="ps_addressdetails.bgroup";
			args[1]="ps_addressdetails.addno";
			args[2]="'"+bgroup+"'";
			args[3]="'"+refno+"'";
			args[4]="null";
			
			title[0]="ps_address.bgroup";
			title[1]="ps_address.addno";
			title[2]="ps_address.bgroup";
			title[3]="ps_address.refno";
			title[4]="ps_address.endd";
			
		}
		
		/**For a meantime, CALCTYPE="PL"*/
		
		if(tableName.equals("CALC_OUTPUT_5")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+refno+"'";
			args[1]="'"+bgroup+"'";
			args[2]="'PL'";
			
			title[0]="REFNO";
			title[1]="BGROUP";
			title[2]="CALCTYPE";
			
		}
		
		/**MP_MANAGER
		 * select wd.wd01x, mpm.mpfm15x, wd.refno
		 * from MP_MANAGER mpm, welfare_detail wd
		 * where mpm.manager=wd.wd01x
		 * and wd.refno =???
		 * and wd.bgroup=???
		 * */
		
		if(tableName.equals("MP_MANAGER")){
			args=new String[2];
			title=new String[2];
			
			args[0]="(select wd01x from welfare_detail where refno='"+refno+"'" +
					" and bgroup='"+bgroup+"')";
			args[1]="'"+bgroup+"'";
						
			title[0]="MANAGER";
			title[1]="BGROUP";
						
		}
		
		/**bank detail*/
		if (tableName.equalsIgnoreCase("PS_PMDETAILS")){
			args=new String[4];
			title=new String[4];
			
			args[0]="PS_PM.PMDNO";
			args[1]="'"+bgroup+"'";
			args[2]="'"+refno+"'";
			args[3]="PS_PM.BGROUP";
						
			title[0]="PS_PMDETAILS.PMDNO";
			title[1]="PS_PM.BGROUP";
			title[2]="PS_PM.REFNO";
			title[3]="PS_PMDETAILS.BGROUP";
		}
		
		/**
		 * CW_CASE_LIST BGROUP, REFNO, 
		 * CASECODE = CaseWorkSQLHandler.BP_CHANGE_BANK_CW 
		 * most updated day(CL02D  )
		 * */
		
		
		if (tableName.equalsIgnoreCase("CW_CASE_LIST")){
			args=new String[4];
			title=new String[4];
			
			args[0]="'"+bgroup+"'";
			args[1]="'"+refno+"'";
			args[2]="'"+CaseWorkSQLHandler.BP_CHANGE_BANK_CW+"'";
			args[3]="(select max(CASENO) from cw_case_list where refno='"+refno+"'" +
					" and bgroup='"+bgroup+"' and casecode='"+CaseWorkSQLHandler.BP_CHANGE_BANK_CW+"')";
						
			title[0]="BGROUP";
			title[1]="REFNO";
			title[2]="CASECODE";
			title[3]="CASENO";
		}


	
		/**
		 * select revfac as LTA
		 * from revfac
		 * where factype='LTAA'
		 * and yoe='2007'
		 * */

		
		if (tableName.equalsIgnoreCase("revfac")){
			args=new String[2];
			title=new String[2];
			
			args[0]="'LTAA'";
			args[1]="2007";
								
			title[0]="factype";
			title[1]="yoe";
			
		}
		
		/**Total AVC (Avc, Tvin, Aver)
		 * select mp_fund.MPFN01X provider,         -- full name of the fund
		 * AVC_history.AH02D,                -- date updated
		 * AVC_history.AH10P                 -- value
		 * from AVC_history, mp_fund
		 * where
		 * mp_fund.FUNDCODE = AVC_history.AH09X
		 * AND AVC_history.BGROUP = 'BPF'
		 * AND
		 * AVC_history.REFNO = '0002430'
		 * AND AVC_history.AH01X = 'AVC'       -- AVC - standard additional voluntry funds
		 * ORDER BY  AVC_history.AH02D DESC   
		 * */
		
		if (tableName.equalsIgnoreCase("AVC_history")){
			args=new String[3];
			title=new String[3];
			
			args[0]="AVC_history.AH09X";
			args[1]="'"+bgroup+"'";
			args[2]="'"+refno+"' and AVC_HISTORY.AH01X in ('AVC','TVIN','AVER')";
								
			title[0]="mp_fund.FUNDCODE";
			title[1]="AVC_HISTORY.BGROUP";
			title[2]="AVC_HISTORY.REFNO";
			
		}
		
		/**FPS:
		 * select mh03c as FPS from miscellaneous_history
		 * where mh03c is not null and bgroup=? and refno=?
		 *       and seqno=(select max(seqno)
		 *       from miscellaneous_history 
		 *       where mh03c is not null and bgroup=? and refno=? )
		 */
		 
		if (tableName.equalsIgnoreCase("miscellaneous_history")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+bgroup+"'";
			args[1]="'"+refno+"'";
			args[2]="(select max(seqno) from miscellaneous_history " +
					"where mh03c is not null and bgroup='"+bgroup+"' and refno='"+refno+"')";
								
			title[0]="mh03c is not null and BGROUP";
			title[1]="REFNO";
			title[2]="seqno";
			
		}
		
		/** basicPS:
		 * select mh04c as basic  from miscellaneous_history
		 * where mh04c is not null and bgroup=? and refno=?
		 *       and seqno= (select max(seqno) from miscellaneous_history
		 *       where mh04c is not null and bgroup=? and refno=? )
		 **/
		
		if (tableName.equalsIgnoreCase("miscellaneous_history_1")){
			args=new String[3];
			title=new String[3];
			
			args[0]="'"+bgroup+"'";
			args[1]="'"+refno+"'";
			args[2]="(select max(seqno) from miscellaneous_history " +
					"where mh04c is not null and bgroup='"+bgroup+"' and refno='"+refno+"')";
								
			title[0]="mh04c is not null and BGROUP";
			title[1]="REFNO";
			title[2]="seqno";
			
		}
		
	}
	
	/**
	 * build whereClause for each parameter tableName
	 * 
	 * @param tableName
	 * @return String -- whereClause
	 * 
	 * */
	public String checkConstraint(String tableName){
		constraintDefiner(tableName);
		whereClause=new StringBuffer("");
		whereClause.append(" WHERE ");
		for(int i=0; i<args.length; i++){
			if (args[i]=="null")
				whereClause.append(title[i]+" is "+args[i]+"");
			else
				whereClause.append(title[i]+"="+args[i]+"");
			
			if (i<(args.length-1))
				whereClause.append(" AND ");
				
		}
		return whereClause.toString();
	}
}
