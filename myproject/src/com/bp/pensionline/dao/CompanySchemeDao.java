package com.bp.pensionline.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CompanySchemeDao {
	private static String WILD_CARD = "-*";
	 
	public CompanySchemeDao(){};
	
	private Map<String, String> valueMap = new HashMap<String, String>();
	
	public void set(String key, String value) {

		
		if (valueMap == null) {
			valueMap = new HashMap<String, String>();
		}
		valueMap.put(key, value);
	}
	public String get(String key){

		if (key != null && valueMap != null) {
			Object obj = valueMap.get(key);
			return obj == null ? new String("") : obj.toString();
		} else {
			return "";
		}

	}
	
    /**
     * Compare the hashmap of key (Business name)
     * and value BGROUP-Scheme number against what is passed in
     * If no matches then look for wild cards, e.g. BPF-*
     * 
     * @author Tu Nguyen
     * @param valueToCompare
     * @return
     */
	public String getSuitableScheme(String valueToCompare){
	
		Set<String> keySet = valueMap.keySet();
		
		Object []keyArray = keySet.toArray();
		
		//Find Exact Match, no wild cards
		for(int i=keyArray.length -1; i>=0; i-- ){
			
			String key = (String)keyArray[i]; //[0] BP
			
			String value = valueMap.get(key); //[BP] BPF-0001
			
			if (valueToCompare.equalsIgnoreCase(value)){
				//[BP] BPF-0001 = BPF-0275
				return key;  //no
			}
		}	

		
		//Find wild cards
		//change our String to a wild card:
		String wildLook = valueToCompare.substring(0, valueToCompare.indexOf('-')) + WILD_CARD;
				
		//above gives us BGROUP-*, e.g. BPF-* 
		
		for(int i=keyArray.length -1; i>=0; i-- ){
			
			String key = (String)keyArray[i]; //[0] BP
			
			String value = valueMap.get(key); //[BP] BPF-0001
			
			if (wildLook.equalsIgnoreCase(value)){
				//[BP] BPF-* = BPF-*
				return key;  //no
			}
		}		
		
		return "";
	}

}
