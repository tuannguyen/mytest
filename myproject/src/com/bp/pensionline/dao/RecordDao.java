package com.bp.pensionline.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.DateUtil;

public abstract class RecordDao implements Comparable{

	private long testNumber = 0;
	public void setTestNumber(long value){
		
		this.testNumber = value;
		
	}
	public long getTestNumber(){
		
		return testNumber;
		
	}
	private Map<String, String> valueMap = new HashMap<String, String>();

	/** The marshalled object itself */
	private String _Xml;

	/** return all the values in this */
	public Map<String, String> getValueMap() {
		return valueMap;
	}

	public void setValueMap(Map<String, String> valueMap) {
		this.valueMap = valueMap;
	}

	/** return all the values in this as an iterator */
	public Iterator getValueMapIterator() {
		return this.valueMap.keySet().iterator();
	}

	/**
	 * get a value for an Attribute
	 * 
	 * @param key
	 * @return String value for the attribute
	 */
	public String get(String key){

		if (key != null && valueMap != null) {
			Object obj = valueMap.get(key);
			return obj == null ? new String("") : obj.toString();
		} else {
			return "";
		}

	}

	/**
	 * store a value for a given key
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {

		/*
		 * UK CODE: if valueMap existes, create NEW valueMap ?????? if (valueMap !=
		 * null){ valueMap = new HashMap(); }
		 */
		if (valueMap == null) {
			valueMap = new HashMap<String, String>();
		}
		valueMap.put(key, value);
	}

	/* modified by cuongdv
	public String toXML() {
		return this._Xml;
	}
	*/

	public String getBgroup() {
		return get(Environment.MEMBER_BGROUP);
	}

	public String getRefno() {
		return get(Environment.MEMBER_REFNO);
	}

	public String getXML() {
		return this._Xml;
	}

	public void setXML(String xml) {
		this._Xml = xml;
	}

	/** security indicator: SI, it is used for security level in Aquila */
	public String getSI() {
		return get(Environment.MEMBER_SI);
	}

	public String getStatus() {
		return get(Environment.MEMBER_STATUS);
	}


	public String getPosStart() {
		return get(Environment.MEMBER_POSSTART);
	}

	public String getPosEnd() {
		return get(Environment.MEMBER_POSEND);
	}


	public String getScheme() {
		return get(Environment.MEMBER_SCHEME);

	}

	public String getStatusRaw() {
		return get(Environment.MEMBER_STATUSRAW);
	}

	/**
	 * over ride to show the attribute value contents
	 */
	public String toString() {
		return this.valueMap.toString() + getTestNumber();
	}
	
	public String toXML(){
		
		StringBuffer myXML = new StringBuffer();

			Iterator loop = getValueMapIterator();
			myXML.append("<"+Environment.POSTAG+">");
			myXML.append("\n");
			while(loop.hasNext()){
				Object key=loop.next();
				Object value=getValueMap().get(key);
				myXML.append("<" + key+">");
				myXML.append(value);
				myXML.append("</"+key+">");
				myXML.append("\n");
			}
			myXML.append("</"+Environment.POSTAG+">");
			
		return myXML.toString();	
	}
	public int compareTo(Object o) throws ClassCastException {
		RecordDao temp=(RecordDao)o;
		Date tempDate=DateUtil.getNormalDate(this.getPosStart());
		Date oDate=DateUtil.getNormalDate(temp.getPosStart());
		if (DateUtil.getDateDiff(tempDate, oDate)>0){
			return 1;
		}else {
			return 0;
		}
	}
}
