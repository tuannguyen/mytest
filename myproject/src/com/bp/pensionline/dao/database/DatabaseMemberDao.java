package com.bp.pensionline.dao.database;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.DBMemberConnector;
import com.bp.pensionline.database.DBMemberConnectorExt;

/**
 * This is a concrete implementation of the abstract class MemberDAO
 * This class is used to build the Member DAO from data held in the 
 * database.
 * 
 * It uses the Member Maper to find the SQL locations for the values
 * Then on the fly generates the SQL to populate this.
 * 
 * Only issues are Bgroup and Refno
 * 
 * @author Tu Nguyen
 *
 */
public class DatabaseMemberDao extends MemberDao {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	//private static final long serialVersionUID = 1L;
	
	//private ArrayList contributionList;
	//String refno = null;

	//String bgroup = null;

	//Other special tags, e.g. PLO
	protected Map<String, String> indicatorMap = new HashMap<String, String>();

	/**
	 * default constructor 
	 *
	 **/
	public DatabaseMemberDao(String description) {

	}

	
	/** default constructor */
	public DatabaseMemberDao() {
		super();
	}
	
	/**
	 * constructor 
	 * @param refno
	 * @param bgroup
	 */
	public DatabaseMemberDao(String bgroup, String refno) {

		LOG.info("DatabaseMemberDao: " + bgroup + "-" + refno);
		set(Environment.MEMBER_BGROUP, bgroup);

		set(Environment.MEMBER_REFNO, refno);

		//gets the member mapper information
		DBMemberConnector dmc = new DBMemberConnector(refno, bgroup);  //coffee pot

		//super.replaceValueMap(dmc.getValue());
		addValues(dmc.getValue());		


		indicatorMap = dmc.getMapIndicator();

		//map_othercases();

		
		
	}// end of DatabaseMemberDAO (String args1, String args2]



	/**
	 * Marshall this object into XML
	 * 
	 * @return
	 */

	public String toXML() {

		
		StringBuffer myXML = new StringBuffer();

		myXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

		myXML.append("<Member>\n");

		myXML.append("<Header>\n");

		Iterator loop = getValueMap().keySet().iterator();
		//Period of Service Tag
		myXML.append("<" + Environment.POSTAG + ">");
		myXML.append("\n");
		while (loop.hasNext()) {
			Object key = loop.next();
			Object value = getValueMap().get(key);

			if (Environment.POSTAG.equals(indicatorMap.get(key))
					|| Environment.TWOTAGS.equals(indicatorMap.get(key))) {
				myXML.append("<" + key + ">");
				myXML.append(value);
				myXML.append("</" + key + ">");
				myXML.append("\n");
			}

		}
		myXML.append("</" + Environment.POSTAG + ">");
		myXML.append("</Header>\n"); //End of Period of Service

		loop = getValueMap().keySet().iterator();

		//Period of Service Data
		myXML.append("<" + Environment.POSDTAG + ">");
		myXML.append("\n");
		while (loop.hasNext()) {
			Object key = loop.next();
			Object value = getValueMap().get(key);
			if (Environment.POSTAG.equals(indicatorMap.get(key)) == false
					&& Environment.TAGNAME[2].equals(indicatorMap.get(key)) == false) {
				myXML.append("<" + key + ">");				
				myXML.append(value);
				myXML.append("</" + key + ">");
				myXML.append("\n");
			}
		}

		myXML.append("</" + Environment.POSDTAG + ">");

		myXML.append("</Member>");
		return myXML.toString();

	}
	
	@Override
	public void buildDocument(byte[] arr) {
		// TODO Auto-generated method stub

	}


	/**
	 * This is a shallow copy, but should be deep do a deep copy not shallow
	 */
	public Object clone() throws CloneNotSupportedException {
		MemberDao memTemp;
		synchronized(this){
			
			//String ref = new String(this.refno);
			//String bgroup = new String(this.bgroup);
			
			memTemp = new DatabaseMemberDao();
							
			//copy each of the items in the map not the references to memory
			//http://www.java2s.com/Code/Java/Language-Basics/DeepCopyTest.htm
			
	        Iterator iterator = this.valueMap.keySet().iterator();
	        while (iterator.hasNext()) {        	
	        	Object key = iterator.next();
	        	memTemp.set(new String(""+key), new String(this.get(""+key)));
	        	//System.out.println(" DatabaseMemberDao clone ["+key+"], this="+this.get(""+key)+", memTemp="+memTemp.get(""+key));
	        }
	        System.out.println(" >>>>>> ["+Environment.MEMBER_REFNO+"], this="+this.get(""+Environment.MEMBER_REFNO)+", memTemp="+memTemp.get(""+Environment.MEMBER_REFNO));
			//deep copy the contents
			memTemp.getValueMap().putAll(this.getValueMap());
			

			
			
		}
		return memTemp;
	}	
	

	

		
}
