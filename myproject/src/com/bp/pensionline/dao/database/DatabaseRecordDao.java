package com.bp.pensionline.dao.database;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.database.DBRecordConnector;
import com.bp.pensionline.exception.MemberNotFoundException;


/**
 * Record Data access built from database
 * 
 * @author Tu Nguyen
 *
 */
public class DatabaseRecordDao extends RecordDao {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	//private static final long serialVersionUID = 1L;

   /**
    * default constructor
    */   
   public DatabaseRecordDao(String bgroup, String refno) throws MemberNotFoundException{
	   
	   	setTestNumber(System.currentTimeMillis());
	   
	   	set(Environment.MEMBER_REFNO, refno);
	   	set(Environment.MEMBER_BGROUP, bgroup);

	   	//LOG.info(this.getClass().toString() + ": DatabaseRecordDao: " + bgroup + "-" + refno);	
	   	
		DBRecordConnector drc = new DBRecordConnector(bgroup, refno);
		
		try{
						
			setValueMap(drc.getValue());

			//LOG.info(this.getClass().toString() + ": setMap OK");				
			
		}catch(MemberNotFoundException mnfe){
			
			throw mnfe;
		}
   }
	
   
	/**
	 * 
	 * @return Return the XML string 
	 */
	
	// modified by cuongdv
	/*
	public String toXML(){
	
		StringBuffer myXML = new StringBuffer();

			Iterator loop = super.getValueMapIterator();
			myXML.append("<"+Environment.POSTAG+">");
			myXML.append("\n");
			while(loop.hasNext()){
				Object key=loop.next();
				Object value=getValueMap().get(key);
				myXML.append("<" + key+">");
				myXML.append(value);
				myXML.append("</"+key+">");
				myXML.append("\n");
			}
			myXML.append("</"+Environment.POSTAG+">");
			
		return myXML.toString();
	
	}
	
	*/
	
}
