package com.bp.pensionline.dao.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.TextUtil;
import com.bp.pensionline.util.Variants;

//import javax.servlet.jsp.tagext.*;

public class AvcDetailDao {
		

	public AvcDetailDao(){
			
				
				// formatMap=mMapper.getMapFormat();
	};
 

	 static Connection getConnection(){
	        /*
	         * Create new conn
	         */
		 Connection conn=null;
	        try{
	          
	            	DBConnector dbConn = DBConnector.getInstance();
	            	conn = dbConn.getDBConnFactory(Environment.AQUILA);
	          
	        }catch(Exception ex){
	            ex.printStackTrace();
	        }
	        return conn;
	    }

	 
/**
 * @author Vinh Dao
 * @param refno
 * @param bgroup
 * @return arrayList
 * 
 * */
	public Map<String, String> getAvcDetail(String refno, String bgroup){
		ArrayList<String> AvcList = new ArrayList<String>();
		ArrayList<String> TvinList = new ArrayList<String>();
		ArrayList<String> AverList = new ArrayList<String>();
		
		String[] AvcGroup={"provider","date","value"};
		String[] AverGroup={"provider", "date", "value"};
		String[] TvinGroup={"provider", "date", "value"};
		String AvcName="AvcDetail";
		String AverName="AverDetail";
		String TvinName="TvinDetail";
		
		// Overide memberDao totalAVCs since it was wrong from memberMapper.xml: HuyTran 13-Jan-2012
		String totalAVCKey = "totalAvc";
		double totalAVCValue = 0.0;
		
		StringBuffer AvcXML=new StringBuffer();
		
		Map<String, String> avcDetailMap=new HashMap<String, String> ();
		Variants converter=new Variants();
		
		
		Connection conn=getConnection();
		  try{
		     if(conn != null)  {
		    	Statement stmt=conn.createStatement();
	    	//obj to store return value
				ResultSet rs=null;
					
				/**-- AVCs
				 * select
				 * mp_fund.MPFN01X provider,         -- full name of the fund
				 * AVC_history.AH02D,                -- date updated
				 * AVC_history.AH10P                 -- value
				 * from AVC_history, mp_fund
				 * where mp_fund.FUNDCODE = AVC_history.AH09X
				 * AND AVC_history.BGROUP = 'BPF'
				 * AND AVC_history.REFNO = '0002430'
				 * AND AVC_history.AH01X = 'AVC'       -- AVC - standard additional voluntry funds
				 * ORDER BY AVC_history.AH02D DESC  
				 * */
				
//				String sqlQuery="select mp_fund.MPFN01X provider," +
//									"AVC_history.AH02D date_updated," +
//									" AVC_history.AH10P avc_value," +
//									" AVC_history.AH01X type"+
//								" from AVC_history, mp_fund " +
//								" where mp_fund.FUNDCODE = AVC_history.AH09X" +
//									" AND AVC_history.BGROUP = '"+bgroup+"'" +
//									" AND AVC_history.REFNO = '"+refno+"'" +
//						 			" AND AVC_history.AH01X in ('AVC','AVER','TVIN')" +
//						 		" ORDER BY AVC_history.AH02D DESC";
				
				/**
				 * Fixing issue REPORTING-1432 to include
				 * 3 L & G AVC funds do not show on the PensionLine benefit statement screen
				 */
				/**
				 * New query
				 * 
				 * select
				 * fund.MPFN01X provider,--fullnameofthefund
				 * fund.FUNDCODE,
				 * avc_his.AH02D,--dateupdated
				 * avc_his.AH10P--value
				 * from 
				 * (select AH02D, AH10P, AH09X from AVC_history where
				 * AVC_history.BGROUP='BPF'
				 * AND AVC_history.REFNO='0068320' AND AVC_history.AH01X='AVC') avc_his,
				 * (select MPFN01X, FUNDCODE  from mp_fund 
				 * 		union select 'L & G European Equities (EX-UK)' MPFN01X, 'EEI' FUNDCODE from mp_fund
				 * 		union select 'L & G Ethical UK Equities' MPFN01X, 'EUE' FUNDCODE from mp_fund
				 * 		union select 'L & G Pre-Retirement Fund' MPFN01X, 'PRE' FUNDCODE from mp_fund) fund 
				 * where avc_his.AH09X = fund.FUNDCODE
				 */
				String sqlQuery= "select fund.MPFN01X provider, avc_his.AH02D date_updated, avc_his.AH10P avc_value, avc_his.AH01X type" +
						" from  " +
						"	(select AH02D, AH10P, AH09X, AH01X " +
						"	 from AVC_history " +
						"	 where AVC_history.BGROUP='" + bgroup +"' AND AVC_history.REFNO='" + refno + "' AND AVC_history.AH01X in ('AVC','AVER','TVIN') ) avc_his, " +
						"	(select case FUNDCODE when 'EUE' then 'L & G Ethical UK Equities' else MPFN01X end as MPFN01X, FUNDCODE  from mp_fund    " +
						"		union select 'L & G European Equities (EX-UK)' MPFN01X, 'EEI' FUNDCODE from mp_fund   " +
						///"		union select 'L & G Ethical UK Equities' MPFN01X, 'EUE' FUNDCODE from mp_fund   " +
						"		union select 'L & G Pre-Retirement Fund' MPFN01X, 'PRE' FUNDCODE from mp_fund) fund  " +
						" where avc_his.AH09X = fund.FUNDCODE";				
				//System.out.println(sqlQuery);
				rs=stmt.executeQuery(sqlQuery);
				double AvcTotal= 0;
				double AverTotal=0;
				double TvinTotal=0;
				
				int avcIterator=0;
				int averIterator=0;
				int tvinIterator=0;
				String provider=Environment.AVC_NO_FUND_NAME;
				
					while(rs.next()){
						if (rs.getString("type").equals("AVC")){
							
							/**Assign provider*/
							if( rs.getString("provider")!=null && rs.getString("provider").trim().length()>0)
								provider=rs.getString("provider");
								//escape special Characters
							    provider = TextUtil.htmlTagEscape(provider);

							
							AvcList.add(avcIterator, provider);
							avcIterator++;
							
							/**Assign date updated*/
							AvcList.add(avcIterator,converter.convert("Date",rs.getString("date_updated")));
							avcIterator++;
							
							AvcList.add(avcIterator,converter.convert("Money", rs.getString("avc_value")));
							AvcTotal+=Double.parseDouble(rs.getString("avc_value"));
							avcIterator++;
							
						}
						
						else if(rs.getString("type").equals("AVER")){
				
							/**Assign provider*/
							
							if( rs.getString("provider")!=null && rs.getString("provider").trim().length()>0)
								provider=rs.getString("provider");
								//escape special Characters
								provider = TextUtil.htmlTagEscape(provider);
							AverList.add(averIterator, provider);
							averIterator++;
							
							/**Assign date updated*/
							AverList.add(averIterator,converter.convert("Date",rs.getString("date_updated")));
							averIterator++;
							
							AverList.add(averIterator,converter.convert("Money", rs.getString("avc_value")));
							AverTotal+=Double.parseDouble(rs.getString("avc_value"));
							averIterator++;
						}
						
						else if (rs.getString("type").equals("TVIN")){
							/**Assign provider*/
							
							if( rs.getString("provider")!=null && rs.getString("provider").trim().length()>0)
								provider=rs.getString("provider");
								//escape special Characters
								provider = TextUtil.htmlTagEscape(provider);
							TvinList.add(tvinIterator, provider);
							tvinIterator++;
							
							/**Assign date updated*/
							TvinList.add(tvinIterator,converter.convert("Date",rs.getString("date_updated")));
							tvinIterator++;
							
							TvinList.add(tvinIterator,converter.convert("Money", rs.getString("avc_value")));
							TvinTotal+=Double.parseDouble(rs.getString("avc_value"));
							tvinIterator++;
						}
						
					}
					
					// HuyTran fix REPORTING-1432 issue
					totalAVCValue = (AvcTotal + AverTotal + TvinTotal);
					avcDetailMap.put(totalAVCKey, "" + totalAVCValue);
					/**
					 *Map AVC part to avcDetailMap
					 * 
					 * */
					
					//AvcXML.append("<AvcInformation>");
											
					AvcXML.append(getXMLString(AvcGroup,AvcList, AvcName));
					AvcXML.append("<Total>"+converter.convert("Money", Double.toString(AvcTotal))+"</Total>\n");					
					if(AvcList.size()>0)
					{
				//		AvcXML.append("<AvcInformation>true</AvcInformation>\n");
						avcDetailMap.put("AvcInformationAvailable", "true");
						
					}
						
					else
					{
						avcDetailMap.put("AvcInformationAvailable", "false");
					//	AvcXML.append("<AvcInformation>false</AvcInformation>\n");
					}
					
				//	AvcXML.append("</AvcInformation>\n");
					avcDetailMap.put("AvcInformation", AvcXML.toString());
					
					/**
					 *Map AVER part to avcDetailMap
					 * 
					 * */
					AvcXML=new StringBuffer();
				//	AvcXML.append("<AverInformation>");
					AvcXML.append(getXMLString(AverGroup,AverList, AverName));
					AvcXML.append("<Total>"+converter.convert("Money", Double.toString(AverTotal))+"</Total>\n");
					if(AverList.size()>0)
						avcDetailMap.put("AverInformationAvailable", "true");
						//AvcXML.append("<AverInformation>true</AverInformation>\n");
					else
						avcDetailMap.put("AverInformationAvailable", "false");
						//AvcXML.append("<AverInformation>false</AverInformation>\n");
				//	AvcXML.append("</AverInformation>\n");
					avcDetailMap.put("AverInformation", AvcXML.toString());
					
					/**
					 *Map TVIN part to avcDetailMap
					 * 
					 * */
					AvcXML=new StringBuffer();
			//		AvcXML.append("<TvinInformation>");
					AvcXML.append(getXMLString(TvinGroup,TvinList, TvinName));
					AvcXML.append("<Total>"+converter.convert("Money", Double.toString(TvinTotal))+"</Total>\n");
					
					if(TvinList.size()>0)
						avcDetailMap.put("TvinInformationAvailable", "true");
						//AvcXML.append("<TvinInformation>true</TvinInformation>\n");
					else
						avcDetailMap.put("TvinInformationAvailable", "false");
						//AvcXML.append("<TvinInformation>false</TvinInformation>\n");
					
				//	AvcXML.append("</TvinInformation>\n");
					avcDetailMap.put("TvinInformation", AvcXML.toString());
					
				rs.close();
				conn.close();
				}
		     }
		    	catch(SQLException sqlException){
		    	 sqlException.printStackTrace();
		    	 }
		   		    
		 return avcDetailMap;
		
		 
	 }
	/*ArrayList getList(String provider, String avcValue, String ){
		
		/**Assign provider*/
	/*	if( rs.getString("provider")!=null && rs.getString("provider").trim().length()>0)
			provider=rs.getString("provider");
		AvcList.add(avcIterator, provider);
		avcIterator++;
		
		/**Assign date updated*/
		/*AvcList.add(avcIterator,converter.convert("Date",rs.getString("date_updated")));
		avcIterator++;
		
		AvcList.add(avcIterator,converter.convert("Money", rs.getString("avc_value")));
		AvcTotal+=Double.parseDouble(rs.getString("avc_value"));
		avcIterator++;
		
	}*/
	
	StringBuffer getXMLString(String[] groupTag,ArrayList arrayList, String groupName ){
		int iterator=0;
		StringBuffer toXML=new StringBuffer();
		while(iterator<arrayList.size()/3){
			toXML.append("<"+groupName+">");
			toXML.append("\n");
			int i=0;
			while(i<groupTag.length){
				toXML.append("<"+groupTag[i]+">");
				if (arrayList.get(iterator*3+i)!=null)
					toXML.append(arrayList.get(iterator*3+i));
				else
					toXML.append(" ");
				
				toXML.append("</"+groupTag[i]+">");
				i++;
			}
			toXML.append("</"+groupName+">\n");
			iterator++;
			}
		
		return toXML;
	}
	

}






