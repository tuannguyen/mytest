package com.bp.pensionline.dao.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.Variants;

//import javax.servlet.jsp.tagext.*;

public class ContributionHistoryDao {


public ContributionHistoryDao(){
		
			
			// formatMap=mMapper.getMapFormat();
	};
 

	 static Connection getConnection(){
	        /*
	         * Create new conn
	         */
		 Connection conn=null;
	        try{
	          
	            	DBConnector dbConn = DBConnector.getInstance();
	            	conn = dbConn.getDBConnFactory(Environment.AQUILA);
	          
	        }catch(Exception ex){
	            ex.printStackTrace();
	        }
	        return conn;
	    }

	 
/**
 * @author Vinh Dao
 * @param refno
 * @param bgroup
 * @return arrayList
 * 
 * */
	public ArrayList<String> getContributionHistory(String refno, String bgroup){
		ArrayList<String> arrayList = new ArrayList<String>();
		Variants converter=new Variants();
		
		Connection conn=getConnection();
		  try{
		     if(conn != null)  {
		    	Statement stmt=conn.createStatement();
	    	//obj to store return value
				ResultSet rs=null;
				String sqlQuery="SELECT ca26x, OL03D, OL04D " +
						"FROM old_scheme_detail " +
						"where REFNO='"+refno+"' and BGROUP='"+bgroup+"'";

				rs=stmt.executeQuery(sqlQuery);
				int i=0;
					while(rs.next()){
						String temp;
						arrayList.add(i, rs.getString("CA26X"));
						i++;
						if (rs.getString("OL03D")!=null){
						temp=converter.convert("Date", rs.getString("OL03D"));
				//		System.out.println("Temp is: "+temp);
						arrayList.add(i,temp);
						}else
							arrayList.add(i,rs.getString("OL03D"));
						i++;
						
						if (rs.getString("OL04D")!=null){
						temp=converter.convert("Date", rs.getString("OL04D"));
						arrayList.add(i, temp);
						}else
							arrayList.add(i,rs.getString("OL04D"));
						
						i++;
						
					}
					
				rs.close();
				DBConnector connector = DBConnector.getInstance();
				connector.close(conn);//conn.close();

				}
		     }
		    	catch(SQLException sqlException){
		    	 sqlException.printStackTrace();
		    	 }
		   		    
		 return arrayList;
		
		 
	 }
	
}






