package com.bp.pensionline.dao.xml;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.RecordDao;
import com.bp.pensionline.exception.MemberNotFoundException;
import com.bp.pensionline.test.XmlReader;

public class XmlRecordDao extends RecordDao {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
	private String filePath = "/system/modules/com.bp.pensionline.test_members/";

	private String fileName = "member.xml";

	public XmlRecordDao(String _Bgroup, String _Refno) throws MemberNotFoundException {

		boolean isUser = false;
		
		set(Environment.MEMBER_BGROUP,_Bgroup);

		set(Environment.MEMBER_REFNO,_Refno);

		try {
			
			XmlReader reader = new XmlReader();
			
			ByteArrayInputStream btArr = new ByteArrayInputStream(reader.readFile(filePath + fileName));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			// get period of service nodes
			NodeList listOfMembers = doc.getElementsByTagName(Environment.POSTAG);

			// iterate through each period of service
			for (int s = 0; s < listOfMembers.getLength(); s++) {

				Node firstNode = listOfMembers.item(s);
				if (firstNode.getNodeType() == Node.ELEMENT_NODE) {

					Element firstElement = (Element) firstNode;

					String strBgroup = getNodeValue(firstElement, Environment.MEMBER_BGROUP);

					String strRefno = getNodeValue(firstElement, Environment.MEMBER_REFNO);

					// compare with given bgroup refno
					if (strBgroup.equals(getBgroup())&& strRefno.equals(getRefno())) {
						
						isUser = true;

						set(Environment.MEMBER_SCHEME,getNodeValue(firstElement, Environment.MEMBER_SCHEME));
						set(Environment.MEMBER_SCHEME_RAW,getNodeValue(firstElement, Environment.MEMBER_SCHEME_RAW));
						set(Environment.MEMBER_STATUS,getNodeValue(firstElement, Environment.MEMBER_STATUS));

						set(Environment.MEMBER_STATUSRAW,getNodeValue(firstElement,	Environment.MEMBER_STATUSRAW));

						set(Environment.MEMBER_POSSTART,getNodeValue(firstElement,	Environment.MEMBER_POSSTART));

						String strPosEnd = null;

						try {

							strPosEnd = getNodeValue(firstElement, Environment.MEMBER_POSEND);

						} catch (Exception e) {

							strPosEnd = null;

						}
						if (null != strPosEnd) {

							set(Environment.MEMBER_POSEND, strPosEnd);

						} else {

							set(Environment.MEMBER_POSEND, new String("present"));

						}

						set(Environment.MEMBER_SI,getNodeValue(firstElement, Environment.MEMBER_SI));

						break;
						

					} else
						continue;

				}// end of if clause

			}// end of for loop with s var
		
		}
		catch(FileNotFoundException fnfe){
			
			LOG.info(fnfe.getMessage() + fnfe.getCause());
			MemberNotFoundException mffe = new MemberNotFoundException("Can not found member with Bgroup=" + _Bgroup + " and Refno=" + _Refno);
			throw mffe;
			
		}
		catch (SAXParseException err) {

			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());

			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {

			Exception x = e.getException();
			
			((x == null) ? e : x).printStackTrace();

		}catch(Exception ex){
			LOG.info(ex.getMessage() + ex.getCause());
		}
		
		if(!isUser){
			
			MemberNotFoundException mffe = new MemberNotFoundException("Can not found member with Bgroup=" + _Bgroup + " and Refno=" + _Refno);
			throw mffe;				
			
		}
	}

	/* modified by cuongdv 
	 * 
	public void setToXML() {
		StringBuffer buf = new StringBuffer();
		buf.append("<PeriodOfService><Bgroup>").append(getBgroup()).append(
				"</Bgroup>");
		buf.append("<Scheme>").append(getScheme()).append("</Scheme>");
		buf.append("<Refno>").append(getRefno()).append("</Refno>");
		buf.append("<MembershipStatus>").append(getStatus()).append(
				"</MembershipStatus>");
		buf.append("<Start>").append(getPosStart()).append("</Start>");
		buf.append("<End>").append(getPosEnd()).append("</End>");
		buf.append("<SecurityIndicator>").append(getSI()).append(
				"</SecurityIndicator>").append("</PeriodOfService>");
		setXML(buf.toString());

	}
	*/

	public String getNodeValue(Element element, String tagName)
			throws Exception {
		NodeList nodeList = element.getElementsByTagName(tagName);
		Element tagNameElement = (Element) nodeList.item(0);
		NodeList textList = tagNameElement.getChildNodes();
		return ((Node) textList.item(0)).getNodeValue().trim();
	}

	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer tempStr = new StringBuffer();

		tempStr.append("getBgroup(): ");
		tempStr.append(getBgroup());
		tempStr.append("getRefno(): ");
		tempStr.append(getRefno());

		return tempStr.toString();
	}

}
