package com.bp.pensionline.dao.xml;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.database.DatabaseMemberDao;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.StringUtil;

public class XmlMemberDao extends MemberDao {
	
	public String toXML() {

		String bGroup = this.get(Environment.MEMBER_BGROUP);

		String refNo = this.get(Environment.MEMBER_REFNO);

		String fileName = "Member_" + bGroup + "_" + refNo + ".xml";

		XmlReader reader = new XmlReader();

		try {

			byte[] arr = reader.readFile(Environment.XMLFILE_DIR + fileName);

			String str = new String(arr);

			return str;
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return null;
		}
	}

	public XmlMemberDao(String bGroup, String refNo) {
		this.set(Environment.MEMBER_BGROUP, new String(bGroup));
		this.set(Environment.MEMBER_REFNO, new String(refNo));
		String fileName = "Member_" + bGroup + "_" + refNo + ".xml";
		XmlReader reader = new XmlReader();
		try {
			this.buildDocument(reader.readFile(Environment.XMLFILE_DIR + fileName));
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
	}

	public XmlMemberDao(String description) {

		this.set(Environment.MEMBER_DESCRIPTION, description);
		description = StringUtil.Escape(description);// Replace special
		// charaters or blanks
		// by underline
		String fileName = description + ".xml";
		XmlReader reader = new XmlReader();
		try {
			this.buildDocument(reader.readFile(Environment.XMLFILE_DIR + fileName));
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}

	}

	public void buildDocument(byte[] arr) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(arr);

			document = builder.parse(is);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		Element root = document.getDocumentElement();

		Node sibroot = root.getElementsByTagName("PeriodOfServiceData").item(0);

		NodeList nodeList = sibroot.getChildNodes();

		int nlength = nodeList.getLength();
		String value = "";
		for (int i = 0; i < nlength; i++) {
			value = "";
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.hasChildNodes() == true
						&& node.getChildNodes().getLength() > 1) {

					String name = node.getNodeName();
					NodeList cList = node.getChildNodes();
					int len = cList.getLength();

					for (int x = 0; x < len; x++) {
						Node cNode = cList.item(x);
						if (cNode.getNodeType() == Node.ELEMENT_NODE) {
							value = value + "<" + cNode.getNodeName() + ">\n";
							if (cNode.getChildNodes().getLength() > 1) {
								NodeList gcList = cNode.getChildNodes();
								int gLen = gcList.getLength();
								for (int y = 0; y < gLen; y++) {
									Node gcNode = gcList.item(y);
									if (gcNode.getNodeType() == Node.ELEMENT_NODE) {
										value = value + "<"
												+ gcNode.getNodeName() + ">"
												+ gcNode.getTextContent()
												+ "</" + gcNode.getNodeName()
												+ ">";
									} else {
									}
								}
							} else {
								value = value + cNode.getTextContent();
							}
							value = value + "</" + cNode.getNodeName() + ">\n";
							
						} else {
						}
					}
					
					this.set(name, value);
				} else {

					String name = node.getNodeName();
					value = node.getTextContent();
					
					this.set(name, value);
				}
			} else {

			}

		}
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		//super.clone();
		MemberDao memTemp = new XmlMemberDao("not set", "not set");
		
		HashMap tempHashMap = new HashMap();
		
		//deep copy the contents
		memTemp.getValueMap().putAll(this.getValueMap());
		
		return memTemp;
	}

	@Override
	public String get(String key) {
		// TODO Auto-generated method stub
		return super.get(key);
	}

	@Override
	public String getBgroup() {
		// TODO Auto-generated method stub
		return super.getBgroup();
	}

	@Override
	public String getMemberData(String key) {
		// TODO Auto-generated method stub
		return super.getMemberData(key);
	}

	@Override
	public String getRefno() {
		// TODO Auto-generated method stub
		return super.getRefno();
	}

	@Override
	public Map<String, String> getValueMap() {
		// TODO Auto-generated method stub
		return super.getValueMap();
	}

	@Override
	public void replaceValueMap(Map<String, String> valueMap) {
		// TODO Auto-generated method stub
		super.replaceValueMap(valueMap);
	}

	@Override
	public void set(String key, String value) {
		// TODO Auto-generated method stub
		super.set(key, value);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
