package com.bp.pensionline.dao.xml;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.XmlReader;

public class XmlPaySlipDao extends MemberDao {

	private Hashtable<String, String> m_datalist = new Hashtable<String, String>();	

	public String getMemberData(String key) {
		Object obj = m_datalist.get(key);
		return obj == null ? new String("") : obj.toString();
	}

	public void buildDocument(byte[] arr) {

	}

	public String toXML() {
		return null;
	}

	public XmlPaySlipDao(String bGroup, String refNo, String fiscalYear,
			String tagName) {

		String fileName = "Member_" + bGroup + "_" + refNo + ".xml";
		//Load and build according to bGroup and refNo
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try {

			builder = factory.newDocumentBuilder();
			XmlReader reader = new XmlReader();
			ByteArrayInputStream is = new ByteArrayInputStream(reader
					.readFile(Environment.XMLFILE_DIR + fileName));
			document = builder.parse(is);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		//Get valid year that need to load
		Element root = document.getDocumentElement();
		NodeList yearList = root.getElementsByTagName("CurrentTaxYear");

		int pos = 0;
		boolean validated = false;

		for (int i = 0; i < yearList.getLength(); i++) {
			Node sibroot = yearList.item(i);
			if (sibroot.getNodeType() == Node.ELEMENT_NODE) {
				NodeList nodeList = sibroot.getChildNodes();
				Node cNode = nodeList.item(0);
				if (cNode.getTextContent().equals(fiscalYear)) {
					//if requestYear found in the file, stop searching and set its position to 'pos'
					pos = i;
					validated = true;

					break;
				} else
					continue;
			} else
				continue;
		}
		if (validated) {
			//if found validated data, put them into hashTable
			NodeList monthList = root.getElementsByTagName(tagName);

			Node month = monthList.item(pos);
			if (month.getNodeType() == Node.ELEMENT_NODE) {

				String name = month.getNodeName();
				String value = month.getTextContent();

				m_datalist.put(name, value);
			} else {
			}
		} else {

		}

	}
	
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		//super.clone();
		MemberDao memTemp = new XmlPaySlipDao("not set", "not set", "not set", "not set");
		
		HashMap tempHashMap = new HashMap();
		
		//deep copy the contents
		memTemp.getValueMap().putAll(this.getValueMap());
		
		return memTemp;
	}
}
