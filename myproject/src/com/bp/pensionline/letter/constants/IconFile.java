/**
 * 
 */
package com.bp.pensionline.letter.constants;

/**
 * @author AS5920G
 *
 */
public class IconFile {
	private String url = "";
	private String name = "";
	private String title = "";
	private Boolean local = false;
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param local the local to set
	 */
	public void setLocal(Boolean local) {
		this.local = local;
	}
	/**
	 * @return the local
	 */
	public Boolean isLocal() {
		return local;
	}
}
