/**
 * 
 */
package com.bp.pensionline.letter.constants;

import java.util.Comparator;

/**
 * @author AS5920G
 *
 */
public class HeaderLine {
	private String name = "";
	private String value = "";
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the name
	 */
	public String getDisplayName() {
		return name.substring(0,1).toUpperCase() + name.substring(1);
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	public class HeaderLineComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			HeaderLine obj1 = (HeaderLine) o1;
			HeaderLine obj2 = (HeaderLine) o2;
			String str1 = (obj1 == null ? null : obj1.getName());
	        String str2 = (obj2 == null ? null : obj2.getName());
	        return (str1 == null ? (str2 == null ? 0 : -1) : str1.compareToIgnoreCase(str2));
	    }
	}
}
