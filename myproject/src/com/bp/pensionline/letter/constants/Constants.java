/*
 * Constant.java
 *
 * Created on March 15, 2007, 2:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.letter.constants;

import com.bp.pensionline.util.CheckConfigurationKey;

/**
 *
 * @author mall
 */
public class Constants {
    public static final String PARAM = "xml";
    public static final String CONTENT_TYPE = "text/xml";
    
    /** Creates a new instance of Constant */
    public static final String AJAX_LETTER_REQUESTED = "AjaxLetterParameterRequest";
    public static final String AJAX_LETTER_RESPONSE = "AjaxLetterParameterResponse";    
    
    /* Password letter */
    public static final String LETTER_PASSWORD = "Password";
    public static final String LETTER_PASSWORD_ERROR = "Error";
    public static final String LETTER_PASSWORD_NUM_BP_UNREG = "num_bp_unreg";   
    public static final String LETTER_PASSWORD_NUM_BC_UNREG = "num_bc_unreg";   
    public static final String LETTER_PASSWORD_NUM_SUBS_UNREG = "num_subs_unreg";   
    public static final String LETTER_PASSWORD_NUM_PENSION_RESET = "num_pension_reset"; 
    public static final String LETTER_PASSWORD_ADD_SIGNATURE = "add_signature";   
    public static final String LETTER_PASSWORD_COMMIT_CHANGES = "commit_changes";     
    
    public static final String LETTER_PASSWORD_TOTAL_BP_UNREG = "total_bp_unreg";   
    public static final String LETTER_PASSWORD_TOTAL_BC_UNREG = "total_bc_unreg";   
    public static final String LETTER_PASSWORD_TOTAL_SUBS_UNREG = "total_subs_unreg";   
    public static final String LETTER_PASSWORD_TOTAL_PENSION_RESET = "total_pension_reset";
    
    // Password flagging
    public static final String LETTER_PASSWORD_FLAGGED_WELCOME_PACK_LIST = "welcome_pack_list";   
    public static final String LETTER_PASSWORD_FLAGGED_PASSWORD_RESET_LIST = "password_reset_list"; 
    public static final String LETTER_ADDRESS_QUERY_FILE  = "/system/modules/com.bp.pensionline.letter/letter_member_address.sql";
    public static final String LETTER_PASSWORD_FLAGGED_UPDATE_LIST_VALDIATE = "update_list_validate";
    
    public static interface SIGNATURE {
    	public static final String ACTION				  = "ACTION";
    	public static final String ACTION_SAVE			  = "Save";
    	public static final String ACTION_UPLOAD_IMAGE	  = "Upload";
    	public static final String ACTION_UPDATE		  = "Update";
    	public static final String ACTION_DELETE		  = "Delete";
    	public static final String URI					  = "Uri";
    	public static final String TAG_SIGNATURE		  = "Signature";
    	public static final String TAG_LOCATION			  = "ImageLocation";
    	public static final String TAG_ID			  	  = "Id";
    	public static final String TAG_ONLINE			  = "Online";
    	public static final String TAG_PATH				  = "Path";
    	public static final String TAG_NAME				  = "Name";
    	public static final String TAG_TELEPHONE		  = "Telephone";
    	public static final String TAG_EMAIL		  	  = "Email";
    	public static final String TAG_URL				  = "Url";
    	public static final String TAG_TITLE			  = "Title";
    	public static final String TAG_DEFAULT			  = "Default";
    	public static final String TAG_RESULT			  = "Result";
    	public static final String TAG_ERROR			  = "Error";
    	public static final String XML_LOCATION 		  = CheckConfigurationKey.getStringValue("xmlLocation");
    	public static final String IMAGE_GALLERY_LOCATION = CheckConfigurationKey.getStringValue("imagesLocation");
    	public static final String IMAGE_EXTENSION		  = CheckConfigurationKey.getPublishProperty("imageExtension");
    	public static final String XML_PATH		 		  = XML_LOCATION+"signature_old.xml";
    	public static final String XML_VFS_PATH	 		  = CheckConfigurationKey.getStringValue("xmlVsfFile");
    	public static final String DEFAULT_USERNAME		  = CheckConfigurationKey.getStringValue("prgUsername");
    	public static final String DEFAULT_ADDRESS		  = CheckConfigurationKey.getStringValue("prgAddress");
    	public static final String SIGNATURE_PARAGRAPH	  = CheckConfigurationKey.getStringValue("signatureParag");
    	public static final String SUMMARY_PAGE			  = CheckConfigurationKey.getStringValue("summaryPage");
    }

    public static interface HEADER {
    	public static final String XML_VFS_PATH			  = SIGNATURE.XML_LOCATION+"header_setting.xml";
    	public static String LETTER_TABLE_TEMPLATE		  = "LETTER_TABLE_TEMPLATE";
    }
}
