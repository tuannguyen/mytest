package com.bp.pensionline.letter.producer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import net.sf.jasperreports.engine.export.oasis.ContentBuilder;

import org.apache.commons.logging.Log;
import org.apache.xml.serializer.Serializer;
import org.apache.xml.serializer.SerializerFactory;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException; 

public class LetterTransformer
{
	public static final Log log = CmsLog.getLog(LetterTransformer.class);	
	/**
	 * Get content of word document as bytes
	 * @param htmlInputSteam
	 * @param html2FoXSLStream
	 * @param html2WordXSLStream
	 * @return
	 */
	public static String getWordContent (InputStream htmlInputSteam, 
										 InputStream html2FoXSLStream, 
										 InputStream html2WordXSLStream) throws UnsupportedEncodingException {
		DOMResult wordDom = convertHtml2WordML(htmlInputSteam, html2FoXSLStream, html2WordXSLStream);
		String content = "";
		
		if (wordDom != null)
		{
			Node root = wordDom.getNode();
			NodeList docNodes = root.getChildNodes();
			Node bodyNode = null;
			for (int i = 0; i < docNodes.getLength(); i++)
			{
				Node docNode = docNodes.item(i);
				if (docNode != null & docNode.getNodeType() == Node.ELEMENT_NODE && docNode.getNodeName().equals("w:wordDocument"))
				{
					NodeList wordNodes = docNode.getChildNodes();
					for (int j = 0; j < wordNodes.getLength(); j++)
					{
						Node wordNode = wordNodes.item(j);
						if (wordNode != null & wordNode.getNodeType() == Node.ELEMENT_NODE && wordNode.getNodeName().equals("w:body"))
						{
							bodyNode = wordNode;
							break;
						}
					}
					break;
				}
			}
			
		    // Instantiate an Xalan XML serializer and use it to serialize the output DOM to System.out
		    // using the default output format, except for indent="yes"
				
			if (bodyNode != null)
			{
				NodeList bodyNodes = bodyNode.getChildNodes();
				StringBuffer contentBuf = new StringBuffer();
				for (int i = 0; i < bodyNodes.getLength(); i++)
				{
					Node pageNode = bodyNodes.item(i);
					if (pageNode.getNodeType() == Node.ELEMENT_NODE && pageNode.getNodeName().equals("w:sectPr")) continue;
					contentBuf.append(getNodeAsRawText(pageNode));
				}
				content = contentBuf.toString();
			}
		}
		content = new String(content.getBytes("UTF-8"),"UTF-8");
		log.info("Word: "+content);
		return content;
	}
	
	public static String getNodeAsRawText(Node node)
	{
		StringBuffer rawText = new StringBuffer();		
		// Get attributes
		if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			rawText.append("<").append(node.getNodeName());
			NamedNodeMap attributes = node.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++)
			{
				rawText.append(" ").append(attributes.item(i).getNodeName()).append("=");
				rawText.append("\"").append(attributes.item(i).getNodeValue()).append("\"");
			}
			rawText.append(">");
			NodeList nodeList = node.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++)
			{
				Node child = nodeList.item(i);
				rawText.append(getNodeAsRawText(child));
			}
			rawText.append("</").append(node.getNodeName()).append(">");
		}
		else if (node.getNodeType() == Node.TEXT_NODE)
		{
			String nodeContent = node.getTextContent();
			// for some reason <w:p> is consisdered a text node
			if (nodeContent != null && nodeContent.length() > 0)
			{
				if (nodeContent.indexOf("<w:") < 0 && nodeContent.indexOf("</w:") < 0 )
				{
					//log.info("content is " + nodeContent);
					nodeContent = nodeContent.replaceAll("&", "&amp;");
					nodeContent = nodeContent.replaceAll("<", "&lt;");
					nodeContent = nodeContent.replaceAll(">", "&gt;");	
				}
			}
			rawText.append(nodeContent);
		}
		else if (node.getNodeType() == Node.CDATA_SECTION_NODE)
		{
			rawText.append("<![CDATA[").append(node.getTextContent()).append("]]>");
		}	
		
		return rawText.toString();
	}	
	
	/**
	 * serialize a node to a sequence of bytes stored in a ByteArrayOutputStream
	 * @param aNode
	 * @param byteOutputStream
	 */
	public static void serializeChildrendOfNode (Node aNode, Serializer serializer, OutputStream outputStream)
	{
		try
		{
			if (aNode != null)
			{
				serializer.setOutputStream(outputStream);
				serializer.asDOMSerializer().serialize(aNode);				
			}
		}
		catch (Exception e)
		{
			log.error("Error in serialize node: " + e.toString());
		}

	}
	
	/**
	 * Process htmlInputStream and convert it to WordML
	 * HTML -> FO-ML -> WordML
	 * @param htmlInputSteam
	 * @param html2FoXSL
	 * @param html2WordXSL
	 * @return
	 */
	public static DOMResult convertHtml2WordML (InputStream htmlInputSteam, InputStream html2FoXSLStream, InputStream html2WordXSLStream)
	{
		DOMResult resultDom = null;
		try
		{
			log.info("convert HTML 2 Fo Begin: ");
			byte[] foByte = convertToByte(htmlInputSteam, html2FoXSLStream);
			//log.info("HTML document: ");
			//log.info("FO document: "+new String(foByte,"UTF-8"));
//			String test = new String(foByte);
//			log.error("TEST: " + test);
			if (foByte != null && foByte.length > 0)
			{
				ByteArrayInputStream foStream = new ByteArrayInputStream(foByte);
				log.info("convert Fo 2 Word Begin: ");
				resultDom = convertToDom (foStream, html2WordXSLStream);
				log.info("convert Fo 2 Word completes!");
				// Check wordml result
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				
			    java.util.Properties xmlProps = OutputPropertiesFactory.getDefaultMethodProperties("xml");
				xmlProps.setProperty("indent", "no");
				xmlProps.setProperty("standalone", "no");
				Serializer serializer = SerializerFactory.getSerializer(xmlProps);
				serializer.setOutputStream(outStream);
				serializer.asDOMSerializer().serialize(resultDom.getNode());
				outStream.flush();
				//log.info("WORD document: "+outStream.toString("UTF-8")); 
			}
		}
		catch (Exception e)
		{
			log.error("Error in converting: " + e.toString());
		}
		
		return resultDom;
		
	}
	/**
	 * Convert html file to xsl-fo file represented as an byte array
	 * 
	 * @param htmlFile
	 * @return byte array represents the xsl-fo file
	 */
	public static byte[] convertToByte(InputStream xmlFileStream, InputStream xslFileStream) 
		throws 	TransformerException, TransformerConfigurationException, FileNotFoundException,
	    		ParserConfigurationException, SAXException, IOException
	{
		DOMResult domResult = convertToDom(xmlFileStream, xslFileStream);
		
	    //Instantiate an Xalan XML serializer and use it to serialize the output DOM to System.out
	    // using the default output format, except for indent="yes"
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
	    java.util.Properties xmlProps = OutputPropertiesFactory.getDefaultMethodProperties("xml");
		xmlProps.setProperty("indent", "no");
		xmlProps.setProperty("standalone", "no");
		Serializer serializer = SerializerFactory.getSerializer(xmlProps);
		serializer.setOutputStream(outStream);
		serializer.asDOMSerializer().serialize(domResult.getNode());
		
		outStream.flush();
		return outStream.toByteArray();
	}
	
	/**
	 * Convert html file to xsl-fo file represented as an byte array
	 * 
	 * @param htmlFile
	 * @return byte array represents the xsl-fo file
	 */
	public static DOMResult convertToDom(InputStream xmlFileStream, InputStream xslFileStream) 
		throws 	TransformerException, TransformerConfigurationException, FileNotFoundException,
	    		ParserConfigurationException, SAXException, IOException
	{
		TransformerFactory tFactory = TransformerFactory.newInstance();

		log.info("xmlFileStream: " + xmlFileStream);
		log.info("xslFileStream: " + xslFileStream);
		if (tFactory.getFeature(DOMSource.FEATURE)
				&& tFactory.getFeature(DOMResult.FEATURE))
		{
			//Instantiate a DocumentBuilderFactory.
			DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();

			// And setNamespaceAware, which is required when parsing xsl files
			dFactory.setNamespaceAware(true);
			//log.info("check poin 1");
			//Use the DocumentBuilderFactory to create a DocumentBuilder.
			DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
			//log.info("check poin 2");
			//Use the DocumentBuilder to parse the XSL stylesheet.
			Document xslDoc = dBuilder.parse(xslFileStream);
			//log.info("check poin 3");
			// Use the DOM Document to define a DOMSource object.
			DOMSource xslDomSource = new DOMSource(xslDoc);
			//log.info("check poin 4");
			// Set the systemId: note this is actually a URL, not a local filename
			xslDomSource.setSystemId(xslFileStream.toString());
			//log.info("check poin 5");
			// Process the stylesheet DOMSource and generate a Transformer.
			Transformer transformer = tFactory.newTransformer(xslDomSource);
			//log.info("check poin 6");
			//Use the DocumentBuilder to parse the XML input.
			Document xmlDoc = dBuilder.parse(xmlFileStream);
			//log.info("check poin 7");
			// Use the DOM Document to define a DOMSource object.
			DOMSource xmlDomSource = new DOMSource(xmlDoc);
			//log.info("check poin 8");
			// Set the base URI for the DOMSource so any relative URIs it contains can
			// be resolved.
			xmlDomSource.setSystemId(xmlFileStream.toString());

			// Create an empty DOMResult for the Result.
			DOMResult domResult = new DOMResult();
			//log.info("check poin 9");
			// Perform the transformation, placing the output in the DOMResult.
			transformer.transform(xmlDomSource, domResult);
			//log.info("check poin 10");
			return domResult;
		}
		else
		{
			throw new org.xml.sax.SAXNotSupportedException(
					"DOM node processing not supported!");
		}
	}	
}
