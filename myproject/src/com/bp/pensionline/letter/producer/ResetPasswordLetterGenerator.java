package com.bp.pensionline.letter.producer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.letter.dao.LetterMemberDao;
import com.bp.pensionline.letter.util.PasswordGenerator;

public class ResetPasswordLetterGenerator
{
	public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String MEMBER_VALUE_PATTERN = "([\\[]{2}[^\\[\\]]*[\\]]{2}){1}";
	public static final String breakPattern = "(&lt;br){1}[ \\t]*(/&gt;){1}|(<br){1}[ \\t]*(/>){1}";
	
	private Pattern memValuePattern = Pattern.compile(MEMBER_VALUE_PATTERN);
	
	
	private LetterMemberDao memberDao = null;
	
	private String newPassword = null;
	
	public ResetPasswordLetterGenerator(LetterMemberDao memberDao)
	{
		this.memberDao = memberDao;
		newPassword = PasswordGenerator.generateInitialMemberPassword();
	}
	
	public String processLetterHeader(String header)
	{
		if (header == null)
		{
			return null;
		}
		String updateHeader = header;
		
		Matcher memValueMatcher = memValuePattern.matcher(header);
		
		while (memValueMatcher.find())
		{
			String metaMemValueStr = memValueMatcher.group();
			String memValueKey = metaMemValueStr.substring(2, metaMemValueStr.length() - 2);
			
			String memValue = memberDao.get(memValueKey);
			if (memValue == null)
			{
				memValue = "";
			}
			
			updateHeader = updateLetterParagraph(updateHeader, memValueKey, memValue);
			
			
			log.info("Header: Replace member value: " + metaMemValueStr + " with " + memValue);			
		}
		return updateHeader;
	}	
	
//	public String processLetterFooter(String footer, boolean hasSignature, String signatureContent)
//	{
//		if (footer == null)
//		{
//			return null;
//		}	
//		
//		String updateFooter = footer;		
//		Matcher memValueMatcher = memValuePattern.matcher(footer);
//		
//		while (memValueMatcher.find())
//		{
//			String metaMemValueStr = memValueMatcher.group();
//			String memValueKey = metaMemValueStr.substring(2, metaMemValueStr.length() - 2);
//			
//			String memValue = "";
//			if (memValueKey.toLowerCase().indexOf("signature") >= 0)
//			{
//				if (hasSignature)
//				{
//					updateFooter = addLetterSignature(footer, memValueKey, signatureContent);
//				}
//				else
//				{
//					updateFooter = addEmptySignature(footer, memValueKey, 3);
//				}
//			}			
//			else
//			{
//				memValue = memberDao.get(memValueKey);
//				if (memValue == null)
//				{
//					memValue = "";
//				}				
//				updateFooter = updateLetterParagraph(updateFooter, memValueKey, memValue);
//			}
//						
//			log.info("Footer: Replace member value: " + metaMemValueStr + " with " + memValue);			
//		}
//		
//		return updateFooter;
//	}	

	
	public String processLetterBody(String body)
	{
		if (body == null)
		{
			return null;
		}	
		
		String updateBody = body;		
		Matcher memValueMatcher = memValuePattern.matcher(body);
		
		while (memValueMatcher.find())
		{
			String metaMemValueStr = memValueMatcher.group();
			String memValueKey = metaMemValueStr.substring(2, metaMemValueStr.length() - 2);
			
			String memValue = "";
			if (memValueKey.toLowerCase().indexOf("password") >= 0)
			{				
				updateBody = replaceString(updateBody, "[[" + memValueKey + "]]", newPassword);
			}
			else
			{
				memValue = memberDao.get(memValueKey);
				if (memValue == null)
				{
					memValue = "";
				}				
				updateBody = updateLetterParagraph(updateBody, memValueKey, memValue);
			}

			log.info("Body: Replace member value: " + metaMemValueStr + " with " + memValue);			
		}
		
//		String[] emptySectProTags = {"<w:sectPr></w:sectPr>", "<w:sectPr/>"};
//		if (updateBody != null)
//		{
//			updateBody = removeRedundantTag(updateBody, emptySectProTags);	
//		}
			
		return updateBody;
	}
	
	public String removeRedundantTag(String letterPart, String[] tagNames)
	{
		String result = letterPart;
		for (int i = 0; i < tagNames.length; i++)
		{
			result = replaceString(result, tagNames[i], "");
		}
		
		return result;
	}
	
	/**
	 * Replace paragraph contains member value key by a new whole paragraph contains signature image.
	 * @param letterPart
	 * @param memValueKey
	 * @param newParagraph
	 * @return
	 */
	public String addLetterSignature (String letterPart, String memValueKey, String newParagraph)
	{
		if (letterPart == null)
		{
			return null;
		}
		
		if (newParagraph == null)
		{
			return letterPart;
		}
		
		String para = getCurrentParagraph(letterPart, memValueKey);
		String result = replaceParagraph(letterPart, para, newParagraph);
		
		return result;
	}
	
	/**
	 * Replace paragraph contains member value key by a number of empty paragraphs .
	 * @param letterPart
	 * @param memValueKey
	 * @param newParagraph
	 * @return
	 */
	public String addEmptySignature (String letterPart, String memValueKey, int numOfBreaks)
	{
		if (letterPart == null)
		{
			return null;
		}
		
		numOfBreaks = (numOfBreaks <= 0) ? 1 : numOfBreaks;
				
		String para = getCurrentParagraph(letterPart, memValueKey);
		StringBuffer updateValue = new StringBuffer();
		for (int i = 0; i < numOfBreaks; i++)
		{
			String tempPara = replaceString(para, "[[" + memValueKey + "]]", "");
			updateValue.append(tempPara);			
		}
		String result = replaceParagraph(letterPart, para, updateValue.toString());
		
		return result;
	}	
	
	/**
	 * Update the member value to the key with consideration of paragraph in WordML
	 * Replace all WordML non-understandable in member value such as <br/>
	 * @param memValueKey
	 * @param memValue
	 * @return
	 */
	public String updateLetterParagraph (String letterPart, String memValueKey, String memberValue)
	{
		if (letterPart == null)
		{
			return null;
		}
		String result = letterPart;
		String[] valueParts = memberValue.split(breakPattern);
		
		if (valueParts.length > 1)
		{
			// find the paragraph that contain the memberValueKey
//			String paraPatternStr = "(<w:p>){1}[^\\[\\]]*(\\[\\[" + memValueKey + "\\]\\])+[^\\[\\]]*(</w:p>){1}";
//			Pattern paraPattern = Pattern.compile(paraPatternStr);
			// find the current para
			String para = getCurrentParagraph(letterPart, memValueKey);
			if (para != null)
			{
				// update with new multiple paragraphs
				StringBuffer updateValue = new StringBuffer();
				for (int i = 0; i < valueParts.length; i++)
				{
					if (!valueParts[i].trim().equals(""))
					{
						String tmpValuePart = valueParts[i];
						if (tmpValuePart.indexOf("&") >= 0)
						{
							tmpValuePart = tmpValuePart.replaceAll("&", "&amp;");
						}
						
						String tempPara = replaceString(para, "[[" + memValueKey + "]]", tmpValuePart);
						updateValue.append(tempPara);
					}
				}
				
				result = replaceParagraph(letterPart, para, updateValue.toString());
			}
		}
		else
		{
			String tmpMemberValue = memberValue.replaceAll("&", "&amp;");
			result = replaceString(result, "[[" + memValueKey + "]]", tmpMemberValue);
		}
		
		return result;
	}
	
	private String getCurrentParagraph (String letterPart, String memValueKey)
	{
		int keyIndex = letterPart.indexOf("[[" + memValueKey + "]]");
		if (keyIndex > 0)
		{
			String fisrtPart = letterPart.substring(0, keyIndex);
			String secondPart = letterPart.substring(keyIndex);
			
			String paraFirstPart = fisrtPart.substring(fisrtPart.lastIndexOf("<w:p>"));
			String paraSecondPart = secondPart.substring(0, secondPart.indexOf("</w:p>")) + "</w:p>";
			
			return paraFirstPart + paraSecondPart;
		}
		
		return null;
	}
	
	public String replaceParagraph (String letterPart, String oldPara, String newPara)
	{
		int index = letterPart.indexOf(oldPara);
		StringBuffer result = new StringBuffer();
		// append first part
		result.append(letterPart.substring(0, index));
		// append new para
		result.append(newPara);
		// append last part
		result.append(letterPart.substring(index + oldPara.length()));
		
		return result.toString();
	}
	/**
	 * Update listId for numbering list. Find the string <w:ilfo w:val="2">
	 * @return
	 */
	public String updateListForMember(String letterPart, int listId)
	{
		String oldListTagStr = "<w:ilfo w:val=\"2\">";
		String updateListTagStr = "<w:ilfo w:val=\"" + listId + "\">";
		String result = replaceString(letterPart, oldListTagStr, updateListTagStr);
		
		return result;
	}
	
	/**
	 * Update listId for numbering list. Find the string <w:ilfo w:val="2">
	 * @return
	 */
	public static String replaceString(String letterPart, String oldString, String newString)
	{
		int index = letterPart.indexOf(oldString);
		StringBuffer result = new StringBuffer();
		String tempStr = letterPart;
		while (index >= 0)
		{			
			// append first part
			result.append(tempStr.substring(0, index));
			// append new para
			result.append(newString);
			// append last part
			tempStr = tempStr.substring(index + oldString.length());
			index = tempStr.indexOf(oldString);			
		}		
		
		result.append(tempStr);

		return result.toString();
		
	}	
	
	public String getNewPassword()
	{
		return newPassword;
	}

}
