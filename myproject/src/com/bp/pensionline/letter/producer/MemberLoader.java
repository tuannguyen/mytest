package com.bp.pensionline.letter.producer;
/*
 * Util class used for member loader for all types of letters
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.letter.dao.LetterMemberDao;
import com.bp.pensionline.letter.handler.PasswordLetterHandler;

public class MemberLoader
{
	public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	/**
	 * Get total number of BPF members waiting for welcome letter
	 * @return
	 */
	public static int getTotalBPUnregMember()
	{
		// Get all members requiring form password reset
		String sqlQuery = "select count(*) as num from Basic where BD19A != 'NL' and BGROUP = 'BPF' and BD08X in (select upper(EXTUSERID) " +
				"from BP_registrations where RESET = 'W')";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("rs sqlQuery: " + sqlQuery);
		int totalBPUnreg = 0;
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				totalBPUnreg = rs.getInt("num");
				log.error("Total BP unregistered member: " + totalBPUnreg);
			}			
			
			rs.close();
			pstm.close();			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		
		return totalBPUnreg;
	}	
	
	
	/**
	 * Get total number of BPF members waiting for welcome letter
	 * @return
	 */
	public static int getTotalBCUnregMember()
	{
		// Get all members requiring form password reset
		String sqlQuery = "select count(*) as num from Basic where BD19A != 'NL' and BGROUP = 'BCF' and BD08X in (select upper(EXTUSERID) " +
				"from BP_registrations where RESET = 'W')";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("rs sqlQuery: " + sqlQuery);
		int totalBCUnreg = 0;
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				totalBCUnreg = rs.getInt("num");
				log.error("Total BC unregistered member: " + totalBCUnreg);
			}			
			
			rs.close();
			pstm.close();			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		
		return totalBCUnreg;
	}		
	
	
	/**
	 * Get total number of BPF members waiting for welcome letter
	 * @return
	 */
	public static int getTotalSubsUnregMember()
	{
		// Get all members requiring form password reset
		String sqlQuery = "select count(*) as num from Basic where BD19A != 'NL' and BGROUP = 'SPF' and BD08X in (select upper(EXTUSERID) " +
				"from BP_registrations where RESET = 'W')";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("rs sqlQuery: " + sqlQuery);
		int totalSubsUnreg = 0;
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				totalSubsUnreg = rs.getInt("num");
				log.error("Total Subs unregistered member: " + totalSubsUnreg);
			}			
			
			rs.close();
			pstm.close();			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		
		return totalSubsUnreg;
	}			
	
	/**
	 * Get total number of members waiting for password reset 
	 * @return
	 */
	public static int getTotalPasswordResetMember()
	{
		// Get all members requiring form password reset
		String sqlQuery = "select count(*) as num from Basic where BD19A != 'NL' and BD08X in (select upper(EXTUSERID) " +
				"from BP_registrations where RESET = 'P')";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("rs sqlQuery: " + sqlQuery);
		int totalPRMember = 0;
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				totalPRMember = rs.getInt("num");
				log.error("Total password reset member: " + totalPRMember);
			}			
			
			rs.close();
			pstm.close();			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		
		return totalPRMember;
	}
	/**
	 * Get numOfMember of BGroup and refNo composed string of members eg, BPF-0007655 requiring for password 
	 * reset. With member has more than 1 POS, the BGroup and refNo of lastest POS will be get.
	 * @return
	 */
	public static ArrayList<String> getTotalPasswordResetMemberIds ()
	{
		// hash map contain nino as keys and BGroup-Refno as values
		HashSet<String> ninoSet = new HashSet<String>();
		ArrayList<String> result = new ArrayList<String>();
		
		// Get all members requiring form password reset
		String sqlQuery = "select BGROUP, REFNO, BD08X as NI, CREFNO, " +
				"decode(BD19A, 'AC', '01', 'PN', '02', 'PP', '03', 'WB', '04', 'CB', '05') " +
				"as status from Basic where BD19A != 'NL' and BD08X in (select upper(EXTUSERID) " +
				"from BP_registrations where RESET = 'P') ORDER BY status ASC, BGROUP ASC";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		
		//log.info("rs sqlQuery: " + sqlQuery);
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			
			// Temperary store the Ids for later sort out
			ArrayList<String> tempBPFIds = new ArrayList<String>();
			ArrayList<String> tempBCFIds = new ArrayList<String>();
			ArrayList<String> tempSubIds = new ArrayList<String>();
			
			while (rs.next())
			{
				String nino = rs.getString("NI");
				String bGroup = rs.getString("BGROUP");
				String refNo = rs.getString("REFNO");
				if (!ninoSet.contains(nino))
				{
					ninoSet.add(nino);
					if (bGroup != null && bGroup.equals("BPF"))
					{
						tempBPFIds.add(bGroup + "-" + refNo);
					}
					else if (bGroup != null && bGroup.equals("BCF"))
					{
						tempBCFIds.add(bGroup + "-" + refNo);
					}
					else if (bGroup != null)
					{
						tempSubIds.add(bGroup + "-" + refNo);
					}
					LetterMemberDao.ninoMap.put(refNo, nino);
					log.info("Got reset password member : " + nino +" : " + bGroup + "-" + refNo);
				}
			}			
			
			rs.close();
			pstm.close();	
			
			// Build the result in the order: BPF -> BCF -> Sub
			for (int i = 0; i < tempBPFIds.size(); i++)
			{
				result.add(tempBPFIds.get(i));
			}
			for (int i = 0; i < tempBCFIds.size(); i++)
			{
				result.add(tempBCFIds.get(i));
			}
			for (int i = 0; i < tempSubIds.size(); i++)
			{
				result.add(tempSubIds.get(i));
			}			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		return result;
	}
	
	/**
	 * Get Nino number of member
	 * @param bgroup
	 * @param refNo
	 * @return
	 */
	public static String getNinoFromMemberId (String memberId)
	{
		String nino = null;
		if (memberId != null && !memberId.trim().equals(""))
		{			
			int hyphenIndex = memberId.indexOf('-');
			if (hyphenIndex < 0)
			{
				return null;
			}
			
			String bGroup = memberId.substring(0, hyphenIndex);
			String refNo = memberId.substring(hyphenIndex + 1);	
			
			// Get nino
			String sqlQuery = "select BD08X as NI from Basic " +
					"where BGROUP = '" + bGroup + "' and REFNO = '" + refNo + "'";
			
			DBConnector connector = DBConnector.getInstance();
			Connection con = null;	
			log.info("getNinoFromMemberId sqlQuery: " + sqlQuery);
			try
			{
				con = connector.getDBConnFactory(Environment.AQUILA);
				PreparedStatement pstm = con.prepareStatement(sqlQuery);		
				ResultSet rs = pstm.executeQuery();
				
				if (rs.next())
				{
					nino = rs.getString("NI");			
				}			
				
				rs.close();
				pstm.close();
				
			}
			catch (SQLException e)
			{
				log.error("Error in get members' requiring password reset: " + e.toString());
			}
			finally
			{
				if (con != null)
				{
					try
					{
						DBConnector.getInstance().close(con);
					}
					catch (Exception e)
					{
						log.error("Error in closing connection: " + e.toString());
					}
				}
			}
		}
		
		log.info("MemberID: " + memberId + " has nino: " + nino);
		return nino;		
	}
	
	/**
	 * Get all BGroup and refNo of members eg, BPF-0007655 requiring for welcome pack. With member has more
	 * than 1 POS, the BGroup and refNo of lastest POS will be get.
	 * @return
	 */
	public static ArrayList<String> getTotalWelcomePackMemberIdsByGroup (String bgroup)
	{
		// hash map contain nino as keys and BGroup-Refno as values
		HashSet<String> ninoSet = new HashSet<String>();
		ArrayList<String> result = new ArrayList<String>();
		
		// Get all members requiring form password reset
		String sqlQuery = "select BGROUP, REFNO, BD08X as NI, CREFNO, " +
				"decode(BD19A, 'AC', '01', 'PN', '02', 'PP', '03', 'WB', '04', 'CB', '05') " +
				"as status from Basic where BD19A != 'NL' and BGROUP = '" + bgroup + "' and BD08X in " +
				"(select upper(EXTUSERID) from BP_registrations where RESET = 'W') ORDER BY status ASC";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("rs sqlQuery: " + sqlQuery);
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			while (rs.next())
			{
				String nino = rs.getString("NI");
				String bGroup = rs.getString("BGROUP");
				String refNo = rs.getString("REFNO");
				if (!ninoSet.contains(nino))
				{
					ninoSet.add(nino);
					result.add(bGroup + "-" + refNo);
					LetterMemberDao.ninoMap.put(refNo, nino);
					log.info("Got welcome pack member: " + nino +" : " + bGroup + "-" + refNo);
				}				
			}			
			
			rs.close();
			pstm.close();			
			
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		return result;
	}	
	
	/**
	 * load member by member Id with format" bgroup-refno
	 * @param memberId
	 * @return
	 */
	public static LetterMemberDao loadMemberById(String memberId)
	{
		if (memberId != null && !memberId.trim().equals(""))
		{
			log.info("MemberID: " + memberId);
			int hyphenIndex = memberId.indexOf('-');
			if (hyphenIndex < 0)
			{
				return null;
			}
			
			String bGroup = memberId.substring(0, hyphenIndex);
			String refNo = memberId.substring(hyphenIndex + 1);
			
			// general exception catching due to the method calls are too deep
			LetterMemberDao member = null;
			try
			{
				member = new LetterMemberDao(refNo, bGroup);
			}
			catch (Exception e)
			{
				log.error("General exception in getting member. This is due to the data is not well updated. " 
						+ e.toString());
				/**
				 * create empty Member with Error description
				 */
				member = new LetterMemberDao();
				member.setDataError(true);
				member.setErrorDescription(e.toString()+". Please make sure all data is fulfilled for member "+memberId);
			}
			
			return member;
		}
		
		return null;
	}
	
	public static void updatePasswordForMember(String ninoPasswordStr)
	{
		int hyphenIndex = ninoPasswordStr.indexOf('-');
		if (hyphenIndex < 0)
		{
			return;
		}
		
		String nino = ninoPasswordStr.substring(0, hyphenIndex);
		String newPassword = ninoPasswordStr.substring(hyphenIndex + 1);	
		
		String sqlDeleteOldPassword = "DELETE from we_authenticate " + "WHERE EXTUSERID = lower('" + nino + "')";		
		String sqlInsertNewPassword = "INSERT INTO we_authenticate (EXTUSERID, PASSWORD, WEAU01X, WEAU02I) VALUES " +
				"(lower('" + nino + "'), adm_auth_gen.encrypt('" + newPassword + "'), 'N', 0)";
		
//		String sqlUpdatePassword = "UPDATE we_authenticate SET password = adm_auth_gen.encrypt('"
//				+ newPassword + "') " + "WHERE EXTUSERID = lower('" + nino + "')";
		String sqlUpdateReset = "UPDATE BP_REGISTRATIONS SET RESET = null WHERE extuserid = lower('" + nino + "')";

//		log.info("Delete old password: " + sqlDeleteOldPassword);
//		log.info("Insert new password: " + sqlInsertNewPassword);
		if (nino != null && !nino.trim().equals(""))
		{			
			DBConnector connector = DBConnector.getInstance();
			Connection con = null;
			PreparedStatement pstmDeleteOldPassword = null;
			PreparedStatement pstmInsertNewPassword = null;
			PreparedStatement pstmUpdateReset = null;
			try
			{
				con = connector.getDBConnFactory(Environment.AQUILA);
				con.setAutoCommit(false);
				pstmDeleteOldPassword = con.prepareStatement(sqlDeleteOldPassword);
				pstmUpdateReset = con.prepareStatement(sqlUpdateReset);
				pstmDeleteOldPassword.executeUpdate();
				pstmUpdateReset.executeUpdate();
				con.commit();
				
				log.info("Delete old password from we_authenticate Ok: " + nino);
				
				pstmInsertNewPassword = con.prepareStatement(sqlInsertNewPassword);
				pstmInsertNewPassword.executeUpdate();
				con.commit();
				
				log.info("Member " + nino + " has new pasword: " + newPassword);
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{
				}
				log.debug("updateMemberAfterPasswordReset error : " + e.toString());
			}
			finally
			{
				if (con != null)
				{
					try
					{
						connector.close(con);// con.close();
					}
					catch (Exception e)
					{
					}
				}
			}

		}
	}	
	
	/**
	 * Insert a member that is flaged for password action
	 * @param ninoPasswordStr
	 * @param type
	 */
	public static void insertPasswordFlaggedMember(String nino, String type)
	{
		String newPassword = "P3nsions";
		log.info("insert new password flagged member: " + nino + " - " + type);	
		
//		String sqlDeleteOldPasswordAuth = "DELETE from we_authenticate " + "WHERE EXTUSERID = lower('" + nino + "')";		
//		String sqlInsertNewPasswordAuth = "INSERT INTO we_authenticate (EXTUSERID, PASSWORD, WEAU01X, WEAU02I) VALUES " +
//				"(lower('" + nino + "'), adm_auth_gen.encrypt('" + newPassword + "'), 'N', 0)";
//				
		String sqlDeleteOldPasswordReg = "DELETE from BP_REGISTRATIONS " + "WHERE EXTUSERID = lower('" + nino + "')";		
		String sqlInsertNewPasswordReg = "INSERT INTO BP_REGISTRATIONS (EXTUSERID, PASSWORD, RESET) VALUES " +
				"(lower('" + nino + "'), '" + newPassword + "', '" + type + "')";		

//		log.info("Delete old password: " + sqlDeleteOldPassword);
//		log.info("Insert new password: " + sqlInsertNewPassword);
		if (nino != null && !nino.trim().equals(""))
		{
			DBConnector connector = DBConnector.getInstance();
			Connection con = null;
			PreparedStatement pstmDeleteOldPasswordReg = null;
			PreparedStatement pstmInsertNewPasswordReg = null;			
//			PreparedStatement pstmDeleteOldPasswordAuth = null;
//			PreparedStatement pstmInsertNewPasswordAuth = null;

			try
			{
				con = connector.getDBConnFactory(Environment.AQUILA);
			}
			catch (Exception e)
			{				
				log.error("updateMemberAfterPasswordReset error in creating new DB connection: " + e.toString());
				return;
			}
			
			if (con == null)
			{
				return;
			}
			// delete old record
			try
			{
				con.setAutoCommit(false);
				pstmDeleteOldPasswordReg = con.prepareStatement(sqlDeleteOldPasswordReg);
				pstmDeleteOldPasswordReg.executeUpdate();
//				pstmDeleteOldPasswordAuth = con.prepareStatement(sqlDeleteOldPasswordAuth);				
//				pstmDeleteOldPasswordAuth.executeUpdate();
				con.commit();												
			}
			catch (Exception e)
			{
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{
					log.error("updateMemberAfterPasswordReset error: failed in rollback : " + ex.toString());
				}
				log.error("updateMemberAfterPasswordReset error in delete old record: " + e.toString());
			}			
			
			// update new record
			try
			{
				if (con == null)
				{
					return;
				}
				pstmInsertNewPasswordReg = con.prepareStatement(sqlInsertNewPasswordReg);
				pstmInsertNewPasswordReg.executeUpdate();
//				pstmInsertNewPasswordAuth = con.prepareStatement(sqlInsertNewPasswordAuth);								
//				pstmInsertNewPasswordAuth.executeUpdate();
				con.commit();
				
				con.setAutoCommit(true);
			}
			catch (Exception e)
			{
				try
				{
					con.rollback();
				}
				catch (Exception ex)
				{
					log.error("updateMemberAfterPasswordReset error: failed in rollback : " + ex.toString());
				}
				log.error("updateMemberAfterPasswordReset error in insert new record: " + e.toString());
			}

			if (con != null)
			{
				try
				{
					connector.close(con);// con.close();
				}
				catch (Exception e)
				{
					log.error("updateMemberAfterPasswordReset error in close connection: " + e.toString());
				}
			}
		}
	}	
	
	/**
	 * Insert a member that is flaged for password action
	 * @param ninoPasswordStr
	 * @param type
	 */
	public static void updateListPasswordFlaggedMember(ArrayList<String> ninoSet, String type)
	{		
		if (ninoSet == null && ninoSet.size() == 0)
		{
			return;
		}
		
		log.info("Reset password flag to null for members: " + ninoSet.size());
		// build ninoSet set string
		StringBuffer ninoSetBuf = new StringBuffer();
		for (int i = 0; i < ninoSet.size(); i++)
		{
			ninoSetBuf.append('\'').append(ninoSet.get(i).toLowerCase()).append('\'');
			if (i < ninoSet.size() -1)
			{
				ninoSetBuf.append(",");
			}
		}
		
		String reset = null;
		if (type == null)
		{
			reset = "null";
		}
		else if (type.equals("W"))
		{
			reset = "'W'";
		}
		else if (type.equals("P"))
		{
			reset = "'P'";
		}
		
		String sqlUpdatePasswordReg = "UPDATE BP_REGISTRATIONS SET RESET = " + reset + " WHERE EXTUSERID in (" + ninoSetBuf.toString() + ")";		
		//log.info("sqlUpdatePasswordReg: " + sqlUpdatePasswordReg);
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;
		PreparedStatement pstmUpdatePasswordReg = null;

		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstmUpdatePasswordReg = con.prepareStatement(sqlUpdatePasswordReg);
			pstmUpdatePasswordReg.executeUpdate();

			con.commit();
			
			con.setAutoCommit(true);
		}
		catch (Exception e)
		{
			try
			{
				con.rollback();
			}
			catch (Exception ex)
			{
			}
			log.debug("updateMemberAfterPasswordReset error : " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);// con.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}	
	
	/**
	 * Get nino of all members with a specific type: 'W', 'P', null
	 * @return
	 */
	public static ArrayList<String> getNinoByPasswordFlaggingType (String type)
	{
		ArrayList<String> result = new ArrayList<String>();		
		
		// Get all members requiring form password reset
		String sqlQuery = "select EXTUSERID as NI from BP_registrations where RESET = '" + type + "' order by NI asc"; 
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		
		//log.info("rs sqlQuery: " + sqlQuery);
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			while (rs.next())
			{
				String nino = rs.getString("NI");
				result.add(nino);
			}			
			
			rs.close();
			pstm.close();	
	
		}
		catch (SQLException e)
		{
			log.error("Error in get members' requiring password reset: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		return result;
	}	
	
	/**
	 * Get nino of all members with a specific type: 'W', 'P', null
	 * @return
	 */
	public static boolean isMemberExistByNino (String nino)
	{
		boolean result = false;
		
		// Get all members requiring form password reset
		String sqlQuery = "select BD08X as NI from basic where BD19A != 'NL' and BD08X = upper('" + nino + "') "; 
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		
		//log.info("rs sqlQuery: " + sqlQuery);
		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);		
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				result = true;
			}			
			
			rs.close();
			pstm.close();	
	
		}
		catch (SQLException e)
		{
			log.error("Error in isMemberExistByNino: " + e.toString());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					log.error("Error in closing connection: " + e.toString());
				}
			}
		}
		
		return result;
	}		
}
