package com.bp.pensionline.letter.dao;

import java.io.ByteArrayInputStream;
import java.util.Comparator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.constants.HeaderLine;
import com.bp.pensionline.letter.util.FileUtil;
import com.bp.pensionline.test.XmlReader;

public class SignatureXmlDao
{
	public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private boolean isUseOnlineImage = false;
	private String imageFileName = null;
	private String imageData = null;	// Base64 encoded image data
	private String name = null;
	private String title = null;
	private String telephone = null;
	private String email = null;
	private String url = null;
	private String id = null;
	private boolean isDefault = false;
	
	/*
	 * Constructor for new implementation
	 */
	public SignatureXmlDao(boolean isUseOnlineImage, String imageFileName, 
						   String imageData, String name, String title, 
						   String telephone, String email, String url, String id,
						   boolean isDefault) {
		this.isUseOnlineImage = isUseOnlineImage;
		this.imageFileName = imageFileName;
		this.imageData = imageData;
		this.name = name;
		this.title = title;
		this.telephone = telephone;
		this.email = email;
		this.url = url;
		this.id = id;
		this.isDefault = isDefault;
	}
	
	public SignatureXmlDao() {}
	
	public SignatureXmlDao(String signatureXmlFileName)
	{
		// Get the template file names from letter_mapping.xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		XmlReader xmlReader = new XmlReader();
		try
		{
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null)
			{
				return;
			}
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);

			document = builder.parse(is);
		}
		catch (Exception ex)
		{
			log.error("Error in parsing XML signature: " + ex.toString());
			return;
		}
		
		Element root = document.getDocumentElement();
		
		NodeList paramNodes = root.getChildNodes();	
		
		int nlength = paramNodes.getLength();
		for (int i = 0; i < nlength; i++)
		{
			Node paramNode = paramNodes.item(i);
			if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
			{
				if (paramNode.getNodeName().equals(Constants.SIGNATURE.TAG_LOCATION))
				{
					NodeList locationNodes = paramNode.getChildNodes();
					for (int j = 0; j < locationNodes.getLength(); j++)
					{
						Node locationNode = locationNodes.item(j);
						if (locationNode.getNodeType() == Node.ELEMENT_NODE && 
								locationNode.getNodeName().equals(Constants.SIGNATURE.TAG_ONLINE))							
						{							
							isUseOnlineImage = Boolean.parseBoolean(locationNode.getTextContent());
						}
						else if (locationNode.getNodeType() == Node.ELEMENT_NODE && 
								locationNode.getNodeName().equals(Constants.SIGNATURE.TAG_PATH))
						{
							setImageFileName(locationNode.getTextContent());
							
							if (isUseOnlineImage)
							{
								// get signature image from CMS
								byte[] signatureImgBytes = FileUtil.getFileContentAsBytes(getImageFileName());
								if (signatureImgBytes != null)
								{
									imageData = new sun.misc.BASE64Encoder().encode(signatureImgBytes);						
								}									
							}						
						}
					}
				}		
				if (paramNode.getNodeName().equals(Constants.SIGNATURE.TAG_NAME))
				{
					name = paramNode.getTextContent();
					name = name.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
						replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");					
				}					
				if (paramNode.getNodeName().equals(Constants.SIGNATURE.TAG_TITLE))
				{
					title = paramNode.getTextContent();
					title = title.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
					replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
				}
			}
			log.info("Signature: isUseOnlineImage: " + isUseOnlineImage);	
			log.info("Signature: imageFileName: " + getImageFileName());
			log.info("Signature: name: " + name);
			log.info("Signature: title: " + title);			
		}
	}
	/**
	 * @return the isUseLocalImage
	 */
	public boolean isUseOnlineImage()
	{
		return isUseOnlineImage;
	}

	/**
	 * @return the imageFileName
	 */
	public String getImageFileName()
	{
		return imageFileName;
	}

	/**
	 * @return the imageData
	 */
	public String getImageData()
	{
		// Testing purpose
//		byte[] signatureImgBytes = FileUtil.getFileContentAsBytes("/system/modules/com.bp.pensionline.letter/signature.gif");
//		if (signatureImgBytes != null)
//		{
//			imageData = new sun.misc.BASE64Encoder().encode(signatureImgBytes);						
//		}			
		return imageData;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param imageFileName the imageFileName to set
	 */
	public void setImageFileName(String imageFileName) {
		log.info("Image file name: "+imageFileName);
		this.imageFileName = imageFileName;
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("id="+id+",online="+isUseOnlineImage+",name="+name);
		buf.append(",image="+imageFileName+",telephone="+telephone+",email="+email+",url="+url);
		
		return buf.toString();
	}
	
	/**
	 * @param isDefault the isDefault to set
	 */
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 * @return the isDefault
	 */
	public boolean isDefault() {
		return isDefault;
	}

	public class SignatureComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			SignatureXmlDao obj1 = (SignatureXmlDao) o1;
			SignatureXmlDao obj2 = (SignatureXmlDao) o2;
			String str1 = (obj1 == null ? null : obj1.getId());
	        String str2 = (obj2 == null ? null : obj2.getId());
	        return (str1 == null ? (str2 == null ? 0 : -1) : str1.compareToIgnoreCase(str2));
	    }
	}
}
