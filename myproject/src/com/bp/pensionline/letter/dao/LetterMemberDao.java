package com.bp.pensionline.letter.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.letter.util.FileUtil;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.letter.constants.Constants;

public class LetterMemberDao extends MemberDao
{
	static final long serialVersionUID = 1L;
	
	public static final Log LOG = CmsLog.getLog(LetterMemberDao.class);
	private String errorDescription = "";
	private boolean isDataError = false;
	public static String[] mandatoryFields = null;
	
	//This map is used to retrieve NINO by REFNO in case the member does not have data
	public static Map<String, String> ninoMap = new HashMap<String, String>();
	
	public LetterMemberDao()
	{
		super();
	}

	public LetterMemberDao(String refno, String bgroup)
	{
		valueMap = new HashMap<String, String>(); //Collections.synchronizedMap(new HashMap<String, String>());

		valueMap.put(Environment.MEMBER_REFNO, refno);
		valueMap.put(Environment.MEMBER_BGROUP, bgroup);
		
		/**
		 * get mandatory fields at the first time
		 */
		if (mandatoryFields == null) {
			try {
				InputStream is = FileUtil.getInputStream("/system/modules/com.bp.pensionline.letter/header.properties");
				if (is != null) {
					Properties pros = new Properties();
					pros.load(is);
					mandatoryFields = pros.getProperty("mandatoryFields").split(",");
				}
				LOG.info("Mandatory fields: "+mandatoryFields);
			} catch (IOException ioe) {
				LOG.error("Exception while retrieving madatory fields configued "+ioe.toString());
			} catch (Exception e) {
				LOG.error("Exception while retrieving madatory fields configued "+e.toString());
			}
		}
		// update map values
		getMemberLetterData(refno, bgroup);
	}
	
	private void getMemberLetterData(String refno, String bgroup)
	{
		// Get all members requiring form password reset
		String addressQuery = getLetterMemberQuery(Constants.LETTER_ADDRESS_QUERY_FILE);
		String memberQuery = getLetterMemberQuery(Environment.LETTER_MEMBER_QUERY_FILE);
		if (memberQuery == null) return;
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		//log.info("sqlQuery: " + sqlQuery);
		try
		{
			//Put NINO to value map whether get data success or not
			String nino = ninoMap.get(refno);
			if (nino==null) nino="NA";
			valueMap.put("Nino", nino);
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			//get address information first
			addressQuery = addressQuery.replaceAll(":refno", "'" + refno + "'");
			addressQuery = addressQuery.replaceAll(":bgroup", "'" + bgroup + "'");
			LOG.info("Address Query: "+addressQuery);
			ResultSet ars = con.createStatement().executeQuery(addressQuery);
			ResultSetMetaData am = ars.getMetaData();
			int columns = am.getColumnCount();
			if (ars.next()) {
				for (int c=1; c<columns; c++) {
					String colName = am.getColumnName(c);
					Object colObject = ars.getObject(c);
					if (colObject!=null) {
						valueMap.put(colName, colObject.toString());
					} else if (isMandatory(colName)) {
						isDataError = true;
						errorDescription = "General exception in getting member data, " +
										   colName+" is invalid for member "+bgroup+"-"+refno+"(NULL)";
						break;
					}
				}
			} else {
				isDataError = true;
				errorDescription = "Address is invalid for member "+bgroup+"-"+refno;
			}
			
			if (!isDataError) {
				//if address valid, get other informations
				memberQuery = memberQuery.replaceAll(":refno", "'" + refno + "'");
				memberQuery = memberQuery.replaceAll(":bgroup", "'" + bgroup + "'");
				LOG.info("Letter member query: " + memberQuery);
				ResultSet rs = con.createStatement().executeQuery(memberQuery);
				
				int nullCounter = 0;
				ResultSetMetaData rsmd = rs.getMetaData();
				columns = rsmd.getColumnCount();
				if (rs.next()) {
					for (int i = 1; i <= columns; i++) {
						String colName = rsmd.getColumnName(i);
						Object colObject = rs.getObject(i);
						if (colObject != null) {
							LOG.info("put in map: " + colName + " - " + colObject.toString());
							valueMap.put(colName, colObject.toString());
						} else if (isMandatory(colName)) {
							isDataError = true;
							errorDescription = "General exception in getting member data, " +
											   colName+" is invalid for member "+bgroup+"-"+refno+"(NULL)";
							break;
						} else {
							nullCounter++;
							int type = rsmd.getColumnType(i);
							String value = null;
							if (Types.BOOLEAN == type) {
								value = "false";
							} else if (Types.INTEGER == type || Types.BIGINT == type ||
									   Types.DECIMAL == type || Types.DOUBLE == type ||
									   Types.FLOAT == type || Types.NUMERIC == type ||
									   Types.REAL == type || Types.SMALLINT == type || Types.TINYINT == type) {
								value = "0";
							} else {
								value = "";
							}
							valueMap.put(colName, value);
							LOG.info("put in map: " + colName + " - "+value);
						}
					}			
				} else {
					isDataError = true;
					errorDescription = "Cannot find member "+bgroup+"-"+refno;
				}
				if (nullCounter == columns) {
					isDataError = true;
					errorDescription = "General exception in getting member data, could not find any data for "+bgroup+"-"+refno;
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			LOG.error("Error in get members' requiring password reset: " + e.toString());
			isDataError = true;
			errorDescription = "General exception in getting member data ("+e.toString()+"), value is invalid for member "+bgroup+"-"+refno;
		} catch (NullPointerException ne) {
			ne.printStackTrace();
			isDataError = true;
			errorDescription = "General exception in getting member data ("+ne.toString()+"), value is invalid for member "+bgroup+"-"+refno;
		} finally {
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing connection: " + e.toString());
				}
			}
		}		
	}
	
	private String getLetterMemberQuery(String fileName)
	{
		try
		{
			XmlReader xmlReader = new XmlReader();
			
			byte[] letterQueryByteArr = xmlReader.readFile(fileName);
			if (letterQueryByteArr != null)
			{
				return new String(letterQueryByteArr);
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in getting letter member query: " + e);
		}
		
		return null;
	}

	@Override
	public void buildDocument(byte[] arr)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toXML()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param isDataError the isDataError to set
	 */
	public void setDataError(boolean isDataError) {
		this.isDataError = isDataError;
	}

	/**
	 * @return the isDataError
	 */
	public boolean isDataError() {
		return isDataError;
	}

	/**
	 * The utility method to check a field is mandatory or not
	 */
	private boolean isMandatory(String fieldName) {
		if (mandatoryFields != null) {
			for (int i=0; i<mandatoryFields.length; i++) {
				if (fieldName.equalsIgnoreCase(mandatoryFields[i])) {
					return true;
				}
			}
		}
		return false;
	}
}
