/*
 * @author: Huy Tran
 * FileUtil.java
 * 
 * Provide utility methods for processing text files from VFS of OpenCMS
 */
package com.bp.pensionline.letter.util;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.test.XmlReader;

public class FileUtil
{
	public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * Get the combined InputSream of 2 files. 
	 * @param firstFile
	 * @param secondFile
	 * @return
	 * @throws IOException
	 */
	public static InputStream combine (InputStream firstFile, InputStream secondFile) throws IOException
	{
		if (firstFile != null && secondFile != null)
		{
			return new SequenceInputStream(firstFile, secondFile);
		}
		else if (firstFile != null && secondFile == null)
		{
			return firstFile;
		}
		else if (firstFile == null && secondFile != null)
		{
			return secondFile;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Get the an InputStream to the file
	 * @param filename
	 * @return
	 */
	public static InputStream getInputStream (String filename)
	{
		try
		{
			XmlReader fileReader = new XmlReader();
			byte[] contentBytes = fileReader.readFile(filename);
			if (contentBytes != null)
			{
				return new ByteArrayInputStream(contentBytes);
			}
		}
		catch (FileNotFoundException fnfe)
		{
			log.error("Error in creating input stream from file: " + filename + " - " + fnfe.toString());
		}
		
		return null;
	}
	
	public static String getFileContentAsString(String filename)
	{
		String content = null;
		try
		{
			XmlReader fileReader = new XmlReader();
			byte[] contentBytes = fileReader.readFile(filename);
			if (contentBytes != null)
			{
				content = new String(contentBytes);
			}
		}
		catch (FileNotFoundException fnfe)
		{
			log.error("Error in getFileContent: " + fnfe.toString());
		}
		
		return content;
	}	
	
	public static String getFileContentAsString(String filename, String encoding)
	{
		String content = null;
		try
		{
			XmlReader fileReader = new XmlReader();
			byte[] contentBytes = fileReader.readFile(filename);
			if (contentBytes != null)
			{				
				try
				{
					content = new String(contentBytes, encoding);
				}
				catch (UnsupportedEncodingException uee)
				{
					content = new String(contentBytes);
				}				
			}
		}
		catch (FileNotFoundException fnfe)
		{
			log.error("Error in getFileContent: " + fnfe.toString());
		}
		
		return content;

	}
	
	public static byte[] getFileContentAsBytes(String filename)
	{
		byte[] contentBytes = null;
		try
		{
			XmlReader fileReader = new XmlReader();
			contentBytes = fileReader.readFile(filename);	
		}
		catch (FileNotFoundException fnfe)
		{
			log.error("Error in getFileContent: " + fnfe.toString());
		}
		
		return contentBytes;

	}	
	
	public static byte[] getImageContentAsBytes(String filename)
	{
		byte[] contentBytes = null;
		try
		{
			XmlReader fileReader = new XmlReader();
			contentBytes = fileReader.readFile(filename);	
		}
		catch (FileNotFoundException fnfe)
		{
			log.error("Error in getFileContent: " + fnfe.toString());
		}
		
		return contentBytes;

	}	
}
