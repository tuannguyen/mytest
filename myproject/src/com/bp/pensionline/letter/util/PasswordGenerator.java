package com.bp.pensionline.letter.util;

import java.util.Random;


/**
 * An Automatically generated password must be 8 characters long and have no strange or confusing characters.
 * 
 * A personally chosen paswword by the user may have 8 characters and may have any characters available.
 * 
 * AUTO GENERATION
 * ===============
 * Must be 8 characters, with one Capital letter, one lowercase, one number
 * this will create a password string with the following format
 * it will contain a minimum of one Number and one lowercase and one Upper case character
 * 
 * It will adjust automatically for the length of arrays and characters in them
 *
 * This will remove difficult to distinguish characters from passwords, eg. 0Oo, or zero Oh and oh
 * 
 * @author Dominic Carlyle
 *
 */
public class PasswordGenerator
{
	public static int DEFAULT_PASSWORD_LENGTH = 8; // pwd_length
	
	public static final char[][] PASSWRD_CHAR_SET = {"acdefghmnpqrtwxyz".toCharArray(), "ACDEFMNPRTWXYZ".toCharArray(), "2346789".toCharArray()};

	/**
	 * Passwords must be at least eight characters long and contain characters from 
	 * at least three of the following four character groups: lower case letters, 
	 * upper case letters, numbers and special characters, e.g. exAmpLe29?
	 * @return password
	 */
	public static String generateInitialMemberPassword ()
	{
		StringBuffer passwordBuf = new StringBuffer();
		
		Random random = new Random();
		
		while (true)
		{
			for (int i = 0; i <DEFAULT_PASSWORD_LENGTH; i++)
			{
				int nextIndex = random.nextInt();
				if (nextIndex < 0)
				{
					nextIndex *= -1;
				}
				int setIndex = nextIndex%PASSWRD_CHAR_SET.length;			
				int charIndex = nextIndex%PASSWRD_CHAR_SET[setIndex].length;
				
				passwordBuf.append(PASSWRD_CHAR_SET[setIndex][charIndex]);
			}
			
			if (validatePassword(passwordBuf.toString()))
			{
				return passwordBuf.toString();
			}
			else
			{
				passwordBuf.delete(0, passwordBuf.length());
			}
		}
	
	}
	
	/**
	 * Passwords must be at least eight characters long and contain characters from 
	 * at least three of the following four character groups: lower case letters, 
	 * upper case letters, numbers and special characters, e.g. exAmpLe29?
	 * @return is password valid
	 */
    public static boolean validatePassword(String password)
    {
		if (password.length() >= DEFAULT_PASSWORD_LENGTH)
		{
			// matches all alphabetical + 0-9 + special chars
			//String regex = "[\\p{Punct}]";
			if (password.matches("[^2346789]*") || password.matches("[^ACDEFMNPRTWXYZ]*") || password.matches("[^acdefghmnpqrtwxyz]*"))
			{
				return false;
			}
			
			return true;
					
		}
		
		return false;
    }	
    
    public static void main(String []args) 
    {
    	boolean test = "adMnAPmN".matches("[^acdefghmnpqrtwxyzACDEFMNPRTWXYZ2346789]");
    	System.out.println("test = " + test);
        //testing
    	for(int i=0; i < 100; i++ ){
    		System.out.println(generateInitialMemberPassword());
    	}
    }
 
}