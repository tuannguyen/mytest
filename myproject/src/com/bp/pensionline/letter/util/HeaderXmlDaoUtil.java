package com.bp.pensionline.letter.util;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.db.CmsResourceState;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.constants.HeaderLine;
import com.bp.pensionline.publishing.util.PLPublishThread;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

public class HeaderXmlDaoUtil {
	public static final Log log = CmsLog.getLog(HeaderXmlDaoUtil.class);
	
	private List<HeaderLine> headerLines = new ArrayList<HeaderLine>();
	
	/*
	 * This utility class will parse XML file to retrieve all stored signatures
	 */
	public HeaderXmlDaoUtil(String signatureXmlFileName) {
		// Get the template file names from letter_mapping.xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		XmlReader xmlReader = new XmlReader();
		try {
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null) {
				return;
			}
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);

			document = builder.parse(is);
		} catch (Exception ex) {
			log.error("Error in parsing XML Header: " + ex.toString());
			return;
		}
		
		Element root = document.getDocumentElement();
		NodeList paramNodes = root.getChildNodes();	
		
		int nlength = paramNodes.getLength();
		for (int i = 0; i < nlength; i++) {
			Node paramNode = paramNodes.item(i);
			if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE) {
				HeaderLine header = new HeaderLine();
				header.setName(paramNode.getNodeName());
				header.setValue(paramNode.getTextContent());
				getHeaderLines().add(header);
			}			
		}
		log.info("Header list size: " + getHeaderLines().size());
	}

	public HeaderXmlDaoUtil(List<HeaderLine> headerLines) {
		this.headerLines = headerLines;
	}
	
	public boolean saveHeaderSetting(String xmlPath) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElement("Header");
		
			HeaderLine headerLine = new HeaderLine();
			HeaderLine.HeaderLineComparator comparator = headerLine.new HeaderLineComparator();
			Collections.sort(headerLines, comparator);
			for (int i=0; i<headerLines.size(); i++) {
				HeaderLine line = headerLines.get(i);
				Node node = document.createElement(line.getName());
				node.setTextContent(line.getValue());
				root.appendChild(node);
			}
			document.appendChild(root);
			document2CmsXml(xmlPath, document);
		} catch (Exception e) {
			log.error("EXCEPTION while saving header setting: "+e.toString());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 */
	private static void document2CmsXml(String signatureXmlFileName, Document document) 
						                throws CmsException, TransformerConfigurationException,
						                	   TransformerException {
		Source source = new DOMSource(document);
		StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.transform(source, result);
		String xmlContent = sw.toString();
		System.out.print(xmlContent);
		
		CmsObject cmsObj = SystemAccount.getAdminCmsObject();
		//System.out.println("CmsObj: "+cmsObj);//just for trace
		cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
		cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
		if (cmsObj.existsResource(signatureXmlFileName)) {
			CmsLock lock = cmsObj.getLock(signatureXmlFileName);
			if (!lock.isUnlocked()) {
				cmsObj.changeLock(signatureXmlFileName);
			} else {
				cmsObj.lockResourceTemporary(signatureXmlFileName);
			}
			CmsFile cmsFile = cmsObj.readFile(signatureXmlFileName);
			cmsFile.setContents(xmlContent.getBytes());
			cmsFile.setState(CmsResourceState.STATE_CHANGED);
			cmsObj.writeFile(cmsFile);
			cmsObj.unlockResource(signatureXmlFileName);
			
			//then publish
        	CmsPublishList publishFile = OpenCms.getPublishManager().getPublishList(cmsObj, cmsFile, false);  	              																
    		PLPublishThread thread = new PLPublishThread(cmsObj, publishFile);
            // start the publish thread
            thread.start();
		}
	}

	/**
	 * @param headerLines the headerLines to set
	 */
	public void setHeaderLines(List<HeaderLine> headerLines) {
		this.headerLines = headerLines;
	}

	/**
	 * @return the headerLines
	 */
	public List<HeaderLine> getHeaderLines() {
		return headerLines;
	}

}
