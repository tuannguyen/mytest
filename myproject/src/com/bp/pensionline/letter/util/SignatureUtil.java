/**
 * 
 */
package com.bp.pensionline.letter.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResourceFilter;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.constants.IconFile;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author AS5920G
 *
 */

public class SignatureUtil {
	private String imageGallery = "";
	public static final Log LOG = CmsLog.getLog(SignatureUtil.class);
	
	public SignatureUtil(String gallery) {
		this.imageGallery = gallery;
	}
	public List<IconFile> getAllIcons() {
		List<IconFile> iconList = new ArrayList<IconFile>();
		if (imageGallery.trim().length() > 0) {
			try {
				CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
				cmsAdminObj.getRequestContext().setSiteRoot("/");				
				cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Online"));
				List listFilesNoDir = cmsAdminObj.getFilesInFolder(imageGallery, CmsResourceFilter.ALL);
				for (int i=0; i<listFilesNoDir.size(); i++) {
					CmsFile cmsFile = (CmsFile) listFilesNoDir.get(i);
					if (isImageFile(cmsFile.getName())) {
						IconFile icon = new IconFile();
						icon.setLocal(false);
						icon.setName(cmsFile.getName());
						icon.setUrl(cmsFile.getRootPath());
						iconList.add(icon);
					}
				}
			} catch (CmsException cmse) {
				LOG.error("Exception occurred while retrieving images from path \""+imageGallery+"\"....\n"+cmse.toString());
				cmse.printStackTrace();
			}
		}
		LOG.info("There are "+iconList.size()+" images in folder "+imageGallery);
		System.out.println("There are "+iconList.size()+" images in folder "+imageGallery);
		return iconList;
	}
	
	private boolean isImageFile(String fileName ) {
		LOG.info("Check extension of file "+fileName+" with extension "+Constants.SIGNATURE.IMAGE_EXTENSION);
		StringTokenizer tokens = new StringTokenizer(Constants.SIGNATURE.IMAGE_EXTENSION,",");
		while (tokens.hasMoreElements()) {
			String extension = "."+tokens.nextToken().trim();
			if (fileName.toLowerCase().endsWith(extension)) {
				return true;
			}
		}
		return false;
	}
}
