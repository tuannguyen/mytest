package com.bp.pensionline.letter.util;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.db.CmsResourceState;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.types.CmsResourceTypeImage;
import org.opencms.lock.CmsLock;
import org.opencms.lock.CmsLockType;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.constants.HeaderLine;
import com.bp.pensionline.letter.constants.HeaderLine.HeaderLineComparator;
import com.bp.pensionline.letter.dao.SignatureXmlDao;
import com.bp.pensionline.publishing.util.PLPublishThread;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.SystemAccount;

public class SignatureXmlDaoUtil {
	public static final Log log = CmsLog.getLog(SignatureXmlDaoUtil.class);
	
	private List<SignatureXmlDao> signList = new ArrayList<SignatureXmlDao>();
	
	/*
	 * This utility class will parse XML file to retrieve all stored signatures
	 */
	public SignatureXmlDaoUtil(String signatureXmlFileName) {
		// Get the template file names from letter_mapping.xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		XmlReader xmlReader = new XmlReader();
		try {
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null) {
				return;
			}
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);

			document = builder.parse(is);
		} catch (Exception ex) {
			log.error("Error in parsing XML signature: " + ex.toString());
			return;
		}
		
		Element root = document.getDocumentElement();
		NodeList paramNodes = root.getChildNodes();	
		
		int nlength = paramNodes.getLength();
		for (int i = 0; i < nlength; i++) {
			Node paramNode = paramNodes.item(i);
			if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE) {
				if (paramNode.getNodeName().equals(Constants.SIGNATURE.TAG_SIGNATURE)) {
					NodeList signNodes = paramNode.getChildNodes();
					String id = null;
					String name = null;
					String telephone = null;
					String email = null;
					String url = null;
					String imageFileName = null;
					String imageData = null;
					String title = null;
					boolean isUseOnlineImage = false;
					boolean isDefault = false;
					for (int k=0; k<signNodes.getLength(); k++) {
						Node signNode = signNodes.item(k);
						if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_ID.equals(signNode.getNodeName())) {
							id = signNode.getTextContent();
							id = id.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_NAME.equals(signNode.getNodeName())) {
							name = signNode.getTextContent();
							name = name.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_TELEPHONE.equals(signNode.getNodeName())) {
							telephone = signNode.getTextContent();
							telephone = telephone.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_EMAIL.equals(signNode.getNodeName())) {
							email = signNode.getTextContent();
							email = email.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_URL.equals(signNode.getNodeName())) {
							url = signNode.getTextContent();
							url = url.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_TITLE.equals(signNode.getNodeName())) {
							title = signNode.getTextContent();
							title = title.replaceAll("\\(amp\\)", "&").replaceAll("\\(gt\\)", ">").replaceAll("\\(lt\\)", "<").
							replaceAll("\\(plus\\)", "+").replaceAll("\\(percent\\)", "%");
						} else if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_DEFAULT.equals(signNode.getNodeName())) {
							isDefault = Boolean.parseBoolean(signNode.getTextContent());
						} else {
							NodeList locationNodes = signNode.getChildNodes();
							for (int j = 0; j < locationNodes.getLength(); j++) {
								Node locationNode = locationNodes.item(j);
								if (locationNode.getNodeType() == Node.ELEMENT_NODE && 
										locationNode.getNodeName().equals(Constants.SIGNATURE.TAG_ONLINE))							
								{							
									isUseOnlineImage = Boolean.parseBoolean(locationNode.getTextContent());
								}
								else if (locationNode.getNodeType() == Node.ELEMENT_NODE && 
										locationNode.getNodeName().equals(Constants.SIGNATURE.TAG_PATH))
								{
									imageFileName = locationNode.getTextContent();
									
									if (isUseOnlineImage)
									{
										// get signature image from CMS
										byte[] signatureImgBytes = FileUtil.getFileContentAsBytes(imageFileName);
										if (signatureImgBytes != null) {
											imageData = new sun.misc.BASE64Encoder().encode(signatureImgBytes);						
										}									
									}						
								}
							}
						}
					}
					SignatureXmlDao sign = new SignatureXmlDao(isUseOnlineImage, imageFileName, imageData,
															   name, title, telephone, email, url, id, isDefault);
					signList.add(sign);
				}		
			}
			log.info("Signature list size: " + signList.size());			
		}
	}

	/**
	 * @param signList the signList to set
	 */
	public void setSignList(List<SignatureXmlDao> signList) {
		this.signList = signList;
	}

	/**
	 * @return the signList
	 */
	public List<SignatureXmlDao> getSignList() {
		SignatureXmlDao defaultObj = null;
		for (int i=0; i<signList.size(); i++) {
			if (signList.get(i).isDefault()) {
				defaultObj = signList.remove(i);
				break;
			}
		}
		SignatureXmlDao signature = new SignatureXmlDao();
		SignatureXmlDao.SignatureComparator comparator = signature.new SignatureComparator();
		Collections.sort(signList, comparator);
		signList.add(0, defaultObj);
		return signList;
	}
	
	/**
	 * @param SignatureXmlDao the signature
	 */
	public void addSignature(SignatureXmlDao signature) {
		signList.add(signature);
	}
	
	/**
	 * @return the signature
	 */
	public SignatureXmlDao getSignature(String id) {
		for (int i=0; i<signList.size(); i++) {
			SignatureXmlDao signature = signList.get(i);
			if (signature.getId().equals(id)) {
				return signature;
			}
		}
		return null;
	}
	
	/**
	 * 
	 */
	public static String uploadUserLocalImage(String fileName, byte[] imageData) {
		String filePath = Constants.SIGNATURE.IMAGE_GALLERY_LOCATION+fileName;
		try {
			CmsObject cmsObj = SystemAccount.getAdminCmsObject();
			cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
			cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
			int i=0;
			String extension = fileName.substring(fileName.lastIndexOf('.'));
			String path = "";
			while(cmsObj.existsResource(filePath)) {
				path = filePath.substring(0, filePath.lastIndexOf('.'));
				filePath = path+(++i)+extension;
			}
			CmsResource cmsFile = cmsObj.createResource(filePath, CmsResourceTypeImage.getStaticTypeId(), imageData, new ArrayList());
//			cmsFile.setState(CmsResourceState.STATE_CHANGED);
//			cmsObj.writeResource(cmsFile);			
			//then publish
        	CmsPublishList publishFile = OpenCms.getPublishManager().getPublishList(cmsObj, cmsFile, false);  	              																
    		PLPublishThread thread = new PLPublishThread(cmsObj, publishFile);
            // start the publish thread
            thread.start();
            
		} catch (CmsException cme) {
			cme.printStackTrace();
			return null;
		}
		
		return filePath;
	}
	/**
	 * Update signature by id,
	 * @param signatureXmlFileName the configuration file
	 * @param id the signature id to update
	 * @param signature the signature content
	 */
	public static boolean updateSignature(String signatureXmlFileName, String id, SignatureXmlDao signature) {
		log.info("updateSignatureDaoXml():BEGIN");
		try {
			log.info("updating "+id);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			XmlReader xmlReader = new XmlReader();
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null) {
				return false;
			}
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);
			document = builder.parse(is);
			
			/*
			 * update signature
			 */
			Element root = document.getDocumentElement();
			Node oldNode = null;
			NodeList paramNodes = root.getChildNodes();
			boolean found = false;
			for (int i = 0; i < paramNodes.getLength(); i++) {
				oldNode = paramNodes.item(i);
				if (found) break;
				if (oldNode != null && oldNode.getNodeType() == Node.ELEMENT_NODE) {
					if (oldNode.getNodeName().equals(Constants.SIGNATURE.TAG_SIGNATURE)) {
						NodeList signNodes = oldNode.getChildNodes();
						for (int k=0; k<signNodes.getLength(); k++) {
							Node signNode = signNodes.item(k);
							if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_ID.equals(signNode.getNodeName())) {
								if (id.equals(signNode.getTextContent())) {
									oldNode = root.removeChild(oldNode);
									break;
								}
							}
						}
					}
				}
			}
			System.out.println("OLD: "+oldNode);
			root.appendChild(buildNode(document, signature));
			
			document2CmsXml(signatureXmlFileName, document);
			
		} catch (TransformerConfigurationException tce) {
			tce.printStackTrace();
			return false;
		} catch (TransformerException te) {
			te.printStackTrace();
			return false;
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		} catch (SAXException saxe) {
			saxe.printStackTrace();
			return false;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} catch (CmsException e) {
			e.printStackTrace();
			return false;
		}
		log.info("updateSignatureDaoXml():END");
		return true;
	}
	/**
	 * Delete Signature by id
	 * @param signatureXmlFileName the configuration file
	 * @param id the signature id to delete
	 */
	public static boolean deleteSignature(String signatureXmlFileName, String id) {
		log.info("deleteSignature():BEGIN");
		try {
			log.info("delete "+id);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			XmlReader xmlReader = new XmlReader();
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null) {
				return false;
			}
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);
			document = builder.parse(is);
			
			/*
			 * update signature
			 */
			Element root = document.getDocumentElement();
			Node oldNode = null;
			NodeList paramNodes = root.getChildNodes();
			boolean found = false;
			for (int i = 0; i < paramNodes.getLength(); i++) {
				oldNode = paramNodes.item(i);
				if (found) break;
				if (oldNode != null && oldNode.getNodeType() == Node.ELEMENT_NODE) {
					if (oldNode.getNodeName().equals(Constants.SIGNATURE.TAG_SIGNATURE)) {
						NodeList signNodes = oldNode.getChildNodes();
						for (int k=0; k<signNodes.getLength(); k++) {
							Node signNode = signNodes.item(k);
							if (signNode.getNodeType() == Node.ELEMENT_NODE && Constants.SIGNATURE.TAG_ID.equals(signNode.getNodeName())) {
								if (id.equals(signNode.getTextContent())) {
									oldNode = root.removeChild(oldNode);
									break;
								}
							}
						}
					}
				}
			}
			document2CmsXml(signatureXmlFileName, document);
			
		} catch (TransformerConfigurationException tce) {
			tce.printStackTrace();
			return false;
		} catch (TransformerException te) {
			te.printStackTrace();
			return false;
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		} catch (SAXException saxe) {
			saxe.printStackTrace();
			return false;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} catch (CmsException e) {
			e.printStackTrace();
			return false;
		}
		log.info("deleteSignature():END");
		return true;
	}
	/**
	 * Create new Signature
	 * @param signatureXmlFileName the configuration file
	 * @param signature the Signature to create
	 */
	public static boolean addSignature(String signatureXmlFileName, SignatureXmlDao signature) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			XmlReader xmlReader = new XmlReader();
			byte[] letterMappingByteArr = xmlReader.readFile(signatureXmlFileName);
			if (letterMappingByteArr == null) {
				return false;
			}
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);
			document = builder.parse(is);
			
			/*
			 * add signature
			 */
			Element root = document.getDocumentElement();
			root.appendChild(buildNode(document, signature));
			document2CmsXml(signatureXmlFileName, document);
			
		} catch (TransformerConfigurationException tce) {
			tce.printStackTrace();
			return false;
		} catch (TransformerException te) {
			te.printStackTrace();
			return false;
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			return false;
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return false;
		} catch (SAXException saxe) {
			saxe.printStackTrace();
			return false;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		} catch (CmsException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	/**
	 * 
	 */
	private static Node buildNode(Document document, SignatureXmlDao signature) {
		Node sNode = document.createElement(Constants.SIGNATURE.TAG_SIGNATURE);
		Node idNode = document.createElement(Constants.SIGNATURE.TAG_ID);
		idNode.setTextContent(signature.getId());
		sNode.appendChild(idNode);
		Node imageNode = document.createElement(Constants.SIGNATURE.TAG_LOCATION);
		Node isOnline = document.createElement(Constants.SIGNATURE.TAG_ONLINE);
		isOnline.setTextContent(String.valueOf(signature.isUseOnlineImage()));
		Node path = document.createElement(Constants.SIGNATURE.TAG_PATH);
		path.setTextContent(signature.getImageFileName());
		imageNode.appendChild(isOnline);
		imageNode.appendChild(path);
		sNode.appendChild(imageNode);
		Node name = document.createElement(Constants.SIGNATURE.TAG_NAME);
		name.setTextContent(signature.getName());
		sNode.appendChild(name);
		Node title = document.createElement(Constants.SIGNATURE.TAG_TITLE);
		title.setTextContent(signature.getTitle());
		sNode.appendChild(title);
		Node phone = document.createElement(Constants.SIGNATURE.TAG_TELEPHONE);
		phone.setTextContent(signature.getTelephone());
		sNode.appendChild(phone);
		Node email = document.createElement(Constants.SIGNATURE.TAG_EMAIL);
		email.setTextContent(signature.getEmail());
		sNode.appendChild(email);
		Node url = document.createElement(Constants.SIGNATURE.TAG_URL);
		url.setTextContent(signature.getUrl());
		sNode.appendChild(url);
		Node def = document.createElement(Constants.SIGNATURE.TAG_DEFAULT);
		def.setTextContent(String.valueOf(signature.isDefault()));
		sNode.appendChild(def);
		return sNode;
	}
	/**
	 * 
	 */
	private static void document2CmsXml(String signatureXmlFileName, Document document) 
						                throws CmsException, TransformerConfigurationException,
						                	   TransformerException {
		Source source = new DOMSource(document);
		StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.transform(source, result);
		String xmlContent = sw.toString();
		System.out.print(xmlContent);
		
		CmsObject cmsObj = SystemAccount.getAdminCmsObject();
		//System.out.println("CmsObj: "+cmsObj);//just for trace
		cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
		cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
		if (cmsObj.existsResource(signatureXmlFileName)) {
			CmsLock lock = cmsObj.getLock(signatureXmlFileName);
			if (!lock.isUnlocked()) {
				cmsObj.changeLock(signatureXmlFileName);
			} else {
				cmsObj.lockResourceTemporary(signatureXmlFileName);
			}
			CmsFile cmsFile = cmsObj.readFile(signatureXmlFileName);
			cmsFile.setContents(xmlContent.getBytes());
			cmsFile.setState(CmsResourceState.STATE_CHANGED);
			cmsObj.writeFile(cmsFile);
			cmsObj.unlockResource(signatureXmlFileName);
			
			//then publish
        	CmsPublishList publishFile = OpenCms.getPublishManager().getPublishList(cmsObj, cmsFile, false);  	              																
    		PLPublishThread thread = new PLPublishThread(cmsObj, publishFile);
            // start the publish thread
            thread.start();
		}
	}
}
