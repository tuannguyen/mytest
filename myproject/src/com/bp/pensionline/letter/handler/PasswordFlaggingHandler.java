package com.bp.pensionline.letter.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.producer.MemberLoader;

public class PasswordFlaggingHandler extends HttpServlet
{
	public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
		
	
	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * 
	 */
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/xml");
		PrintWriter out = response.getWriter();
		String command = request.getParameter("command");		

		log.info("command: " + command);
		
		try
		{
			if (command != null && command.equals("list"))
			{
				/*
				 * Initialize members from database
				 */
				ArrayList<String> totalWelcomePackNinos = MemberLoader.getNinoByPasswordFlaggingType("W");
				ArrayList<String> totalPasswordResetNinos = MemberLoader.getNinoByPasswordFlaggingType("P");
				
				String responseXML = buildPasswordFlaggedListXML(totalWelcomePackNinos, totalPasswordResetNinos);
				
				out.print(responseXML);
				out.flush();
				out.close();				
			}
			else if (command != null && command.equals("update"))
			{
				String welcomePackIdListStr = request.getParameter("wpFlaggedList"); 
				String passwordResetIdListStr = request.getParameter("prFlaggedList");
				String lastWelcomePackIdListStr = request.getParameter("lastWPFlaggedList"); 
				String lastPasswordResetIdListStr = request.getParameter("lastPRFlaggedList");
				
				log.info("welcomePackIdListStr: " + welcomePackIdListStr);
				log.info("passwordResetIdListStr: " + passwordResetIdListStr);
				log.info("lastWelcomePackIdListStr: " + lastWelcomePackIdListStr);
				log.info("lastPasswordResetIdListStr: " + lastPasswordResetIdListStr);
				
				boolean isUpdatedListValid = false;
				if (welcomePackIdListStr != null)
				{
					isUpdatedListValid = updatePasswordFlaggedMember(welcomePackIdListStr, lastWelcomePackIdListStr, "W");
				}
				if (passwordResetIdListStr != null)
				{
					isUpdatedListValid = updatePasswordFlaggedMember(passwordResetIdListStr, lastPasswordResetIdListStr, "P");
				}
				
				
				String responseXML = buildPasswordFlaggedUpdateXML(isUpdatedListValid);
				
				out.print(responseXML);
				out.flush();
				out.close();				
			}
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();

		}

	}
	
	/**
	 * Update password flagged for members contains in listIdStr.
	 * Type "W" for welcome pack, "P" for password reset
	 * 
	 * @return true if the all members in the new list exist, else return false.
	 * @param listIdStr
	 * @param type
	 */
	public boolean updatePasswordFlaggedMember (String listNinoStr, String lastNinoStr, String type)
	{
		if (listNinoStr == null)
		{
			return false;
		}
		
		boolean isPasswordFlaggedUpdatedListValid = true;
		
		if (lastNinoStr != null)
		{
			String[] arrLastNinos = lastNinoStr.split(",");
			ArrayList<String> lastNinoList = new ArrayList<String>();
			for (int i = 0; i < arrLastNinos.length; i++)
			{
				String nino = arrLastNinos[i];
				if (nino != null && !nino.trim().equals(""))
				{
					nino = nino.trim();
					lastNinoList.add(nino);
					//log.info("Nino added to lastNinoList: " + nino);
				}
			}
			// set reset of all last member to null
			MemberLoader.updateListPasswordFlaggedMember(lastNinoList, null);
		}
		
		String[] arrNinos = listNinoStr.split(",");
		for (int i = 0; i < arrNinos.length; i++)
		{
			String nino = arrNinos[i];
			if (nino != null && !nino.trim().equals(""))
			{
				nino = nino.trim();
//				log.info("Insert password flagged member: " + nino);
				if (MemberLoader.isMemberExistByNino(nino))
				{
					MemberLoader.insertPasswordFlaggedMember(nino, type);
				}
				else
				{
					log.error("Member with this NI does not exist: " + nino);
					isPasswordFlaggedUpdatedListValid = false;
				}
				
			}
		}
		
		return isPasswordFlaggedUpdatedListValid;
	}
	
	public String buildPasswordFlaggedListXML(ArrayList<String> totalWelcomePackBPFMemberIds, ArrayList<String> totalWelcomePackBCFMemberIds,
			ArrayList<String> totalWelcomePackSubsMemberIds, ArrayList<String> totalPasswordResetMemberIds) 
	{
		StringBuffer buffer = new StringBuffer();
		
		// build up the list contents
		StringBuffer welcomePackListBuffer = new StringBuffer();
		for (int i = 0; i < totalWelcomePackBPFMemberIds.size(); i++)
		{
			welcomePackListBuffer.append(totalWelcomePackBPFMemberIds.get(i)).append("\n");
		}
		for (int i = 0; i < totalWelcomePackBCFMemberIds.size(); i++)
		{
			welcomePackListBuffer.append(totalWelcomePackBCFMemberIds.get(i)).append("\n");
		}		
		for (int i = 0; i < totalWelcomePackSubsMemberIds.size(); i++)
		{
			welcomePackListBuffer.append(totalWelcomePackSubsMemberIds.get(i)).append("\n");
		}
		
		StringBuffer passwordResetListBuffer = new StringBuffer();
		for (int i = 0; i < totalPasswordResetMemberIds.size(); i++)
		{
			passwordResetListBuffer.append(totalPasswordResetMemberIds.get(i)).append("\n");
		}		
		

		buffer.append("<").append(Constants.AJAX_LETTER_RESPONSE).append(">\n");
		buffer.append("\t<").append(Constants.LETTER_PASSWORD).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_FLAGGED_WELCOME_PACK_LIST).append(">")
			.append(welcomePackListBuffer).append("</").append(Constants.LETTER_PASSWORD_FLAGGED_WELCOME_PACK_LIST).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_FLAGGED_PASSWORD_RESET_LIST).append(">")
			.append(passwordResetListBuffer).append("</").append(Constants.LETTER_PASSWORD_FLAGGED_PASSWORD_RESET_LIST).append(">\n");		
				
		buffer.append("\t</").append(Constants.LETTER_PASSWORD).append(">\n");
		buffer.append("</").append(Constants.AJAX_LETTER_RESPONSE).append(">");
		
		return buffer.toString();		
	}
	
	public String buildPasswordFlaggedListXML(ArrayList<String> totalWelcomePackNinos, 
			ArrayList<String> totalPasswordResetNinos) 
	{
		StringBuffer buffer = new StringBuffer();
		
		// build up the list contents
		StringBuffer welcomePackListBuffer = new StringBuffer();
		for (int i = 0; i < totalWelcomePackNinos.size(); i++)
		{
			welcomePackListBuffer.append(totalWelcomePackNinos.get(i).toLowerCase()).append("\n");
		}

		StringBuffer passwordResetListBuffer = new StringBuffer();
		for (int i = 0; i < totalPasswordResetNinos.size(); i++)
		{
			passwordResetListBuffer.append(totalPasswordResetNinos.get(i).toLowerCase()).append("\n");
		}		
		

		buffer.append("<").append(Constants.AJAX_LETTER_RESPONSE).append(">\n");
		buffer.append("\t<").append(Constants.LETTER_PASSWORD).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_FLAGGED_WELCOME_PACK_LIST).append(">")
			.append(welcomePackListBuffer).append("</").append(Constants.LETTER_PASSWORD_FLAGGED_WELCOME_PACK_LIST).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_FLAGGED_PASSWORD_RESET_LIST).append(">")
			.append(passwordResetListBuffer).append("</").append(Constants.LETTER_PASSWORD_FLAGGED_PASSWORD_RESET_LIST).append(">\n");		
				
		buffer.append("\t</").append(Constants.LETTER_PASSWORD).append(">\n");
		buffer.append("</").append(Constants.AJAX_LETTER_RESPONSE).append(">");
		
		return buffer.toString();		
	}
	
	public String buildPasswordFlaggedUpdateXML(boolean isUpdateListValid) 
	{
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constants.AJAX_LETTER_RESPONSE).append(">\n");
		buffer.append("\t<").append(Constants.LETTER_PASSWORD).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_FLAGGED_UPDATE_LIST_VALDIATE).append(">")
			.append(isUpdateListValid).append("</").append(Constants.LETTER_PASSWORD_FLAGGED_UPDATE_LIST_VALDIATE).append(">\n");			
				
		buffer.append("\t</").append(Constants.LETTER_PASSWORD).append(">\n");
		buffer.append("</").append(Constants.AJAX_LETTER_RESPONSE).append(">");
		
		return buffer.toString();		
	}	

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}
}
