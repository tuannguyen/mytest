/**
 * 
 */
package com.bp.pensionline.letter.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsPublishList;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.types.CmsResourceTypePlain;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.publishing.util.PLPublishThread;
import com.bp.pensionline.sqlreport.constants.Constant;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author AS5920G
 *
 */
public class SignatureHandler extends HttpServlet {
	public static final Log LOG = CmsLog.getLog(SignatureHandler.class);
	
	private void processRequest(HttpServletRequest request,
								HttpServletResponse response) throws ServletException, IOException {
		String online = request.getParameter(Constants.SIGNATURE.TAG_ONLINE);
		String imagePath = request.getParameter(Constants.SIGNATURE.TAG_PATH);
		String name = request.getParameter(Constants.SIGNATURE.TAG_NAME);
		String title = request.getParameter(Constants.SIGNATURE.TAG_TITLE);
		
		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		boolean saveOK = true;
		String error = "";
		try {
			CmsObject cmsAdminObj = SystemAccount.getAdminCmsObject();
			cmsAdminObj.getRequestContext().setSiteRoot("/");				
			cmsAdminObj.getRequestContext().setCurrentProject(cmsAdminObj.readProject("Offline"));
			
			boolean isOnline = Boolean.parseBoolean(online);
			String xmlContent = buildXmlContent(isOnline, imagePath, name, title);
			if (!cmsAdminObj.existsResource(Constants.SIGNATURE.XML_PATH)) {
				CmsResource file = cmsAdminObj.createResource(Constants.SIGNATURE.XML_PATH, CmsResourceTypePlain.getStaticTypeId(), xmlContent.getBytes(), new ArrayList());
	        	CmsLock lock = cmsAdminObj.getLock(Constants.SIGNATURE.XML_PATH);
				if (lock.isNullLock()) {
					// resource is unlocked, so lock it
					cmsAdminObj.lockResource(Constants.SIGNATURE.XML_PATH);
				}
	        	//cmsAdminObj.writeFile(file);
	        	cmsAdminObj.changeLock(Constants.SIGNATURE.XML_PATH);
	        	cmsAdminObj.unlockResource(Constants.SIGNATURE.XML_PATH);
	        	
	        	CmsPublishList publishList = cmsAdminObj.getPublishList(file, false);
	        	PLPublishThread thread = new PLPublishThread(cmsAdminObj, publishList);
                LOG.info("Start publishing thread");
                // start the publish thread
                thread.start();
                
			} else {
				CmsFile file = cmsAdminObj.readFile(Constants.SIGNATURE.XML_PATH);
				CmsLock lock = cmsAdminObj.getLock(Constants.SIGNATURE.XML_PATH);
				if (lock.isNullLock()) {
					// resource is unlocked, so lock it
					cmsAdminObj.lockResource(Constants.SIGNATURE.XML_PATH);
				}
				file.setContents(xmlContent.getBytes());
				file.setType(CmsResourceTypePlain.getStaticTypeId());
	        	cmsAdminObj.writeFile(file);
	        	cmsAdminObj.changeLock(Constants.SIGNATURE.XML_PATH);
	        	cmsAdminObj.unlockResource(Constants.SIGNATURE.XML_PATH);
	        	
	        	CmsPublishList publishList = cmsAdminObj.getPublishList(file, false);
	        	PLPublishThread thread = new PLPublishThread(cmsAdminObj, publishList);
                LOG.info("Start publishing thread");
                // start the publish thread
                thread.start();
			}
			/*save path to session for reference
			if (!isOnline) {
				imagePath = imagePath.replace('\\','@');
			}
			request.getSession().setAttribute(Constants.SIGNATURE.TAG_PATH, imagePath);
			request.getSession().setAttribute(Constants.SIGNATURE.TAG_ONLINE, Boolean.toString(isOnline));
			request.getSession().setAttribute(Constants.SIGNATURE.TAG_NAME, name);
			request.getSession().setAttribute(Constants.SIGNATURE.TAG_TITLE, title);
			*/
			
		} catch (CmsException cmse) {
			saveOK = false;
			error = cmse.getMessage();
			LOG.error("Exception occurred while writing to \""+Constants.SIGNATURE.XML_LOCATION+"\"....\n"+cmse.toString());
			cmse.printStackTrace();
		} catch (Exception e) {
			saveOK = false;
			error = e.getMessage();
			LOG.error("Exception occurred while writing to \""+Constants.SIGNATURE.XML_LOCATION+"\"....\n"+e.toString());
			e.printStackTrace();
		}
		String xmlResult = buildXmlResult(saveOK, Constants.SIGNATURE.XML_PATH, error);
		out.print(xmlResult);
		out.close();
	}
	
	private String buildXmlContent(boolean isOnline, String imagePath, String name, String title) {
		StringBuffer buf = new StringBuffer();
		String l = "<";
		String r = ">";
		String lc = "</";
		imagePath = imagePath.replace('\\', '/');
		buf.append(l+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_LOCATION+r).append('\n');
		buf.append("		"+l+Constants.SIGNATURE.TAG_ONLINE+r).append(isOnline).append(lc+Constants.SIGNATURE.TAG_ONLINE+r).append('\n');
		buf.append("		"+l+Constants.SIGNATURE.TAG_PATH+r).append(imagePath).append(lc+Constants.SIGNATURE.TAG_PATH+r).append('\n');
		buf.append("	"+lc+Constants.SIGNATURE.TAG_LOCATION+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_NAME+r).append(name).append(lc+Constants.SIGNATURE.TAG_NAME+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_TITLE+r).append(title).append(lc+Constants.SIGNATURE.TAG_TITLE+r).append('\n');
		buf.append(lc+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		return buf.toString();
	}
	
	private String buildXmlResult(boolean saveOK, String path, String error) {
		StringBuffer buf = new StringBuffer();
		String l = "<";
		String r = ">";
		String lc = "</";
		buf.append(l+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		buf.append(l+Constants.SIGNATURE.TAG_RESULT+r).append(saveOK).append(lc+Constants.SIGNATURE.TAG_RESULT+r).append('\n');
		if (!saveOK) {
			buf.append(l+Constants.SIGNATURE.TAG_ERROR+r).append(error).append(lc+Constants.SIGNATURE.TAG_ERROR+r).append('\n');
		}
		buf.append(l+Constants.SIGNATURE.TAG_PATH+r).append(path).append(lc+Constants.SIGNATURE.TAG_PATH+r).append('\n');
		buf.append(lc+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		return buf.toString();
	}
	
	protected void doGet(HttpServletRequest request,
			 			 HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
