/**
 * 
 */
package com.bp.pensionline.letter.handler;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.dao.SignatureXmlDao;
import com.bp.pensionline.letter.util.SignatureXmlDaoUtil;

/**
 * @author AS5920G
 *
 */
public class SignatureCreator extends HttpServlet {
	public static final Log LOG = CmsLog.getLog(SignatureCreator.class);
	
	private void processRequest(HttpServletRequest request,
								HttpServletResponse response) throws ServletException, IOException {
		try {
			if (ServletFileUpload.isMultipartContent(request)) {
				FileItemFactory factory = new DiskFileItemFactory();
                // Create a new file upload handler
                ServletFileUpload upload = new ServletFileUpload(factory);
                // Parse the request
                List /* FileItem */ items = upload.parseRequest(request);
                Iterator iter = items.iterator();
                FileItem item = null;
                FileItem tmpItem = null;
                String imageName = "";
                String storeName = null;
                String action = null;
                String online = null;
				boolean isOnline = false;
				String uri = null;
				String path = null;
				String name = null;
				String phone = null;
				String email = null;
				String url = null; 
				String id = null; //used for update
				String title = null;
                while (iter.hasNext()) {
                    tmpItem = (FileItem) iter.next();
                    if (tmpItem.isFormField()) {
                        String fieldName = tmpItem.getFieldName();
                        String value = tmpItem.getString().trim();
                        System.out.println(fieldName + ": " + value);
                        if(Constants.SIGNATURE.TAG_NAME.equals(fieldName)) {
                        	storeName = value.replace(' ','.');
                        	name = value;
                        } else if (Constants.SIGNATURE.TAG_PATH.equals(fieldName)) {
                        	path = value.replace('\\', '/');
                        } else if (Constants.SIGNATURE.URI.equals(fieldName)) {
                        	uri = value;
                        } else if (Constants.SIGNATURE.TAG_ONLINE.equals(fieldName)) {
                        	online = value;
                        } else if (Constants.SIGNATURE.TAG_TELEPHONE.equals(fieldName)) {
                        	phone = value;
                        } else if (Constants.SIGNATURE.TAG_EMAIL.equals(fieldName)) {
                        	email = value;
                        } else if (Constants.SIGNATURE.TAG_URL.equals(fieldName)) {
                        	url = value;
                        } else if (Constants.SIGNATURE.ACTION.equals(fieldName)) {
                        	action = value;
                        } else if (Constants.SIGNATURE.TAG_ID.equals(fieldName)) {
                        	id = value;
                        } else if (Constants.SIGNATURE.TAG_TITLE.equals(fieldName)) {
                        	title = value;
                        }
                        
                    } else {
                        item = tmpItem;
                        String fileName = item.getName();
                        imageName = fileName;
                        long sizeInBytes = item.getSize();
                        System.out.println(imageName + ": " + fileName + "-"
                            + sizeInBytes);
                    }
                }
                if (id==null | id.trim().length() == 0) id=name;
                if (Constants.SIGNATURE.ACTION_SAVE.equals(action)) {
                	if (Constants.SIGNATURE.TAG_ONLINE.equalsIgnoreCase(online)) {
                		isOnline = true;
                	}
                	if (item != null && !isOnline) {
                		isOnline = true;
                		if(storeName==null) {
                    		int idx = imageName.lastIndexOf('\\');
                    		if (idx == -1) idx = imageName.lastIndexOf('/');
                    		if (idx!=-1) storeName = imageName.substring(idx+1);
                    		else storeName = imageName;
                        } else {
                        	storeName += imageName.substring(imageName.indexOf('.'));
                        }
                		path = SignatureXmlDaoUtil.uploadUserLocalImage(storeName, item.get());
                    	LOG.info("UPLOADING "+storeName+" is "+path);
                    }
	                SignatureXmlDao signature = new SignatureXmlDao(isOnline, path, null, name, title, phone, email, url, id, false);
					boolean ok = SignatureXmlDaoUtil.addSignature(Constants.SIGNATURE.XML_VFS_PATH, signature);
					System.out.println(path+"-"+ok);
					response.sendRedirect(uri);
                } else if (Constants.SIGNATURE.ACTION_UPLOAD_IMAGE.equals(action)) {
                	/*
                	Not use
                	
                	request.getSession().setAttribute(Constants.SIGNATURE.TAG_ONLINE, Boolean.TRUE);
                	request.getSession().setAttribute(Constants.SIGNATURE.TAG_PATH, Constants.SIGNATURE.IMAGE_GALLERY_LOCATION+storeName);
                	request.getSession().setAttribute(Constants.SIGNATURE.TAG_ID, id);
                	response.sendRedirect(uri);
                	*/
                } else if (Constants.SIGNATURE.ACTION_UPDATE.equals(action)) {
                	if (Constants.SIGNATURE.TAG_ONLINE.equalsIgnoreCase(online)) {
                		isOnline = true;
                	}
                	//get current signature
                	SignatureXmlDaoUtil dao = new SignatureXmlDaoUtil(Constants.SIGNATURE.XML_VFS_PATH);
                	SignatureXmlDao sign = dao.getSignature(id);
                	if (sign.getImageFileName().equals(path)) { 
                		//if no new image, set online option as current
                		isOnline = sign.isUseOnlineImage();
                	} else {
                    	if (item != null && !isOnline) {
                    		isOnline = true;
                    		if(storeName==null) {
                        		int idx = imageName.lastIndexOf('\\');
                        		if (idx == -1) idx = imageName.lastIndexOf('/');
                        		if (idx!=-1) storeName = imageName.substring(idx+1);
                        		else storeName = imageName;
                            } else {
                            	storeName += imageName.substring(imageName.indexOf('.'));
                            }
                    		path = SignatureXmlDaoUtil.uploadUserLocalImage(storeName, item.get());
                        	isOnline = true;
                        	LOG.info("UPLOADING "+storeName+" is "+path);
                        }
                	}
                	SignatureXmlDao signature = new SignatureXmlDao(isOnline, path, null, name, title, phone, email, url, id, false);
                	System.out.println("Signature: "+signature);
                	boolean ok = SignatureXmlDaoUtil.updateSignature(Constants.SIGNATURE.XML_VFS_PATH, id, signature);
                	response.sendRedirect(uri);
                } else if (Constants.SIGNATURE.ACTION_DELETE.equals(action)) {
                	boolean ok = SignatureXmlDaoUtil.deleteSignature(Constants.SIGNATURE.XML_VFS_PATH, id);
                	System.out.println("Delete "+id+" "+ok);
                	response.sendRedirect(uri);
                }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String buildXmlContent(boolean isOnline, String imagePath, String name, String title) {
		StringBuffer buf = new StringBuffer();
		String l = "<";
		String r = ">";
		String lc = "</";
		imagePath = imagePath.replace('\\', '/');
		buf.append(l+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_LOCATION+r).append('\n');
		buf.append("		"+l+Constants.SIGNATURE.TAG_ONLINE+r).append(isOnline).append(lc+Constants.SIGNATURE.TAG_ONLINE+r).append('\n');
		buf.append("		"+l+Constants.SIGNATURE.TAG_PATH+r).append(imagePath).append(lc+Constants.SIGNATURE.TAG_PATH+r).append('\n');
		buf.append("	"+lc+Constants.SIGNATURE.TAG_LOCATION+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_NAME+r).append(name).append(lc+Constants.SIGNATURE.TAG_NAME+r).append('\n');
		buf.append("	"+l+Constants.SIGNATURE.TAG_TITLE+r).append(title).append(lc+Constants.SIGNATURE.TAG_TITLE+r).append('\n');
		buf.append(lc+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		return buf.toString();
	}
	
	private String buildXmlResult(boolean saveOK, String path, String error) {
		StringBuffer buf = new StringBuffer();
		String l = "<";
		String r = ">";
		String lc = "</";
		buf.append(l+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		buf.append(l+Constants.SIGNATURE.TAG_RESULT+r).append(saveOK).append(lc+Constants.SIGNATURE.TAG_RESULT+r).append('\n');
		if (!saveOK) {
			buf.append(l+Constants.SIGNATURE.TAG_ERROR+r).append(error).append(lc+Constants.SIGNATURE.TAG_ERROR+r).append('\n');
		}
		buf.append(l+Constants.SIGNATURE.TAG_PATH+r).append(path).append(lc+Constants.SIGNATURE.TAG_PATH+r).append('\n');
		buf.append(lc+Constants.SIGNATURE.TAG_SIGNATURE+r).append('\n');
		return buf.toString();
	}
	
	protected void doGet(HttpServletRequest request,
			 			 HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
