package com.bp.pensionline.letter.handler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.letter.constants.Constants;
import com.bp.pensionline.letter.dao.LetterMemberDao;
import com.bp.pensionline.letter.dao.SignatureXmlDao;
import com.bp.pensionline.letter.producer.LetterTransformer;
import com.bp.pensionline.letter.producer.MemberLoader;
import com.bp.pensionline.letter.producer.ResetPasswordLetterGenerator;
import com.bp.pensionline.letter.producer.WelcomeLetterGenerator;
import com.bp.pensionline.letter.util.FileUtil;
import com.bp.pensionline.letter.util.SignatureXmlDaoUtil;
import com.bp.pensionline.test.XmlReader;


public class PasswordLetterHandler extends HttpServlet
{
	public static final Log log = CmsLog.getLog(PasswordLetterHandler.class);	
	
	private static final String LETTER_NAME = "password";
	private static final String LETTER_RESET_PASSWORD_NAME = "reset_password";
	private static final String LETTER_WELCOME_PACK_NAME = "welcome_pack";
	
	private int numBPFUnregMembers = 0;
	private int numBCFUnregMembers = 0;
	private int numSubsUnregMembers = 0;
	private int numPensionResetMembers = 0;
	
	private boolean addSignature = false;
	private boolean commitChanges = false;
	
	// letter template files
	private String beforeHeaderFilename = null;
	private String headerFilename = null;
	private String bodyResetPasswordFilename = null;
	private String bodyWelcomePackFilename = null;
	private String footerFilename = null;
	private String afterFooterFilename = null;
	private String signatureLocalFilename = null;
	private String signatureCmsFilename = null;
	private String html2fo_xsl = null;
	private String fo2word_xsl = null;
	private String table_template = null; //template file for table
	private String table_template_content = ""; //template file for table
	private SignatureXmlDao signatureXmlDao = null;
	
	// letter template contents
	private String beforeHeaderContent = null;
	private String headerContent = null;
	private String footerContent = null;
	private String afterFooterContent = null;
	private String signatureContent = null;
	
	private String bodyPasswordResetContent = null;
	private String bodyWelcomePackContent = null;	
	
	/* Total member get at the first request to reduced database calls */
	private ArrayList<String> totalWelcomePackBPFMemberIds = new ArrayList<String>();
	private ArrayList<String> totalWelcomePackBCFMemberIds = new ArrayList<String>();
	private ArrayList<String> totalWelcomePackSubsMemberIds = new ArrayList<String>();
	private ArrayList<String> totalPasswordResetMemberIds = new ArrayList<String>();
	
	private ArrayList<LetterMemberDao> welcomPackMembers = new ArrayList<LetterMemberDao>();
	private ArrayList<String> welcomPackMemberIds = new ArrayList<String>();	
	private ArrayList<LetterMemberDao> resetPasswordMembers = new ArrayList<LetterMemberDao>();
	private ArrayList<String> resetPasswordMemberIds = new ArrayList<String>();	
	
	private ArrayList<String> ninoPasswordList = null;
	
	public static final String LETTER_PAGE_BREAK_MARKER = "((PageBreak))";
	private boolean isWelcomePackPageBreakMarked = false;
	private boolean isPasswordResetPageBreakMarked = false;
		
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		String xml = request.getParameter(Constants.PARAM);
		String xmlResponse = null;
		
		response.setContentType("text/xml");
		PrintWriter out = response.getWriter();			
		
		if (parseRequestXml(xml))
		{					
			if (isPasswordLetterInitRequest(request))
			{			
				// Initialize active numbers of welcome pack and reset password
				int totalBPFUnreg = (totalWelcomePackBPFMemberIds != null) ? totalWelcomePackBPFMemberIds.size() : 0;
				int totalBCFUnreg = (totalWelcomePackBCFMemberIds != null) ? totalWelcomePackBCFMemberIds.size() : 0;
				int totalSubsUnreg = (totalWelcomePackSubsMemberIds != null) ? totalWelcomePackSubsMemberIds.size() : 0;
				int totalPensionReset = (totalPasswordResetMemberIds != null) ? totalPasswordResetMemberIds.size() : 0;
					
				xmlResponse = buildPasswordResponseXML(totalBPFUnreg, totalBCFUnreg,
						totalSubsUnreg, totalPensionReset, false);
			}
			else
			{				
				// -- TEST
				//renderWordTest(request, response);			
				try
				{		
					signatureXmlDao = new SignatureXmlDao(Environment.LETTER_SIGNATURE_SETTING_FILE);
					updatePasswordMembers(request);
											
					if ((welcomPackMembers != null && welcomPackMembers.size() > 0)
							|| (resetPasswordMembers != null && resetPasswordMembers.size() > 0))
					{
						//request.getSession().setAttribute("membersExisted", "true");
						// Initialize content once
						if (initializeTemplateContents(request))
						{
							renderWord(welcomPackMembers, resetPasswordMembers, request);
							
							if (commitChanges)
							{
								commitPasswordChanges(request);
								
								// Update numbers of welcome pack and reset password
								int totalBPFUnreg = (totalWelcomePackBPFMemberIds != null) ? totalWelcomePackBPFMemberIds.size() : 0;
								int totalBCFUnreg = (totalWelcomePackBCFMemberIds != null) ? totalWelcomePackBCFMemberIds.size() : 0;
								int totalSubsUnreg = (totalWelcomePackSubsMemberIds != null) ? totalWelcomePackSubsMemberIds.size() : 0;
								int totalPensionReset = (totalPasswordResetMemberIds != null) ? totalPasswordResetMemberIds.size() : 0;
									
								xmlResponse = buildPasswordResponseXML(totalBPFUnreg, totalBCFUnreg,
										totalSubsUnreg, totalPensionReset, true);
								
							}
							else
							{
								// default response. Not necessary to send back data
								xmlResponse = buildPasswordResponseXML(0, 0, 0, 0, false);								
							}
						}	
					}
					else
					{
						log.info("There is no members satisfied!");
						xmlResponse = buildPasswordResponseXMLError();
					}
				}
				catch (Exception ex) {
					//throw new ServletException(ex);
					//make sure we swap back from xml
					log.error("Error in generating reset password letter: " + ex.toString());
					ex.printStackTrace();
					xmlResponse = buildPasswordResponseXMLError();
				}
			}
		}
		
		out.print(xmlResponse);
		out.close();		
	}
	
	/**TODO: update this method
	 * Update new members to the list
	 * @param newMemberIds
	 * @return
	 */
	private void updatePasswordMembers (HttpServletRequest request)
	{
		/*
		 * Begin updating
		 */
		// check if new member added
		welcomPackMemberIds = getNewWelcomePackMemberIds(numBPFUnregMembers, numBCFUnregMembers, numSubsUnregMembers);
		welcomPackMembers = new ArrayList<LetterMemberDao>();
		// check if new member added
		for (int i = 0; i < welcomPackMemberIds.size(); i++)
		{
			String newMemberId = welcomPackMemberIds.get(i);
			// Get LetterMemberDao for this new member
			LetterMemberDao newMember = MemberLoader.loadMemberById(newMemberId);
			if (newMember != null)
			{
				welcomPackMembers.add(newMember);
			}				
			log.info("Add new welcome pack member: " + newMemberId);
		}	
		
		// get new reset password member Ids
		resetPasswordMemberIds = getNewPasswordResetMemberIds(numPensionResetMembers);
		resetPasswordMembers = new ArrayList<LetterMemberDao>();
		// check if new member added
		for (int i = 0; i < resetPasswordMemberIds.size(); i++)
		{
			String newMemberId = resetPasswordMemberIds.get(i);
			// Get LetterMemberDao for this new member
			LetterMemberDao newMember = MemberLoader.loadMemberById(newMemberId);
			if (newMember != null)
			{
				resetPasswordMembers.add(newMember);	
			}
						
			log.info("Add new reset password member: " + newMemberId);
		}				
		
		log.info("Number of member reset password: " + resetPasswordMembers.size());
		log.info("Number of member welcome pack: " + welcomPackMembers.size());
	}
	
	/**
	 * Get new welcome pack member Ids from the total member Ids based on user input
	 */
	private ArrayList<String> getNewWelcomePackMemberIds (int numBPMemSubmit, int numBCSubmit, int numSubsSubmit)
	{
		ArrayList<String> newWelcomePackMemberIds = new ArrayList<String>();
		for (int i = 0; i < numBPMemSubmit; i++)
		{
			newWelcomePackMemberIds.add(totalWelcomePackBPFMemberIds.get(i));
		}
		for (int i = 0; i < numBCSubmit; i++)
		{
			newWelcomePackMemberIds.add(totalWelcomePackBCFMemberIds.get(i));
		}
		for (int i = 0; i < numSubsSubmit; i++)
		{
			newWelcomePackMemberIds.add(totalWelcomePackSubsMemberIds.get(i));
		}
		
		return newWelcomePackMemberIds;
	}
	
	/**
	 * Get new reset password member Ids from the total member Ids based on user input
	 */	
	private ArrayList<String> getNewPasswordResetMemberIds(int numPensionResetSubmit)
	{
		ArrayList<String> newPasswordResetMemberIds = new ArrayList<String>();
		for (int i = 0; i < numPensionResetSubmit; i++)
		{
			newPasswordResetMemberIds.add(totalPasswordResetMemberIds.get(i));
		}
		
		return newPasswordResetMemberIds;
	}
	
	/**
	 * Check if the list contain specified member with memberId 
	 * @param memberList
	 * @param memberId
	 * @return
	 */
	public LetterMemberDao getMemberFromList (ArrayList<LetterMemberDao> memberList, String memberId)
	{
		for (int i = 0; i < memberList.size(); i++)
		{
			LetterMemberDao member = (LetterMemberDao)memberList.get(i);
			if (member != null)
			{
				String comparedMemberId = member.get(Environment.MEMBER_BGROUP) + "-" +  member.get(Environment.MEMBER_REFNO);
				if (memberId != null && memberId.equals(comparedMemberId))
				{
					return member;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Update members after letter generated
	 *
	 */
	private void commitPasswordChanges (HttpServletRequest request)
	{
		if (ninoPasswordList == null)
		{
			return;
		}
		// check if new member added
		for (int i = 0; i < ninoPasswordList.size(); i++)
		{
			String ninoPasswordStr = ninoPasswordList.get(i);
			MemberLoader.updatePasswordForMember(ninoPasswordStr);
		}
		
		// reset the list
		ninoPasswordList = null;		
		
		// remove just updated members in the total password member Ids
		if (welcomPackMemberIds != null)
		{
			for (int i = 0; i < welcomPackMemberIds.size(); i++)
			{
				String welcomePackMemId = welcomPackMemberIds.get(i);
				if (totalWelcomePackBPFMemberIds != null && totalWelcomePackBPFMemberIds.contains(welcomePackMemId))
				{
					totalWelcomePackBPFMemberIds.remove(welcomePackMemId);
				}
				if (totalWelcomePackBCFMemberIds != null && totalWelcomePackBCFMemberIds.contains(welcomePackMemId))
				{
					totalWelcomePackBCFMemberIds.remove(welcomePackMemId);
				}
				if (totalWelcomePackSubsMemberIds != null && totalWelcomePackSubsMemberIds.contains(welcomePackMemId))
				{
					totalWelcomePackSubsMemberIds.remove(welcomePackMemId);
				}				
			}
		}
		
		if (resetPasswordMemberIds != null)
		{
			for (int i = 0; i < resetPasswordMemberIds.size(); i++)
			{
				String resetPasswordMemId = resetPasswordMemberIds.get(i);
				if (totalPasswordResetMemberIds != null && totalPasswordResetMemberIds.contains(resetPasswordMemId))
				{
					totalPasswordResetMemberIds.remove(resetPasswordMemId);
				}				
			}
		}
		
		request.getSession().setAttribute("totalWelcomePackBPFMemberIds", totalWelcomePackBPFMemberIds);
		request.getSession().setAttribute("totalWelcomePackBCFMemberIds", totalWelcomePackBCFMemberIds);
		request.getSession().setAttribute("totalWelcomePackSubsMemberIds", totalWelcomePackSubsMemberIds);
		request.getSession().setAttribute("totalPasswordResetMemberIds", totalPasswordResetMemberIds);		

		log.info("Remain number of member BPF welcome pack: " + totalWelcomePackBPFMemberIds.size());
		log.info("Remain number of member BCF welcome pack: " + totalWelcomePackBCFMemberIds.size());
		log.info("Remain number of member Subs welcome pack: " + totalWelcomePackSubsMemberIds.size());
		log.info("Remain number of member reset password: " + totalPasswordResetMemberIds.size());		
	}	
	
	private boolean initializeTemplateContents (HttpServletRequest request)
	{
		if (initializeTemplateFiles())
		{
			/*
			 * Getting the content: Lookup in session variables first. If not found, create a new content
			 */
			log.info("Going to get file contents...");
			
			beforeHeaderContent = FileUtil.getFileContentAsString(beforeHeaderFilename, "UTF-8");
			
			headerContent = FileUtil.getFileContentAsString(headerFilename, "UTF-8");
			
			footerContent = FileUtil.getFileContentAsString(footerFilename, "UTF-8");
			
			afterFooterContent = FileUtil.getFileContentAsString(afterFooterFilename, "UTF-8");
			
			if (signatureXmlDao != null)
			{
				if (!signatureXmlDao.isUseOnlineImage())
				{
					signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
				}
				else
				{
					signatureContent = FileUtil.getFileContentAsString(signatureCmsFilename, "UTF-8");
				}
				log.info("IMAGE STRING: "+signatureContent);
			}
			
			// update page break attribute for letter
//			Boolean isWelcomePackPageBreakMarkedBool = (Boolean)request.getSession().getAttribute("isWelcomePackPageBreakMarked");
//			if (isWelcomePackPageBreakMarkedBool != null)
//			{
//				isWelcomePackPageBreakMarked = isWelcomePackPageBreakMarkedBool.booleanValue();
//			}
//			Boolean isPasswordResetPageBreakMarkedBool = (Boolean)request.getSession().getAttribute("isPasswordResetPageBreakMarked");
//			if (isPasswordResetPageBreakMarkedBool != null)
//			{
//				isPasswordResetPageBreakMarked = isPasswordResetPageBreakMarkedBool.booleanValue();
//			}
			
			// translate content to WordML
			String htmlWelcomePackBodyContent = rawProcessBodyContent(bodyWelcomePackFilename);
			isWelcomePackPageBreakMarked = isPageBreakMarked(htmlWelcomePackBodyContent);
			bodyWelcomePackContent = transformHTML2WordML(htmlWelcomePackBodyContent);
			String wBr = "</w:t></w:r><w:r><w:br/></w:r><w:r><w:t>";
			bodyWelcomePackContent = bodyWelcomePackContent.replaceAll("--LineBreak--", wBr);
			//System.out.println("BODY: "+bodyWelcomePackContent);
			String htmlBodyResetPasswordContent = rawProcessBodyContent(bodyResetPasswordFilename);
			isPasswordResetPageBreakMarked = isPageBreakMarked(htmlBodyResetPasswordContent);
			bodyPasswordResetContent = transformHTML2WordML(htmlBodyResetPasswordContent);
			bodyPasswordResetContent = bodyPasswordResetContent.replaceAll("--LineBreak--", wBr);
//			log.info("beforeHeaderContent: " + beforeHeaderContent);
//			log.info("headerContent: " + headerContent);
//			log.info("footerContent: " + footerContent);
//			log.info("afterFooterContent: " + afterFooterContent);
//			log.info("signatureContent: " + signatureContent);
//			log.info("bodyWelcomePackContent: " + bodyWelcomePackContent);
//			log.info("bodyPasswordResetContent: " + bodyPasswordResetContent);
			// store these contents in session for later use
//			request.getSession().setAttribute("beforeHeaderContent", beforeHeaderContent);
//			request.getSession().setAttribute("headerContent", headerContent);
//			request.getSession().setAttribute("footerContent", footerContent);
//			request.getSession().setAttribute("afterFooterContent", afterFooterContent);
//			request.getSession().setAttribute("signatureContent", signatureContent);
//			request.getSession().setAttribute("bodyPasswordResetContent", bodyPasswordResetContent);
//			request.getSession().setAttribute("bodyWelcomePackContent", bodyWelcomePackContent);
//			request.getSession().setAttribute("isWelcomePackPageBreakMarked", new Boolean(isWelcomePackPageBreakMarked));
//			request.getSession().setAttribute("isPasswordResetPageBreakMarked", new Boolean(isPasswordResetPageBreakMarked));
//			
			if (beforeHeaderContent != null && headerContent != null && footerContent != null && afterFooterContent != null
					&& signatureContent != null && bodyPasswordResetContent != null && bodyWelcomePackContent != null)
			{			
				log.info("Contents retrieved!");				
				return true;
			}
			else
			{
				log.error("Contents are not fully retrieved!");	
			}
		}
		else
		{
			log.error("Getting file templates failed!");
		}
		
		return false;
	}
	
	/**
	 * Initialize template filenames. This needs to be called only once to reduce processing time
	 */
	private boolean initializeTemplateFiles ()
	{		
		// Get the template file names from letter_mapping.xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		XmlReader xmlReader = new XmlReader();
		try
		{
			byte[] letterMappingByteArr = xmlReader.readFile(Environment.LETTER_MAPPING_FILE);
			if (letterMappingByteArr == null)
			{
				return false;
			}
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(letterMappingByteArr);

			document = builder.parse(is);
		}
		catch (Exception ex)
		{
			log.error("Error in parsing XML member: " + ex.toString());
			return false;
		}
		
		NodeList letterNodes = document.getElementsByTagName(Environment.LETTER_TAG_NAME);
		Node passwordLetterNode = null;
		
		// finding the letter node for reset password letter
		for (int i = 0; i < letterNodes.getLength(); i++)
		{
			Node letterNode = letterNodes.item(i);
			NamedNodeMap letterAtts = letterNode.getAttributes();
			Node nameAtt = letterAtts.getNamedItem("name");
			if (nameAtt != null && nameAtt.getNodeValue() != null 
					&& nameAtt.getNodeValue().equals(LETTER_NAME))
			{
				passwordLetterNode = letterNode;
				log.info("Letter mapping found!");
				break;
			}
		}
		
		// get template file name from reset password node
		if (passwordLetterNode != null)
		{
			NodeList letterElements = passwordLetterNode.getChildNodes();
			for (int i = 0; i < letterElements.getLength(); i++)
			{
				Node letterElement = letterElements.item(i);
				if (letterElement != null && letterElement.getNodeType() == Node.ELEMENT_NODE) 
				{
					// if tag is XSLT, get style sheet file name
					if (letterElement.getNodeName().equals(Environment.LETTER_XSLT_TAG_NAME))
					{						
						NodeList letterXSLs = letterElement.getChildNodes();
						for (int j = 0; j < letterXSLs.getLength(); j++)
						{
							Node letterTemplate = letterXSLs.item(j);
							if (letterTemplate.getNodeType() == Node.ELEMENT_NODE &&
									letterTemplate.getNodeName().equals(Environment.LETTER_XSL_HTML2FO_TAG_NAME))
							{
								html2fo_xsl = letterTemplate.getTextContent();
							}
							if (letterTemplate.getNodeType() == Node.ELEMENT_NODE &&
									letterTemplate.getNodeName().equals(Environment.LETTER_XSL_FO2WORD_TAG_NAME))
							{
								fo2word_xsl = letterTemplate.getTextContent();
							}
						}
						
						continue;
					}
					
					// if tag is BODY, get body template file names
					if (letterElement.getNodeName().equals(Environment.LETTER_BODY_TAG_NAME))
					{						
						NodeList letterBodies = letterElement.getChildNodes();
						for (int j = 0; j < letterBodies.getLength(); j++)
						{
							Node letterTemplate = letterBodies.item(j);
							if (letterTemplate != null && letterTemplate.getNodeType() == Node.ELEMENT_NODE)
							{
								// finding the name attribute value
								NamedNodeMap letterTemplateAtts = letterTemplate.getAttributes();
								Node nameAtt = letterTemplateAtts.getNamedItem("name");
								if (nameAtt != null && nameAtt.getNodeValue() != null 
										&& nameAtt.getNodeValue().equals(LETTER_RESET_PASSWORD_NAME))
								{
									bodyResetPasswordFilename = letterTemplate.getTextContent();
								}
								if (nameAtt != null && nameAtt.getNodeValue() != null 
										&& nameAtt.getNodeValue().equals(LETTER_WELCOME_PACK_NAME))
								{
									bodyWelcomePackFilename = letterTemplate.getTextContent();
								}	
							}						
						}
						
						continue;
					}					
					
					// else get letter templates for each part
					NodeList letterTemplates = letterElement.getChildNodes();
					String templateFilename = null;
					for (int j = 0; j < letterTemplates.getLength(); j++)
					{
						Node letterTemplate = letterTemplates.item(j);
						if (letterTemplate.getNodeType() == Node.ELEMENT_NODE &&
								letterTemplate.getNodeName().equals(Environment.LETTER_TEMPLATE_TAG_NAME))
						{
							templateFilename = letterTemplate.getTextContent();
							break;
						}
					}
					
					if (letterElement.getNodeName().equals(Environment.LETTER_BEFOREHEADER_TAG_NAME))
					{
						beforeHeaderFilename = templateFilename;
					}
					else if (letterElement.getNodeName().equals(Environment.LETTER_HEADER_TAG_NAME))
					{
						headerFilename = templateFilename;
					}
					else if (letterElement.getNodeName().equals(Environment.LETTER_FOOTER_TAG_NAME))
					{
						footerFilename = templateFilename;
					}
					else if (letterElement.getNodeName().equals(Environment.LETTER_AFTERFOOTER_TAG_NAME))
					{
						afterFooterFilename = templateFilename;
					}
					else if (letterElement.getNodeName().equals(Environment.LETTER_SIGNATURE_LOCAL_TAG_NAME))
					{
						signatureLocalFilename = templateFilename;
					}
					else if (letterElement.getNodeName().equals(Environment.LETTER_SIGNATURE_CMS_TAG_NAME))
					{
						signatureCmsFilename = templateFilename;
					}					
					else if (Constants.HEADER.LETTER_TABLE_TEMPLATE.equals(letterElement.getNodeName())) {
						table_template = templateFilename;
					}
				}
			}
		}
		
		log.info("beforeHeaderFilename: " + beforeHeaderFilename);
		log.info("headerFilename: " + headerFilename);
		log.info("bodyResetPasswordFilename: " + bodyResetPasswordFilename);
		log.info("bodyWelcomePackFilename: " + bodyWelcomePackFilename);
		log.info("footerFilename: " + footerFilename);	
		log.info("afterFooterFilename: " + afterFooterFilename);	
		log.info("signatureLocalFilename: " + signatureLocalFilename);
		log.info("signatureCmsFilename: " + signatureCmsFilename);
		log.info("html2fo_xsl: " + html2fo_xsl);	
		log.info("fo2word_xsl: " + fo2word_xsl);		
		if (beforeHeaderFilename != null 			&& !beforeHeaderFilename.trim().equals("") &&
				headerFilename != null 				&& !headerFilename.trim().equals("") &&
				bodyResetPasswordFilename != null 	&& !bodyResetPasswordFilename.trim().equals("") &&
				bodyWelcomePackFilename != null 	&& !bodyWelcomePackFilename.trim().equals("") &&
				footerFilename != null 				&& !footerFilename.trim().equals("") &&
				afterFooterFilename != null 		&& !afterFooterFilename.trim().equals("") &&
				signatureLocalFilename != null 		&& !signatureLocalFilename.trim().equals("")	&&
				signatureCmsFilename != null 		&& !signatureCmsFilename.trim().equals("")	&&
				html2fo_xsl != null 				&& !html2fo_xsl.trim().equals("") &&
				fo2word_xsl != null 				&& !fo2word_xsl.trim().equals("")					
			)
		{						
			return true;
		}
		
		return false;
	}
	
	public String transformHTML2WordML (String htmlBodyText)
	{
		if (htmlBodyText == null)
		{
			return null;
		}
		try
		{
			String transformedBodyText = LetterTransformer.getWordContent(
					new ByteArrayInputStream(htmlBodyText.getBytes("UTF-8")),
					FileUtil.getInputStream(html2fo_xsl),
					FileUtil.getInputStream(fo2word_xsl));
			//log.info(transformedBodyText);
			return transformedBodyText;
		}
		catch (UnsupportedEncodingException usee) 
		{
			log.error("Error in getting HTML content as UTF-8: " + usee.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	/**
	 * Due to body templates are stored in OpenCMS as XML file, it is required to raw process the XML to get
	 * content from <content> tags.
	 * @param bodyTemplateFilename
	 * @return
	 */
	private String rawProcessBodyContent (String bodyTemplateFilename)
	{
		StringBuffer resultBuf = new StringBuffer();				
		byte[] contentBytes = FileUtil.getFileContentAsBytes(bodyTemplateFilename);
		
		// Get the template file names from letter_mapping.xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			if (contentBytes == null)
			{
				return null;
			}
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream is = new ByteArrayInputStream(contentBytes);

			document = builder.parse(is);
			
			resultBuf.append("<html><body>");
			
			NodeList contentNodes = document.getElementsByTagName(Environment.LETTER_BODY_CONTENT_TAG_NAME);
			
			// finding the letter node for reset password letter
			for (int i = 0; i < contentNodes.getLength(); i++)
			{
				Node contentNode = contentNodes.item(i);
				// finding CDATA section node
				if (contentNode.getNodeType() == Node.ELEMENT_NODE)
				{
					NodeList textNodes = contentNode.getChildNodes();
					for (int j = 0; j < textNodes.getLength(); j++)
					{
						Node textNode = textNodes.item(j);
						if (textNode.getNodeType() == Node.CDATA_SECTION_NODE)
						{
							resultBuf.append(((CDATASection)textNode).getData());
							break;
						}						
					}
				}
				
			}	
			resultBuf.append("</body></html>");
		}
		catch (Exception ex)
		{
			log.error("Error in parsing XML member: " + ex.toString());
			return null;
		}		

		// replace any special squence of charaters that make the transformation failed.
		String result = resultBuf.toString().replaceAll("&nbsp;", " ");
		result = result.replaceAll("&pound;", "�");
		result = result.replaceAll("&lsquo;", "'");
		result = result.replaceAll("&rsquo;", "'");
		result = result.replaceAll("<br />", "--LineBreak--");
		
		result = result.replaceAll("&bull;", " ");
				
		if(result.contains("your combined pension forecast"))
		{
			log.info("Replace wrong tag");
			result=result.replaceAll("</p></body>", "</body>");
		}
		
		//remove any \n or \t characters
		result = result.replaceAll("[\n]", "");
		result = result.replaceAll("[\t]", "");
	
		log.info("body content: " + result);
		return result;
	}
	
	private boolean isPageBreakMarked (String htmlContent)
	{
		if (htmlContent != null && htmlContent.indexOf(LETTER_PAGE_BREAK_MARKER) >= 0)
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Renders an WordML which is written
	 * directly to the response object's OutputStream
	 */
	public void renderWord(ArrayList<LetterMemberDao> welcomePackMembers, ArrayList<LetterMemberDao> resetPasswordMembers,
			/*HttpServletResponse response)*/HttpServletRequest request) throws ServletException
	{
		try
		{
			//String folder = "D:/";
			StringBuffer wordContentBuffer = new StringBuffer();

			if (welcomePackMembers.size() > 0 || resetPasswordMembers.size() > 0)
			{
				ninoPasswordList = new ArrayList<String>();
				
				String updatedbeforeHeaderContent = beforeHeaderContent;
				// update beforeHeaderContent for numbering list
				if (welcomePackMembers.size() > 0)
				{				
					updatedbeforeHeaderContent = WelcomeLetterGenerator.addNewNumberingList(beforeHeaderContent, 
							welcomePackMembers.size() -1);
				}

				wordContentBuffer.append(updatedbeforeHeaderContent);
				
				int numLetters = welcomePackMembers.size() + resetPasswordMembers.size();
				log.info("Number of letters generated: " + numLetters);
				int letterIndex = 0;
				signatureContent = WelcomeLetterGenerator.processSignature(signatureContent, signatureXmlDao);
				String footer = WelcomeLetterGenerator.processLetterFooter(null, addSignature, signatureContent, signatureXmlDao);
				
				/*
				 * Retrieve all current signatures in the system
				 */
				SignatureXmlDaoUtil signatureDaoUtil = new SignatureXmlDaoUtil(Constants.SIGNATURE.XML_VFS_PATH);
				String additionalInfoTmp = FileUtil.getFileContentAsString(Constants.SIGNATURE.SIGNATURE_PARAGRAPH, "UTF-8");
				log.info("Signature list size: "+signatureDaoUtil.getSignList().size());
				
				/*
				 * Pre-process Header content to include header items which will be displayed
				 */
				headerContent = WelcomeLetterGenerator.preProcessHeader(headerContent);
				/*
				 * get table content from template
				 */
				if (table_template != null) {
					table_template_content = FileUtil.getFileContentAsString(table_template, "UTF-8");
				}
				/*
				 * get paragraph for default signature
				 */
				String addressPrg = FileUtil.getFileContentAsString(Constants.SIGNATURE.DEFAULT_ADDRESS, "UTF-8");
				String usernamePrg = FileUtil.getFileContentAsString(Constants.SIGNATURE.DEFAULT_USERNAME, "UTF-8");
				String namePrg = usernamePrg.replaceAll("TextField", "Username");
				String addrPrg = addressPrg.replaceAll("TextField", "Address");
				// append reset password letters
				//failed list to hold members which could not produce letter for
				List<LetterMemberDao> wpFailedList = new ArrayList<LetterMemberDao>();
				List<LetterMemberDao> psFailedList = new ArrayList<LetterMemberDao>(); 
				for (int i = 0; i < welcomePackMembers.size(); i++) {
					LetterMemberDao welcomePackMember = welcomePackMembers.get(i);
					WelcomeLetterGenerator welcomeLetterGenerator = new WelcomeLetterGenerator(welcomePackMember);
					
					if (welcomePackMember.isDataError()) {
						wpFailedList.add(welcomePackMember);
						ninoPasswordList.add(welcomePackMember.get("Nino") + "-" + welcomeLetterGenerator.getNewPassword());
						continue;
					}
					log.info("Welcome pack letter generation begin for: " + 
							welcomePackMember.get(Environment.MEMBER_BGROUP) + "-" +  welcomePackMember.get(Environment.MEMBER_REFNO));

					String header = welcomeLetterGenerator.processLetterHeader(headerContent);
					String table = welcomeLetterGenerator.processLetterHeader(table_template_content);
										
					//log.info("header: " + header);
					
					//log.info("footer: " + footer);
					String body = welcomeLetterGenerator.processLetterBody(bodyWelcomePackContent);
					
					// modify numbering list id for welcome pack if number of Welcome Pack letters > 1. From
					// the second letter
					if (i > 0)
					{
						body = welcomeLetterGenerator.updateListForMember(body, i + WelcomeLetterGenerator.NUMBERING_LISTDEF_ID_START - 1);
					}
					
					/*
					 * Process table
					 */
					String tablePagrp = WelcomeLetterGenerator.getParagraph(body, "LetterTable");
					while(tablePagrp!=null && !body.equals(WelcomeLetterGenerator.replaceParagraph(body, tablePagrp, table))) {
						body = WelcomeLetterGenerator.replaceParagraph(body, tablePagrp, table);
					}
					/*
					 * Process PageBreak
					 */
					int p=0;
					String pageBreakPagrp = WelcomeLetterGenerator.getParagraph(body, "PageBreak");
					while(pageBreakPagrp!=null && !body.equals(WelcomeLetterGenerator.replaceParagraph(body, pageBreakPagrp, "<w:br w:type=\"page\"/>"))) {
						p++;
						body = WelcomeLetterGenerator.replaceParagraph(body, pageBreakPagrp, "<w:br w:type=\"page\"/>");
					}
					/*
					 * Process Signature
					 */
					String[] signaturePagrp = WelcomeLetterGenerator.getSignatureParagraph(body, "Signature");
					int count = 0;
					while (signaturePagrp != null) {
						log.info("SIGNATURE: "+signaturePagrp[1]);
						SignatureXmlDao signature = signatureDaoUtil.getSignature(signaturePagrp[1]);
						String signatureContent = "";
						if (signature != null) {
							if (signature.isDefault()) {
								//Default signature, generate user and address as parameters
								//Users then update these fields
								signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
								signatureContent = WelcomeLetterGenerator.processSignature(signatureContent, signature);
								signatureContent += namePrg;
								signatureContent += addrPrg;
							} else {
								if (signature.isUseOnlineImage()) {
									signatureContent = FileUtil.getFileContentAsString(signatureCmsFilename, "UTF-8");
									int idx = signatureContent.indexOf("wordml://");
									String first = signatureContent.substring(0, idx+9);
									String last = signatureContent.substring(idx+17);
									signatureContent = first + ++count + last;
									int lastIdx = signatureContent.lastIndexOf("wordml://");
									first = signatureContent.substring(0, lastIdx+9);
									last = signatureContent.substring(lastIdx+17);
									signatureContent = first + count + last;
								} else {
									signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
								}
								/*
								 * Add signature
								 */
								signatureContent = WelcomeLetterGenerator.processSignature(signatureContent, signature);
								if (signature.getName() != null) {
									String name = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getName());
									signatureContent += name;
								}
								if (signature.getTitle() != null && signature.getTitle().trim().length() > 0) {
									String title = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getTitle());
									signatureContent += title;
								}
								if (signature.getTelephone() != null && signature.getTelephone().trim().length() > 0) {
									String telephone = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getTelephone());
									signatureContent += telephone;
								}
								if (signature.getEmail() != null && signature.getEmail().trim().length() > 0) {
									String email = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getEmail());
									signatureContent += email;
								}
								if (signature.getUrl() != null && signature.getUrl().trim().length() > 0) {
									String url = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getUrl());
									signatureContent += url;
								}
							}
							body = WelcomeLetterGenerator.replaceParagraph(body, signaturePagrp[0], signatureContent);
						} else {
							signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
							signatureContent = signatureContent.replace("[[Filename]]", "C:/@.@");
							String na = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", "Signature "+signaturePagrp[1]+" is not availble");
							signatureContent += na;
							body = WelcomeLetterGenerator.replaceParagraph(body, signaturePagrp[0], signatureContent);
						}
						signaturePagrp = WelcomeLetterGenerator.getSignatureParagraph(body, "Signature");
						
					}
					/*
					 * End process signature
					 * Process Header
					 */
					String headerPagrp = WelcomeLetterGenerator.getParagraph(body, "LetterHeader");
					while(headerPagrp!=null && !body.equals(WelcomeLetterGenerator.replaceParagraph(body, headerPagrp, header))) {
						body = WelcomeLetterGenerator.replaceParagraph(body, headerPagrp, header);
					}
					
					//wordContentBuffer.append(header);
					wordContentBuffer.append(body);
					wordContentBuffer.append(footer);

					// append page break if letter is not the last letter
					if (++letterIndex < numLetters){
						wordContentBuffer.append("<w:p><w:r><w:br w:type=\"page\"/></w:r></w:p>");
					}					
					ninoPasswordList.add(welcomePackMember.get("Nino") + "-" + welcomeLetterGenerator.getNewPassword());					
					log.info("Welcom pack letter generation completes");
				}								

				// append reset password letters
				boolean passWorkdTemplateHasBreak = false;
				for (int i = 0; i < resetPasswordMembers.size(); i++) {
					LetterMemberDao resetPasswordMember = resetPasswordMembers.get(i);
					ResetPasswordLetterGenerator resetPasswordLetterGenerator = new ResetPasswordLetterGenerator(resetPasswordMember);
					
					if (resetPasswordMember.isDataError()) {
						psFailedList.add(resetPasswordMember);
						ninoPasswordList.add(resetPasswordMember.get("Nino") + "-" + resetPasswordLetterGenerator.getNewPassword());
						continue;
					}
					log.info("Reset password letter generation begin for: " + 
							resetPasswordMember.get(Environment.MEMBER_BGROUP) + "-" +  resetPasswordMember.get(Environment.MEMBER_REFNO));

					String header = resetPasswordLetterGenerator.processLetterHeader(headerContent);
					//log.info("header: " + header);
					//String footer = resetPasswordLetterGenerator.processLetterFooter(footerContent, addSignature, signatureContent);
					//log.info("footer: " + footer);
					String body = resetPasswordLetterGenerator.processLetterBody(bodyPasswordResetContent);
					
					/*
					 * Process PageBreak
					 */
					String pageBreakPagrp = WelcomeLetterGenerator.getParagraph(body, "PageBreak");
					int p=0;
					while(pageBreakPagrp!=null && !body.equals(WelcomeLetterGenerator.replaceParagraph(body, pageBreakPagrp, "<w:br w:type=\"page\"/>"))) {
						passWorkdTemplateHasBreak = true;
						p++;
						body = WelcomeLetterGenerator.replaceParagraph(body, pageBreakPagrp, "<w:br w:type=\"page\"/>");
					}
					//System.out.println("Break times: "+p);
					/*
					 * Process Signature
					 */
					String[] signaturePagrp = WelcomeLetterGenerator.getSignatureParagraph(body, "Signature");
					int count = 0;
					while (signaturePagrp != null) {
						log.info("SIGNATURE: "+signaturePagrp[1]);
						SignatureXmlDao signature = signatureDaoUtil.getSignature(signaturePagrp[1]);
						String signatureContent = "";
						if (signature != null) {
							if (signature.isDefault()) {
								//Default signature, generate user and address as parameters
								//Users then update these fields
								signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
								signatureContent = WelcomeLetterGenerator.processSignature(signatureContent, signature);
								signatureContent += namePrg;
								signatureContent += addrPrg;
							} else {
								if (signature.isUseOnlineImage()) {
									signatureContent = FileUtil.getFileContentAsString(signatureCmsFilename, "UTF-8");
									int idx = signatureContent.indexOf("wordml://");
									String first = signatureContent.substring(0, idx+9);
									String last = signatureContent.substring(idx+17);
									signatureContent = first + ++count + last;
									int lastIdx = signatureContent.lastIndexOf("wordml://");
									first = signatureContent.substring(0, lastIdx+9);
									last = signatureContent.substring(lastIdx+17);
									signatureContent = first + count + last;
								} else {
									signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
								}
								/*
								 * Add signature
								 */
								signatureContent = WelcomeLetterGenerator.processSignature(signatureContent, signature);
								if (signature.getName() != null) {
									String name = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getName());
									signatureContent += name;
								}
								if (signature.getTitle() != null && signature.getTitle().trim().length() > 0) {
									String title = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getTitle());
									signatureContent += title;
								}
								if (signature.getTelephone() != null && signature.getTelephone().trim().length() > 0) {
									String telephone = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getTelephone());
									signatureContent += telephone;
								}
								if (signature.getEmail() != null && signature.getEmail().trim().length() > 0) {
									String email = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getEmail());
									signatureContent += email;
								}
								if (signature.getUrl() != null && signature.getUrl().trim().length() > 0) {
									String url = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", signature.getUrl());
									signatureContent += url;
								}
							}
							body = WelcomeLetterGenerator.replaceParagraph(body, signaturePagrp[0], signatureContent);
						} else {
							signatureContent = FileUtil.getFileContentAsString(signatureLocalFilename, "UTF-8");
							signatureContent = signatureContent.replace("[[Filename]]", "C:/@.@");
							String na = WelcomeLetterGenerator.updateLetterParagraph(additionalInfoTmp, "Name", "Signature "+signaturePagrp[1]+" is not availble");
							signatureContent += na;
							body = WelcomeLetterGenerator.replaceParagraph(body, signaturePagrp[0], signatureContent);
						}
						signaturePagrp = WelcomeLetterGenerator.getSignatureParagraph(body, "Signature");
					}
					/*
					 * End process signature
					 */
					
					String headerPagrp = WelcomeLetterGenerator.getParagraph(body, "LetterHeader");
					while(headerPagrp!=null && !body.equals(WelcomeLetterGenerator.replaceParagraph(body, headerPagrp, header))) {
						body = WelcomeLetterGenerator.replaceParagraph(body, headerPagrp, header);
					}
					//wordContentBuffer.append(header);
					wordContentBuffer.append(body);
					wordContentBuffer.append(footer);
					
					// append page break if letter is not the last letter
					letterIndex += 1;
					if (letterIndex < numLetters)
					{
						wordContentBuffer.append("<w:p><w:r><w:br w:type=\"page\"/></w:r></w:p>");
					}
										
					ninoPasswordList.add(resetPasswordMember.get("Nino") + "-" + resetPasswordLetterGenerator.getNewPassword());
					log.info("Reset password letter generation completes");
				}
				
				/**
				 * Start producing summary page
				 */
				String summaryPage = FileUtil.getFileContentAsString(Constants.SIGNATURE.SUMMARY_PAGE, "UTF-8");
				summaryPage = summaryPage.replaceAll("TotalWelcomePack", ""+welcomePackMembers.size()).
							              replaceAll("SuccessWelcomePack", ""+(welcomePackMembers.size()-wpFailedList.size())).
							              replaceAll("FailureWelcomePack", ""+wpFailedList.size()).
							              replaceAll("TotalPasswordReset", ""+resetPasswordMembers.size()).
							              replaceAll("SuccessPasswordReset", ""+(resetPasswordMembers.size()-psFailedList.size())).
							              replaceAll("FailurePasswordReset", ""+psFailedList.size());
				
				String errorDetails = "<w:p><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"1\"/>" +
									  "</w:listPr></w:pPr><w:r><w:t>NINO</w:t></w:r>" +
									  "<w:r><w:t> (TYPE): ERROR</w:t></w:r>" +
									  "<w:r><w:t> </w:t></w:r></w:p>";
				//log.info("Summary page: "+summaryPage);
				if (!passWorkdTemplateHasBreak) {
					log.info("Password Template does not page break, then insert");
					wordContentBuffer.append("<w:p><w:r><w:br w:type=\"page\"/></w:r></w:p>");
				}
				wordContentBuffer.append(summaryPage);
				for (int c=0; c<wpFailedList.size(); c++) {
					LetterMemberDao obj = wpFailedList.get(c);
					String errorLine = errorDetails.replaceAll("NINO", obj.getMemberData("Nino")).
													replaceAll("TYPE", "WelcomePack").
													replaceAll("ERROR", obj.getErrorDescription());
					wordContentBuffer.append(errorLine);
				}
				for (int c=0; c<psFailedList.size(); c++) {
					LetterMemberDao obj = psFailedList.get(c);
					String errorLine = errorDetails.replaceAll("NINO", obj.getMemberData("Nino")).
													replaceAll("TYPE", "PasswordReset").
													replaceAll("ERROR", obj.getErrorDescription());
					wordContentBuffer.append(errorLine);
				}
				/**
				 * End producing summary page
				 */
				wordContentBuffer.append(afterFooterContent);
			}

			byte[] content = wordContentBuffer.toString().getBytes("UTF-8");
			request.getSession().removeAttribute("letterContent");
			request.getSession().setAttribute("letterContent", content);			
			
//			log.info("Writing to file. Please wait...");
//			FileOutputStream outputFile = new FileOutputStream("D:/letter_password.xml");
//			outputFile.write(content);
//			outputFile.flush();
//			outputFile.close();	
//			log.info("Writing to file complete!");			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new ServletException(ex);
		}
	}
	
	private boolean isPasswordLetterInitRequest(HttpServletRequest request)
	{
		if (numBPFUnregMembers == 0 && numBCFUnregMembers == 0 && numSubsUnregMembers == 0 && numPensionResetMembers == 0)
		{
			/*
			 * Initialize members from database
			 */
			totalWelcomePackBPFMemberIds = MemberLoader.getTotalWelcomePackMemberIdsByGroup("BPF");
			totalWelcomePackBCFMemberIds = MemberLoader.getTotalWelcomePackMemberIdsByGroup("BCF");
			//Commented out on July 07 2009, only members in BPF,BCF use letters
			//totalWelcomePackSubsMemberIds = MemberLoader.getTotalWelcomePackMemberIdsByGroup("SPF");
			totalPasswordResetMemberIds = MemberLoader.getTotalPasswordResetMemberIds();
		
			// store these list in the session for later use
			request.getSession().setAttribute("totalWelcomePackBPFMemberIds", totalWelcomePackBPFMemberIds);
			request.getSession().setAttribute("totalWelcomePackBCFMemberIds", totalWelcomePackBCFMemberIds);
			request.getSession().setAttribute("totalWelcomePackSubsMemberIds", totalWelcomePackSubsMemberIds);
			request.getSession().setAttribute("totalPasswordResetMemberIds", totalPasswordResetMemberIds);
			
			return true;
		}
		
		return false;
	}
	
	private boolean parseRequestXml(String xml)
	{
		//log.info("xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			NodeList list = root.getElementsByTagName(Constants.LETTER_PASSWORD);
			//log.info("list length: " + list.getLength());
			if (list.getLength() > 0)
			{
				Node passwordNode = list.item(0);
				NodeList paramNodes = passwordNode.getChildNodes();
				int nlength = paramNodes.getLength();
				//log.info("nlength: " + nlength);
				for (int i = 0; i < nlength; i++)
				{
					Node paramNode = paramNodes.item(i);
					if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
					{
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_NUM_BP_UNREG))
						{
							//log.info(" getting numBPFUnregMembers: " + paramNode.getTextContent());
							numBPFUnregMembers = Integer.parseInt(paramNode.getTextContent());
						}
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_NUM_BC_UNREG))
						{
							//log.info(" getting numBCFUnregMembers: " + paramNode.getTextContent());
							numBCFUnregMembers = Integer.parseInt(paramNode.getTextContent());
						}
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_NUM_SUBS_UNREG))
						{
							//log.info(" getting numSubsUnregMembers: " + paramNode.getTextContent());
							numSubsUnregMembers = Integer.parseInt(paramNode.getTextContent());
						}
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_NUM_PENSION_RESET))
						{
							//log.info(" getting numPensionResetMembers: " + paramNode.getTextContent());
							numPensionResetMembers = Integer.parseInt(paramNode.getTextContent());
						}
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_ADD_SIGNATURE))
						{
							//log.info(" getting numPensionResetMembers: " + paramNode.getTextContent());
							addSignature = (paramNode.getTextContent() != null && paramNode.getTextContent().equalsIgnoreCase("true"));
						}
						if (paramNode.getNodeName().equals(Constants.LETTER_PASSWORD_COMMIT_CHANGES))
						{
							//log.info(" getting numPensionResetMembers: " + paramNode.getTextContent());
							commitChanges = (paramNode.getTextContent() != null && paramNode.getTextContent().equalsIgnoreCase("true"));
						}						
					}
				}
			}

			bais.close();
			log.info("numBPFUnregMembers: " + numBPFUnregMembers);
			log.info("numBCFUnregMembers: " + numBCFUnregMembers);
			log.info("numSubsUnregMembers: " + numSubsUnregMembers);
			log.info("numPensionResetMembers: " + numPensionResetMembers);
			log.info("addSignature: " + addSignature);
			log.info("commitChanges: " + commitChanges);
			
			return true;

		}
		catch (Exception ex)
		{
			numBPFUnregMembers = 0;
			numBCFUnregMembers = 0;
			numSubsUnregMembers = 0;
			numPensionResetMembers = 0;
		}
		

		return false;

	}
	
	public String buildPasswordResponseXML(int totalBPFUnreg, int totalBCFUnreg, 
			int totalSubsUnreg, int totalPensionReset, boolean commitChanges) 
	{

		// valueItem = valueItem.trim();

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constants.AJAX_LETTER_RESPONSE).append(">\n");
		buffer.append("\t<").append(Constants.LETTER_PASSWORD).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_TOTAL_BP_UNREG).append(">")
			.append(totalBPFUnreg).append("</").append(Constants.LETTER_PASSWORD_TOTAL_BP_UNREG).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_TOTAL_BC_UNREG).append(">")
			.append(totalBCFUnreg).append("</").append(Constants.LETTER_PASSWORD_TOTAL_BC_UNREG).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_TOTAL_SUBS_UNREG).append(">")
			.append(totalSubsUnreg).append("</").append(Constants.LETTER_PASSWORD_TOTAL_SUBS_UNREG).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_TOTAL_PENSION_RESET).append(">")
			.append(totalPensionReset).append("</").append(Constants.LETTER_PASSWORD_TOTAL_PENSION_RESET).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_COMMIT_CHANGES).append(">")
		.append(commitChanges).append("</").append(Constants.LETTER_PASSWORD_COMMIT_CHANGES).append(">\n");		
				
		buffer.append("\t</").append(Constants.LETTER_PASSWORD).append(">\n");
		buffer.append("</").append(Constants.AJAX_LETTER_RESPONSE).append(">");
		
		return buffer.toString();
	}	
	
	public String buildPasswordResponseXMLError() 
	{
		// valueItem = valueItem.trim();

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constants.AJAX_LETTER_RESPONSE).append(">\n");
		buffer.append("\t<").append(Constants.LETTER_PASSWORD).append(">\n");
		
		buffer.append("\t\t<").append(Constants.LETTER_PASSWORD_ERROR).append(">")
			.append(true).append("</").append(Constants.LETTER_PASSWORD_ERROR).append(">\n");	
				
		buffer.append("\t</").append(Constants.LETTER_PASSWORD).append(">\n");
		buffer.append("</").append(Constants.AJAX_LETTER_RESPONSE).append(">");
		
		return buffer.toString();
	}	
	
	/**
	 * Clear all session stored variables. This method is called when user log out
	 * @param request
	 */
	public static void clearAllPasswordSessionData(HttpServletRequest request)
	{
		// Template content
		request.getSession().setAttribute("beforeHeaderContent", null);
		request.getSession().setAttribute("headerContent", null);
		request.getSession().setAttribute("footerContent", null);
		request.getSession().setAttribute("afterFooterContent", null);
		request.getSession().setAttribute("signatureContent", null);
		request.getSession().setAttribute("bodyPasswordResetContent", null);
		request.getSession().setAttribute("bodyWelcomePackContent", null);
		request.getSession().setAttribute("isWelcomePackPageBreakMarked", null);
		request.getSession().setAttribute("isPasswordResetPageBreakMarked", null);		
		
		// Total member Ids
		request.getSession().setAttribute("totalWelcomePackBPFMemberIds", null);
		request.getSession().setAttribute("totalWelcomePackBCFMemberIds", null);
		request.getSession().setAttribute("totalWelcomePackSubsMemberIds", null);
		request.getSession().setAttribute("totalPasswordResetMemberIds", null);		
		
		request.getSession().setAttribute("letterContent", null);
		
		log.info("Password letter: All data in session cleared!");
	}
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	public String getServletInfo()
	{
		return "Short description";
	}	
	
	public void renderWordTest (HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			String folder = "D:/";		
			log.info("opening file. Please wait...");
			File file = new File(folder + "reset_password.xml");
			FileInputStream inFile = new FileInputStream(file);
			byte[] content = new byte[(int)file.length()];
			
			inFile.read(content);
			inFile.close();
			
//			response.setContentType("application/msword");
//			response.getOutputStream().write(content);
//			response.getOutputStream().flush();
//			response.getOutputStream().close();
			
			request.getSession().setAttribute("letterContent", content);							
			request.getSession().setAttribute("membersExisted", "true");
			log.info("flushing file completes!");
		}
		catch (IOException e)
		{
			log.error("There was an error in reading file: " + e.toString());
		}			
	}
}
