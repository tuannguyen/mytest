 package com.bp.pensionline.modeller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.calc.producer.BpAcCrCalcProducer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.util.TextUtil;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 16/05/2007
 * 
 *
 */
public class ActiveRetirement extends HttpServlet {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
		/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * Handler GET method
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * Handler POST method
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @param request
	 * @param response
	 * Handler client request
	 */
	protected void doProcess(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType(Constant.CONTENT_TYPE);
		
		System.out.println("Enter ActiveRetirement doProcceess");
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		MemberDao memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
		String xmlReq = request.getParameter(Constant.PARAM);
		HashMap valueMap = new HashMap();
		String xmlRes=null;

		try {
			prepareData(xmlReq, valueMap);
			
			PrintWriter out = response.getWriter();
			int Npa = NumberUtil.getInt(String.valueOf(valueMap.get("RetirementAge")));
			Date dob = DateUtil.getDate(memberDao.get("Dob"));
			Date DoR = DateUtil.addYears(dob, Npa);
			//convert to sql date
			java.sql.Date dorsqlDate = new java.sql.Date(DoR.getTime());
			int cbtOption = NumberUtil.getInt(String.valueOf(valueMap.get("ContributoryOption")));
			double cash = NumberUtil.getDouble(String.valueOf(valueMap.get("CashLumpSum")));
			
			// call the producer to trigger the Calculation
			BpAcCrCalcProducer producer = new BpAcCrCalcProducer();
			MemberDao memberTemp=producer.runCalc(memberDao, dorsqlDate, cbtOption, cash);
			
				
			
			/*int _mMinage=getMinAge(request, response);
			int _mMaxCash=getMaxCash(request, response);*/
			String []_tagName={"Npa","CINN91",MemberDao.NpaCashLumpSum,MemberDao.NpaPensionWithChosenCash,
					MemberDao.NpaSpousesPension, MemberDao.overfundIndicator, MemberDao.veraIndicator, 
					MemberDao.MaximumCashLumpSum, MemberDao.PensionWithMaximumCash, MemberDao.MaximumCashLumpSumExact,
					MemberDao.NpaCashLumpSumCurrency, MemberDao.ReducedPension };
			
			//String _tagValue[]={String.valueOf(Npa),String.valueOf(cbtOption),
			//		TextUtil.substituteCurrency("�100"),
			//		TextUtil.substituteCurrency("�100"),
			//		TextUtil.substituteCurrency("�100")	
			//};
			
			String _tagValue[]={String.valueOf(Npa),String.valueOf(cbtOption),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.CashLumpSum)),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.PensionWithChosenCash)),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.SpousesPension)),
					memberTemp.get(MemberDao.overfundIndicator),
					memberTemp.get(MemberDao.veraIndicator),
					memberTemp.get(MemberDao.MaximumCashLumpSum),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.PensionWithMaximumCash)),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.MaximumCashLumpSumExact)),
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.CashLumpSumCurrency)), 
					TextUtil.substituteCurrency(memberTemp.get(MemberDao.ReducedPension)) };
			
			
			debugMemberDao(memberTemp);
			xmlRes=generateXmlValue(_tagName, _tagValue);
			 //xmlRes=getXMLData("Modeller","Modeller","Modeller");
			 
			out.print(xmlRes);
			//System.out.println(xmlRes);
			out.close();

		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.modeller.ActiveRetirement.doProcess", e);
			
			xmlRes=getXMLDataError("Modeller","Modeller");
			try {
				PrintWriter out = response.getWriter();
			
				out.print(xmlRes);
				
				out.close();
			} catch (Exception ex) {
				// TODO: handle exception
				LOG.error("com.bp.pensionline.modeller.ActiveRetirement.doProcess", ex);
			
			}
			
		}

	}

	/**
	 * @param param
	 * @param valueMap
	 * @return data parameter as xml
	 * @throws Exception
	 */
	protected boolean prepareData(String param, HashMap valueMap)
			throws Exception {
		boolean isTrue = false;
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();

			ByteArrayInputStream byteArr = new ByteArrayInputStream(param
					.getBytes());

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(byteArr);

			doc.getDocumentElement().normalize();

			NodeList listOfMembers = doc.getElementsByTagName("Modeller");

			for (int s = 0; s < listOfMembers.getLength(); s++) {

				Node firstNode = listOfMembers.item(s);
				if (firstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element firstElement = (Element) firstNode;
					valueMap.put("RetirementAge", getNodeValue(firstElement,
							"RetirementAge"));
					valueMap.put("ContributoryOption", getNodeValue(
							firstElement, "ContributoryOption"));

					valueMap.put("CashLumpSum", getNodeValue(firstElement,
							"CashLumpSum"));

				}
			}
			isTrue = true;
		} catch (Exception e) {
			// TODO: handle exception
			isTrue = false;
			LOG.error("com.bp.pensionline.modeller.ActiveRetirement.prepareData", e);
			throw e;

		}
		return isTrue;

	}

	protected String getNodeValue(Element element, String tagName)
			throws Exception {
		NodeList nodeList = element.getElementsByTagName(tagName);
		Element tagNameElement = (Element) nodeList.item(0);
		NodeList textList = tagNameElement.getChildNodes();

		if (((Node) textList.item(0)) != null) {

			return ((Node) textList.item(0)).getNodeValue().trim();
		} else {
			return new String("null");
		}

	}
	/**
	 * @param memberDao
	 * @return xml response from memberDao 
	 */
	private String getXMLData(String tagName,String valueItem,String updateField){
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(
				">").append(tagName).append("</").append(
				Constant.REQUESTED_TAG).append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(
				">").append(updateField).append("</").append(
				Constant.UPDATE_FIELD).append(">\n");
		buffer.append("         <").append(Constant.TYPE).append(">\n");
		buffer.append("             <").append("Text").append("/>\n");
		buffer.append("         </").append(Constant.TYPE).append(">\n");
		buffer.append("         <").append(Constant.VALUE).append(">")
				.append(valueItem).append("</").append(Constant.VALUE)
				.append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE)
				.append(">\n");

		return buffer.toString();
		
		
	}
	/**
	 * @return xml response error
	 */
	private String getXMLDataError(String tagName,String updateField){
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append(Constant.DATA).append(">\n");
		buffer.append("         <").append(Constant.REQUESTED_TAG).append(">")
				.append(tagName).append("</").append(Constant.REQUESTED_TAG)
				.append(">\n");
		buffer.append("         <").append(Constant.UPDATE_FIELD).append(">")
				.append(updateField).append("</").append(Constant.UPDATE_FIELD)
				.append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE).append(
				">[[" + tagName + "]]</").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append(Constant.DATA).append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();
		
	}
	

	
	/**
	 * @param tagName
	 * @param tagValue
	 * @return String value as Xml. tagName[] and tagValue[] have the same size
	 *         (length)
	 */
	private String generateXmlValue(String tagName[], String[] tagValue) {
		StringBuffer buf = new StringBuffer();
		buf.append("<AjaxResponseXml>");
		int tagnameLength = tagName.length;
		int tagvalueLength = tagName.length;
		if (tagnameLength == tagvalueLength) {
			for (int i = 0; i < tagnameLength; i++) {
				buf.append("<").append(tagName[i]).append(">");
				 if (tagValue[i]!=null && tagValue[i].length()>0){
					 buf.append(tagValue[i]);
				 }else {
					 buf.append(StringUtil.EMPTY_STRING);
				 }
				buf.append("</").append(tagName[i])
						.append(">");

			}

		} else {
			buf.append("<Error>True</Error>");
		}
		buf.append("</AjaxResponseXml>");
		return buf.toString();

	}
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			//StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			//xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				//xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
//			xmlString.append("</MemberData>\n");
//			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
//					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}	
	
	public void exportToFile(String fileName, String content)
	{
		try
		{
			File xmlFile = new File(fileName);
			FileOutputStream xmlOutputStream = new FileOutputStream(xmlFile);
			xmlOutputStream.write(content.getBytes());
			xmlOutputStream.flush();
			xmlOutputStream.close();
		}
		catch (Exception e)
		{
			LOG.info(this.getClass().toString()+ ". Error in exporting file to " + fileName + ": " + e.getMessage());
		}
	}	

}
