package com.bp.pensionline.modeller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.calc.producer.BpAcCrCalcProducer;
import com.bp.pensionline.test.Constant;

public class ACCRCalcAdjustUpdater extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private static final long serialVersionUID = 1L;		
	

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType("text/xml");
		PrintWriter out = response.getWriter();
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;

		boolean isAdjustUsed = BpAcCrCalcProducer.isCalcAdjustUsed();
		LOG.info("CR Calculation Adjustment: " + isAdjustUsed);
		
		xmlResponse = buildXmlResponse(isAdjustUsed);
		
		out.print(xmlResponse);
		out.close();

	}


	public String buildXmlResponse(boolean isAdjustUsed) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		buffer.append("     <Adjust>").append(isAdjustUsed).append("</Adjust>").append("\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
		

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

}
