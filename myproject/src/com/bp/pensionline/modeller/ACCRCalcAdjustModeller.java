package com.bp.pensionline.modeller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.calc.producer.BpAcCrCalcProducer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.StringUtil;
public class ACCRCalcAdjustModeller extends HttpServlet 
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String AJAX_CALCADJUST_UNITTEST_RESPONSE = "AjaxCalcAdjustResponse";
	public static final String AJAX_CALCADJUST_BGROUP = "BGroup";
	public static final String AJAX_CALCADJUST_REFNO = "RefNo";
	public static final String AJAX_CALCADJUST_USED = "cal_adjustUsed";
	public static final String AJAX_CALCADJUST_NEW_CONTRIBUTION = "cal_accRate";
	public static final String AJAX_CALCADJUST_RETIRE_AGE = "cal_nra";
	public static final String AJAX_CALCADJUST_CASHLUMPSUM = "cal_cash";
	
	private static final long serialVersionUID = 1L;		
	
	private String bGroup = null;
	private String refNo = null;
	private boolean adjust = false;
	
	private int cinn91 = -1;
	private double lta = 0;
	private Date dob = null;
	private int retirementAge = 60;	// default is 60
	private int newContOption = 0;
	private double cash = 0.0;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;		
		MemberDao memberTemp = null;
		
		try
		{
			if (parseRequestXml(xml) && bGroup != null && refNo != null)
			{
				// get CNN91 and DoR from bGroup and refNo
				if (updateInputData(bGroup, refNo))
				{
				
					LOG.info("cinn91: " + cinn91);
					LOG.info("dob: " + dob);
					LOG.info("lta: " + lta);
					
					if (cinn91 > -1)
					{
						if (newContOption <= 0)
						{
							newContOption = cinn91;
						}
	
						Date DoR = DateUtil.addYears(dob, retirementAge);
						
						//convert to sql date
						java.sql.Date dorsqlDate = new java.sql.Date(DoR.getTime());
						
						// call the producer to trigger the Calculation
						BpAcCrCalcProducer producer = new BpAcCrCalcProducer();
						memberTemp = producer.runAdjustCalcUnitTest(bGroup, refNo, cinn91,  
								dorsqlDate, lta, null, newContOption, cash, adjust);
					}
				}
			}
			else
			{
				LOG.error("Error in parsing request XML");
			}
		}
		catch (Exception e)
		{
			LOG.error("Error calculating Unit test: " + e);
		}

		if (memberTemp != null)
		{
			// populate other somes attributes
			memberTemp.set("calc_date", new SimpleDateFormat("dd MMM yyyy").format(new Date()));
			
			memberTemp.set("CINN91", new Integer (cinn91).toString());
			memberTemp.set("cash", StringUtil.getString(NumberUtil.to2Dp(cash)));
			
			memberTemp.set("acc_date", new SimpleDateFormat("dd MMM yyyy").format(DateUtil.getFirstDayOfNextMonth()));
			memberTemp.set("acc_rate", new Integer (newContOption).toString());
			
			memberTemp.set("lta", StringUtil.getString(NumberUtil.to2Dp(lta)));
			
			xmlResponse = buildXmlResponse(memberTemp);
		}
		else
		{
			xmlResponse = buildXmlResponseError("Unknown error while calculating member pension!");
		}
		
		
		out.print(xmlResponse);
		out.close();

	}
	
	/**
	 * Get current contribution option of an active member
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	private boolean updateInputData (String bGroup, String refNo)
	{
		String getCINN91Query =  "select b.BD19A as \"MembershipStatusRaw\", " +
				"trim(leading '0' from initcap(to_char(b.bd11d, 'DD MON YYYY'))) as \"Dob\", " +
				"cd.CA10T as \"CINN91\" " +
				"from basic b, CATEGORY_DETAIL cd " +
				"where b.bgroup= ? and b.refno= ? and cd.bgroup= b.bgroup and cd.ca26x= b.ca26x";
		
		String getLTAQuery = "select trim(to_char(rf.REVFAC, '999999999.99')) as \"Lta\"" +
				" from (" +
				"	select to_number(to_char(sysdate, 'YYYY'),'9999') year from dual " +
				"	where to_char(sysdate, 'MMDD') >= to_number('0406', '9999') " +
				"	union select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year from dual" +
				" 	where to_char(sysdate, 'MMDD') < to_number('0406', '9999')" +
				") " +
				"taxyear ,revfac rf " +
				" where rf.FACTYPE = 'LTAA' and rf.YOE = taxyear.year";
		
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;	
		LOG.info("rs sqlQuery: " + getCINN91Query);

		try
		{
			con = connector.getDBConnFactory(Environment.AQUILA);
			PreparedStatement pstm = con.prepareStatement(getCINN91Query);	
			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next())
			{
				String memberStatus = rs.getString("MembershipStatusRaw");
				
				if (memberStatus != null && memberStatus.equals("AC"))
				{
					String cinn91Str = rs.getString("CINN91");
					if (cinn91Str != null)
					{
						try
						{
							this.cinn91 = Integer.parseInt(cinn91Str);
						}
						catch (NumberFormatException nfe)
						{
							LOG.error("CINN91 is not in valid format: " + nfe);
							return false;
						}
					}
					
					String dobStr = rs.getString("Dob");
					if (dobStr != null)
					{
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
						try
						{
							this.dob = dateFormat.parse(dobStr);
						}
						catch (ParseException pe)
						{
							LOG.error("Date of birth is not in valid: " + pe);
							return false;
						}
						
					}
					
				}
			}			
			
			rs.close();
			pstm.close();	
			
			pstm = con.prepareStatement(getLTAQuery);	
			
			rs = pstm.executeQuery();
			
			if (rs.next())
			{
				String ltaStr = rs.getString("Lta");	
				if (ltaStr != null)
				{
					try
					{
						this.lta = Double.parseDouble(ltaStr);
					}
					catch (NumberFormatException nfe)
					{
						LOG.error("lta is not in valid format: " + nfe);
						return false;
					}
				}				
			}			
			
			rs.close();
			pstm.close();			
		}
		catch (SQLException e)
		{
			LOG.error("Error in get members' CINN91: " + e.toString());
			return false;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector.getInstance().close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing connection: " + e.toString());
					return false;
				}
			}
		}
		
		return true;
	}

	public String buildXmlResponse(MemberDao member) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(AJAX_CALCADJUST_UNITTEST_RESPONSE).append(">");
		
		Map<String, String> map = member.getValueMap();		
		Set<String> keys = map.keySet();
		Iterator<String> keyIterator = keys.iterator();
		while (keyIterator.hasNext())
		{
			String key = keyIterator.next();
			String value = map.get(key);
			
			buffer.append("<").append(key).append(">");
			buffer.append(value);
			buffer.append("</").append(key).append(">");
			
		}
//		buffer.append("<").append("calc_date").append(">");
//		buffer.append(new SimpleDateFormat("dd MMM yyyy").format(new Date()));
//		buffer.append("</").append("calc_date").append(">");		
		buffer.append("</").append(AJAX_CALCADJUST_UNITTEST_RESPONSE).append(">");
		
		//LOG.info("Response XML: " + buffer.toString());
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(AJAX_CALCADJUST_UNITTEST_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(AJAX_CALCADJUST_UNITTEST_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		LOG.info("parseRequestXml xml: " + xml);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_BGROUP))
					{
						bGroup = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_REFNO))
					{
						refNo = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_USED))
					{
						String adjustStr = paramNode.getTextContent();
						LOG.info("adjustStr: " + adjustStr);
						if (adjustStr != null && adjustStr.equalsIgnoreCase("True"))
						{
							adjust = true;
						}
						else
						{
							adjust = false;
						}
					}
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_RETIRE_AGE))
					{
						String retireAgeStr = paramNode.getTextContent();
						if (retireAgeStr != null && retireAgeStr.trim().length() > 0)
						{
							retirementAge = Integer.parseInt(retireAgeStr);
						}
					}
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_NEW_CONTRIBUTION))
					{
						String newContOptionStr = paramNode.getTextContent();
						if (newContOptionStr != null && newContOptionStr.trim().length() > 0)
						{
							newContOption = Integer.parseInt(newContOptionStr);
						}
					}	
					if (paramNode.getNodeName().equals(AJAX_CALCADJUST_CASHLUMPSUM))
					{
						String cashStr = paramNode.getTextContent();
						if (cashStr != null && cashStr.trim().length() > 0)
						{
							cash = Double.parseDouble(cashStr);
						}
					}					
				}
			}
			
			LOG.info("bGroup: " + bGroup);
			LOG.info("refNo: " + refNo);
			LOG.info("adjust: " + adjust);
			LOG.info("retirementAge: " + retirementAge);
			LOG.info("newContOption: " + newContOption);
			LOG.info("cash: " + cash);
			
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}

}
