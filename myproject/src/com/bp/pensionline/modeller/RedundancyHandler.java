/**
 * 
 */
package com.bp.pensionline.modeller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.calc.producer.BpAcRdCalcProducer;
import com.bp.pensionline.calc.producer.BpAcRrCalcProducer;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.NumberUtil;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.util.TextUtil;

/**
 * @author Duy Thao Nguyen
 * @since 25/05/2007
 * @version 1.0
 * 
 */
public class RedundancyHandler extends HttpServlet {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public RedundancyHandler() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProccess(request, response);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProccess(request, response);

	}
	

	/**
	 * @param request
	 * @param response
	 * Hanlder client request
	 */
	private void doProccess(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType(Constant.CONTENT_TYPE);
		CmsUser currentUser = SystemAccount.getCurrentUser(request);
		MemberDao memberDao = (MemberDao) currentUser.getAdditionalInfo(Environment.MEMBER_KEY);
		
		
		String xmlReq = request.getParameter(Constant.PARAM);
		HashMap valueMap = new HashMap();
		String xmlRes = null;
		MemberDao member=null;
		boolean callRR =false;
		try {

			prepareData(xmlReq, valueMap);
			java.sql.Date rDate = DateUtil.getDate(String.valueOf(valueMap.get(MemberDao.RedundancyDate)));
			
			// Due to legal [legislation] requirements by 6 April 2010 no RR figures shown for retirement ages below 55. 
			/*
			Currently we use "age" on the date of calculation to determine whether to show the pension results (if we run the RR or the RD calc). We would update this to include a check on the legislation change date:

				a. If Date of calc is after 05/04/2010 AND age at DoC is > 55 then show RR pension results
				b. If Date of calc is before 06/04/2010 use the exisiting logic (and confirm what this is) 
			*/
			Date effectiveDate = getConfiguredRedundancyDateChangeEffective();
			if (effectiveDate == null)
			{
				// if unable to get configured date, set it value to  06/04/2010
				effectiveDate = new SimpleDateFormat("dd/MM/yyyy").parse("06/04/2010");
			}
			LOG.info("Redundancy date change effective: " + effectiveDate);
			
			// get age of member at calculation date
			java.util.Date dateOfBirdth = DateUtil.getNormalDate(memberDao.get("Dob"));
			java.util.Date calculationDate = new Date(rDate.getTime());			
			int age = DateUtil.getYearsBetween(dateOfBirdth, rDate);
			
			if (DateUtil.compareDates(calculationDate, effectiveDate) >= 0)
			{
				LOG.info("Use new logic for Redundancy Planner for member: " + age);
				int minAge = 55;
				if (age >= minAge)
				{
					// will call BP RR Producer					
					BpAcRrCalcProducer producer = new BpAcRrCalcProducer();
					member = producer.runCalc(memberDao, rDate);
					callRR=true;					
				}
				else
				{
					// will call BP RD Producer								
					BpAcRdCalcProducer producer = new BpAcRdCalcProducer();
					member = producer.runCalc(memberDao, rDate);
					callRR = false;					
				}				
			}
			else	// use existing logic
			{
				LOG.info("Use existing logic for Redundancy Planner for member: " + age);
				int minAge=50;
				String strDJS = memberDao.get("DateJoinedScheme");
				java.util.Date tempDate = DateUtil.getNormalDate(strDJS);
				java.util.Date date2Compare = DateUtil.getNormalDate("06 Apr 2006");
				if (DateUtil.compareDates(tempDate,date2Compare)>=0)
				{// member join after 06 Apr 2006
					minAge = 55;
	                         			
				}else
				{
					minAge = 50;	                
				}
	
				//compare TodayDate and the date passed in from AJAX, return 0 if they equal
				//int todayBoolean = DateUtil.compareDates(todayDate, d);
				//System.out.print("Check boolean and RdDate from Dao"+todayBoolean+ memberDao.get(MemberDao.RedundancyDate));

				if (age >= minAge)
				{
					// will call BP RR Producer					
					BpAcRrCalcProducer producer = new BpAcRrCalcProducer();
					member = producer.runCalc(memberDao, rDate);
					callRR=true;					
				}
				else
				{
					// will call BP RD Producer								
					BpAcRdCalcProducer producer = new BpAcRdCalcProducer();
					member = producer.runCalc(memberDao, rDate);
					callRR=false;					
				}
			}
			
			if (memberDao.get(MemberDao.RedundancyDate)!=null){// user use modeller
				
				String[] _tagName = { MemberDao.RedundancyDate ,MemberDao.AccruedPension, MemberDao.EgpCash,
						MemberDao.SrpCash, MemberDao.TaxFreeCash,
						MemberDao.TaxableCash, MemberDao.TaxPayable,
						MemberDao.RrReducedPension, MemberDao.RrSpousesPension,
						MemberDao.RrMaxLumpSum, MemberDao.RrResidualPension,"CalcRR"};

				String[] _tagValue = {StringUtil.getString(valueMap.get(MemberDao.RedundancyDate)),
						TextUtil.substituteCurrency(member.get(MemberDao.AccruedPension)),
						TextUtil.substituteCurrency(member.get(MemberDao.EgpCash)),
						TextUtil.substituteCurrency(member.get(MemberDao.SrpCash)),
						TextUtil.substituteCurrency(member.get(MemberDao.TaxFreeCash)),
						TextUtil.substituteCurrency(member.get(MemberDao.TaxableCash)),
						TextUtil.substituteCurrency(member.get(MemberDao.TaxPayable)),
						TextUtil.substituteCurrency(member.get(MemberDao.RrReducedPension)),
						TextUtil.substituteCurrency(member.get(MemberDao.RrSpousesPension)),
						TextUtil.substituteCurrency(member.get(MemberDao.RrMaxLumpSum)),
						TextUtil.substituteCurrency(member.get(MemberDao.RrResidualPension)),
						StringUtil.getString(callRR)};
				xmlRes = generateXmlValue(_tagName, _tagValue);//Generate xml response client

				PrintWriter out = response.getWriter();
				out.print(xmlRes);
				out.flush();
				out.close();

			}else {// when page load
				
				//clone memberDao
				MemberDao memberTemp=(MemberDao)memberDao.clone();
				//MemberDao memberTemp=memberDao;// copy object
				
				memberTemp.set( MemberDao.RedundancyDate , StringUtil.getString(valueMap.get(MemberDao.RedundancyDate)));
				memberTemp.set( MemberDao.AccruedPension, StringUtil.getString(member.get(MemberDao.AccruedPension)));
				memberTemp.set( MemberDao.EgpCash, StringUtil.getString(member.get(MemberDao.EgpCash)));
				memberTemp.set( MemberDao.SrpCash, StringUtil.getString(member.get(MemberDao.SrpCash)));
				memberTemp.set( MemberDao.TaxFreeCash, StringUtil.getString(member.get(MemberDao.TaxFreeCash)));
				memberTemp.set( MemberDao.TaxableCash, StringUtil.getString(member.get(MemberDao.TaxableCash)));
				memberTemp.set( MemberDao.TaxPayable, StringUtil.getString(member.get(MemberDao.TaxPayable)));
				memberTemp.set( MemberDao.RrReducedPension, StringUtil.getString(member.get(MemberDao.RrReducedPension)));
				memberTemp.set( MemberDao.RrSpousesPension, StringUtil.getString(member.get(MemberDao.RrSpousesPension)));
				memberTemp.set( MemberDao.RrMaxLumpSum, StringUtil.getString(member.get(MemberDao.RrMaxLumpSum)));
				memberTemp.set( MemberDao.RrResidualPension, StringUtil.getString(member.get(MemberDao.RrResidualPension)));
				memberTemp.set( "CalcRR", StringUtil.getString(callRR));
				
				currentUser.deleteAdditionalInfo(Environment.MEMBER_KEY);
				currentUser.setAdditionalInfo(Environment.MEMBER_KEY,memberTemp);
				
				//BEGIN:EDIT
				String[] _tagName = { MemberDao.RedundancyDate ,MemberDao.AccruedPension, MemberDao.EgpCash,
						MemberDao.SrpCash, MemberDao.TaxFreeCash,
						MemberDao.TaxableCash, MemberDao.TaxPayable,
						MemberDao.RrReducedPension, MemberDao.RrSpousesPension,
						MemberDao.RrMaxLumpSum, MemberDao.RrResidualPension,"CalcRR"};

				String[] _tagValue = {StringUtil.getString(valueMap.get(MemberDao.RedundancyDate)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.AccruedPension)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.EgpCash)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.SrpCash)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.TaxFreeCash)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.TaxableCash)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.TaxPayable)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.RrReducedPension)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.RrSpousesPension)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.RrMaxLumpSum)),
						TextUtil.substituteCurrency(memberTemp.get(MemberDao.RrResidualPension)),
						StringUtil.getString(callRR)};
				xmlRes = generateXmlValue(_tagName, _tagValue);//Generate xml response client

				PrintWriter out = response.getWriter();
				out.print(xmlRes);
				out.flush();
				out.close();
				//END:EDIT

			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("com.bp.pensionline.modeller.RedundancyHandler.doProccess", e);
			
		}

	}

	/**
	 * @param param
	 * @param valueMap
	 * @return
	 * @throws Exception
	 * Parse xml request from client
	 */
	private boolean prepareData(String param, HashMap valueMap)
			throws Exception {
		boolean isTrue = false;
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();

			ByteArrayInputStream byteArr = new ByteArrayInputStream(param
					.getBytes());

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(byteArr);

			doc.getDocumentElement().normalize();

			NodeList listOfMembers = doc.getElementsByTagName("AjaxParameterRequest");

			for (int s = 0; s < listOfMembers.getLength(); s++) {

				Node firstNode = listOfMembers.item(s);
				if (firstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element firstElement = (Element) firstNode;
					valueMap.put(MemberDao.RedundancyDate, getNodeValue(firstElement,MemberDao.RedundancyDate));
					

				}
			}
			isTrue = true;
		} catch (Exception e) {
			// TODO: handle exception
			isTrue = false;
			LOG.error("com.bp.pensionline.modeller.RedundancyHandler.prepareData", e);
			throw e;

		}
		return isTrue;

	}

	/**
	 * @param element
	 * @param tagName
	 * @return
	 * @throws Exception
	 * Get node value from xml
	 */
	private  String getNodeValue(Element element, String tagName)
			throws Exception {
		NodeList nodeList = element.getElementsByTagName(tagName);
		Element tagNameElement = (Element) nodeList.item(0);
		NodeList textList = tagNameElement.getChildNodes();

		if (((Node) textList.item(0)) != null) {
			

			return ((Node) textList.item(0)).getNodeValue().trim();
		} else {
			return new String("null");
		}

	}

	/**
	 * @param tagName
	 * @param tagValue
	 * @return String value as Xml. tagName[] and tagValue[] have the same size
	 *         (length)
	 */
	private String generateXmlValue(String tagName[], String[] tagValue) {
		StringBuffer buf = new StringBuffer();
		buf.append("<AjaxResponseXml>");
		int tagnameLength = tagName.length;
		int tagvalueLength = tagName.length;
		if (tagnameLength == tagvalueLength) {
			for (int i = 0; i < tagnameLength; i++) {
				buf.append("<").append(tagName[i]).append(">");
				if (tagValue[i] != null && tagValue[i].length() > 0) {
					buf.append(tagValue[i]);
				} else {
					buf.append(StringUtil.EMPTY_STRING);
				}
				buf.append("</").append(tagName[i]).append(">");

			}

		} else {
			buf.append("<Error>True</Error>");
		}
		buf.append("</AjaxResponseXml>");
		return buf.toString();

	}
	
	/**
	 * Check if global calc adjust is set in XML
	 * @return
	 */
	public static Date getConfiguredRedundancyDateChangeEffective() 
	{
		Date redundancyDateChangeEffective = null;
		try 
		{
			XmlReader reader = new XmlReader();// Create object read xml file (CalculationMapping.xml)

			ByteArrayInputStream btArr = new ByteArrayInputStream(
					reader.readFile(Environment.CALC_REDUNDANCY_DATE_CHANGE_EFFECTIVE_FILE));

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(btArr);

			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();

			// get calc list
			NodeList calcList = root.getElementsByTagName("RedundancyDateChangeEffective");
			
			int listLength = calcList.getLength();
			if (listLength > 0)
			{
				Node calcAdjustNode = calcList.item(0);
				if (calcAdjustNode != null && calcAdjustNode.getNodeType() == Node.ELEMENT_NODE)
				{
					String redundancyDateChangeEffectiveStr = calcAdjustNode.getTextContent();	
					redundancyDateChangeEffective = new SimpleDateFormat("dd/MM/yyyy").parse(redundancyDateChangeEffectiveStr);
				}
			}						
		}
		catch (Exception e) 
		{
			LOG.error("Error in getting getConfiguredReducedPensionColName: " + e);
		}
		
		return redundancyDateChangeEffective;
	}	

}
