package com.bp.pensionline.exception;

public class MemberNotFoundException extends Exception {
	
	private String _message ;
	
	public MemberNotFoundException(String message){
		
		_message = message;
		
	}
	public String getMessage(){
		return _message;
	}
}
