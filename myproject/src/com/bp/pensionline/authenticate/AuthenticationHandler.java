package com.bp.pensionline.authenticate;

import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.exception.DAOException;

public interface AuthenticationHandler {
	public boolean authenticate() throws AuthenticationException, DAOException;
}
