package com.bp.pensionline.authenticate;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.ldap.LDAPAuthenticateImpl;
import com.bp.pensionline.authenticate.util.AuthenticationHelper;
import com.bp.pensionline.authenticate.util.StringUtil;

public class AuthenticationFactory {

	public static String BP1_START = "bp1";
	public static String USERNAME_REGEX_BP1 = "(^([a-z]){5}\\d$)";
	public static String USERNAME_REGEX_NINO = "(^([a-zA-Z]){2}[0-9]{6}[a-zA-Z]{1})$";
	public static String USERNAME_REGEX_CMS = "([a-zA-Z0-9]|[-_~$@\\.]){8,30}$";
	
	public static final Log LOG = CmsLog.getLog(AuthenticationFactory.class);
	
	public static AuthenticationHandler getAuthenticationHandler(String username,
			String password) throws AuthenticationException {
		
		String loginName = username.toLowerCase();
		
		// Removes special characters
		loginName = StringUtil.escape(loginName);
		
		// Log information out
		LOG.info("Analysing user type for name '" + loginName + "'...");
		
		// Do checks
		if (loginName.startsWith(BP1_START)) {
			// BP1 user detected
			LOG.info("--> Name '" + loginName + "' is BP1 user");
			return new LDAPAuthenticateImpl(loginName, password);
		} else if (AuthenticationHelper.match_BP1(loginName)) {
			// BP1 user detected
			LOG.info("--> Name '" + loginName + "' is BP1 user");
			return new LDAPAuthenticateImpl(loginName, password);
		} else if (AuthenticationHelper.match_NINO(loginName)) {
			// NINO user detected
			LOG.info("--> Name '" + loginName + "' is NINO user");
			return new NINOAuthenticateImpl(loginName, password);
		} else if (AuthenticationHelper.match_CMS(loginName)) {
			// CMS user detected
			LOG.info("--> Name '" + loginName + "' is CMS user");
			return new CMSAuthenticationImpl(loginName, password);
		} 
		
		LOG.info("--> Name '" + loginName + "' does not match any type of [BP1, NINO, CMS]");
		throw new AuthenticationException();
	}
}
