package com.bp.pensionline.authenticate.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsRequestContext;
import org.opencms.file.CmsUser;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.authenticate.ASD;
import com.bp.pensionline.authenticate.service.PLSelfService;
import com.bp.pensionline.authenticate.util.AuthenticationHelper;
import com.bp.pensionline.authenticate.util.AuthenticationUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.handler.UserExpriedHandler;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;
/**
 * Servlet implementation class ChangeNINO
 */
public class ChangeNINO extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger LOG = Logger.getLogger(ChangeNINO.class);
	
	//private String requestUserName;
	private final String NODE_PLNAME = "plName";
	
	//private String plName;
	//private String responseMessage;
	private String success = "Success";

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reqPlId = request.getParameter("plid");
		CmsUser currentUser = AuthenticationUtil.getCurrentUser(request);
		
		String requestUserName = null;
		String responseMessage = null;
		if(currentUser != null)	requestUserName = currentUser.getName();
		
		if (requestUserName == null) {
			responseMessage = "You must login before change your NINO.";
		} else if (reqPlId == null) {
			responseMessage = "The requested parameter is null.";
		} else {
			try {			
				if (validateNINO(reqPlId).equals(success)) {
					String ninoName = requestUserName;
					int index = requestUserName.lastIndexOf("/");
					if (index != -1) {
						ninoName = requestUserName.substring(index + 1).trim();
					}
					LOG.info("Matched PensionLine ID - " + reqPlId + " for NINO: " 
							+ ninoName);
					try {
						Map<String, String> map = currentUser.getAdditionalInfo();
						String password = map.get(Environment.MEMBER_USERPASSWORD);
						
						
						if ((password != null) && (!password.trim().equals(""))) {
							LOG.info("Start create PL ID - " + reqPlId + " for NINO: " 
									+ ninoName);
							
							Map<String, String> newmap = currentUser.getAdditionalInfo();
							newmap.put(Environment.MEMBER_USERPASSWORD, "");
							
							boolean result = 
								AuthenticationUtil.createWebUser(reqPlId, 
										password, "Members", 
										currentUser.getDescription(), 
										newmap);

							if (result) {
								LOG.info("PL ID " + reqPlId + " created for NINO " 
										+ ninoName.toLowerCase());
								
								// Update force change password table
								UserExpriedHandler.update(requestUserName, reqPlId);
								
								// Update ADD_STATIC_DATA
								PLSelfService service = new PLSelfService();
								String refno = service.getREFNOForMigrated(ninoName);
								if (refno != null) {
									ASD asd = new ASD();
									asd.setPlId(reqPlId);
									asd.setRefno(refno);
									service.updateMigration(asd, ninoName);
								}

								// Processes logout
								LOG.info("Force logout - " + reqPlId 
										+ " for NINO: " + ninoName);
								AuthenticationUtil.logout(request);
								
								LOG.info("Start check/delete NINO - " + requestUserName);
								CmsObject cmsObj = AuthenticationUtil.getAdminCmsObject();
								cmsObj.deleteUser(requestUserName);

								responseMessage = "NINO has been changed successfully.";
								LOG.info("The NINO has been removed - " 
										+ ninoName.toLowerCase());
							} else {
								responseMessage = "Unable to create PL ID " + reqPlId 
									+ " for NINO: " + ninoName;
								LOG.error(responseMessage);
							}
						} else {
							responseMessage = "Unable to create PL ID " 
								+ reqPlId + " for NINO: " + ninoName;
						}
					} catch (Exception ex) {
						responseMessage = "Unable to create PL ID " 
							+ reqPlId + " for NINO: " + ninoName;
						LOG.error(responseMessage + ", detail: " 
								+ ex.getMessage());
					}
				} else {
					responseMessage = validateNINO(reqPlId);
					LOG.info("The entered name does not match - " + reqPlId);
				}
			} catch (Exception ex) {
				// ex.printStackTrace();
				responseMessage = "Fail to change NINO.";
			}
		}

		response.setHeader("pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-control", "no-store"); 
		response.setHeader("Expires", "-1"); 

		response.getWriter().print(responseMessage);
	}
	
	/*
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String requestXml = request.getParameter(Constant.PARAM);
		CmsUser currentUser = AuthenticationUtil.getCurrentUser(request);
		 
		if(currentUser != null)	requestUserName = currentUser.getName();
		
		if (requestUserName == null) {
			responseMessage = "You must login before change your NINO.";
		} else if (requestXml == null) {
			responseMessage = "Fail to change NINO.";
		} else {
			try {
				if (!parseRequestXml(requestXml)) {
					responseMessage = "System cannot parse the XML.";
					return;
				}
				
				if (validateNINO(plName)) {
					LOG.info("Matched PensionLine ID - " + plName);
					try {
						Map<String, String> map = currentUser.getAdditionalInfo();
						String password = map.get(Environment.MEMBER_USERPASSWORD);
						
						if ((password != null) && (!password.trim().equals(""))) {
							LOG.info("Start create PL ID - " + plName);
							
							Map<String, String> newmap = currentUser.getAdditionalInfo();
							newmap.put(Environment.MEMBER_USERPASSWORD, "");
							
							boolean result = 
								AuthenticationUtil.createWebUser(plName, 
										password, "Members", 
										currentUser.getDescription(), 
										newmap);

							if (result) {
								String bgroup = "";
								String ninoName = requestUserName;
								int index = requestUserName.lastIndexOf("/");
								if (index != -1) {
									ninoName = requestUserName.substring(index + 1).trim();
								}
								
								LOG.info("PL ID " + plName + " created for NINO " 
										+ ninoName.toLowerCase());
								
								// Update force change password table
								UserExpriedHandler.update(requestUserName, plName);
								
								// Update ADD_STATIC_DATA
								PLSelfService service = new PLSelfService();
								String refno = service.getREFNOForMigrated(ninoName);
								if (refno != null) {
									ASD asd = new ASD();
									asd.setPlId(plName);
									asd.setRefno(refno);
									service.updateMigration(asd, ninoName);
								}

								// Processes logout
								LOG.info("Force logout - " + plName);
								AuthenticationUtil.logout(request);
								
								LOG.info("Start check/delete NINO - " + requestUserName);
								CmsObject cmsObj = AuthenticationUtil.getAdminCmsObject();
								cmsObj.deleteUser(requestUserName);

								responseMessage = "NINO has been changed successfully.";
								LOG.info("The NINO has been removed - " 
										+ ninoName.toLowerCase());
							} else {
								responseMessage = "Unable to create PL ID " + plName + ".";
								LOG.error(responseMessage);
							}
						} else {
							responseMessage = "Unable to create PL ID.";
						}
					} catch (Exception ex) {
						responseMessage = "Unable to create PL ID.";
						LOG.error(responseMessage + ", detail: " 
								+ ex.getMessage());
					}
				} else {
					LOG.info("The entered name does not match - " + plName);
				}
			} catch (Exception ex) {
				// ex.printStackTrace();
				responseMessage = "Fail to change NINO.";
			}
		}

		response.setHeader("pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-control", "no-store"); 
		response.setHeader("Expires", "-1"); 
		response.getWriter().print(responseMessage);
	} */
	
	private String parseRequestXml(String xml)
	{
		String result = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try	{
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(inputStream);

			Element root = document.getDocumentElement();
			NodeList list = root.getChildNodes();
			int nlength = list.getLength();
			for (int i = 0; i < nlength; i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equalsIgnoreCase(NODE_PLNAME)) {						
						result = getCDataText(node);
					}
				}
			}
			inputStream.close();
		}
		catch (Exception ex)
		{
			LOG.error("Exception in ChangeNINO: " + ex.getMessage());
			return result;
		}

		return result;
	}
	
	public String getCDataText(Node aNode) {
		if (aNode != null) {
			NodeList requestTagNodes = aNode.getChildNodes();
			for (int j = 0; j < requestTagNodes.getLength(); j++) {
				Node current = requestTagNodes.item(j);
				if(current.getNodeType() == Node.CDATA_SECTION_NODE) {
					return ((CDATASection)current).getData();
				}
			}			
		}
		
		return "";
	}
	
	private String validateNINO(String plId) {
		String result = success;
		if (AuthenticationHelper.isNINOUser(plId)) {
			result = "Your username must be not in the format of a NINO.";
			LOG.warn(result + " - " + plId);
			return result;
		}
		
		if (AuthenticationUtil.checkWebuserExist(plId) 
				|| AuthenticationUtil.checkUserExist(plId)) {
			result = "Invalid username selected, please select another username.";
			LOG.warn(result + " - " + plId);
			return result;
		}
		
		PLSelfService service = new PLSelfService();
		if (service.existASDRow(plId)) {
			result = "Your username must be not existed as a"
				+ " BP1 user id stored in the Administrator database.";
			LOG.warn(result
					+ " - " 
					+ plId);
			return result;
		}
		
		return result;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
