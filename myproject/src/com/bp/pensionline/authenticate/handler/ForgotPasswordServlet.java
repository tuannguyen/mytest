package com.bp.pensionline.authenticate.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.authenticate.ASD;
import com.bp.pensionline.authenticate.UserInfo;
import com.bp.pensionline.authenticate.service.PLSelfService;
import com.bp.pensionline.authenticate.util.AuthenticationUtil;
import com.bp.pensionline.authenticate.util.DateUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.consumer.ImailConsumer;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.database.ImailSQLHandler;
import com.bp.pensionline.test.Constant;
import com.bp.pensionline.util.SystemAccount;

/**
 * Servlet implementation class ForgotPasswordServlet
 */
public class ForgotPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	static Logger LOG = Logger.getLogger(ForgotPasswordServlet.class);
	private HashMap<Object, Object> valueMap = new HashMap<Object, Object>();

	private final String NODE_NINO = "NINO";
	private final String NODE_DOB = "DOB";
	private final String NODE_POSTCODE = "POSTCODE";
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String requestXml = request.getParameter(Constant.PARAM);
		
		String ninoName = null;
		String dateOfBirth = null;
		String postCode = null;
		String responseMessage = null;
		
		String error101 = com.bp.pensionline.authenticate.Constant.EE101;
		if (requestXml == null) {
			responseMessage = error101;
			return;
		}
		
		try {
			DocumentBuilderFactory factory = 
				DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;
			try	{
				builder = factory.newDocumentBuilder();
				ByteArrayInputStream inputStream = 
					new ByteArrayInputStream(requestXml.getBytes());

				document = builder.parse(inputStream);

				Element root = document.getDocumentElement();
				NodeList list = root.getChildNodes();
				int nlength = list.getLength();
				for (int i = 0; i < nlength; i++) {
					Node node = list.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						if (node.getNodeName().equalsIgnoreCase(NODE_NINO)) {
							ninoName = getCDataText(node);
						} else if (node.getNodeName().equalsIgnoreCase(NODE_DOB)) {
							dateOfBirth = getCDataText(node);
						} else if (node.getNodeName().equalsIgnoreCase(NODE_POSTCODE)) {
							postCode = getCDataText(node);
						}
					}
				}
				inputStream.close();
			}
			catch (Exception ex)
			{
				LOG.error("Exception in pocessing password self-service: " 
						+ ex.getMessage());
				ninoName = "";
				dateOfBirth = "";
				postCode = "";
				responseMessage = "System cannot parse your data.";
				return ;
			}

			LOG.info("Check user's informatin - " + ninoName);
			
			UserInfo info = new UserInfo();
			info.setNinoName(ninoName);
			info.setDateOfBirth(DateUtil.formatDate(dateOfBirth));
			if (postCode == null) {
				postCode = "";
			}
			info.setPostCode(postCode);
			
			UserInfo resultObj = new UserInfo();
			PLSelfService service = new PLSelfService();
			resultObj = service.getUserInfo(info);

			if (resultObj == null) {
				LOG.info("System cannot find user's information - " + ninoName);
				responseMessage = error101;
				String errorMessage = responseMessage;
	            String errorDes = responseMessage;
	            
	            String errorRef = ("" + Math.random()).substring(0, 7);
	            
	            request.getSession().setAttribute("errorStatus",
	            		com.bp.pensionline.authenticate.Constant.ERROR_CODE_204);
	            request.getSession().setAttribute("errorRef", errorRef);
	            request.getSession().setAttribute("errorMessage", errorMessage);
	            request.getSession().setAttribute("errorDes", errorDes);
			} else {
				responseMessage = "Success";

				// Processes IMail data
				processImail(request, response, ninoName, resultObj);
				LOG.info("An iMail has been created - " + ninoName);
			}
		} catch (Exception ex) {
			LOG.error("Error in processing user's request - " + ex.getMessage());
			responseMessage = error101;
		}

		response.getWriter().print(responseMessage);
	}
	
	private void processImail(HttpServletRequest request,
			HttpServletResponse response, String ninoName, UserInfo info) 
					throws ServletException, IOException {
		response.setContentType(Constant.CONTENT_TYPE);
		
		// Gets max value from IMail table
		ImailSQLHandler imailSQLHandler = new ImailSQLHandler();		
		int maxIMailNo = imailSQLHandler.getMaxIMailNo() + 1;
		
		try {
			LOG.info("Start process IMail");

			valueMap.put(Environment.IMAIL_BGROUP, info.getBgroup());
			valueMap.put(Environment.IMAIL_REFNO, info.getRefno());
			valueMap.put(Environment.IMAIL_TEMP, maxIMailNo);
			valueMap.put(Environment.IMAIL_NI, info.getNinoName());
			valueMap.put(Environment.IMAIL_EMAILADRESS, ninoName 
					+ EMAIL_SUFFIX);
			valueMap.put(Environment.IMAIL_SUBJECT, EMAIL_SUBJECT);
			valueMap.put(Environment.IMAIL_QUERY, EMAIL_SUBJECT);
			valueMap.put(Environment.IMAIL_CREATIONDATE, new java.sql.Date(
					System.currentTimeMillis()));
			valueMap.put(Environment.IMAIL_CASEWORKID, maxIMailNo);
			valueMap.put(IMAIL.FIRS_TNAME, info.getFirstName());
			valueMap.put(IMAIL.LAST_NAME, info.getLastName());
			
			valueMap.put(Environment.IMAIL_MESSAGE, getMessage());
			valueMap.put(Environment.IMAIL_FORMDATA,
					prepareFormDataFieldValue());

			LOG.info("Update IMail data in database");
			ImailConsumer imailConsumer = new ImailConsumer(valueMap);
			imailConsumer.processData();
			
			LOG.info("End process IMail");
		} catch (Exception e) {
			LOG.error(e.getMessage() +e.getCause());
		} // end-try-catch
	}
	
	public static final String EMAIL_SUFFIX = "@bp.pensionline.com";
	public static final String EMAIL_SUBJECT = "Forgot my password";
	
	public interface IMAIL {
		public static final String FIRS_TNAME = "FIRSTNAME";
		public static final String LAST_NAME = "LASTNAME";
	}
	
	private String getMessage() {
		String msg = "";
		
		msg += "Bgroup: " +  valueMap.get(Environment.IMAIL_BGROUP);
		msg += "\n";
		
		msg += "RefNo: " +  valueMap.get(Environment.IMAIL_REFNO);
		msg += "\n";
		
		msg += "NI: " +  valueMap.get(Environment.IMAIL_NI);
		msg += "\n";
		
		msg += "FirstName: " +  valueMap.get(IMAIL.FIRS_TNAME);
		msg += "\n";
		
		msg += "LastName: " +  valueMap.get(IMAIL.LAST_NAME);
		msg += "\n";
		
		return msg;
	}
	
	private String prepareFormDataFieldValue() throws Exception {
		StringBuffer buf = new StringBuffer();
		String mySpace = "	";
		buf.append("<ForgotPassword>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<Bgroup>");
		buf.append(valueMap.get(Environment.IMAIL_BGROUP));
		buf.append("</Bgroup>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<Refno>");
		buf.append(valueMap.get(Environment.IMAIL_REFNO));
		buf.append("</Refno>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<NI>");
		buf.append(valueMap.get(Environment.IMAIL_NI));
		buf.append("</NI>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<CreationDate>");
		buf.append(valueMap.get(Environment.IMAIL_CREATIONDATE));
		buf.append("</CreationDate>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<FirstName>");
		buf.append(valueMap.get(IMAIL.FIRS_TNAME));
		buf.append("</FirstName>");
		buf.append("\n");
		
		buf.append(mySpace);
		buf.append("<LastName>");
		buf.append(valueMap.get(IMAIL.LAST_NAME));
		buf.append("</LastName>");
		buf.append("\n");

		buf.append("</ForgotPassword>");
		return buf.toString();

	}
	
	protected String getNodeValue(Element element, String tagName)
							throws Exception {
		NodeList nodeList = element.getElementsByTagName(tagName);
		Element tagNameElement = (Element) nodeList.item(0);
		NodeList textList = tagNameElement.getChildNodes();
		
		if (((Node) textList.item(0)) != null) {
			return ((Node) textList.item(0)).getNodeValue().trim();
		} else {
			return new String("null");
		}
	}
	
	public String getCDataText(Node aNode) {
		if (aNode != null) {
			NodeList requestTagNodes = aNode.getChildNodes();
			for (int j = 0; j < requestTagNodes.getLength(); j++) {
				Node current = requestTagNodes.item(j);
				if(current.getNodeType() == Node.CDATA_SECTION_NODE) {
					return ((CDATASection)current).getData();
				}
			}			
		}
		
		return "";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
