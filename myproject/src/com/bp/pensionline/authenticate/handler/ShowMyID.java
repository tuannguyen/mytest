package com.bp.pensionline.authenticate.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.opencms.configuration.CmsConfigurationManager;
import org.opencms.jsp.util.CmsJspStatusBean;
import org.opencms.main.Messages;
import org.opencms.main.OpenCmsCore;
import org.opencms.util.CmsStringUtil;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.authenticate.UserInfo;
import com.bp.pensionline.authenticate.service.PLSelfService;
import com.bp.pensionline.authenticate.util.DateUtil;
import com.bp.pensionline.test.Constant;

/**
 * Servlet implementation class GetUserID
 */
public class ShowMyID extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger LOG = Logger.getLogger(ShowMyID.class);
	
	private final String NODE_NINO = "NINO";
	private final String NODE_DOB = "DOB";
	private final String NODE_POSTCODE = "POSTCODE";

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String requestXml = request.getParameter(Constant.PARAM);
		
		String ninoName = null;
		String dateOfBirth = null;
		String postCode = null;
		String responseMessage = null;
		LOG.info("Processes UserId self service...");
		
		String error101 = com.bp.pensionline.authenticate.Constant.EE101;
		String userid = "userid";
		if (requestXml == null) {
			responseMessage = error101;
		} else {
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = null;
				Document document = null;
				try	{
					builder = factory.newDocumentBuilder();
					ByteArrayInputStream inputStream = new ByteArrayInputStream(requestXml.getBytes());

					document = builder.parse(inputStream);

					Element root = document.getDocumentElement();
					NodeList list = root.getChildNodes();
					int nlength = list.getLength();
					for (int i = 0; i < nlength; i++) {
						Node node = list.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							if (node.getNodeName().equalsIgnoreCase(NODE_NINO)) {						
								ninoName = getCDataText(node).trim();
							} else if (node.getNodeName().equalsIgnoreCase(NODE_DOB)) {
								dateOfBirth = getCDataText(node).trim();
							} else if (node.getNodeName().equalsIgnoreCase(NODE_POSTCODE)) {
								postCode = getCDataText(node).trim();
							}
						}
					}
					inputStream.close();
				}
				catch (Exception ex)
				{
					LOG.error("Exception in ChangeNINO: " + ex.getMessage());
					ninoName = "";
					dateOfBirth = "";
					postCode = "";
					responseMessage = "Invalid data.";
					return;
				}
				
				PLSelfService service = new PLSelfService();
				
				UserInfo info = new UserInfo();
				info.setNinoName(ninoName);
				info.setDateOfBirth(DateUtil.formatDate(dateOfBirth));
				if (postCode == null) {
					postCode = "";
				}
				info.setPostCode(postCode);
				
				String result = service.getUserID(info);
				if ((result != null) && (result.trim().length() > 0)) {
					LOG.info("Valid data for getting UserID - NINO: " + ninoName);
					userid = result.trim();
					responseMessage = "Success";
				} else {
					responseMessage = error101;
					String errorMessage = responseMessage;
		            String errorDes = responseMessage;
		            
		            String errorRef = ("" + Math.random()).substring(0, 7);
		            
		            request.getSession().setAttribute("errorStatus",
		            		com.bp.pensionline.authenticate.Constant.ERROR_CODE_204);
		            request.getSession().setAttribute("errorRef", errorRef);
		            request.getSession().setAttribute("errorMessage", errorMessage);
		            request.getSession().setAttribute("errorDes", errorDes);
					LOG.error("System cannot find the user's details");
				}
			} catch (Exception ex) {
				LOG.error("Error in processing request, details: " 
						+ ex.getMessage());
				responseMessage = error101;
			}
		}
		
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		
		response.getWriter().print(generateXML(responseMessage, userid));
	}
	
	private String generateXML(String message, String userid) {
		String xml = "";
		xml += "<AjaxParameterResponse>\n";		
		xml += "		<Message><![CDATA[" + message + "]]></Message>\n";
		xml += "		<UserId><![CDATA[" + userid + "]]></UserId>\n";
		xml += "</AjaxParameterResponse>\n";
		
		return xml;
	}

	public String getCDataText(Node aNode) {
		if (aNode != null) {
			NodeList requestTagNodes = aNode.getChildNodes();
			for (int j = 0; j < requestTagNodes.getLength(); j++) {
				Node current = requestTagNodes.item(j);
				if(current.getNodeType() == Node.CDATA_SECTION_NODE) {
					return ((CDATASection)current).getData();
				}
			}			
		}
		
		return "";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet");
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		processRequest(request, response);
	}

}
