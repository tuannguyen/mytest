package com.bp.pensionline.authenticate;

import java.io.Serializable;

public class AuthObject implements Serializable {
	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private String bp1Id;
	private String nino;
	private String plId;

	public String getPlId() {
		return plId;
	}
	public void setPlId(String plId) {
		this.plId = plId;
	}
	public String getBp1Id() {
		return bp1Id;
	}
	public void setBp1Id(String bp1Id) {
		this.bp1Id = bp1Id;
	}
	public String getNino() {
		return nino;
	}
	public void setNino(String nino) {
		this.nino = nino;
	}
}
