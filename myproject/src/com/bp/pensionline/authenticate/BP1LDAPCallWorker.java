package com.bp.pensionline.authenticate;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.ldap.LDAPAuthenticateImpl;

public class BP1LDAPCallWorker {
	private static long lastTry = 0;
	
	private static int timeBetweenRetries = 40 * 1000; // 40 seconds
	private static int maxConnections = 15;
	static Logger LOG = Logger.getLogger(BP1LDAPCallWorker.class);
	
	public static final boolean canCallLDAP(long currentTime) {
		boolean canCall = true;
		
		long connections = 0;
		LOG.info("A call check is delivered - currentTime: " + currentTime);
		LOG.info("A call check is delivered - lastTry: " + lastTry);
		LOG.info("A call check is delivered - timeBetweenRetries: " + timeBetweenRetries);
		LOG.info("A call check is delivered - connections: " + connections);
		if (currentTime  - lastTry <= timeBetweenRetries ){
			LOG.info("A call check is delivered - time in limitation - check connections");
			if (connections >= maxConnections) {
				LOG.info("A call check is delivered - connections not allowed");
	            
	            canCall = false;
			}
		}
		
		// Let say OK
		LOG.info("A call check is delivered - connections allowed");
		LOG.info("A call check is delivered - time allowed");
		lastTry = currentTime;
		
		return canCall;
	}
	
	private boolean available = false;

    public synchronized long get() {
        while (available == false) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        available = false;
        notifyAll();
        return lastTry;
    }

    public synchronized void put(long value) {
        while (available == true) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        lastTry = value;
        available = true;
        notifyAll();
    }
}
