package com.bp.pensionline.authenticate.exception;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Thrown to indicate that one or more properties that are required to configure
 * a service are missing or invalid e.g. non-numeric for a property requiring a
 * numeric value.
 *
 * @author  Binh Nguyen
 * @version 1.0.0 November 19, 2009
 */
public class PropertyException extends Exception {

    //~ Instance fields --------------------------------------------------------

    /**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/** The missing properties. */
    private List<String> missingProperties;

    /** The invalid properties. */
    private List<String> invalidProperties;

    /**
     * The name of the properties file that is associated with the missing or
     * invalid properties.
     */
    private String propertyFileName;

    //~ Constructors -----------------------------------------------------------

    /**
     * Constructs a <code>PropertyException</code> with no detail message.
     */
    public PropertyException() {
        super();
        missingProperties = new ArrayList<String>();
        invalidProperties = new ArrayList<String>();
    }

    /**
     * Constructs a <code>PropertyException</code> with the specified detail
     * message.
     *
     * @param  msg  the detail message.
     */
    public PropertyException(final String msg) {
        super(msg);
        missingProperties = new ArrayList<String>();
        invalidProperties = new ArrayList<String>();
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Indicates if there are properties that were missing from the property
     * file.
     *
     * @return  <code>true</code> if there is one or more missing properties;
     *          otherwise <code>false</code>.
     */
    public final boolean hasMissing() {
        return (missingProperties.size() > 0);
    }

    /**
     * Indicates if there are properties that were invalid in the property file.
     *
     * @return  <code>true</code> if there is one or more invalid properties;
     *          otherwise <code>false</code>.
     */
    public final boolean hasInvalid() {
        return (invalidProperties.size() > 0);
    }

    /**
     * Add the property name to the list of missing properties.
     *
     * @param  propertyName  the name of the property.
     */
    public final void addMissingProperty(final String propertyName) {
        if (propertyName != null) {
            missingProperties.add(propertyName);
        }
    }

    /**
     * Add the property name to the list of invalid properties.
     *
     * @param  propertyName  the name of the property.
     */
    public final void addInvalidProperty(final String propertyName) {
        if (propertyName != null) {
            invalidProperties.add(propertyName);
        }
    }

    /**
     * Returns a <code>Collection</code> of properties that were missing.
     *
     * <p>The collection will be empty if there are no missing properties.</p>
     *
     * @return  a collection of <code>String</code> property names that are
     *          missing.
     */
    public final List<String> getMissingProperties() {
        return missingProperties;
    }

    /**
     * Returns a <code>Collection</code> of properties that were invalid.
     *
     * <p>The collection will be empty if there are no invalid properties.</p>
     *
     * @return  a collection of <code>String</code> property names that are
     *          invalid.
     */
    public final List<String> getInvalidProperties() {
        return invalidProperties;
    }

    /**
     * Sets the name of the properties file that is associated with this
     * exception.
     *
     * @param  propertyFileName  the property file name.
     */
    public final void setPropertyFileName(final String propertyFileName) {
        this.propertyFileName = propertyFileName;
    }

    /**
     * Returns the name of the properties file that is associated with this
     * exception.
     *
     * @return  the name of the properties file.
     */
    public final String getPropertyFileName() {
        return this.propertyFileName;
    }

    /**
     * Returns a <code>String</code> based representation of a <code>
     * MissingPropertyException</code>.
     *
     * @return  <code>PropertyException</code> as a <code>String</code>.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [ ");
        if (super.getMessage() != null) {
            sb.append("errorMessage=").append(super.getMessage()).append(", ");
        }
        sb.append("propertyFileName=");
        if (propertyFileName == null) {
            sb.append("Not Set, ");
        } else {
            sb.append(propertyFileName).append(", ");
        }
        sb.append("missingProperties={ ");
        if (missingProperties.size() > 0) {
            for (Iterator<String> iter = missingProperties.iterator();
                    iter.hasNext();) {
                String propName = (String) iter.next();
                sb.append(propName);
                sb.append(", ");
            }
            sb.setLength(sb.length() - 2);
        } else {
            sb.append("empty");
        }
        sb.append(" }, ");
        sb.append("invalidProperties={ ");
        if (invalidProperties.size() > 0) {
            for (Iterator<String> iter = invalidProperties.iterator();
                    iter.hasNext();) {
                String propName = (String) iter.next();
                sb.append(propName);
                sb.append(", ");
            }
            sb.setLength(sb.length() - 2);
        } else {
            sb.append("empty");
        }
        sb.append(" }");
        sb.append(" ]");

        return sb.toString();
    }
}
