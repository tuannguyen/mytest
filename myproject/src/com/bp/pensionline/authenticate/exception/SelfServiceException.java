package com.bp.pensionline.authenticate.exception;

public class SelfServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public SelfServiceException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	/**
     * Returns a <code>String</code> based representation of a <code>
     * SelfServiceException</code>.
     *
     * @return  <code>SelfServiceException</code> as a <code>String</code>.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [ ");
        sb.append("msg=");
        sb.append(this.msg);
        sb.append(" ]");

        return sb.toString();
    }
}
