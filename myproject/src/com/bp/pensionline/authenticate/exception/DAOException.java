package com.bp.pensionline.authenticate.exception;

public class DAOException extends Exception {
	private String msg;
	
	public DAOException() {
		this.msg = "Access database fail.";
	}
	
	public DAOException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	/**
     * Returns a <code>String</code> based representation of a <code>
     * AuthenticationException</code>.
     *
     * @return  <code>AuthenticationException</code> as a <code>String</code>.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [ ");
        sb.append("msg=");
        sb.append(this.msg);
        sb.append(" ]");

        return sb.toString();
    }
}
