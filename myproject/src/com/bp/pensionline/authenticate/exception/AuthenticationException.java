package com.bp.pensionline.authenticate.exception;

/**
 * Thrown to indicate that that the user failed to authenticate using the
 * specified user id and password.
 *
 * @author  Binh Nguyen
 * @version	1.0.0 November 02, 2009
 */
public class AuthenticationException extends Exception {

    //~ Instance fields --------------------------------------------------------

    private String msg;

    //~ Constructors -----------------------------------------------------------

    /**
     * Creates a new instance of <code>AuthenticationException</code> without
     * detail message.
     */
    public AuthenticationException() {
        this.msg = "Authentication failed.";
    }

    /**
     * Constructs an instance of <code>AuthenticationException</code> with the
     * specified detail message.
     *
     * @param  msg  the detail message.
     */
    public AuthenticationException(String msg) {
        super(msg);
        this.msg = msg;
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Returns a <code>String</code> based representation of a <code>
     * AuthenticationException</code>.
     *
     * @return  <code>AuthenticationException</code> as a <code>String</code>.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [ ");
        sb.append("msg=");
        sb.append(this.msg);
        sb.append(" ]");

        return sb.toString();
    }
}
