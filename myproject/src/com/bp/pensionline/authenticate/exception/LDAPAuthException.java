package com.bp.pensionline.authenticate.exception;

public class LDAPAuthException extends Exception {
	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public LDAPAuthException(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage()
	{
		return message;
	}
}
