package com.bp.pensionline.authenticate.exception;

/**
 * Thrown to indicate that an error has occurred when connecting or
 * communicating with BP1 LDAP.
 *
 * @author  Binh Nguyen
 * @version 1.0.0 November 19, 2009
 */
public class Bp1LdaoException extends Exception {

    //~ Instance fields --------------------------------------------------------

    /**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/** Default message if not supplied. */
    private String msg = "BP1 LDAP Exception";

    /** The LDAP error code (if any). */
    private int ldapError;

    //~ Constructors -----------------------------------------------------------

    /**
     * Creates a new <code>Bp1LdaoException</code> object with the details of
     * the wrapped exception e.g. stack trace. The actual exception is
     * discarded.
     *
     * @param  exception  the wrapped exception.
     */
    public Bp1LdaoException(Exception exception) {
        super(exception);
    }

    /**
     * Constructs an instance of <code>Bp1LdaoException</code> with the
     * specified detail message.
     *
     * @param  ex   the wrapped Exception
     * @param  msg  the detail message.
     */
    public Bp1LdaoException(Exception ex, String msg) {
        super(ex);
        this.msg = msg;
    }

    /**
     * Constructs an instance of <code>Bp1LdaoException</code> with the
     * specified detail message and associated LDAP error code.
     *
     * @param  ex         the wrapped Exception
     * @param  msg        the detail message.
     * @param  ldapError  the original LDAP error code.
     */
    public Bp1LdaoException(Exception ex, String msg, int ldapError) {
        super(ex);
        this.msg = msg;
        this.ldapError = ldapError;
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Returns the LDAP error (if any).
     *
     * @return  the LDAP error.
     */
    public int getLDAPError() {
        return ldapError;
    }

    /**
     * Returns a <code>String</code> based representation of a <code>
     * Bp1LdaoException</code>.
     *
     * @return  <code>Bp1LdaoException</code> as a <code>String</code>.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [ ");
        sb.append("message=").append(super.getMessage()).append(", ");
        sb.append("ldapError=").append(ldapError);
        sb.append(" ]");

        return sb.toString();
    }
}