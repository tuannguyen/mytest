package com.bp.pensionline.authenticate.util;

import com.bp.pensionline.authenticate.ResourceLoader;
import com.bp.pensionline.authenticate.exception.Bp1LdaoException;
import com.bp.pensionline.authenticate.exception.PropertyException;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.LDAPSocketFactory;
import com.novell.ldap.connectionpool.PoolManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.Security;

import java.util.Properties;

/**
 * This class provides LDAP utility methods for the BP1 system. A client can use
 * these services to:
 *
 * <ul>
 *   <li>Get an LDAP connection with <code>getLDAPConnection()</code> ,all
 *     settings are take from <code>ldap.properties</code>.</li>
 *   <li>Get an LDAP connection with the user provided parameters using <code>
 *     getLDAPConnection(String host, int port, String principal, String
 *     credentials, boolean secure, String path)</code> method.</li>
 *   <li>Return a connection to the pool using <code>
 *     makeConnectionAvailable(LDAPConnection conn)</code> method.</li>
 *   <li>Get popular properties of directory server as <code>
 *     namingContexts</code>,<code>vendorName</code>...</li>
 * </ul>
 *
 * <p>Usage: An internal <code>PoolManager</code> manages connections to a
 * single LDAP server. The pool consists of a finite number of physical
 * connections to the LDAP server (maxConnections) and a finite number of
 * LDAPConnection objects sharing a physical connection (maxSharedConnections)
 * </p>
 *
 * <p>A physical connection, and its shared LDAPConnection objects are
 * associated with an LDAP DN and password (DN/PW). <code>
 * getBoundConnection</code> searches for a physical connection associated with
 * a given DN/PW and returns an available LDAPConnection object associated with
 * that DN/PW. If none are available it searches for an unused physical
 * connection, binds using the given DN/PW, and returns an LDAPConnection
 * object. If no physical connection is available then it waits. Once an
 * LDAPConnection object is no longer needed the <code>
 * makeConnectionAvailable</code> function must be called to make the
 * LDAPConnection available to other threads.</p>
 *
 * <p>The LDAP connection parameters can be configured using a property file
 * called <code>ldap.properties</code></p>
 *
 * @author   Binh Nguyen
 * @version  1.0.1 November 19, 2009
 */
public class LDAPUtil {

    //~ Static fields/initializers ---------------------------------------------

    /** Property key prefix string. */
    public static final String LDAP_PROP_PREFIX = "com.bp.pensionline.ldap";

    /**
     * String identifier for LDAP host. Value "com.bp.pensionline.ldap.host"
     */
    public static final String LDAP_HOST = LDAP_PROP_PREFIX + ".host";

    /**
     * String identifier for LDAP port. Value "com.bp.pensionline.ldap.port"
     */
    public static final String LDAP_PORT = LDAP_PROP_PREFIX + ".port";

    /**
     * String identifier for LDAP principal. Value
     */
    public static final String LDAP_PRINCIPAL = LDAP_PROP_PREFIX + ".principal";

    /**
     * String identifier for LDAP credentials. Value
     * "com.bp.pensionline.ldap.credentials"
     */
    public static final String LDAP_CREDENTIALS = LDAP_PROP_PREFIX
        + ".credentials";

    /**
     * String identifier for LDAP secure. Value
     * "com.bp.pensionline.ldap.secure"
     */
    public static final String LDAP_SECURE = LDAP_PROP_PREFIX + ".secure";

    /**
     * String identifier for LDAP certificate file path. Value
     * "com.bp.pensionline.ldap.path"
     */
    public static final String LDAP_PATH = LDAP_PROP_PREFIX + ".path";

    /** String identifier for credentials encoding. */
    public static final String LDAP_CREDENTIALS_ENCODING = "UTF8";

    /** String identifier for the key to LDAP security properties. */
    public static final String LDAP_KEY_STORE = "javax.net.ssl.trustStore";

    /**
     * Identifies the maximum number of physical connections allowed. Value
     * "com.bp.pensionline.ldap.maxconnections"
     */
    public static final String LDAP_MAX_CONNECTIONS = LDAP_PROP_PREFIX
        + ".maxconnections";

    /**
     * Identifies the maximum number of shared connections per physical
     * connection. Value "com.bp.pensionline.ldap.maxsharedconnections"
     */
    public static final String LDAP_MAX_SHARED_CONNECTIONS = LDAP_PROP_PREFIX
        + ".maxsharedconnections";

    /**
     * String identifier for LDAP <code>vendorVersion</code> attribute. Value
     * "vendorVersion"
     */
    public static final String VENDOR_VERSION = "vendorVersion";

    /**
     * String identifier for LDAP <code>namingContexts</code> attribute. Value
     * "namingContexts"
     */
    public static final String NAMING_CONTEXTS = "namingContexts";

    /**
     * String identifier for LDAP <code>netscapemdsuffix</code> attribute. Value
     * "netscapemdsuffix"
     */
    public static final String NETSCAPE_MD_SUFFIX = "netscapemdsuffix";

    /**
     * String identifier for LDAP <code>vendorName</code> attribute. Value
     * "vendorName"
     */
    public static final String VENDOR_NAME = "vendorName";

    /**
     * The version of directory server e.g Sun-Java(tm)-System-Directory/6.0.
     */
    private static String vendorVersion = null;

    /** The naming context of directory server. */
    private static String namingContexts = null;

    /** The netscape suffix e.g cn=ldap://dc=PLASMA:1389. */
    private static String netscapemdsuffix = null;

    /** The vendor name e.g Sun Microsystems, Inc. */
    private static String vendorName = null;

    /** Logger for this class. */
    private static Logger logger = LoggerFactory.getLogger(LDAPUtil.class);

    /** The LDAP connection pool . */
    private static PoolManager pool = null;

    /** The name of the host to which we will bind. */
    private static String host;

    /** The LDAP port number for the bind. */
    private static int port;

    /** Maximum number of connections. */
    private static int maxConnections;

    /** Maximum number of shared connections. */
    private static int maxSharedConnections;

    /**
     * The name of the LDAP user we will bind with e.g. cn=Directory Manager.
     */
    private static String principal;

    /** The principal's credentials used in the bind. */
    private static String credentials;

    /** Whether to user SSL for the LDAP connection. */
    private static boolean secure = false;

    /** Path to the keystore if we are using SSL. */
    private static String keyPath;

    /**
     * The name of the LDAP properties file that contains connection properties
     * for BP1 LDAP.
     */
    public static final String LDAP_PROPS_FILE_NAME = "ldap.properties";

    /** Indicates if the LDAP connection has been configured. */
    private static boolean configured;

    //~ Constructors -----------------------------------------------------------

    /**
     * This is a utility class and can not be instantiated.
     */
    private LDAPUtil() {
        // does nothing
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Loads the properties for configuring the LDAP connection and pool.
     *
     * @throws  PropertyException  if the property file can not be found or if
     *                             there is a missing or invalid property.
     * @throws  Bp1LdaoException   if an error occurs when creating the LDAP
     *                             connection pool.
     */
    private static void loadProperties() throws PropertyException,
    	Bp1LdaoException {
        if (logger.isDebugEnabled()) {
            logger.debug("Loading properties from {}...", LDAP_PROPS_FILE_NAME);
        }
        Properties props = new Properties();
        PropertyException propExc = new PropertyException();
        propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
        try {
            // Load the property file.
            props = ResourceLoader.getInstance().getProperties(
                    LDAPUtil.LDAP_PROPS_FILE_NAME);

            // Host property
            host = props.getProperty(LDAP_HOST);
            if ((host == null) || (host.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value", LDAP_HOST);
                propExc.addMissingProperty(LDAP_HOST);
            } else {
                logger.info("Property {} loaded with value {}", LDAP_HOST,
                    host);
            }

            // LDAP port.
            String portValue = props.getProperty(LDAP_PORT);
            if ((portValue == null) || (portValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value", LDAP_PORT);
                propExc.addMissingProperty(LDAP_PORT);
            } else {
                try {
                    port = new Integer(portValue).intValue();
                    logger.info("Property {} loaded with value {}", LDAP_PORT,
                        portValue);
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_PORT, portValue);
                    propExc.addInvalidProperty(LDAP_PORT);
                }
            }

            // Maximum number of connections.
            String maxConnectionsValue = props.getProperty(
                    LDAP_MAX_CONNECTIONS);
            if ((maxConnectionsValue == null)
                    || (maxConnectionsValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_MAX_CONNECTIONS);
                propExc.addMissingProperty(LDAP_MAX_CONNECTIONS);
            } else {
                try {
                    maxConnections = new Integer(maxConnectionsValue)
                        .intValue();
                    if (maxConnections <= 0) {
                        logger.error(
                            "{} is not valid, received {}, expected a value > 0",
                            LDAP_MAX_CONNECTIONS, maxConnectionsValue);
                        propExc.addInvalidProperty(LDAP_MAX_CONNECTIONS);
                    } else {
                        logger.info("Property {} loaded with value {}",
                            LDAP_MAX_CONNECTIONS, maxConnections);
                    }
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_MAX_CONNECTIONS, maxConnectionsValue);
                    propExc.addInvalidProperty(LDAP_MAX_CONNECTIONS);
                }
            }

            // Maximum number of shared connections.
            String maxSharedConnectionsValue = props.getProperty(
                    LDAP_MAX_SHARED_CONNECTIONS);
            if ((maxSharedConnectionsValue == null)
                    || (maxSharedConnectionsValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_MAX_SHARED_CONNECTIONS);
                propExc.addMissingProperty(LDAP_MAX_SHARED_CONNECTIONS);
            } else {
                try {
                    maxSharedConnections = new Integer(
                            maxSharedConnectionsValue).intValue();
                    logger.info("Property {} loaded with value {}",
                        LDAP_MAX_SHARED_CONNECTIONS, maxSharedConnections);
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_MAX_SHARED_CONNECTIONS, maxSharedConnectionsValue);
                    propExc.addInvalidProperty(LDAP_MAX_SHARED_CONNECTIONS);
                }
            }

            // LDAP principal.
            principal = props.getProperty(LDAP_PRINCIPAL);
            if ((principal == null) || (principal.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_PRINCIPAL);
                propExc.addMissingProperty(LDAP_PRINCIPAL);
            } else {
                logger.info("Property {} loaded with value {}", LDAP_PRINCIPAL,
                    principal);
            }

            // LDAP credentials.
            credentials = props.getProperty(LDAP_CREDENTIALS);
            if ((credentials == null) || (credentials.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_CREDENTIALS);
                propExc.addMissingProperty(LDAP_CREDENTIALS);
            } else {
                logger.info("Property {} loaded with value {}",
                    LDAP_CREDENTIALS, "********");
            }

            // If max shared connections is greater than max connections
            // then assign it to max connections.
            if (maxSharedConnections > maxConnections) {
                maxSharedConnections = maxConnections;
                logger.warn(
                    "Max shared connections [{}] is greater than max connections [{}], setting max shared connections to {}",
                    new Object[] {
                        maxSharedConnectionsValue, maxConnectionsValue,
                        maxSharedConnectionsValue
                    });
            }

            // Create the pool manager for this LDAP connection.
            pool = new PoolManager(host, port, maxConnections,
                    maxSharedConnections, null);
            configured = true;
        } catch (LDAPException ex) {
            logger.error(
                "LDAP error when creating LDAP connection pool, connection cofiguration: {}, LDAP error={}, error={}, ",
                new Object[] {
                    dumpConnectionParams(), ex.getLDAPErrorMessage(),
                    ex.getMessage()
                });
            throw new Bp1LdaoException(ex, ex.getLDAPErrorMessage(),
                ex.getResultCode());
        } catch (FileNotFoundException ex) {
            logger.error(
                "Failed to load properties from file, file {} doesn't exist",
                LDAP_PROPS_FILE_NAME);
            propExc = new PropertyException(ex.getMessage());
            propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
            throw propExc;
        } catch (IOException ex) {
            logger.error("Failed to read file {}, error: {}",
                LDAP_PROPS_FILE_NAME, ex.getMessage());
            propExc = new PropertyException(ex.getMessage());
            propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
            throw propExc;
        }
    }

    /**
     * Get an <code>LDAPConnection</code> with the parameters read in property
     * file.
     *
     * @return  null if configuration is missing.
     *
     * @throws  Bp1LdaoException     DOCUMENT ME!
     */
    public static synchronized LDAPConnection getLDAPConnection()
        throws Bp1LdaoException {
        LDAPConnection connection = null;
        if (!configured) {
            try {
                loadProperties();
            } catch (PropertyException ex) {
                logger.error(
                    "Failed to configure LDAP from the property file {} ",
                    LDAP_PROPS_FILE_NAME);
                throw new Bp1LdaoException(ex,
                    "Failed to configure LDAP from the property file "
                    + LDAP_PROPS_FILE_NAME);
            }
        }
        try {
            synchronized (pool) {
            	logger.info("Getting connection from pool...");
                connection = pool.getBoundConnection(principal,
                        credentials.getBytes(LDAP_CREDENTIALS_ENCODING));
            }
        } catch (UnsupportedEncodingException ex) {
            logger.error(
                "Encoding error when creating LDAP connection, connection cofiguration: {}, error={}, ",
                dumpConnectionParams(), ex.getMessage());
            throw new Bp1LdaoException(ex);
        } catch (LDAPException ex) {
            logger.error(
                "LDAP error when creating LDAP connection, connection cofiguration: {}, LDAP error={}, error={}, ",
                new Object[] {
                    dumpConnectionParams(), ex.getLDAPErrorMessage(),
                    ex.getMessage()
                });
            throw new Bp1LdaoException(ex, ex.getLDAPErrorMessage(),
                ex.getResultCode());
        } catch (InterruptedException ex) {
            logger.error(
                "Interruption error when creating LDAP connection, connection cofiguration: {}, error={}, ",
                dumpConnectionParams(), ex.getMessage());
            throw new Bp1LdaoException(ex);
        }

        // if the connection is null, it does not use the pool, create a
        // connection directly to LDAP
        if (connection == null) {
            try {
            	logger.info("Getting connection directly...");
                connection = makeNewLDAPConnection();
                if (logger.isDebugEnabled()) {
                    logger.debug("Created a new LDAP connection");
                }
            } catch (Bp1LdaoException ex) {
                logger.error(
                    "LDAP error when creating LDAP connection, connection cofiguration: {}, error={}, LDAP error code={}",
                    new Object[] {
                        dumpConnectionParams(), ex.getMessage(),
                        ex.getLDAPError()
                    });
                throw ex;
            }
        }

        return connection;
    }

    /**
     * Make this connection available and put it back into the connection pool.
     *
     * @param  conn  the LDAPConnection to be made available.
     */
    public static void makeConnectionAvailable(LDAPConnection conn) {
        synchronized (pool) {
            if (conn != null) {
                pool.makeConnectionAvailable(conn);
            }
        }
    }

    /**
     * Helps if we can not get <code>LDAPConnection</code> from the pool. Then
     * we create new <code>LDAPConnection</code> directly to LDAP server.
     *
     * @return  a <code>LDAPConnection</code>
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    private static LDAPConnection makeNewLDAPConnection()
        throws Bp1LdaoException {
        int ldapVersion = LDAPConnection.LDAP_V3;
        LDAPConnection connection = null;
        try {
            if (secure) {
                LDAPSocketFactory ssf;

                // Dynamically set JSSE as a security provider
                Security.addProvider(
                    new com.sun.net.ssl.internal.ssl.Provider());

                // Dynamically set the property that JSSE uses to identify
                // the key store that holds trusted root certificates
                System.setProperty(LDAP_KEY_STORE, keyPath);
                ssf = new LDAPJSSESecureSocketFactory();

                // Set the socket factory as the default for all future
                // connections
                LDAPConnection.setSocketFactory(ssf);
                connection = new LDAPConnection();
            } else {
                connection = new LDAPConnection();
            }
            connection.connect(host, port);
            connection.bind(ldapVersion, principal,
                credentials.getBytes(LDAP_CREDENTIALS_ENCODING));
        } catch (LDAPException ex) {
            logger.error(
                "LDAP error when creating LDAP connection, connection cofiguration: {}, LDAP error={}, error={}, ",
                new Object[] {
                    dumpConnectionParams(), ex.getLDAPErrorMessage(),
                    ex.getMessage()
                });
            throw new Bp1LdaoException(ex, ex.getMessage(), ex.getResultCode());
        } catch (UnsupportedEncodingException ex) {
            logger.error(
                "Encoding error when creating LDAP connection, connection cofiguration: {}, error={}, ",
                dumpConnectionParams(), ex.getMessage());
            throw new Bp1LdaoException(ex, ex.getMessage());
        }

        return connection;
    }

    /**
     * Retrieves and caches the pre-defined properties in the LDAP server.
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    private static void loadLDAPServerAttributes() throws Bp1LdaoException {
        boolean attributeOnly = false;
        String[] returnedAttributes = { "*", "+" };
        LDAPConnection conn = getLDAPConnection();
        try {
            LDAPSearchResults searchResults = conn.search(
                    "",
                    LDAPConnection.SCOPE_BASE,
                    "(objectclass=*)",
                    returnedAttributes,
                    attributeOnly);

            /*  The search returns one entry in the search results, and
             *  it is the root DSE.
             */
            LDAPEntry entry = null;
            try {
                entry = searchResults.next();
            } catch (LDAPException ex) {
                logger.error(
                    "LDAP error when retrieving server attributes, connection cofiguration: {}, LDAP error={}, error={}, ",
                    new Object[] {
                        dumpConnectionParams(), ex.getLDAPErrorMessage(),
                        ex.getMessage()
                    });
            }
            LDAPAttributeSet attributeSet = entry.getAttributeSet();

            // get the naming context
            namingContexts = getStringValue(attributeSet, NAMING_CONTEXTS);

            // get vendor name
            vendorName = getStringValue(attributeSet, VENDOR_NAME);

            // get version
            vendorVersion = getStringValue(attributeSet, VENDOR_VERSION);

            // get netscapemdsuffix
            netscapemdsuffix = getStringValue(attributeSet, NETSCAPE_MD_SUFFIX);
        } catch (LDAPException ex) {
            logger.error(
                "LDAP error when retrieving server attributes, connection cofiguration: {}, LDAP error={}, error={}, ",
                new Object[] {
                    dumpConnectionParams(), ex.getLDAPErrorMessage(),
                    ex.getMessage()
                });
        }
    }

    /**
     * Wrapper method return the <code>String</code> value of a <code>
     * LDAPAttribute</code>.
     *
     * @param   attributeSet   a set of <code>LDAPAttribute</code>
     * @param   attributeName  name of the <code>LDAPAttribute</code> whose
     *                         value we wish to retrieve.
     *
     * @return  <code>null</code> if <code>LDAPAttributeSet</code> is <code>
     *          null</code> or the specified <code>attributeName</code> provided
     *          is incorrect
     */
    public static String getStringValue(LDAPAttributeSet attributeSet,
        String attributeName) {
        String attributeValue = null;
        if (attributeSet == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("getStringValue() : LDAPAttributeSet is null");
            }
        } else {
            LDAPAttribute attribute = attributeSet.getAttribute(attributeName);
            if (attribute == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug(
                        "getStringValue() : LDAPAttribute with name {} has a null value",
                        attributeName);
                }
            } else {
                attributeValue = attribute.getStringValue();
                if (logger.isDebugEnabled()) {
                    logger.debug(
                        "getStringValue() : LDAPAttribute with name {} has a value {}",
                        attributeName, attributeValue);
                }
            }
        }

        return attributeValue;
    }

    /**
     * Wrapper method return the <code>byte</code> value of a <code>
     * LDAPAttribute</code>.
     *
     * @param   attributeSet   a set of <code>LDAPAttribute</code>
     * @param   attributeName  name of the <code>LDAPAttribute</code> whose
     *                         value we wish to retrieve.
     *
     * @return  zero length array of <code>byte</code> if <code>
     *          LDAPAttributeSet</code> is <code>null</code> or the <code>
     *          attributeName</code> provided is incorrect
     */
    public static byte[] getByteValue(LDAPAttributeSet attributeSet,
        String attributeName) {
        byte[] attributeValue = new byte[0];
        if (attributeSet == null) {
            logger.info("getByteValue : LDAPAttributeSet is null");
        } else {
            LDAPAttribute attribute = attributeSet.getAttribute(attributeName);
            if (attribute == null) {
                logger.info(
                    "getByteValue : LDAPAttribute with name {} has a null value",
                    attributeName);
            } else {
                attributeValue = attribute.getByteValue();
                if (logger.isDebugEnabled()) {
                    logger.debug(
                        "getStringValue() : LDAPAttribute with name {} has a length of {} bytes",
                        attributeName, attributeValue.length);
                }
            }
        }

        return attributeValue;
    }

    /**
     * Return the <code>namingContexts</code> attribute of the LDAP server with
     * default configuration.
     *
     * @return  <code>null</code> if we could not get <code>
     *          namingContexts</code> attribute
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    public static String getNamingContexts() throws Bp1LdaoException {
        if (namingContexts == null) {
            loadLDAPServerAttributes();
        }

        return namingContexts;
    }

    /**
     * Return the <code>netscapemdsuffix</code> attribute of the LDAP server
     * with default configuration.
     *
     * @return  <code>null</code> if we could not get <code>
     *          netscapesuffix</code>
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    public static String getNetscapemdsuffix() throws Bp1LdaoException {
        if (netscapemdsuffix == null) {
            loadLDAPServerAttributes();
        }

        return netscapemdsuffix;
    }

    /**
     * Return the <code>vendorName</code> attribute of the LDAP server with
     * default configuration.
     *
     * @return  <code>null</code> if we could not get <code>vendorName</code>
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    public static String getVendorName() throws Bp1LdaoException {
        if (vendorName == null) {
            loadLDAPServerAttributes();
        }

        return vendorName;
    }

    /**
     * Return the <code>vendorVersion</code> attribute of the LDAP server with
     * default configuration.
     *
     * @return  <code>null</code> if we could not get <code>vendorVersion</code>
     *
     * @throws  Bp1LdaoException  if an error occurs when communicating or
     *                            connecting with LDAP.
     */
    public static String getVendorVersion() throws Bp1LdaoException {
        if (vendorVersion == null) {
            loadLDAPServerAttributes();
        }

        return vendorVersion;
    }

    /**
     * Extacts and returns the LDAP common name (CN) from the specified LDAP DN.
     *
     * @param   ldapDN  the LDAP DN.
     *
     * @return  the extracted common name (CN) or <code>null</code> if the
     *          specified LDAP DN was <code>null</code> or does not contain the
     *          string "cn|CN".
     */
    public static String getCN(String ldapDN) {
        String ldapCN = null;
        if (ldapDN != null) {
            int cnStartPos = ldapDN.indexOf("cn=");
            if (cnStartPos == -1) {
                cnStartPos = ldapDN.indexOf("CN=");
            }
            if (cnStartPos != -1) {
                cnStartPos += 3;
                int firstCommaPos = ldapDN.indexOf(",");
                if (firstCommaPos != -1) {
                    ldapCN = ldapDN.substring(cnStartPos, firstCommaPos);
                } else {
                    ldapCN = ldapDN.substring(cnStartPos);
                }
            }
        }

        return ldapCN;
    }

    /**
     * Dumps the LDAP connections parameters into a string. Useful for debugging
     * connection problems.
     *
     * @return  LDAP connections parameters as a string.
     */
    public static String dumpConnectionParams() {
        StringBuffer sb = new StringBuffer();
        if (vendorName != null) {
            sb.append("vendorName=").append(vendorName).append(", ");
        }
        if (vendorVersion != null) {
            sb.append("vendorVersion=").append(vendorVersion).append(", ");
        }
        sb.append("namingContexts=").append(namingContexts).append(", ");
        sb.append("principal=").append(principal).append(", ");
        sb.append("credentials=").append("********").append(", ");
        sb.append("encoding=").append(LDAP_CREDENTIALS_ENCODING).append(", ");
        sb.append("host=").append(host).append(", ");
        sb.append("port=").append(port).append(", ");
        sb.append("maxConnections=").append(maxConnections).append(", ");
        sb.append("maxSharedConnections=").append(maxSharedConnections);

        return sb.toString();
    }

    /**
     * Return a <code>String</code> based representation of an <code>
     * LDAPUtil</code> class.
     *
     * @return  name and connection parameters of the <code>LDAPUtil</code>
     *          class.
     */
    @Override public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(LDAPUtil.class.getName());
        sb.append(" [ ");
        sb.append(LDAPUtil.dumpConnectionParams());
        sb.append(" ]");

        return sb.toString();
    }
}