package com.bp.pensionline.authenticate.util;

import java.sql.Date;
import java.text.SimpleDateFormat;

import com.bp.pensionline.authenticate.Constant;

public class DateUtil {
	public static String formatDate(String sdate) {
		String result = null;
		
		try {
			Date date = Date.valueOf(sdate);
			SimpleDateFormat sdf = 
				new SimpleDateFormat(Constant.FORMAT_DATE_DDMMMYY);
			result = sdf.format(date);
			System.out.println("Result: " + result);
		} catch (Exception ex){
			ex.printStackTrace();
			result = new Date(System.currentTimeMillis()).toString();
		}
		
		return result;
	}
	
//	public static void main(String[] args) {
//		String s = 2009 + "-" + 11 + "-" + 31;
//		java.sql.Date d = java.sql.Date.valueOf(s);
//	    SimpleDateFormat sdf;
//	    
//	    sdf = new SimpleDateFormat("dd-MMM-yy");
//	    System.out.println(sdf.format(d));
//	}
}
