package com.bp.pensionline.authenticate.util;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsContextInfo;
import org.opencms.main.CmsSessionInfo;
import org.opencms.main.CmsSessionManager;
import org.opencms.main.OpenCms;
import org.opencms.security.CmsOrgUnitManager;
import org.opencms.util.CmsUUID;

import com.bp.pensionline.authenticate.UserInfo;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.util.SystemAccount;

public class AuthenticationUtil {
	static Logger LOG = Logger.getLogger(AuthenticationUtil.class);
	
	private static CmsObject adminObj = null;
	public static final String webUnitName = 
		CheckConfigurationKey.getStringValue("webuser.unitname") + "/";
	
	public static final String USER_ADMIN_NAME = "adminName";
	public static final String USER_ADMIN_PASSWORD = "adminPassword";
	
	public static final String USER_SPECIAL_ADMIN_NAME = "Admin";
	public static final String USER_SPECIAL_SUPERUSER_NAME = "superuser";
	public static final String USER_SPECIAL_CMG_NAME = "tu.nguyen";
	
	public static final String LDAP_FILE_PROPERTIES = "ldap.properties";
	
	public static Properties getLdapProperties() {
        Properties pros = null;
        try {
            pros = PropertyLoader.loadProperties(
                    LDAP_FILE_PROPERTIES);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return pros;
    }
	
	public static boolean isSpecialUser(String username) {
		if (username != null) {
			if ((username.trim().equals(USER_SPECIAL_ADMIN_NAME))
					|| (username.trim().equals(USER_SPECIAL_SUPERUSER_NAME))
					|| (username.trim().equals(USER_SPECIAL_CMG_NAME)))
			return true;
		}
		
		return false;
	}

	public static void unswap(String username, String group, CmsObject obj) {
		try {
			if (obj.userInGroup(username, group)) {
				obj.removeUserFromGroup(username, group);
			}
		} catch (Exception ex) {
			System.out.println("Cannot unswap the group BP1_User_group for " + username);
		}
	}

	public static boolean isWebuser(String username) {
		CmsUser user = SystemAccount.getWebuserByName(username);				
		if(user != null 
				&& user.isWebuser() 
				&& user.getDescription() != null 
				&& !user.getDescription().equals("")) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Create CmsObject
	 * @return a CmsObject
	 */
	public static synchronized CmsObject getAdminCmsObject() {
		try	{
			if(adminObj == null) {
				String adminUsername = 
					CheckConfigurationKey.getStringValue(USER_ADMIN_NAME);
				String adminPassword = 
					CheckConfigurationKey.getStringValue(USER_ADMIN_PASSWORD);				
				
				CmsObject newAdminObj = 
					OpenCms.initCmsObject(OpenCms.getDefaultUsers()
							.getUserGuest());	
				String adminLoggedInName = 
					newAdminObj.loginUser(adminUsername, 
							adminPassword, CmsContextInfo.LOCALHOST);
				
				if (adminLoggedInName != null 
						&& adminLoggedInName.equals(adminUsername))	{
					adminObj = newAdminObj;					
				}
			}
			
			adminObj.getRequestContext().setCurrentProject(adminObj.readProject("Offline"));
			adminObj.getRequestContext().setSiteRoot("/");
		}
		catch (Exception e)	{
			LOG.error("System cannot initialize CmsObject, error detail: " + e.getMessage());
		}
		return adminObj;
	}
	
	public static boolean createWebUser(String name, String password,
			String group, String description, 
			Map additionalInfos) {
		boolean result = false;
		CmsObject cmo = getAdminCmsObject();
		
		try	{
			CmsUser cmu = cmo.createUser(name, password, description, additionalInfos);
			if (cmu != null) {
				cmu.setFirstname("FirstName");
				cmu.setLastname("LastName");
				cmu.setEmail("test@mail.com");
				cmu.setFlags(32768);
				cmo.writeUser(cmu);
				
				CmsOrgUnitManager unitManager = OpenCms.getOrgUnitManager();
				unitManager.setUsersOrganizationalUnit(cmo, webUnitName, cmu.getName());
				cmo.addUserToGroup(webUnitName + cmu.getName(), "Members");
				result = true;
			}
		}
		catch (Exception e)	{
			LOG.error("System cannot create user - " + name 
					+ ", error detail: " + e.getMessage());
		}

		return result;
	}
	
	public static boolean checkWebuserExist(String username) {
		username = webUnitName + username;
		boolean result = false;
		
		CmsUser user = null;
		try	{
			CmsObject obj = getAdminCmsObject();
			user = obj.readUser(username);
			
			if (user != null) {
				result = true;
			}
		}
		catch (Exception e)	{
			LOG.warn("System cannot find the user '" + username + "'");
		}
		
		return result;
	}
	
	public static CmsUser getWebuserExist(String username) {
		username = webUnitName + username;
		
		CmsUser user = null;
		try	{
			CmsObject obj = getAdminCmsObject();
			user = obj.readUser(username);
		}
		catch (Exception e)	{
			LOG.warn("System cannot find the user '" + username + "'");
		}
		
		return user;
	}
	
	public static boolean checkUserExist(String username)
	{
		boolean result = false;
		
		CmsUser user = null;
		try	{
			CmsObject obj = getAdminCmsObject();
			user = obj.readUser(username);
			
			if (user != null) {
				result = true;
			}
		}
		catch (Exception e)	{
			LOG.warn("System cannot find the user '" + username + "'");
		}
		
		return result;
	}
	
	public static CmsUser getCurrentUser(HttpServletRequest request) {
		CmsSessionManager man = OpenCms.getSessionManager();
		CmsUser currentUser = null;
		try {
			CmsSessionInfo info = man.getSessionInfo(request);
			CmsUUID currentUserId = info.getUserId();
			
			LOG.info("Try to get current user: " 
					+ currentUserId==null ? "NULL" : currentUserId.toString());
			
			currentUser = getAdminCmsObject().readUser(currentUserId);
		} catch (Exception ex) {
			LOG.error("System cannot get current user, error detail: " 
					+ ex.getMessage());
		}
		
		return currentUser;
	}
	
	public static void logout(HttpServletRequest request) throws Exception
	{
		HttpSession session = request.getSession(false);
		
		if (session != null)
		{
			session.invalidate();
		}
	}
	
	 public static UserInfo getRefNo(List<UserInfo> items) {
	    	UserInfo result = null;
	    	
	    	try {
	    		if ((items != null) && (items.size() > 0)) {
	    			// AC
	    			for (UserInfo item : items) {
	    				if (item.getStatus().trim().toUpperCase().equals("AC")) {
	    					result = item;
	    					break;
	    				}
	    			}
	    			
	    			// PN
	    			if ((result == null) || (result.equals(""))) {
	    				for (UserInfo item : items) {
	        				if (item.getStatus().trim().toUpperCase().equals("PN")) {
	        					result = item;
	        					break;
	        				}
	        			}
	    			}
	    			
	    			
	    			// PP
	    			if ((result == null) || (result.equals(""))) {
	    				for (UserInfo item : items) {
	        				if (item.getStatus().trim().toUpperCase().equals("PP")) {
	        					result = item;
	        					break;
	        				}
	        			}
	    			}
	    			
	    			// WB
	    			if ((result == null) || (result.equals(""))) {
	    				for (UserInfo item : items) {
	        				if (item.getStatus().trim().toUpperCase().equals("WB")) {
	        					result = item;
	        					break;
	        				}
	        			}
	    			}
	    			
	    			// CB
	    			if ((result == null) || (result.equals(""))) {
	    				for (UserInfo item : items) {
	        				if (item.getStatus().trim().toUpperCase().equals("CB")) {
	        					result = item;
	        					break;
	        				}
	        			}
	    			}
	    			
	    			if ((result == null) || (result.equals(""))) {
	    				result = items.get(0);
	    			}
	    		} // end-if
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	    	}
	    	
	    	return result;
	    }
}
