package com.bp.pensionline.authenticate.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.AuthObject;
import com.bp.pensionline.authenticate.AuthenticationFactory;
import com.bp.pensionline.authenticate.database.ASDDataAccess;
import com.bp.pensionline.authenticate.exception.DAOException;
import com.bp.pensionline.authenticate.service.PLSelfService;
import com.bp.pensionline.database.MigrationDataHandler;
import com.bp.pensionline.handler.DataMigrationHandler;

public class AuthenticationHelper {
	
	static Logger LOG = Logger.getLogger(AuthenticationHelper.class);
	public static void alterSession() {
		
	}
	/**
	 * This method gets object info on the Administrator database 
	 * for the specific user
	 * @return AuthObject object
	 * @param bp1Name the bp1 user name
	 */
	public static AuthObject getAuthObject(String bp1Name) throws DAOException {
		AuthObject asdObj = null;
		
		try {
			ASDDataAccess asdDao = new ASDDataAccess();
			asdObj = asdDao.getBP1Record(bp1Name);
		} catch (DAOException de) {
			LOG.warn("Data access exception, error detail: "
					+ de.getMessage());
			throw new DAOException(de.getMessage());
		} catch (Exception ex) {
			LOG.error("Failed to check bp1 user in"
					+ " the Administrator database for bp1 name '"
					+ bp1Name + "': "
					+ ex.getMessage());
		}
		
		return asdObj;
	}
	
	/**
	 * This method checks if the given LDAP user name 
	 * exists in table ADD_STATIC_DATA [table in Oracle Administrator database]
	 * @return true if the name exists, otherwise false value thrown
	 * @param bp1Name the bp1 user name
	 */
	public static boolean checkBP1UserExist(String bp1Name, AuthObject asdObj) 
								throws DAOException {
		boolean rt = false;
		
		try {		
			if ((asdObj != null) && (asdObj.getBp1Id() != null)
					&& (!asdObj.getBp1Id().trim().equals(""))) {
				rt = true;
			} // end-if
		} catch (Exception ex) {
			LOG.error("Failed to check bp1 user in"
					+ " the Administrator database for bp1 name '"
					+ bp1Name + "': "
					+ ex.getMessage());
		}
		
		return rt;
	}
	
	public static boolean checkBP1MultiRows(String bp1Name) 
								throws DAOException {
		boolean rt = false;
		
		try {
			ASDDataAccess dao = new ASDDataAccess();
			return dao.hasMultiRows(bp1Name);
		} catch (Exception ex) {
			LOG.error("Failed to check bp1 multi-rows user in"
			+ " the Administrator database for bp1 name '"
			+ bp1Name + "': "
			+ ex.getMessage());
		}
		
		return rt;
	}
	
	/**
	 * This method helps to check if a NINO 
	 * exists in table BASIC inner join table ADD_STATIC_DATA or not
	 * @param asdObj the ASD Object
	 * @return the NINO value
	 */
	public static String getNINOInASD(AuthObject asdObj) {
		String nino = null;
		
		try {	
			if ((asdObj != null) && (asdObj.getNino() != null)
					&& (!asdObj.getNino().trim().equals(""))) {
				nino = asdObj.getNino().trim();
			}
		} catch (Exception ex) {
			LOG.error("Failed to check NINO in"
					+ " the Administrator database: "
					+ ex.getMessage());
		}
		
		return nino;
	}
	
	public static String getPLIDInASD(AuthObject asdObj) {
		String nino = null;
		
		try {	
			if ((asdObj != null) && (asdObj.getPlId() != null)
					&& (!asdObj.getPlId().trim().equals(""))) {
				nino = asdObj.getNino().trim();
			}
		} catch (Exception ex) {
			LOG.error("Failed to check PL ID in"
					+ " the Administrator database: "
					+ ex.getMessage());
		}
		
		return nino;
	}
	
	/**
	 * This method calls method checkUserAuthenticate(userName) in
	 * class <code>MigrationDataHandler</code>
	 * @param username the user name in format of a NINO
	 * @return true if the NINO value is existed in table 
	 * WE_AUTHENTICATE
	 * @throws DAOException the authentication exception
	 * that indicating if DAO access fails
	 * @throws Exception
	 */
	public static boolean checkNINOWeAuthenticate(String username) 
							throws DAOException, Exception {
		return (new MigrationDataHandler()).checkUserAuthenticate(username);
	}
	
	public static String correctBp1Name(String bp1Name) {
		String rt = null;
		
		try {
			bp1Name = bp1Name.toLowerCase();
			bp1Name = StringUtil.escape(bp1Name);
			int index = bp1Name.indexOf("\\");
			if (index == -1) {
				return bp1Name;
			} // end-if
			
			LOG.info("Start correct BP1 name - " + bp1Name + "...");
			
			rt = bp1Name.substring(index + 1);
			LOG.info("End correct BP1 name - " + rt + "!");
		} catch (Exception ex) {
			LOG.error("Failed to correct BP1 name '" 
					+ bp1Name 
					+ "', detail: " 
					+ ex.getMessage());
		}
		
		return rt;
	}
	
	public static boolean startWithBp1(String username) {
		boolean rt = false;
		
		try {
			String originalName = username;
			username = username.toLowerCase();
			username = StringUtil.escape(username);
			if (username.startsWith("bp1")) {
				LOG.info("The BP1 name - " + originalName + " - start with Bp1");
				return true;
			} // end-if
		} catch (Exception ex) {
			LOG.error("Failed to check start with BP1 for - " 
					+ username 
					+ ", detail: " 
					+ ex.getMessage());
		}
		
		return rt;
	}
	
	public static boolean isBP1User(String username) {
		boolean rt = false;
		
		try {
			String loginName = username.toLowerCase();
			
			// Removes special characters
			loginName = StringUtil.escape(loginName);
			
            if (loginName.startsWith(AuthenticationFactory.BP1_START)) {
            	return true;
            } // end-if
            
            loginName = correctBp1Name(loginName);
            if (match_BP1(loginName)) {
            	rt = true;
            }
            
            if (username.length() == 6) {
            	rt = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "isBP1User(String username): "
                + ex.getMessage());
        }
        
		return rt;
	}
	
	public static boolean isNINOUser(String username) {
		boolean rt = false;
		
		try {
			String loginName = username.toLowerCase();
			
			// Removes special characters
			loginName = StringUtil.escape(loginName);
            
            if (match_NINO(loginName)) {
            	rt = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "isNINOUser(String username): "
                + ex.getMessage());
        }
        
		return rt;
	}
	
	public static boolean isStandardCMSUser(String username) {
		boolean rt = false;
		
		try {
			String loginName = username.toLowerCase();
			
			// Removes special characters
			loginName = StringUtil.escape(loginName);
            
            if (match_CMS(loginName)) {
            	rt = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "isStandardCMSUser(String username): "
                + ex.getMessage());
        }
        
		return rt;
	}
	
	public static boolean match_BP1(String username) {
		boolean matching = false;
		
		try {
            Pattern pattern = Pattern.compile(AuthenticationFactory.USERNAME_REGEX_BP1);
            Matcher matcher = pattern.matcher(username);
            while (matcher.find()) {
            	matching = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "match_BP1(String username): "
                + ex.getMessage());
        }
        
		return matching;
	}
	
	public static boolean match_NINO(String username) {
		boolean matching = false;
		
		try {
            Pattern pattern = Pattern.compile(AuthenticationFactory.USERNAME_REGEX_NINO);
            Matcher matcher = pattern.matcher(username);
            while (matcher.find()) {
            	matching = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "match_NINO(String username): "
                + ex.getMessage());
        }
        
		return matching;
	}
	
	public static boolean isBlankPassword(String username, String password) {
		boolean matching = false;
		
		if ((password == null) || (password.trim().length() <= 0)) {
			LOG.error("LDAP password is null or blank for user " + username);
			matching = true;
		}
        
		return matching;
	}
	
	public static boolean match_CMS(String username) {
		boolean matching = false;
		
		try {
            Pattern pattern = Pattern.compile(AuthenticationFactory.USERNAME_REGEX_CMS);
            Matcher matcher = pattern.matcher(username);
            while (matcher.find()) {
            	matching = true;
            }
        } catch (Exception ex) {
        	LOG.error("Error in AuthenticationHelper::"
            	+ "match_NINO(String username): "
                + ex.getMessage());
        }
        
		return matching;
	}
	
	/**
	 * Migrates user with PL ID entered that link to a NINO
	 * @param username the PL ID
	 * @param password the password entered by user
	 * @return true if a web user found
	 */
	public static boolean migrateUserDetails(String username, String password) {
		boolean result = false;
		
		try {
			PLSelfService service = new PLSelfService();
			String ninoName = service.getNINOForPLID(username);
			if ((ninoName != null) && (!ninoName.equals(""))) {
				ninoName = ninoName.toLowerCase().trim();
				
				LOG.info("Migrating PL ID for NINO " + ninoName);
				DataMigrationHandler handler=new DataMigrationHandler();
				result = handler.migrateUserDetails(username, password, ninoName);
			}
		} catch (Exception ex) {
			LOG.error("Error in AuthenticationHelper: " 
					+ ex.getMessage());
		}
		
		return result;
	}
}
