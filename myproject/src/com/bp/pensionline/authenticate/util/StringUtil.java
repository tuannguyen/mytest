package com.bp.pensionline.authenticate.util;

import java.util.List;

import com.bp.pensionline.authenticate.RefNoData;

public class StringUtil {
	/**
     * documents description.
     *
     * @param   inputStr  the parameter
     *
     * @return  the return value
     */
    public static String escape(String inputStr) {
        if (inputStr != null) {
            inputStr = inputStr.replace("\b", "\\b");
            inputStr = inputStr.replace("\f", "\\f");
            inputStr = inputStr.replace("\n", "\\n");
            inputStr = inputStr.replace("\r", "\\r");
            inputStr = inputStr.replace("\t", "\\t");
            inputStr = inputStr.replace("\t", "\\t");
        }

        return inputStr;
    }
    
    public static String removeWhileSpace(String inputStr) {
    	String result = "";
    	if ((inputStr == null) || (inputStr.trim().equals(""))) {
    		return result;
    	}
    	
    	return result = inputStr.replaceAll("\\s", "");
    }
    
    public static String getRefNoFromList(List<RefNoData> items) {
    	String result = null;
    	
    	try {
    		if ((items != null) && (items.size() > 0)) {
    			// AC
    			for (RefNoData item : items) {
    				if (item.getStatus().trim().toUpperCase().equals("AC")) {
    					result = item.getRefno();
    					break;
    				}
    			}
    			
    			// PN
    			if ((result == null) || (result.equals(""))) {
    				for (RefNoData item : items) {
        				if (item.getStatus().trim().toUpperCase().equals("PN")) {
        					result = item.getRefno();
        					break;
        				}
        			}
    			}
    			
    			
    			// PP
    			if ((result == null) || (result.equals(""))) {
    				for (RefNoData item : items) {
        				if (item.getStatus().trim().toUpperCase().equals("PP")) {
        					result = item.getRefno();
        					break;
        				}
        			}
    			}
    			
    			// WB
    			if ((result == null) || (result.equals(""))) {
    				for (RefNoData item : items) {
        				if (item.getStatus().trim().toUpperCase().equals("WB")) {
        					result = item.getRefno();
        					break;
        				}
        			}
    			}
    			
    			// CB
    			if ((result == null) || (result.equals(""))) {
    				for (RefNoData item : items) {
        				if (item.getStatus().trim().toUpperCase().equals("CB")) {
        					result = item.getRefno();
        					break;
        				}
        			}
    			}
    			
    			if ((result == null) || (result.equals(""))) {
    				result = items.get(0).getRefno();
    			}
    		} // end-if
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
    	return result;
    }
}
