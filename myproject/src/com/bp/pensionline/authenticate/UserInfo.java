package com.bp.pensionline.authenticate;

import java.io.Serializable;

public class UserInfo implements Serializable {
	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1L;
	private String ninoName;
	private String dateOfBirth;
	private String postCode;
	private String refno;
	private String bgroup;
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBgroup() {
		return bgroup;
	}
	public void setBgroup(String bgroup) {
		this.bgroup = bgroup;
	}
	public String getRefno() {
		return refno;
	}
	public void setRefno(String refno) {
		this.refno = refno;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	private String firstName;
	private String lastName;
	private String userId;
	
	public String getNinoName() {
		return ninoName;
	}
	public void setNinoName(String ninoName) {
		this.ninoName = ninoName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("NinoName = " + ninoName);
		sb.append(", DateOfBirth = " + dateOfBirth);
		sb.append(", PostCode = " + postCode);
		if (refno != null) {
			sb.append(", RefNo = " + refno);
		}
		if (firstName != null) {
			sb.append(", FirstName = " + firstName);
		}
		if (lastName != null) {
			sb.append(", LastName = " + lastName);
		}
		sb.append("]");
		return sb.toString();
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
}
