package com.bp.pensionline.authenticate.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.ASD;
import com.bp.pensionline.authenticate.Constant;
import com.bp.pensionline.authenticate.RefNoData;
import com.bp.pensionline.authenticate.UserInfo;
import com.bp.pensionline.authenticate.exception.DAOException;
import com.bp.pensionline.authenticate.util.AuthenticationUtil;
import com.bp.pensionline.authenticate.util.StringUtil;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

/**
 * 
 * @author 	Binh Nguyen
 * @version	1.0.5 May 11, 2010
 */
public class SelfServiceDataAccess {
	static Logger LOG = Logger.getLogger(SelfServiceDataAccess.class);
	
	private static final String SQL_SELECT_CHECK_MIGRATED = ""
		+ " SELECT DISTINCT ASD.ASD58X, B.REFNO, B.BD19A FROM ADD_STATIC_DATA ASD"
		+ " INNER JOIN BASIC B ON B.REFNO = ASD.REFNO"
		+ " WHERE B.BGROUP = ASD.BGROUP AND"
		+ " B.BD08X = UPPER(?)";
	private static final String SQL_UPDATE_MIGRATION = ""
		+ "UPDATE ADD_STATIC_DATA SET ASD58X = ?"
		+ " WHERE DATA_TYPE = 'GENINFO' AND REFNO = ?"
		+ " AND BGROUP = ?";
	
	private static final String SQL_GET_USERID = ""
		+ "SELECT DISTINCT NVL(ASD58X, ?) AS USERID,"
		+ " PAD.POSTCODE FROM ADD_STATIC_DATA ASD"
		+ " INNER JOIN BASIC B ON ASD.REFNO = B.REFNO"
		+ " INNER JOIN WE_AUTHENTICATE W"
		+ " ON B.BD08X = UPPER(W.EXTUSERID)"
		+ " INNER JOIN PS_ADDRESS PA ON B.REFNO = PA.REFNO"
		+ " INNER JOIN PS_ADDRESSDETAILS PAD ON PA.ADDNO = PAD.ADDNO"
		+ " WHERE ASD.BGROUP = B.BGROUP AND ASD.DATA_TYPE = 'GENINFO'"
		+ " AND B.BGROUP = PA.BGROUP AND PAD.BGROUP = B.BGROUP"
		+ " AND BD19A != 'NL'"
		// Update by Huy Tran to fix the issue of REPORTING-1570 that the check needs to be updated to only search through addresses that have an end_date of NULL.
		+ " AND PA.ENDD is NULL"		
		// End of Update		
		+ " AND B.BGROUP = PA.BGROUP AND B.BD08X = ?"
		+ " AND TO_DATE(B.BD11D, '" 
		+ Constant.FORMAT_DATE_ORA_DDMMMYY
		+ "') = TO_DATE(?, '" 
		+ Constant.FORMAT_DATE_ORA_DDMMMYY 
		+ "')";
	private static final String SQL_GET_BGROUP = ""
		+ "SELECT BGROUP FROM BASIC WHERE BD19A != 'NL' AND BGROUP != 'SPF'"
		+ " AND BD08X = UPPER(?) AND REFNO = ?";
	
	private static final String SQL_GET_USER_PASSWORD = ""
		+ "SELECT DISTINCT NVL(ASD58X, ?) AS USERID,"
		+ " B.REFNO, B.BD05A AS FIRSTNAME, B.BD38A AS SURNAME,"
		+ " PAD.POSTCODE, B.BGROUP, BD19A FROM ADD_STATIC_DATA ASD"
		+ " INNER JOIN BASIC B ON ASD.REFNO = B.REFNO"
		+ " INNER JOIN WE_AUTHENTICATE W"
		+ " ON B.BD08X = UPPER(W.EXTUSERID)"
		+ " INNER JOIN PS_ADDRESS PA ON B.REFNO = PA.REFNO"
		+ " INNER JOIN PS_ADDRESSDETAILS PAD ON PA.ADDNO = PAD.ADDNO"
		+ " WHERE ASD.BGROUP = B.BGROUP AND ASD.DATA_TYPE = 'GENINFO'"
		+ " AND B.BGROUP = PA.BGROUP AND PAD.BGROUP = B.BGROUP"
		+ " AND BD19A != 'NL'"
// Update by Huy Tran to fix the issue of REPORTING-1570 that the check needs to be updated to only search through addresses that have an end_date of NULL.
		+ " AND PA.ENDD is NULL"		
// End of Update		
		+ " AND B.BGROUP = PA.BGROUP AND B.BD08X = UPPER(?)"
		+ " AND TO_DATE(B.BD11D, '" 
		+ Constant.FORMAT_DATE_ORA_DDMMMYY
		+ "') = TO_DATE(?, '" 
		+ Constant.FORMAT_DATE_ORA_DDMMMYY 
		+ "')";
	
	private static final String SQL_CHECK_WELCOME_PACK = ""
		+ "SELECT PASSWORD FROM WE_AUTHENTICATE"
		+ " WHERE UPPER(EXTUSERID) = ?"
		+ " AND PASSWORD IS NOT NULL"
		+ " AND LENGTH(PASSWORD) > 0 AND PASSWORD <> 'Null'";
	
	private static final String SQL_GET_REFNO = ""
		+ "SELECT REFNO, BD19A FROM BASIC WHERE BD08X=? AND BD19A != 'NL'";
	
	/**
	 * Checks if the 
	 * @param bp1Name
	 * @return
	 * @throws DAOException
	 */
	public boolean isMigrated(String ninoName) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		boolean result = false;
		try {
			LOG.info("Start check for self-service migrated"
					+ " - " + ninoName);
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm = con.prepareStatement(SQL_SELECT_CHECK_MIGRATED);
			pstm.setString(1, ninoName);
			rs = pstm.executeQuery();
			
			// Checks record
			String pl_id = null;
			if (rs.next()) {;
				pl_id = rs.getString(1);
			} // end-while

			String yn = "NO";
			if ((pl_id != null) && (pl_id.trim().length() > 0)) {
				result = true;
				yn = "YES";
				LOG.info("The NINO '" + ninoName + "' has been migrated.");
			} // end-if
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();		
			LOG.info("End check for self-service migrated - "
					+ ninoName + " - " + yn + " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public String getREFNOForMigrated(String ninoName) throws DAOException {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		String result = null;
		try {
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm=con.prepareStatement(SQL_SELECT_CHECK_MIGRATED);
			pstm.setString(1, ninoName);
			rs = pstm.executeQuery();
			
			// Checks record
			List<RefNoData> list = new ArrayList<RefNoData>();
			RefNoData data = null;
			while (rs.next()) {
				data = new RefNoData();
				data.setRefno(rs.getString(2));
				data.setStatus(rs.getString(3));
				list.add(data);
			} // end-while
			
			result = StringUtil.getRefNoFromList(list);
			if (result != null) {
				LOG.info("Refno has been found: " + result);
			}
			
			// Close result set and statement
			rs.close();
			pstm.close();
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public List<String> getBgroups(String ninoName, String refno, Connection con) throws DAOException {
		ResultSet rs = null;
		PreparedStatement pstm = null;
		List<String> result = new ArrayList<String>();
		try {		
			pstm = con.prepareStatement(SQL_GET_BGROUP);
			pstm.setString(1, ninoName);
			pstm.setString(2, refno);
			rs = pstm.executeQuery();
			
			// Checks record
			if (rs.next()) {;
				result.add(rs.getString(1));
				LOG.info("Bgroup: " + rs.getString(1));
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} // end-try-catch
		
		return result;
	}

	public void updateMigration(ASD asd, String ninoName) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			LOG.info("Start update NINO self-service migration process"
					+ " - " + asd.getPlId());
			DBConnector connector = DBConnector.getInstance();
			
			conn = connector.getDBConnFactory(Environment.AQUILA);
			
			if (conn == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
		
			// Change session to compatible mode
			SessionDataAccess.alterSession(conn);
			int rows = 0;
			List<String> bgroupList = getBgroups(ninoName, asd.getRefno().trim(), conn);
			if ((bgroupList != null) && (bgroupList.size() > 0)) {
				for (String g : bgroupList) {
					stmt = conn.prepareStatement(SQL_UPDATE_MIGRATION);
					
					stmt.setString(1, asd.getPlId().trim());
					stmt.setString(2, asd.getRefno().trim());
					stmt.setString(3, g.trim());
				
					rows += stmt.executeUpdate();
				}
			} else {
				LOG.info("No BGROUP found for PL ID: " + asd.getPlId().trim());
			}
			
			

			long t2 = System.currentTimeMillis();
			LOG.info("End update NINO self-service migration process, " 
					+  rows 
					+ " rows affected (" + (t2-t1) + " ms)");
		}
		catch (Exception _e) {
			LOG.error( "Exception: " + _e.getMessage(), _e );
			throw new DAOException("Exception: " + _e.getMessage());
		}
		finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (conn != null) {
					connector.close(conn);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
	}
	
	public String getUserID(UserInfo info) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		String result = null;
		try {
			LOG.info("Start get UserID"
					+ " - " + info.toString());
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm  =con.prepareStatement(SQL_GET_USERID);
			pstm.setString(1, info.getNinoName());
			pstm.setString(2, info.getNinoName().toUpperCase());
			pstm.setString(3, info.getDateOfBirth());
			rs = pstm.executeQuery();
			
			// Checks record
			String temp = "";
			while (rs.next()) {
				temp = rs.getString(2);
				
				if (temp != null) {
					temp = temp.toLowerCase().trim();
					temp = StringUtil.removeWhileSpace(temp);
				} else {
					temp = "";
				}
				
				String pc = StringUtil.removeWhileSpace(
						info.getPostCode().toLowerCase().trim());
				
				if (temp.equals(pc)) {
					result = rs.getString(1);
					LOG.info("UserID found - " + result);
					break;
				}
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();		
			LOG.info("End get UserID - "
					+ info.toString() + " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			LOG.error("Error in getting My User ID, detail: " + ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public UserInfo getUserInfo(UserInfo info) throws DAOException {
		long t1 = System.currentTimeMillis();
		UserInfo result = null;
		
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;

		try {
			LOG.info("Start get User Info"
					+ " - " + info.toString());
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm = con.prepareStatement(SQL_GET_USER_PASSWORD);
			pstm.setString(1, info.getNinoName());
			pstm.setString(2, info.getNinoName());
			pstm.setString(3, info.getDateOfBirth());
			rs = pstm.executeQuery();
			
			// Checks record
			String temp = "";
			List<UserInfo> list = new ArrayList<UserInfo>();
			while (rs.next()) {
				temp = rs.getString(5);

				if (temp != null) {
					temp = temp.toLowerCase().trim();
					temp = StringUtil.removeWhileSpace(temp);
				} else {
					temp = "";
				}
				
				String pc = StringUtil.removeWhileSpace(
						info.getPostCode().toLowerCase().trim());
				
				if (temp.equals(pc)) {
					result = new UserInfo();
					
					result.setUserId(rs.getString(1));
					result.setRefno(rs.getString(2));
					result.setFirstName(rs.getString(3));
					result.setLastName(rs.getString(4));
					result.setPostCode(rs.getString(5));
					result.setBgroup(rs.getString(6));
					result.setStatus(rs.getString(7));
					
					// Last part
					result.setNinoName(info.getNinoName());
					result.setDateOfBirth(info.getDateOfBirth());

					LOG.info("Found a row with status - " + result.getStatus());
					list.add(result);
				}
			} // end-while
			
			// Checks for correct RefNo
			if ((list != null) && (list.size() > 0)) {
				result = AuthenticationUtil.getRefNo(list);
			}
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();		
			LOG.info("End get User Info - "
					+ info.toString() + " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public boolean hasWelcomePack(String ninoName) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		boolean result = false;
		try {
			LOG.info("Start check for existing welcome pack"
					+ " - " + ninoName);
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm = con.prepareStatement(SQL_CHECK_WELCOME_PACK);
			pstm.setString(1, ninoName.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			String yn = "NO";
			if (rs.next()) {;
				result = true;
				yn = "YES";
				LOG.info("The NINO '" + ninoName + "' has a welcome pack.");
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();		
			LOG.info("End check for existing welcome pack - "
					+ ninoName + " - " + yn + " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public String getRefNo(String ninoName) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		String result = null;
		try {
			LOG.info("Start get RefNo"
					+ " - " + ninoName);
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm = con.prepareStatement(SQL_GET_REFNO);
			pstm.setString(1, ninoName.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			String yn = "NO";
			List<RefNoData> list = new ArrayList<RefNoData>();
			RefNoData data = null;
			if (rs.next()) {;
				data = new RefNoData();
				data.setRefno(rs.getString(1));
				data.setStatus(rs.getString(2));
				list.add(data);
			} // end-while
			
			result = StringUtil.getRefNoFromList(list);
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();		
			LOG.info("End get RefNo - "
					+ ninoName + " - " + yn + " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
}
