package com.bp.pensionline.authenticate.database;

import java.sql.Connection;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class SessionDataAccess {
	static final Logger LOG = Logger.getLogger(SessionDataAccess.class);
	private static final String SQL_SESSION_SORT = 
		"ALTER SESSION SET NLS_SORT ='BINARY_CI'";
	private static final String SQL_SESSION_COMP = 
		"ALTER SESSION SET NLS_COMP='LINGUISTIC'";
	private static final String SQL_WEST_EUROPEAN_CI = 
		"ALTER SESSION SET NLS_SORT='WEST_EUROPEAN_CI'";
	
	public static void alterSession(Connection con) {
		try {
			//Statement stm = con.createStatement();
			//stm.executeUpdate(SQL_SESSION_SORT);
			//stm.executeUpdate(SQL_SESSION_COMP);
			//stm.executeUpdate(SQL_WEST_EUROPEAN_CI);
			
			//stm.close();
			// Do nothing for current purposes
		} catch (Exception ex) {
			LOG.error("Error in changing session: " + ex.getMessage());
		} 
	}
}
