package com.bp.pensionline.authenticate.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.AuthObject;
import com.bp.pensionline.authenticate.exception.DAOException;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

public class ASDDataAccess {
	static final Logger LOG = Logger.getLogger(ASDDataAccess.class);
	private static final String SQL_SELECT_BP1_EXISTING = 
		"SELECT B.BD08X as NINO, ASD.ASD56X as BP1, ASD.ASD58X as PLID"
		+ " FROM BASIC B INNER JOIN ADD_STATIC_DATA ASD ON B.REFNO = ASD.REFNO"
		+ " INNER JOIN WE_AUTHENTICATE W ON UPPER(W.EXTUSERID) = B.BD08X"
		+ " WHERE B.BGROUP = ASD.BGROUP AND ASD.ASD56X = ?";
	
	private static final String SQL_EXISTING_ASD_ROW = 
		"SELECT DISTINCT ASD.ASD56X as BP1"
		+ " FROM ADD_STATIC_DATA ASD"
		+ " WHERE ASD.ASD56X = ?";
	
	private static final String SQL_SELECT_CHECK_MULTIROWS = ""
		+ "SELECT DISTINCT (B.BD08X)"
		+ " FROM BASIC B"
		+ " INNER JOIN ADD_STATIC_DATA ASD"
		+ " ON B.REFNO = ASD.REFNO"
		+ " WHERE B.BGROUP = ASD.BGROUP"
		+ " AND B.BD19A != 'NL'"
		+ " AND ASD.ASD56X = ?";
	
	private static final String SQL_SELECT_PLID_ON_NINO = ""
		+ "SELECT B.BD08X FROM BASIC B"
		+ " INNER JOIN ADD_STATIC_DATA ASD ON B.REFNO = ASD.REFNO"
		+ " INNER JOIN WE_AUTHENTICATE W ON UPPER(W.EXTUSERID) = B.BD08X"
		+ " WHERE B.BGROUP = ASD.BGROUP AND ASD.DATA_TYPE = 'GENINFO'"
		+ " AND ASD.ASD58X = ?";
	
	public AuthObject getBP1Record(String bp1) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		AuthObject result = null;
		try {
			LOG.info("Start look for the BP1 ID '" + bp1 
					+ "' in Administrator database");
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm = con.prepareStatement(SQL_SELECT_BP1_EXISTING);
			pstm.setString(1, bp1.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			if (rs.next()) {;
				result = new AuthObject();
				result.setNino(rs.getString(1));
				result.setBp1Id(rs.getString(2));
				result.setPlId(rs.getString(3));
			} // end-while

			if ((result != null) 
					&& (result.getBp1Id() != null)
					&& (!result.getBp1Id().equals(""))) {
				String msg = "The BP1 ID '" + bp1 + "' found.";
				
				// Logs out
				LOG.info(msg);
			} else {
				String msg = "The BP1 ID '" + bp1 + "' doest not exist"
				+ " in Administrator database";
				
				// Logs out
				LOG.info(msg);
			}
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();
			LOG.info("End look for BP1 ID '" 
					+ bp1 
					+ "' in Administrator database"
					+ " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public boolean existASDRow(String username) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		boolean result = false;
		try {
			LOG.info("Start check if PL ID exist as a BP1 user"
					+ " in the Administrator database - " + username);
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm=con.prepareStatement(SQL_EXISTING_ASD_ROW);
			pstm.setString(1, username.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			String yn = "NO";
			if (rs.next()) {
				yn = "YES";
				result = true;
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();
			LOG.info("End check if PL ID exist as a BP1 user"
					+ " in the Administrator database - "
					+ username 
					+ " - " + yn
					+ " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public boolean hasMultiRows(String bp1Name) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		boolean result = false;
		try {
			LOG.info("Start checking multi-rows - " + bp1Name 
					+ " - [ADD_STATIC_DATA]");
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm=con.prepareStatement(SQL_SELECT_CHECK_MULTIROWS);
			pstm.setString(1, bp1Name.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			int rows = 0;
			List<String> items = new ArrayList<String>();
			while (rs.next()) {
				rows ++;
				items.add(rs.getString(1));
			} // end-while
			
			String multiRowStr = "NO";
			if (rows > 1) {
				LOG.error(items.get(0) + ", " + items.get(1) 
						+ " share same BP1 ID '"
						+ bp1Name
						+ "'");
				multiRowStr = "YES";
				result = true;
			}
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();
			LOG.info("End checking multi-rows for BP1 ID - " 
					+ bp1Name + " - " + multiRowStr
					+ " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public boolean existWeAuthenticate(String nino) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		boolean result = false;
		try {
			LOG.info("Looking for the NINO '" + nino 
					+ "' in [WE_AUTHENTICATE]");
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm=con.prepareStatement(SQL_SELECT_BP1_EXISTING);
			pstm.setString(1, nino);
			rs = pstm.executeQuery();
			
			// Checks record
			if (rs.next()) {
				result = true;
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();
			LOG.info("The NINO '" + nino + "' existing in [WE_AUTHENTICATE]"
					+ " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
	
	public String getNINOForPLID(String plName) throws DAOException {
		long t1 = System.currentTimeMillis();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstm = null;
		String result = null;
		try {
			LOG.info("Start look for the NINO base on PL ID '" 
					+ plName 
					+ "'");
			DBConnector connector = DBConnector.getInstance();
			
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			if (con == null) {
				String msg = "Cannot get connection to Administrator database";
				
				// Logs out and throw an exception
				LOG.error(msg);
				throw new DAOException(msg);
			} // end-if
			
			// Change session to compatible mode
			SessionDataAccess.alterSession(con);
			
			pstm=con.prepareStatement(SQL_SELECT_PLID_ON_NINO);
			pstm.setString(1, plName.toUpperCase());
			rs = pstm.executeQuery();
			
			// Checks record
			if (rs.next()) {
				result = rs.getString(1);
				LOG.info("NINO " + result + " found for PL ID " + plName);
			} // end-while
			
			// Close result set and statement
			rs.close();
			pstm.close();
			
			long t2 = System.currentTimeMillis();
			LOG.info("End look for the NINO base on PL ID '" + plName + "'"
					+ " (" + (t2-t1) + " ms)");
		} catch (Exception ex) {
			throw new DAOException(ex.getMessage());
		} finally {
			try {
				DBConnector connector = DBConnector.getInstance();
				if (con != null) {
					connector.close(con);
				}
			} catch (Exception e) {
				LOG.error("Error in closing connection: " + e);
			}
		}
		
		return result;
	}
}
