package com.bp.pensionline.authenticate;

public class Constant {
	public static final String FORMAT_DATE_DDMMMYY = "dd-MMM-yy";
	public static final String FORMAT_DATE_ORA_DDMMMYY = "dd-MON-yy";
	public static final String ERROR_CODE_204 = "204";
	public static final String EE101 = "EE101";
}