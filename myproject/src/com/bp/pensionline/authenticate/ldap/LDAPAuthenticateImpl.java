package com.bp.pensionline.authenticate.ldap;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bp.pensionline.authenticate.AuthenticationHandler;
import com.bp.pensionline.authenticate.ResourceLoader;
import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.exception.Bp1LdaoException;
import com.bp.pensionline.authenticate.exception.DAOException;
import com.bp.pensionline.authenticate.exception.PropertyException;
import com.bp.pensionline.authenticate.util.AuthenticationHelper;
import com.bp.pensionline.authenticate.util.AuthenticationUtil;
import com.bp.pensionline.authenticate.util.LDAPUtil;

public class LDAPAuthenticateImpl implements AuthenticationHandler {
	private static Logger logger = LoggerFactory.getLogger(LDAPAuthenticateImpl.class);

    /** Property key prefix string. */
    public static final String LDAP_PROP_PREFIX = "com.bp.pensionline.ldap";

    /** String identifier for LDAP host. Value "com.bp.pensionline.ldap.host" */
    public static final String LDAP_HOST = LDAP_PROP_PREFIX + ".host";

    /** String identifier for LDAP port. Value "com.bp.pensionline.ldap.port" */
    public static final String LDAP_PORT = LDAP_PROP_PREFIX + ".port";

    /** String identifier for LDAP principal. Value */
    public static final String LDAP_PRINCIPAL = LDAP_PROP_PREFIX + ".principal";

    /**
     * String identifier for LDAP credentials. Value
     * "com.bp.shipping.ldap.credentials"
     */
    public static final String LDAP_CREDENTIALS = LDAP_PROP_PREFIX
        + ".credentials";

    /**
     * String identifier for LDAP secure. Value "com.bp.pensionline.ldap.secure"
     */
    public static final String LDAP_SECURE = LDAP_PROP_PREFIX + ".secure";

    /**
     * String identifier for LDAP certificate file path. Value
     * "com.bp.pensionline.ldap.path"
     */
    public static final String LDAP_PATH = LDAP_PROP_PREFIX + ".path";

    /** String identifier for credentials encoding. */
    public static final String LDAP_CREDENTIALS_ENCODING = "UTF8";

    /** String identifier for the key to LDAP security properties. */
    public static final String LDAP_KEY_STORE = "javax.net.ssl.trustStore";

    /**
     * Identifies the maximum number of physical connections allowed. Value
     * "com.bp.pensionline.ldap.maxconnections"
     */
    public static final String LDAP_MAX_CONNECTIONS = LDAP_PROP_PREFIX
        + ".maxconnections";

    /** The user filter value that identifies if the configuration is for AD. */
    public static final String LDAP_USER_FILTER = LDAP_PROP_PREFIX
        + ".userfilter";

    /** The Object class. */
    public static final String LDAP_OBJECT_CLASS = LDAP_PROP_PREFIX
        + ".objectclass";

    /** The LDAP search base. */
    public static final String LDAP_SEARCH_BASE = LDAP_PROP_PREFIX
        + ".searchbase";

    /** The LDAP returned result. */
    public static final String LDAP_RETURNED_RESULT = LDAP_PROP_PREFIX
        + ".result";

    /**
     * Identifies the maximum number of shared connections per physical
     * connection. Value "com.bp.pensionline.ldap.maxsharedconnections"
     */
    public static final String LDAP_MAX_SHARED_CONNECTIONS = LDAP_PROP_PREFIX
        + ".maxsharedconnections";

    /**
     * String identifier for LDAP <code>vendorVersion</code> attribute. Value
     * "vendorVersion"
     */
    public static final String VENDOR_VERSION = "vendorVersion";

    /**
     * String identifier for LDAP <code>namingContexts</code> attribute. Value
     * "namingContexts"
     */
    public static final String NAMING_CONTEXTS = "namingContexts";

    /**
     * String identifier for LDAP <code>netscapemdsuffix</code> attribute. Value
     * "netscapemdsuffix"
     */
    public static final String NETSCAPE_MD_SUFFIX = "netscapemdsuffix";

    /**
     * String identifier for LDAP <code>vendorName</code> attribute. Value
     * "vendorName"
     */
    public static final String VENDOR_NAME = "vendorName";

    /**
     * The version of directory server e.g Sun-Java(tm)-System-Directory/6.0.
     */
    private static String vendorVersion = null;

    /** The naming context of directory server. */
    private static String namingContexts = null;

    /** The netscape suffix e.g cn=ldap://dc=PLASMA:1389. */
    private static String netscapemdsuffix = null;

    /** The vendor name e.g Sun Microsystems, Inc. */
    private static String vendorName = null;

    /** The name of the host to which we will bind. */
    private static String host;

    /** The LDAP port number for the bind. */
    private static int port;

    /** Maximum number of connections. */
    private static int maxConnections;

    /** Maximum number of shared connections. */
    private static int maxSharedConnections;

    /**
     * The name of the LDAP user we will bind with e.g. cn=Directory Manager.
     */
    private static String principal;

    /** The principal's credentials used in the bind. */
    private static String credentials;

    /** Whether to user SSL for the LDAP connection. */
    private static boolean secure = false;

    /** Whether to the Active Directory or not. */
    public static boolean activeDirectory = false;

    /** The search base. */
    public static String searchBase;

    /** The object class. */
    public static String objectClass;

    /** The returned result. */
    public static int returnedResult;

    /** Path to the keystore if we are using SSL. */
    private static String keyPath;

    /**
     * The name of the LDAP properties file that contains connection properties
     * for BP1 LDAP.
     */
    public static final String LDAP_PROPS_FILE_NAME = "ldap.properties";

    /** Indicates if the LDAP connection has been configured. */
    private static boolean configured;
	
	private String username;
	private String password;
	
	public LDAPAuthenticateImpl() {
	}
	
	public LDAPAuthenticateImpl(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public boolean authenticate() throws DAOException {
		boolean rt = false;
		
		try {
			logger.info("Start authenticate to BP1 LDAP - " 
					+ username);
			username = AuthenticationHelper.correctBp1Name(username);

			if (!AuthenticationHelper.checkNINOWeAuthenticate(username)) {
				String msg = "Authentication fails for BP1 name '" 
					+ username + "'";
				
				// Logs and exception
				logger.error(msg);
				throw new AuthenticationException(msg);
			}
			// LDAP authentication
			if (!ldapAuthenticate()) {
				String msg = "Authentication fails for BP1 name '" 
					+ username + "'";
				
				// Logs and exception
				logger.error(msg);
				throw new AuthenticationException(msg);
			}
			logger.info("End authenticate to BP1 LDAP - " 
					+ username + " - YES");
		} catch (DAOException de) {
			throw new DAOException(de.getMessage());
		}
		catch (Exception ex) {
			logger.error("Failed to authenticate BP1 name '" 
					+ username + "', error detail: " 
					+ ex.getMessage());
		}
		
		return rt;
	}
	
	public boolean ldapAuthenticate(String username, String password) {
		//Declares connection variables
		long t1 = System.currentTimeMillis();
		String authType="simple";
		String securityPrincipal = "CN=-svc-planview,OU=Svc,OU=rEU,OU=Admin,DC=bp1,DC=ad,DC=bp,DC=com";
		String securityCredentials = "566xorveRc88R3";
		String initialContextFactory="com.sun.jndi.ldap.LdapCtxFactory";
		String host = "149.184.178.72";
		String port = "389";
		String url = "ldap://" + host + ":" + port;
		String searchBase = "DC=bp1,DC=ad,DC=bp,DC=com";
		
		String returnedAtts[] = { "cn", "sAMAccountName" };
		String searchFilter = null;
			
		// Defines environment
		Hashtable<Object, Object> env= new Hashtable<Object, Object>(11);
		env.put(Context.SECURITY_AUTHENTICATION, authType);
		env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);//User
		env.put(Context.SECURITY_CREDENTIALS, securityCredentials);//Password
		env.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
		env.put(Context.PROVIDER_URL, url);
		
		// Pool
		//env.put("com.sun.jndi.ldap.connect.pool", usePool);
		//env.put("com.sun.jndi.ldap.connect.timeou", timeout);

		// Try to authenticate
		
		boolean auth = false;
		
		if ((password == null) || (password.trim().length() <= 0)) {
			logger.error("LDAP password is null or blank - " + username);
			return auth;
		}
		
		DirContext ctx = null;
		try {
			logger.info("Start authenticate to BP1 LDAP - " + username);
			// Create the initial context
			ctx = new InitialDirContext(env);
			
			// Checks the context and throw an exception if the returned value null
			if (ctx != null) {
				SearchControls searchCtls = new SearchControls();
				
				//Specify the attributes to return
				searchCtls.setReturningAttributes(returnedAtts);
				
				//Specify the search scope
				searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				
				//initialize counter to total the results
				int totalResults = 0;
				
				searchFilter = "(&(objectClass=user)(sAMAccountName=" + username + "))";
				
				// Search for objects using the filter
				NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter,
					searchCtls);
				
				//Loop through the search results
				while (answer.hasMoreElements()) {
					SearchResult sr = (SearchResult) answer.next();

					totalResults++;

					String dn = sr.getName() + "," + searchBase;
					
					// Gets attributes
					Attributes attrs = sr.getAttributes();
					// Checks whether attributes is null or not
					if (attrs != null) {
						try {
							// Do bind again with specified Principal/Credentials
							env.put(Context.SECURITY_PRINCIPAL, dn);
							env.put(Context.SECURITY_CREDENTIALS, password);
							
							// Authenticate to the server
							ctx = new InitialDirContext(env);
							
							// Really authenticated
							auth = true;
						} catch (NullPointerException e) {
							// if null condition met
							System.out.println("Errors listing attributes: "
									+ e);
							// Authenticate failed
							logger.error("End authenticate to BP1 LDAP - " 
									+ username + " - NO");
							return auth;
						} // try-catch 2
					} // if
					
				} // while
			} // if
			
			// Return authentication result whether it is true or false
			String result = "NO";
			if (auth) {
				result = "YES";
			}
			long t2 = System.currentTimeMillis();
			logger.info("End authenticate to BP1 LDAP - " 
					+ username + " - " + result
					+ " (" + (t2-t1) + " ms)");
			return auth;
		} catch (javax.naming.AuthenticationException aEx) {
			aEx.printStackTrace();
			// Authenticate failed
			logger.error("End authenticate to BP1 LDAP - " 
					+ username + " - NO");
			return auth;
		} catch (NamingException nEx) {
			nEx.printStackTrace();
			// Authenticate failed
			logger.error("End authenticate to BP1 LDAP - " 
					+ username + " - NO");
			return auth;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception cex) {
					logger.warn("Cannot close the connection: " 
							+ cex.getMessage());
				}
			}
		} // try-catch-finally
	}
	
	public boolean ldapAuthenticate() {
		//Declares connection variables
		String authType="simple";
		String securityPrincipal = "CN=-svc-planview,OU=Svc,OU=rEU,OU=Admin,DC=bp1,DC=ad,DC=bp,DC=com";
		String securityCredentials = "566xorveRc88R3";
		String initialContextFactory="com.sun.jndi.ldap.LdapCtxFactory";
		String host = "149.184.178.72";
		int port = 389;
		String url = "ldap://" + host + ":" + port;
		String searchBase = "DC=bp1,DC=ad,DC=bp,DC=com";
		String ldapID = "uid";
		
		// Load properties
		try {
			loadProperties();
			
			// Assigns values
			// Authentication Principal
			securityPrincipal = principal;
			
			// Authentication securityCredentials
			securityCredentials = credentials;

			// Authentication host
			host = LDAPAuthenticateImpl.host;
			
			// Authentication port
			port = LDAPAuthenticateImpl.port;
			
			// Authentication search base
			searchBase = LDAPAuthenticateImpl.searchBase;
			
			// Authentication URL
			url = "ldap://" + host + ":" + port;
			
			// Authentication Filter
			if (activeDirectory) {
				ldapID = "sAMAccountName";
			}
		} catch (Exception e) {
			logger.error("Error in loading properties, error: {}", e.getMessage());
		}

		//Specify the Base for the search
		String returnedAtts[] = { "cn", ldapID };
		String searchFilter = null;
			
		// Defines environment
		Hashtable<Object, Object> env= new Hashtable<Object, Object>(11);
		env.put(Context.SECURITY_AUTHENTICATION,authType);
		env.put(Context.SECURITY_PRINCIPAL,securityPrincipal);//User
		env.put(Context.SECURITY_CREDENTIALS, securityCredentials);//Password
		env.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
		env.put(Context.PROVIDER_URL, url);

		// Try to authenticate
		
		boolean auth = false;
		try {
			// Create the initial context
			logger.info("Start authenticate to BP1 LDAP - " 
					+ username);
			DirContext ctx = new InitialDirContext(env);
			
			// Checks the context and throw an exception if the returned value null
			if (ctx != null) {
				SearchControls searchCtls = new SearchControls();
				
				//Specify the attributes to return
				searchCtls.setReturningAttributes(returnedAtts);
				
				//Specify the search scope
				searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				
				//initialize counter to total the results
				int totalResults = 0;
				
				String objectType = "*";
				if (activeDirectory) {
					objectType = "user";
				}
				
				searchFilter = "(&(objectClass=" + objectType + ")(" + ldapID + "=" + username + "))";
				logger.info("Search filter: {}", searchFilter);
				
				// Search for objects using the filter
				NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter,
					searchCtls);
				
				//Loop through the search results
				while (answer.hasMoreElements()) {
					SearchResult sr = (SearchResult) answer.next();

					totalResults++;

					String dn = sr.getName() + "," + searchBase;
					
					// Gets attributes
					Attributes attrs = sr.getAttributes();
					// Checks whether attributes is null or not
					if (attrs != null) {
						try {
							// Do bind again with specified Principal/Credentials
							env.put(Context.SECURITY_PRINCIPAL, dn);
							env.put(Context.SECURITY_CREDENTIALS, password);
							
							// Authenticate to the server
							ctx = new InitialDirContext(env);
							
							// Really authenticated
							auth = true;
						} catch (NullPointerException e) {
							// if null condition met
							System.out.println("Errors listing attributes: "
									+ e);
							// Authenticate failed
							logger.error("End authenticate to BP1 LDAP - " 
									+ username + " - NO");
							return auth;
						} // try-catch 2
					} // if
					
				} // while
			} // if
			
			// Return authentication result whether it is true or false
			String result = "YES";
			if (!auth) {
				result = "NO";
			}
			logger.info("End authenticate to BP1 LDAP - " 
					+ username + " - " + result);
			return auth;
		} catch (javax.naming.AuthenticationException aEx) {
			aEx.printStackTrace();
			// Authenticate failed
			logger.error("End authenticate to BP1 LDAP - " 
					+ username + " - NO");
			return auth;
		} catch (NamingException nEx) {
			nEx.printStackTrace();
			// Authenticate failed
			logger.error("End authenticate to BP1 LDAP - " 
					+ username + " - NO");
			return auth;
		} // try-catch 1
	}
	
	/**
     * Loads the properties for configuring the LDAP connection and pool.
     *
     * @throws  PropertyException  if the property file can not be found or if
     *                             there is a missing or invalid property.
     * @throws  Bp1LdaoException   if an error occurs when creating the LDAP
     *                             connection pool.
     */
    private static void loadProperties() throws PropertyException,
        Bp1LdaoException {
        if (logger.isDebugEnabled()) {
            logger.debug("Loading properties from {} ...", LDAP_PROPS_FILE_NAME);
        }
        Properties props = new Properties();
        PropertyException propExc = new PropertyException();
        propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
        try {
            // Load the property file.
            props = ResourceLoader.getInstance().getProperties(
            		LDAPAuthenticateImpl.LDAP_PROPS_FILE_NAME);

            // Host property
            host = props.getProperty(LDAP_HOST);
            if ((host == null) || (host.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value", LDAP_HOST);
                propExc.addMissingProperty(LDAP_HOST);
            } else {
                logger.info("Property {} loaded with value {}", LDAP_HOST,
                    host);
            }

            // LDAP port.
            String portValue = props.getProperty(LDAP_PORT);
            if ((portValue == null) || (portValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value", LDAP_PORT);
                propExc.addMissingProperty(LDAP_PORT);
            } else {
                try {
                    port = new Integer(portValue).intValue();
                    logger.info("Property {} loaded with value {}", LDAP_PORT,
                        portValue);
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_PORT, portValue);
                    propExc.addInvalidProperty(LDAP_PORT);
                }
            }

            // Maximum number of connections.
            String maxConnectionsValue = props.getProperty(
                    LDAP_MAX_CONNECTIONS);
            if ((maxConnectionsValue == null)
                    || (maxConnectionsValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_MAX_CONNECTIONS);
                propExc.addMissingProperty(LDAP_MAX_CONNECTIONS);
            } else {
                try {
                    maxConnections = new Integer(maxConnectionsValue)
                        .intValue();
                    if (maxConnections <= 0) {
                        logger.error(
                            "{} is not valid, received {}, expected a value > 0",
                            LDAP_MAX_CONNECTIONS, maxConnectionsValue);
                        propExc.addInvalidProperty(LDAP_MAX_CONNECTIONS);
                    } else {
                        //logger.info("Property {} loaded with value {}",
                            //LDAP_MAX_CONNECTIONS, maxConnections);
                    }
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_MAX_CONNECTIONS, maxConnectionsValue);
                    propExc.addInvalidProperty(LDAP_MAX_CONNECTIONS);
                }
            }

            // Maximum number of shared connections.
            String maxSharedConnectionsValue = props.getProperty(
                    LDAP_MAX_SHARED_CONNECTIONS);
            if ((maxSharedConnectionsValue == null)
                    || (maxSharedConnectionsValue.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_MAX_SHARED_CONNECTIONS);
                propExc.addMissingProperty(LDAP_MAX_SHARED_CONNECTIONS);
            } else {
                try {
                    maxSharedConnections = new Integer(
                            maxSharedConnectionsValue).intValue();
                    //logger.info("Property {} loaded with value {}",
                        //LDAP_MAX_SHARED_CONNECTIONS, maxSharedConnections);
                } catch (NumberFormatException e) {
                    logger.error(
                        "{} is not valid, received {}, expected a numeric value",
                        LDAP_MAX_SHARED_CONNECTIONS, maxSharedConnectionsValue);
                    propExc.addInvalidProperty(LDAP_MAX_SHARED_CONNECTIONS);
                }
            }

            // LDAP principal.
            principal = props.getProperty(LDAP_PRINCIPAL);
            if ((principal == null) || (principal.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_PRINCIPAL);
                propExc.addMissingProperty(LDAP_PRINCIPAL);
            } else {
                logger.info("Property {} loaded with value {}", LDAP_PRINCIPAL,
                    principal);
            }

            // LDAP Credentials.
            credentials = props.getProperty(LDAP_CREDENTIALS);
            if ((credentials == null) || (credentials.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_CREDENTIALS);
                propExc.addMissingProperty(LDAP_CREDENTIALS);
            } else {
                logger.info("Property {} loaded with value {}",
                    LDAP_CREDENTIALS, "********");
            }

            // LDAP search base.
            searchBase = props.getProperty(LDAP_SEARCH_BASE);
            if ((searchBase == null) || (searchBase.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_SEARCH_BASE);
                propExc.addMissingProperty(LDAP_SEARCH_BASE);
            } else {
                logger.info("Property {} loaded with value {}",
                    LDAP_SEARCH_BASE, searchBase);
            }

            // LDAP Object class.
            objectClass = props.getProperty(LDAP_OBJECT_CLASS);
            if ((objectClass == null) || (objectClass.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_OBJECT_CLASS);
                propExc.addMissingProperty(LDAP_OBJECT_CLASS);
            } else {
                logger.info("Property {} loaded with value {}",
                    LDAP_OBJECT_CLASS, objectClass);
            }

            // LDAP Active Directory.
            String activeStr = props.getProperty(LDAP_USER_FILTER);
            if ((activeStr == null) || (activeStr.trim().length() == 0)) {
                logger.error("{} is missing or has an empty value",
                    LDAP_USER_FILTER);
                propExc.addMissingProperty(LDAP_USER_FILTER);
            } else {
                if (activeStr.trim().equals("sAMAccountName")) {
                    activeDirectory = true;
                }
                logger.info("Property {} loaded with value {}",
                    LDAP_USER_FILTER, activeStr);
                logger.info("Property {} loaded with value {}",
                    "Active Directory", activeDirectory);
            }

            // If max shared connections is greater than max connections
            // then assign it to max connections.
            if (maxSharedConnections > maxConnections) {
                maxSharedConnections = maxConnections;
                logger.warn(
                    "Max shared connections [{}]"
                    + " is greater than max connections [{}],"
                    + " setting max shared connections to {}",
                    new Object[] {
                        maxSharedConnectionsValue, maxConnectionsValue,
                        maxSharedConnectionsValue
                    });
            }
            configured = true;
        } catch (FileNotFoundException ex) {
            logger.error(
                "Failed to load properties from file, file {} doesn't exist",
                LDAP_PROPS_FILE_NAME);
            propExc = new PropertyException(ex.getMessage());
            propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
            throw propExc;
        } catch (IOException ex) {
            logger.error("Failed to read file {}, error: {}",
                LDAP_PROPS_FILE_NAME, ex.getMessage());
            propExc = new PropertyException(ex.getMessage());
            propExc.setPropertyFileName(LDAP_PROPS_FILE_NAME);
            throw propExc;
        }
    }
}
