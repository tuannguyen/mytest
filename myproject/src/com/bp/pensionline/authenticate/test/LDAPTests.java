package com.bp.pensionline.authenticate.test;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Iterator;

import com.bp.pensionline.authenticate.exception.Bp1LdaoException;
import com.bp.pensionline.authenticate.ldap.LDAPAuthenticateImpl;
import com.bp.pensionline.authenticate.util.LDAPUtil;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.util.Base64;

import junit.framework.TestCase;

public class LDAPTests extends TestCase {
	private final String username = "ldapuser";
	private final String password = "LDAPUser@";
	int ldapVersion = LDAPConnection.LDAP_V3;
	
	public void testAuthenticate() {
		LDAPAuthenticateImpl auth = new LDAPAuthenticateImpl(username, password);
		boolean result = auth.ldapAuthenticate();
		assertTrue("Authenticate fails", result);
	}
}
