package com.bp.pensionline.authenticate.test;

import java.util.ArrayList;
import java.util.List;

import com.bp.pensionline.authenticate.RefNoData;
import com.bp.pensionline.authenticate.util.StringUtil;

import junit.framework.TestCase;


public class StringTest extends TestCase {
	public void testList() {
		List<RefNoData> items = new ArrayList<RefNoData>();
		RefNoData data = new RefNoData();
		data.setRefno("1");
		data.setStatus("DD");
		items.add(data);
		//
		data.setRefno("2");
		data.setStatus("PP");
		items.add(data);
		
		//
		data.setRefno("3");
		data.setStatus("PN");
		items.add(data);
		//
		data.setRefno("4");
		data.setStatus("AC");
		items.add(data);
		
		System.out.println(StringUtil.getRefNoFromList(items));
	}
}
