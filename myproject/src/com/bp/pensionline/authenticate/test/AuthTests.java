package com.bp.pensionline.authenticate.test;

import com.bp.pensionline.authenticate.AuthenticationFactory;
import com.bp.pensionline.authenticate.AuthenticationHandler;
import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.exception.DAOException;
import com.bp.pensionline.authenticate.ldap.LDAPAuthenticateImpl;

import junit.framework.TestCase;

public class AuthTests extends TestCase {
	private final String USERNAME_BP1 = "nguyb6";
	private final String USERNAME_BP1_WITH_START = "bp1\nguyb6";
	
	private final String PASSWORD = "1234";
	
	public void testBP1Authenticate() {
		try {
			AuthenticationHandler handler = 
				AuthenticationFactory.getAuthenticationHandler(USERNAME_BP1, PASSWORD);
			boolean result = handler.authenticate();
			assertTrue("Cannot authenticate", result);
		} catch (AuthenticationException ae) {
			fail("Authentication fails: " + ae.getMessage());
		} catch (DAOException de) {
			fail("DAO access fails: " + de.getMessage());
		}
		
	}
	
	public void testBP1StartAuthenticate() {
		try {
			AuthenticationHandler handler = 
				AuthenticationFactory.getAuthenticationHandler(USERNAME_BP1_WITH_START, PASSWORD);
			boolean result = handler.authenticate();
			assertTrue("Cannot authenticate", result);
		} catch (AuthenticationException ae) {
			fail("Authentication fails: " + ae.getMessage());
		} catch (DAOException de) {
			fail("DAO access fails: " + de.getMessage());
		}
		
	}
	
	public void testCMGLDAPAuthenticate() {
		try {
			String username = "binhnguyen";
			String password = "adminpassX1@";
			LDAPAuthenticateImpl ldap = new LDAPAuthenticateImpl(username, password);
			boolean result = ldap.ldapAuthenticate();
			assertTrue("Cannot authenticate", result);
		} catch (Exception de) {
			fail("auth fails: " + de.getMessage());
		}
		
	}
}
