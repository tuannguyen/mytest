package com.bp.pensionline.authenticate.test;

import com.bp.pensionline.authenticate.database.ASDDataAccess;
import com.bp.pensionline.authenticate.exception.DAOException;

import junit.framework.TestCase;

public class DAOTests extends TestCase {
	public void testMultiRows() throws DAOException {
		ASDDataAccess dao = new ASDDataAccess();
		boolean result = dao.hasMultiRows("nguyb6");
		assertTrue("No multi-rows found", result);
	}
}
