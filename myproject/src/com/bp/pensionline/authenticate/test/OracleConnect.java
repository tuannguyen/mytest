package com.bp.pensionline.authenticate.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.bp.pensionline.authenticate.database.SessionDataAccess;

public class OracleConnect {
	public static void main(String[] args) {
	    try {
	      Connection con=null;
	      Class.forName("oracle.jdbc.driver.OracleDriver");
	      con=DriverManager.getConnection(
	        "jdbc:oracle:thin:@127.1.1.204:4522:BP101S",
	        "BPPL4CMS",
	        "BPPL4CMS");
	      PreparedStatement s=con.prepareStatement("Select * from we_authenticate where extuserid = ?");
	      s.setString(1, "ne799385C");
	      
	      SessionDataAccess.alterSession(con);
	      
//	      Statement sm = con.createStatement();
//	      sm.executeUpdate("ALTER SESSION SET NLS_SORT ='BINARY_CI'");
//	      sm.executeUpdate("ALTER SESSION SET NLS_COMP='LINGUISTIC'");
//	      sm.close();

	      ResultSet rs = s.executeQuery();
	      
	      while (rs.next()) {
	    	  System.out.println("hic " + rs.getString(1));
	      }
	      s.close();
	      con.close();
	      System.out.print("done");
	   } catch(Exception e){e.printStackTrace();}
	 }
}
