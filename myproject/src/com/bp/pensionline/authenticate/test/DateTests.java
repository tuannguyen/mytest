package com.bp.pensionline.authenticate.test;

import junit.framework.TestCase;

import com.bp.pensionline.authenticate.util.DateUtil;


public class DateTests extends TestCase {
	public void testFormat() {
		String date = "2009-12-12";
		String formatted = DateUtil.formatDate(date);
		System.out.println(formatted);
	}
}
