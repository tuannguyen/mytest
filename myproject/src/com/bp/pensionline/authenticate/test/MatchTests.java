package com.bp.pensionline.authenticate.test;

import com.bp.pensionline.authenticate.AuthenticationFactory;
import com.bp.pensionline.authenticate.AuthenticationHandler;
import com.bp.pensionline.authenticate.CMSAuthenticationImpl;
import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.util.AuthenticationHelper;
import com.bp.pensionline.authenticate.util.StringUtil;

import junit.framework.TestCase;

public class MatchTests extends TestCase {

	private final String USERNAME_CMS = "binhnguyen";
	private final String USERNAME_NOT_CMS = "binhnguyen#";
	private final String USERNAME_BP1_WITH_START = "bp1\nguyb6";
	private final String USERNAME_BP1 = "nguyb6";
	private final String USERNAME_NOT_BP1 = "tuan77";
	private final String USERNAME_NINO = "az1234567u";
	private final String USERNAME_NOT_NINO = "az1234567u20";
	private final String password = "123456";

	public void testCMSUser() {
		try {
			System.out.println("Checking user " + USERNAME_CMS + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(USERNAME_CMS, password);
			assertNotNull("The auth is null", auth);
			if ("com.bp.pensionline.authenticate.CMSAuthenticationImpl".equals(auth.getClass().getName())) {
				System.out.println(USERNAME_CMS + " is CMS user");
			} else {
				System.out.println(USERNAME_CMS + " is NOT CMS user");
			}
		} catch (AuthenticationException ex) {
			fail("Autentication fail: " + ex.getMessage());
		}
	}
	
	public void testNOTCMSUser() {
		try {
			System.out.println("Checking user " + USERNAME_NOT_CMS + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(USERNAME_NOT_CMS, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(USERNAME_NOT_CMS + " is NOT CMS user");
		} catch (Exception e) {
			fail("Error in testNOTCMSUser(): " + e.getMessage());
		}
	}
	
	public void testBP1LDAPUser() {
		String loginName = null;
		try {
			loginName = StringUtil.escape(USERNAME_BP1_WITH_START);
			System.out.println("Checking user " + loginName + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(loginName, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(loginName + " is NOT BP1 user");
		} catch (Exception e) {
			fail("Error in testBP1LDAPUser(): " + e.getMessage());
		}
	}
	
	public void testLDAPUser() {
		String loginName = null;
		try {
			loginName = StringUtil.escape(USERNAME_BP1);
			System.out.println("Checking user " + loginName + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(loginName, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(loginName + " is NOT BP1 user");
		} catch (Exception e) {
			fail("Error in testLDAPUser(): " + e.getMessage());
		}
	}
	
	public void testNOTLDAPUser() {
		String loginName = null;
		try {
			loginName = StringUtil.escape(USERNAME_NOT_BP1);
			System.out.println("Checking user " + loginName + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(loginName, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(loginName + " is NOT BP1 user");
		} catch (Exception e) {
			fail("Error in testNOTLDAPUser(): " + e.getMessage());
		}
	}
	
	public void testNINOUser() {
		String loginName = null;
		try {
			loginName = StringUtil.escape(USERNAME_NINO);
			System.out.println("Checking user " + loginName + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(loginName, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(loginName + " is NOT BP1 user");
		} catch (Exception e) {
			fail("Error in testNINOUser(): " + e.getMessage());
		}
	}
	
	public void testNOTNINOUser() {
		String loginName = null;
		try {
			loginName = StringUtil.escape(USERNAME_NOT_NINO);
			System.out.println("Checking user " + loginName + "...");
			AuthenticationHandler auth = 
				AuthenticationFactory.getAuthenticationHandler(loginName, password);
			assertNotNull("The auth is not null", auth);
		} catch (AuthenticationException ex) {
			System.out.println(loginName + " is NOT BP1 user");
		} catch (Exception e) {
			fail("Error in testNOTNINOUser(): " + e.getMessage());
		}
	}
	
	public void bp1User() {
		boolean auth = AuthenticationHelper.isBP1User(USERNAME_BP1_WITH_START);
		assertTrue("No no", auth);
		System.out.println("True Or False: " + auth);
	}
}
