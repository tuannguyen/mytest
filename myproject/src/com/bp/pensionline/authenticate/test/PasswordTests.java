package com.bp.pensionline.authenticate.test;

import com.bp.pensionline.authenticate.PasswordService;

import junit.framework.TestCase;

public class PasswordTests extends TestCase {
	public void testEncryptePassword() throws Exception {
		PasswordService service = PasswordService.getInstance();
		String encryptedPassword = service.encrypt("LDAPUser@");
		System.out.println("encryptedPassword: " + encryptedPassword);
		
		String alternative = "{MD5}kTi03AB9oPjjZ9jivze4TQ==";
		
		encryptedPassword = "{MD5}" + encryptedPassword;
		if (encryptedPassword.equals(alternative)) {
			String msg = "Two passwords are the same.";
			System.out.println("Message: " + msg);
		}
	}
}
