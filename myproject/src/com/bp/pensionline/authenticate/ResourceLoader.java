package com.bp.pensionline.authenticate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * A utility class that makes it possible to load a configuration file as
 * property objects.
 *
 * <p>Properties are searched for in several places in the following order:</p>
 *
 * <ol>
 *   <li>An external properties directory whose location is specified by the
 *     system property {@link ResourceLoader#PL_EXTERNAL_PROPS_DIR}.</li>
 *   <li>A properties file that can be loaded through the class loader.</li>
 *   <li>An absolute location for a property file.</li>
 * </ol>
 *
 * <p>Properties files once loaded are cached locally for performance.</p>
 *
 * @author  Binh Nguyen
 * @version 1.0.1 November 19, 2009
 */
public class ResourceLoader {

    //~ Static fields/initializers ---------------------------------------------

    /**
     * The system property that points to the location of the external PL
     * property directory.
     */
    public static final String PL_EXTERNAL_PROPS_DIR =
        "com.bp.pensionline.properties.dir";

    /** The defaul Locale value is used to load ResourceBundle. */
    public static final Locale DEFAULT_LOCATE = Locale.UK;

    /**
     * Singleton instance of ResourceLoader class. This instance will be used
     * throughout the system.
     */
    private static ResourceLoader INSTANCE = null;

    /** Logger for this class. */
    private static Logger logger = LoggerFactory.getLogger(
            ResourceLoader.class);

    //~ Instance fields --------------------------------------------------------

    /** The map contains loaded Properties. */
    private Map<String, Properties> propsObjectMap =
        new HashMap<String, Properties>();

    //~ Constructors -----------------------------------------------------------

    /**
     * Private constructor for ResourceLoader. This method cannot be invoked
     * from external.
     */
    private ResourceLoader() {
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Loads the properties file with the specified name.
     *
     * <p>The order and precedence of loading is as follows:</p>
     *
     * <ol>
     *   <li>Try and load from the internal cache.</li>
     *   <li>Try and load from the external properties diretory</li>
     *   <li>Finally, try and load using the classloader from the JAR/WAR.</li>
     * </ol>
     *
     * @param   propsFileName  the name of the properties file.
     *
     * @return  loaded properties or <code>null</code> if the property file can
     *          not be found.
     *
     * @throws  FileNotFoundException  If the property file does not exist in
     *                                 the external directory or is accessible
     *                                 via the class loader.
     * @throws  IOException            If an error occurs when reading the
     *                                 property file through the class loading
     *                                 mechanism.
     */
    public synchronized Properties getProperties(String propsFileName)
        throws FileNotFoundException, IOException {
        Properties props = null;
        if (propsFileName != null) {
            // Try to get it from the cache.
            props = propsObjectMap.get(propsFileName);

            // If it is not in the cache, then try and load from the external
            // directory or if that fails then try and class load it from the
            // JAR/WAR.
            if (props == null) {
                String plExtPropsDir = System.getProperty(
                        PL_EXTERNAL_PROPS_DIR);
                if (plExtPropsDir != null) {
                    File dir = new File(plExtPropsDir);
                    if (dir.isDirectory()) {
                        // Try and load from the external properties directory.
                        File propsFile = new File(plExtPropsDir,
                                propsFileName);
                        if (propsFile.exists() && propsFile.canRead()) {
                            try {
                                props = new Properties();
                                props.load(new FileInputStream(
                                        new File(plExtPropsDir,
                                            propsFileName)));
                                propsObjectMap.put(propsFileName, props);
                            } catch (FileNotFoundException ex) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug(
                                        "Failed to load {} from {}, trying class loader next...",
                                        propsFileName, plExtPropsDir);
                                }
                            } catch (IOException ex) {
                                logger.warn(
                                    "IO error reading {} from {}, error={}, trying class loader next...",
                                    new Object[] {
                                        propsFileName, plExtPropsDir,
                                        ex.getMessage()
                                    });
                            }
                        }
                    }
                } else {
                    logger.warn(
                        "{} is not a directory, trying class loader next...",
                        plExtPropsDir);
                }
                if (props == null) {
                    // Try and class load it e.g. from a JAR or WAR.
                    InputStream in = getResourceAsStream(propsFileName);
                    if (in != null) {
                        props = new Properties();
                        props.load(in);
                        in.close();
                        propsObjectMap.put(propsFileName, props);
                    }
                }
            }
        }

        return props;
    }

    /**
     * Gets a resource bundle using the specified base name, the default locate,
     * and the caller's class loader.
     *
     * @param   baseName  the base name of the resource bundle, a fully
     *                    qualified class name
     *
     * @return  a resource bundle for the given base name and the default locate
     *
     * @throws  NullPointerException      if baseName is null
     * @throws  MissingResourceException  if no resource bundle for the
     *                                    specified base name can be found
     */
    public ResourceBundle getResourceBundle(String baseName)
        throws NullPointerException, MissingResourceException {
        return ResourceBundle.getBundle(baseName);
    }

    /**
     * Gets a resource bundle using the specified base name and locate, and the
     * caller's class loader. If <code>locate</code> is null, then the {@link
     * DEFAULT_LOCATE} is used
     *
     * @param   baseName  the base name of the resource bundle, a fully
     *                    qualified class name
     * @param   locale    the locate for which a resource bundle is desired
     *
     * @return  a resource bundle for the given base name and locate
     *
     * @throws  NullPointerException      if baseName is null
     * @throws  MissingResourceException  if no resource bundle for the
     *                                    specified base name can be found
     */
    public ResourceBundle getResourceBundle(String baseName, Locale locale)
        throws NullPointerException, MissingResourceException {
        if (locale != null) {
            return ResourceBundle.getBundle(baseName, locale);
        } else {
            return ResourceBundle.getBundle(baseName, DEFAULT_LOCATE);
        }
    }

    /**
     * Get an input stream object associated with a resource that is available
     * to the resource loader.
     *
     * @param   resourceName  the name of the resource which can be an absolute
     *                        or a relative path.
     *
     * @return  an input stream representing the resource.
     *
     * @throws  FileNotFoundException  If the resource is not found in the
     *                                 class path.
     * @throws  IOException            If a stream cannot be opened for this
     *                                 resource.
     */
    public InputStream getResourceAsStream(String resourceName)
        throws FileNotFoundException, IOException {
        // Class loader.
        ClassLoader cldr = this.getClass().getClassLoader();

        // Input stream.
        InputStream in = cldr.getResourceAsStream(resourceName);

        // If the resource is not found in the class path of the class loader
        // then maybe the resource name is an absolute file path.
        if (in == null) {
            try {
                // Works if the user has given an absolute path.
                in = new FileInputStream(resourceName);
            } catch (FileNotFoundException e) {
                throw e;
            }
        }

        return in;
    }

    /**
     * Get a signleton instance of <code>ResourceLoader</code>.
     *
     * @return  instance of <code>ResourceLoader</code>
     */
    public static synchronized ResourceLoader getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ResourceLoader();
        }

        return INSTANCE;
    }

    /**
     * Return a short description of <code>ResourceLoader.</code>
     *
     * @return  the name of <code>ResourceLoader</code>
     */
    @Override public String toString() {
        return getClass().getName();
    }
}