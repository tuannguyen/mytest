package com.bp.pensionline.authenticate;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.exception.AuthenticationException;
import com.bp.pensionline.authenticate.service.PLSelfService;
import com.bp.pensionline.authenticate.util.AuthenticationHelper;
import com.bp.pensionline.util.SystemAccount;

/**
 * This class allows to authenticate a user with NINO 
 * and then migrate to a PL ID
 * @author 	Binh Nguyen
 * @version 1.0.0 November 30, 2009
 */
public class NINOAuthenticateImpl implements AuthenticationHandler {
	static Logger LOG = Logger.getLogger(NINOAuthenticateImpl.class);
	private String username;
	private String password;
	
	public NINOAuthenticateImpl(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public boolean authenticate()
			throws AuthenticationException {
		String errorMsg = "";
		boolean auth = false;
		try {
			// Make sure entered user name is correct
			this.username = this.username.trim();
			
			PLSelfService selfService = new PLSelfService();
			if (SystemAccount.checkWebuserExist(username)) {
				// Checks if the user has already migrated
				LOG.info("The NINO name exists in CMS database - " + username);
				
				if (selfService.isMigrated(username)) {
					errorMsg = "The NINO user has already migrated";
					LOG.error(errorMsg);
					return auth;
				}
			} else {
				// Checks if the user has not a welcome pack
				LOG.info("The NINO name does not exists in CMS database - " 
						+ username);
				LOG.info("Start check welcome pack - " 
						+ username);
				if (!selfService.hasWelcomePack(username)) {
					errorMsg = "The user does not have a welcome pack yet - " 
						+ username;
					LOG.error(errorMsg);
					return auth;
				}
				LOG.info("End check welcome pack - " 
						+ username + " - YES");
			}
			
			// Already passed the above steps --> OK
			auth = true;
		} catch (Exception ex) {
			LOG.error("Failed to authenticate with NINO - " + username
					+ ", detail: " + ex.getMessage());
		}
		
		return auth;
	}

}
