package com.bp.pensionline.authenticate.service;

import org.apache.log4j.Logger;

import com.bp.pensionline.authenticate.ASD;
import com.bp.pensionline.authenticate.UserInfo;
import com.bp.pensionline.authenticate.database.ASDDataAccess;
import com.bp.pensionline.authenticate.database.SelfServiceDataAccess;
import com.bp.pensionline.authenticate.exception.DAOException;

public class PLSelfService {
	static Logger LOG = Logger.getLogger(PLSelfService.class);
	public boolean isMigrated(String ninoName) throws DAOException {
		return (new SelfServiceDataAccess()).isMigrated(ninoName);
	}
	
	public boolean existASDRow(String username) {
		boolean result = false;
		try {
			ASDDataAccess dao = new ASDDataAccess();
			result =  dao.existASDRow(username);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public String getNINOForPLID(String plName) {
		String result = null;
		try {
			ASDDataAccess dao = new ASDDataAccess();
			result =  dao.getNINOForPLID(plName);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public String getREFNOForMigrated(String ninoName) {
		String result = null;
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			result =  dao.getREFNOForMigrated(ninoName);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public void updateMigration(ASD asd, String ninoName) throws DAOException {
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			dao.updateMigration(asd, ninoName);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
	}
	
	public String getUserID(UserInfo info) throws DAOException {
		String result = null;
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			result = dao.getUserID(info);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public String getRefNo(String ninoName) throws DAOException {
		String result = null;
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			result = dao.getRefNo(ninoName);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public UserInfo getUserInfo(UserInfo info) throws DAOException {
		UserInfo result = null;
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			result = dao.getUserInfo(info);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
	
	public boolean hasWelcomePack(String ninoName) throws DAOException {
		boolean result = false;
		try {
			SelfServiceDataAccess dao = new SelfServiceDataAccess();
			result =  dao.hasWelcomePack(ninoName);
		} catch (DAOException de) {
			LOG.error(de.getMessage());
		}
		
		return result;
	}
}
