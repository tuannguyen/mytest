/**
 * 
 */
package com.bp.pensionline.cmsaccount;

import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

/**
 * @author BachLe
 *
 */
public class AccountDeletionThread extends Thread {
	public static final Log LOG = CmsLog.getLog(AccountDeletionThread.class);
	
	private List<String> mAccounts;
	private long mWaitTime = 60000; //default is 1 minutes
	private int min = 1;
	
	public AccountDeletionThread(List<String> accounts, long waitTime) {
		mAccounts = accounts;
		mWaitTime = waitTime;
		min = (int)(mWaitTime/60000);
	}
	
	/**
     * Starts the thread to index a single resource.<p>
     * @see java.lang.Runnable#run()
     */
    @Override
	public void run() {
    	//send messages
    	AccountUtil au = new AccountUtil();
    	CmsObject cmso = au.getCmsAdminObject();
    	List<String> sessionIds = au.getLoggedInIds(mAccounts);
    	String msg = "Please log out, your account will be disabled in "+min+" minutes!";    	
    	for (int i=0; i<sessionIds.size(); i++) {
    		OpenCms.getSessionManager().sendBroadcast(cmso, msg, sessionIds.get(i));
    	}
    	
    	//remove
    	if (sessionIds.size() > 0) {
    		try {
        		LOG.info("Thead is waiting for deleting executed");
        		sleep(mWaitTime+120000);
        		LOG.info("Start deleting");
        		au.removeAccounts(mAccounts);
        	} catch (InterruptedException ie) {
        		LOG.error("Could not cause this thread to sleep !");
        	} catch (Exception ex) {
        		LOG.error("Exception occurred: "+ex.toString());
        		ex.printStackTrace();
        	}
    	}
    }
}
