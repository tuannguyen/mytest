/**
 * 
 */
package com.bp.pensionline.cmsaccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

/**
 * @author BachLe
 *
 */
public class AccountManager extends Thread {
	public static final Log LOG = CmsLog.getLog(AccountManager.class);
	
	public static final String ACTION_CREATE = "CREATE_ACCOUNT";
	public static final String ACTION_REMOVE = "REMOVE_ACCOUNT";
	
	private String mAction;
	
	/**
	 * Constructor
	 * @param action
	 */
	public AccountManager(String action) {
		mAction = action;
	}
	
	/**
     * Starts the thread to index a single resource.<p>
     * @see java.lang.Runnable#run()
     */
    @Override
	public void run() {
    	AccountUtil au = new AccountUtil();
    	Properties p = au.loadConfiguration();
    	List<String> accounts = new ArrayList<String>();
    	List<String> loggedIns = new ArrayList<String>();
    	List<String> allSessions = au.getLoggedInAccounts();
    	Map<String, List<String>> groups = new HashMap<String, List<String>>();
    	
    	if (!p.isEmpty()) {
    		int i=0;
    		try {
    			String password = p.getProperty(Constant.PASSWORD);
    			String waitTime = p.getProperty(Constant.WAIT_TIME);
        		Iterator keys = p.keySet().iterator();
        		boolean isLoggedIn = false;
        		while (keys.hasNext()) {
        			isLoggedIn = false;
        			String account = (String)keys.next();
        			if ("password".equals(account) || "waitTime".equals(account)) continue;
        			String v = p.getProperty(account);
        			List<String> group = new ArrayList<String>();
        			String[] g = v.split(",");
        			for (i=0; i<g.length; i++) {
        				group.add(g[i]);
        			}
        			//check if account is being logged in
    				for (int j=0; j<allSessions.size(); j++) {
    					if (account.equals(allSessions.get(j))) {
    						isLoggedIn = true;
    						break;
    					}
    				}
        			groups.put(account, group);
        			if (isLoggedIn) {
        				loggedIns.add(account);
        			} else {
        				accounts.add(account);
        			}
        		}
        		if (ACTION_CREATE.equals(mAction)) {
        			au.createAccounts(accounts, groups, password);
        			Constant.isCMGAccountDisabled = false;
        		} else {
        			au.removeAccounts(accounts);
        			Constant.isCMGAccountDisabled = true;
        			//other thread for deleting logged in session
        			LOG.info("Current logged in: "+loggedIns.size());
        			AccountDeletionThread t = new AccountDeletionThread(loggedIns, Long.valueOf(waitTime));
        			t.start();
        		}
    		} catch (Exception e) {
    			LOG.error("EXCEPTION occurred "+e.toString());
    		}
    	}
    }
}
