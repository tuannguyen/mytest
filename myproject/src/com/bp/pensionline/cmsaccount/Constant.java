package com.bp.pensionline.cmsaccount;

public class Constant {
	
	public static final String CONFIG_FILE = "/system/modules/com.bp.pensionline.cmsaccount/config.properties";
	
	public static final String PASSWORD   = "password";
	public static final String WAIT_TIME  = "waitTime";
	
	public static boolean isCMGAccountDisabled = false;
	public static long configWaitTime          = -1;
}
