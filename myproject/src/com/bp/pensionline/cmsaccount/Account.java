/**
 * 
 */
package com.bp.pensionline.cmsaccount;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BachLe
 *
 */
public class Account {
	private String userName;
	private boolean isDisabled = false;
	private boolean isLoggedIn = false;
	private List<String> groups = new ArrayList<String>();
	
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param isDisabled the isDisabled to set
	 */
	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}
	/**
	 * @return the isDisabled
	 */
	public boolean isDisabled() {
		return isDisabled;
	}
	/**
	 * @param isLoggedIn the isLoggedIn to set
	 */
	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	/**
	 * @return the isLoggedIn
	 */
	public boolean isLoggedIn() {
		return isLoggedIn;
	}
	/**
	 * @param groups the groups to set
	 */
	public void setGroups(String[] pGroups) {
		for (int i=0; i<pGroups.length; i++) {
			groups.add(pGroups[i]);
		}
	}
	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	/**
	 * @return the groups
	 */
	public List<String> getGroups() {
		return groups;
	}
	/**
	 * @return the groups string
	 */
	public String getGroupsAsString() {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<groups.size(); i++) {
			buf.append(", "+groups.get(i));
		}
		if (buf.indexOf(",")!=-1) {
			return buf.substring(2);
		} else {
			return "";
		}
	}
}
