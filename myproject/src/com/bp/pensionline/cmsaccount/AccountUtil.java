/**
 * Utility class for creating/deleting CMS accounts
 */
package com.bp.pensionline.cmsaccount;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsContextInfo;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.CmsSessionInfo;
import org.opencms.main.CmsSystemInfo;
import org.opencms.main.OpenCms;
import org.opencms.util.CmsPropertyUtils;

/**
 * @author BachLe
 *
 */
public class AccountUtil {
	private static String configPath = OpenCms.getSystemInfo().
										getAbsoluteRfsPathRelativeToWebInf(CmsSystemInfo.FOLDER_CONFIG + 
																		   CmsSystemInfo.FILE_PROPERTIES);
	
	public static final Log LOG = CmsLog.getLog(AccountUtil.class);
	
	private static CmsObject adminObj = null;
	
	/**
	 * Delete CMS accounts
	 * @param accounts list of accounts to be deleted
	 * @throws Exception
	 */
	public void removeAccounts(List<String> accounts) throws Exception {
		CmsObject cmso = getCmsAdminObject();
		if (cmso != null) {
			for (int i=0; i<accounts.size(); i++) {
				cmso.deleteUser(accounts.get(i));
			}
			LOG.info(accounts.size()+" accounts have been removed");
		}
	}
	
	/**
	 * Create CMS accounts and assign to given CMS groups
	 * @param accounts list of accounts to be created
	 * @param groups groups for each account
	 * @param password password is the same for all accounts
	 * @throws Exception
	 */
	public void createAccounts(List<String> accounts, Map<String, List<String>> groups, String password) throws Exception {
		CmsObject cmso = getCmsAdminObject();
		if (cmso != null) {
			for (int i=0; i<accounts.size(); i++) {
				try {
					CmsUser u = cmso.createUser(accounts.get(i), password, "C-MG", null);
					List<String> groupName = groups.get(accounts.get(i));
					for (int j=0; j<groupName.size(); j++) {
						cmso.addUserToGroup(u.getName(), groupName.get(j));
					}
				} catch (CmsException cme) {
					LOG.error("EXCEPTION occurred: "+cme.toString());
				}
			}
		}
	}
	
	/**
	 * Get accounts from configuration file
	 * @return
	 */
	public List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<Account>();
		Properties p = loadConfiguration();
		if (p != null) {
			//get account names from configuration file
			Iterator keys = p.keySet().iterator();
			List<String> loggedIns = getLoggedInAccounts();
    		while (keys.hasNext()) {
    			Account a = new Account();
    			String username = (String)keys.next();
    			if ("password".equals(username) || "waitTime".equals(username)) continue;
    			String[] groups = p.getProperty(username).split(",");
    			if (Constant.isCMGAccountDisabled | !isAccountExist(username)) {
    				a.setDisabled(true);//not existing means being disabled
    				Constant.isCMGAccountDisabled = true; //temporarily, all is disabled if one get disabled
    			} else {
    				//check if account is being logged in
    				for (int i=0; i<loggedIns.size(); i++) {
    					if (username.equals(loggedIns.get(i))) {
    						a.setLoggedIn(true);
    						break;
    					}
    				}
    			}
    			a.setUserName(username);
    			a.setGroups(groups);
    			accounts.add(a);
    		}
    		//get wait time
    		Constant.configWaitTime = Long.valueOf(p.getProperty(Constant.WAIT_TIME));
		}
		return accounts;
	}
	
	/**
	 * @return all current logged in users
	 */
	public List<String> getLoggedInAccounts() {
		List<String> ids = new ArrayList<String>();
        Iterator itSessions = OpenCms.getSessionManager().getSessionInfos().iterator();
        CmsObject cmso = getCmsAdminObject();
        while (itSessions.hasNext()) {
            CmsSessionInfo sessionInfo = (CmsSessionInfo)itSessions.next();
            CmsUser user;
            try {
                user = cmso.readUser(sessionInfo.getUserId());
            } catch (CmsException e) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(e.getLocalizedMessage(), e);
                }
                continue;
            }
            ids.add(user.getName());
        }
        LOG.info("Current logged in size: "+ids.size());
        return ids;
	}
	
	/**
	 * @return all current logged in users
	 */
	public List<CmsUser> getLoggedInUsers() {
		List<CmsUser> users = new ArrayList<CmsUser>();
        Iterator itSessions = OpenCms.getSessionManager().getSessionInfos().iterator();
        CmsObject cmso = getCmsAdminObject();
        while (itSessions.hasNext()) {
            CmsSessionInfo sessionInfo = (CmsSessionInfo)itSessions.next();
            CmsUser user;
            try {
                user = cmso.readUser(sessionInfo.getUserId());
            } catch (CmsException e) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(e.getLocalizedMessage(), e);
                }
                continue;
            }
            users.add(user);
        }
        LOG.info("Current logged in size: "+users.size());
        return users;
	}
	
	/**
	 * @return session IDs of given users
	 */
	public List<String> getLoggedInIds(List<String> accounts) {
		List<String> ids = new ArrayList<String>();
		Iterator itSessions = OpenCms.getSessionManager().getSessionInfos().iterator();
        CmsObject cmso = getCmsAdminObject();
        while (itSessions.hasNext()) {
            CmsSessionInfo sessionInfo = (CmsSessionInfo)itSessions.next();
            CmsUser user;
            try {
                user = cmso.readUser(sessionInfo.getUserId());
            } catch (CmsException e) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(e.getLocalizedMessage(), e);
                }
                continue;
            }
            for (int i=0; i<accounts.size(); i++) {
            	if (accounts.get(i).equals(user.getName())) {
            		//CmsUUID sId = sessionInfo.getSessionId();
            		ids.add(sessionInfo.getSessionId().toString());
            		break;
            	}
            }
        }
        LOG.info("Logged in sessions: "+ids.size());
		return ids;
	}
	
	/**
	 * Check an account if it is existing in the system
	 * @param account
	 * @return
	 */
	private boolean isAccountExist(String account) {
		CmsObject cmso = getCmsAdminObject();
		if (cmso != null) {
			try {
				CmsUser u = cmso.readUser(account);
				if (u != null) return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}
	
	/**
	 * get information from configuration file
	 * @return
	 */
	public Properties loadConfiguration() {
		Properties pros = new Properties();
		CmsObject cmso = getCmsAdminObject();
		if (cmso != null) {
			try {
				if (cmso.existsResource(Constant.CONFIG_FILE)) {
					CmsFile pf = cmso.readFile(Constant.CONFIG_FILE);
					byte[] arr = pf.getContents();
					ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
					pros.load(bIn);
				}
			} catch (CmsException cme) {
				LOG.error("EXCEPTION occurred: "+cme.toString());
			} catch (IOException ioe) {
				LOG.error("EXCEPTION occurred: "+ioe.toString());
			}
		}
		return pros;
	}
	/**
	 * Get system Administrator account
	 * @return
	 */
	public static synchronized CmsObject getCmsAdminObject() {
		try {		
			if(adminObj == null) {
				String adminUsername = getStringValue("adminName");
				String adminPassword = getStringValue("adminPassword");				
				
				CmsObject newAdminObj = OpenCms.initCmsObject(OpenCms.getDefaultUsers().getUserGuest());	
				String adminLoggedInName = newAdminObj.loginUser(adminUsername, adminPassword, CmsContextInfo.LOCALHOST);
				//LOG.info("adminLoggedInName: " + adminLoggedInName);
				if (adminLoggedInName != null && adminLoggedInName.equals(adminUsername)) {
					adminObj = newAdminObj;					
				}
			}
			adminObj.getRequestContext().setCurrentProject(adminObj.readProject("Offline"));
			adminObj.getRequestContext().setSiteRoot("/");
			return adminObj;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("EXCEPTION occurred: " + e);
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Load properties from opencms.properties
	 * @return
	 * @throws IOException
	 */
	public static ExtendedProperties getConfigurations () throws IOException {
		return CmsPropertyUtils.loadProperties(configPath);
	}
	
	public static String getStringValue(String key) {
		try  {
			return getConfigurations().getString(key);
		}  catch (Exception e)  {
			LOG.error("Error in getStringValue from configuration: " + key + " : " + e);
			return null;
		}
	}
}
