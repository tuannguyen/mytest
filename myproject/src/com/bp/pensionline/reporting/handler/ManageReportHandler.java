package com.bp.pensionline.reporting.handler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsUUID;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.reporting.constants.Constant;
import com.bp.pensionline.reporting.dto.ReportDTO;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.bp.pensionline.wiki.XmlFaqDao;

public class ManageReportHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(ManageReportHandler.class);
	
	public static final String REPORT_BASE_FOLDER = CheckConfigurationKey.getStringValue("webstat.base");
	public static final String REPORT_WEB_FOLDER = CheckConfigurationKey.getStringValue("report.web");
	
	private static final long serialVersionUID = 1L;		
	
	private String reportAction;
	private String reportId = null;
	private String reportTitle;
	private String reportXMLString = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		boolean isReloadPage = true;

		if (parseRequestXml(xml))
		{
			if (reportId == null || reportId.trim().equals(""))
			{
				reportId = new CmsUUID().toString();
			}			
			
			updateReportsToFileSystem(reportAction, reportId, reportXMLString);
			
			actionResult = true;
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, isReloadPage);
		
		out.print(xmlResponse);
		out.close();

	}
	
	public boolean updateReportsToFileSystem(String reportAction, String reportId, String newReportContent)
	{
		try
		{
			if (reportAction != null)
			{
				if (reportAction.equals("Delete") || reportAction.equals("Edit"))
				{
					if (reportId != null && !reportId.trim().equals(""))
					{						
						// delete the old file
						File oldFile = new File(REPORT_BASE_FOLDER  + "XML/" + reportId + ".xml");
						if (oldFile.exists())
						{
							oldFile.delete();
							LOG.info("Delete report xml file: " + reportId);
						}
					}
				}
				
				if (reportAction.equals("Edit") || reportAction.equals("Add"))
				{
					// create a new report file
					if (reportId != null && !reportId.trim().equals(""))
					{
						OutputStreamWriter xmlWriter 
							= new OutputStreamWriter(new FileOutputStream(REPORT_BASE_FOLDER + "XML/" + reportId + ".xml"));
						xmlWriter.write(newReportContent);
						xmlWriter.flush();
						xmlWriter.close();
						
						return true;
					}
				}					
			}		
		}
		catch (Exception e)
		{
			LOG.error("Error while update reports: " + e);
		}
		
		return false;
	}

	public String buildXmlResponse(boolean isActionOk, boolean willReloadPage) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <RpActionResult>").append(isActionOk).append("</RpActionResult>\n");
		buffer.append("     <RpPageReload>").append(willReloadPage).append("</RpPageReload>\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		xml = xml.replaceAll("\\(plus\\)", "+");
		
		String comment = "";
		StringBuffer tablesBuf = new StringBuffer("<").append(Constant.XML_WEBSTAT_REPORT_TABLES).append(">");
		StringBuffer reportXMLBuf = new StringBuffer("<").append(Constant.XML_WEBSTAT_REPORT).append(">");
		
		//LOG.info("parseRequestXml xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_ACTION))
					{
						reportAction = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_ID))
					{
						reportId = paramNode.getTextContent();
						if (reportAction != null && reportAction.equals("Delete"))
						{
							return true;
						}
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TITLE))
					{
						//LOG.info(" getting ptbPageURI: " + paramNode.getTextContent());
						reportTitle = paramNode.getTextContent();
						if (reportTitle == null || reportTitle.trim().equals(""))
						{
							return false;
						}
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_COMMENT))
					{
						//LOG.info(" getting ptbAction: " + paramNode.getTextContent());
						comment = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLES))
					{
						//LOG.info(" getting ptbReason: " + paramNode.getTextContent());

					}					
				}
			}
			// build the tables element
			NodeList tableList = root.getElementsByTagName(Constant.AJAX_WEBSTAT_REPORT_TABLE);		
			if (tableList != null && tableList.getLength() == 0)
			{
				return false;
			}
			for (int i = 0; i < tableList.getLength(); i++)
			{
				Node table = tableList.item(i);
				if (table != null)
				{
					tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE).append(">");
					NodeList tableChildren = table.getChildNodes();
					// append table id
					tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE_ID).append(">")
						.append("tb" + i).append("</").append(Constant.XML_WEBSTAT_REPORT_TABLE_ID).append(">");
										
					for (int j = 0; j < tableChildren.getLength(); j++)
					{
						Node tableChild = tableChildren.item(j);
						if (tableChild != null && tableChild.getNodeType() == Node.ELEMENT_NODE)
						{
							if (tableChild.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_TITLE))
							{
								String tableTitle = tableChild.getTextContent();
								tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE_TITLE).append("><![CDATA[")
									.append(tableTitle).append("]]></").append(Constant.XML_WEBSTAT_REPORT_TABLE_TITLE).append(">");
							}
							else if (tableChild.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_QUERY))
							{
								String tableQuery = tableChild.getTextContent();
								tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE_QUERY).append("><![CDATA[")
									.append(tableQuery).append("]]></").append(Constant.XML_WEBSTAT_REPORT_TABLE_QUERY).append(">");
							}
							else if (tableChild.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_CHARTTYPE))
							{
								String tableChartType = tableChild.getTextContent();
								tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE_CHARTTYPE).append(">")
									.append(tableChartType).append("</").append(Constant.XML_WEBSTAT_REPORT_TABLE_CHARTTYPE).append(">");
							}
							else if (tableChild.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_NOTE))
							{
								String tableNote = tableChild.getTextContent();								
								tablesBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TABLE_NOTE).append("><![CDATA[")
									.append(tableNote).append("]]></").append(Constant.XML_WEBSTAT_REPORT_TABLE_NOTE).append(">");
							}													
						}
					}
					tablesBuf.append("</").append(Constant.XML_WEBSTAT_REPORT_TABLE).append(">");
				}
			}
			
			tablesBuf.append("</").append(Constant.XML_WEBSTAT_REPORT_TABLES).append(">");

			bais.close();

			reportXMLBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_TITLE).append("><![CDATA[").
				append(reportTitle).append("]]></").append(Constant.XML_WEBSTAT_REPORT_TITLE).append(">");
			reportXMLBuf.append("<").append(Constant.XML_WEBSTAT_REPORT_COMMENT).append("><![CDATA[").
				append(comment).append("]]></").append(Constant.XML_WEBSTAT_REPORT_COMMENT).append(">");			
			reportXMLBuf.append(tablesBuf);
			reportXMLBuf.append("</").append(Constant.XML_WEBSTAT_REPORT).append(">");
			
			reportXMLString = reportXMLBuf.toString();
			
			LOG.info("report action: " + reportAction);
			LOG.info("reportXMLString: " + reportXMLString);
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing com.bp.shipping.reporting request XML: " + ex.toString());
			ex.printStackTrace();
		}
		

		return false;

	}	
	
	
	public static Vector listAvailableReports()
	{
		Vector<ReportDTO> reports = new Vector<ReportDTO>();
		
		// list all xml files in REPORT_BASE_FOLDER
		try
		{
			String xmlFolder = REPORT_BASE_FOLDER + "XML/";
			File reportFolder = new File(xmlFolder.substring(0, xmlFolder.length() - 1));
			
			if (reportFolder.isDirectory())
			{
				System.out.println("Get report in folder: "+reportFolder.getAbsolutePath());
				File[] reportFiles = reportFolder.listFiles();
				
				// sort the file name based on alphabet order
				TreeSet<ReportDTO> reportTree = new TreeSet<ReportDTO>();
				
				for (int i = 0; i < reportFiles.length; i++)
				{
					File reportFile = reportFiles[i];
					if (reportFile != null && reportFile.isFile() && reportFile.getName().endsWith(".xml"))
					{
						ReportDTO reportDTO = new ReportDTO(reportFile);
						// LOG.debug("Adding report to tree...: " + reportMetaDataDTO.getTitle());
						reportTree.add(reportDTO);
					}
					
				}
				
				Iterator<ReportDTO> reportTreeIter = reportTree.iterator();
				while (reportTreeIter.hasNext())
				{
					ReportDTO reportDTO = reportTreeIter.next();					
					reports.add(reportDTO);					
				}
			}
		} 
		catch (Exception e)
		{
			LOG.error("Error in reading XML report files: " + e);
		}
		LOG.info("There are "+reports.size()+" reports in folder "+REPORT_BASE_FOLDER);
		return reports;
	}	


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
