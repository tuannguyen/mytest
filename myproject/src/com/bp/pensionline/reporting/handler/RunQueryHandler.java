package com.bp.pensionline.reporting.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.reporting.constants.Constant;
import com.bp.pensionline.reporting.producer.ReportProducer;

public class RunQueryHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_XML_FOLDER = "D:/report_xml/";
	
	private static final long serialVersionUID = 1L;		
	
	private String tableTitle = null;
	private String tableQuery = null;
	private String tableNote = null;
	private String tableChartType = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		String outputType = null;

		if (parseRequestXml(xml))
		{
			String reportOutputHtml = ReportProducer.buildQueryHtmlOutput(tableTitle, tableQuery, tableNote, tableChartType);

			if (reportOutputHtml != null)
			{
				actionResult = true;
				outputType = "HTML";
				request.getSession().setAttribute("reportHtmlContent", reportOutputHtml);					
			}		
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, outputType);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, String reportOutputType) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <rp_actionResult>").append(isActionOk).append("</rp_actionResult>\n");
		buffer.append("     <rp_reportOutput>").append(reportOutputType).append("</rp_reportOutput>\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{		
		//LOG.info("parseRequestXml xml: " + xml);
		xml = xml.replaceAll("\\(amp\\)", "&");
		xml = xml.replaceAll("\\(gt\\)", ">");
		xml = xml.replaceAll("\\(lt\\)", "<");
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_TITLE))
					{
						tableTitle = paramNode.getTextContent();						
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_QUERY))
					{
						tableQuery = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_NOTE))
					{
						tableNote = paramNode.getTextContent();
						LOG.info("tableNote: " + tableNote);
					}					
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE_CHARTTYPE))
					{
						tableChartType = paramNode.getTextContent();
					}					
				}
			}
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
