package com.bp.pensionline.reporting.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.reporting.constants.Constant;
import com.bp.pensionline.reporting.dto.ReportDTO;
import com.bp.pensionline.reporting.dto.ReportTableDTO;
import com.bp.pensionline.reporting.producer.ReportProducer;

public class RunReportHandler extends HttpServlet {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	
	private static final long serialVersionUID = 1L;		
	
	private Vector<ReportDTO> selectedReports = new Vector<ReportDTO>();	
	private String reportStartDate = null;
	private String reportEndDate = null;
	private String reportOutputType = null;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		response.setContentType(Constant.CONTENT_TYPE);
		PrintWriter out = response.getWriter();

		// get request
		String xml = request.getParameter(Constant.PARAM);		
		
		//CmsUser currentUser = SystemAccount.getCurrentUser(request);
		String xmlResponse = null;
		boolean actionResult = false;
		String outputType = null;
		selectedReports.clear();

		if (parseRequestXml(xml))
		{
			if (reportOutputType != null && reportOutputType.equals("BROWSER_TABLE") && 
					selectedReports != null && selectedReports.size() > 0)
			{
				String reportOutputHtml = ReportProducer.buildSelectedReportHtmlOutput(selectedReports, reportStartDate, reportEndDate);

				if (reportOutputHtml != null)
				{
					actionResult = true;
					outputType = "HTML";
					request.getSession().setAttribute("reportHtmlContent", reportOutputHtml);					
				}					
			}
			else if (reportOutputType != null && reportOutputType.equals("DOWNLOAD_PDF")  && 
					selectedReports != null && selectedReports.size() > 0)
			{
				byte[] reportOutputPdf = ReportProducer.buildSelectedReportPDFOutput(selectedReports, reportStartDate, reportEndDate);
				
								
				if (reportOutputPdf != null)
				{
					LOG.info("reportOutputPdf length: " + reportOutputPdf.length);
					actionResult = true;
					outputType = "PDF";
					request.getSession().setAttribute("reportPdfContent", reportOutputPdf);
				}					
			}
		}
		else
		{
			LOG.info("Error in parsing request XML");
		}
		
		xmlResponse = buildXmlResponse(actionResult, outputType);
		
		out.print(xmlResponse);
		out.close();

	}

	public String buildXmlResponse(boolean isActionOk, String reportOutputType) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <rp_actionResult>").append(isActionOk).append("</rp_actionResult>\n");
		buffer.append("     <rp_reportOutput>").append(reportOutputType).append("</rp_reportOutput>\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	public String buildXmlResponseError(String message) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		buffer.append("     <Error>").append(message).append("</Error>").append("\n");
		buffer.append("</").append(Constant.AJAX_WEBSTAT_REPORT_RESPONSE).append(">\n");
		
		return buffer.toString();
	}
	
	private boolean parseRequestXml(String xml)
	{				
		//LOG.info("xml: " + xml);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();

			ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

			document = builder.parse(bais);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();	
			
			int nlength = paramNodes.getLength();
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORTS))
					{
						NodeList reportList = paramNode.getChildNodes();
						for (int j = 0; j < reportList.getLength(); j++)
						{
							Node reportNode = reportList.item(j);
							if (reportNode != null && reportNode.getNodeType() == Node.ELEMENT_NODE 
									&& reportNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT))
							{
								// get the selected report Id and it selected tables and put
								// to the map with key is reportId
								NodeList reportDetails = reportNode.getChildNodes();
								String selectedReportId = null;
								ArrayList<ReportTableDTO> selectedTables = new ArrayList<ReportTableDTO>();
								
								for (int k = 0; k < reportDetails.getLength(); k++)
								{
									Node reportDetail = reportDetails.item(k);
									
									if (reportDetail != null && reportDetail.getNodeType() == Node.ELEMENT_NODE
											&& reportDetail.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_ID))
									{
										selectedReportId = reportDetail.getTextContent();
									}
									else if (reportDetail != null && reportDetail.getNodeType() == Node.ELEMENT_NODE
											&& reportDetail.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLES))
									{
										NodeList tables = reportDetail.getChildNodes();
										for (int l = 0; l < tables.getLength(); l++)
										{
											Node table = tables.item(l);
											if (table != null && table.getNodeType() == Node.ELEMENT_NODE &&
													table.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_TABLE))
											{
												ReportTableDTO tableDTO = new ReportTableDTO();
												tableDTO.setTableId(table.getTextContent());
												selectedTables.add(tableDTO);
											}
										}
									}
								}
								
								
								if (selectedReportId != null)
								{
									ReportDTO reportDTO = new ReportDTO();
									reportDTO.setReportId(selectedReportId);
									reportDTO.setTables(selectedTables);
									selectedReports.add(reportDTO);
								}								
							}
						}
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_STARTDATE))
					{
						reportStartDate = paramNode.getTextContent();
					}
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_ENDDATE))
					{
						reportEndDate = paramNode.getTextContent();
					}									
					if (paramNode.getNodeName().equals(Constant.AJAX_WEBSTAT_REPORT_OUTPUTTYPE))
					{
						//LOG.info(" getting ptbAction: " + paramNode.getTextContent());
						reportOutputType = paramNode.getTextContent();				
					}									
				}
			}
			
			LOG.info("number of Reports 2: " + selectedReports.size());
			LOG.info("reportStartDate: " + reportStartDate);
			LOG.info("reportEndDate: " + reportEndDate);
			
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Error in parsing run report request XML: " + ex.toString());
		}
		

		return false;

	}	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
