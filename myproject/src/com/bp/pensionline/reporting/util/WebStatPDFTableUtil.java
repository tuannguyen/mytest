package com.bp.pensionline.reporting.util;

import java.awt.Color;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
/*
 * This class use iText engine to generate PDF reports from table input.
 */

public class WebStatPDFTableUtil extends PdfPTable
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final float[][] COLUMN_PERCENT_WIDTH = {
		new float[] {100},
		new float[] {70, 30},
		new float[] {50, 30, 20},
		new float[] {40, 20, 20, 20}
		};
	
	public static PdfPTable createWebStatDataTable (String tableTitle, Vector<String> colNames, Vector<Vector<String>> tableRows)
	{	
		PdfPTable pdfTable = null;
		float[] colWidths = new float[colNames.size()];
		for (int i = 0; i < colWidths.length; i++)
		{
			colWidths[i] = 100/colWidths.length;
		}

		if (colNames != null && colNames.size() > 0)
		{
			//pdfTable = new PdfPTable(COLUMN_PERCENT_WIDTH[colNames.size() - 1]);
			pdfTable = new PdfPTable(colWidths);
//			LOG.info("Table width: " + pdfTable.getTotalWidth());

//			Remove table title, move it to a <h2> text			
//			if (tableTitle != null )
//			{
//				// Add table title cell
//				PdfPCell titleCell = new PdfPCell(new Paragraph(
//						tableTitle, FontFactory.getFont(FontFactory.HELVETICA,
//								12,
//								Font.BOLD, new Color(255, 255, 255))));
//				titleCell.setColspan(colNames.size());
//				titleCell.setBackgroundColor(new Color(0x00, 0x90, 0x00));
//				titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				titleCell.setVerticalAlignment(Element.ALIGN_TOP);
//				
//				//titleCell.setPaddingTop(5);
//				pdfTable.addCell(titleCell);
//			}
			//LOG.debug("table total width: " + pdfTable.getTotalWidth());

			// Add column names
			for (int i = 0; i < colNames.size(); i++)
			{
				PdfPCell columNameCell = new PdfPCell(new Paragraph(colNames.elementAt(i), 
						new Font(Font.TIMES_ROMAN, 10, Font.BOLD, new Color(0xff, 0xff, 0xff))));
				columNameCell.setBackgroundColor(new Color(0x00, 0x90, 0x00));
				pdfTable.addCell(columNameCell);			
			}

			// Add rows
			if (tableRows != null && tableRows.size() > 0)
			{
				for (int i = 0; i < tableRows.size(); i++)
				{
					Vector<String> row = tableRows.elementAt(i);
					if (row != null)
					{
						for (int j = 0; j < row.size(); j++)
						{
							PdfPCell dataCell = new PdfPCell(new Paragraph(row.elementAt(j), 
									new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(0x06, 0x06, 0x06))));
							pdfTable.addCell(dataCell);
						}
					}
				}
			}			
		}

		
		return pdfTable;
	}
	
	public static PdfPTable createWebStatChartTable (Image chartImage)
	{	
		PdfPTable pdfTable = null;

		if (chartImage != null)
		{
			pdfTable = new PdfPTable(COLUMN_PERCENT_WIDTH[0]);
			
			// Add table title cell
			PdfPCell imgCell = new PdfPCell(chartImage, false);
			imgCell.setColspan(1);
			imgCell.setBorder(0);
			
			//titleCell.setPaddingTop(5);
			pdfTable.addCell(imgCell);	
		}

		
		return pdfTable;
	}	
}
