package com.bp.pensionline.reporting.util;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.reporting.dto.PDFReportContentDTO;
import com.bp.pensionline.reporting.dto.PDFReportTableContentDTO;
import com.bp.pensionline.reporting.handler.ManageReportHandler;
import com.bp.pensionline.reporting.producer.ReportProducer;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class WebStatPDFExporter
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static final String REPORT_AUTHOR = CheckConfigurationKey.getStringValue("report.author");
	public static final String DEFAULT_DOCUMENT_TITLE = "All webstats reports";
	public static byte[] generatePDFBytes (PDFReportContentDTO pdfReportContentDTO)
	{
		byte[] result = null;
		if (pdfReportContentDTO != null)
		{
			String reportTitle = pdfReportContentDTO.getReportTitle(); 
			String reportComment = pdfReportContentDTO.getReportComment(); 
			ArrayList<PDFReportTableContentDTO> pdfTables = pdfReportContentDTO.getPdfTables();
			
			Document pdf = new Document();		
			ByteArrayOutputStream pdfByteArrayOutputStream = new ByteArrayOutputStream();
			try
			{
				//ByteArrayOutputStream pdfByteOutput = new ByteArrayOutputStream();
				PdfWriter writer = PdfWriter.getInstance(pdf, pdfByteArrayOutputStream);
				
				//insert image in sequence
				//writer.setStrictImageSequence(true);

				
				// general information of PDF report
				pdf.addAuthor(REPORT_AUTHOR);
				
				HeaderFooter header = new HeaderFooter(new Phrase(" "), false);
				header.setAlignment(Element.ALIGN_LEFT);
				header.setBorderColor(new Color(0x00, 0x90, 0x00));
				header.setBorderWidthBottom(1);	
				header.setBorderWidthTop(0);
				
				HeaderFooter footer = new HeaderFooter(new Phrase("Author : " + REPORT_AUTHOR), false);
				footer.setAlignment(Element.ALIGN_LEFT);
				footer.setBorderColor(new Color(0x00, 0x90, 0x00));				
				footer.setBorderWidthBottom(0);
				footer.setBorderWidthTop(1);				
				
				pdf.setFooter(footer);
				pdf.setHeader(header);
				
				// add logo and banner image
				writer.setPageEvent(new WebStatPageEvents());
				
				if (reportTitle != null)
				{
					// Add title of report
					pdf.addTitle(reportTitle);
					pdf.addSubject(reportTitle);
					pdf.addKeywords("pensionline, " + reportTitle.replaceAll(" ", ", "));	
				}
				
				// open the document
				pdf.open();	
				
				Paragraph blankPara = new Paragraph(" ");
				
				if (reportTitle != null)
				{
					Paragraph titlePara = new Paragraph(reportTitle, 
							new Font(Font.TIMES_ROMAN, 20, Font.BOLD, new Color(0x09, 0xc0, 0x00)));
					titlePara.setAlignment(Element.ALIGN_LEFT);
					pdf.add(titlePara);
					pdf.add(blankPara);
				}
				
				if (reportComment != null)
				{
					Paragraph commentPara = new Paragraph(reportComment, 
							new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0x006, 0x06, 0x06)));
					commentPara.setAlignment(Element.ALIGN_LEFT);
					pdf.add(commentPara);
					pdf.add(blankPara);
				}
				
				// add tables
				if (pdfTables != null)
				{
					for (int i = 0; i < pdfTables.size(); i++)
					{
						PDFReportTableContentDTO pdfTable = pdfTables.get(i);
						PdfPTable pdfPTable = pdfTable.getTableContent();
						String chartPngFilename = pdfTable.getChartImageFilename();
						
						if (pdfPTable != null)
						{
							pdfPTable.setHorizontalAlignment(Element.ALIGN_LEFT);
							pdf.add(pdfPTable);
							pdf.add(blankPara);
							
							if (chartPngFilename != null)
							{
								Image png = Image.getInstance(chartPngFilename);
								if (png != null)
								{
//									png.setAlignment(Element.ALIGN_LEFT);						
//									png.scalePercent(60);
//									pdf.add(png);
//									pdf.add(blankPara);
									PdfPTable imgTable = WebStatPDFTableUtil.createWebStatChartTable(png);
									imgTable.setHorizontalAlignment(Element.ALIGN_LEFT);
									if (imgTable != null)
									{
										pdf.add(imgTable);
										pdf.add(blankPara);
									}
								}
							}							
						}					
					}
				}
				

				pdf.close();
				result = pdfByteArrayOutputStream.toByteArray();
				
			}
			catch (Exception e)
			{
				LOG.error("Error in creating PDF report document: " + e.toString());
			}	
		}

		
		return result;
	}
	
	public static byte[] generateMultiPDFBytes (Vector<PDFReportContentDTO> pdfReportContentDTOs, 
			String startDate, String endDate)
	{
		byte[] result = null;
		if (pdfReportContentDTOs != null && pdfReportContentDTOs.size() > 0)
		{
			Document pdf = new Document();	
			
			ByteArrayOutputStream pdfByteArrayOutputStream = new ByteArrayOutputStream();
			try
			{
				//ByteArrayOutputStream pdfByteOutput = new ByteArrayOutputStream();
				PdfWriter writer = PdfWriter.getInstance(pdf, pdfByteArrayOutputStream);
				
				//insert image in sequence
				//writer.setStrictImageSequence(true);
				if (startDate == null || startDate.trim().equals(""))
				{
					startDate = "              ";
				}				
				if (endDate == null|| endDate.trim().equals(""))
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat(ReportProducer.REPORT_DATE_FORMAT);
					endDate = dateFormat.format(new Date());
				}
				
				StringBuffer blankPaddingBuff = new StringBuffer();
				for (int i = 0; i < 60; i++)
				{
					blankPaddingBuff.append(" ");
				}

				
				// general information of PDF report
				pdf.addAuthor(REPORT_AUTHOR);
				
				HeaderFooter header = new HeaderFooter(new Phrase(" "), false);
				header.setAlignment(Element.ALIGN_LEFT);
				header.setBorderWidthBottom(1);	
				header.setBorderWidthTop(0);
				header.setBorderColor(new Color(0x00, 0x90, 0x00));
				
				pdf.setMargins(pdf.leftMargin(), pdf.rightMargin(), pdf.topMargin() + 20, pdf.bottomMargin() + 10);				
				// Add title of report
				pdf.addTitle(DEFAULT_DOCUMENT_TITLE);
				pdf.addSubject(DEFAULT_DOCUMENT_TITLE);
				pdf.addKeywords("bp, pensionline, webstats, report");	
				
				LOG.info("Open the document");
				// add logo and banner image
				writer.setPageEvent(new WebStatPageEvents());				
				// open the document
				
				HeaderFooter footer = new HeaderFooter(
						new Phrase("Author : " + REPORT_AUTHOR + blankPaddingBuff.toString() 
										+ startDate + " to " + endDate, 
									new Font(Font.TIMES_ROMAN, 8, Font.NORMAL, new Color(0xa0, 0xa0, 0xa0))), 
						false);
				footer.setAlignment(Element.ALIGN_LEFT);
				footer.setBorderWidthBottom(0);
				footer.setBorderWidthTop(1);
				footer.setBorderColor(new Color(0x00, 0x90, 0x00));				
				
				pdf.open();
				
				// Creating cover sheet
				try
				{
					File checkFile = new File(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_banner.gif");
					if (checkFile.exists())
					{
						Image bpShippingBanner = Image.getInstance(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_banner.gif");
						bpShippingBanner.setAbsolutePosition(pdf.right() - 200, pdf.top() - 300);
						pdf.add(bpShippingBanner);	
						
						PdfContentByte cb = writer.getDirectContent();
			            cb.beginText();
			            
			            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			            cb.setFontAndSize(bf, 14);

			            // we show some text starting on some absolute position with a given alignment
			            cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, "website usage report", 
			            		pdf.right() - 200 + bpShippingBanner.getWidth(), 
			            		pdf.top() - 320, 0);
			            cb.setFontAndSize(bf, 10);
			            cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, startDate + " to " + endDate,
			            		pdf.right() - 200 + bpShippingBanner.getWidth(), 
			            		pdf.top() - 340, 0);
			            
			            // we tell the contentByte, we've finished drawing text
			            cb.endText();						
					}		
				}
				catch (Exception e)
				{
					LOG.error("Error in adding footer images: " + e);
		
				}
				
				pdf.setFooter(footer);
				pdf.setHeader(header);				
				pdf.newPage();
				
				Paragraph blankPara = new Paragraph(" ");				
				
				for (int i = 0; i < pdfReportContentDTOs.size(); i++)
				{
					PDFReportContentDTO pdfReportContentDTO = pdfReportContentDTOs.elementAt(i);
					
					String reportTitle = pdfReportContentDTO.getReportTitle(); 
					String reportComment = pdfReportContentDTO.getReportComment(); 
					ArrayList<PDFReportTableContentDTO> pdfTables = pdfReportContentDTO.getPdfTables();
													
					
					if (reportTitle != null)
					{
						Paragraph titlePara = new Paragraph(reportTitle, 
								new Font(Font.TIMES_ROMAN, 20, Font.BOLD, new Color(0x00, 0x90, 0x00)));
						titlePara.setAlignment(Element.ALIGN_LEFT);
						pdf.add(titlePara);
						pdf.add(blankPara);
					}
					
					if (reportComment != null)
					{
						Paragraph commentPara = new Paragraph(reportComment, 
								new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0x06, 0x06, 0x06)));
						commentPara.setAlignment(Element.ALIGN_LEFT);
						pdf.add(commentPara);
						pdf.add(blankPara);							
					}
					pdf.newPage();
					
					// add tables
					if (pdfTables != null)
					{
						for (int j = 0; j < pdfTables.size(); j++)
						{
							PDFReportTableContentDTO pdfTable = pdfTables.get(j);
							String tableTitle = pdfTable.getTableTitle();							
							PdfPTable pdfPTable = pdfTable.getTableContent();
							String chartPngFilename = pdfTable.getChartImageFilename();
							String tableNote = pdfTable.getTableNote();
							
							Paragraph tableTitlePara = new Paragraph(tableTitle, 
									new Font(Font.TIMES_ROMAN, 14, Font.BOLD, new Color(0x90, 0xC0, 0x00)));
							
							Paragraph tableNotePara = new Paragraph(tableNote, 
									new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(0x06, 0x06, 0x06)));
							tableNotePara.setAlignment(Element.ALIGN_LEFT);
							pdf.add(tableTitlePara);
							pdf.add(tableNotePara);
							pdf.add(blankPara);
							
							if (pdfPTable != null)
							{
								pdfPTable.setHorizontalAlignment(Element.ALIGN_LEFT);
								pdf.add(pdfPTable);
								pdf.add(blankPara);						
							}
							
							if (chartPngFilename != null)
							{
								Image png = Image.getInstance(chartPngFilename);
								if (png != null)
								{
//									png.setAlignment(Element.ALIGN_LEFT);						
//									png.scalePercent(60);
//									pdf.add(png);
//									pdf.add(blankPara);
									png.scalePercent(60);
									PdfPTable imgTable = WebStatPDFTableUtil.createWebStatChartTable(png);
									imgTable.setHorizontalAlignment(Element.ALIGN_LEFT);
									if (imgTable != null)
									{
										pdf.add(imgTable);
										pdf.add(blankPara);
									}
								}
							}
							
							if (j < pdfTables.size() - 1)
							{
								pdf.newPage();
							}
						}
					}					
					
					if (i < pdfReportContentDTOs.size() - 1)
					{
						pdf.newPage();
					}
				}	
				
				pdf.close();
				result = pdfByteArrayOutputStream.toByteArray();				
			}
			catch (Exception e)
			{
				LOG.error("Error in creating PDF report document: " + e.toString());
			}			
		}

		return result;
	}	
}
