package com.bp.pensionline.reporting.util;

/*
 * $Id: EndPage.java 2752 2007-05-15 14:58:33Z blowagie $
 * $Name$
 *
 * This code is part of the 'iText Tutorial'.
 * You can find the complete tutorial at the following address:
 * http://itextdocs.lowagie.com/tutorial/
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * itext-questions@lists.sourceforge.net
 */

import java.io.File;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.reporting.handler.ManageReportHandler;
import com.lowagie.text.Document;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Demonstrates the use of WebStatPageEvents.
 */
public class WebStatPageEvents extends PdfPageEventHelper
{	
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/* (non-Javadoc)
	 * @see com.lowagie.text.pdf.PdfPageEventHelper#onStartPage(com.lowagie.text.pdf.PdfWriter, com.lowagie.text.Document)
	 */
	@Override
	public void onStartPage(PdfWriter writer, Document document)
	{
		try
		{
			LOG.info("Page number: " + document.getPageNumber());
			if (document.getPageNumber() > 1)
			{
				File checkFile = new File(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_logo.gif");
				if (checkFile.exists())
				{
					Image bpLogo = Image.getInstance(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_logo.gif");
					bpLogo.scalePercent(50);
					bpLogo.setAbsolutePosition(document.left(), document.top() + 10);
					document.add(bpLogo);
				}

				checkFile = new File(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_banner.gif");
				if (checkFile.exists())
				{
					Image bpShippingBanner = Image.getInstance(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/customer_banner.gif");
					bpShippingBanner.scalePercent(50);
					bpShippingBanner.setAbsolutePosition(document.right() - 90, document.top() + 10);
					document.add(bpShippingBanner);	
				}				
			}				
			// draw the horizontal line
//			PdfContentByte  cb = writer.getDirectContent();
//			if (cb != null)
//			{
//	            int w = (int)(document.right() - document.left());
//	            int h = 600;
//	            
//	            PdfTemplate pdfTemplate = cb.createTemplate(w, h);
//	            Graphics2D g2 = pdfTemplate.createGraphics(w, h);
//	            pdfTemplate.setWidth(w);
//	            pdfTemplate.setHeight(h);
//	            

//	            
//	            g2.setColor(new Color(0x00, 0x90, 0x00));
//	            g2.drawLine((int)x1, (int)y1, (int)x2, (int)y2);
//	            g2.dispose();
//	            cb.addTemplate(pdfTemplate, 50, 400);	            
//			}
			
//            DefaultFontMapper mapper = new DefaultFontMapper();
//            FontFactory.registerDirectories();
//            mapper.insertDirectory("DataDicConfigurationXML:\\windows\\fonts");
//            // we create a template and a Graphics2D object that corresponds with it
//            int w = (int)(document.right() - document.left());
//            int h = 20;
//            double x1 = document.left();
//            double y1 = document.top();            
//            PdfContentByte cb = writer.getDirectContent();
//            PdfTemplate tp = cb.createTemplate(w, h);
//            Graphics2D g2 = tp.createGraphics(w, h, mapper);
//            tp.setWidth(w);
//            tp.setHeight(h);
//            
//            g2.setColor(new Color(0x00, 0x90, 0x00));
//            g2.drawLine(0, 0, w, 0);
//
//            g2.dispose();
//            cb.addTemplate(tp, (float)x1, (float)y1);
			
			
		}
		catch (Exception e)
		{
			LOG.error("Error in adding header images: " + e);
			throw new ExceptionConverter(e);
		}
	}

	/**
	 * @see com.lowagie.text.pdf.PdfPageEventHelper#onEndPage(com.lowagie.text.pdf.PdfWriter,
	 *      com.lowagie.text.Document)
	 */
	public void onEndPage(PdfWriter writer, Document document)
	{
		if (document.getPageNumber() > 1)
		{
			try
			{
				File checkFile = new File(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/cmg_logo.gif");
				if (checkFile.exists())
				{
					Image pngCmgLogo = Image.getInstance(ManageReportHandler.REPORT_BASE_FOLDER + "Commons/cmg_logo.gif");
					pngCmgLogo.scalePercent(60);
					
					pngCmgLogo.setAbsolutePosition(document.right() - pngCmgLogo.getWidth(), 
							document.bottom() - 20);
					document.add(pngCmgLogo);
				}			
			}
			catch (Exception e)
			{
				LOG.error("Error in adding footer images: " + e);
				throw new ExceptionConverter(e);
	
			}
		}
	}
	
}
