package com.bp.pensionline.reporting.producer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.reporting.chart.PLWebStatsChart;
import com.bp.pensionline.reporting.database.ReportResultSetBuilder;
import com.bp.pensionline.reporting.dto.PDFReportContentDTO;
import com.bp.pensionline.reporting.dto.PDFReportTableContentDTO;
import com.bp.pensionline.reporting.dto.ReportDTO;
import com.bp.pensionline.reporting.dto.ReportTableDTO;
import com.bp.pensionline.reporting.handler.ManageReportHandler;
import com.bp.pensionline.reporting.util.WebStatPDFExporter;
import com.bp.pensionline.reporting.util.WebStatPDFTableUtil;
import com.bp.pensionline.util.CheckConfigurationKey;
import com.lowagie.text.pdf.PdfPTable;

public class ReportProducer
{
	public static final String REPORT_DATE_FORMAT = "MMMMM dd, yyyy";
	
	public static final String QUERY_FAILURE_MESSAGE = "Query execution failed! Please check again!";
	public static final String CHART_RENDER_FAILURE_MESSAGE = "Cannot render chart for the data table!";
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	/**
	 * Build HTML output string from created report XML
	 * @param reportXmlFile
	 * @return
	 */
	public static String buildQueryHtmlOutput (String tableTitle, String tableQuery, String tableNote, String chartType)
	{				
		Date endDate = getDefaultEndDate();
		Date startDate = getDefaultStartDate();
		String startDateStr = "";
		String endDateStr = "";
		LOG.info("buildQueryHtmlOutput tableNote: " + tableNote);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(REPORT_DATE_FORMAT);
		
		if (startDateStr == null || startDateStr.trim().equals(""))
		{
			startDateStr = dateFormat.format(startDate);
		}
		
		if (endDateStr == null || endDateStr.trim().equals(""))
		{
			endDateStr = dateFormat.format(endDate);
		}		

		StringBuffer reportBuf = new StringBuffer();
		
		reportBuf.append("<div class='outer_report'>\n");
		
		// create table
		reportBuf.append("<div class=\"report_output_tables\">");								
		ReportResultSetBuilder reportResultSetBuilder = 
			new ReportResultSetBuilder (tableQuery, startDate, endDate);
		
		boolean queryResult = reportResultSetBuilder.execute();
		
		if (queryResult)
		{			
			Vector<String> colNames = reportResultSetBuilder.getColumnNames();
			Vector<Vector<String>> tableRows = reportResultSetBuilder.getReportRows();
			String chartImagePath = "";
			
			// create table title
			reportBuf.append("<h2>").append(tableTitle).append("</h2>\n");
			String[] tableNoteSplitteds = tableNote.split("\n");
			for (int i = 0; i < tableNoteSplitteds.length; i++)
			{
				String tableNoteSplitted = tableNoteSplitteds[i];
				if (tableNoteSplitted == null || tableNoteSplitted.trim().length() == 0)
				{
					tableNoteSplitted = "&nbsp;";
				}
				reportBuf.append("<p>").append(tableNoteSplitted).append("</p>\n");
			}
			
			reportBuf.append("<table class=\"report_output_table\"><tbody>");
			// Remove title of table, move it to <h2> tag
			//reportBuf.append("<tr><th colspan=\"" + colNames.size() + "\">").append(tableTitle).append("</th></tr>");
			// create table column names
			reportBuf.append("<tr>");
			for (int i = 0; i < colNames.size(); i++)
			{
				reportBuf.append("<th>").append(colNames.elementAt(i)).append("</th>");
			}
			reportBuf.append("</tr>");
			// create table data
			for (int i = 0; i < tableRows.size(); i++)
			{
				reportBuf.append("<tr>");
				Vector<String> tableRow = tableRows.elementAt(i);
				for (int j = 0; j < tableRow.size(); j++)
				{
					reportBuf.append("<td>").append(tableRow.elementAt(j)).append("</td>");
				}
				reportBuf.append("</tr>");
			}
			reportBuf.append("</tbody></table>");
			reportBuf.append("</div>");	
			reportBuf.append("<br/>");	
			
			// create chart image based on chart type
			if (chartType != null && 
					(chartType.equals(PLWebStatsChart.CHART_TYPE_BAR) || chartType.equals(PLWebStatsChart.CHART_TYPE_PIE)))
			{
				LOG.info("Create WebStat chart for table: " + chartType);
				// Only draw chart if the table has at least 2 properties (should always be 2)
				if (colNames.size() == 2)
				{				
					// create dataset for chart
					if (tableRows.size() > 0)
					{
						Vector<String> xValues = new Vector<String>();
						Vector<String> yValues = new Vector<String>();
						for (int j = 0; j < tableRows.size(); j++)
						{
							Vector<String> tableRow = tableRows.elementAt(j);
							if (tableRow.size() == 2)
							{
								xValues.add(tableRow.elementAt(0));
								yValues.add(tableRow.elementAt(1));
							}						
						}
														
						PLWebStatsChart webStatsChart = new PLWebStatsChart(tableTitle, chartType, xValues, yValues, 
								colNames.elementAt(0), colNames.elementAt(1));
						
						String tableTitle_ = tableTitle.replaceAll(" ", "_");
						long timestamp = System.currentTimeMillis()/1000;
						String pngFile = ManageReportHandler.REPORT_BASE_FOLDER + "PNG/" + tableTitle_ +
							"_" + timestamp + ".png";
						
						if (webStatsChart.createChartImage(pngFile))
						{
							chartImagePath = ManageReportHandler.REPORT_WEB_FOLDER + "PNG/" + tableTitle_ +
								"_" + timestamp + ".png";
							
							// render chart PNGs
							reportBuf.append("<div class=\"report_chart_holder\">");
							reportBuf.append("<img src='").append(chartImagePath).append("' alt='")
								.append(tableTitle).append("'/>");
							reportBuf.append("</div>");					
						}
					}
				}
				else
				{
					// render chart PNGs
					reportBuf.append("<div class=\"report_output_charts\">");
					reportBuf.append("<div id=\"chart_error\">");
					reportBuf.append(CHART_RENDER_FAILURE_MESSAGE);
					reportBuf.append("</div>");
					reportBuf.append("</div>");								
				}				
			}
		}
		else
		{
			reportBuf.append("<h2 class=\"report_query_error\">").append(QUERY_FAILURE_MESSAGE).append("</h2>");
			reportBuf.append("<p>Reason: " + reportResultSetBuilder.getException() + "</p>");
		}
		reportBuf.append("</div>");	
		reportBuf.append("</div>\n");
		
		// append footer with author and report date to HTML output
		reportBuf.append("<br />\n");
		reportBuf.append("<br />\n");
		reportBuf.append("<div class='footer_report'>\n");
		reportBuf.append("<div class='footer_report_author'>Author: " + CheckConfigurationKey.getStringValue("report.author"));
		reportBuf.append("</div>\n");
		String reportDateStr = startDateStr + " to " + endDateStr;
		reportBuf.append("<div class='footer_report_date'>").append(reportDateStr);
		reportBuf.append("</div>\n");
		reportBuf.append("<div class='footer_report_cmglogo'>");
		reportBuf.append("<a href='http://www.c-mg.net'>");
		reportBuf.append("<img src='/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/cmg_logo.gif' alt='DataDicConfigurationXML-mg logo' />");		
		reportBuf.append("</a>\n");		
		reportBuf.append("</div>\n");		
		
		return reportBuf.toString();
	}
	
	/**
	 * Build HTML output string from created report XML
	 * @param reportId
	 * @return
	 */
	public static String buildReportHtmlOutput (String reportId, ArrayList<String> selectedTables,
			String startDateStr, String endDateStr)
	{

		LOG.info("build reportXmlFile: " + reportId + " from " + startDateStr + " to " + endDateStr);
		//LOG.debug("build reportXmlFile: " + selectedTables.size());
		
		if (reportId != null && selectedTables != null && selectedTables.size() > 0)
		{
			ReportDTO reportMetaDataDTO = 
				new ReportDTO(ManageReportHandler.REPORT_BASE_FOLDER + "XML/" +  reportId + ".xml");
			
			if (reportMetaDataDTO.isReportExist())
			{
				Date startDate = getDefaultStartDate();
				Date endDate = getDefaultEndDate();
				
				SimpleDateFormat dateFormat = new SimpleDateFormat(REPORT_DATE_FORMAT);
				try
				{
					startDate = dateFormat.parse(startDateStr);					
					endDate = new Date(dateFormat.parse(endDateStr).getTime());		
				}
				catch (Exception e)
				{
					LOG.info("Date input is not valid, ignore date: " + startDateStr + " & " + endDateStr);
				}
				
				
				StringBuffer reportBuf = new StringBuffer();
				
				// create header
				reportBuf.append("\n<div class=\"report_output_header\">\n");
				reportBuf.append("<h1>").append(reportMetaDataDTO.getTitle()).append("</h1>\n");
				
				String[] tableCommentSplitteds = reportMetaDataDTO.getComment().split("\n");
				for (int i = 0; i < tableCommentSplitteds.length; i++)
				{
					String tableCommentSplitted = tableCommentSplitteds[i];
					if (tableCommentSplitted == null || tableCommentSplitted.trim().length() == 0)
					{
						tableCommentSplitted = "&nbsp;";
					}
					reportBuf.append("<p>").append(tableCommentSplitted).append("</p>\n");
				}
				
				reportBuf.append("</div>\n");
				
				//ArrayList<String> charts = new ArrayList<String>();
				
				// create table
				reportBuf.append("<div class=\"report_output_tables\">\n");				
				ArrayList tables = reportMetaDataDTO.getTables();
				for (int i = 0; i < tables.size(); i++)
				{
					// query data for report
					ReportTableDTO table = (ReportTableDTO)tables.get(i);
					if (table != null && selectedTables.contains(table.getTableId()))
					{						
						// get table title and table query
						String tableTitle = (table.getTableTitle() == null) ? "" : table.getTableTitle();
						String query = table.getTableQuery();
						String chartType = table.getChartType();
						String tableNote = (table.getTableNote() == null) ? "" : table.getTableNote();
						
						// build result set object based on query
						ReportResultSetBuilder reportResultSetBuilder = 
							new ReportResultSetBuilder (query, startDate, endDate);
						reportResultSetBuilder.execute();
						
						Vector<String> colNames = reportResultSetBuilder.getColumnNames();
						Vector<Vector<String>> tableRows = reportResultSetBuilder.getReportRows();						
						LOG.info("HUYYYYY: ");
						reportBuf.append("<h2>").append(tableTitle).append("</h2>\n");
						
						String[] tableNoteSplitteds = tableNote.split("\n");
						for (int j = 0; j < tableNoteSplitteds.length; j++)
						{
							String tableNoteSplitted = tableNoteSplitteds[j];
							if (tableNoteSplitted == null || tableNoteSplitted.trim().length() == 0)
							{
								tableNoteSplitted = "&nbsp;";
							}
							reportBuf.append("<p>").append(tableNoteSplitted).append("</p>\n");	
						}
						
						// create table title
						reportBuf.append("<table class=\"report_output_table\"><tbody>\n");
						// remove table title, move it to <h2> tag
						//reportBuf.append("<tr>\n<th colspan=\"" + colNames.size() + "\">").append(tableTitle).append("</th>\n</tr>\n");
						// create table column names
						reportBuf.append("<tr>\n");
						for (int j = 0; j < colNames.size(); j++)
						{
							reportBuf.append("<td class=\"report_column_name\" width=\"" +
									100/(colNames.size()) + "%\">").
									append(colNames.elementAt(j)).append("</td>\n");
						}
						reportBuf.append("</tr>\n");
						// create table data
						for (int j = 0; j < tableRows.size(); j++)
						{
							reportBuf.append("<tr>\n");
							Vector<String> tableRow = tableRows.elementAt(j);
							for (int k = 0; k < tableRow.size(); k++)
							{
								reportBuf.append("<td width=\"" +
										100/(colNames.size()) + "%\">")
										.append(tableRow.elementAt(k)).append("</td>\n");
							}
							reportBuf.append("</tr>\n");
						}
						reportBuf.append("</tbody></table>\n");
												
						// create chart image based on chart type
						if (chartType != null && 
								(chartType.equals(PLWebStatsChart.CHART_TYPE_BAR) || chartType.equals(PLWebStatsChart.CHART_TYPE_PIE)))
						{
							LOG.info("Create WebStat chart for table: " + chartType);
							// Only draw chart if the table has at least 2 properties (should always be 2)
							if (colNames.size() == 2)
							{				
								// create dataset for chart
								if (tableRows.size() > 0)
								{
									Vector<String> xValues = new Vector<String>();
									Vector<String> yValues = new Vector<String>();
									for (int j = 0; j < tableRows.size(); j++)
									{
										Vector<String> tableRow = tableRows.elementAt(j);
										if (tableRow.size() == 2)
										{
											xValues.add(tableRow.elementAt(0));
											yValues.add(tableRow.elementAt(1));
										}						
									}
																	
									PLWebStatsChart webStatsChart = new PLWebStatsChart(tableTitle, chartType, xValues, yValues, 
											colNames.elementAt(0), colNames.elementAt(1));
									
									String tableTitle_ = tableTitle.replaceAll(" ", "_");
									long timestamp = System.currentTimeMillis()/1000;
									String pngFile = ManageReportHandler.REPORT_BASE_FOLDER + "PNG/" + tableTitle_ +
										"_" + timestamp + ".png";
									
									if (webStatsChart.createChartImage(pngFile))
									{
										String pngWebPath = ManageReportHandler.REPORT_WEB_FOLDER + "PNG/" + tableTitle_ +
												"_" + timestamp + ".png";
										reportBuf.append("<div class=\"report_chart_holder\">\n");
										reportBuf.append("<img src='").append(pngWebPath).append("' alt='")
											.append(tableTitle).append("'/>\n");
										reportBuf.append("</div>\n");				
									}
								}
							}
						}
					}					
				}	
				
				// render chart PNGs
//				reportBuf.append("<div class=\"report_output_charts\">");
//				for (int i = 0; i < charts.size(); i++)
//				{
//					String tableTitle_ = charts.get(i);
//					String pngFile = ManageReportHandler.REPORT_WEB_FOLDER + "PNG/" + tableTitle_ +
//						"_" + startDateSecond + "_" + endDateSecond + ".png";
//					reportBuf.append("<div class=\"report_chart_holder\">");
//					reportBuf.append("<img src='").append(pngFile).append("' alt='")
//						.append(charts.get(i)).append("'/>");
//					reportBuf.append("</div>");
//				}
				
				reportBuf.append("</div>\n");					
				
				return reportBuf.toString();
			}
		}
		
		return null;
	}
	
	/**
	 * Build HTML output string from created report XML
	 * @param reportXmlFile
	 * @return
	 */
	public static String buildSelectedReportHtmlOutput (Vector<ReportDTO> selectedReports,
			String startDateStr, String endDateStr)
	{
		
		Date startDate = getDefaultStartDate();
		Date endDate = getDefaultEndDate();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(REPORT_DATE_FORMAT);
		
		if (startDateStr == null || startDateStr.trim().equals(""))
		{
			startDateStr = dateFormat.format(startDate);
		}
		
		if (endDateStr == null || endDateStr.trim().equals(""))
		{
			endDateStr = dateFormat.format(endDate);
		}
		
		StringBuffer allReportOutputHtmlBuf = new StringBuffer();
		allReportOutputHtmlBuf.append("<div class='outer_report'>\n");
				
		if (selectedReports != null)
		{
			for (int i = 0; i < selectedReports.size(); i++)
			{
				ReportDTO reportDTO = selectedReports.elementAt(i);
				
				String reportId = reportDTO.getReportId();
				
				ArrayList<String> selectedTables = new ArrayList<String>();
				// build selected tables array 
				for (int j = 0; j < reportDTO.getTables().size(); j++)
				{
					ReportTableDTO reportTableDTO = reportDTO.getTables().get(j);
					if (reportTableDTO != null )
					{
						selectedTables.add(reportTableDTO.getTableId());
					}					
				}

				String reportOutputHtml = buildReportHtmlOutput(reportId, selectedTables, startDateStr, endDateStr);
				if (reportOutputHtml != null)
				{
					allReportOutputHtmlBuf.append(reportOutputHtml);
				}				
			}
		}		
		
		allReportOutputHtmlBuf.append("</div>\n");
		
		// append footer with author and report date to HTML output
		allReportOutputHtmlBuf.append("<br />\n");
		allReportOutputHtmlBuf.append("<br />\n");
		allReportOutputHtmlBuf.append("<div class='footer_report'>\n");
		allReportOutputHtmlBuf.append("<div class='footer_report_author'>Author: " + CheckConfigurationKey.getStringValue("report.author"));
		allReportOutputHtmlBuf.append("</div>\n");
		String reportDateStr = startDateStr + " to " + endDateStr;
		allReportOutputHtmlBuf.append("<div class='footer_report_date'>").append(reportDateStr);
		allReportOutputHtmlBuf.append("</div>\n");
		allReportOutputHtmlBuf.append("<div class='footer_report_cmglogo'>");
		allReportOutputHtmlBuf.append("<a href='http://www.c-mg.net'>");
		allReportOutputHtmlBuf.append("<img src='/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/cmg_logo.gif' alt='DataDicConfigurationXML-mg logo' />");		
		allReportOutputHtmlBuf.append("</a>\n");		
		allReportOutputHtmlBuf.append("</div>\n");
		

		
		// do this for calling method to deal with null return
		if (allReportOutputHtmlBuf.length() > 0)
		{
			return allReportOutputHtmlBuf.toString();
		}
		
		return null;
	}	
	
	/**
	 * Build PDF bytes from created report XML
	 * @param reportXmlFile
	 * @return
	 */
//	public static byte[] buildReportPDFOutput (String reportXmlFile, String startDateStr, 
//			String endDateStr, String chartType)
//	{
//		byte[] result = null;
//		
//		LOG.info("reportXmlFile: " + reportXmlFile);
//		if (reportXmlFile != null)
//		{
//			Report reportMetaDataDTO = new Report(reportXmlFile);
//			if (reportMetaDataDTO.isReportExist())
//			{
//				long startDateSecond = getDefaultStartDate();
//				long endDateSecond = getDefaultEndDate();
//				
//				SimpleDateFormat dateFormat = new SimpleDateFormat(REPORT_DATE_FORMAT);
//				try
//				{
//					Date startDate = dateFormat.parse(startDateStr);
//					startDateSecond = startDate.getTime() / 1000;
//					
//					Date endDate = dateFormat.parse(endDateStr);
//					endDateSecond = (endDate.getTime() / 1000) + 3600;					
//				}
//				catch (Exception e)
//				{
//					LOG.info("Date input is not valid, ignore date: " + startDateStr + " & " + endDateStr);
//				}								
//				
//				// create table							
//				ArrayList tables = reportMetaDataDTO.getTables();
//				ArrayList<PDFReportTableContentDTO> pdfTables = new ArrayList<PDFReportTableContentDTO>();
//				ArrayList<String> charts = new ArrayList<String>();
//				
//				for (int i = 0; i < tables.size(); i++)
//				{
//					// query data for report
//					ReportSection table = (ReportSection)tables.get(i);
//					if (table != null)
//					{
//						String tableTitle = table.getTableTitle();
//						String query = table.getTableQuery();
//						String tableNote = table.getTableNote();
//						
//						ReportResultSetBuilder reportResultSetBuilder = 
//							new ReportResultSetBuilder (query, startDateSecond, endDateSecond);
//						reportResultSetBuilder.execute();
//						Vector<String> colNames = reportResultSetBuilder.getColumnNames();
//						Vector<Vector<String>> tableRows = reportResultSetBuilder.getReportRows();
//						
//						
//						LOG.info("Create WebStat chart: " + chartType);
//						String pngFile = null;
//						// create chart image based on chart type
//						if (chartType != null && 
//								(chartType.equals(PLWebStatsChart.CHART_TYPE_BAR) || chartType.equals(PLWebStatsChart.CHART_TYPE_PIE)))
//						{							
//							// Only draw chart if the table has at least 2 properties (should always be 2)
//							if (colNames.size() == 2)
//							{				
//								// create dataset for chart
//								if (tableRows.size() > 0)
//								{
//									Vector<String> xValues = new Vector<String>();
//									Vector<String> yValues = new Vector<String>();
//									for (int j = 0; j < tableRows.size(); j++)
//									{
//										Vector<String> tableRow = tableRows.elementAt(j);
//										if (tableRow.size() == 2)
//										{
//											xValues.add(tableRow.elementAt(0));
//											yValues.add(tableRow.elementAt(1));
//										}						
//									}
//																	
//									PLWebStatsChart webStatsChart = new PLWebStatsChart(tableTitle, chartType, xValues, yValues, 
//											colNames.elementAt(0), colNames.elementAt(1));
//									
//									String tableTitle_ = tableTitle.replaceAll(" ", "_");
//									pngFile = ManageReportHandler.REPORT_BASE_FOLDER + "PNG/" + tableTitle_ +
//										"_" + startDateSecond + "_" + endDateSecond + ".png";
//									
//									if (webStatsChart.createChartImage(pngFile))
//									{
//										charts.add(pngFile);								
//									}
//								}
//							}
//						}
//						
//						if (colNames != null && colNames.size() > 0)
//						{
//							// Create pdf tables array
//							PdfPTable pdfTable = WebStatPDFTableUtil.createWebStatDataTable(tableTitle, colNames, tableRows);
//							if (pdfTable != null)
//							{
//								pdfTables.add(new PDFReportTableContentDTO(pdfTable, pngFile, tableNote));
//							}							
//						}
//					}					
//				}
//				
//				LOG.info("Get content bytes of PDF");
//				// generate PDF content bytes
//				result = WebStatPDFExporter.generatePDFBytes(
//							new PDFReportContentDTO
//							(
//									reportMetaDataDTO.getTitle(),
//									reportMetaDataDTO.getComment(),
//									pdfTables
//							)
//						);
//			}
//		}
//		
//		return result;
//	}
	
	/**
	 * Build HTML output string from created report XML
	 * @param reportXmlFile
	 * @return
	 */
	public static byte[] buildSelectedReportPDFOutput (Vector<ReportDTO> selectedReports,
			String startDateStr, String endDateStr)
	{
		byte[] result = null;		
		if (selectedReports != null)
		{
			// Initialize a reportContents vector to hold the bytes
			Vector<PDFReportContentDTO> reportContents = new Vector<PDFReportContentDTO>();
			
			for (int i = 0; i < selectedReports.size(); i++)
			{
				ReportDTO reportDTO = selectedReports.elementAt(i);
				
				String reportId = reportDTO.getReportId();
				
				ArrayList<String> selectedTables = new ArrayList<String>();
				// build selected tables array 
				for (int j = 0; j < reportDTO.getTables().size(); j++)
				{
					ReportTableDTO reportTableDTO = reportDTO.getTables().get(j);
					if (reportTableDTO != null )
					{
						selectedTables.add(reportTableDTO.getTableId());
					}					
				}
				
				
				ReportDTO reportMetaDataDTO = new ReportDTO(ManageReportHandler.REPORT_BASE_FOLDER + "XML/" +  reportId + ".xml");
				
				if (reportMetaDataDTO.isReportExist())
				{
					Date startDate = getDefaultStartDate();
					Date endDate = getDefaultEndDate();
					
					SimpleDateFormat dateFormat = new SimpleDateFormat(REPORT_DATE_FORMAT);
					
					if (startDateStr == null || startDateStr.trim().equals(""))
					{
						startDateStr = dateFormat.format(startDate);
					}
					
					if (endDateStr == null || endDateStr.trim().equals(""))
					{
						endDateStr = dateFormat.format(endDate);
					}										
					
					try
					{
						startDate = dateFormat.parse(startDateStr);						
						endDate = new Date (dateFormat.parse(endDateStr).getTime() + 3600 * 24 * 1000);
					}
					catch (Exception e)
					{
						LOG.info("Date input is not valid, ignore date: " + startDateStr + " & " + endDateStr);
					}								
					
					// create table							
					ArrayList tables = reportMetaDataDTO.getTables();
					ArrayList<PDFReportTableContentDTO> pdfTables = new ArrayList<PDFReportTableContentDTO>();
					ArrayList<String> charts = new ArrayList<String>();
					
					for (int j = 0; j < tables.size(); j++)
					{
						// query data for report
						ReportTableDTO table = (ReportTableDTO)tables.get(j);
						if (table != null && selectedTables.contains(table.getTableId()))
						{
							// get table title and table query 
							String tableTitle = table.getTableTitle();
							String query = table.getTableQuery();
							String chartType = table.getChartType();
							String tableNote = table.getTableNote();
							
							// build result set object
							ReportResultSetBuilder reportResultSetBuilder = 
								new ReportResultSetBuilder (query, startDate, endDate);
							reportResultSetBuilder.execute();
							
							Vector<String> colNames = reportResultSetBuilder.getColumnNames();
							Vector<Vector<String>> tableRows = reportResultSetBuilder.getReportRows();
							
							
							LOG.info("Create WebStat chart: " + chartType);
							String pngFile = null;
							// create chart image based on chart type
							if (chartType != null && 
									(chartType.equals(PLWebStatsChart.CHART_TYPE_BAR) || chartType.equals(PLWebStatsChart.CHART_TYPE_PIE)))
							{							
								// Only draw chart if the table has at least 2 properties (should always be 2)
								if (colNames.size() == 2)
								{				
									// create dataset for chart
									if (tableRows.size() > 0)
									{
										Vector<String> xValues = new Vector<String>();
										Vector<String> yValues = new Vector<String>();
										for (int k = 0; k < tableRows.size(); k++)
										{
											Vector<String> tableRow = tableRows.elementAt(k);
											if (tableRow.size() == 2)
											{
												xValues.add(tableRow.elementAt(0));
												yValues.add(tableRow.elementAt(1));
											}						
										}
																		
										PLWebStatsChart webStatsChart = new PLWebStatsChart(tableTitle, chartType, xValues, yValues, 
												colNames.elementAt(0), colNames.elementAt(1));
										
										String tableTitle_ = tableTitle.replaceAll(" ", "_");
										long timestamp = System.currentTimeMillis() / 1000;
										pngFile = ManageReportHandler.REPORT_BASE_FOLDER + "PNG/" + tableTitle_ +
											"_" + timestamp + ".png";
										
										if (webStatsChart.createChartImage(pngFile))
										{
											charts.add(pngFile);								
										}
									}
								}
							}
							
							if (colNames != null && colNames.size() > 0)
							{
								// Create pdf tables array
								PdfPTable pdfTable = WebStatPDFTableUtil.createWebStatDataTable(tableTitle, colNames, tableRows);
								if (pdfTable != null)
								{
									pdfTables.add(new PDFReportTableContentDTO(tableTitle, pdfTable, pngFile, tableNote));
								}							
							}
						}					
					}
										
					// generate PDF content bytes
					reportContents.add(new PDFReportContentDTO (
										reportMetaDataDTO.getTitle(),
										reportMetaDataDTO.getComment(),
										pdfTables));
				}						
			}
			
			// build the result bytes
			LOG.info("Get content bytes of PDF");			
			result = WebStatPDFExporter.generateMultiPDFBytes(reportContents, startDateStr, endDateStr);
			
//			try
//			{
//				// Test
//				java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream("D:/webstat_report_local.pdf");
//				fileOutputStream.write(result);
//				fileOutputStream.flush();
//				fileOutputStream.close();				
//			}
//			catch (Exception e)
//			{
//				LOG.error("" + e);
//			}
			
		}			

		
		return result;
	}
	
	/**
	 * List all available reports XML files
	 * @return
	 */
	public static Vector<String> listAvailableReports()
	{
		Vector<String> reports = new Vector<String>();
		
		// list all xml files in REPORT_BASE_FOLDER
		try
		{
			String xmlFolder = ManageReportHandler.REPORT_BASE_FOLDER + "XML/";
			File reportFolder = new File(xmlFolder.substring(0, xmlFolder.length() - 1));
			
			if (reportFolder.isDirectory())
			{
				File[] reportFiles = reportFolder.listFiles();
				
				// sort the file name based on alphabet order
				TreeSet<String> filenameTree = new TreeSet<String>();
				
				for (int i = 0; i < reportFiles.length; i++)
				{
					File reportFile = reportFiles[i];
					if (reportFile != null && reportFile.isFile() && reportFile.getName().endsWith(".xml"))
					{
						filenameTree.add(reportFile.getName());
					}
					
				}
				
				Iterator<String> filenameTreeIter = filenameTree.iterator();
				while (filenameTreeIter.hasNext())
				{
					String fileName = filenameTreeIter.next();
					reports.add(xmlFolder + fileName);					
				}
			}
		} 
		catch (Exception e)
		{
			LOG.error("Error in reading XML report files: " + e);
		}
		
		return reports;
	}
	
	/**
	 * Get default start date which is start month of last 3 months from today in seconds.
	 * @return
	 */
	public static Date getDefaultStartDate ()
	{
		long currentMilis = System.currentTimeMillis();
		
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(currentMilis);
		
		// roll back 3 months before today
		cal.add(Calendar.MONTH, -3);
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	/**
	 * Get default end date which is start day of this month from today in seconds.
	 * @return
	 */	
	public static Date getDefaultEndDate ()
	{
		long currentMilis = System.currentTimeMillis();
		
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(currentMilis);
		
		// Get the first day of this month
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		
		return cal.getTime();
	}
}
