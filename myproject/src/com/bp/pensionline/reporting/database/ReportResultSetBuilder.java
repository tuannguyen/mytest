package com.bp.pensionline.reporting.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

public class ReportResultSetBuilder
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private String query;
	
	private Date startDate = new Date();
	
	private Date endDate = new Date();
	
	
	private String exception = null;
	
	// column names
	private Vector<String> columnNames = new Vector<String>();
	// data matrix
	private Vector<Vector<String>> reportRows = new Vector<Vector<String>>();
	
	public ReportResultSetBuilder(String userQuery, Date startDate, Date endDate)
	{
		// build up database query
		this.query = userQuery;
		this.startDate = startDate;
		this.endDate = endDate;		
	}
	
	/**
	 * Run query and update columnNames and ResultSet array
	 * @return
	 */
	public boolean execute()
	{
		// reset the exception message
		exception = null;
		
		if (query == null)
		{
			return false;
		}
		
		Connection con = null;
		DBConnector connector = DBConnector.getInstance();
		try
		{
			//int caseWorkNo = CaseWorkSQLHandler.getMaxCaseNo() + 1;			
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.WEBSTATS);
			String selectQuery = query; 
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			if (this.startDate != null)
			{
				String startDateStr = dateFormat.format(startDate);
				selectQuery = selectQuery.replaceAll(":START_DATE", "'" + startDateStr + "'");
			}
			
			if (this.endDate != null)
			{
				String endDateStr = dateFormat.format(endDate);
				selectQuery = selectQuery.replaceAll(":END_DATE", "'" + endDateStr + "'");
			}	
			
			LOG.info("Run report query: " + selectQuery);	
			
			PreparedStatement pstm = con.prepareStatement(selectQuery);						
			
			ResultSet rs = pstm.executeQuery();
			
			// get column names from ResutlSet MetaData
			ResultSetMetaData rsMD = rs.getMetaData();
			
			int numCols = rsMD.getColumnCount();
			//LOG.info("numCols = " + numCols);
			for (int i = 0; i < numCols; i++)
			{
				String colName = rsMD.getColumnName(i + 1);
				//LOG.info("Column at " + (i + 1) + " = " + colName);
				columnNames.add(colName);
			}
			
			// get report data
			while (rs.next())
			{
				Vector<String> reportRow = new Vector<String>();
				for (int i = 0; i < numCols; i++)
				{					
					String rowValue = rs.getString(i + 1);
					reportRow.add(rowValue);
				}
				
				reportRows.add(reportRow);
			}
			
			pstm.close();
			
			return true;
		}
		catch (SQLException sqle)
		{
			LOG.error("Error in execute report query: " + sqle.toString());
			exception = sqle.toString();
		}
		finally
		{
			if (con != null)
			{
				try
				{
					connector.close(con);
				}
				catch (Exception e)
				{
					LOG.error("Error in closing MySQL connection: " + e.toString());
				}
			}
		}
		
		return false;
	}

	/**
	 * @return the columnNames
	 */
	public Vector<String> getColumnNames()
	{
		return columnNames;
	}

	/**
	 * @return the reportRows
	 */
	public Vector<Vector<String>> getReportRows()
	{
		return reportRows;
	}

	/**
	 * @return the exception
	 */
	public String getException()
	{
		return exception;
	}
}
