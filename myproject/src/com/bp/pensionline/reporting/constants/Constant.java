/*
 * Constant.java
 *
 * Created on March 15, 2007, 2:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.bp.pensionline.reporting.constants;

/**
 *
 * @author mall
 */
public class Constant {
    public static final String PARAM = "xml";
    public static final String CONTENT_TYPE = "text/xml";
    
    /** Creates a new instance of Constant */
    public static final String AJAX_WEBSTAT_REPORT_REQUESTED 	= "AjaxReportParameterRequest";
    public static final String AJAX_WEBSTAT_REPORT_RESPONSE 	= "AjaxReportParameterResponse";
    
    public static final String AJAX_WEBSTAT_REPORT_ACTION 		= "rp_action";
    public static final String AJAX_WEBSTAT_REPORT_ID 			= "rp_id";
    public static final String AJAX_WEBSTAT_REPORT_TITLE 		= "rp_title";
    public static final String AJAX_WEBSTAT_REPORT_COMMENT 		= "rp_comment";
    public static final String AJAX_WEBSTAT_REPORT_TABLES 		= "rp_tables";
    public static final String AJAX_WEBSTAT_REPORT_TABLE 		= "rp_table";    
    public static final String AJAX_WEBSTAT_REPORT_TABLE_TITLE 	= "rp_table_title";
    public static final String AJAX_WEBSTAT_REPORT_TABLE_QUERY 	= "rp_table_query";
    public static final String AJAX_WEBSTAT_REPORT_TABLE_CHARTTYPE 	= "rp_table_charttype";  
    public static final String AJAX_WEBSTAT_REPORT_TABLE_NOTE	= "rp_table_note";
    
    // Run report
    public static final String AJAX_WEBSTAT_REPORTS 				= "rp_reports";
    public static final String AJAX_WEBSTAT_REPORT 					= "rp_report";
    public static final String AJAX_WEBSTAT_REPORT_STARTDATE 		= "rp_startDate";
    public static final String AJAX_WEBSTAT_REPORT_ENDDATE 			= "rp_endDate";
    public static final String AJAX_WEBSTAT_REPORT_OUTPUTTYPE 		= "rp_outputType";  
    
    public static final String XML_WEBSTAT_REPORT 				= "WebStat_Report";
    public static final String XML_WEBSTAT_REPORT_TITLE 		= "Title";
    public static final String XML_WEBSTAT_REPORT_COMMENT 		= "Comment";
    public static final String XML_WEBSTAT_REPORT_TABLES 		= "Tables";
    public static final String XML_WEBSTAT_REPORT_TABLE 		= "Table";
    public static final String XML_WEBSTAT_REPORT_TABLE_ID 		= "Table_Id";
    public static final String XML_WEBSTAT_REPORT_TABLE_TITLE 	= "Table_Title";
    public static final String XML_WEBSTAT_REPORT_TABLE_QUERY 	= "Table_Query";
    public static final String XML_WEBSTAT_REPORT_TABLE_CHARTTYPE = "Table_ChartType";
    public static final String XML_WEBSTAT_REPORT_TABLE_NOTE = "Table_Note";
}
