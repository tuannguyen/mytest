package com.bp.pensionline.reporting.dto;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.reporting.constants.Constant;

public class ReportDTO implements Comparable<ReportDTO>
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private String reportId;
	private boolean reportExist = false;
	private String title;
	private String comment;
	private ArrayList<ReportTableDTO> tables = new ArrayList<ReportTableDTO>();

	
	public ReportDTO()
	{
		// use this constructor for run report
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(ReportDTO o)
	{
		if (o == null || o.getTitle() == null)
		{
			return 1;
		}
		
		int comparedResult = this.getTitle().toLowerCase().compareTo(o.getTitle().toLowerCase());
		
		//LOG.debug("Report compate: " + comparedResult);
		return comparedResult;
	}


	public ReportDTO(String reportIdXML)
	{
		if (reportIdXML == null)
		{
			return;
		}
		
		try
		{	
			File reportFile = new File(reportIdXML);
			String fileName = reportFile.getName();
			if (fileName.endsWith(".xml"))
			{
				reportId = fileName.substring(0, fileName.indexOf(".xml"));
			}
			
			FileInputStream xmlInput = new FileInputStream(reportFile);
			LOG.info("parseRequestXml xml: " + reportId);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;

			builder = factory.newDocumentBuilder();

			document = builder.parse(xmlInput);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();				
			int nlength = paramNodes.getLength();
			
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TITLE))
					{
						setTitle(paramNode.getTextContent());
					}				
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_COMMENT))
					{
						setComment(paramNode.getTextContent());					
					}					
				}
			}
			
			// build the tables element
			NodeList tableList = root.getElementsByTagName(Constant.XML_WEBSTAT_REPORT_TABLE);		
			
			for (int i = 0; i < tableList.getLength(); i++)
			{
				Node table = tableList.item(i);
				if (table != null)
				{
					NodeList tableChildren = table.getChildNodes();
					ReportTableDTO tableDTO = new ReportTableDTO();
					
					for (int j = 0; j < tableChildren.getLength(); j++)
					{
						Node tableChild = tableChildren.item(j);
						
						if (tableChild != null && tableChild.getNodeType() == Node.ELEMENT_NODE)
						{							
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_ID))
							{
								tableDTO.setTableId(tableChild.getTextContent());
							}							
							else if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_TITLE))
							{
								tableDTO.setTableTitle(tableChild.getTextContent());
							}
							else if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_QUERY))
							{
								tableDTO.setTableQuery(tableChild.getTextContent());
							}
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_CHARTTYPE))
							{
								tableDTO.setChartType(tableChild.getTextContent());
							}	
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_NOTE))
							{
								tableDTO.setTableNote(tableChild.getTextContent());
							}							
						}						
					}
					
					// add to tables array
					tables.add(tableDTO);	
					
					reportExist = true;
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while parsing report XML file: " + e);
		}
	}
	
	public ReportDTO(File xmlFile)
	{
		if (xmlFile == null)
		{
			return;
		}
		
		try
		{	
			String fileName = xmlFile.getName();
			if (fileName.endsWith(".xml"))
			{
				reportId = fileName.substring(0, fileName.indexOf(".xml"));
			}
			
			FileInputStream xmlInput = new FileInputStream(xmlFile);
			LOG.info("report Id: " + reportId);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;

			builder = factory.newDocumentBuilder();

			document = builder.parse(xmlInput);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();				
			int nlength = paramNodes.getLength();
			
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TITLE))
					{
						setTitle(paramNode.getTextContent());
					}				
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_COMMENT))
					{
						setComment(paramNode.getTextContent());					
					}					
				}
			}
			
			// build the tables element
			NodeList tableList = root.getElementsByTagName(Constant.XML_WEBSTAT_REPORT_TABLE);		
			
			for (int i = 0; i < tableList.getLength(); i++)
			{
				Node table = tableList.item(i);
				if (table != null)
				{
					NodeList tableChildren = table.getChildNodes();
					ReportTableDTO tableDTO = new ReportTableDTO();
					
					for (int j = 0; j < tableChildren.getLength(); j++)
					{
						Node tableChild = tableChildren.item(j);
						
						if (tableChild != null && tableChild.getNodeType() == Node.ELEMENT_NODE)
						{							
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_ID))
							{
								tableDTO.setTableId(tableChild.getTextContent());
							}							
							else if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_TITLE))
							{
								tableDTO.setTableTitle(tableChild.getTextContent());
							}
							else if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_QUERY))
							{
								tableDTO.setTableQuery(tableChild.getTextContent());
							}
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_CHARTTYPE))
							{
								tableDTO.setChartType(tableChild.getTextContent());
							}						
						}						
					}
					
					// add to tables array
					tables.add(tableDTO);	
					
					reportExist = true;
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while parsing report XML file: " + e);
		}
	}
	
	/**
	 * @return the comment
	 */
	public String getComment()
	{
		if (comment == null)
		{
			return "";
		}
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}
	/**
	 * @return the tables
	 */
	public ArrayList<ReportTableDTO> getTables()
	{
		return tables;
	}
	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	/**
	 * @return the reportExist
	 */
	public boolean isReportExist()
	{
		return reportExist;
	}
	/**
	 * @return the reportId
	 */
	public String getReportId()
	{
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(String reportId)
	{
		this.reportId = reportId;
	}

	/**
	 * @param tables the tables to set
	 */
	public void setTables(ArrayList<ReportTableDTO> tables)
	{
		this.tables = tables;
	}		
}
