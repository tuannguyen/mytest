package com.bp.pensionline.reporting.dto;

import com.lowagie.text.pdf.PdfPTable;

public class PDFReportTableContentDTO
{
	private String tableTitle = null;
	private PdfPTable tableContent = null;
	private String tableNote = null;
	private String chartImageFilename = null;
	
	public PDFReportTableContentDTO(String tableTitle, PdfPTable tableContent, String chartImageFilename, String tableNote)
	{
		this.tableTitle = (tableTitle == null) ? "" : tableTitle;
		this.tableContent = tableContent;
		this.tableNote = (tableNote == null) ? "" : tableNote;
		this.chartImageFilename = chartImageFilename;		
	}
		
	/**
	 * @return the tableTitle
	 */
	public String getTableTitle()
	{
		return tableTitle;
	}


	/**
	 * @param tableTitle the tableTitle to set
	 */
	public void setTableTitle(String tableTitle)
	{
		this.tableTitle = tableTitle;
	}


	/**
	 * @return the chartImageFilename
	 */
	public String getChartImageFilename()
	{
		return chartImageFilename;
	}
	/**
	 * @param chartImageFilename the chartImageFilename to set
	 */
	public void setChartImageFilename(String chartImageFilename)
	{
		this.chartImageFilename = chartImageFilename;
	}
	/**
	 * @return the tableContent
	 */
	public PdfPTable getTableContent()
	{
		return tableContent;
	}
	/**
	 * @param tableContent the tableContent to set
	 */
	public void setTableContent(PdfPTable tableContent)
	{
		this.tableContent = tableContent;
	}
	/**
	 * @return the tableNote
	 */
	public String getTableNote()
	{
		return tableNote;
	}
	/**
	 * @param tableNote the tableNote to set
	 */
	public void setTableNote(String tableNote)
	{
		this.tableNote = tableNote;
	}
	
	
}
