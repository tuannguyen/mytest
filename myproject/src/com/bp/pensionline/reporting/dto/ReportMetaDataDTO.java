package com.bp.pensionline.reporting.dto;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.reporting.constants.Constant;

public class ReportMetaDataDTO
{
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private String reportId;
	private boolean reportExist = false;
	private String title;
	private String comment;
	private ArrayList<String[]> tables = new ArrayList<String[]>();
	
	
	public ReportMetaDataDTO(String reportIdXML)
	{
		if (reportIdXML == null)
		{
			return;
		}
		
		try
		{	
			File reportFile = new File(reportIdXML);
			String fileName = reportFile.getName();
			
			if (fileName.endsWith(".xml"))
			{
				reportId = fileName.substring(0, fileName.indexOf(".xml"));
			}
			
			FileInputStream xmlInput = new FileInputStream(reportFile);
			//LOG.info("parseRequestXml xml: " + xml);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			Document document = null;

			builder = factory.newDocumentBuilder();

			document = builder.parse(xmlInput);

			Element root = document.getDocumentElement();
			
			NodeList paramNodes = root.getChildNodes();				
			int nlength = paramNodes.getLength();
			
			for (int i = 0; i < nlength; i++)
			{
				Node paramNode = paramNodes.item(i);
				if (paramNode != null && paramNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TITLE))
					{
						setTitle(paramNode.getTextContent());
					}				
					if (paramNode.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_COMMENT))
					{
						setComment(paramNode.getTextContent());					
					}					
				}
			}
			
			// build the tables element
			NodeList tableList = root.getElementsByTagName(Constant.XML_WEBSTAT_REPORT_TABLE);		
			
			for (int i = 0; i < tableList.getLength(); i++)
			{
				Node table = tableList.item(i);
				if (table != null)
				{
					NodeList tableChildren = table.getChildNodes();
					String[] tableInfo = new String[3];
					for (int j = 0; j < tableChildren.getLength(); j++)
					{
						Node tableChild = tableChildren.item(j);
						
						if (tableChild != null && tableChild.getNodeType() == Node.ELEMENT_NODE)
						{
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_TITLE))
							{
								tableInfo[0] = tableChild.getTextContent();
							}
							else if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_QUERY))
							{
								tableInfo[1] = tableChild.getTextContent();
							}
							if (tableChild.getNodeName().equals(Constant.XML_WEBSTAT_REPORT_TABLE_CHARTTYPE))
							{
								tableInfo[2] = tableChild.getTextContent();
							}						
						}						
					}
					
					// add to tables array
					tables.add(tableInfo);	
					
					reportExist = true;
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error while parsing report XML file: " + e);
		}
	}
	/**
	 * @return the comment
	 */
	public String getComment()
	{
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}
	/**
	 * @return the tables
	 */
	public ArrayList getTables()
	{
		return tables;
	}
	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	/**
	 * @return the reportExist
	 */
	public boolean isReportExist()
	{
		return reportExist;
	}
	/**
	 * @return the reportId
	 */
	public String getReportId()
	{
		return reportId;
	}		
}
