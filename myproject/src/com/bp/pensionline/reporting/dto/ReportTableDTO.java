package com.bp.pensionline.reporting.dto;

public class ReportTableDTO
{
	private String tableId = null;
	private String tableTitle = null;
	private String tableQuery = null;
	private String chartType = "CHART_TYPE_NONE";
	private String tableNote = null;
	

	/**
	 * @return the tableNote
	 */
	public String getTableNote()
	{
		return tableNote;
	}
	/**
	 * @param tableNote the tableNote to set
	 */
	public void setTableNote(String tableNote)
	{
		this.tableNote = tableNote;
	}
	/**
	 * @return the tableId
	 */
	public String getTableId()
	{
		return tableId;
	}
	/**
	 * @param tableId the tableId to set
	 */
	public void setTableId(String tableId)
	{
		this.tableId = tableId;
	}
	/**
	 * @return the tableQuery
	 */
	public String getTableQuery()
	{
		return tableQuery;
	}
	/**
	 * @param tableQuery the tableQuery to set
	 */
	public void setTableQuery(String tableQuery)
	{
		this.tableQuery = tableQuery;
	}
	/**
	 * @return the tableTitle
	 */
	public String getTableTitle()
	{
		return tableTitle;
	}
	/**
	 * @param tableTitle the tableTitle to set
	 */
	public void setTableTitle(String tableTitle)
	{
		this.tableTitle = tableTitle;
	}
	/**
	 * @return the chartType
	 */
	public String getChartType()
	{
		return chartType;
	}
	/**
	 * @param chartType the chartType to set
	 */
	public void setChartType(String chartType)
	{
		this.chartType = chartType;
	}
}
