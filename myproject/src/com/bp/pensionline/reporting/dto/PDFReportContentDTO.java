package com.bp.pensionline.reporting.dto;

import java.util.ArrayList;


public class PDFReportContentDTO
{
	private String reportTitle = null; 
	private String reportComment = null; 
	private String startDate = null;
	private String endDate = null;
	private ArrayList<PDFReportTableContentDTO> pdfTables = new ArrayList<PDFReportTableContentDTO>(); 
	
	
	public PDFReportContentDTO(String reportTitle, String reportComment, 
			ArrayList<PDFReportTableContentDTO> pdfTables)
	{
		this.reportTitle = reportTitle;
		this.reportComment = reportComment;
		this.pdfTables = pdfTables;
	}
	
	/**
	 * @return the pdfTables
	 */
	public ArrayList<PDFReportTableContentDTO> getPdfTables()
	{
		return pdfTables;
	}
	/**
	 * @param pdfTables the pdfTables to set
	 */
	public void setPdfTables(ArrayList<PDFReportTableContentDTO> pdfTables)
	{
		this.pdfTables = pdfTables;
	}
	/**
	 * @return the reportComment
	 */
	public String getReportComment()
	{
		return reportComment;
	}
	/**
	 * @param reportComment the reportComment to set
	 */
	public void setReportComment(String reportComment)
	{
		this.reportComment = reportComment;
	}
	/**
	 * @return the reportTitle
	 */
	public String getReportTitle()
	{
		return reportTitle;
	}
	/**
	 * @param reportTitle the reportTitle to set
	 */
	public void setReportTitle(String reportTitle)
	{
		this.reportTitle = reportTitle;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	
	
}
