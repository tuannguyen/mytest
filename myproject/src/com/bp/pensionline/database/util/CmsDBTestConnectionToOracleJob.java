package com.bp.pensionline.database.util;


import org.opencms.file.CmsObject; 
import org.opencms.main.CmsLog; 
import org.opencms.scheduler.I_CmsScheduledJob; 

import java.util.Hashtable;
import java.util.Map; 
import org.apache.commons.logging.Log; 
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

 
public class CmsDBTestConnectionToOracleJob implements I_CmsScheduledJob { 
     
    private static final Log LOG = CmsLog.getLog(CmsDBTestConnectionToOracleJob.class); 
     
      
    public String launch(CmsObject cms, Map parameters)  { 
    	
    	LOG.info("CmsDBTestConnectionToOracleJob.launch");
    	
    	String sqlQuerySelect  = "SELECT SYSDATE FROM DUAL";
 
    	Connection con = null;
    	String oracleDate = "0000-00-00 00:00:00.0";	
    	Statement stm = null;
    	ResultSet rs = null; 	

    	try {
			Hashtable<String, String> env = new Hashtable<String, String>();

			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"org.jnp.interfaces.NamingContextFactory");
			env.put(Context.PROVIDER_URL, "jnp://127.0.0.1:1099");
			env.put(Context.URL_PKG_PREFIXES,
					"org.jboss.naming:org.jnp.interfaces");
			Context initContext = new InitialContext(env);
			DataSource ds = (DataSource) initContext
					.lookup("PensionLine_XAOracleDS");
			con = ds.getConnection();
    		stm = con.createStatement();

    		rs = stm.executeQuery(sqlQuerySelect);
    		
    		if (rs.next()) {
    			oracleDate = rs.getString("SYSDATE");
    		}
    		LOG.info("Connection is successful" );
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		// Don't particularly care
    		LOG.error("CmsDBTestConnectionToOracleJob.launch: fail to connect", e);
    	} finally {
    		if (con != null) {
    			try {
    				con.close();
    			} catch (Exception ex) {
    				// Don't particularly care
    				LOG.error("CmsDBTestConnectionToOracleJob.launch: fail to close", ex);
    			}
    		}
    	}	
    	
        return oracleDate;
        
    } 

}
 
