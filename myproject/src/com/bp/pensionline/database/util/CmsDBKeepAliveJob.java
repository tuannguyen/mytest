/**
 * reference : http://www.langhua.cn/opencms6.2.1/core/org/opencms/scheduler/I_CmsScheduledJob.html
 * 
 * Launch is called at run time
 */
package com.bp.pensionline.database.util;
//package org.opencms.scheduler.jobs; 

import org.opencms.file.CmsObject; 
import org.opencms.main.CmsLog; 
import org.opencms.scheduler.I_CmsScheduledJob; 
import java.util.Map; 
import org.apache.commons.logging.Log; 
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

 
public class CmsDBKeepAliveJob implements I_CmsScheduledJob { 
     
    private static final Log LOG = CmsLog.getLog(CmsDBKeepAliveJob.class); 
     
      
    public String launch(CmsObject cms, Map parameters) throws Exception { 
    	
    	String sqlQuerySelect  = "SELECT SYSDATE FROM DUAL";
 
    	Connection con = null;
    	String oracleDate = "0000-00-00 00:00:00.0";	
    	Statement stm = null;
    	ResultSet rs = null; 	

    	try {
    		DBConnector connector = DBConnector.getInstance();
    	
    		con = connector.getDBConnFactory(Environment.AQUILA);

    		stm = con.createStatement();

    		rs = stm.executeQuery(sqlQuerySelect);
    		
    		if (rs.next()) {
    			oracleDate = rs.getString("SYSDATE");
    		}
            connector.close(con);

    	} catch (Exception e) {
    		// Don't particularly care
    		LOG.error("CmsDBKeepAliveJob.launch: fail to connect", e);
    	} finally {
    		if (con != null) {
    			try {
    				DBConnector connector = DBConnector.getInstance();
    				connector.close(con);

    			} catch (Exception ex) {
    				// Don't particularly care
    				LOG.error("CmsDBKeepAliveJob.launch: fail to close", ex);
    			}
    		}
    	}	
    	
        return oracleDate;
        
    } 

}
 
