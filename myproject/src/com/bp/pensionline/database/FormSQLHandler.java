package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import com.bp.pensionline.constants.Environment;

/**
 * @author SonNT
 * @version 1.0
 * @since 2007/05/16
 * This class is used to update BP_FORMS table in mySQL DB 
 *
 */
public class FormSQLHandler{
	
	/*
	 * FormSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Connection conn ;	
	
	public FormSQLHandler(){
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			DBConnector connector = DBConnector.getInstance();
			//conn  = connector.getDBConnFactory(Environment.SQL);	
			conn  = connector.getDBConnFactory(Environment.PENSIONLINE);
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}		
	}
	
	public void closeConnection(){
		try{
			conn.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
		}
	}
		
	/**
	 * Implement select method for Sql DB  
	 * @param bgroup
	 * @param refno
	 * @param type
	 * @return
	 */
	public HashMap selectSqlDb(String casework, String bGroup){		
		
		HashMap resultMap = new HashMap();
		if(conn != null){		
			
			try {
				PreparedStatement stmt = conn.prepareStatement("SELECT * FROM BP_FORMS " +
						"WHERE CaseWorkID = ? and Bgroup = ?");
				stmt.setString(1, casework);
				stmt.setString(2, bGroup);
				ResultSet rs = null;
				
				/*
				 * Excute query
				 */
				rs = stmt.executeQuery();
				while(rs.next()){
					/*
					 * Get and decode parameters
					 */
					
					String member = rs.getString(2) + "-" + rs.getString(3);
					String submitted = rs.getString(4);					
					String subject = rs.getString(5);
					String formData = com.bp.pensionline.util.Base64Decoder.decode(rs.getString(6));
					
					
					/*
					 * Put values into map
					 */				
					resultMap.put("member", member);
					resultMap.put("subject", subject);
					resultMap.put("submitted", submitted);
					resultMap.put("formData", formData);
					
				}
				stmt.close();				

			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
		}		
		return resultMap;
	}
	
	
	
	/**
	 * Implement insert method for Sql DB
	 * @param bgroup
	 * @param refno
	 * @param casework
	 * @param type
	 * @param data
	 * @return
	 */
	public boolean insertSqlDb(String userName, String bgroup, String refno, String casework, String type, String data){		
		
		boolean isInserted = false;
		if(conn != null){			
			int i = 0;
			try {
				
				/*
				 *Create EOW table if not exists 
				 */
				PreparedStatement stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `BP_FORMS` " +
						"(WebuserID VARCHAR(30), BGroup VARCHAR(15), Refno VARCHAR(15), " +
						"CreationDate DATETIME, FormType ENUM('ChangeOfEmail','ChangeOfPhone','ChangeOfAddress','ChangeOfBank'), " +
						"FormData TEXT, Archived DATETIME, CaseworkID VARCHAR(15))");
				stmt.execute();
				
				/*
				 * Update old row
				 */
				stmt = conn.prepareStatement("UPDATE BP_FORMS SET Archived = getDate() " +
						"WHERE Archived is Null AND BGROUP = ? AND REFNO = ? AND FormType = ?");
				stmt.setString(1, bgroup);
				stmt.setString(2, refno);
				stmt.setString(3, type);
				stmt.executeUpdate();
				
								
				/*
				 * Encode parameters
				 */
				String formContent = com.bp.pensionline.util.Base64Encoder.encoder(data.getBytes());
				
				/*
				 * Insert new row
				 */
				stmt = conn.prepareStatement("INSERT INTO BP_FORMS " +
						"(WebuserID, BGroup, Refno, CreationDate, FormType, FormData, Archived, CaseworkID)" +
						" VALUES (?, ?, ?, NOW(), ?, ?, null, ?)");
				stmt.setString(1, userName);
				stmt.setString(2, bgroup);
				stmt.setString(3, refno);
				stmt.setString(4, type);
				stmt.setString(5, formContent);
				stmt.setString(6, casework);
				i = stmt.executeUpdate();
				
				stmt.close();

			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				
				isInserted = true;
				

			}
		}
		
		return isInserted;
	}
	
}