package com.bp.pensionline.database;

/**
 * @author SonNT
 * @version 1.0
 * @since 2007/05/24
 */
public class FuturePay{
	/*
	 * Class describes payment details of one month
	 */
	String Payment;
	double Gross = 0;
	double Adjust = 0;
	double Tax = 0;
	double Nett = 0;
	int Year;	
	int Month;
	
	public FuturePay(String paymentDate, double grossPension, double adjustments, double incomeTax, double netPension,
			int year, int month){
		Payment = paymentDate;
		Gross = grossPension;
		Adjust = adjustments;
		Tax -= incomeTax;
		Nett = netPension;
		Year = year;
		Month = month;
	}
	
}