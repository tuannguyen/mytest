package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
//import java.util.StringTokenizer;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.member.SQLConstraint;
import com.bp.pensionline.util.Variants;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.test.*;

import java.io.FileNotFoundException;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.main.CmsLog;


/**
 * This is to hook to the database, get data for all field given by fieldMap
 * from MemberMapper It updates valueMap after getting actual data from database
 * for a given pair of refno and bgroup
 * 
 * 
 * getConnection() queryBuilder(String tableName) getValue() getMapIndicator()
 * getAllTable()
 * 
 */

public class DBMemberConnector {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private Map<String, String> tableMap = new HashMap<String, String>();

	private Map<String, String> fieldMap = new HashMap<String, String>();	

	private Map<String, String> valueMap = new HashMap<String, String>();

	private MemberMapper mMapper = new MemberMapper();
	
	private int NUMBER_OF_TABLE = 30;

//	private int STRING_LENGTH = 30;

	/**
	 * 
	 * 
	 * 
	 * @param refno
	 * @param bgroup
	 */

	public DBMemberConnector(String refno, String bgroup) 
	{
		tableMap = mMapper.getAttributeTable();
		fieldMap = mMapper.getMapField();
		
		valueMap = mMapper.getMapValue();
		
		valueMap.put(Environment.MEMBER_BGROUP, bgroup);

		valueMap.put(Environment.MEMBER_REFNO, refno);
		
				
	};




	public Map<String, String> getValueMap()
	{
		return valueMap;
	}



	public MemberMapper getmMapper()
	{
		return mMapper;
	}



	/**
	 * get Connection
	 * 
	 * @return conn
	 * 
	 */
	private Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {

			DBConnector dbConn = DBConnector.getInstance();//new DBConnector();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);

		} catch (Exception ex) {
			LOG.error("DBMemberConnector", ex);
		}
		return conn;
	}



	/**
	 * This method is doing the following - getConnection - Hook up to database
	 * and take all data that need, and - put data in the valueMap
	 * 
	 * @return valueMap
	 */

	public Map<String, String> getValue() 
	{		
		String[] allTable;
		String sqlQuery = "";
		Connection conn = getConnection();
		LOG.info("DBMemberConnector: AQUILA connection got: " + conn);
		try
		{
			if (conn != null)
			{

				Statement stmt = conn.createStatement();

				// obj to store return value
				ResultSet rs = null;

				ResultSetMetaData rsmd = null;

				allTable = getAllTable();
				int loop = 0;

				while (loop < allTable.length)
				{

					try
					{

						sqlQuery = queryBuilder(allTable[loop]);
						
						LOG.info("Init member SQL: " + sqlQuery);

						rs = stmt.executeQuery(sqlQuery);

						rsmd = rs.getMetaData();
						while (rs.next())
						{
							for (int i = 1; i <= rsmd.getColumnCount(); i++)
							{
								mapGenerate(rs.getObject(i), rsmd.getColumnName(i));
							}
						}
					}
					catch (SQLException sqlEx)
					{
						LOG.error("DBMemberConnector", sqlEx);
					}
					loop++;

				}
				rs.close();
				//conn.close();
			}
		}
		catch (Exception ex)
		{
			LOG.error("DBMemberConnectorThread failed while running queries from table: ", ex);
		}
		finally
		{
			try
			{
				if (conn != null)
				{
					conn.close();
				}
			}
			catch (SQLException sqle){}
		}
		
		// Run at the top folder
		assignValueMap(Environment.MEMBERMAPPERFILE_DIR);				

		
		LOG.info("DBMemberConnector: getValue Done: " + valueMap.size());
		return valueMap;

	}

	/**
	 * this method return mapIndicator, this is used when marshall
	 * DatabaseMemberDao object to XML file
	 * 
	 */

	public Map<String, String> getMapIndicator() {

		return mMapper.getMapIndicator();
	}
	
	
	/**
	 * @author VinhDT 15 Jun 07
	 * @param folderName	
	 *  
	 *  This method is to run SQLqueries in all files under a specified folderName
	 *  and assign the value that it get to valueMap
	 * */
	
	private void assignValueMap(String folderName){
		
		CmsObject cmsObj = SystemAccount.getAdminCmsObject();
		
		// Modified by Huy
		if (!cmsObj.existsResource(folderName))
		{
			return;
		}
			
		List resources = null;
		try {
			
			//get all resources in dictionary folder (includes files and folders)
			resources = cmsObj.getResourcesInFolder(folderName, CmsResourceFilter.ALL);
		}
		catch(Exception ex) {

			LOG.error("Error while getting resources in folder: " + folderName + ". " + ex.toString());
			
		}
		
		if (resources != null && resources.size() > 0)
		{
			//get iterator
			Iterator iter = resources.iterator();
			
			//loop through iterator
			for ( ; iter.hasNext(); ) {

				CmsResource resource = (CmsResource) iter.next();
				
				//if resource is a file then  add it's name to the m_cache
				if(resource.isFile())
				{
					//LOG.info("Assign value from query to member: " + folderName+resource.getName());
					Connection conn = getConnection();
					XmlReader xReader=new XmlReader();
												
					try {
						
						if (conn != null) 
						{

							ResultSet rs = null;
							ResultSetMetaData rsmd = null;
							
							try 
							{    			
						    	
								String SQLQuery=new String(xReader.readFile(folderName+resource.getName()));
								//PreparedStatement pstm = conn.prepareStatement(SQLQuery);	
						    	
						    	/**Get the array contain the location in which preparedStatement 
						    	 * must replace with the value
						    	 * if result[i]=bgroup, must set query at location i, value of Bgroup
						    	 * if result[i]=refno, must set query at location i, value of Refno
						    	 */								
								SQLQuery = SQLQuery.trim();
								// get the last char of the sql query and compare with ";"
								char lastChar = SQLQuery.charAt(SQLQuery.length()-1);
								if (lastChar == ';'){
									SQLQuery = SQLQuery.substring(0, SQLQuery.length()-1);
								}
								//SQLQuery.substring(0, endIndex)''
								
								SQLQuery = SQLQuery.replaceAll(":bgroup", "'"+valueMap.get(Environment.MEMBER_BGROUP)+"'");
								SQLQuery = SQLQuery.replaceAll(":refno", "'"+valueMap.get(Environment.MEMBER_REFNO)+"'");
								SQLQuery = SQLQuery.replaceAll(":BGROUP", "'"+valueMap.get(Environment.MEMBER_BGROUP)+"'");
								SQLQuery = SQLQuery.replaceAll(":REFNO", "'"+valueMap.get(Environment.MEMBER_REFNO)+"'");
								SQLQuery = SQLQuery.replaceAll(":Bgroup", "'"+valueMap.get(Environment.MEMBER_BGROUP)+"'");
								SQLQuery = SQLQuery.replaceAll(":Refno", "'"+valueMap.get(Environment.MEMBER_REFNO)+"'");
								SQLQuery = SQLQuery.replaceAll(":BGroup", "'"+valueMap.get(Environment.MEMBER_BGROUP)+"'");
								SQLQuery = SQLQuery.replaceAll(":RefNo", "'"+valueMap.get(Environment.MEMBER_REFNO)+"'");

								rs = conn.createStatement().executeQuery(SQLQuery);
								/*
								PreparedStatement pstm = conn.prepareStatement(SQLQuery);
						    	pstm.execute();				
						    	rs = pstm.executeQuery();
						    	*/
						    	rsmd=rs.getMetaData();																		
						    	//Get total number of column
						    	int colsCount=rsmd.getColumnCount();								
						    	
						    	//Assign the value to valueMap
						    	while(rs.next()){
						    		for(int j=1;j<=colsCount;j++)
						    			valueMap.put(rsmd.getColumnName(j), rs.getString(j));
						    	}
							} 							
							catch (FileNotFoundException e)
							{
								LOG.error("[1] DBMemberConnector, could not find standard file", e);
						    			
							}
								
							rs.close();
							//DBConnector.getInstance().close(conn);
													
						}
						
					}
					catch (Exception ex) {
						LOG.error("Error while executing query in the file: " + ex);
					}
					finally					
					{
						if (conn != null)
						{
							try{
								conn.close();
							}catch(SQLException sqle){}
						}
					}
					
				}					
			}
			
		}				
	}
	
	/**
	 * getAllTable that memberMapper read from XML File
	 * @return String[]
	 * 
	 */
	private String[] getAllTable() {
		String[] allTable = new String[NUMBER_OF_TABLE];
		Iterator<String> keys = tableMap.keySet().iterator();
		int i = 0;
	//	int k = 0;

		while (keys.hasNext()) {
			boolean check = false;
			Object tableNameKey = keys.next();
			String tableName = tableMap.get(tableNameKey).toString();
			for (int j = 0; j < i; j++) {
				if (tableName.equalsIgnoreCase(allTable[j])) {
					check = true;
					break;
				}

			}
			if (check == false) {
				allTable[i] = tableName;
				i++;
			}

		//	k++;
		}
		String[] newAllTable = new String[i];
		for (i = 0; i < newAllTable.length; i++) {
			newAllTable[i] = allTable[i];
			
		}
		
		return newAllTable;

	}	
	
	/**
	 * build sqlQuery for each table
	 * 
	 * @param tableName
	 * @return
	 * @update: May 7, 07 
	 * @author Vinh Dao
	 */

	private String queryBuilder(String tableName) {
		
		String refno = valueMap.get(Environment.MEMBER_REFNO).toString();
		String bgroup = valueMap.get(Environment.MEMBER_BGROUP).toString();
		String allFieldSelect = mMapper.allFieldNeeded(tableName);

		SQLConstraint sqlHelper = new SQLConstraint(refno, bgroup);
		String whereClause = sqlHelper.checkConstraint(tableName);
		
		/**If read table name ASD, return to it real name Add_static_data*/
		if (tableName.equalsIgnoreCase("ASD"))
			tableName="add_static_data asd";
		
		/**
		 * If read table name Augmentation_benefit_augmented
		 * OR table name is Augmentation_benefit_eastern
		 * Return to it real name Augmentation_benefit
		 * 
		 */

		if (tableName.equalsIgnoreCase("AAB"))
			tableName="augmentation_benefit";
			
		if (tableName.equalsIgnoreCase("HTI"))
			tableName="transfer_in";
		
		if (tableName.equalsIgnoreCase("miscellaneous_history_1"))
			tableName="miscellaneous_history";
			
		String sqlQuery = "SELECT " + allFieldSelect + " " + "FROM "
				+ tableName;

		/**If table is CATEGORY_DETAIL, need to join with BASIC table */
		if (tableName.equalsIgnoreCase("CATEGORY_DETAIL"))
			sqlQuery = sqlQuery+ ", BASIC";
		
		/**If table is AVC_history, need to join with mp_fund table */
		if (tableName.equalsIgnoreCase("AVC_history"))
			sqlQuery = sqlQuery+ ", mp_fund";
		
		/**If table is PS_ADDRESSDETAILS, need to join with PS_ADDRESS table */
		if (tableName.equalsIgnoreCase("PS_ADDRESSDETAILS"))
			sqlQuery = sqlQuery+ ", ps_address";
		
		if (tableName.equalsIgnoreCase("PS_PMDETAILS"))
			sqlQuery = sqlQuery+ ", PS_PM";
		
		sqlQuery=sqlQuery+ " " + whereClause;
		
		if (tableName.equalsIgnoreCase("transfer_in")){
			LOG.debug("\n\n $$$$$$$$$$$$ DBMemberConector "+sqlQuery +" $$$$$$$$$$$$$\n\n\n");
		}
	
		return sqlQuery;
	}	
	
	/**
	 * Assign valueMap for a given Key
	 * 
	 * @param rsObject
	 * @param rsColumn
	 */
	private void mapGenerate(Object rsObject, String rsColumn) {
		
		Variants converter = new Variants();
		
		Iterator<String> keys = fieldMap.keySet().iterator();

		while (keys.hasNext()) {
			
			Object fieldKey = keys.next();
			
			String output;

			String temp = fieldKey.toString() + "_";

			
			if (temp.equalsIgnoreCase(rsColumn)){
               
                String tmpString;
                if (rsObject!=null)
                    tmpString=rsObject.toString();                                
                else
                    tmpString="";
                             
                output=converter.convert(fieldKey.toString(),tmpString);
                //output=tmpString;
 
                valueMap.put(fieldKey.toString(),output.toString());
            } 
			
			
		}

	}// end of mapGenerate	
}