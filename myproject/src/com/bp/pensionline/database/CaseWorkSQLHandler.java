package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.NumberUtil;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 04/05/2007
 */

/**
 * @author SonNT
 * @version 2.0
 * @since 2007/5/17 
 */

/**
 * @author Dcarlyle
 * @version 2.0
 * @since 2007/6/19 
 */

public class CaseWorkSQLHandler {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	/**
	 * Series of functions to operate on casework which is specific to the 
	 * API implementation
	 */
	
	/**
	 * Declare sql query insert into CW_CASE_LIST table 
	 */
	private static String sqlQuery = "Insert into CW_CASE_LIST "
		+ "(BGROUP,REFNO,CASENO,CASETYPE,CASECODE,EVTYPE,"
		+ "CL01X,"
		+ "CL02D,CL03D,CL04D,CL05D,CL06X,CL07D,CL08D,"
		+ "CL09I,"
		+ "CL10X,CL11X,CL12X,CL13X,CL14X,CL15X,CL16X,CL17X,CL18X,CL19X,CL20X,CL21X,"
		+ "CL22D,"
		+ "CL23I,CL24I,CL25I,CL26X,CL27I,CL28X,CL29X,CL30I,CL31X,CL32X,CL33X) values( "
		+ "?,?,?,?,?,null,?,SYSDATE,SYSDATE,SYSDATE,null,null,null,null,?,null,null,null,null,null,?,?,?,?,?,?,?,null,null,null,null,null,null,null,null,null,null,null,null)";
//		+ "?,?,?,?,?,null,?,SYSDATE,SYSDATE,SYSDATE,null,null,null,null,?,?,?,?,null,null,?,?,?,?,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)";


	/**
	 * Depending on name the caseworks are split between the internal BP teams
	 * 
	 * NUBPTONE - A to M
	 * NUBPTTWO - N to Z
	 */
	public static String A2M = "NUBPTONE";

	/**
	 * Depending on name the caseworks are split between the internal BP teams
	 * 
	 * NUBPTONE - A to M
	 * NUBPTTWO - N to Z
	 */
	public static String N2Z = "NUBPTTWO";

	// default user info to assign casework to. EV10283
	public static String PL_DEPT = "PENS";

	public static String PL_TEAM = "BPPL";

	public static String PL_USER = "NUBPTPEN";

	//Team Identification 
	/**  Team one caseworkers*/
	public static String BP_TEAM_1 = "BPT1";

	/** Team two caseworkers */
	public static String BP_TEAM_2 = "BPT2";

	/** Team thrree caseworkers */
	public static String BP_TEAM_3 = "BPT3";

	/** Team ? caseworkers */
	public static String BPTZ = "BPTZ";

	/** subscheme also known as other schemes */
	public static String BP_SUB_SCHEME = "BPSUB";

	/** case work priority */
	public static String BP_PRIORITY = "1";

	//Case Types
	/** a new memeber case type, in process, will become a Member */
	public static String NEW_MEMBER_CASE_TYPE = "N";

	/** a member case type */
	public static String MEMBER_CASE_TYPE = "M";

	/** a bulk member who is part of a patch process */
	public static String BULK_MEMBER_CASE_TYPE = "B";

	//Casework codes
	/** calculate the amount on a final fund */
	public static String FINAL_FUND_VALUE_CW = "BPOQ";

	/** Change Bank */
	public static String BP_CHANGE_BANK_CW = "BPCB";
	
	/** Change Email */
	public static String BP_CHANGE_EMAIL_CW = "BPEM";
	
	/** Change Phone */
	public static String BP_CHANGE_PHONE_CW = "BPPN";
	
	/** Change Address */
	public static String BP_CHANGE_ADDRESS_CW = "BPAD";

	/** change contributory option casework code */
	public static String BP_CHANGE_CONTRIBUTORY_CW = "BPAC";

	/** submit an Imail */
	public static String BP_IMAIL_CW = "BPPL";
	
	/** Completion codes for casework */
	public static String BP_CW_COMPELETION_CODE_CP = "CP";
	

	//Department Codes
	/** Pensions department code */
	public static String PENS_DC = "PENS";

	/**
	 * Process which team a user falls into based on their name and other?
	 * 
	 * @param surname
	 * @return return the split on the names, either BPT1, BPT2, BPT3, BPTZ, BPSUB
	 */
	public static String teamIdentification(String surname) {

		String result = BP_TEAM_1;

		if (surname != null && surname != "") {
			String analyse = surname.trim().toUpperCase();

			if (analyse.charAt(0) > 'H') {
				result = BP_TEAM_2;
			} 
		} 
		return result;
	}

	/**
	 * Process which group a user falls into based on their name
	 * 
	 * @param surname
	 * @return return the split on the names, either NUBPTONE or NUBPTTWO
	 */
	public static String userIdentification(String surname) {

		String result = A2M;

		if (surname != null && surname != "") {
			String analyse = surname.trim().toUpperCase();

			if (analyse.charAt(0) > 'M') {
				result = N2Z;
			}
		} 
		return result;
	}
	
	public static void debugValues (HashMap values)
	{
		if (values != null)
		{			
			Set keys = values.keySet();
			Iterator keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = (String)keyIterator.next();
				Object value = values.get(key);

				System.out.println("DEBUG CASEWORK VALUE: " + key + " = " + value);
				
			}
		}
	}	

	/**
	 * @author ThaoND
	 * Insert data to Oracle Database 
	 * @param map
	 * @throws Exception
	 */
	
	public  void insertOracleImail(HashMap map) throws Exception {
		Connection con = null;

		//System.out.println("insertOracleImail Begin --->");
		//debugValues(map);
		try {
			String tmpQuery = String.valueOf(map.get(Environment.IMAIL_QUERY));
			String tmpQueryFirst = "";
			String tmpQuerySecond = "";
			
			String bGroup = (String) map.get(Environment.IMAIL_BGROUP);
			String refNo = (String) map.get(Environment.IMAIL_REFNO);
			
			int caseMaxNo=Integer.parseInt(String
					.valueOf(map.get(Environment.IMAIL_TEMP)));
			if (tmpQuery.length() > 30) {
				tmpQueryFirst = tmpQuery.substring(0, 30);
			} else {
				tmpQueryFirst = tmpQuery;
			}
			tmpQuerySecond = tmpQuery.length() > 60 ? tmpQuery
					.substring(30, 60) : "";
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);

			pstm.setString(1, (String) map.get(Environment.IMAIL_BGROUP));
			pstm.setString(2, (String) map.get(Environment.IMAIL_REFNO));
			pstm.setInt(3, caseMaxNo);
			pstm.setString(4, CaseWorkSQLHandler.MEMBER_CASE_TYPE);
			pstm.setString(5, CaseWorkSQLHandler.BP_IMAIL_CW);
			pstm.setString(6,(String)map.get(Environment.IMAIL_NAME));
			pstm.setInt(7, NumberUtil.getInt(CaseWorkSQLHandler.BP_PRIORITY));
			pstm.setString(8, String
					.valueOf(map.get(Environment.IMAIL_SUBJECT)));
			pstm.setString(9, String.valueOf(map
					.get(Environment.IMAIL_EMAILADRESS)));
			pstm.setString(10, tmpQueryFirst);
			pstm.setString(11, tmpQuerySecond);

			pstm.setString(12, PENS_DC);
			pstm.setString(13, teamIdentification(getSurname(bGroup, refNo) ));
			pstm.setString(14, userIdentification(getSurname(bGroup, refNo) ));				
			
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			//System.out.println("insertOracleImail END --->");
		} catch (Exception e) {
		    //System.out.println("Error in insert new Imail to Oracle DB: " + e.getMessage());
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception e) {
					throw e;
				}

			}

		}

	}
	
	/**
	 * @author SonNT
	 * insert bank details casework
	 * @param bGroup
	 * @param refNo
	 * @param name
	 * @param bank
	 * @param accNumber
	 * @param sortCode
	 * @param rollNumber
	 * @return
	 * @throws Exception
	 */
	public  int insertOracleBankDetails(String bGroup, String refNo, 
			String accName, String bankName, String accNumber, String sortCode, String rollNumber) throws Exception {
		
		Connection con = null;
		int caseworkID = 0;

		try {			
			
			/*
			 * BGROUP     - bgroup
				REFNO      - refno
				CASENO     - (Select max(CASENO ) from CW_CASE_LIST) + 1
				CASETYPE   - M this comes from CaseWorkSQLHandler.MEMBER_CASE_TYPE
				CASECODE   - this comes from CaseWorkSQLHandler.BP_CHANGE_BANK_CW 
				CL01X      - Surname and initial
				CL02D      - date added, e.g. 28-JUN-04,  
				CL09I      - this comes from CaseWorkSQLHandler.BP_PRIORITY
				CL15X      - <Bank> from form handler
				CL16X      - <AccountNumber> from form handler
				CL17X      - <SortCode> from form handler
				CL18X      - <RollNumber> from form handler
				CL19X      - department - PENS (PENS_DC)
				CL20X      - Casework team, BPSUBS, BPT1 or BPT2
				CL21X      - SUB TEAM 
			 */
			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			
			//int caseNo = getMaxCaseNo() + 1;
			
			PreparedStatement pstm = con.prepareStatement("insert into CW_CASE_LIST " +
					"(BGROUP, REFNO, CASENO, CASETYPE, CASECODE, CL01X, CL02D, CL09I, CL15X, CL16X, CL17X, CL18X, CL19X, CL20X, CL21X) " +
					"values (?, ?, (Select max(CASENO ) + 1 from CW_CASE_LIST) ,? ,? ,?, SYSDATE, ?, ? ,? ,? ,?,?,?,?)");


			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			//pstm.setInt(3, caseNo);			
			//pstm.setInt(3, "(Select max(CASENO ) from CW_CASE_LIST) + 1");  // we do it this way rather than a separate to prevent someone else also getting this ID...!
			pstm.setString(3, CaseWorkSQLHandler.MEMBER_CASE_TYPE);
			pstm.setString(4, CaseWorkSQLHandler.BP_CHANGE_BANK_CW);
			pstm.setString(5, accName);			
			pstm.setInt(6,  Integer.parseInt(CaseWorkSQLHandler.BP_PRIORITY));
			pstm.setString(7, bankName);
			pstm.setString(8, accNumber);
			pstm.setString(9, sortCode);
			pstm.setString(10, rollNumber);			

			pstm.setString(11, PENS_DC);
			pstm.setString(12, teamIdentification(getSurname(bGroup, refNo) ));
			pstm.setString(13, userIdentification(getSurname(bGroup, refNo) ));	
			
			pstm.execute();
			
			//caseworkID = getMaxCaseNo();
			
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
		    LOG.info(e.getMessage() +e.getCause());
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception e) {
					throw e;
				}

			}

		}
		caseworkID = getMaxCaseNo(bGroup, refNo);
		return caseworkID;
	}
	
	/**
	 * @author SonNT
	 * insert email address casework
	 * @param bGroup
	 * @param refNo
	 * @param emailaddress
	 * @return
	 * @throws Exception
	 */
	public  int insertOracleEmail(String bGroup, String refNo, 
			String emailaddress) throws Exception {
		
		Connection con = null;
		int caseworkID = 0;
		String email = emailaddress.substring(0,emailaddress.indexOf("@"));
		String address = emailaddress.substring(emailaddress.indexOf("@")+1);

		try {			
			
			/*
			 * BGROUP     - bgroup
				REFNO      - refno
				CASENO     - (Select max(CASENO ) from CW_CASE_LIST) + 1
				CASETYPE   - M this comes from CaseWorkSQLHandler.MEMBER_CASE_TYPE				
				CL02D      - date added, e.g. 28-JUN-04,  
				CL09I      - this comes from CaseWorkSQLHandler.BP_PRIORITY
				CL11X      - Close case work, for archive tracking only
				CL15X      - <Email part 1> from form handler
				CL16X      - <Email part 2> from form handler
				CL19X      - department - PENS (PENS_DC)
				CL20X      - Casework team, BPSUBS, BPT1 or BPT2
				CL21X      - SUB TEAM 
			 */
			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			
			//int caseNo = getMaxCaseNo() + 1;
			
			PreparedStatement pstm = con.prepareStatement("insert into CW_CASE_LIST " +
					"(BGROUP, REFNO, CASENO, CASETYPE, CASECODE, CL02D, CL09I, CL15X, CL16X, CL19X, CL20X, CL21X, CL11X) " +
					"values (?, ?, (Select max(CASENO ) + 1 from CW_CASE_LIST) ,? ,? ,SYSDATE, ?, ?, ?, ?, ?, ?, ?)");

			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			//pstm.setInt(3, caseNo);		
			//pstm.setInt(3, "(Select max(CASENO ) from CW_CASE_LIST) + 1");				
			pstm.setString(3, CaseWorkSQLHandler.MEMBER_CASE_TYPE);
			pstm.setString(4, CaseWorkSQLHandler.BP_CHANGE_EMAIL_CW);//casecode			
			pstm.setInt(5, Integer.parseInt(CaseWorkSQLHandler.BP_PRIORITY));
			pstm.setString(6, email);
			pstm.setString(7, address);
			
			pstm.setString(8, PENS_DC);
			pstm.setString(9, teamIdentification(getSurname(bGroup, refNo) ));
			pstm.setString(10, userIdentification(getSurname(bGroup, refNo) ));	
			pstm.setString(11, BP_CW_COMPELETION_CODE_CP);	
			

			pstm.execute();
			
			//caseworkID = getMaxCaseNo();
			
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
		    LOG.info(e.getMessage() +e.getCause());
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception e) {
					throw e;
				}

			}

		}
		caseworkID = getMaxCaseNo(bGroup, refNo);
		return caseworkID;
	}
	
	/**
	 * @author SonNT
	 * insert phone casework
	 * @param bGroup
	 * @param refNo
	 * @param phone
	 * @param fax
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	public  int insertOraclePhone(String bGroup, String refNo, 
			String phone, String fax, String mobile) throws Exception {
		
		Connection con = null;
		int caseworkID = 0;

		try {			
			
			/*
			 * BGROUP     - bgroup
				REFNO      - refno
				CASENO     - (Select max(CASENO ) from CW_CASE_LIST) + 1
				CASETYPE   - M this comes from CaseWorkSQLHandler.MEMBER_CASE_TYPE
				CL02D      - date added, e.g. 28-JUN-04,  
				CL09I      - this comes from CaseWorkSQLHandler.BP_PRIORITY
				CL11X  	   - completion code, we always set to CP (Closed) so administrators have it for archive purpose only
				CL15X      - <Phone> from form handler
				CL16X      - <Fax> from form handler
				CL17X      - <Mobile> from form handler
				CL19X      - department - PENS (PENS_DC)
				CL20X      - Casework team, BPSUBS, BPT1 or BPT2
				CL21X      - SUB TEAM 
			 */
			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			
			//int caseNo = getMaxCaseNo() + 1;
			
			PreparedStatement pstm = con.prepareStatement("insert into CW_CASE_LIST " +
					"(BGROUP, REFNO, CASENO, CASETYPE, CASECODE, CL02D, CL09I, CL15X, CL16X, CL17X, CL19X, CL20X, CL21X, CL11X ) " +
					"values (?, ?, (Select max(CASENO ) + 1 from CW_CASE_LIST) ,? ,? ,SYSDATE, ?, ?, ? ,?, ?, ?, ?, ?)");

			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
			//pstm.setInt(3, caseNo);		
			//pstm.setInt(3, "(Select max(CASENO ) from CW_CASE_LIST) + 1");	
			
			pstm.setString(3, CaseWorkSQLHandler.MEMBER_CASE_TYPE);
			pstm.setString(4, CaseWorkSQLHandler.BP_CHANGE_PHONE_CW);			
			pstm.setInt(5, Integer.parseInt(CaseWorkSQLHandler.BP_PRIORITY));
			pstm.setString(6, phone);
			pstm.setString(7, fax);
			pstm.setString(8, mobile);
			
			pstm.setString(9, PENS_DC);
			pstm.setString(10, teamIdentification(getSurname(bGroup, refNo) ));
			pstm.setString(11, userIdentification(getSurname(bGroup, refNo) ));	
			pstm.setString(12, BP_CW_COMPELETION_CODE_CP);	

			pstm.execute();
			
			//caseworkID = getMaxCaseNo();
			
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
		    LOG.info(e.getMessage() +e.getCause());
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception e) {
					throw e;
				}

			}

		}
		caseworkID = getMaxCaseNo(bGroup, refNo);
		return caseworkID;
	}
	
	/**
	 * @author SonNT
	 * insert address casework
	 * @param bGroup
	 * @param refNo
	 * @param house
	 * @param street
	 * @param district
	 * @param postaltown
	 * @param county
	 * @param postcode
	 * @param country
	 * @return
	 * @throws Exception
	 */
	public  int insertOracleAddress(String bGroup, String refNo, 
			String house, String street, String district, String postaltown,
			String county, String postcode, String country) throws Exception {
		
		Connection con = null;
		int caseworkID = 0;

		try {			
			
			/*
			 * BGROUP     - bgroup
				REFNO      - refno
				CASENO     - (Select max(CASENO ) from CW_CASE_LIST) + 1
				CASETYPE   - M this comes from CaseWorkSQLHandler.MEMBER_CASE_TYPE
				CL02D      - date added, e.g. 28-JUN-04,  
				CL09I      - this comes from CaseWorkSQLHandler.BP_PRIORITY
				CL15X      - <House> from form handler
				CL16X      - <Street> from form handler
				CL17X      - <District> from form handler
				CL18X      - <Postaltown> from form handler
				CL19X      - department - PENS (PENS_DC)
				CL20X      - Casework team, BPSUBS, BPT1 or BPT2
				CL21X      - SUB TEAM 
			 */
			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			
			//int caseNo = getMaxCaseNo() + 1;
			
			PreparedStatement pstm = con.prepareStatement("insert into CW_CASE_LIST " +
					"(BGROUP, REFNO, CASENO, CASETYPE, CASECODE, CL02D, CL09I, CL15X, CL16X, CL17X, CL18X, CL19X, CL20X, CL21X) " +
					"values (?, ?, (Select max(CASENO ) + 1 from CW_CASE_LIST) ,? ,? ,SYSDATE, ?, ?, ? ,? ,?, ?, ?, ?)");

			pstm.setString(1, bGroup);
			pstm.setString(2, refNo);
		
			//pstm.setInt(3, caseNo);			
			//pstm.setInt(3, "(Select max(CASENO ) from CW_CASE_LIST) + 1");  // we do it this way rather than a separate to prevent someone else also getting this ID...!
			
			pstm.setString(3, CaseWorkSQLHandler.MEMBER_CASE_TYPE);
			pstm.setString(4, CaseWorkSQLHandler.BP_CHANGE_ADDRESS_CW);			
			pstm.setInt(5, Integer.parseInt(CaseWorkSQLHandler.BP_PRIORITY));
			pstm.setString(6, house);
			pstm.setString(7, street);
			pstm.setString(8, district);
			pstm.setString(9, postaltown);
			pstm.setString(10, PENS_DC);
			pstm.setString(11, teamIdentification(getSurname(bGroup, refNo) ));
			pstm.setString(12, userIdentification(getSurname(bGroup, refNo) ));									
					
			pstm.execute();
			
			//caseworkID = getMaxCaseNo();
			
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
		    LOG.info(e.getMessage() +e.getCause() + bGroup + refNo);
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception e) {
					throw e;
				}

			}

		}
		caseworkID = getMaxCaseNo(bGroup, refNo);
		return caseworkID;
	}


	/** 
	 * Get the users surname, if none available return X
	 * @author DCarlyle
	 * @return
	 */
	public String getSurname(String bGroup, String refno) {
		Connection con = null;
		String surname = "unknown";
		try {
			
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm
					.executeQuery("select BD02A from basic where BGROUP='"+bGroup+"' AND REFNO='"+refno+"'");
			while (rs.next()) {
				surname = rs.getString("BD02A");
			}

			return surname;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
			return surname;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}

	
	/** 
	 * Returns the max inserted for that user
	 * @author ThaoND
	 * @return
	 *
	 */
	public int getMaxCaseNo(String bgroup, String refno){
			
		Connection con = null;
		try {
			int caseCode = 0;
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm
					.executeQuery("SELECT MAX(CASENO) AS CASENO FROM CW_CASE_LIST WHERE BGROUP='"+bgroup+"' AND REFNO='"+refno+"'");
			while (rs.next()) {
				caseCode = rs.getInt("CASENO");
			}

			return caseCode;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause()+" bgroupRefo="+bgroup+refno);
			return 0;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}
	
	
	/** 
	 * Not thread safe
	 * @author ThaoND
	 * @return
	 *
	 */
	public int getMaxCaseNo() {
		Connection con = null;
		try {
			int caseCode = 0;
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm
					.executeQuery("SELECT MAX(CASENO) AS CASENO FROM CW_CASE_LIST");
			while (rs.next()) {
				caseCode = rs.getInt("CASENO");
			}

			return caseCode;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
			return 0;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}
}
