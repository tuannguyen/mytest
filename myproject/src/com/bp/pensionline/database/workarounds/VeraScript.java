package com.bp.pensionline.database.workarounds;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
import com.bp.pensionline.util.DateUtil;
public class VeraScript {
	
	
	/**
	 * @param userid
	 * @param calctype
	 * @param bgroup
	 * Update calc_input table for Vera stuff
	 */
	public void updateVera(String userid, String calctype, String bgroup, int veraFlag, int veraYears) {
		String sqlUpdate = "Update calc_input set CINN71 = ?," +
								" CINN72 = ?," +
								" CINN73 = ?," +
								" CINN74 = ?," +
								" CINN75 = ?," +
								" CINN76 = ?," +
								" CINN77 = ? " +
								"where refno =? and  calctype = ? and  BGROUP = ? ";
		
		//default values for "vera years of service" check - either 0 or 1.
		int VE15 = 0;
		int VE16 = 0;
		int VE17 = 0;
		int VE18 = 0;
		int VE19 = 0;
		int VE20 = 0;
		
		if (veraYears==15){
			VE15 = 1;
		}
		else if(veraYears==16){
			VE16 = 1;
		}
		else if(veraYears==17){
			VE17 = 1;
		}
		else if(veraYears==18){
			VE18 = 1;
		}
		else if(veraYears==19){
			VE19 = 1;
		}
		else if(veraYears>=20){
			VE20 = 1;
		}
		
		Connection con = null;
		PreparedStatement pstm = null;
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm = con.prepareStatement(sqlUpdate);
			pstm.setInt(1, VE15);
			pstm.setInt(2, VE16);
			pstm.setInt(3, VE17);
			pstm.setInt(4, VE18);
			pstm.setInt(5, VE19);
			pstm.setInt(6, VE20);
			pstm.setInt(7, veraFlag);
						
			pstm.setString(8, userid);
			pstm.setString(9, calctype);
			pstm.setString(10, bgroup);
				
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			System.out.println("Update Vera has been done!!!");

			
		} catch (Exception e) {
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
			
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}

	}
	
	public Date getDateQualifiedService (String bgroup, String refno){
		String sqlUpdate = "Select BD22D from basic where bgroup = ? and refno = ? ";
		
		Connection con = null;
		PreparedStatement pstm = null;
		//default set to 01 DEC 2006
		Date dateQualifiedService = DateUtil.getDate("01 DEC 2006");
		try {

			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			
			pstm = con.prepareStatement(sqlUpdate);
			pstm.setString(1, bgroup);
			pstm.setString(2, refno);
		
			ResultSet rs = null;		
			rs = pstm.executeQuery();
			if (rs.next()) {
				dateQualifiedService = rs.getDate("BD22D");
			}
			
			pstm.close();
			rs.close();
			
			System.out.println("Get date qualified service has been done!!!" + dateQualifiedService);

			
		} catch (Exception e) {
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
			}
			
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return dateQualifiedService;
	}
	
	/**
	 * Return Vera Years.
	 */	
	public int checkVeraYear (Date dateQualifiedService){
		Date veraDefaultDate = DateUtil.getDate("01 DEC 2006");
		//get the difference from the 01-12-2006 and the date qualified service
		int yearOfService = DateUtil.getDateDiff(dateQualifiedService, veraDefaultDate);
	
		System.out.println("Years Of service"+yearOfService);
		return yearOfService;
		
	}
	
	/**
	 * Return VEEL value: either 0 or 1.
	 */
	public int checkVeraFlag (Date Doc, Date dateQualifiedService){
		int flag = 0;
		//get the difference from the Date of Calculation and the date qualified service
		int yearOfService = DateUtil.getDateDiff(dateQualifiedService, Doc);
		if (yearOfService >= 30){
			flag = 1;
		}
		return flag;
	}
		
}
