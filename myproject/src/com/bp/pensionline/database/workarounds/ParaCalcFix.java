package com.bp.pensionline.database.workarounds;

import com.bp.pensionline.util.CheckConfigurationKey;

public class ParaCalcFix {

	//ParaClacFix runSetup = new ParaClacFix
	public String getSQLString(String username, String password, 
			String bgroup, String refno, String calctype, String doc){
    

	StringBuffer unixArg = new StringBuffer();

	unixArg.append("/ADM-noncore/");
	
	// get the CalcServer from opencms.properties
	String calcServer = CheckConfigurationKey.getStringValue("calcServer");
	unixArg.append(calcServer);
	
	unixArg.append("/sql/run_bp_setcrinput.sh");
	unixArg.append(" ");
	unixArg.append(username);
	unixArg.append(" ");
	unixArg.append(password);
	unixArg.append(" ");
	unixArg.append(bgroup);
	unixArg.append(" ");
	unixArg.append(refno);
	unixArg.append(" ");
	unixArg.append(calctype);
	unixArg.append(" ");
	unixArg.append(doc);  //or CIND01 from calc_input

	String sql = "returnStatus := pms_zz_exec.run_unix_Command('"+unixArg.toString()+"', 1)"; 
	System.out.println(doc);
	return sql;
	
	}
}
