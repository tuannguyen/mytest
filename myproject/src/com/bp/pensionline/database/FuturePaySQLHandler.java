package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.Variants;


/**
 * @author SonNT
 * @version 1.0
 * @since 2007/05/24	
 *
 */
public class FuturePaySQLHandler{
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	public static int[] MONTHS_REMAINING_IN_TAX_YEAR = {3,2,1,12,11,10,9,8,7,6,5,4,3};
	
	private Connection conn;
	private ArrayList<FuturePay> futurePayment = new ArrayList<FuturePay>(); // HUY: Modify to use List rather than Hashmap
	private Hashtable<String, String> resultMap;
	private ResultSet rs;
	private PreparedStatement pstmt;
	//int size;
	
	public FuturePaySQLHandler(String bGroup, String refNo, Hashtable<String, String> map){
		
		//Get connection
		try{
			DBConnector connector = DBConnector.getInstance();
			conn  = connector.getDBConnFactory(Environment.AQUILA);			
		}
		catch (Exception e) {
			LOG.info(this.getClass().toString() + ".FuturePaySQLHandler: " + e.toString());			
		}
		
		resultMap = map;
		
		if(conn != null){
			try{
				java.util.Date today = new java.util.Date();
				Date firstPaid = new java.sql.Date(System.currentTimeMillis());
				
				//Select firtpaid
				pstmt = conn.prepareStatement("select firstpdd as firstpaid ,pamt as amount, ptreason as reason from ps_pdetails where psetno= (select psetno from ps_pcontrol where bgroup=? and refno=?) AND lastpdd IS NULL AND PTYPE = 'P' order by psseqno DESC");
				pstmt.setString(1, bGroup);
				pstmt.setString(2, refNo);
				rs = pstmt.executeQuery();
							

				String paymentDate = "";
				String amount = "0";
				String tax = "0";
				Variants var = new Variants();
				String dateStr = "1", monthStr = "", yearStr = "";	// default is the first day of month
				
				if(rs.next()){
					firstPaid = rs.getDate("firstpaid");
					paymentDate = var.convert("DateNumeric", firstPaid.toString());	
					LOG.info("paymentDate: " + paymentDate);
					dateStr = paymentDate.substring(0, paymentDate.indexOf("/"));
					amount = rs.getString(2);					
				}
				
				//Select firtpaid
				pstmt = conn.prepareStatement("select firstpdd as firstpaid ,pamt as amount, ptreason as reason from ps_pdetails where psetno= (select psetno from ps_pcontrol where bgroup=? and refno=?) AND lastpdd IS NULL AND PTYPE = 'T' order by psseqno DESC");
				pstmt.setString(1, bGroup);
				pstmt.setString(2, refNo);
				rs = pstmt.executeQuery();	
				if(rs.next()){
					tax = rs.getString(2);					
				}				
				
				
				//LOG.info(this.getClass().toString() + " paymentDate: " + paymentDate + " : " + firstPaid.getTime());
				int month, year;
				if (firstPaid.compareTo(today) >= 0){
					month = Integer.parseInt(paymentDate.substring(paymentDate.indexOf("/") + 1, paymentDate.lastIndexOf("/")));
					year = Integer.parseInt(paymentDate.substring(paymentDate.lastIndexOf("/") + 1));
				}
				else
				{
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(today);
					year = calendar.get(Calendar.YEAR);
					month = calendar.get(Calendar.MONTH) + 1; // include current month. Sometime it is PAID, sometimes it is not PAID
				}
				
				//Get the lastest month that has been paid to calculate the left months of the current tax year
//				pstmt = conn.prepareStatement("Select max(TAXPNTD) from PS_PPROCHIST where psetno = (select psetno from ps_pcontrol where bgroup=? and refno=?) and status = 'PAID'");
//				pstmt.setString(1, bGroup);
//				pstmt.setString(2, refNo);
//				
//				rs = pstmt.executeQuery();
//				
//				if(rs.next()){
//					paymentDate = var.convert("DateNumeric", rs.getString(1));	
//				}
				
				//get month and year for comparing. Modified by Huy
//				String month = new Integer(startMonth).toString();
//				
//				Calendar cal = new GregorianCalendar();
//				int currentYear = cal.get(Calendar.YEAR);
//				String year = String.valueOf(currentYear);
//				
//				if (paymentDate != null && paymentDate.length() > 0) {
//					month = paymentDate.substring(paymentDate.indexOf("/") + 1,paymentDate.lastIndexOf("/"));
//					year = paymentDate.substring(paymentDate.lastIndexOf("/") + 1);
//				}	
			
				LOG.info(this.getClass().toString() + ".FuturePay from: " + year + " - " + month);
				
				//get left months 
				int retiredMonth = (month > 1)? (month - 1) : 12;
				int monthRemainInTaxYear = MONTHS_REMAINING_IN_TAX_YEAR[retiredMonth - 1];
				
				LOG.info(this.getClass().toString() + ".monthRemainInTaxYear: " + year + " - " + month);
				for(int i = 0; i < monthRemainInTaxYear; i++){
					/*
					 * Create instances of FuturePay to put in map
					 */
									
					int newMonth;
					int newYear = year;
					
					if(month + i > 12){
						//if the month > 12, it's in the next year
						newMonth = month + i - 12;
						newYear = year + 1;
					}
					else{
						newMonth = month + i;
					}
					
					yearStr = "" + newYear;
					if(newMonth < 10){
						monthStr = "0" + newMonth;
					}
					else
					{
						monthStr = "" + newMonth;
					}
					
					//get new paymentDate for the month
					paymentDate = dateStr + "/" + monthStr + "/" + yearStr;
					LOG.info(this.getClass().toString() + ".Future Pay after: " + paymentDate + " has amount: " + amount);
					futurePayment.add(new FuturePay(paymentDate, Double.parseDouble(amount), 0, Double.parseDouble(tax), 0, newYear, newMonth));

				}
				
				updatePaymentDetails(bGroup, refNo);
				getPaymentMethod(bGroup, refNo);
				getTaxCode(bGroup, refNo);
				updateDeductDetails(bGroup, refNo);
				replaceDetails();
			}
			catch (Exception e) {
				LOG.error(this.getClass().toString() + " error: " + e.getMessage());
			}			
		}
	}
	
	/**
	 * Close the connection to db
	 */
	public void closeConnection(){
		try{
			pstmt.close();
			conn.close();
		}
		catch (Exception e) {
			LOG.info(this.getClass().toString() + ".closeConnection" + e.getMessage());
		}
	}
	
	/**
	 * Return the map to the consumer
	 * @return
	 */
	public Hashtable<String, String> getReturnMap(){
		
		return resultMap;
	}
	
	/**
	 * Update map base on ps_pdetailschng data
	 */
	private void updatePaymentDetails(String bGroup, String refNo){
		if(conn != null){
			try {
				
				//get change details in ps_pdetailschng table
				pstmt = conn.prepareStatement("select source, ps_pdetailschng.pamt, ps_pdetailschng.ptreason, ps_pdetailschng.permchng, ps_pdetailschng.chngd " +
						"from ps_pdetailschng " +
						"where psetno= ( select psetno from ps_pcontrol where bgroup=? and refno=? ) " +
						"and ps_pdetailschng.status = 'READY' " +
						"order by ps_pdetailschng.chngd ASC");
				pstmt.setString(1, bGroup);
				pstmt.setString(2, refNo);
				
				rs = pstmt.executeQuery();
				
				while(rs.next()){
					
					String source = rs.getString(1);
					double amount = Double.parseDouble(rs.getString(2));
					String reason = rs.getString(3);   
					String permChange = rs.getString(4);					
					Variants var = new Variants();
					String taxDate = var.convert("DateNumeric", rs.getString(5));
					
					int taxMonth = Integer.parseInt(taxDate.substring(taxDate.indexOf("/") + 1, taxDate.lastIndexOf("/")));
					int taxYear = Integer.parseInt(taxDate.substring(taxDate.lastIndexOf("/") + 1));			
					//LOG.info(this.getClass().toString() + ": taxYear: " + taxYear + " - taxMonth" + taxMonth);
					
					// Huy: DEDUCT is got from ps_ded table
					if(permChange.compareTo("P") == 0){
						//if permChange is "P", apply change to all months in map which is after taxDate
						for(int i = 0; i < futurePayment.size(); i++){
							FuturePay monthDetails = (FuturePay)futurePayment.get(i);							
							// Huy
							if((monthDetails.Year == taxYear && monthDetails.Month >= taxMonth) || monthDetails.Year > taxYear){	
								LOG.info(taxYear + "/" + taxMonth +  ": update " + permChange + " amount: " + amount);
								updatePaymentAndTaxValue(monthDetails, source, reason, amount);
								
								//remove old member in map and put in new one
								//futurePayment.remove(i);					//Not neccessary
								futurePayment.set(i, monthDetails);
							}
							else
								continue;
						}
					}
					else{
						//if permChange is "T", apply change to only one month that matches taxDate
						for(int i = 0; i < futurePayment.size(); i++){
							FuturePay monthDetails = (FuturePay)futurePayment.get(i);
							if(monthDetails.Year == taxYear && monthDetails.Month == taxMonth){
								LOG.info(taxYear + "/" + taxMonth +  ": update " + permChange + " amount: " + amount);
								updatePaymentAndTaxValue(monthDetails, source, reason, amount);
								
								//remove old member in map and put in new one
								//futurePayment.remove(i);
								futurePayment.set(i, monthDetails);
								break;
							}
							else
								continue;
						}
					}
				}
				
				for(int i = 0; i < futurePayment.size(); i++){
					//recalculate the nett for each month in map
					FuturePay monthDetails = (FuturePay)futurePayment.get(i);
					monthDetails.Nett = monthDetails.Gross + monthDetails.Adjust - monthDetails.Tax;   
//					futurePayment.remove(i);
					futurePayment.set(i, monthDetails);
				}
				
			} catch (Exception e) {
				LOG.info(this.getClass().toString() + ".updatePaymentDetails: " + e.getMessage());
			}			
		}
	}
	
	/**
	 * Update map base on ps_ded data to get DEDUCT value
	 */
	private void updateDeductDetails(String bGroup, String refNo)
	{
		String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		String getDeductSQL = "select dedamt from ps_ded where bgroup = ? and refno = ? and startd <= ? and endd >= ?";
		if(conn != null)
		{
			for(int i = 0; i < futurePayment.size(); i++)
			{
				FuturePay monthDetails = (FuturePay)futurePayment.get(i);			
				
				// get month
				int month = monthDetails.Month;
				int year = monthDetails.Year;
				String monthInputStr = "01/" +  months[month - 1] + "/" + year;
				LOG.info("Update deduct for " + monthInputStr);
				try
				{
					PreparedStatement pstm = conn.prepareStatement(getDeductSQL);
					pstm.setString(1, bGroup);
					pstm.setString(2, refNo);
					pstm.setString(3, monthInputStr);
					pstm.setString(4, monthInputStr);
					
					ResultSet rs = pstm.executeQuery();
					double deduct = 0.0;
					while (rs.next())
					{
						deduct += rs.getDouble(1);
					}
					LOG.info("Deduct for " + monthInputStr + " has value: " + deduct);
					
					monthDetails.Adjust -= deduct;
					monthDetails.Nett = monthDetails.Gross + monthDetails.Adjust - monthDetails.Tax;
					
					// update back to the array
					futurePayment.set(i, monthDetails);
				}				
				catch (Exception e)
				{
					LOG.error("Error in updating payslip deduct for member of month: " + monthInputStr);
				}
			}			
		}
	}	
	
	/**
	 * Replace old data in map which was generated by history process
	 */
	private void replaceDetails(){
		String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		Variants var = new Variants();
		
		for(int i = 0; i < futurePayment.size(); i++){
			FuturePay monthDetails = (FuturePay)futurePayment.get(i);			
			String key = months[monthDetails.Month - 1];
			
			//remove all month details which have been recalculated if payment date is empty
			if (resultMap.get(key+"Date") != null && resultMap.get(key+"Date").trim().equals(""))
			{
				LOG.info("Update details for: " + monthDetails.Payment);
				resultMap.remove(key+"Date");
				resultMap.remove(key+"Gross");
				resultMap.remove(key+"Adjust");
				resultMap.remove(key+"Tax");
				resultMap.remove(key+"Nett");
				
				
				resultMap.put(key+"Date", monthDetails.Payment);

				//put new values if they > 0
				if(String.valueOf(monthDetails.Gross).compareTo("0.0") != 0)
					resultMap.put(key+"Gross", var.convert("Money", String.valueOf(monthDetails.Gross)));
				else
					resultMap.put(key+"Gross", var.convert("Money", "0"));
				
				if(String.valueOf(monthDetails.Adjust).compareTo("0.0") != 0)
					resultMap.put(key+"Adjust", var.convert("Money", String.valueOf(monthDetails.Adjust)));
				else
					resultMap.put(key+"Adjust",  var.convert("Money", "0"));
				
				if(String.valueOf(monthDetails.Tax).compareTo("0.0") != 0)
					resultMap.put(key+"Tax", var.convert("Money", String.valueOf(monthDetails.Tax)));
				else
					resultMap.put(key+"Tax",  var.convert("Money", "0"));
				
				resultMap.put(key+"Nett", var.convert("Money", String.valueOf(monthDetails.Nett)));				
			}
		}
		
		//recalculate total Gross of the year
		double totGross = 0;
		for(int i = 0; i < months.length; i++){
			//get value of each month
			String gross = (String)resultMap.get(months[i] + "Gross");
			if(gross != null && gross.compareTo("") != 0){
				//if value > 0 then replace currency symbol and commas 
				gross = gross.replaceAll("&#163;", "");
				gross = gross.replaceAll(",", "");
			}
			else{
				gross = "0";
			}
			
			totGross += Double.parseDouble(gross);
		}
		resultMap.remove("TotGross");
		resultMap.put("TotGross", var.convert("Money", String.valueOf(totGross)));
		
		//recalculate total Adjust of the year
		double totAdjust = 0;
		for(int i = 0; i < months.length; i++){
			String adjust = (String)resultMap.get(months[i] + "Adjust");
			if (adjust != null && adjust.compareTo("") != 0) {
				
				adjust = adjust.replaceAll("&#163;", "");
				adjust = adjust.replaceAll(",", "");
			}
			else{
				adjust = "0";
			}
			totAdjust += Double.parseDouble(adjust);
		}
		resultMap.remove("TotAdjust");
		resultMap.put("TotAdjust", var.convert("Money", String.valueOf(totAdjust)));
		
		//recalculate total Tax of the year
		double totTax = 0;
		for(int i = 0; i < months.length; i++){
			String tax = (String)resultMap.get(months[i] + "Tax");
			if (tax != null && tax.compareTo("") != 0) {
				tax = tax.replaceAll("&#163;", "");
				tax = tax.replaceAll(",", "");
			}			
			else{
				tax = "0";
			}
			totTax += Double.parseDouble(tax);
		}
		resultMap.remove("TotTax");
		resultMap.put("TotTax", var.convert("Money", String.valueOf(totTax)));
		
		//recalculate total Nett of the year
		double totNett = 0;
		for(int i = 0; i < months.length; i++){
			String nett = (String)resultMap.get(months[i] + "Nett");
			if (nett != null && nett.compareTo("") != 0) {
				nett = nett.replaceAll("&#163;", "");
				nett = nett.replaceAll(",", "");
			}
			else{
				nett = "0";
			}
			totNett += Double.parseDouble(nett);
		}
		resultMap.remove("TotNett");
		resultMap.put("TotNett", var.convert("Money", String.valueOf(totNett)));
		
	}
	
	/**
	 * Get Payment Method
	 * @param bGroup
	 * @param refNo
	 */
	private void getPaymentMethod(String bGroup, String refNo){
		if(conn != null){
			try {
				pstmt = conn.prepareStatement("select " +
						"pmcode from ps_pmdetails " +
						"where pmdno =(select max(pmdno) from ps_pm where bgroup=? and refno=? AND status != 'PENDING') " +
						"and bgroup=?");
				pstmt.setString(1, bGroup);
				pstmt.setString(2, refNo);
				pstmt.setString(3, bGroup);
				
				rs = pstmt.executeQuery();
				if(rs.next()){
					resultMap.put("PaymentMethod", rs.getString(1));
				}
			} catch (Exception e) {
				LOG.info(this.getClass().toString() + ".getPaymentMethod: " + e.getMessage());
			}			
		}
		
	}
	
	/**
	 * Get Tax Code - have not had any information about ps_tax table yet.
	 * @param bGroup
	 * @param refNo
	 */
	private void getTaxCode(String bGroup, String refNo){
		if(conn != null){
			try {
				pstmt = conn.prepareStatement("select distinct( taxcode ) " +
						"from ps_ptax where taxno=(select psetno from ps_pcontrol " +
						"where bgroup=? " +
						"and refno=?)");
				pstmt.setString(1, bGroup);
				pstmt.setString(2, refNo);
				
				rs = pstmt.executeQuery();
				if(rs.next()){
					resultMap.put("TaxCode", rs.getString(1));
				}
			} catch (Exception e) {
				LOG.info(this.getClass().toString() + ".getTaxCode: " + e.getMessage());
			}			
		}
	}
	
	/**
	 * compare source of change to calculate data for payment details of month
	 * @param monthDetails
	 * @param source
	 * @param reason
	 * @param amount
	 */
	private void updatePaymentAndTaxValue(FuturePay monthDetails, String source, String reason, double amount){
		if(source.compareTo("BPFUND") == 0)
			monthDetails.Gross += amount;
		
		if(source.compareTo("TAX") == 0)
			monthDetails.Tax -= amount;

		if(source.compareTo("ADDITION_LATE") == 0)
			monthDetails.Adjust -= amount;
		
//		if(source.compareTo("DEDUCT") == 0){
//			if(reason != null && reason.compareTo("PID") == 0)
//				monthDetails.Adjust += amount;
//		} 
		  
		if(source.compareTo("ADDITION") == 0)
			monthDetails.Gross += amount;
		
		if(source.compareTo("DEFAULT") == 0)
			monthDetails.Adjust = amount;

		if(source.compareTo("BCFUND") == 0)
			monthDetails.Gross += amount;
		
		if(source.compareTo("BCTRADE") == 0)
			monthDetails.Gross += amount;

		if(source.compareTo("PREDED") == 0){
			if(reason != null && reason.compareTo("PID") == 0)
				monthDetails.Adjust += amount;
		}
		
		if(source.compareTo("SPFUND") == 0)
			monthDetails.Gross += amount;
	}
	
}