package com.bp.pensionline.database;



/*
 * (c) copyright BP Pensions 2004
 */

//JDK Imports
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.commons.logging.Log;
import org.opencms.db.CmsDbPool;
import org.opencms.db.CmsSqlManager;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.CheckConfigurationKey;


/**
 *  A helper class associated with the database, using jar ojdbc14_g.jar
 *  
 *  This is the factory to the DB connetions
 *  FGGH
 *
 *  @author dcarlyle
 *  @author $Author: cvsuser $
 *  @version $Revision: 1.3 $
 * 
 *  $Id: DBConnector.java,v 1.3 2007/05/23 10:52:53 cvsuser Exp $
 */

/*
 * ISSUES: org.jboss.resource.adapter.jdbc.WrapperDataSource, extends data source,
 *  however we need: javax.sql.ConnectionPoolDataSource, so we get a class cast issue, even if we update the 
 *  jboss-web.xml
 *  
 *  So we ned to create our own datasources, by reading in from OpenCMS
 * 
 */
public class DBConnector {

  public static final Log log = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
  //Logger log = Logger.getLogger(DBConnector.class.getName());
   
  private static DBConnector dbConnector = null;
  
   
   /**
    * default constructor
    */
   private DBConnector() {
      super();      
      log.debug("DBConnector constructor");
      
   }

   public static DBConnector getInstance(){
	  if (dbConnector == null){
		  dbConnector = new DBConnector();
	  }
	  return dbConnector;
   }
   
   
   public int getPoolActivitySize(){
	   return -1;
   }

   public Connection getDBConnFactory(int environment) throws SQLException{  

	   Connection envDB = null;
	   
	   if (environment == Environment.AQUILA) {
		   
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }
	   else if (environment == Environment.LETTERS) {
		   
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }
	   else if (environment == Environment.CMS) {
		
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }	
	   else if (environment == Environment.SQL) {
		   /*
		    * Try to get Sql connection from CmsSqlmanager
		    */
		   
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection(CmsDbPool.getDefaultDbPoolName());
	   }
	   /*
	    * THIS IS FOR DB SPLITTING 
	    */ 
	   else if (environment == Environment.WEBSTATS) {
		  
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("webstats");
		   		 
	   }	
	   else if (environment == Environment.PENSIONLINE) {
			
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("pensionline");
		 
	   }	
	   else if (environment == Environment.AVIARY) {
			
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("aviary");
		 
	   }
	   else if (environment == Environment.EXTRAVIEW) {
			
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("ev");
		 
	   }
	   else if (environment == Environment.RELEASE) {
			
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("release");
		 
	   }
	   else if (environment == Environment.REPORT) {
			
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection("report");
		 
	   }
	   else
	   {
		   envDB = getDBConn(Environment.ORACLE_DB); 
	   }	   
	   
	   
	   // Try to get connection the second time

 
	   //log.info("HUY:--->Connection created: " + envDB);
	   return envDB;
   }
   
	public Connection establishConnection(String dbName)  throws SQLException
	{  
		Connection connection = null;

		int env = -1;
		if (dbName != null && dbName.equalsIgnoreCase("webstats"))
		{
			env = Environment.WEBSTATS;
		}
		if (dbName != null && dbName.equalsIgnoreCase("pensionline"))
		{
			env = Environment.PENSIONLINE;
		}
		if (dbName != null && dbName.equalsIgnoreCase("AQUILLA"))
		{
			env = Environment.AQUILA;
		}
		if (dbName != null && dbName.equalsIgnoreCase("aviary"))
		{
			env = Environment.AVIARY;
		}
		if (dbName != null && dbName.equalsIgnoreCase("ev"))
		{
			env = Environment.EXTRAVIEW;
		}
		if (dbName != null && dbName.equalsIgnoreCase("release"))
		{
			env = Environment.RELEASE;
		}
		if (dbName != null && dbName.equalsIgnoreCase("report"))
		{
			env = Environment.REPORT;
		}
		if (dbName != null && dbName.equalsIgnoreCase("opencms"))
		{
			env = Environment.SQL;
		}
		if (env == Environment.AQUILA)
		{
			connection = getPooledDBConn(Environment.ORACLE_DB);
		}
		else
		{
			connection = getDBConnFactory(env);
		}
		
		return connection;  
	  
	}    
   
	   /**
	    * default database access
	    * @return
	    * @throws SQLException
	    */  
	   private Connection getPooledDBConn(String jndiEnv) throws SQLException{
		   
		   Connection conn = null;
		   try {
	    	  
			   //log.info("Create a new connection for DBConnector, OracleConnectionCacheImpl");
			   
			   /* Create an instance of ConnCacheBean */
			   ConnCacheBean connCacheBean = ConnCacheBean.getInstance();
			      
			   /* Get OracleDataSource from the ConnCacheBean */
			   OracleDataSource ods = connCacheBean.getDataSource();
			   
			   if (ods != null)
			   {
				   conn = ods.getConnection();
			   }		
			   else
			   {
				   log.info("Datasource is nul");
			   }
	      } 
		  catch (Exception ex) {
//	    	  log.error("OracleConnectionCacheImpl critical issue with Orcale Pool", ex);
			  log.error("Get a connection from jdbc:XAOracleDS is fail", ex);
	    	  //ex.printStackTrace();
	      }
		  
		  return conn;
		  
	   }	
   
   /**
    * default database access
    * @return
    * @throws SQLException
    */  
   private Connection getDBConn(String jndiEnv) throws SQLException{
	   
	   Connection conn = null;
	   try {
    	  
		   //log.info("Create a new connection for DBConnector, OracleConnectionCacheImpl");
		   
		   /* Create an instance of ConnCacheBean */
//		   ConnCacheBean connCacheBean = ConnCacheBean.getInstance();
		      
		   /* Get OracleDataSource from the ConnCacheBean */
//		   OracleDataSource ods = connCacheBean.getDataSource();
		   
		   Context initContext = new InitialContext();
		   DataSource ods = (DataSource) initContext.lookup("java:XAOracleDS");
		   if (ods != null)
		   {
			   conn = ods.getConnection();
		   }		
		   else
		   {
			   log.info("Datasource is nul");
		   }
      } 
	  catch (Exception ex) {
//    	  log.error("OracleConnectionCacheImpl critical issue with Orcale Pool", ex);
		  log.error("Get a connection from jdbc:XAOracleDS is fail", ex);
    	  //ex.printStackTrace();
      }  
	  
	  // Try 1 more time if conn == null
	  if (conn == null)
	  {
		   try {
		    	  
			   //log.info("Create a new connection for DBConnector, OracleConnectionCacheImpl");
			
			   /* Create an instance of ConnCacheBean */
//			   ConnCacheBean connCacheBean = ConnCacheBean.getInstance();
			      
			   /* Get OracleDataSource from the ConnCacheBean */
//			   OracleDataSource ods = connCacheBean.getDataSource();
			   
			   Context initContext = new InitialContext();
			   DataSource ods = (DataSource) initContext.lookup("java:comp/env/jdbc/XAOracleDS");
			   if (ods != null)
			   {
				   conn = ods.getConnection();
			   }		
			   else
			   {
				   log.info("Datasource is nul");
			   }
	      } 
		  catch (Exception ex) {
//	    	  log.error("OracleConnectionCacheImpl critical issue with Orcale Pool", ex);
			  log.error("Get a connection from jdbc:XAOracleDS is fail", ex);
	    	  //ex.printStackTrace();
	      } 
	  }
      return conn;
   }
   
   
   /**
    * Debug info
    * 
    * @return
    */
   public String debug(){
	   
	   try{
		   StringBuffer dbString = new StringBuffer("DBConnection req to Aquila, hash code: " + this.hashCode());
		   dbString.append("\n maxLimit:  " + ConnCacheBean.getInstance().getCacheMaxLimit());		  
		   dbString.append("\n Cache Size(expecting 1 connection pool minimum): " + ConnCacheBean.getInstance().getCacheSize());     
		   
		   return dbString.toString();
		   
	   }catch(Exception exSQL){
		   return exSQL.getMessage();
	   }
   }
   
   /**
    * Debug info for HTML output
    * 
    * @return
    */
   public String debugHTML(){	   
	   
	   try{
		   StringBuffer dbString = new StringBuffer("<p>DBConnection req to Aquila, hash code: " + this.hashCode());
		   dbString.append("</p><p> maxLimit:  " + ConnCacheBean.getInstance().getCacheMaxLimit());		  
		   dbString.append("</p><p> Cache Size(expecting 1 connection pool minimum): " + ConnCacheBean.getInstance().getCacheSize());     
		   
		   return dbString.toString();
		   
	   }catch(Exception exSQL){
		   return exSQL.getMessage();
	   }	   
   }   
   
   /**
    * 
    */
   public Connection getJNDIConnection(String jndi) throws SQLException{
	   Connection conn = null;
       final String LOCATION = "getConnection()";
       log.info(LOCATION+":BEGIN");

       try {
           log.info("Start getting the connection...");
           // Create the Initial Naming Context
           Context ctx = new InitialContext();
           //Context ctx = (Context) new InitialContext().lookup("java:comp/env");
           if (ctx == null)
               throw new SQLException(LOCATION, "Could not create the Initial Naming Context");

           DataSource ds = (DataSource) ctx.lookup("java:"+jndi);
           if (ds == null) {
        	   ds = (DataSource) ctx.lookup(jndi);
           }
           //DataSource ds = (DataSource) ctx.lookup("/jdbc/"+jndi);
           conn = ds.getConnection();
           log.info("Got the connection successfully.");
           log.info("END");
       } catch (Exception ex) {
           ex.printStackTrace();
           throw new SQLException(LOCATION, ex.getLocalizedMessage());
       }
       return conn; 
   }
   
   public Connection getDirectConnection() {
	   Connection conn = null;
	   try {
		   Class.forName("oracle.jdbc.driver.OracleDriver");    
	       String url = CheckConfigurationKey.getStringValue("calcURL");  
	       conn = DriverManager.getConnection(url,CheckConfigurationKey.getStringValue("calcUserName"),CheckConfigurationKey.getStringValue("calcPassword"));
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	  return conn; 
   }
   
   public void close (Connection conn){
	   if (conn != null) {
  
			try {
//				log.info("Close conection... Current connections: " + ConnCacheBean.getInstance().getActiveSize() + " - Cache size: " + ConnCacheBean.getInstance().getCacheSize());
//				if (ConnCacheBean.getInstance().getActiveSize() >= 
//					ConnCacheBean.getInstance().getCacheSize() )  
				{
					conn.close();
					//log.info("Connection closed! " + ConnCacheBean.getInstance().getActiveSize());
				} 
			} catch (Exception eSql) {
				log.error("tried and failed to close connection", eSql);
			}
	   }
   }
   

}