package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.database.DBConnector;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/8
 * This class is used to change address in Oracle DB
 * 
 * @author Tu Nguyen
 * Updated to manipulate with Oracle DB
 *
 */

public class ChangeAddressSQLHandler{
	/*
	 * EOWSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Connection conn;	
	
	public ChangeAddressSQLHandler(int enviroment){
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			DBConnector connector = DBConnector.getInstance();
			conn  = connector.getDBConnFactory(enviroment);			
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}		
		
		System.out.println("ABC");
	}
	
	public void closeConnection(){
		try{
			DBConnector connector = DBConnector.getInstance();
			connector.close(conn);//con.close();

		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
		}
	}
	
	
	/*
	 * 
	 * Methods for Oracle Database----------------------------------------------------------->
	 * 
	 */

	/**
	 * Implement select method for Oracle DB  
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	public boolean selectOracleDb(String bGroup,String refNo){
		boolean exist = false;
			
		if(conn != null){		
			
			try {
				PreparedStatement stmt = conn.prepareStatement("select psa.*, psad.* from ps_address psa, ps_addressdetails psad where psa.bgroup=psad.bgroup and psa.addno=psad.addno" +
						" and psa.BGROUP = ? AND psa.REFNO = ? AND psa.endd is NULL");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				ResultSet rs = null;
				
				/*
				 * Excute query
				 */
				rs = stmt.executeQuery();
				
				if(rs.next()){
					/*
					 * Return true if exists
					 */
					exist = true;
				}	
				stmt.close();
			} 
			catch(SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
		}
		return exist;
	}
	

	/**
	 * Implement method to update address in Oracle DB
	 * @param bGroup
	 * @param refNo
	 * @param home
	 * @param street
	 * @param district
	 * @param town
	 * @param county
	 * @param postcode
	 * @param country
	 * @return
	 */
	public boolean updateOracleDb(String bGroup, String refNo, String home, String street, String district,
			String town, String county, String postcode, String country){
		
		boolean isUpdated = false;
		if(conn != null){
			try{
				PreparedStatement stmt;							
				ResultSet rs = null;
				
				
				int new_addno = 1;
				int new_authno = 1;
				int maxAdSequenceNumber = 1;
				int nextAdSequenceNumber = 1;
				
				int i = 0;
				boolean exist = this.selectOracleDb(bGroup, refNo);
				
				if(exist){
					/* 
					 *Select maximum addno and authno
					 */
					String selectAdd = "SELECT MAX(addno) FROM ps_addressdetails WHERE BGROUP = ? ";
					String selectAuth = "SELECT MAX(authno) FROM ps_address WHERE BGROUP = ? AND REFNO = ? AND ENDD is NULL";
					String selectSequence = "SELECT MAX(adseqno) FROM ps_address WHERE BGROUP = ? AND REFNO = ? ";
					
					stmt = conn.prepareStatement(selectAdd);
					stmt.setString(1, bGroup);
					//stmt.setString(2, refNo);
					rs = stmt.executeQuery();
					rs.next();
					String  maxAddNo = rs.getString(1);
					
					
					stmt = conn.prepareStatement(selectAuth);
					stmt.setString(1, bGroup);
					stmt.setString(2, refNo);
					rs = stmt.executeQuery();
					rs.next();
					String  maxAuthNo = rs.getString(1);					
					
					stmt = conn.prepareStatement(selectSequence);
					stmt.setString(1, bGroup);
					stmt.setString(2, refNo);
					rs = stmt.executeQuery();
				    rs.next();
				    maxAdSequenceNumber = this.convertMax(rs.getString(1));
				    nextAdSequenceNumber = this.addOneToSeq(rs.getString(1));
					
					/*
					 * Set end date for endd field in ps_address table				 
					 */
					stmt = conn.prepareStatement("UPDATE ps_address SET endd = SYSDATE WHERE BGROUP = ? AND REFNO = ? AND adseqno = ?");
					stmt.setString(1, bGroup);
					stmt.setString(2, refNo);
					stmt.setInt(3, maxAdSequenceNumber);
					i += stmt.executeUpdate();
					
					System.out.println("UPDATE ps_address SET endd = SYSDATE WHERE BGROUP = "+bGroup+" AND REFNO = "+refNo+" AND addno = "+Integer.parseInt(maxAddNo));
					
					/*
					 * Set new addno and new authno
					 */
					new_addno = addOneToSeq(maxAddNo);
					new_authno = addOneToSeq(maxAuthNo);
					
				}
				else{
					/*
					 * Do nothing 
					 */
					
				}
				
				/*
				 * Insert new row in to ps_address
				 */
				stmt = conn.prepareStatement("insert into ps_address (bgroup, refno, schmem, addcode, adseqno, startd, endd, pslip, addno, authno, astatus, tpflag) " +
						"values (?, ?,'M','GENERAL',?,SYSDATE, null, null, ?, ?, 'A', null)");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				stmt.setInt(3, nextAdSequenceNumber);
				stmt.setInt(4, new_addno);
				stmt.setInt(5, new_authno);
				i += stmt.executeUpdate();	
				System.out.println("insert into ps_address (bgroup, refno, schmem, addcode, adseqno, startd, endd, pslip, addno, authno, astatus, tpflag) " +
				"values ("+bGroup+", "+refNo+",'M','GENERAL',"+maxAdSequenceNumber+",SYSDATE, null, null, , "+new_addno+", "+new_authno+", 'A', null)");
				
				/*
				 *Insert new add into ps_addressdetails 
				 */
				stmt = conn.prepareStatement("insert into  ps_addressdetails  (BGROUP,ADDNO,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,ADDRESS5,POSTCODE,TELEPHON,COUNTRY,EMAILADR,FAX,ORGNAME,COUNTRY_CO,CONTACTN,ALT_TELE) " +
						"values (?, ?, ?, ?, ?, ?, ?, ?, null, ?, null, null, null,'N', null, null)");
				stmt.setString(1, bGroup);
				stmt.setInt(2, new_addno);
				stmt.setString(3, home);	// address line 1
				stmt.setString(4, street);	// address line 2
				stmt.setString(5, town);	// address line 3
				
				/*
				Manh updated http://www.c-mg.info/jira/browse/REPORTING-775
				*/
//				if(county.trim().equals("")){
//					stmt.setString(6, district);
//				}else{
//					stmt.setString(6, county+", "+district);
//				}
				// Huy updated http://www.c-mg.info/jira/browse/REPORTING-775 to store address line 4 and 5 in different columns
				stmt.setString(6, district); // address line 4
				stmt.setString(7, county);	 // address line 5
				stmt.setString(8, postcode);
				stmt.setString(9, country);				
				
								
				i += stmt.executeUpdate();
				
				/*
				 * Get new postcode key
				 */
				String newPostcodeKey = postcode.substring(0, postcode.indexOf(" "));
				/*
				 * Perform update PLOArea
				 */
				PLOAreaSQLHandler plo = new PLOAreaSQLHandler(conn);
				plo.updatePLOArea(bGroup, refNo, newPostcodeKey);
				plo.closeConnection();
				
				
				if(i == 3 || i == 2){
					isUpdated = true;
				}
				stmt.close();
			}
			catch (Exception e) {
				
				LOG.info(e.getMessage() +e.getCause());
			}			
		}
		else{
		}		
		return isUpdated;
	}
	
	public int addOneToSeq(String stringNum){
		if (stringNum != null){
			Integer tNum = Integer.valueOf(stringNum) + 1;
			
			return tNum.intValue();
		}
		return 1;
	}
	
	public int convertMax(String stringNum){
		if (stringNum != null){
			Integer tNum = Integer.valueOf(stringNum);
			
			return tNum.intValue();
		}
		return 1;
	}
	
	
}
