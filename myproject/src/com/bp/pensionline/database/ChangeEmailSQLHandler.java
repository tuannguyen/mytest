package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * This class is used to change email adress in Oracle DB
 */
public class ChangeEmailSQLHandler{
	/*
	 * EOWSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Connection conn;	
	
	public ChangeEmailSQLHandler(int enviroment){
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			DBConnector connector = DBConnector.getInstance();
			conn  = connector.getDBConnFactory(enviroment);			
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}		
	}
	
	public void closeConnection(){
		try{
			DBConnector connector = DBConnector.getInstance();
			connector.close(conn);//con.close();
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}
	}
	
	
	/*
	 * 
	 * Methods for Oracle Database----------------------------------------------------------->
	 * 
	 */
	
	
	/**
	 *  Implement select method for Sql DB  
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	public boolean selectOracleDb(String bGroup,String refNo){
		boolean isExist = false;
			
		if(conn != null){		
			
			try {
				
				PreparedStatement stmt = conn.prepareStatement("SELECT bgroup, refno, mpam01x FROM MP_ADDCOMM" +
				" WHERE BGROUP = ? AND REFNO = ? AND mpam01x = 'GENERAL'");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				
				ResultSet rs = null;	
				
				/*
				 * Excute query
				 */
				rs = stmt.executeQuery();
				
				if(rs.next()){
					/*
					 * Return true if exists
					 */
					isExist = true;
				}		
				stmt.close();
			} 
			catch(SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
		}
		return isExist;
	}

	
	/**
	 * Implement upadte method for Oracle DB
	 * @param bGroup
	 * @param refNo
	 * @param email
	 * @param address
	 * @return
	 */
	public boolean insertOracleDb(String bGroup, String refNo, String email, String address)	
	{
		int sequenceNo = getMaxCaseNoAddOne(bGroup, refNo);
		
		boolean isUpdated = false;
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("INSERT INTO MP_ADDCOMM (bgroup, refno, reftype, MPAM01X, MPAM02X, MPAM04X, SEQNO) " +
						"VALUES (?, ?, 'M', 'GENERAL', ?, ?,?)");				
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				stmt.setString(3, email);
				stmt.setString(4, address);
				stmt.setInt(5, sequenceNo);
				/*
				 * Excute query
				 */
				
				i = stmt.executeUpdate();
				stmt.close();
				
			} 
			catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if inserted or not
			 */
			if (i > 0) {
				isUpdated = true;
				
			} 
			
		}
		
		return isUpdated;
	}
	
	
	/**
	 * 
	 * Implement update method for Oracle DB		 
	 * @param bGroup
	 * @param refNo
	 * @param emailaddress
	 * @return
	 */
	public boolean updateOracleDb(String bGroup, String refNo, String emailaddress){
		
		boolean isUpdated = false;
				
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("UPDATE MP_ADDCOMM SET MPAM02X = ?, MPAM04X = ?" +
						" WHERE BGROUP = ? AND REFNO = ? AND MPAM01X ='GENERAL'");
				
				String email = emailaddress.substring(0,emailaddress.indexOf("@"));
				String address = emailaddress.substring(emailaddress.indexOf("@")+1);
			
				
				
				boolean exist = this.selectOracleDb(bGroup, refNo);
				if(exist){
					
					stmt.setString(1, email);
					stmt.setString(2, address);
					stmt.setString(3, bGroup);
					stmt.setString(4, refNo);
					/*
					 * Excute query
					 */
					i = stmt.executeUpdate();
				}
				else {
					//Insert new row
					isUpdated = this.insertOracleDb(bGroup, refNo, email, address);
				}
				stmt.close();
			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isUpdated = true;

			} 
		}
		return isUpdated;
		
	}
	
	public int getMaxCaseNoAddOne(String bgroup, String refno){
		
		Connection con = null;
		try {
			int caseCode = 0;
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm.executeQuery("SELECT MAX(SEQNO) AS CASENO FROM MP_ADDCOMM WHERE BGROUP='"+bgroup+"' AND REFNO='"+refno+"'");
			if (rs.next()) {
				caseCode = rs.getInt("CASENO") + 1;
			}
			else{
				caseCode = 1;
			}

			return caseCode;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error(e.getMessage() +e.getCause()+" bgroupRefo="+bgroup+refno);
			return 0;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}
	
	public int addOneToMaxSeq(String stringNum){
		if (stringNum != null){
			Integer tNum = Integer.valueOf(stringNum) + 1;
			
			return tNum.intValue();
		}
		return 1;
	}
	
	public int getMaxSeq(String stringNum){
		if (stringNum != null){
			Integer tNum = Integer.valueOf(stringNum);
			
			return tNum.intValue();
		}
		return 1;
	}
	
	
}