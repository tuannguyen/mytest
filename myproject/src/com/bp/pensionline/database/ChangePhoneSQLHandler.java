package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * This class is used to change phone, fax, mobile in Oracle DB
 *
 */

public class ChangePhoneSQLHandler{
	/*
	 * EOWSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Connection conn;	
	
	public ChangePhoneSQLHandler(int enviroment){
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			DBConnector connector = DBConnector.getInstance();
			conn  = connector.getDBConnFactory(enviroment);
			
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}		
	}
	
	public void closeConnection(){
		try{
			DBConnector connector = DBConnector.getInstance();
			connector.close(conn);//conn.close();

		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
		}
	}
	
	
	/*
	 * 
	 * Methods for Oracle Database----------------------------------------------------------->
	 * 
	 */
	
	/**
	 * Implement select method for Sql DB  
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	public boolean selectOracleDb(String bGroup,String refNo){
		boolean isExist = false;
			
		if(conn != null){		
			
			try {
				PreparedStatement stmt = conn.prepareStatement("SELECT bgroup, refno, mpam01x FROM MP_ADDCOMM" +
						" WHERE BGROUP = ? AND REFNO = ? AND mpam01x = 'GENERAL'");
				ResultSet rs = null;				
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
								
				rs = stmt.executeQuery();
				
				if(rs.next()){
					//Return true if exists
					isExist = true;
				}	
				stmt.close();
			} 
			catch(SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
		}
		return isExist;
	}

	/**
	 * Implement upadte method for Oracle DB
	 * @param bGroup
	 * @param refNo
	 * @param phone
	 * @param fax
	 * @param mobile
	 * @return
	 */
	public boolean insertOracleDb(String bGroup, String refNo, String phone, String fax, String mobile)	
	{
		int sequenceNo = getMaxCaseNoAddOne(bGroup, refNo);
		boolean isUpdated = false;
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("INSERT INTO MP_ADDCOMM (bgroup, refno, reftype, mpam01x, MPAM06X, MPAM05X, MPAM07X, SEQNO) " +
						"VALUES ( ?, ?, 'M', 'GENERAL', ?, ?, ?, ?)");				
				
				stmt.setString(1 , bGroup);
				stmt.setString(2 , refNo);
				stmt.setString(3 , phone);
				stmt.setString(4 , fax);
				stmt.setString(5 , mobile);
				stmt.setInt(6, sequenceNo);
				/*
				 * Excute query
				 */
				
				i = stmt.executeUpdate();
				stmt.close();
				
			} 
			catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if inserted or not
			 */
			if (i > 0) {
				isUpdated = true;
				
			} 
			
		}
		
		return isUpdated;
	}
	
	
	/**
	 * Implement update method for Oracle DB
	 * @param bGroup
	 * @param refNo
	 * @param phone  MPAM06X
	 * @param fax    MPAM05X
	 * @param mobile MPAM07X
	 * @return
	 */
	public boolean updateOracleDb(String bGroup, String refNo, String phone, String fax, String mobile){
		
		boolean isUpdated = false;
		
		//handle the empty string
		String phoneField = "MPAM06X";
		String faxField = "MPAM05X";
		String mobileField = "MPAM07X";
		
		if (phone.trim().equalsIgnoreCase("") || phone == null){
			phone = getFieldDetail(bGroup, refNo, phoneField);
		}
		
		if (fax.trim().equalsIgnoreCase("") || fax == null){
			fax = getFieldDetail(bGroup, refNo, faxField);
		}
		
		if (mobile.trim().equalsIgnoreCase("") || mobile == null){
			mobile = getFieldDetail(bGroup, refNo, mobileField);
		}
				
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("UPDATE MP_ADDCOMM SET MPAM06X = ?, MPAM05X = ?, MPAM07X = ?" +
						" WHERE BGROUP = ? AND REFNO = ? AND mpam01x='GENERAL'");
	
				boolean exist = this.selectOracleDb(bGroup, refNo);
				if(exist){
					stmt.setString(1, phone);
					stmt.setString(2, fax);
					stmt.setString(3, mobile);
					stmt.setString(4, bGroup);
					stmt.setString(5, refNo);
					/*
					 * Excute query
					 */
					i = stmt.executeUpdate();
				}
				else {
					//Insert new row
					isUpdated = this.insertOracleDb(bGroup, refNo, phone, fax, mobile);
				}
				stmt.close();
			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isUpdated = true;

			}
		}
		return isUpdated;
		
	}
	
	public int getMaxCaseNoAddOne(String bgroup, String refno){
		
		Connection con = null;
		try {
			int caseCode = 0;
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm.executeQuery("SELECT MAX(SEQNO) AS CASENO FROM MP_ADDCOMM WHERE BGROUP='"+bgroup+"' AND REFNO='"+refno+"'");
			if (rs.next()) {
				caseCode = rs.getInt("CASENO") + 1;
			}
			else{
				caseCode = 1;
			}

			return caseCode;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error(e.getMessage() +e.getCause()+" bgroupRefo="+bgroup+refno);
			return 0;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}
	
	public String getFieldDetail(String bgroup, String refno, String field){
		
		Connection con = null;
		try {
			String fieldDetail = null;
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			Statement stm = con.createStatement();
			ResultSet rs;
			rs = stm.executeQuery("SELECT "+field+" AS FIELD FROM MP_ADDCOMM WHERE BGROUP='"+bgroup+"' AND " +
					"REFNO='"+refno+"' and mpam01x='GENERAL' ");
			if (rs.next()) {
				fieldDetail = rs.getString("FIELD");
			}
			else{
				fieldDetail = null;
			}

			return fieldDetail;
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error(e.getMessage() +e.getCause()+" bgroupRefo="+bgroup+refno);
			return null;
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);//con.close();

				} catch (Exception ex) {

				}
			}
		}
	}
	
}