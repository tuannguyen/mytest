package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.SystemAccount;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 15 - 06 - 2007
 * @author Binh Nguye updated delete/checkUserExit method
 */
public class UserExpriedHandler {
	static Logger log = Logger.getLogger(UserExpriedHandler.class.getName());
	private static final String sqlInsert="INSERT INTO bp_force_change_password (Date,Data,Gname,System)VALUES(getDate(),?,?,?)";
	private static final String sqlDeleteByDataGname = 
		"DELETE FROM bp_force_change_password WHERE (Data= ? OR Data = ?) AND Gname= ?";
	private static final String sqlCheckExitByData = 
		"SELECT count(*) as count FROM bp_force_change_password  WHERE Data=?"
		+ " or Data = ?";
	private static final String SQL_UPDATE_PLNAME = "UPDATE BP_FORCE_CHANGE_PASSWORD"
		+ " SET [Data] = ? WHERE [Data] = ? OR [Data] = ?";
	
	/**
	 * Default constructor
	 */
	public UserExpriedHandler(){}
	
	public void insert(String data,String gname,String system) throws Exception {
		Connection con=null;
		try {
			PreparedStatement pstm=null;
			DBConnector connector=DBConnector.getInstance();
			//con=connector.getDBConnFactory(Environment.SQL);
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			pstm=con.prepareStatement(sqlInsert);
			pstm.setString(1, data);
			pstm.setString(2, gname);
			pstm.setString(3, system);
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			
		} catch (Exception e) {
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				log.error(ex);
			}
			log.error(e);
			throw e;
		}finally{
			try {
				DBConnector connector=DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
			
				log.error(e);
				throw e;
			}
		}
		
	}
	public void delete (String data,String gname) throws Exception {
		Connection con=null;
		try {
			log.info("Start delete user from force change password - " + data);
			PreparedStatement pstm=null;
			
			// Check and add prefix
			String fullData = data;
			if (!fullData.contains(SystemAccount.webUnitName)) {
				fullData = SystemAccount.webUnitName + data;
			}
			
			DBConnector connector=DBConnector.getInstance();
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			
			pstm=con.prepareStatement(sqlDeleteByDataGname);
			pstm.setString(1, data);
			pstm.setString(2, fullData);
			pstm.setString(3, gname);
			
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			
			log.info("End delete user from force change password - " + data);
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception ex) {
				log.error(ex);
			}
			log.error(e);
			throw e;
		}finally{
			try {
				DBConnector connector=DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				log.error(e);
				throw e;
				
			}
		}
		
	}
	
	public void update(String data, String plName) throws Exception {
		Connection con=null;
		try {
			log.info("Start update user from force change password - " + data);
			PreparedStatement pstm=null;
			
			// Check and add prefix
			String fullData = data;
			if (!fullData.contains(SystemAccount.webUnitName)) {
				fullData = SystemAccount.webUnitName + data;
			}
			
			if (!plName.contains(SystemAccount.webUnitName)) {
				plName = SystemAccount.webUnitName + plName;
			}
			
			DBConnector connector=DBConnector.getInstance();
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			
			pstm=con.prepareStatement(SQL_UPDATE_PLNAME);
			pstm.setString(1, plName);
			pstm.setString(2, data);
			pstm.setString(3, fullData);
			
			pstm.execute();
			con.commit();
			con.setAutoCommit(true);
			
			log.info("End update user from force change password - " + data);
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception ex) {
				log.error(ex);
			}
			log.error(e);
			throw e;
		}finally{
			try {
				DBConnector connector=DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				log.error(e);
				throw e;
				
			}
		}
		
	}
	
	public boolean checkUserExit (String data) throws Exception {
		Connection conn=null;
		boolean isExit=false;
		
		try {
			log.info("Start check user existing in force change password - " 
					+ data);
			PreparedStatement pstm=null;
			ResultSet rs=null;
			int count=0;
			
			// Check and add prefix
			String fullData = data;
			if (!fullData.contains(SystemAccount.webUnitName)) {
				fullData = SystemAccount.webUnitName + data;
			}
			
			DBConnector connector=DBConnector.getInstance();
			conn=connector.getDBConnFactory(Environment.PENSIONLINE);
			
			pstm=conn.prepareStatement(sqlCheckExitByData);
			pstm.setString(1, data);
			pstm.setString(2, fullData);
			
			rs=pstm.executeQuery();
			while (rs.next()){
				count=rs.getInt("count");
				if (count >0){
					isExit=true;
					Log.info("Found a row - " + data);
				}else{
					isExit=false;
					Log.info("Not found any row - " + data);
				}
			}
			
			log.info("End check user existing in force change password - " 
					+ data);
			return isExit;
			
		} catch (Exception e) {
			log.error(e);
			isExit=false;
			return isExit;
			
		} finally {
			if (conn!=null){
				try {
					DBConnector connector=DBConnector.getInstance();
					connector.close(conn);
				} catch (Exception e) {
					log.error(e);
					throw e;
				}
			}
		}
	}
}
