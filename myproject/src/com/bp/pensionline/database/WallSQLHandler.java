package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.StringUtil;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 07/05/2007
 * 
 *
 */
public class WallSQLHandler {
	
	public static final Log LOG = CmsLog.getLog(WallSQLHandler.class);
	private static final String sqlInsertQuery="INSERT INTO BP_Wall_groups (Date,Data,GName,SuperUser,System ) VALUES(getDate(),?,?,?,?)";
	//private static final String sqlSelectGName="SELECT GName,Data FROM BP_Wall_groups WHERE Data=?||'-'||?";
	private static final String sqlSelectGName="SELECT GName,Data FROM BP_Wall_groups WHERE Data=?";
	private static final String sqlDeleteByGName="DELETE FROM BP_Wall_groups WHERE GName =?";
	private static final String sqlSelectByGName="SELECT Data FROM BP_Wall_groups WHERE GName =?";
	
	/**
	 * Default contructor
	 */
	public WallSQLHandler(){}
	public static void update(String  data,String gName,String superUser,String system)throws Exception {
		Connection con=null;
		try 
		{
			DBConnector connector=DBConnector.getInstance();
			//con=connector.getDBConnFactory(Environment.SQL);
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			deleteByGname(gName, con);
			if (data!=null && data.length()>0)
			{
				insert(data, gName, superUser, system, con);
			}
			
			con.setAutoCommit(true);
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			LOG.info("com.bp.pensionline.database.WallSQLHandler.update error: " + e);
			try 
			{
				con.rollback();
			} 
			catch (Exception ex)
			{
				// TODO: handle exception
				LOG.info("com.bp.pensionline.database.WallSQLHandler.update can not rollback cause by: " + ex);
				throw ex;
			}
			throw e;
		}
		finally
		{
			if (con!=null)
			{
				try 
				{
					con.close();
				}
				catch (Exception e) 
				{
					// TODO: handle exception
					LOG.info("com.bp.pensionline.database.WallSQLHandler.update can not close connection cause by: "  + e);
					throw e;
				}
			}
		}
		
	}
	/**
	 * @param data
	 * @param gName
	 * @param superUser
	 * @param system
	 * @throws Exception
	 */
	public void insert(String  data,String gName,String superUser,String system) throws Exception 
	{
		Connection con=null;
		try 
		{
			DBConnector connector=DBConnector.getInstance();
			//con=connector.getDBConnFactory(Environment.SQL);
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			if (data!=null && data.length()>0)
			{
				insert(data, gName, superUser, system, con);
			}
			con.setAutoCommit(true);
		}
		catch (Exception e) 
		{
			LOG.error("com.bp.pensionline.database.WallSQLHandler.insert error: " + e);
			try 
			{
				con.rollback();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			throw e;
		}
		finally
		{
			if (con!=null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					throw e;
				}
			}
		}
		
	}
	
	/**
	 * @param data
	 * @param gName
	 * @param superUser
	 * @param system
	 * @param conn
	 * @throws Exception
	 * Insert data into BP_Wall_groups table (MySQL database)
	 */
	private static void insert(String  data,String gName,String superUser,
			String system,Connection conn) throws Exception 
	{
		
		try 
		{
			
			PreparedStatement pstm = conn.prepareStatement(sqlInsertQuery);
			pstm.setString(1, data);
			pstm.setString(2, gName);
			pstm.setString(3, superUser);
			pstm.setString(4, system);
			pstm.execute();
			
		}
		catch (Exception e) 
		{
			LOG.info("com.bp.pensionline.database.WallSQLHandler.insert error: " + e);
			throw e;
		}
		
	}
	public  void deleteByGname(String gName) throws Exception 
	{
		Connection con=null;
		try 
		{
			DBConnector connector=DBConnector.getInstance();
			//con=connector.getDBConnFactory(Environment.SQL);
			con=connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			deleteByGname(gName, con);
			con.setAutoCommit(true);
		} 
		catch (Exception e)
		{// TODO: handle exception
			LOG.error("com.bp.pensionline.database.WallSQLHandler.insert error: " + e);
			try 
			{
				con.rollback();
			} 
			catch (Exception ex) 
			{
				// TODO: handle exception				
				LOG.error(ex.getMessage());
				throw ex;
			}
			throw e;
		}
		finally
		{
			if (con!=null)
			{
				try 
				{
					con.close();
				}
				catch (Exception e)
				{
					// TODO: handle exception					
					LOG.error(e.getMessage());
					throw e;
				}
			}
		}
		
	}
	/**
	 * @param gName
	 * @param conn
	 * @throws Exception
	 * Delete from BP_Wall_groups table where GName=gName
	 */
	private static void deleteByGname(String gName,Connection conn) throws Exception {
		
		try {
			
			PreparedStatement pstm = conn.prepareStatement(sqlDeleteByGName);
			pstm.setString(1, gName);
			pstm.execute();
		}catch (Exception e) {
			// TODO: handle exception
			LOG.info("com.bp.pensionline.database.WallSQLHandler.deleteByGname error: ");
			LOG.info(e.getMessage() +e.getCause());
			
			throw e;
		}
		
	}
	
	/**
	 * @param bgroup
	 * @param refno
	 * @return
	 * @throws Exception
	 * Get list of gname ( group name)  by bgroup and refno
	 */
	public static List getGName(String bgroup,String refno)throws Exception 
	{
		Connection con=null;
		ResultSet rs=null;
		PreparedStatement pstm =null;
		List  <String > gName=new ArrayList<String>() ;
		try 
		{
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			pstm=con.prepareStatement(sqlSelectGName);
			pstm.setString(1,bgroup+"-"+refno);
			//pstm.setString(2,refno);
			rs=pstm.executeQuery();
			while (rs.next())
			{
				String tempData=rs.getString(Environment.WALL_DATA);// Data field's value in BP_Wall_groups table
				String tmpGname=rs.getString(Environment.WALL_GNAME);
				if (tempData.indexOf(bgroup+"-"+refno)!=-1 && tempData.indexOf("#")==-1)
				{
					// if tempData containt bgroup-refno (BPF-0066832) and not containt #
					gName.add(tmpGname);// add groupName for this record
				}
				
			}
			if (gName.size()>0)
			{				
				return gName;
			}
			else 
			{
				return null;
			}
			
		} catch (Exception e) 
		{
			// TODO: handle exception
			LOG.info("com.bp.pensionline.database.WallSQLHandler.getGName error: " + e);
			
			throw e;
		}
		finally
		{
			if (con!=null)
			{
				try 
				{
					con.close();
				} 
				catch (Exception e) 
				{
					// TODO: handle exception
					LOG.info("com.bp.pensionline.database.WallSQLHandler.getGName can not close connection by: " + e);					
				}
			}
		}
		
		
	}
	public List<String> getDataByGname(String gName)throws Exception
	{
		Connection con=null;
		ResultSet rs=null;
		PreparedStatement pstm =null;
		List <String> result=new ArrayList<String>();
		
		try 
		{
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			pstm=con.prepareStatement(sqlSelectByGName);
			pstm.setString(1,gName);
			
			rs=pstm.executeQuery();
			while (rs.next())
			{
				String temp=rs.getString("Data");
				if (temp!=null && temp.length() >0)
				{
					result.add(temp.trim());
				}
				else 
				{
					result.add(StringUtil.EMPTY_STRING);
				}
			}
			if (!result.equals(null)&&result.size()>0)
			{
				return result;
			}
			else 
			{
				return null;
			}
			
		} 
		catch (Exception e)
		{
			// TODO: handle exception
			LOG.info("com.bp.pensionline.database.WallSQLHandler.getDataByGname error: " + e);			
			throw e;
		}
		finally
		{
			if (con!=null)
			{
				try 
				{
					con.close();
				} 
				catch (Exception e)
				{
					// TODO: handle exception
					LOG.info("com.bp.pensionline.database.WallSQLHandler.getDataByGname can not close connection by: " + e);
				}
			}
		}
		
	}
}
