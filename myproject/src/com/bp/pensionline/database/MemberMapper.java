package com.bp.pensionline.database;

import java.io.ByteArrayInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.XmlReader;


public class MemberMapper{
	static Map<String, String> attributeTable= new HashMap<String, String>();
	static Map<String, String> mapField=new HashMap<String, String>();
	private Map<String, String> mapValue=new HashMap<String, String>();
	static Map<String, String> mapIndicator;
	
	public MemberMapper(){
			mapIndicator=new HashMap<String, String>();
				
			Document doc = parseFile(Environment.MEMBERMAPPER_FILE);
			
			for(int i=0; i<Environment.TAGNAME.length; i++){
				paramMapper(doc, Environment.TAGNAME[i]);            	
			}
     
	}
	
	//ParseXML
	public Document parseFile(String fileName) {
       DocumentBuilder docBuilder;
       Document doc = null;
       XmlReader reader = new XmlReader();
       
       ByteArrayInputStream is = null;
       
       DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
       
       docBuilderFactory.setIgnoringElementContentWhitespace(true);
       
       try {
    	   
            docBuilder = docBuilderFactory.newDocumentBuilder();

            is = new ByteArrayInputStream(reader.readFile(fileName));
            
        }
       	catch(FileNotFoundException  fnfe){
    	   System.out.println("Cannot found the file " + fileName);
    	   return null;
       	}
        catch (ParserConfigurationException e) {
        	
            System.out.println("Wrong parser configuration: " + e.getMessage());
            return null;
        }
        
        try {
        	
        	doc = docBuilder.parse(is);
        	
        }
        catch (SAXException e) {
            System.out.println("Wrong XML file structure: " + e.getMessage());
            return null;
        }
        catch (IOException e) {
            System.out.println("Could not read source file: " + e.getMessage());
        }
        return doc;
    }
	
	
	
	// assign map value with given document & tagname in XML file
	/**
	 * @param doc
	 * @param tagName
	 * */
	public void paramMapper(Document doc, String tagName)
	{
		doc.getDocumentElement().normalize();
        
        NodeList nList=doc.getElementsByTagName(tagName);
        
       	for(int s=0;s<nList.getLength();s++)
    	{
    		 Node POSNode=nList.item(s);
    		 NodeList child=POSNode.getChildNodes();
    	
    		 for (int i=0; i<child.getLength();i++){
    			 Node childPOSNode=child.item(i);
    			 if(childPOSNode.getNodeType()==Node.ELEMENT_NODE){
    			
    				String key=childPOSNode.getNodeName();
    					
        			NodeList grandChildPOSNode=childPOSNode.getChildNodes();
        			for (int e=0;e<grandChildPOSNode.getLength();e++)
        			{
        			 Node grandChildPOSNodeValue=grandChildPOSNode.item(e);
        			 if(grandChildPOSNodeValue.getNodeType()==Node.TEXT_NODE){
        				 
        			 String value=grandChildPOSNodeValue.getNodeValue();
        			 try{
        			
        				if(value.length()>=1){
        						if ((int)value.charAt(0)==163)
        							value=value.substring(1, value.length());        						
        				}
        				
        				int countFullStop=0;
        				for(int loop=0; loop<value.length(); loop++){
        						if ((int)value.charAt(loop)==46)
        						countFullStop++;        					
        				}
        				if((countFullStop==0)){
        						mapValue.put(key, value);
        						mapIndicator.put(key,tagName);
        				}
        	
        				else{
        					StringTokenizer st=new StringTokenizer(value,".");
	      					String tableName= st.nextToken();
	      					String fieldSelect;
	      					if(countFullStop>1)
	      					{
	      						if (tableName.equalsIgnoreCase("category_detail"))
	      							fieldSelect=value.substring(tableName.length()+1,value.length());
	      						else
	      							fieldSelect=value;
	      					}
	      					else 
	      						fieldSelect=st.nextToken();
	      					      			
	      					attributeTable.put(fieldSelect, tableName);
	             			mapField.put(key, fieldSelect);
	             		
	             			mapValue.put(key, "");
	             			
	             			if(mapIndicator.get(key)==null)
	             				mapIndicator.put(key,tagName);
	             			else
	             				mapIndicator.put(key,mapIndicator.get(key)+tagName );
	             		
        				}       				     				
        			 }
     					catch(Exception ex){
     							ex.getStackTrace();
     					}
        			 }
        			 }
        			}
    			 }
    		 
    	    	}
       	}
	
	public Map<String, String> getAttributeTable(){
		return attributeTable;
	}
	
	public Map<String, String> getMapField(){
		return mapField;
	}
	
	public Map<String, String> getMapValue(){
		return mapValue;
	}
	public Map<String, String> getMapIndicator(){
		return mapIndicator;
	}
	
	/**
	 * @param tableName
	 * @return 
	 * 
	 * */
	
	public String allFieldNeeded(String tableName){
	
		StringBuffer output=new StringBuffer("");
		Iterator keys=mapField.keySet().iterator();
		
		int i=0;
		while(keys.hasNext()){
			Object currentTag=keys.next();
			Object currentKey=mapField.get(currentTag);
			
			if(attributeTable.get(currentKey).toString().equalsIgnoreCase(tableName)){
				if(i>0){
					output.append(",");	
				}
				output.append(currentKey+" "+currentTag+"_");
				i++;	
			}
		
		}
		return output.toString();
		
	}    //end of allFieldNeeded

}