package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.database.DBConnector;
/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/5
 * This class is used to select and update data for EOW module
 *
 */

public class EOWSQLHandler{
	/*
	 * EOWSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Connection conn;	
	
	public EOWSQLHandler(int enviroment){
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			DBConnector connector = DBConnector.getInstance();
			conn  = connector.getDBConnFactory(enviroment);			
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
		}		
	}
	
	public void closeConnection(){
		try{
			conn.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
		}
	}
	
	
	/*
	 * 
	 * Methods for SQL DB------------------------------------------------------------------------>
	 * 
	 */
	
	
	/**
	 * Implement select method for Sql DB  
	 * @param bgroup
	 * @param refno
	 * @return
	 */
	public Map selectSqlDb(String bgroup, String refno){
		/*
		 * 
		 */
		Map resultMap = new HashMap();
		if(conn != null){		
			
			try {
				PreparedStatement stmt = conn.prepareStatement("SELECT Message FROM BP_EOW_INFO " +
						"WHERE Archived = 'No' AND BGROUP = ? AND REFNO = ?");
				stmt.setString(1, bgroup);
				stmt.setString(2, refno);
				ResultSet rs = null;
				
				/*
				 * Excute query
				 */
				rs = stmt.executeQuery();
				while(rs.next()){
					/*
					 * Decode parameters
					 */
					String message = com.bp.pensionline.util.Base64Decoder.decode(rs.getString(1));
					
					/*
					 * Put values into map
					 */				
					resultMap.put("message", message);
				}
				stmt.close();				

			} catch (SQLException sqlEx) {
				LOG.error("SQLException: " + sqlEx.getMessage() + sqlEx.getCause());
			}
		}
		
		return resultMap;
	}
	
	
	/**
	 * Implement insert method for Sql DB
	 * @param bgroup
	 * @param refno
	 * @param mess
	 * @param form
	 * @return
	 */
	public boolean insertSqlDb(String bgroup, String refno, String mess, String form){
		
		if (mess != null && !"".equals(mess))
				LOG.info("Insert the Additional Information with  BGROUP: " + bgroup + " REFNO: " + refno + " MESSAGE: " + mess);
		else 
			    LOG.info("Delete the Additional Information with  BGROUP: " + bgroup + " REFNO: " + refno);
		
		boolean isInserted = false;
		if(conn != null){			
			int i = 0;
			try {
				
				/*
				 *Create EOW table if not exists 
				 */
				//This sql for MySQL
//				PreparedStatement stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `BP_EOW_INFO` (" +
//						"BGroup VARCHAR(15), Refno VARCHAR(15), " +
//						"CreationDate DATETIME, Message TEXT, Form TEXT, " +
//						"Archived VARCHAR(3), CaseworkID VARCHAR(15))");
				
				//This sql for Microsoft SQL Server
				PreparedStatement stmt = conn.prepareStatement("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].BP_EOW_INFO') AND type in (N'U'))" +
						" CREATE TABLE dbo.BP_EOW_INFO(" +
						" BGroup VARCHAR(15), Refno VARCHAR(15)," + 
						" CreationDate DATETIME, Message TEXT, Form TEXT," + 
						" Archived VARCHAR(3), CaseworkID VARCHAR(15))");
				stmt.execute();
				
				/*
				 * Encode parameters
				 */				
				String message = com.bp.pensionline.util.Base64Encoder.encoder(mess.getBytes());
				String formContent = com.bp.pensionline.util.Base64Encoder.encoder(form.getBytes());
				
				/*
				 * Build query
				 */
				stmt = conn.prepareStatement("INSERT INTO BP_EOW_INFO (BGroup, Refno, CreationDate, Message, Form, Archived, CaseworkID)" +
						" VALUES (?, ?, getDate(), ?, ?, 'No', null)");
				stmt.setString(1, bgroup);
				stmt.setString(2, refno);
				stmt.setString(3, message);
				stmt.setString(4, formContent);
			
				/*
				 * Excute query
				 */
				i = stmt.executeUpdate();
				stmt.close();
			} catch (SQLException sqlEx) {
				LOG.error("SQLException: " + sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isInserted = true;

			}		
			
		}
		
		return isInserted;
	}
	
	
	/**
	 *  Implement update method for Sql DB
	 * @param bgroup
	 * @param refno
	 * @return
	 */
	public boolean updateSqlDb(String bgroup, String refno){
		boolean isUpdated = false;
			
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("UPDATE BP_EOW_INFO SET Archived = 'Yes' " +
						"WHERE Archived ='No' AND BGROUP = ? AND REFNO = ?");
				stmt.setString(1, bgroup);
				stmt.setString(2, refno);
				
				/*
				 * Excute query
				 */
				i = stmt.executeUpdate();
				stmt.close();

			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isUpdated = true;
			} 		
			
		}		
		return isUpdated;		
	}
	
	
	/*
	 * 
	 * Methods for Oracle Database----------------------------------------------------------->
	 * 
	 */
	
	/**
	 * Implement select method for Sql DB  
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	public Map selectOracleDb(String bGroup,String refNo){
		Map resultMap = new HashMap();
			
		if(conn != null){		
			
			try {
				
				/*
				 * select data to show Lump Sum
				 * --------------------------Mapper
					<Title>			ND05X	title
					<Forenames>		ND03X	forenames
					<Initials>		ND04X	initials
					<Surname>		ND02X	surname
					<Street>		ND07X, --Address Line 1
					<PostalTown>	ND08X, --Address Line 2
					<District>		ND09X, --Address Line 3
					<County>		ND10X, --Address Line 4					
					<Postcode>		ND17X	postcode
					<Country>		ND16X	country
					<Relationship>	ND11X	relationship
					<Percentage>	ND12N	percentage
				 * 
				 */
				PreparedStatement stmt = conn.prepareStatement("SELECT ND05X, ND03X, ND04X, ND20A, ND07X, ND08X, ND09X, ND10X, ND15X, ND17X, ND16X, ND11X, ND12N" +
						" FROM NOMINATION_DETAIL WHERE BGROUP = ? AND REFNO = ? AND ND12N is not Null AND ND19D is Null");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				ResultSet rs = null;
				
				/*
				 * Excute Lump Sum query
				 */
				rs = stmt.executeQuery();
				
				/*
				 * Put values of Lump Sum in resultMap
				 */	
				HashMap lumpMap = new HashMap();
				while(rs.next()){
					HashMap map = new HashMap();//Once get new row create a new map					
					
					map.put("Title",rs.getString("ND05X"));
					map.put("Forenames",rs.getString("ND03X"));
					map.put("Initials",rs.getString("ND04X"));
					map.put("Surname",rs.getString("ND20A"));
					map.put("Street",rs.getString("ND07X"));
					map.put("PostalTown",rs.getString("ND08X"));
					map.put("District",rs.getString("ND09X"));
					map.put("County",rs.getString("ND10X"));
					map.put("Postcode",rs.getString("ND17X"));
					map.put("Country",rs.getString("ND16X"));
					map.put("Relationship",rs.getString("ND11X"));
					map.put("Percentage",rs.getString("ND12N"));
					
					lumpMap.put(rs.getRow(), map);
				}
				
				/*
				 * select data to show Survivor Pension 
				 */
				stmt = conn.prepareStatement("SELECT ND05X, ND03X, ND04X, ND20A, ND07X, ND08X, ND09X, ND10X, ND15X, ND17X, ND16X, ND11X" +
						" FROM NOMINATION_DETAIL WHERE BGROUP = ? AND REFNO = ? AND ND13N is not Null AND ND19D is Null");
				//use ND13N not ND12N
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				/*
				 * Excute Survivor query
				 */
				rs = stmt.executeQuery();			
				
				/*
				 * Put values of Survivor in resultMap
				 */
				HashMap surMap = new HashMap();
				while(rs.next()){
					
					HashMap map = new HashMap();//Once get new row create a new map
					
					map.put("Title",rs.getString("ND05X"));
					map.put("Forenames",rs.getString("ND03X"));
					map.put("Initials",rs.getString("ND04X"));
					map.put("Surname",rs.getString("ND20A"));
					map.put("Street",rs.getString("ND07X"));
					map.put("PostalTown",rs.getString("ND08X"));
					map.put("District",rs.getString("ND09X"));
					map.put("County",rs.getString("ND10X"));
					map.put("Postcode",rs.getString("ND17X"));
					map.put("Country",rs.getString("ND16X"));					
					map.put("Relationship",rs.getString("ND11X"));
					surMap.put(rs.getRow(), map);
				}
				
				//Store maps in resultMap
				resultMap.put("LumpSumMap", lumpMap);
				resultMap.put("SurvivorMap", surMap);
				stmt.close();

			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
		}
		return resultMap;
	}

	
	/**
	 * Implement upadte method for Oracle DB
	 * @param type
	 * @param bGroup
	 * @param refNo
	 * @param title
	 * @param forenames
	 * @param street
	 * @param postalTown
	 * @param initials
	 * @param surname
	 * @param district
	 * @param county
	 * @param postcode
	 * @param country
	 * @param relationship
	 * @param percentage
	 * @return
	 */
	public boolean insertOracleDb(String type, String bGroup, String refNo, String title,
			String forenames, String street, String postalTown, String initials, String surname,
			  String district, String county, String postcode, String country,
			    String relationship, String percentage)	
	{
		
		boolean isUpdated = false;
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt;
				ResultSet rs;
				String seqno = "1";
				
				stmt = conn.prepareStatement("SELECT MAX(SEQNO) FROM NOMINATION_DETAIL WHERE BGROUP = ? AND REFNO = ?");
				
				
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				
				//executeQuery
				rs = stmt.executeQuery();				
				
				while(rs.next()){
					String  maxSeqno = rs.getString(1);
					if(maxSeqno != null){
						seqno = String.valueOf(Integer.parseInt(maxSeqno) + 1);
					}
									
				}								
							
				
				/*
				 * Insert new data
				 *  ------------------------------------Mapper
				 *  BGROUP                         VARCHAR2        3            N    Business Group
					REFNO                          VARCHAR2        7            N    Reference Number
					SEQNO                          NUMBER         22    3     0 N    Sequence Number
					ND02X                          VARCHAR2       20            Y    Surname Of Nomination
					ND03X                          VARCHAR2       32            Y    Forenames
					ND04X                          VARCHAR2        4            Y    Initials
					ND05X                          VARCHAR2       10            Y    Title
					ND06X                          VARCHAR2        1            Y    Sex Of Nomination
					ND07X                          VARCHAR2       25            Y    Address Line 1
					ND08X                          VARCHAR2       25            Y    Address Line 2
					ND09X                          VARCHAR2       25            Y    Address Line 3
					ND10X                          VARCHAR2       25            Y    Address Line 4
					ND11X                          VARCHAR2       20            Y    Relationship
					ND12N                          NUMBER         22    9     5 Y    Lump Sum Percentage
					ND13N                          NUMBER         22    9     5 Y    Pension Percentage
					ND14D                          DATE            7            Y    Date Of Nomination
					ND15X                          VARCHAR2       25            Y    Address Line 5
					ND16X                          VARCHAR2       25            Y    Country
					ND17X                          VARCHAR2        8            Y    Post Code
					ND18D                          DATE            7            Y    Date Of Birth
					ND19D                          DATE            7            Y    Date Nomination Revoked
					ND20A                          VARCHAR2       20            Y    Mixed Case Surname
					ND21P                          NUMBER         22   15     2 Y    Amount Paid
					ND22D                          DATE            7            Y    Date Amount Paid
				 */
				
				
				if(type.equals("LumpSum")){
					stmt = conn.prepareStatement("INSERT INTO NOMINATION_DETAIL " +
							"(BGROUP, REFNO, SEQNO, ND05X, ND04X, ND02X, ND03X,	ND07X, ND08X, ND09X, ND10X, ND17X, ND16X, ND11X, ND12N, ND14D, ND19D, ND20A) " +
							"VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (select sysdate from dual), null, ?)");	
				}
				else{
					stmt = conn.prepareStatement("INSERT INTO NOMINATION_DETAIL " +
							"(BGROUP, REFNO, SEQNO, ND05X, ND04X, ND02X, ND03X,	ND07X, ND08X, ND09X, ND10X, ND17X, ND16X, ND11X, ND13N, ND14D, ND19D, ND20A) " +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (select sysdate from dual), null, ?)");
				}
				
				/** ND20A: Surname in lower case, ND02X: Surname in upper case.*/
				 
				String surnameUpperCase = surname.toUpperCase();
				
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				stmt.setInt(3, Integer.parseInt(seqno));
				stmt.setString(4, title);
				stmt.setString(5, initials);
				stmt.setString(6, surnameUpperCase);
				stmt.setString(7, forenames);
				stmt.setString(8, street);
				stmt.setString(9, postalTown);
				stmt.setString(10, district);
				stmt.setString(11, county);
				stmt.setString(12, postcode);
				stmt.setString(13, country);
				stmt.setString(14, relationship);
				stmt.setFloat(15, Float.parseFloat(percentage));
				stmt.setString(16, surname);
				
				
				/*
				 * Excute query
				 */
				i = stmt.executeUpdate();
				stmt.close();
				
			} 
			catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isUpdated = true;

			} 		
		}		
		return isUpdated;
	}
	
	
	
	/**
	 *  Implement update old data by set null end
	 * @param bGroup
	 * @param refNo
	 * @return
	 */
	public boolean updateOracleDb(String bGroup, String refNo){
		
		boolean isUpdated = false;
				
		if(conn != null){			
			int i = 0;
			try {
				PreparedStatement stmt = conn.prepareStatement("UPDATE NOMINATION_DETAIL SET ND19D = SYSDATE " +
						"WHERE ND19D is NULL AND BGROUP = ? AND REFNO = ?");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				/*
				 * Excute query
				 */
				i = stmt.executeUpdate();
				stmt.close();

			} catch (SQLException sqlEx) {
				LOG.info(sqlEx.getMessage() + sqlEx.getCause());
			}
			
			/*
			 * Return if updated or not
			 */
			if (i > 0) {
				isUpdated = true;

			} 
		}
		return isUpdated;
		
	}
}