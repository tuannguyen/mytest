package com.bp.pensionline.database;

import java.io.ByteArrayInputStream;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.test.XmlReader;

public class RecordMapper {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	static Map<String, String> attributeTable = new HashMap<String, String>();

	static Map<String, String> mapField = new HashMap<String, String>();

	static Map<String, String> mapValue = new HashMap<String, String>();

	public RecordMapper() {
		//RecordDAO record=new RecordDAO();
		//String filePath=record.getFilePath();
		try {

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = null;
			XmlReader reader = new XmlReader();

			ByteArrayInputStream is = new ByteArrayInputStream(reader
					.readFile(Environment.RECORDMAPPER_FILE));

			doc = docBuilder.parse(is);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(Environment.POSTAG);

			for (int s = 0; s < nList.getLength(); s++) {
				Node POSNode = nList.item(s);
				NodeList child = POSNode.getChildNodes();

				for (int i = 0; i < child.getLength(); i++) {
					Node childPOSNode = child.item(i);
					if (childPOSNode.getNodeType() == Node.ELEMENT_NODE) {

						String key = childPOSNode.getNodeName();

						NodeList grandChildPOSNode = childPOSNode
								.getChildNodes();
						for (int e = 0; e < grandChildPOSNode.getLength(); e++) {
							Node grandChildPOSNodeValue = grandChildPOSNode
									.item(e);
							if (grandChildPOSNodeValue.getNodeType() == Node.TEXT_NODE) {

								String value = grandChildPOSNodeValue
										.getNodeValue();
								try {
									StringTokenizer st = new StringTokenizer(
											value, ".");
									String tableName = st.nextToken();
									String fieldSelect = st.nextToken();
									attributeTable.put(fieldSelect, tableName);
									//LOG.info("mapField put: " + fieldSelect + " - " + tableName);
									mapField.put(key, fieldSelect);
									//LOG.info("mapField put: " + key + " - " + fieldSelect);
									mapValue.put(key, " ");
								} catch (Exception ex) {
									ex.getStackTrace();
								}
							}
						}

					}

				}

			}

			//System.out.println(attributeTable.get("Bgroup").toString());

		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/*public Map getAttributeTable(){
	 return attributeTable;
	 }*/

	public Map<String, String> getMapField() {
		return mapField;
	}

	public Map<String, String> getMapValue() {
		return mapValue;
	}

	public String allFieldNeeded(String tableName) {
		StringBuffer output = new StringBuffer("");
		Iterator keys = attributeTable.keySet().iterator();

		int i = 0;
		while (keys.hasNext()) {
			Object currentKey = keys.next();
			//LOG.info("allFieldNeeded key: " + currentKey);
			//LOG.info("allFieldNeeded table: " + attributeTable.get(currentKey));
			if (attributeTable.get(currentKey).toString().equals(tableName)) {
				if (i > 0) {
					output.append(",");
				}
				//LOG.info("allFieldNeeded append: " + currentKey);
				output.append(currentKey);
				i++;
			}

		}

		return output.toString();

	}

	//end of main

}