package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;


/**
 * 
 * @author SonNT
 * @version 1.0
 * @since 2007/5/9
 * This class is used to udpate PLOArea when user update their address
 * 
 */
public class PLOAreaSQLHandler{
	/*
	 * PLOAreaSQLHandler use to connect database and excute statements
	 */
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	private Connection conn;
	
	public PLOAreaSQLHandler(Connection connection){
		conn = connection;
	}	
	
	public void closeConnection(){
		try{
			DBConnector connector = DBConnector.getInstance();
			connector.close(conn);//conn.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/*
	 * 
	 * Methods for Oracle Database----------------------------------------------------------->
	 * 
	 */	
	
	/**
	 * Update PLOArea
	 * @param bGroup
	 * @param refNo
	 * @param newPostcodeKey
	 * @return
	 */
	public boolean updatePLOArea(String bGroup, String refNo, String newPostcodeKey){
		
		boolean isUpdated = false;
		try{
			//Get new PLOArea
			String newPLOArea = ""; 
			String oldPLOArea = "";
			String isOversea = "N";
			int i = 0;
			PreparedStatement stmt = conn.prepareStatement(" SELECT 'AR' || pcd01x as new_plo_area FROM postcode_detail" +
					" WHERE postcode = ?" +
					" AND bgroup = ?");
			stmt.setString(1, newPostcodeKey);
			stmt.setString(2, bGroup);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				newPLOArea = rs.getString(1);
			}
			
			
			//Check if the member is oversea or not
			stmt = conn.prepareStatement("SELECT AR08X FROM address_detail WHERE bgroup = ? AND refno = ? AND AR08X ='Y'");
			stmt.setString(1, bGroup);
			stmt.setString(2, refNo);
			rs = stmt.executeQuery();
			while(rs.next()){
				isOversea = rs.getString(1);
				if(isOversea.compareTo("Y") == 0){
					//if member is oversea then ploarea is AR37
					newPLOArea = "AR37";
					
				}
			}
			
			
			//Check if the member does exist in the WELFARE_DETAIL table or not
			boolean exist = false;
			stmt = conn.prepareStatement("select * from WELFARE_DETAIL WHERE bgroup = ? AND refno = ?");
			stmt.setString(1, bGroup);
			stmt.setString(2, refNo);
			rs = stmt.executeQuery();
			while(rs.next()){
				exist = true;
			}
			
			if(exist){
				//Get old PLOArea
				stmt = conn.prepareStatement("select WD01X from WELFARE_DETAIL WHERE bgroup = ? AND refno = ?");
				stmt.setString(1, bGroup);
				stmt.setString(2, refNo);
				rs = stmt.executeQuery();
				while(rs.next()){
					oldPLOArea = rs.getString(1);
				}
				
				if(oldPLOArea != null && oldPLOArea.compareTo("") != 0){
					//Compare old and new value
					if(oldPLOArea.compareTo(newPLOArea) == 0){
						//if equals, do nothing
						
					}
					else{
						//perform update
						
						stmt = conn.prepareStatement("UPDATE WELFARE_DETAIL SET WD01X = ?, WD06D = sysdate, WD09X = ? WHERE bgroup = ? AND refno = ?");
						stmt.setString(1, newPLOArea);
						stmt.setString(2, oldPLOArea);
						stmt.setString(3, bGroup);
						stmt.setString(4, refNo);
						i = stmt.executeUpdate();					
					}
				}
				else{
					//perform update
					
					stmt = conn.prepareStatement("UPDATE WELFARE_DETAIL SET WD01X = ?, WD06D = sysdate, WD09X = null WHERE bgroup = ? AND refno = ?");
					stmt.setString(1, newPLOArea);					
					stmt.setString(2, bGroup);
					stmt.setString(3, refNo);
					i = stmt.executeUpdate();
				}
			}
			else{
				//insert new row
				
				stmt = conn.prepareStatement("INSERT INTO WELFARE_DETAIL (BGROUP, REFNO, WD01X, WD06D) VALUES (?, ?, ?, sysdate)");
				stmt.setString(1, bGroup);					
				stmt.setString(2, refNo);
				stmt.setString(3, newPLOArea);
				i = stmt.executeUpdate();
			}			
			
			
			if(i > 0){
				isUpdated = true;
				
			}
			stmt.close();
			
		}
		catch (Exception e) {
			// TODO: handle exception
			LOG.info(e.getMessage() +e.getCause());
		}
		
		return isUpdated;
		
	}
}
