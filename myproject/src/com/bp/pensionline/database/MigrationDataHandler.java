package com.bp.pensionline.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.DateUtil;
import com.bp.pensionline.util.StringUtil;

/**
 * Reads in the accounts from the old system and migrates them into this system (CMS)
 * 
 * 
 * To find a user with multiple Records:
 * -- shows ALL multiple records owners 
 * select distinct(bd08x) from basic where bd08x in (
 * select distinct(bd08x) from basic where bd08x in ( 
 * Select BD08x from basic 
 * where 
 * (BD19A='AC' OR BD19A='PP' OR BD19A='PN'  OR BD19A='PC' OR BD19a='WB') 
 * group by BD08x having count(*) > 1) )
 * 
 * @author Duy Thao Nguyen
 * @version 1.0
 * 
 *
 * Note: ONLY handle BPF and BCF, do not handle SPF (supplementary Pension fund for high earners)
 */
public class MigrationDataHandler {
	static Logger log = Logger.getLogger(DateUtil.class.getName());
	private static final String sqlCheckMigrationData="select count(*) as count from basic where 1=1" 
			+ "  and BD08X = upper(?) and BD02A = upper(?) and BD04X = ?";
	private static final String sqlSetupUserDescription=
			"select decode(BD19A, 'AC', 1, 'PN', 2, 'PP', 3, 'WB', 4, 'CB', 5, 'DB', 6, 'PC', 7, 'XX', 8)" 
			+ " as status," +
			" bgroup||'-'||crefno as token from basic" +
			" where upper(?) = BD08X and BD19A != 'NL' and BGROUP != 'SPF'" +
			" order by status desc ";
	private static final String sqlCheckUserExpried="select * from WE_AUTHENTICATE where EXTUSERID" 
			+ "  IN (select EXTUSERID  from WE_AUTHENTICATE where  WEAU02I > 3 or WEAU01X ='Y')and EXTUSERID=?";
	private static final String sqlChekUserExit="select * from WE_AUTHENTICATE where EXTUSERID =?  ";
	private static final String sqlGetPasswordForUser="select PASSWORD from WE_AUTHENTICATE where EXTUSERID = ?  ";
	private static final String sqlSetPasswordNullForUser="Update WE_AUTHENTICATE set PASSWORD='Null' where EXTUSERID =?  ";
	
	public MigrationDataHandler(){}
	
	/**
	 * @param username
	 * @param password
	 * @param surname
	 * @param payrollnum
	 * @return 0 if user not exists in WE_AUTHENTICATE tabe else reurn number >0 
	 * @throws Exception
	 */
	public int migrateUserDetailsWithCheck(String username,String password,String surname,String payrollnum) throws Exception{
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement pstm=null;
		int result=0;
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm=con.prepareStatement(sqlCheckMigrationData);
			pstm.setString(1, username);
			pstm.setString(2, surname);
			pstm.setString(3, payrollnum);
			rs=pstm.executeQuery();
			
		
			while(rs.next()){
				result=rs.getInt("count");
			}
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			throw e;
			
		}finally{
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				log.error(e);
			}
			
		}
		
	}
	/**
	 * @param username
	 * @return String form cmsUser description base on group and crefno ( BPF-0000002593)
	 * <code>SQL command : decode(BD19A, 'AC', '01', 'PN', '02', 'PP', '03', 'WB', '04', 'CB', '05', 'DB', '06', 'PC', '07', 'XX', '08') as status
    		, bgroup||'-'||crefno as token  from basic where upper(:username) = BD08X
			and BD19A != 'NL' order by status asc</code>
	 * @throws Exception
	 */
	public String getUserDescription(String username)throws Exception {
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement pstm=null;
		String result=StringUtil.EMPTY_STRING;
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm=con.prepareStatement(sqlSetupUserDescription);
			pstm.setString(1, username);
			rs=pstm.executeQuery();
			
			/*
			 * FUTURE PROOF - load all tokens into an array for browsing at their status location...!
			 */
			//example
			//0 1   2  3
			//0 AC  0  PP
			String tokenArray[] = new String[9];  //max combinations is 8, but array starts at 0 so plus one
						
			int status = 0;
			//int maxStatus = 0;
	
			while(rs.next()){		
				status = rs.getInt("STATUS");				
				result=rs.getString("token");
				//check for existing entries
				if (tokenArray[status] == null){					
					tokenArray[status] = result;					
				}else{	
					tokenArray[status] +=","+result;	
				}
			}

			//close what we dont use.
			rs.close();
			pstm.close();
			
			//read the results into a single string
			result = StringUtil.EMPTY_STRING;
			for (int i=0; i < tokenArray.length; i++){
				
				if (tokenArray[i] != null){				
					result += tokenArray[i] + ",";				
				}
			}
			//check for last comma
			if (result != null && result.endsWith(",")){
				result = result.substring(0, (result.length() - 1));
			}
				

			
			//We now search array for max status in the list
			

			return result;  //tokenArray[maxStatus];   <- only want the first one at the moment
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			
			throw e;
			
		}finally{
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e);
			}
			
		}
		
	}
	/**
	 * @param username
	 * 
	 * @return true if user's password has been expried else return false
	 *  (<code>Password has expired if attempts in WE_AUTHENTICATE is greater than 3 or if the locked flag is set in WE_AUTHENTICATE.</code>)
	 * 
	 * @throws Exception
	 */
	public boolean checkUserExpried(String username) throws Exception{
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement pstm=null;
		boolean isExpried;
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm=con.prepareStatement(sqlCheckUserExpried);
			pstm.setString(1, username);
			rs=pstm.executeQuery();
			if (rs.next()){
				isExpried=true;
			}else{
				isExpried=false;
			}
			rs.close();
			pstm.close();
			return isExpried;
			
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			throw e;
			
		}finally{
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e);
			}
			
		}
		
		
		
	}
	/**
	 * @param username
	 * @return true if user with username exists in WE_AUTHENTICATE table else return false 
	 * @throws Exception
	 */
	public boolean checkUserAuthenticate(String username) throws Exception {
		log.info("checkUserAuthenticate():BEGIN");
		DBConnector connector = DBConnector.getInstance();
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement pstm=null;
		boolean isExit;
		try {
			con = connector.getDBConnFactory(Environment.AQUILA);
			if (con==null){
				log.info("MigrationDataHandler.checkUserAuthenticate:  ************* MigrationData getConnection =null ***********");
				isExit = false;
			} else {
				pstm=con.prepareStatement(sqlChekUserExit);
				pstm.setString(1, username);
				rs=pstm.executeQuery();
				if (rs.next()){
					isExit=true;
				}else{
					isExit=false;
				}
				rs.close();
				pstm.close();
			}		
		} catch (Exception e) {
			// TODO: handle exception
			log.error("EXCEPTION: "+e.toString());
			throw e;
		} finally {
			if (con != null) {
				try { 
					connector.close(con);
				} catch (Exception e) {
					// TODO: handle exception
					log.error(e);
				}
			}
		}
		log.info("checkUserAuthenticate():END");
		return isExit;
	}
	/**
	 * @param username
	 * @return return password for user with username
	 * @throws Exception
	 */
	public String getPasswordForuser(String username) throws Exception {
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement pstm=null;
		String password=StringUtil.EMPTY_STRING;
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			pstm=con.prepareStatement(sqlGetPasswordForUser);
			pstm.setString(1, username);
			rs=pstm.executeQuery();
			while (rs.next()){
				password=rs.getString("PASSWORD");
			}
			rs.close();
			pstm.close();
			
			return password;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			throw e;
			
		}finally{
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e);
			}
			
		}
		
	}
	public String encryptPassword(String originalPass) {
		CallableStatement cs = null;
		Connection con = null;
		/*String result=StringUtil.EMPTY_STRING;*/
		try {
            String procedureQuery = "begin ?:= BP1.adm_auth_gen.encrypt(?); end;";
						
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			cs = con.prepareCall(procedureQuery);
			cs.setString(2, originalPass);
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.execute();
			//byte [] temp=cs.getBytes(1);
			String strTemp = cs.getString(1);
			//String strTemp=new String (temp);
			return strTemp;
			
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				log.error(ex);
				ex.printStackTrace();
				
			}
			log.error(e);
			e.printStackTrace();
			return StringUtil.EMPTY_STRING;
			
		} finally {
			if (con != null) {
				try {
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);
				} catch (Exception e) {
					// TODO: handle exception
					log.error(e);
					
				}
			}
		}
		
	}
	public void setNullPassword(String username) throws Exception {
		Connection con = null;
		
		PreparedStatement pstm=null;
		
		
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.AQUILA);
			con.setAutoCommit(false);
			pstm=con.prepareStatement(sqlSetPasswordNullForUser);
			pstm.setString(1, username);
			pstm.executeUpdate();
			con.commit();
			con.setAutoCommit(true);
			pstm.close();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				log.error(ex);
				ex.printStackTrace();
			}
			log.error(e);
			throw e;
			
		}finally{
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e);
				e.printStackTrace();
			}
			
		}
		
	}

}
