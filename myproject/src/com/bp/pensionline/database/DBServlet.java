package com.bp.pensionline.database;

/*
 * (c) copyright BP Pensions 2004
 */

//JDK Imports
import javax.naming.NamingException;
import javax.servlet.*;
import javax.servlet.http.*;

//JDK Imports
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;

/**
 *  A helper class associated with the database connector to open the connection pool on start up
 *  
 *  
 * Set up notes:

   web.xml

	<servlet>
	    <servlet-name>DBServlet</servlet-name>
	    <servlet-class>com.bp.pensionline.database.DBServlet</servlet-class>
	    <load-on-startup>1</load-on-startup>
	</servlet>

 *
 *  @author dcarlyle
 *  @author $Author: $
 *  @version $Revision:  $
 * 
 *  $Id:  Exp $
 */


public class DBServlet extends HttpServlet {
   
    /**
	 * identification, version 
	 */
	private static final long serialVersionUID = 1L;

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);	
    
    //public static DBConnector dbConn = DBConnector.getInstance();
    
    public static final String BB_FAIL = "red";
    public static final String BB_OK = "green";
    
  
    private DBServlet db = null;

    public DBServlet() throws NamingException
    {
    	super();
    	
     //   Context ctx = new InitialContext();
      //  DBServlet home = (DBServlet)PortableRemoteObject.narrow(ctx.lookup("DBServlet"), DBServlet.class);
      //  try {
      //      this.db = home.create();
      //  } catch (Exception e) { }
    }

       
    
    /**
     * Test the current database connection to see if it can connect
     * 
     * @return a String in Big Brother (BigBrother bb Quest) Format
     */
    public String testConnection() throws Exception{
    	
    	Connection conn;
    	
    	String message ="";
    	String output ="";
		/*
		 * Get connection base on given enviroment parameter
		 */
		try{
			conn  = DBConnector.getInstance().getDBConnFactory(Environment.AQUILA);		
			
			if(conn != null){		
				
				try {
					PreparedStatement stmt = conn.prepareStatement("SELECT ? FROM dual");
					stmt.setString(1, "sysdate");
					ResultSet rs = null;
					
					/*
					 * Excute query
					 */
					rs = stmt.executeQuery();
					while(rs.next()){
						/*
						 * Decode parameters
						 */
						message = rs.getString(1);

					}
					stmt.close();	
					conn.close();

				} catch (SQLException sqlEx) {
					output = sqlEx.getMessage();
					LOG.info(sqlEx.getMessage() + sqlEx.getCause());
				}
			}			
			
		}
		catch (Exception e) {
			LOG.info(e.getMessage() +e.getCause());
			throw new Exception(e);
		}
    	

		if(message != null && message != ""){
			output = BB_OK + " ----- working, return from database at: " + message+ ", current activity:  Activity size:"+DBConnector.getInstance().getPoolActivitySize();  
		}else{
			output = BB_FAIL + " ----- " + output;
		}
		return output;
    }
    
    public void init() throws ServletException {    	
       	
        try {	    
        super.init();
        //Console out
        LOG.info("Starting...  PensionLine Oracle Connection Pool starting up through DBServlet");
        
        testConnection();
        
        } catch (Exception ex) {
        	LOG.info("Failed...  PensionLine Oracle Connection Pool FAILED to start up through DBServlet");
        	LOG.error("unable to start connection pool on startup", ex);
            throw new UnavailableException("JDBC Driver not found. Connection to Aquila Database not possible....!");
        }    	
        LOG.info("Started...  PensionLine Oracle Connection Pool STARTED up through DBServlet: Activity size:"+DBConnector.getInstance().getPoolActivitySize());
    }
    
    
    public void init(ServletConfig config) throws ServletException {    	
   	
        try {	    
        	super.init(config);

        //Console out
        LOG.info("Starting...  PensionLine Oracle Connection Pool starting up through DBServlet");
        
        testConnection();
        
        } catch (Exception ex) {
        	LOG.info("Failed...  PensionLine Oracle Connection Pool FAILED to start up through DBServlet");
        	LOG.error("unable to start connection pool on startup", ex);
            throw new UnavailableException("JDBC Driver not found. Connection to Aquila Database not possible....!");
        }    	
        LOG.info("Started...  PensionLine Oracle Connection Pool STARTED up through DBServlet: Activity size:"+DBConnector.getInstance().getPoolActivitySize());
    }
    
    
    
    public void doGet(	HttpServletRequest req,
            			HttpServletResponse response)
    						throws IOException, ServletException
    {
    	try{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>DBServlet</title>");
			out.println("</head>");
			out.println("<body bgcolor=\"white\">");
			
			out.println("<h1> DBServlet Says: </h1>");
			out.println(this.db.testConnection());
			
			
//
	        // get session object 

	        HttpSession session = req.getSession(true); 

	        // print session info 

	        Date created = new Date(session.getCreationTime()); 
	        Date accessed = new Date( 
	               session.getLastAccessedTime()); 
	  
	        out.println(", ID " + session.getId()); 
	        out.println(", Created: " + created); 
	        out.println(", Last Accessed: " + accessed); 

	        // set session info if needed 

	        String dataName = req.getParameter(", dataName"); 
	        if (dataName != null && dataName.length() > 0) { 
	            String dataValue = req.getParameter("dataValue"); 
	            session.putValue(dataName, dataValue); 
	        } 
//			
			
			
			
			
			out.println("</body>");
			out.println("</html>");
        } catch (Exception ex) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>DBServlet</title>");
			out.println("</head>");
			out.println("<body bgcolor=\"white\">");
			
			out.println("<h1> DBServlet Says: </h1>");
			out.println("an error happened");
			
			out.println("</body>");
			out.println("</html>");
        }  
	}

  
    /**
     * remove the pool
     */
    public void destroy(){
    	this.LOG.debug("shutting down the connection pool");
    }

}




