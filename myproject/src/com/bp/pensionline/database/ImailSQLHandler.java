package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.Base64Encoder;

/**
 * @author Duy Thao Nguyen
 * @version 1.0
 * @since 04/05/2007
 * 
 * @author SonNT
 * @version 2.0
 * @since 2007/05/17
 */


public class ImailSQLHandler {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private static String sqlQuery = "INSERT INTO BP_IMAIL " +
			"(BGroup,Refno,NI,CreationDate,CaseWorkId,Subject,Message,FormData, Archived) " +
			"VALUES (?,?,?,getDate(),?,?,?,?,null)";

	/**
	 * Insert data to BP_IMAIL table ( MySQL database)
	 * 
	 * @param map
	 * @throws Exception
	 */
	public static void insertMySqlImail(HashMap map) throws Exception {
		Connection con = null;
		String imailBgroup = String.valueOf(map.get(Environment.IMAIL_BGROUP));
		String imailRefno = (String) map.get(Environment.IMAIL_REFNO);
		String imailNI = String.valueOf(map.get(Environment.IMAIL_NI));
		String imailSubject = String
				.valueOf(map.get(Environment.IMAIL_SUBJECT));
		String imailMessage = String
				.valueOf(map.get(Environment.IMAIL_MESSAGE));
		String imailFormData = String.valueOf(map
				.get(Environment.IMAIL_FORMDATA));
		int caseMaxNo=Integer.parseInt(String
				.valueOf(map.get(Environment.IMAIL_TEMP)));

		try {
			//int caseWorkNo = CaseWorkSQLHandler.getMaxCaseNo() + 1;
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(sqlQuery);
			pstm.setString(1, imailBgroup);
			pstm.setString(2, imailRefno);
			pstm.setString(3, imailNI);
			pstm.setInt(4, caseMaxNo);
			pstm.setString(5, Base64Encoder.encoder(imailSubject.getBytes()));
			pstm.setString(6, Base64Encoder.encoder(imailMessage.getBytes()));
			pstm.setString(7, Base64Encoder.encoder(imailFormData.getBytes()));
			pstm.execute();

			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {
			LOG.error("com.bp.pensionline.database.ImailSQLHandler.insertMySqlImail error :" + e.toString());
			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				LOG.error("com.bp.pensionline.database.ImailSQLHandler.insertMySqlImail can not rollback cause by: " + e.toString());
				throw ex;
			}
			throw e;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					LOG.error("com.bp.pensionline.database.ImailSQLHandler.insertMySqlImail can not close connection cause by :");
					LOG.info(e.getMessage() +e.getCause());
					throw e;
				}

			}

		}

	}
	
	
	/**
	 * @author SonNT
	 * Select Imail to display
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static HashMap selectMySqlImail(String type,int from) throws Exception {
		Connection con = null;
		String queryPaged = null;
		String queryTotal = null;
		ResultSet rs = null;
		HashMap resultMap = new HashMap();

		
		//check type of view is Actived or Archived Imails
		if(type.compareTo("ViewImail") == 0){
			//get Imail for current page
			queryPaged = "select * from BP_IMAIL where archived is null ORDER BY CreationDate DESC Limit ? , 20";
			//get total of Imails
			queryTotal = "select * from BP_IMAIL where archived is null ORDER BY CreationDate DESC";
		}
		else{
			queryPaged = "select * from BP_IMAIL where archived is not null ORDER BY CreationDate DESC Limit ? , 20"; 
			queryTotal = "select * from BP_IMAIL where archived is not null ORDER BY CreationDate DESC";
		}
		

		try {
			
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			
			PreparedStatement pstm = con.prepareStatement(queryPaged);	
			pstm.setInt(1, from);
			
			rs = pstm.executeQuery();
			
			while(rs.next()){
				HashMap map = new HashMap();//Once get new row create a new map					

				map.put("member",rs.getString(1) + "-" + rs.getString(2));

				map.put("submitted",rs.getString(4));

				map.put("caseworkId",rs.getString(5));

				map.put("subject",com.bp.pensionline.util.Base64Decoder.decode(rs.getString(6)));

				map.put("form",com.bp.pensionline.util.Base64Decoder.decode(rs.getString(8)));

				map.put("archived",rs.getString(9));			

				resultMap.put(rs.getRow(), map);//put child map into result map

			}
			
			pstm = con.prepareStatement(queryTotal);
			rs = pstm.executeQuery();
			int total = 0;
			while(rs.next()){
				total++;
			}
			resultMap.put("total", total);
			
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {

			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					throw e;
				}

			}

		}
		
		return resultMap;

	}
	
	/**
	 * @author SonNT
	 * Update actived imails into archived
	 * @param caseworkId
	 * @return
	 * @throws Exception
	 */
	public static boolean updateMySqlImail(String caseworkId) throws Exception {
		Connection con = null;
		String query = "update BP_IMAIL set ARCHIVED = getDate() where ARCHIVED is null and CASEWORKID = ?";			
		boolean isUpdated = false; 
		int i = 0;
		
		try {
			
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			
			PreparedStatement pstm = con.prepareStatement(query);
			pstm.setString(1, caseworkId);
			i = pstm.executeUpdate();
			
			if(i > 0){
				isUpdated = true;
			}

			con.commit();
			con.setAutoCommit(true);
		} catch (Exception e) {

			try {
				con.rollback();
			} catch (Exception ex) {
				// TODO: handle exception
				throw ex;
			}
			throw e;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					throw e;
				}

			}

		}
		
		return isUpdated;

	}
	
	/**
	 * @author Huy Tran
	 * @return max number of IMail.
	 */
	public int getMaxIMailNo()
	{
		Connection con = null;
		try
		{
			int maxIMailNo = 0;
			DBConnector connector = DBConnector.getInstance();
			//con = connector.getDBConnFactory(Environment.SQL);
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("SELECT MAX(CaseWorkId) AS IMailNo FROM BP_IMAIL");
			while (rs.next())
			{
				maxIMailNo = rs.getInt("IMailNo");
			}

			return maxIMailNo;
		}
		catch (Exception e)
		{
			LOG.error("Error in getting max IMail number: " + e.toString());
			return 0;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					DBConnector connector = DBConnector.getInstance();
					connector.close(con);// con.close();
				}
				catch (Exception ex)
				{

				}
			}
		}
	}
}
