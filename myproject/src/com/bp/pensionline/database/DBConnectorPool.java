package com.bp.pensionline.database;



/*
 * (c) copyright BP Pensions 2004
 */

//JDK Imports
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;
import org.opencms.db.CmsDbPool;
import org.opencms.db.CmsSqlManager;
import org.opencms.main.OpenCms;

import com.bp.pensionline.constants.Environment;
/**
 *  A helper class associated with the database,
 *  
 *  This is the factory to the DB connetions
 *  
 *
 *  @author dcarlyle
 *  @author $Author: cvsuser $
 *  @version $Revision: 1.3 $
 * 
 *  $Id: DBConnector.java,v 1.3 2007/05/23 10:52:53 cvsuser Exp $
 */

/*
 * ISSUES: org.jboss.resource.adapter.jdbc.WrapperDataSource, extends data source,
 *  however we need: javax.sql.ConnectionPoolDataSource, so we get a class cast issue, even if we update the 
 *  jboss-web.xml
 *  
 *  So we ned to create our own datasources, by reading in from OpenCMS
 * 
 */
public class DBConnectorPool {

  	
  
  Logger log = Logger.getLogger(DBConnectorPool.class.getName());
  static ConnCacheBean connCacheBean ; 
   
   /**
    * default constructor
    */
   public DBConnectorPool() {
      super();
     // log.debug("DBConnector constructor");
   }


   public Connection getDBConnFactory(int environment) throws SQLException{  

	   Connection envDB = null;
	   
	   if (environment == Environment.AQUILA) {
		   
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }
	   else if (environment == Environment.LETTERS) {
		   
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }
	   else if (environment == Environment.CMS) {
		
		   envDB = getDBConn(Environment.ORACLE_DB); 
		   
	   }	
	   else if (environment == Environment.SQL) {
		   /*
		    * Try to get Sql connection from CmsSqlmanager
		    */
		   
		   CmsSqlManager sqlManager=OpenCms.getSqlManager();		   
		   envDB = sqlManager.getConnection(CmsDbPool.getDefaultDbPoolName());
	   }
	   else
	   {
		   envDB = getDBConn(Environment.ORACLE_DB); 
	   }	   
 
	   return envDB;
   }
   
   
   /**
    * default database access
    * @return
    * @throws SQLException
    */  
   private Connection getDBConn(String jndiEnv) throws SQLException{
               	  
		if (connCacheBean == null){

			try {
				connCacheBean = new ConnCacheBean();
			} catch (Exception e) {
				log.error("[1] Exception with CacheBean: " + e);
			}
		}	
	   /* Get OracleDataSource from the ConnCacheBean */
	   OracleDataSource ods = connCacheBean.getDataSource();
		
		 	  
		
        Connection conn = null;
        
        try{
          conn = ods.getConnection();
           
          
        }catch(SQLException sqlEx){
        	log.error("[1] sqlexception: " + sqlEx);
        	
      	}catch(Exception ex){
        	log.error("[2] Another exception", ex);
        }
        return conn;
   }
		
    
   public void close (Connection conn){
	   if (conn != null) {
			try {
				conn.close();
				log.info("Connection closing"); 
			} catch (Exception eSql) {
				log.error("tried and failed to close connection", eSql);
			}
	   }
   }

   

}