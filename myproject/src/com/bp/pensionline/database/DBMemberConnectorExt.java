package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Set;
//import java.util.StringTokenizer;

import com.bp.pensionline.aataxmodeller.modeller.MemberLoader;
import com.bp.pensionline.aataxmodeller.modeller.TaxModeller;
import com.bp.pensionline.aataxmodeller.util.DateUtil;
import com.bp.pensionline.aataxmodeller.dto.MemberDetail;
import com.bp.pensionline.aataxmodeller.dto.TaxConfiguredValues;
import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.dao.MemberDao;
import com.bp.pensionline.dao.database.AvcDetailDao;
import com.bp.pensionline.dao.database.ContributionHistoryDao;
import com.bp.pensionline.factory.CalcProducerFactory;
import com.bp.pensionline.handler.CompanySchemeRawHandler;
import com.bp.pensionline.util.SystemAccount;
import com.bp.pensionline.test.*;

import java.io.FileNotFoundException;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.CmsUser;
import org.opencms.main.CmsLog;


/**
 * @author Huy Tran
 * This is to hook to the database, get data for all field given by fieldMap
 * from MemberMapper ALL folder and its sub-folders.
 * It updates valueMap after getting actual data from database
 * for a given pair of refno and bgroup
 * 
 * 
 * getConnection() queryBuilder(String tableName) getValue() getMapIndicator()
 * getAllTable()
 * 
 */

public class DBMemberConnectorExt extends Thread{

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
//	private Map<String, String> tableMap = new HashMap<String, String>();
//
//	private Map<String, String> fieldMap = new HashMap<String, String>();

	private Map<String, String> valueMap = new HashMap<String, String>();

	private MemberMapper mMapper = new MemberMapper();

	
	private MemberDao memberDao = null;
	
	private MemberDetail memberDetail = null;
	
	private String bgroup = null;
	
	private String refno = null;

	/**
	 * 
	 * 
	 * 
	 * @param refno
	 * @param bgroup
	 */

	public DBMemberConnectorExt() 
	{		

	};
	
	/**
	 * @param memberDao the memberDao to set
	 */
	public void setMemberDao(MemberDao memberDao)
	{
		this.memberDao = memberDao;
		
		if (memberDao != null)
		{
			bgroup = this.memberDao.get(Environment.MEMBER_BGROUP);
			refno = this.memberDao.get(Environment.MEMBER_REFNO);			
		}		
	}	
	

	@Override
	public void run()
	{				
		LOG.info("Getting extension data for member start! " + this.memberDao);
		if (this.memberDao != null)
		{
			updateMemberData();
			
			LOG.info("Update member data ok!");
			
			this.memberDao.addValues(valueMap);
			
			ContributionHistoryDao chd=new ContributionHistoryDao();
			
			//String key="ContributionHistory";
			ArrayList<String> contributionList=chd.getContributionHistory(refno, bgroup);
			int count=contributionList.size()/3;
			String countAsString = Integer.toString(count);
			
			this.memberDao.set(Environment.MEMBER_CONTRIBUTION_COUNT, countAsString);
			
			//getValueMap().put("ContributionHistoryCount", countAsString);
			
			
			//System.out.println("This was taken by MR TU:" + countAsString);
			this.memberDao.set(Environment.MEMBER_CONTRIBUTION_HISTORY, this.getContributionHistoryXML(Environment.MEMBER_CONTRIBUTION_HISTORY, contributionList));
			
//			System.out.println("Put:" + Environment.MEMBER_CONTRIBUTION_HISTORY + "-->\n"+ this.getContributionHistoryXML(Environment.MEMBER_CONTRIBUTION_HISTORY, contributionList) );
			
			
			AvcDetailDao ada=new AvcDetailDao();

			Map<String, String> avcDetails=ada.getAvcDetail(refno, bgroup);
			
			// Override LTA to use configured value if it is set
			TaxModeller taxModeller = new TaxModeller();
			Calendar calendar = Calendar.getInstance();
			TaxConfiguredValues values = taxModeller.getTaxConfiguredValues(DateUtil.getTaxYearAsString(calendar.getTime()));						
            double lta = Double.parseDouble(values.getLta());
            			            		            
            // if configurations is true then use configured value
            if (values.isConfigurationUsed())
            {
            	this.memberDao.set(MemberDao.Lta, "" + lta);
            }            
			
			//shallow copy
			//getValueMap().putAll(AvcDetails);
	        //deep copy
			LOG.info("Adding extension data values to MemberDao object!");
			this.memberDao.addValues(avcDetails);		
			
			LOG.info("Extension data for MemberDao achieved!");
			//debugMemberDao(this.memberDao);
			
			LOG.info("Load member details used for calculation...");
			MemberLoader memberLoader = new MemberLoader(bgroup, refno);
			memberDetail = memberLoader.loadMember();
			
			storeInfoOnSession();
			
			// Start member calculations
			CalcProducerFactory producerFactory = new CalcProducerFactory();
			producerFactory.setMemberDao(memberDao);
			producerFactory.start();			
		}
	}



	/**
	 * get Connection
	 * 
	 * @return conn
	 * 
	 */
	private Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {

			DBConnector dbConn = DBConnector.getInstance();//new DBConnector();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);

		} catch (Exception ex) {
			LOG.error("DBMemberConnector", ex);
		}
		return conn;
	}


	/**
	 * This method is doing the following - getConnection - Hook up to database
	 * and take all data that need, and - put data in the valueMap
	 * 
	 * @return valueMap
	 */

	private void updateMemberData()
	{
		if (this.memberDao != null)
		{					
			// Run all queries at ALL folder
			assignValueMap(Environment.MEMBERMAPPERFILE_DIR + "ALL/");


			// LOG.info("MAP: "+valueMap);
			String membershipStatus = this.memberDao.get("MembershipStatusRaw");			
			CompanySchemeRawHandler csrh = CompanySchemeRawHandler.getInstance();
			String companyGroup = csrh.getCompanyGroup(bgroup, this.memberDao.get("SchemeRaw"));

			// Run all SQL under companyGroup
			assignValueMap(Environment.MEMBERMAPPERFILE_DIR + companyGroup+ "/");

			// Run all SQL under MembershipStatus folder
			// There are no member ship status folders now
			// assignValueMap(Environment.MEMBERMAPPERFILE_DIR+MembershipStatus+"/");

			// Then at last, run all SQL under CompanyGroup/MembershipStatus
			// folder.
			assignValueMap(Environment.MEMBERMAPPERFILE_DIR + companyGroup + "/" + membershipStatus + "/");
					
		}

	}

	/**
	 * this method return mapIndicator, this is used when marshall
	 * DatabaseMemberDao object to XML file
	 * 
	 */

	public Map<String, String> getMapIndicator() {

		return mMapper.getMapIndicator();
	}

	
	/**
	 * @author VinhDT 15 Jun 07
	 * @param folderName	
	 *  
	 *  This method is to run SQLqueries in all files under a specified folderName
	 *  and assign the value that it get to valueMap
	 * */
	
	private void assignValueMap(String folderName){
		
		CmsObject cmsObj = SystemAccount.getAdminCmsObject();
		
		// Modified by Huy
		if (!cmsObj.existsResource(folderName))
		{
			return;
		}
		
		List resources = null;
		try {
			
			//get all resources in dictionary folder (includes files and folders)
			resources = cmsObj.getResourcesInFolder(folderName, CmsResourceFilter.ALL);
		}
		catch(Exception ex) {

			LOG.error("Error while getting resources in folder: " + folderName + ". " + ex.toString());
			
		}		
			
		if (resources != null && resources.size() > 0)
		{
			
			//get iterator
			Iterator iter = resources.iterator();
			
			//loop through iterator
			for ( ; iter.hasNext(); ) {

				CmsResource resource = (CmsResource) iter.next();
				

				//if resource is a file then  add it's name to the m_cache
				if(resource.isFile()){
					
					Connection conn = getConnection();
					XmlReader xReader=new XmlReader();
												
					try {
						
						if (conn != null) 
						{

							ResultSet rs = null;
							ResultSetMetaData rsmd = null;
							
							try 
							{    			
						    	
								LOG.info("Extenstion data: " + folderName+resource.getName());
								String SQLQuery=new String(xReader.readFile(folderName+resource.getName()));
								
								//PreparedStatement pstm = conn.prepareStatement(SQLQuery);	
						    	
						    	/**Get the array contain the location in which preparedStatement 
						    	 * must replace with the value
						    	 * if result[i]=bgroup, must set query at location i, value of Bgroup
						    	 * if result[i]=refno, must set query at location i, value of Refno
						    	 */
								
								SQLQuery = SQLQuery.trim();
								// get the last char of the sql query and compare with ";"
								char lastChar = SQLQuery.charAt(SQLQuery.length()-1);
								if (lastChar == ';'){
									SQLQuery = SQLQuery.substring(0, SQLQuery.length()-1);
								}
								//SQLQuery.substring(0, endIndex)''
								
								SQLQuery = SQLQuery.replaceAll(":bgroup", "'"+bgroup+"'");
								SQLQuery = SQLQuery.replaceAll(":refno", "'"+refno+"'");
								SQLQuery = SQLQuery.replaceAll(":BGROUP", "'"+bgroup+"'");
								SQLQuery = SQLQuery.replaceAll(":REFNO", "'"+refno+"'");
								SQLQuery = SQLQuery.replaceAll(":Bgroup", "'"+bgroup+"'");
								SQLQuery = SQLQuery.replaceAll(":Refno", "'"+refno+"'");
								SQLQuery = SQLQuery.replaceAll(":BGroup", "'"+bgroup+"'");
								SQLQuery = SQLQuery.replaceAll(":RefNo", "'"+refno+"'");

								rs = conn.createStatement().executeQuery(SQLQuery);
								/*
								PreparedStatement pstm = conn.prepareStatement(SQLQuery);
						    	pstm.execute();				
						    	rs = pstm.executeQuery();
						    	*/
						    	rsmd=rs.getMetaData();																		
						    	//Get total number of column
						    	int colsCount=rsmd.getColumnCount();								
						    	
						    	//Assign the value to valueMap
						    	while(rs.next()){
						    		for(int j=1;j<=colsCount;j++)
						    			valueMap.put(rsmd.getColumnName(j), rs.getString(j));
						    	}
						    	
						    	LOG.info("Extenstion data got!");
							} 							
							catch (FileNotFoundException e){
						    			// TODO Auto-generated catch block
								LOG.error("[1] DBMemberConnector, could not find standard file", e);
						    			
							}
								
							rs.close();
							
						}
						
					}
					catch (Exception ex) {
						LOG.error("Error while executing query in the file: " + ex);
					}
					finally					
					{
						if (conn != null)
						{
							try{
								conn.close();
							}catch(SQLException sqle){}
						}
					}
				}				
	
			}
			
		}			
	}
	
	/**
	 * Marshall this object into XML
	 * 
	 * @return
	 */
	private String getContributionHistoryXML(String key,
			ArrayList<String> contributionList)
	{

		StringBuffer ContributionHistoryXML = new StringBuffer();

		String[] contributionGroup = { "Option", "From", "To" };
		int iterator = 0;
		while (iterator < contributionList.size() / 3)
		{
			ContributionHistoryXML.append("<Contribution>");
			ContributionHistoryXML.append("\n");
			int i = 0;
			while (i < contributionGroup.length)
			{
				ContributionHistoryXML.append("<" + contributionGroup[i] + ">");
				if (contributionList.get(iterator * 3 + i) != null)
					ContributionHistoryXML.append(contributionList.get(iterator * 3 + i));
				else ContributionHistoryXML.append(" ");

				ContributionHistoryXML.append("</" + contributionGroup[i] + ">");
				i++;
			}
			ContributionHistoryXML.append("</Contribution>\n");
			iterator++;
		}

		return ContributionHistoryXML.toString();
	}
		
	
	public void storeInfoOnSession()
	{
		try
		{
			String sessionId = memberDao.get("SessionId");
			CmsUser cmsuser = SystemAccount.getCurrentUser(sessionId);

			synchronized (this.memberDao)
			{
				cmsuser.setAdditionalInfo(Environment.MEMBER_KEY, this.memberDao);
				cmsuser.setAdditionalInfo(Environment.MEMBER_DETAIL_KEY, this.memberDetail);
			}
			
			

			LOG.info("Extenstion data for member has been stored!");
		}
		catch (Exception ex)
		{
			LOG.error("Store extenstion data for member failed! " + ex.toString());
		}
	}	
	
	public void debugMemberDao (MemberDao member)
	{
		if (member != null)
		{
			LOG.info("****************** DEBUG MEMBER DAO *****************");
			Map<String, String> map = member.getValueMap();
			StringBuffer xmlString = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			xmlString.append("<MemberData>\n\t");
			
			Set<String> keys = map.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext())
			{
				String key = keyIterator.next();
				String value = map.get(key);
				xmlString.append("<" + key + ">" + value + "</" + key + ">\n\t");
				LOG.info("DEBUG MEMBER DAO: " + key + " = " + value);
				
			}
			xmlString.append("</MemberData>\n");
//			exportToFile("D:/" + (member.getBgroup()==null ? "BPF" : member.getBgroup()) + 
//					"_" + member.getRefno() + ".xml", xmlString.toString());
			LOG.info("****************** END OF DEBUG MEMBER DAO. EXPORT SUCCEED *****************");
		}
	}	
	
}