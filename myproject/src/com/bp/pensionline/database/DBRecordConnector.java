package com.bp.pensionline.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.exception.MemberNotFoundException;
import com.bp.pensionline.util.Variants;

//import javax.servlet.jsp.tagext.*;

public class DBRecordConnector {
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	
	private String refno = null;

	private String bgroup = null;

	//private Map tableMap=new HashMap();
	private Map<String, String> fieldMap = new HashMap<String, String>();

	private Map<String, String> valueMap = new HashMap<String, String>();

	private RecordMapper rMapper = new RecordMapper();

	public DBRecordConnector(String bgroup, String refno) {

		this.bgroup = bgroup;
		this.refno = refno;

		//	tableMap=xReader.getAttributeTable();
		fieldMap = rMapper.getMapField();
		//valueMap = rMapper.getMapValue();

	}


	static Connection getConnection() {
		/*
		 * Create new conn
		 */
		Connection conn = null;
		try {

			DBConnector dbConn = DBConnector.getInstance();
			conn = dbConn.getDBConnFactory(Environment.AQUILA);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return conn;
	}

	/* public String queryBuilder(String tableName){
	 String 	allFieldSelect=xReader.allFieldNeeded(tableName);
	 SQLConstraint sqlHelper=new SQLConstraint();
	 String whereClause=sqlHelper.checkConstraint(tableName);
	 String sqlQuery="SELECT "+allFieldSelect+" FROM "+tableName +" "+ whereClause;
	 return sqlQuery;
	 }*/

	public Map<String, String> getValue() throws MemberNotFoundException {
		Connection conn = getConnection();
		String tableName = "BASIC";
		//LOG.info("DBRecordConnector.getValue: " + conn);
		
		try {
			if (conn != null) {
				
				Statement stmt = conn.createStatement();
				String allFieldSelect = rMapper.allFieldNeeded(tableName);
				//LOG.info("allFieldSelect: " + allFieldSelect);
				//obj to store return value
				ResultSet rs = null;

				ResultSetMetaData rsmd = null;

				String sqlQuery = "SELECT " + allFieldSelect + " " + "FROM "
						+ tableName + " " + "WHERE REFNO='" + refno
						+ "' AND BGROUP='" + bgroup + "'";
				LOG.info(this.getClass().toString() + ": Query " + sqlQuery);
				
				rs = stmt.executeQuery(sqlQuery);
				
				if(! rs.next())
				{					
					try
					{
						conn.close();						
					}
					catch(SQLException sqle){}
					
					LOG.error("Can not found member with Bgroup=" + bgroup + " Refno=" + refno);
					throw new MemberNotFoundException("Can not found member with Bgroup=" + bgroup + " Refno=" + refno);
				}
				
				LOG.info("Going to get POS details for member: " + bgroup + " - " + refno);
				rsmd = rs.getMetaData();
				//LOG.info(this.getClass().toString() + ": rsmd cols: " + rsmd.getColumnCount());				
				do
				{
					for (int i = 1; i <= rsmd.getColumnCount(); i++) 
					{
						mapGenerate(rs.getObject(i), rsmd.getColumnName(i));
					}
				}
				while (rs.next()) ;
				
				rs.close();
				
				conn.close();
				//LOG.info("DBRecordConnector.getValue: close connection: " + valueMap.size());
			}

		}
		catch (SQLException sqlException) {
			
			try{
				conn.close();						
			}catch(SQLException sqle){}
			
			sqlException.getStackTrace();
		}
		
		return valueMap;

		
	}

	public void mapGenerate(Object rsObject, String rsColumn) {
		
		Iterator keys = fieldMap.keySet().iterator();
		Variants converter=new Variants();
		
		while (keys.hasNext()) {
			
			Object fieldKey = keys.next();
						
			Object fieldValue = fieldMap.get(fieldKey);
			
			if (fieldValue.equals(rsColumn)) {
				
				if (rsObject != null) {
					String output=converter.convert(fieldKey.toString(), rsObject.toString());
					valueMap.put(fieldKey.toString(), output);
					LOG.info("PUT to DBRecordDao: " + fieldKey.toString() + " - " + rsObject.toString());
				}
				
				//break;

			}
		}
		
		/*
		Variants converter = new Variants();
		
		Iterator keys = fieldMap.keySet().iterator();

		while (keys.hasNext()) {
			
			Object fieldKey = keys.next();
			
			String output;
			String temp = fieldKey.toString();

			if (temp.equalsIgnoreCase(rsColumn)) {
				if (rsObject != null) {
					output = converter.convert(fieldKey.toString(), rsObject.toString());
					
					//modified by CuongDV
					valueMap.put(fieldKey.toString(), output.toString());
					//valueMap.put(fieldKey.toString().toLowerCase(), output.toString());
					
				}
			}

		}
		*/
	}// end of mapGenerate       
}
