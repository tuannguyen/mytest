package com.bp.pensionline.wiki;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;


/**
 * @author CuongDV
 *
 */
public class CaveatManager {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	//this map contains files path  , the key is {PHRASE_SCHEME} and the value is relevant fully file name
	private static Map m_cache = new HashMap();
	
	private static final long m_deadage = 60000; //~1 minutes
	
	//contains time in millis for last update
	private static long m_lastupdate = -1;
		
	public CaveatManager() {

	}


	/**
	 * return the file name for the phrase and scheme
	 * @param phrase
	 * @param scheme
	 * @return
	 */
	public String getFile(String phrase, String scheme){
		
		StringBuffer key = new StringBuffer();
		
		key.append(phrase).append("_").append(scheme.trim());
		
		String cavePath = null;
		
		if(isDirtyCache() || !m_cache.containsKey(key.toString().toLowerCase())){
			
			//get files and update to cache			
			this.updateCache();
			
		}
		
		cavePath = (String)m_cache.get(key.toString().toLowerCase());
		
		return cavePath;
		
	}
	
	/**
	 *  
	 * @return true if m_cache is newly loaded else return false  
	 */
	private boolean isDirtyCache(){

		boolean m_isdirty = false;
		
		long m_currenttime = System.currentTimeMillis();
		
		if(m_lastupdate > 0){
			
			long m_lifetime = m_currenttime - m_lastupdate;
			
			if(m_lifetime >= m_deadage){

				m_isdirty = true;
				
			}			
			
		}else{
			
			m_lastupdate = m_currenttime;
			
			m_isdirty = true;
			
		}
		
		return m_isdirty;
	}
	
	/**
	 * this method is called whenever the m_cache is dirty (over time out)
	 */
	private void updateCache() {

		try {
			
			String folderName = Environment.CAVEAT_DIR;

			//get cms object
			CmsObject cmsObj = SystemAccount.getAdminCmsObject();
			
			//reset last update time
			m_lastupdate = System.currentTimeMillis();
			
			//clear all data in cache
			m_cache.clear();
			
			//start get data
			getFilesInFolder(cmsObj, folderName, StringUtil.EMPTY_STRING);
			
			
		}catch(Exception ex) {

			LOG.info(ex.getMessage() + ex.getCause());
		}

	}
	
	
	/**
	 * traverse through the dictionary directory and get all file name to put into the m_cache 
	 * @param cmsObj
	 * @param folderName
	 * @param scheme
	 */
	private void getFilesInFolder(CmsObject cmsObj, String folderName, String scheme){
		
		try {
			
			//get all resources in the folder caveat
			List resources = cmsObj.getResourcesInFolder(folderName, CmsResourceFilter.ALL);
			
			//get iterator object
			Iterator iter = resources.iterator();
			
			//loop through the iterator 
			for ( ; iter.hasNext(); ) {

				CmsResource resource = (CmsResource) iter.next();

				//if resource is file
				if(resource.isFile()){
					
					CmsProperty title = cmsObj.readPropertyObject(resource, "Title", false);
					
					StringBuffer key = new StringBuffer();
					
					//key is combined from phrase and sheme 
					key.append(title.getValue())
						.append("_")
						.append(scheme);
					
					//value is the file name
					StringBuffer value = new StringBuffer();
					
					//scheme length is greater than zero mean that the phrase is belong to a scheme 
					if( scheme.length() > 0){
						
						value.append("/").append(scheme).append("/").append(resource.getName()) ;
						
					}
					//scheme length is equal zero mean that the phrase is directly children of caveat directory
					else{
						
						value.append("/").append(resource.getName());
						
					}
					
					m_cache.put(key.toString().toLowerCase(), value.toString());
					
				}else if(resource.isFolder()){
					
					getFilesInFolder(cmsObj, resource.getRootPath(), resource.getName());
					
				}
			}
			
		}catch(Exception ex) {

			LOG.info(ex.getMessage() + ex.getCause());
			
		}
	}
}