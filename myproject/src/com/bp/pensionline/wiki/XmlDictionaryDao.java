package com.bp.pensionline.wiki;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;

/**
 * @author SonNT
 */

public class XmlDictionaryDao {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Hashtable hash = null;
	
	private String phrase = null;
	private String scheme = null;
	private String content = null;
	private String update = null;

	/**
	 * Constructor
	 * @param phrase
	 * @param scheme
	 */
	public XmlDictionaryDao(String phrase, String scheme, String updateField) {

		this.phrase = phrase;
		this.scheme = scheme;
		this.update = updateField;
		
		hash = new Hashtable();
		XmlReader reader = new XmlReader();
		byte[] arr = null;
		
		try {

			DictionaryManager dicManager = new DictionaryManager();
			
			String dicPath = dicManager.getFile(phrase, scheme);
			
			arr = reader.readFile( "dictionary" + dicPath);
			
			loadXml(arr);
			
		}
		catch(FileNotFoundException fnfe){
			System.out.println(fnfe.getMessage());
		}
		catch (Exception e) {
			LOG.error("Exception while retrieving dictionary: "+e.toString());
			e.printStackTrace();
		}
		
	}

	/**
	 * @param arr
	 */
	private void loadXml(byte[] arr) {
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = null;

			ByteArrayInputStream is = new ByteArrayInputStream(arr);

			document = builder.parse(is);

			

			NodeList contList = document.getElementsByTagName("content");
			Node cont = contList.item(0);
			Node contCont = cont.getFirstChild();

			

			if (contCont.getNodeType() == Node.CDATA_SECTION_NODE) {

				String value = contCont.getTextContent();

				//hash.put(name, value);
				content = value;
			}
			is.close();

		} catch (Exception ex) {
			LOG.info(ex.getMessage() + ex.getCause());
		}
	}

	/**
	 * @param key
	 * @return
	 */
	public String getMemberData(String key) {

		Object obj = hash.get(key);

		return obj == null ? new String("") : obj.toString();
	}

	/**
	 * @return
	 */
	public String getXmlData() {

		if ( phrase != null && content != null ) {

			StringBuffer buffer = new StringBuffer();

			buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
					">\n");
			buffer.append("     <").append("Dictionary").append(">\n");
			buffer.append("         <").append("Phrase").append(">").append(
					this.phrase).append("</").append("Phrase").append(">\n");
			buffer.append("         <").append("Scheme").append(">").append(
					this.scheme).append("</").append("Scheme").append(">\n");
			buffer.append("         <").append("UpdateField").append(">").append(
					this.update).append("</").append("UpdateField").append(">\n");
			buffer.append("         <").append("Title").append(">").append(
					this.phrase).append("</").append("Title").append(">\n");
			buffer.append("         <").append("Definition").append(">")
					.append("<![CDATA[" + this.content + "]]>").append("</").append("Definition")
					.append(">\n");
			buffer.append("     </").append("Dictionary").append(">\n");
			buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE)
					.append(">\n");

			return buffer.toString();

		} else {

			return getXmlDataError(this.phrase, this.scheme, "Not Found");
		}
	}

	/**
	 * @param phrase
	 * @param scheme
	 * @param mess
	 * @return
	 */
	private String getXmlDataError(String phrase, String scheme, String mess) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");
		buffer.append("     <").append("Dictionary").append(">\n");
		buffer.append("         <").append("Phrase").append(">").append(phrase)
				.append("</").append("Phrase").append(">\n");
		buffer.append("         <").append("Scheme").append(">").append(scheme)
				.append("</").append("Scheme").append(">\n");
		buffer.append("         <").append("UpdateField").append(">").append(
				this.update).append("</").append("UpdateField").append(">\n");
		buffer.append("         <").append(Constant.ERROR).append(">\n");
		buffer.append("             <").append(Constant.MESSAGE).append(
				">[[" + mess + ":" + phrase + "]]</").append(Constant.MESSAGE)
				.append(">\n");
		buffer.append("         </").append(Constant.ERROR).append(">\n");
		buffer.append("     </").append("Dictionary").append(">\n");
		buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(
				">\n");

		return buffer.toString();
	}
}