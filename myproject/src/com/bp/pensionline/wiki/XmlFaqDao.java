package com.bp.pensionline.wiki;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;

/**
 * @author SonNT
 */

public class XmlFaqDao {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private Hashtable hash = null;
	private String update = null;
		
	/**
	 * 
	 */
	public XmlFaqDao(String phrase, String scheme, String updateField){
    	hash = new Hashtable();
    	update = updateField;
    	
		XmlReader reader = new XmlReader();
		
		byte[] arr = null;
		
		try {
			//Read file from VFS
			FaqManager faqMan = new FaqManager();			
			String faqPath = faqMan.getFile(phrase, scheme);			
			arr = reader.readFile( "faq"+faqPath);
			//call load function
			loadXml(arr);
			
		}
		catch(FileNotFoundException fnfe){
			System.out.println(fnfe.getMessage());
		}
		catch (Exception e) {
			LOG.info(e.getMessage() + e.getCause());
		}
    }
	
	/**
	 * @param fileName
	 */
	public void loadXml(byte[] arr){    	
        try{
	    	
	    	
        	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();             
            
            ByteArrayInputStream is =  new ByteArrayInputStream(arr);            
            Document document = builder.parse(is);
            
            this.putElements(document, "Anchor");
            this.putElements(document, "Title");
            this.putElements(document, "Question");
            
            is.close();
            
        }catch(Exception ex){
        	LOG.info(ex.getMessage() + ex.getCause());
        }        	
    }
	
	/**
	 * @param document
	 * @param tagName
	 */
	private void putElements(Document document, String tagName){
		try{
			NodeList rootList = document.getElementsByTagName(tagName);            
	         Node root = rootList.item(0);          
	         Node child = root.getFirstChild();
	         
	         if(child.getNodeType() == Node.CDATA_SECTION_NODE){        		
	         	String name = root.getNodeName();                		
	     		String value = child.getTextContent();
	     		hash.put(name, value);
	         }			 
		}
		catch (Exception e) {
			LOG.info(e.getMessage() + e.getCause());
		}		
	}
	
	/**
	 * @param key
	 * @return
	 */
	public String getMemberData(String key){  
		
    	Object obj = hash.get(key);
    	
    	return obj == null ? new String("") : obj.toString();    	
    }
	
    /**
     * @param phrase
     * @param scheme
     * @return
     */
    public String getXmlData(String phrase, String scheme){
    	

    	
    	String anchor = getMemberData("Anchor");
    	String title = getMemberData("Title");
    	String question = getMemberData("Question");    	
    	
    	if(question.length() > 0){    	
    		StringBuffer buffer = new StringBuffer();

            buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
            buffer.append("     <").append("Faq").append(">\n");
            buffer.append("         <").append("Phrase").append(">").append(phrase).append("</").append("Phrase").append(">\n");
            buffer.append("         <").append("Scheme").append(">").append(scheme).append("</").append("Scheme").append(">\n");
            buffer.append("         <").append("UpdateField").append(">").append(update).append("</").append("UpdateField").append(">\n");
            buffer.append("         <").append("Anchor").append(">").append(anchor).append("</").append("Anchor").append(">\n");
            buffer.append("         <").append("Title").append(">").append(title).append("</").append("Title").append(">\n");
            buffer.append("         <").append("Question").append(">").append(question).append("</").append("Question").append(">\n");
            buffer.append("     </").append("Faq").append(">\n");
            buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
            
			return buffer.toString();
			
    	}else{
    		
    		return getXmlDataError(phrase, scheme,"Not Found");    		
    	}    	
    }
    /**
     * @param phrase
     * @param scheme
     * @param mess
     * @return
     */
    private String getXmlDataError(String phrase, String scheme,String mess){

        StringBuffer buffer = new StringBuffer();
        
        buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
        buffer.append("     <").append("Faq").append(">\n");
        buffer.append("         <").append("Phrase").append(">").append(phrase).append("</").append("Phrase").append(">\n");
        buffer.append("         <").append("Scheme").append(">").append(scheme).append("</").append("Scheme").append(">\n");
        buffer.append("         <").append("UpdateField").append(">").append(update).append("</").append("UpdateField").append(">\n");
        buffer.append("         <").append(Constant.ERROR).append(">\n");
        buffer.append("             <").append(Constant.MESSAGE).append(">[["+ mess + phrase +"]]</").append(Constant.MESSAGE).append(">\n");
        buffer.append("         </").append(Constant.ERROR).append(">\n"); 
        buffer.append("     </").append("Faq").append(">\n");
        buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
        
        return buffer.toString();    	
    }
}