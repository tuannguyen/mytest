package com.bp.pensionline.wiki;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.main.CmsLog;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.util.StringUtil;
import com.bp.pensionline.util.SystemAccount;


/**
 * @author CuongDV
 *
 */
public class DictionaryManager {
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);

	//this Map contains all file name in the dictionary directory
	private static Map m_cache = new HashMap();
	
	//max age of the m_cache, in millis
	private static final long m_deadage = 60000; //~1 minutes
	
	//
	private static long m_lastupdate = -1;
	
	public DictionaryManager() {

	}

	/**
	 * @param phrase
	 * @param scheme
	 * @return file name correlate with the phrase and scheme
	 */
	public String getFile(String phrase, String scheme){
		
		StringBuffer key = new StringBuffer();
		
		key.append(phrase).append("_").append(scheme.trim());
		
		String dicPath = null;
		
		if(isDirtyCache() || !m_cache.containsKey(key.toString().toLowerCase())){
			
			//get files and update to cache
			this.updateCache();
			
		}
		
		dicPath = (String)m_cache.get(key.toString().toLowerCase());
		//System.out.print("Dic path: "+dicPath+" in: "+m_cache);
		//LOG.info("Dic path: "+dicPath+" in: "+m_cache);
		return dicPath;
		
	}
	
	/**
	 * @return false if the m_cache age is over the max age (m_deadage)
	 */
	private boolean isDirtyCache(){

		boolean m_isdirty = false;
		
		long m_currenttime = System.currentTimeMillis();
		
		if(m_lastupdate > 0){
			
			long m_lifetime = m_currenttime - m_lastupdate;
			
			if(m_lifetime >= m_deadage){

				m_isdirty = true;
				
			}			
			
		}else{
			
			m_lastupdate = m_currenttime;
			
			m_isdirty = true;
			
		}
		
		return m_isdirty;
	}
	
	/**
	 * traverse through the dictionary directory and put all files name into the m_cache
	 */
	private void updateCache() {

		try {
			
			String folderName = Environment.DICTIONARY_DIR;
			
			CmsObject cmsObj = SystemAccount.getAdminCmsObject();	
			cmsObj.getRequestContext().setSiteRoot("/");				
			cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
			m_lastupdate = System.currentTimeMillis();
			
			//clear all data in cache
			m_cache.clear();
			
			//start get data
			getFilesInFolder(cmsObj, folderName, StringUtil.EMPTY_STRING);
			
			
		} catch(Exception ex) {
			LOG.error("Exception while traversing dictionary: "+ex.toString());
			ex.printStackTrace();
		}

	}
	
	
	private void getFilesInFolder(CmsObject cmsObj, String folderName, String scheme){
		
		try {
			
			//get all resources in dictionary folder (includes files and folders)
			List resources = cmsObj.getResourcesInFolder(folderName, CmsResourceFilter.ALL);
			
			//LOG.info("resources size: " + resources.size());
			//get iterator
			Iterator iter = resources.iterator();
			
			//loop through iterator
			for ( ; iter.hasNext(); ) {

				CmsResource resource = (CmsResource) iter.next();

				//if resource is a file then  add it's name to the m_cache
				if(resource.isFile()){
					
					CmsProperty title = cmsObj.readPropertyObject(resource, "Title", false);
					
					//key as phrase_scheme
					StringBuffer key = new StringBuffer();					
					key.append(title.getValue())
						.append("_")
						.append(scheme);
					
					//value is file name
					StringBuffer value = new StringBuffer();
					
					if( scheme.length() > 0){
					
						value.append("/").append(scheme).append("/").append(resource.getName()) ;
						
					}else{
						
						value.append("/").append(resource.getName());
						
					}
					
					//add new item to map
					m_cache.put(key.toString().toLowerCase(), value.toString());
					
				}				
				//else if resource is a folder then loop again
				else if(resource.isFolder()){
					
					getFilesInFolder(cmsObj, resource.getRootPath(), resource.getName());
					
				}
			}
			
		}catch(Exception ex) {
			//ex.printStackTrace();
			LOG.error("Error while getting files in dictionary folder: " + ex.toString());
			
		}		
	}	
}