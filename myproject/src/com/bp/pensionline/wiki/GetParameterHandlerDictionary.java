package com.bp.pensionline.wiki;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;


/**
 * @author SonNT
 */

public class GetParameterHandlerDictionary extends HttpServlet { 
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
    private String phrase;
    private String scheme;
    private String update;
    
    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    		
    		response.setContentType(Constant.CONTENT_TYPE);            
            PrintWriter out = response.getWriter();
            
            String xmlRequest = request.getParameter(Constant.PARAM);
            String xmlResponse = null;
           
            
            
            if(xmlRequest != null || xmlRequest.toString().equals("")){
            	try{
            		parseRequestXml(xmlRequest);
            		
            		XmlDictionaryDao xmlDictionaryDao = new XmlDictionaryDao(phrase,scheme, update);
            		
            		xmlResponse = xmlDictionaryDao.getXmlData();
            		
            	}
            	catch(Exception ex)
            	{	
            		LOG.info(ex.getMessage() + ex.getCause());
            	}
            }
           
    
            
            out.println(xmlResponse);        
            out.close();            
    }
    
    //Function to parse request String XML
    /**
     * accept a xml string contains parameters for request
     * @param xml
     */
    private void parseRequestXml(String xml){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        
        try{
            builder = factory.newDocumentBuilder();
            
            ByteArrayInputStream req = new ByteArrayInputStream(xml.getBytes());
            
            document = builder.parse(req);
            
            NodeList rootList = document.getElementsByTagName("Dictionary");  
            
            Node root = rootList.item(0);
            
            NodeList list = root.getChildNodes();
            int nlength = list.getLength();
            
            for(int i=0; i< nlength; i++){
                Node node = list.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE){
                    if(node.getNodeName().equalsIgnoreCase("Phrase")){
                    	phrase = node.getTextContent();
                    }else if(node.getNodeName().equalsIgnoreCase("Scheme")){
                    	scheme = node.getTextContent();
                    }
                    else if(node.getNodeName().equalsIgnoreCase("UpdateField")){
                    	update = node.getTextContent();
                    }
                }
            }            
            req.close();
            
            

        }catch(Exception ex){
            
            phrase = "";
            scheme = "";
            update = "";
        }
        
    }    

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }   
    public String getServletInfo() {
        return "Short description";
    }  
}


