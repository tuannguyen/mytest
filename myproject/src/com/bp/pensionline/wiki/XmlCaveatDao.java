package com.bp.pensionline.wiki;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;
import com.bp.pensionline.test.XmlReader;

/**
 * @author SonNT
 */

public class XmlCaveatDao {

	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
	private String m_phrase  = null;
	private String m_scheme = null;
	private String m_content = null;
	private String update = null;
	
	
	/**
	 * @param phrase
	 * @param scheme
	 */
	public XmlCaveatDao(String phrase, String scheme, String updateField){

    	this.m_phrase = phrase;
    	this.m_scheme = scheme;
    	this.update = updateField;
    	
		XmlReader reader = new XmlReader();
		
		byte[] arr = null;
		
		try {
			//Read file from VFS
			CaveatManager caveatMan = new CaveatManager();			
			String cavePath = caveatMan.getFile(phrase, scheme);			
			arr = reader.readFile( "caveat"+cavePath);
			//call load function
			loadXml(arr);
			
		}
		catch(FileNotFoundException fnfe){
			System.out.println(fnfe.getMessage());
		}
		catch (Exception e) {
			LOG.info(e.getMessage() + e.getCause());
		}
    }
	
	/**
	 * get value from file
	 * @param arr
	 */
	public void loadXml(byte[] arr){
		
        try{
        	
        	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = null;
            
            ByteArrayInputStream is =  new ByteArrayInputStream(arr);            
            document = builder.parse(is);
            
            NodeList contList = document.getElementsByTagName("content");            
            Node cont = contList.item(0);          
            Node contCont = cont.getFirstChild();
            
            if(contCont.getNodeType() == Node.CDATA_SECTION_NODE){       		
              		
        		String value = contCont.getTextContent();        		
        		this.m_content = value;        		
            }
            
            is.close();
            
        }catch(Exception ex){
        	LOG.info(ex.getMessage() + ex.getCause());
        }
    }
	
    /**
     * generate data response
     * @return
     */
    public String getXmlData(){    	

    	
    	if(m_content != null ){    	
    		StringBuffer buffer = new StringBuffer();

            buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
            buffer.append("     <").append("Caveat").append(">\n");
            buffer.append("         <").append("Phrase").append(">").append(m_phrase).append("</").append("Phrase").append(">\n");
            buffer.append("         <").append("Scheme").append(">").append(m_scheme).append("</").append("Scheme").append(">\n");
            buffer.append("         <").append("UpdateField").append(">").append(update).append("</").append("UpdateField").append(">\n");
            buffer.append("         <").append("Title").append(">").append(m_phrase).append("</").append("Title").append(">\n");
            buffer.append("         <").append("Description").append(">").append(m_content).append("</").append("Description").append(">\n");
            buffer.append("     </").append("Caveat").append(">\n");
            buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
            

            
			return buffer.toString();
			  
			
    	}else{
    		
    		return getXmlDataError(this.m_phrase, this.m_scheme,"Not Found");    		
    	}    	
    }
    /**
     * generate error response
     * @param phrase
     * @param scheme
     * @param mess
     * @return
     */
    private String getXmlDataError(String phrase, String scheme,String mess){

        StringBuffer buffer = new StringBuffer();
        
        buffer.append("<").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");
        buffer.append("     <").append("Caveat").append(">\n");
        buffer.append("         <").append("Phrase").append(">").append(phrase).append("</").append("Phrase").append(">\n");
        buffer.append("         <").append("Scheme").append(">").append(scheme).append("</").append("Scheme").append(">\n");
        buffer.append("         <").append("UpdateField").append(">").append(update).append("</").append("UpdateField").append(">\n");
        buffer.append("         <").append(Constant.ERROR).append(">\n");
        buffer.append("             <").append(Constant.MESSAGE).append(">[["+ mess + phrase +"]]</").append(Constant.MESSAGE).append(">\n");
        buffer.append("         </").append(Constant.ERROR).append(">\n"); 
        buffer.append("     </").append("Caveat").append(">\n");
        buffer.append("</").append(Constant.AJAX_PARAMETER_RESPONSE).append(">\n");

        return buffer.toString();    	
    }
}