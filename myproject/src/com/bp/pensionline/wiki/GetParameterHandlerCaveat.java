package com.bp.pensionline.wiki;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bp.pensionline.test.Constant;


/**
 * @author SonNT
 */

public class GetParameterHandlerCaveat extends HttpServlet { 
	
	public static final Log LOG = CmsLog.getLog(org.opencms.jsp.CmsJspLoginBean.class);
    private String phrase;
    private String scheme;
    private String update;
    
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
    		response.setContentType(Constant.CONTENT_TYPE);            
            PrintWriter out = response.getWriter();
            
            String xml = request.getParameter(Constant.PARAM);
            

            
            String xmlResponse = null;
            
            if(xml != null){
            	try{
            		//parse xml request
            		parseRequestXml(xml);
            		
            		

            		//Call xmlDao
            		XmlCaveatDao xmlDictionaryDao = new XmlCaveatDao(phrase, scheme, update);
            		
            		//build response xml
            		xmlResponse = xmlDictionaryDao.getXmlData();
            	}
            	catch(Exception ex)
            	{	
            		LOG.info(ex.getMessage() + ex.getCause());
            	}
            }       
            
            
            out.println(xmlResponse);   
            
          
            
            out.close();
            
    }
    
    /**
     * Function to parse request String XML
     * @param xml
     */
    private void parseRequestXml(String xml){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        
        try{
            builder = factory.newDocumentBuilder();
            
            ByteArrayInputStream req = new ByteArrayInputStream(xml.getBytes());
            
            document = builder.parse(req);
            
            NodeList rootList = document.getElementsByTagName("Caveat");  
            
            Node root = rootList.item(0);
            
            NodeList list = root.getChildNodes();
            int nlength = list.getLength();
            
            for(int i=0; i< nlength; i++){
                Node node = list.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE){
                    if(node.getNodeName().equalsIgnoreCase("Phrase")){
                    	phrase = node.getTextContent();
                    }else if(node.getNodeName().equalsIgnoreCase("Scheme")){
                    	scheme = node.getTextContent();
                    }
                    else if(node.getNodeName().equalsIgnoreCase("UpdateField")){
                    	update = node.getTextContent();
                    }
                }
            }            
            req.close();
            

            

        }catch(Exception ex){            
            phrase = "";
            scheme = "";
            update = "";
        }
        
    }    

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }   
    public String getServletInfo() {
        return "Short description";
    }  
}


