/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package com.cmg.monitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opencms.main.CmsInitException;

import com.bp.pensionline.constants.Environment;
import com.bp.pensionline.database.DBConnector;

/**
 * Servlet implementation class for Servlet: CMGFunctionalMonitorServlet
 */
public class CMGFunctionalMonitorServlet extends HttpServlet{
    static final long serialVersionUID = 1L;

    /**
     * Creates a new CMGFunctionalMonitorServlet object.
     */
    public CMGFunctionalMonitorServlet() {
        super();
    }

    /** (non-Java-doc)
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request,
     * HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        // Database
        String sqlQuerySelect = "SELECT SYSDATE FROM DUAL";
        String sqlMySQLQuerySelect = "SELECT getDate()as Now";
        String sqlWebstatQuerySelect =
            "Select event_date as event_timestamp from BP_STATS_ACCESS where seqno = ( select max(seqno) from BP_STATS_ACCESS)";
        Connection con = null;
        String oracleDate = "0000-00-00 00:00:00.0";
        String mysqlDate = "0000-00-00 00:00:00.0";
        String webstatDate = "0000-00-00 00:00:00.0";
        Statement stm = null;
        ResultSet rs = null;

        // Exceptions in details
        String errorOracle = "None";
        String desOracle = "";
        String errorMySQL = "None";
        String desMySQL = "";
        String errorWebStats = "None";
        String desWebStats = "";

        // DB time
        long pingTime = System.currentTimeMillis();

        // Checks Oracle
        try {
            DBConnector connector = DBConnector.getInstance();
            con = connector.getDBConnFactory(Environment.AQUILA);
            stm = con.createStatement();
            rs = stm.executeQuery(sqlQuerySelect);
            if (rs.next()) {
                oracleDate = rs.getString("SYSDATE");
            }

            // Updates time
            pingTime = System.currentTimeMillis() - pingTime;
            desOracle = "Ping time = " + pingTime + " milliseconds. ";
        } catch (CmsInitException ciex) {
            errorOracle = ciex.getClass().getSimpleName();
            desOracle += ciex.toString();
            ciex.printStackTrace();
        } catch (SQLException sex) {
            errorOracle = sex.getClass().getSimpleName();
            desOracle += sex.toString();
            sex.printStackTrace();
        } catch (Exception ex) {
            errorOracle = ex.getClass().getSimpleName();
            desOracle += ex.toString();
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    DBConnector connector = DBConnector.getInstance();
                    connector.close(con);
                } catch (Exception ex) {
                    errorOracle = ex.getClass().getSimpleName();
                    desOracle += ex.toString();
                    ex.printStackTrace();
                }
            }
        }

        // Checks MySQL
        pingTime = System.currentTimeMillis();
        try {
            DBConnector connector = DBConnector.getInstance();
            con = connector.getDBConnFactory(Environment.SQL);
            stm = con.createStatement();
            rs = stm.executeQuery(sqlMySQLQuerySelect);
            if (rs.next()) {
                mysqlDate = rs.getString("Now");
            }

            // Updates time
            pingTime = System.currentTimeMillis() - pingTime;
            desMySQL = "Ping time = " + pingTime + " milliseconds. ";
        } catch (CmsInitException ciex) {
            errorMySQL = ciex.getClass().getSimpleName();
            desMySQL += ciex.toString();
            ciex.printStackTrace();
        } catch (SQLException sex) {
            errorMySQL = sex.getClass().getSimpleName();
            desMySQL += sex.toString();
            sex.printStackTrace();
        } catch (Exception ex) {
            errorMySQL = ex.getClass().getSimpleName();
            desMySQL += ex.toString();
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    DBConnector connector = DBConnector.getInstance();
                    connector.close(con);
                } catch (Exception ex) {
                    errorMySQL = ex.getClass().getSimpleName();
                    desMySQL += ex.toString();
                    ex.printStackTrace();
                }
            }
        }

        // Checks WebStats
        pingTime = System.currentTimeMillis();
        try {
            DBConnector connector = DBConnector.getInstance();
            con = connector.getDBConnFactory(Environment.WEBSTATS);
            stm = con.createStatement();
            rs = stm.executeQuery(sqlWebstatQuerySelect);
            if (rs.next()) {
                webstatDate = rs.getString("event_timestamp");
            }

            // Updates time
            pingTime = System.currentTimeMillis() - pingTime;
            desWebStats = "Ping time = " + pingTime + " milliseconds. ";
        } catch (CmsInitException ciex) {
            errorWebStats = ciex.getClass().getSimpleName();
            desWebStats += ciex.toString();
            ciex.printStackTrace();
        } catch (SQLException sex) {
            errorWebStats = sex.getClass().getSimpleName();
            desWebStats += sex.toString();
            sex.printStackTrace();
        } catch (Exception ex) {
            errorWebStats = ex.getClass().getSimpleName();
            desWebStats += ex.toString();
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    DBConnector connector = DBConnector.getInstance();
                    connector.close(con);
                } catch (Exception ex) {
                    errorWebStats = ex.getClass().getSimpleName();
                    desWebStats += ex.toString();
                    ex.printStackTrace();
                }
            }
        }
        long freeMemory = Runtime.getRuntime().freeMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();
        long memoryUsed = totalMemory - freeMemory;
        long used = memoryUsed / (1024 * 1024);
        long total = maxMemory / (1024 * 1024);

        // Generate header
        PrintWriter pw = response.getWriter();
        pw.println("<html><head><title>CMG Web Monitor</title></head><body>");
        pw.println("<h1>CMG Monitor</h1><hr>");

        // Generate body
        pw.println("<table width=\"90%\" align=\"center\"><tbody>");

        // THs
        pw.println("<tr align=\"left\">");
        pw.println("<th width=\"15%\"><small>Option</small></th>");
        pw.println("<th width=\"15%\"><small>Value</small></th>");
        pw.println("<th width=\"25%\"><small>Error</small></th>");
        pw.println("<th width=\"45%\"><small>Description</small></th>");
        pw.println("</tr>");

        // TRs
        pw.println("<tr>");
        pw.println("<td>Oracle sysdate</td>");
        pw.println("<td>" + oracleDate + "</td>");
        pw.println("<td>" + errorOracle + "</td>");
        pw.println("<td>" + desOracle + "</td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td>MS SQL sysdate</td>");
        pw.println("<td>" + mysqlDate + "</td>");
        pw.println("<td>" + errorMySQL + "</td>");
        pw.println("<td>" + desMySQL + "</td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td>Last webstat</td>");
        pw.println("<td>" + webstatDate + "</td>");
        pw.println("<td>" + errorWebStats + "</td>");
        pw.println("<td>" + desWebStats + "</td>");
        pw.println("</tr>");

        // End of table
        pw.println("</tbody></table>");

        // Generate footer
        pw.println("</body></html>");

        // Memory proess
        pw.println("<div>");
        pw.println("<p>");
        pw.println(
            "<strong>Option");
        pw.println("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        pw.println("&nbsp;&nbsp;&nbsp;&nbsp;Value</strong>");
        pw.println("</p>");

        // Free Memory
        pw.println("<p>freeMemory&nbsp;&nbsp;&nbsp; " + freeMemory + "</p>");

        // Total Memory
        pw.println("<p>totalMemory&nbsp;&nbsp;&nbsp; " + totalMemory + "</p>");

        // Max Memory
        pw.println("<p>maxMemory&nbsp;&nbsp;&nbsp; " + maxMemory + "</p>");

        // Memory Used
        pw.println("<p>memoryUsed&nbsp;&nbsp;&nbsp; " + memoryUsed + "</p>");

        // End div
        pw.println("</div>");

        // Memory General
        pw.println("<p><strong>" + used + " of " + total
            + " MB used.</strong></p>");

        // Close
        pw.close();
    }

    /** (non-Java-doc)
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request,
     * HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
    }

    public void init() {
        System.out.println(getServletName() + ": initialised");
    }
}
