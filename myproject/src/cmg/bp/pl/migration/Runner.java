/**
 * 
 */
package cmg.bp.pl.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import cmg.bp.pl.migration.bean.MemberBean;
import cmg.bp.pl.migration.db.JDBCConnector;
import cmg.bp.pl.migration.db.NamedParameterStatement;
import cmg.bp.pl.migration.sql.SqlStatement;
import cmg.bp.pl.migration.util.CalcFactory;
import cmg.bp.pl.migration.util.Result;



/**
 * @author BachLe
 *
 */
public class Runner {
	
	private static String fileName     = "";
	private static String outputFile   = "";
	
	private List<String> sqls          = new ArrayList<String>();
		
	private static Map<String, String> refNos = new HashMap<String, String>();

	private StringBuffer sqlOut        = new StringBuffer(); //to view refined sql
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		fileName = args[0];
		outputFile = "RESULT_"+fileName;
		
		Runner run = new Runner();
		refNos = run.getRefNo(fileName);
		
		if (refNos.size() == 0) return;
		System.out.println("Migration Tool - running SQL");
		run.runSQLs();
		System.out.println("\nFinished. The process is automatically exiting....");
		Thread.sleep(10000);
	}
	
	public void runSQLs() {
		String ip = "0.0.0.0";
		List<MemberBean> memberList = new ArrayList<MemberBean>();
		try {
			JDBCConnector connector = new JDBCConnector();
			ip = connector.getIP();
			Connection conn = connector.getConnection();
			
			int colNameMaxLength = 0;
			
			Set<String> keyset = refNos.keySet();
			//execute for each member
			for (Iterator<String> iter=keyset.iterator(); iter.hasNext(); ) {
				String refNo = iter.next();
				String bGroup  = refNos.get(refNo);
				
				NamedParameterStatement st = new NamedParameterStatement(conn, SqlStatement.SQL_HEAD_INFO);
				st.setString("bgroup", bGroup);
				st.setString("refno", refNo);
				ResultSet res = st.executeQuery();
				String memberStatus = "";
				String schemeCode   = "";
				if (res.next()) {
					memberStatus = res.getString("Status");
					schemeCode   = res.getString("Scheme");
					res.close();
					st.close();
				}
				getQueries(bGroup, schemeCode, memberStatus);
				
				MemberBean userInfo = new MemberBean(refNo, bGroup);
				//userInfo.put(MemberBean.MEMBER_SCHEME, schemeCode);
				for (int j=0; j<sqls.size(); j++) {
					NamedParameterStatement stat = new NamedParameterStatement(conn, sqls.get(j));
					
					//handler for each SQL
					try {
						if (stat.includeParam("bgroup")) {
							stat.setString("bgroup", bGroup);
						}
						if (stat.includeParam("refno")) {
							stat.setString("refno", refNo);
						}
						
						//sqlOut.append(stat.parsedSQL+"\n\n");
						ResultSet rs = stat.executeQuery();
						ResultSetMetaData meta = rs.getMetaData();
						
						int cols = meta.getColumnCount();
						while (rs.next()) {
							for (int k=1; k<=cols; k++) {
								Object obj = rs.getObject(k);
								userInfo.put(meta.getColumnName(k).trim(), obj!=null?obj.toString():"No information available");
								
								if (meta.getColumnName(k).length() > colNameMaxLength) {
									colNameMaxLength = meta.getColumnName(k).length();
								}
							}
						}
					} catch (SQLException sqle) {
						userInfo.replaceValueMap(new HashMap<String,String>());
						userInfo.put("No result-JDBC Error", sqle.getMessage());
						
					} catch (IllegalArgumentException iae) {
						userInfo.replaceValueMap(new HashMap<String,String>());
						userInfo.put("No result-JDBC Error", iae.getMessage());
					}
				}
				
				//run CALC
				if ("BPF".equals(bGroup) && "0001".equals(schemeCode) &&
					"AC".equals(userInfo.getMemberData(MemberBean.MEMBER_STATUS))) {
					userInfo.put(JDBCConnector.CONNECTION_USERNAME, connector.getUserName());
					userInfo.put(JDBCConnector.CONNECTION_PASSWORD, connector.getPassword());
					CalcFactory calcFac = new CalcFactory(conn, userInfo, colNameMaxLength);
					try {
						userInfo = calcFac.runAllCalc(userInfo);
					} catch (SQLException se) {
						System.out.println("\nCould not run calculations for user "+refNo+" in group "+bGroup);
						userInfo.putCalcVal("ERROR_@@@@@CALC", se.getMessage());
					} catch (Exception e) {
						System.out.println("\nCould not run calculations for user "+refNo+" in group "+bGroup);
						userInfo.putCalcVal("ERROR_@@@@@CALC", e.getMessage());
					}
					
					colNameMaxLength = calcFac.colNameMaxLength;
				} else {
					System.out.println("\nPlease wait while processing sql for user "+refNo+" in group "+bGroup);
					for (int i=0; i<12; i++) {
						System.out.print(". ");
						Thread.sleep(500);
					}
				}
	
				userInfo.valueMap.remove(JDBCConnector.CONNECTION_USERNAME);
				userInfo.valueMap.remove(JDBCConnector.CONNECTION_PASSWORD);
				
				if (userInfo.getMemberData(MemberBean.MEMBER_GROUP)==null) {
					userInfo.put(MemberBean.MEMBER_GROUP, bGroup);
				}
				if (userInfo.getMemberData(MemberBean.MEMBER_REFNO)==null) {
					userInfo.put(MemberBean.MEMBER_REFNO, refNo);
				}
				
				System.out.println("\nAll details for user "+refNo+" in group "+bGroup+" complete");
				memberList.add(userInfo);
			}
			
			conn.close();
			
			//output the result
			Result.saveResult(outputFile, memberList, colNameMaxLength);
			
		} catch(SQLException se) {
			System.out.println("\nI am sorry but we have not been able to connect to "+ip+". Please check your network configuration file");
			System.out.println("Error details: "+se.getMessage());
		} catch(Exception e) {
			System.out.println("\nException: "+e.toString());
		}
	}
	
	public static Map<String, String> getRefNo (String file){
		Map<String, String> refNoList = new HashMap<String, String>();
		String line = "";
		StringBuffer buf = new StringBuffer();
		
		try {
			LineNumberReader reader = new LineNumberReader(new FileReader(new File(file)));
			while ((line=reader.readLine()) != null) {
				buf.append(line);
			}
			
		} catch (FileNotFoundException fe) {
			System.out.println("File "+file+" not found");
			
		} catch (IOException ie) {
			System.out.println("Exception: "+ie.toString());
		}
		
		
		StringTokenizer tokens = new StringTokenizer(buf.toString(),",");
		while (tokens.hasMoreElements()) {
			String member = tokens.nextToken().trim();
			StringTokenizer token = new StringTokenizer(member,"-");
			if (token.countTokens() == 2) {
				String bGroup = token.nextToken();
				String refNo  = token.nextToken();
				refNoList.put(refNo, bGroup);
			}
		}

		return refNoList;
	}
	
	private void getQueries(String bGroup, String schemeCode, String memberStatus) {
		sqls.clear();
		sqls.add(SqlStatement.SQL_ADDITIONAL_BENEFIT);
		sqls.add(SqlStatement.SQL_BANK_DETAILS);
		sqls.add(SqlStatement.SQL_BASIC_SALARY);
		sqls.add(SqlStatement.SQL_COMPANY);
		sqls.add(SqlStatement.SQL_CPF);
		sqls.add(SqlStatement.SQL_FTE);
		sqls.add(SqlStatement.SQL_GENERAL);
		sqls.add(SqlStatement.SQL_LTA);
		sqls.add(SqlStatement.SQL_PESIONALBE_SALARY);
		sqls.add(SqlStatement.SQL_PLO_DETAILS);
		
		if("BPF".equals(bGroup)) {
			if ("0001".equals(schemeCode)) {
				if ("AC".equals(memberStatus)) {
					sqls.add(SqlStatement.SQL_BPAC_CONTRIBUTING);
				} else {
					
				}
			} else {//SUBS
				if ("AC".equals(memberStatus)) {
					sqls.add(SqlStatement.SQL_SUBSAC_AVC_LTA);
					sqls.add(SqlStatement.SQL_SUBSAC_STATIC_BENEFIT_STATEMENT);
				} else {
					
				}
			}
		} else if ("BCF".equals(bGroup)) {
			sqls.add(SqlStatement.SQL_BC_ADDITIONAL_BENEFIT);
			if ("AC".equals(memberStatus)) {
				sqls.add(SqlStatement.SQL_BCAC_AVC_LTA);
				sqls.add(SqlStatement.SQL_BCAC_EXPLANATORY_NOTES);
				sqls.add(SqlStatement.SQL_BCAC_STATIC_BENEFIT_STATEMENT);
			} else {
				
			}
		} else {
			
		}
	}

}
