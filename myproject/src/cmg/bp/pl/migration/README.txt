BP PensionLine Migration Test Tool

Company - www.C-MG.biz
Contact - info@C-MG.biz

Requirements
============
This tool requires you to have installed Java, it has been tested with version JDK version 1.6.0 to run. 

If your machine does not have the JDK, then please download it from (follow their instructions for the correct version for your operating system):

        http://java.sun.com/javase/downloads/ea/6u10/6u10rcDownload.jsp#6u10JDKs


Running
========
1. Unpack the Zip file to C:/tmp

2. To run, create a CSV file called ��XXXX.txt�� with BGROUP-REFNO (BCF-0019241, BPF-0113415, ....), please look ref.txt for reference
 
3. Make sure the configuration file (configure.properties) is available with content:

		url=url string to connect to the DataSource, ex: jdbc:oracle:thin:@127.1.1.203:4524:BP101P
		username=username of database schemma, ex: BPPL4CMS
		password=password relevant to above username, ex: BPPL4CMS
		
		
		*NOTE: your machine MUST have access to IP in url above. To test access, use "ping" command. Example with IP 127.1.1.203:
		
			        > ping 127.1.1.203
			        
			   If you receive something like below, that means accessing to the IP is OK:
			   
					Pinging 127.1.1.203 with 32 bytes of data:
					Reply from 127.1.1.203: bytes=32 time<1ms TTL=128
					Reply from 127.1.1.203: bytes=32 time<1ms TTL=128
					Reply from 127.1.1.203: bytes=32 time<1ms TTL=128
					Reply from 127.1.1.203: bytes=32 time<1ms TTL=128
					
					Ping statistics for 127.1.1.203:
						Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
					Approximate round trip times in milli-seconds:
						Minimum = 0ms, Maximum = 0ms, Average = 0ms
		
4. Open a command line, DOS-prompt, navigate to the directory c:\tmp containing. 

5. select the "batch" and wait

6. The output file will be "RESULT_XXXX.txt" where XXXX is the name of input CSV file.