package cmg.bp.pl.migration.sql;

/**
 * @author BachLe
 *
 */
public class SqlStatement {
	
	public static final String SQL_HEAD_INFO = "select b.sd01x as Scheme, b.bd19a as Status " +
											   "from basic b  " +
											   "where b.bgroup= :bgroup" +
											   " 	  and b.refno= :refno";
	
	public static final String SQL_GENERAL = "select   b.bgroup as \"Bgroup\"" +
														",b.refno as \"Refno\"" +
														",b.BD08X as \"Nino\"" +
														",nvl(b.BD04X, 'No information available') as \"EmployeeNumber\"" +
														",nvl(b.BD29X, 0) as \"SecurityIndicator\"" +
														",b.BD19A as \"MembershipStatusRaw\"" +
														",b.SD01X as \"SchemeRaw\"" +
														",b.SD01X as \"Scheme\"" +
														",sd.SD02X as \"SchemeName\"" +
														",trim(leading '0' from initcap(to_char(b.BD22D, 'DD MON YYYY'))) as \"DateJoinedScheme\"" +
														",trim(leading '0' from initcap(to_char(b.BD18D, 'DD MON YYYY'))) as \"DateOfLeaving\""   +
														",trim(leading '0' from initcap(to_char(b.BD18D, 'DD MON YYYY'))) as \"PensionStartDate\"" +
														",cd.CA03I as \"Nra\"" +
														",nvl(cd.CA71I, cd.CA03I) as \"Npa\"" +
														",trim(leading '0' from initcap(to_char(b.bd11d, 'DD MON YYYY'))) as \"Dob\"" +
														",trim(leading '0' from initcap(to_char(ADD_MONTHS(b.bd11d, cd.ca03i * 12), 'DD MON YYYY'))) as \"Nrd\"" +
														",trim(leading '0' from initcap(to_char(ADD_MONTHS(b.bd11d, nvl(cd.CA71I, cd.CA03I) * 12), 'DD MON YYYY'))) as \"Npd\"" +
														",initcap(b.BD07A || ' ' || b.BD05A || ' ' || b.BD38A) as \"Name\"" +
														",initcap(b.BD07A) as \"Title\"" +
														",initcap(b.BD38A) as \"Surname\"" +
														",initcap(b.BD07A || ' ' || b.BD38A) as \"ShortName\"" +
														",initcap(addcode) as \"AddressType\"" +
														",replace( trim(trailing chr(13) from " +  
														      "replace( replace(   initcap(address1) || chr(13) "+
														                       "|| initcap(address2) || chr(13) "+
														                       "|| initcap(address3) || chr(13) "+
														                       "|| initcap(address4) || chr(13) "+
														                       "|| initcap(address5) || chr(13) "+
														                       "|| postcode || chr(13) "+
														                       "|| country, chr(13)||chr(13)||chr(13), chr(13)), "+ 
														                "chr(13)||chr(13), chr(13))), "+ 
														      "chr(13), '&lt;br/&gt;') as \"Address\""+
														 ",replace(substr(ad.postcode, 0, 4), ' ', '') as \"WsPostcode\"" +
														 ",nvl(mpac.MPAM06X, 'No information available') as \"TelephoneNumber\" "+
														 " ,nvl(mpac.MPAM07X, 'No information available') as \"MobileNumber\" " +
														 " ,decode((MPAM02X ||'@'|| MPAM04X), '@', 'No information available', (MPAM02X ||'@'|| MPAM04X)) as \"EmailAddress\" " +
														 " ,nvl(mpac.MPAM05X, 'No information available') as \"FaxNumber\" " +
														 " ,nvl(b.BD48X, 'P') as \"Newsletter\"" +
														 " ,decode(nvl(b.BD57X, 'P'), 'P', 'Paper', 'E', 'Electronically by e-mail', 'A', 'Audio tape', 'None') as \"Newsletter.Text\"" +
														 " ,'false' as \"CurrentlyContributing\"" +
														 " ,nvl(b.BD10A, 'U') as \"MaritalStatus\"" +
														 " ,sddl.dol_description as \"MaritalStatus.Text\"" +
														 " ,nvl(b.BD09A, 'null') as \"Gender\"" +
														 " ,decode(nvl(b.BD09A, 'null'), 'M', 'Male', 'F', 'Female', 'Unknown') as \"Gender.Text\"" +
														 " ,decode(nvl(ad.country_co, 'null'), 'Y', 'true', 'N', 'false', 'false') as \"OverseasIndicator.Text\"" +
														 " ,trim(both ' ' from substr(postcode, 0, 4)) as \"WsPostcode\"" +
														 " , 0 as \"BenefitStatementType\"" +
														 " ,trim(leading '0' from initcap(to_char(sysdate, 'DD MON YYYY'))) as \"BenefitStatementDate\"" +
														 " ,'salary' as \"SalaryOrPay\"" +
														 " ,'scheme' as \"SchemeOrFund\"" +
														 " ,'current pensionable salary' as \"CurrentSalaryOrPay\"" +
														 " ,'current pensionable salary' as \"FinalSalaryOrPay\"" +
														 " ,'Current pensionable salary' as \"CurrentSalaryOrPayCaps\"" +
														 " ,'Current pensionable salary' as \"FinalSalaryOrPayCaps\"" +
														 " ,'The Directors of BP Pension Trustees Limited' as \"EowAddressee\"" +
														 " ,'BP Pension Fund Trustee' as \"EowTrustee\"" +
														 " ,trim(leading '0' from initcap(to_char(sysdate, 'DD MONTH YYYY'))) as \"LetterDate\"" +
														 " ,'XyzAbc123' as \"InitialPassword\"   " +
														"from " +
														 " basic b" +
														 " ,SCHEME_DETAIL sd" +
														 " ,CATEGORY_DETAIL cd" +
														 " ,PS_ADDRESSDETAILS ad" +
														 " ,PS_ADDRESS psad" +
														 " ,MP_ADDCOMM mpac " +
														 " ,sd_domain_list sddl  " +
														"where " +
														 " b.bgroup= :bgroup" +
														 " and b.refno= :refno" +
														 " and cd.bgroup= b.bgroup" +
														 " and cd.ca26x= b.ca26x" +
														 " and psad.bgroup= b.bgroup" +
														 " and psad.refno= b.refno" +
														 " and psad.addcode='GENERAL'" +
														 " and psad.endd is null" +
														 " and ad.bgroup = b.bgroup" +
														 " and ad.addno= psad.ADDNO" +
														 " and sd.bgroup= b.bgroup" +
														 " and sd.sd01x= b.sd01x" +
														 " and sddl.domain = 'MAR01'" +
														 " and sddl.bgroup = b.bgroup" +
														 " and sddl.dol_listval = nvl(b.BD10A, 'U')" +
														 " and mpac.bgroup (+) = b.bgroup  " +
														 " and mpac.refno  (+) = b.refno  " +
														 " and mpac.mpam01x (+) = 'GENERAL'";
	
	public static final String SQL_LTA = "select trim(to_char(rf.REVFAC, '999999999.99')) as Lta" +
													 ", chr(163)|| trim(to_char(rf.REVFAC, '999,999,999')) as \"Lta.Pound\" " +
													 "from (select to_number(to_char(sysdate, 'YYYY'),'9999') year " +
													 "             from dual " +
													 "      union select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year " +
													 "             from dual " +
													 "            where to_char(sysdate, 'MMDD') < to_number('0406', '9999')) taxyear" +
													 "     ,revfac rf " +
													 "where  rf.FACTYPE = 'LTAA' and rf.YOE = taxyear.year";
	
	public static final String SQL_PESIONALBE_SALARY = "select trim(to_char(ps.mh03c,'999999999.99')) as \"PensionableSalary\" " +
			                                           "       ,chr(163)||trim(to_char(ps.mh03c,'999,999,999')) as \"PensionableSalary.Pound\" " +
			                                           "from basic b," +
			                                           "	(select seqno " +
			                                           "            from (select seqno, mh02d " +
			                                           "					from MISCELLANEOUS_HISTORY " +
			                                           "					where  bgroup = :bgroup" +
			                                           "						and refno = :refno" +
			                                           "						and mh03c is not null" +
			                                           "					order by mh02d desc ) " +
			                                           "       		where rownum < 2) pss" +
			                                           "	,MISCELLANEOUS_HISTORY ps " +
			                                           "where   b.bgroup = :bgroup" +
			                                           "	and b.refno = :refno" +
			                                           "	and ps.bgroup (+)= b.bgroup" +
			                                           "	and ps.refno  (+)= b.refno" +
			                                           "	and ps.seqno     = pss.seqno";
	
	public static final String SQL_PLO_DETAILS = "select decode( nvl(wd.WD05X, 'N')" +
												 "				, 'Y', 'true'" +
												 "				, 'N', 'false'" +
												 "				) as \"PloDetailsAvailable\" " +
												 "		,nvl(wd.wd01x, 'AR37') as \"PloArea\"" +
												 		",mp.mpfm15x as \"PloName\"" +
												 		",replace( trim(trailing chr(13) from " +
												 		"				replace( replace(MPFM03X || chr(13)" +
												 		"								|| MPFM04X || chr(13)" +
												 		"								|| MPFM05X || chr(13)" +
												 		"								|| MPFM06X || chr(13)" +
												 		"								|| MPFM07X || chr(13)" +
												 		"								|| MPFM11X || chr(13)" +
												 		"								|| MPFM13X, chr(13)||chr(13)||chr(13), chr(13)), " +
												 		"						chr(13)||chr(13), chr(13))), " +
												 		"         chr(13), '&lt;br/&gt;') as \"PloAddress\"" +
												 		",mp.MPFM08X as \"PloTelephone\" "+
												 "from basic b" +
												 "	   ,welfare_detail wd" +
												 "		,mp_manager mp   " +
												 "where b.bgroup= :bgroup" +
												 "		and b.refno= :refno" +
												 "		and wd.bgroup (+) = b.bgroup" +
												 "		and wd.refno  (+) = b.refno" +
												 "		and mp.bgroup     = b.bgroup" +
												 "		and mp.manager    = nvl(wd.wd01x, 'AR37')";
	
	
	
	public static final String SQL_FTE = "SELECT nvl(fte, 1) AS \"FTE\"" +
										 "FROM (SELECT ta.ta10p / ta.ta11p AS \"FTE\" " +
										 		"FROM temporary_absence ta " +
										 		"WHERE bgroup = :bgroup " +
										 		"		AND refno = :refno " +
										 		"		AND ta03a = 'PT' " +
										 		"		AND ta05d IS NULL " +
										 		"UNION " +
										 		"SELECT NULL as \"FTE\" " +
										 		"FROM dual) a " +
										 "WHERE rownum < 2";
	
	public static final String SQL_BASIC_SALARY = "select trim(to_char(ps.mh04c,'999999999.99')) as \"BasicSalary\"" +
												  "		 ,chr(163)||trim(to_char(ps.mh04c,'999,999,999')) as \"BasicSalary.Pound\"  " +
												  "from basic b" +
												  "		,(select seqno" +
												  "			from  (select seqno, mh02d  " +
												  "					from MISCELLANEOUS_HISTORY " +
												  "					where bgroup = :bgroup" +
												  "						and refno = :refno" +
												  "						and mh04c is not null" +
												  "					order by mh02d desc) " +
												  "			where rownum < 2) pss " +
												  "		,MISCELLANEOUS_HISTORY ps " +
												  "where b.bgroup = :bgroup" +
												  "		and b.refno = :refno" +
												  "		and ps.bgroup (+)= b.bgroup" +
												  "		and ps.refno  (+)= b.refno" +
												  "		and ps.seqno     = pss.seqno";
	
	
	public static final String SQL_COMPANY = "select 'BP' as \"Company\", bgroup || '-' || sd01x as \"SchemeId\"  " +
											 "from basic b  " +
											 "where b.bgroup = 'BPF' " +
											 "		and b.bgroup = :bgroup" +
											 "		and b.refno = :refno" +
											 "		and b.sd01x = '0001' " +
											 "union " +
											 "select 'Subs' as \"Company\", bgroup || '-' || sd01x as \"SchemeId\"  " +
											 "from basic b " +
											 "where b.bgroup = 'BPF' " +
											 "		and b.bgroup = :bgroup" +
											 "		and b.refno = :refno" +
											 "		and b.sd01x != '0001' " +
											 "union " +
											 "select 'BC' as \"Company\", bgroup || '-' || sd01x as \"SchemeId\" " +
											 "from basic b  " +
											 "where b.bgroup = 'BCF' " +
											 "		and b.bgroup = :bgroup" +
											 "		and b.refno = :refno";
	
	
	public static final String SQL_CPF = "select decode(nvl(state_benefits.ASD71X, 'Y'), 'Y', 'false', 'N', 'true')  as \"StateBenefitsAvailable\"" +
										 "      ,substr(state_benefits.ASD70X, 1, 2) || ' years and ' " +
										 "											 || to_number(substr(state_benefits.ASD70X, 3, 2)) " +
										 "											 || ' months' as \"StatePensionAge\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD71R, 0),'9,999,999')) " +
										 "				  || ' a year' as \"CurrentBasicStatePension\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD72R, 0),'9,999,999')) " +
										 "				  || ' a year' as \"CurrentAdditionalStatePension\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD70R, 0),'9,999,999')) " +
										 "				  || ' a year' as \"CurrentTotalStatePension\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD74R, 0),'9,999,999')) " +
										 "				  || ' a year' as \"FutureBasicStatePension\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD75R, 0),'9,999,999'))" +
										 "				  || ' a year' as \"FutureAdditionalStatePension\"" +
										 "		,chr(163) || trim(both ' ' from to_char(nvl(state_benefits.ASD73R, 0),'9,999,999')) " +
										 "				  || ' a year' as \"FutureTotalStatePension\" " +
										 "from basic ," +
										 "		add_static_data state_benefits " +
										 "where basic.bgroup                       = :bgroup" +
										 "		and basic.refno                    = :refno" +
										 "		and state_benefits.bgroup      (+) = basic.bgroup" +
										 "		and state_benefits.REFNO       (+) = basic.refno" +
										 "		and state_benefits.DATA_TYPE   (+) = 'STATBEN'";
	
	
	public static final String SQL_BANK_DETAILS = "SELECT nvl(pmd.BKANO, '') as \"BankAccountNumber\"" +
												  "		  ,decode(pmd.BKSCODE, null, '[Not specified]'" +
												  "		  , substr(pmd.BKSCODE,1,2)|| '-' || substr(pmd.BKSCODE,3,2)|| '-' || substr(pmd.BKSCODE,5,2)) as \"SortCode\"" +
												  "		  ,nvl(pmd.BSROLLNO, '') as \"RollNumber\"" +
												  "		  ,nvl(initcap(pmd.BKANAME), '[Not specified]') as \"BankAccountName\"" +
												  "		  ,nvl(initcap(bank.bkname), '[Not specified]') as \"BankName\"" +
												  "FROM basic b" +
												  "		,PS_PMDETAILS pmd" +
												  "		,PS_PM pm" +
												  "		,PS_UKBANK bank  " +
												  "WHERE b.bgroup= :bgroup" +
												  "		AND b.refno= :refno" +
												  "		AND pm.BGROUP    (+) = b.bgroup" +
												  "		AND pm.REFNO     (+) = b.refno" +
												  "		AND pmd.PMDNO    (+) = pm.PMDNO " +
												  "		AND pmd.BGROUP   (+) = pm.BGROUP" +
												  "		AND bank.bkscode (+) = pmd.BKSCODE";
	
	
	public static final String SQL_ADDITIONAL_BENEFIT = "select tvin.tvindays as \"TvinBenefit\"" +
														"		,heritage.tvindays as \"HeritageBenefit\"" +
														"		,sqs.days as \"AugmentedBenefit\"" +
														"		,esb.days as \"EasternServiceBenefit\" " +
														"from (select max(tvindays) as tvindays from ((select REGEXP_REPLACE(nvl(floor(sum(nvl(ti.ti22i,0))/365), 0) || ' years ' || nvl(sum(ti.ti22i) - (floor(sum(ti.ti22i)/365) * 365),0)|| ' days', '^0 years ') as tvindays " +
														"											   from transfer_in ti " +
														"											   where bgroup= :bgroup" +
														"													and refno= :refno" +
														"													and ti.SUB in ('0010','0011','0012','0014','0015'))" +
														"union      " +
														"(select (ASD01X || ' years') as tvindays from add_static_data where data_type = 'SCHBEN' and bgroup = :bgroup and refno = :refno))) tvin" +
														",(select REGEXP_REPLACE(nvl(floor(sum(nvl(ben.days,0))/365), 0) " +
														"						|| ' years ' " +
														"						|| nvl(sum(ben.days) - (floor(sum(ben.days)/365) * 365),0) " +
														"						|| ' days', '^0 years ') as tvindays  " +
														"from ( select sum(nvl(ti.ti22i,0)) as days " +
														"		from transfer_in ti " +
														"		where bgroup= :bgroup" +
														"				and refno= :refno" +
														"				and ti.SUB in ('0022','0023')" +
														"		union " +
														"		select sum(nvl(ab.ab06i,0))  as days " +
														"		from augmentation_benefit ab " +
														"		where bgroup= :bgroup" +
														"				and refno= :refno" +
														"				and ab.SUB in ('OLDN','NEWE','NEWF','NEWK','NEWL')) ben) heritage" +
														",( select REGEXP_REPLACE(nvl(floor(sum(nvl(ab.ab06i,0))/365), 0) " +
														"						  || ' years ' " +
														"						  || nvl(sum(ab.ab06i) - (floor(sum(ab.ab06i)/365) * 365),0) " +
														"						  || ' days', '^0 years ') as days " +
														"	from augmentation_benefit ab " +
														"	where bgroup= :bgroup" +
														"			and refno= :refno " +
														"			and ab.SUB in ('OLDA','OLDB','OLDG')) sqs" +
														",( select REGEXP_REPLACE(nvl(floor(sum(nvl(ab.ab06i,0))/365), 0) " +
														"						  || ' years ' " +
														"						  || nvl(sum(ab.ab06i) - (floor(sum(ab.ab06i)/365) * 365),0) " +
														"						  || ' days', '^0 years ') as days " +
														"	from augmentation_benefit ab  " +
														"	where bgroup= :bgroup" +
														"			and refno= :refno" +
														"			and ab.SUB in ('OLDM','OLDH')) esb";
	
	public static final String SQL_BPAC_CONTRIBUTING = "select 'true' as \"CurrentlyContributing\" from dual";
	
	public static final String SQL_BC_ADDITIONAL_BENEFIT = "select REGEXP_REPLACE(nvl(floor(sum(nvl(ti.ti22i,0))/365), 0) " +
														   "					  || ' years ' " +
														   "					  || nvl(sum(ti.ti22i) - (floor(sum(ti.ti22i)/365) * 365),0)" +
														   "					  || ' days', '^0 years ') as \"TvinBenefit\" " +
														   "from transfer_in ti  " +
														   "where bgroup= :bgroup" +
														   "	  and refno= :refno" +
														   "	  and ti.SUB in ('1161','1162','1163','1164')";
	
	public static final String SQL_BC_GENERAL = "select 'pay' as \"SalaryOrPay\"," +
												"		'fund' as \"SchemeOrFund\"," +
												"		'final pensionable pay' as \"CurrentSalaryOrPay\"," +
												"		'final pensionable pay' as \"FinalSalaryOrPay\"," +
												"		'Final pensionable pay' as \"CurrentSalaryOrPayCaps\"," +
												"		'Final pensionable pay' as \"FinalSalaryOrPayCaps\"," +
												"		'The Directors of Burmah Castrol Pension Trustees Limited' as \"EowAddressee\"," +
												"		'Burmah Castrol Pension Fund Trustee' as \"EowTrustee\", " +
												"		2 as \"BenefitStatementType\"" +
												"from DUAL ";
	
	public static final String SQL_BCAC_AVC_LTA = "select chr(163)||trim(both ' ' from to_char(nvl(Avc.Total, 0),'9,999,999')) as \"AvcTotal\"" +
											  "		 ,trim(both '0' from to_char(nvl(Avc.Total/Lta.Amount*100, 0),'90.99'))||'%' as \"AvcVsLta\"" +
											  		",trim(both '0' from to_char(nvl((Avc.Total/Lta.Amount)*100, 0) + Pension.PensionVsLta,'90.99'))||'%' as \"TotalVsLta\" " +
											  "from (select sum(AVC_history.AH10P) Total " +
											  "		 from AVC_history, mp_fund " +
											  "		 where mp_fund.FUNDCODE = AVC_history.AH09X " +
											  "            AND AVC_history.BGROUP = :bgroup" +
											  "			   AND AVC_history.REFNO = :refno" +
											  "			   AND AVC_history.AH01X in ('AVC','AVER') ) Avc, " +
											  "		( select trim(to_char(rf.REVFAC, '999999999.99')) as Amount " +
											  "		  from (select to_number(to_char(sysdate, 'YYYY'),'9999') year" +
											  "				from dual " +
											  "				where to_char(sysdate, 'MMDD') >= to_number('0406', '9999')" +
											  "		  		union " +
											  "		  		select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year " +
											  "		  		from dual " +
											  "				where to_char(sysdate, 'MMDD') < to_number('0406', '9999')) taxyear, " +
											  "		       revfac rf" +
											  "		  where rf.FACTYPE = 'LTAA' " +
											  "			    and rf.YOE = taxyear.year) Lta, " +
											  "		( select nvl(schben.ASD62R, 0) PensionVsLta " +
											  "       from basic, add_static_data schben" +
											  "		  where basic.bgroup = :bgroup" +
											  "				and basic.refno = :refno" +
											  "				and schben.bgroup (+) = basic.bgroup" +
											  "				and schben.REFNO       (+) = basic.refno " +
											  "				and schben.DATA_TYPE   (+) = 'SCHBEN') Pension;";

	public static final String SQL_BCAC_EXPLANATORY_NOTES = "select decode(nvl(enotes.ASD01R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;accuracy of the data&lt;/h2&gt;&lt;p&gt;" +
																"All the figures shown on your benefit statement are for illustration only. " +
																"Although every effort has been made to ensure the accuracy of the data, this statement does not confer any rights to benefits. " +
																"Your benefits are subject to the provisions of the Burmah Castrol Pension Fund Trust Deed and Rules, " +
																"any changes in legislation and, if you have benefits under the BP Supplementary Pension Plan (SPP), " +
																"the Deed governing the SPP. &lt;/p&gt;') as ASD01R, " +
												  "       decode(nvl(enotes.ASD02R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;final pensionable pay&lt;/h2&gt;&lt;p&gt;" +
												  				"For the purposes of this statement, your final pensionable pay is the average of your base pay over the last 12 months, " +
												  				"less the average of the basic State pension over the last 12 months. &lt;/p&gt;') as ASD02R, " +
												  "		  decode(nvl(enotes.ASD03R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;final pensionable pay&lt;/h2&gt;&lt;p&gt;" +
												  				"For the purposes of this statement, your final pensionable pay is the average of your base pay over the last 12 months. &lt;/p&gt;') as ASD03R, " +
												  "		  decode(nvl(enotes.ASD04R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;transfers into the Fund&lt;/h2&gt;&lt;p&gt;" +
												  				"If you have transferred any final salary benefits from a previous pension arrangement into the Fund, " +
												  				"these benefits will be included in this forecast. &lt;/p&gt;') as ASD04R," +
												  " 	  decode(nvl(enotes.ASD05R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;retained benefits&lt;/h2&gt;&lt;p&gt;" +
												  				"Any benefits you may have in other pension arrangements outside of BP are not taken into account when calculating your benefits shown in this forecast. " +
												  				"If you do have any pension benefits outside of BP, " +
												  				"your Burmah Castrol pension benefits may be reduced when you come to leave or retire. &lt;/p&gt;') as ASD05R," +
												  "		  decode(nvl(enotes.ASD06R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;additional voluntary contributions (AVCs) &lt;/h2&gt;&lt;p&gt;" +
												  				"This forecast does not include contributions or benefits held in any of the Fund''s AVC schemes or freestanding AVCs. " +
												  				"If you have made AVCs, you will receive a separate statement. &lt;/p&gt;') as ASD06R," +
												  " 	  decode(nvl(enotes.ASD07R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;pensions on divorce&lt;/h2&gt;&lt;p&gt;" +
												  				"This forecast does not include any divorce orders that may be applied to your pension. &lt;/p&gt;') as ASD07R, " +
												  "		  decode(nvl(enotes.ASD08R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;your benefits at retirement&lt;/h2&gt;&lt;p&gt;" +
												  				"For your estimated benefits at retirement, we have assumed that you will continue in the Burmah Castrol Pension Fund until you reach age 60 or 65. " +
												  				"We have assumed that your final pensionable pay and current pension build-up rate will remain unchanged over these periods. &lt;/p&gt;') as ASD08R," +
												  "		  decode(nvl(enotes.ASD09R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;pension supplement&lt;/h2&gt;&lt;p&gt;" +
												  				"If you take your pension before your State pension age, you will be paid a pension supplement, in addition to your main pension. " +
												  				"This supplement is designed to boost your income until your State pension comes into payment, at which time payment of the supplement will cease. &lt;/p&gt;&lt;p&gt;" +
												  				"If you do not take your pension before your State pension age, the supplement will not be paid. &lt;/p&gt;') as ASD09R," +
												  "		  decode(nvl(enotes.ASD10R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;if you leave the Company&lt;/h2&gt;&lt;p&gt;" +
												  				"If you leave the Company, you can leave your benefits in the Fund until you retire, known as ''deferring your pension''. " +
												  				"Your benefits will increase in line with the Retail Prices Index, up to a maximum of 5% a year. " +
												  				"Alternatively, you may be able to transfer the value of your benefits to another pension arrangement. &lt;/p&gt;') as ASD10R,  " +
												  "		  decode(nvl(enotes.ASD11R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;the lifetime allowance&lt;/h2&gt;&lt;p&gt;" +
												  				"Your statement shows the value of your current final salary benefits as a percentage of the standard lifetime allowance for the 2007-08 tax year. " +
												  				"The lifetime allowance is set by the Government and can change from year to year. &lt;/p&gt;&lt;p&gt;" +
												  				"It is important that you keep track of your pension benefits from all sources " +
												  				"(including employments before joining Burmah Castrol and any private arrangements you may have) in relation to the lifetime allowance. " +
												  				"If you have any pension benefits from a previous employer or another provider, " +
												  				"you will need to add these to the value of your Burmah Castrol Pension Fund benefits to get a more accurate view of your position against the lifetime allowance. &lt;/p&gt;') as ASD11R," +
												  "		  decode(nvl(enotes.ASD12R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;Supplementary Pension Plan&lt;/h2&gt;&lt;p&gt;" +
												  				"If the Company has invited you to join the BP Supplementary Pension Plan (SPP), any pension benefits (on retirement or death) " +
												  				"in excess of the capped benefits payable from the Burmah Castrol Pension Fund will be paid through the SPP. " +
												  				"This will also include any Burmah Castrol final salary benefits you earned in a particular year, the value of which is in excess of the annual allowance (&pound;225,000 for the 2007-08 tax year). " +
												  				"You may take your SPP benefits in lump sum form if you wish. &lt;/p&gt;') as ASD12R," +
												  "		  decode(nvl(enotes.ASD13R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;spouse''s or civil partner''s pension&lt;/h2&gt;&lt;p&gt;" +
												  				"Your spouse''s or civil partner''s pension will not be affected by any reduction in your own pension that resulted from exchanging part of your pension for cash or retiring early. " +
												  				"If your spouse or civil partner is more than 20 years younger than you, their pension will be reduced. &lt;/p&gt;&lt;p&gt;" +
												  				"Where applicable, pensions are also payable to qualifying children. &lt;/p&gt;') as ASD13R," +
												  "		  decode(nvl(enotes.ASD14R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;lump sum death in service benefit&lt;/h2&gt;&lt;p&gt;" +
												  				"The lump sum death benefit payable from the Burmah Castrol Pension Fund is allocated at the discretion of the Trustee. " +
												  				"You can let the Trustee know who you would like to receive the money by completing an expression of wishes form (available on https://pensionline.bp.com) " +
												  				"and keeping it regularly updated. &lt;/p&gt;') as ASD14R," +
												  "		  decode(nvl(enotes.ASD15R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;tax-free cash&lt;/h2&gt;&lt;p&gt;" +
												  				"You may be able to take some of your pension benefits as tax-free cash. We will tell you how much tax-free cash is available to you, " +
												  				"when you come to retire. &lt;/p&gt;') as ASD15R," +
												  "		  decode(nvl(enotes.ASD16R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;further information&lt;/h2&gt;&lt;p&gt;" +
												  				"For more information on the calculation and payment of benefits, please refer to the explanatory booklet on https://pensionline.bp.com " +
												  				"or contact the BP Pensions Administration Team. &lt;/p&gt;') as ASD16R," +
												  "		  decode(nvl(enotes.ASD17R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;data protection&lt;/h2&gt;&lt;p&gt;" +
												  				"Under the terms of the Data Protection Act 1998, the Company and the Trustee hold personal details about you for the purposes of administering your pension benefits. " +
												  				"This information will be made available to third party organizations who are working for and on behalf of the Company " +
												  				"or the Trustee for the purposes of pension administration. &lt;/p&gt;') as ASD17R " +
												  "from basic b,add_static_data enotes  " +
												  "where b.bgroup = :bgroup " +
												  "		 and b.refno = :refno " +
												  "		 and enotes.bgroup    (+) = b.bgroup " +
												  "		 and enotes.refno     (+) = b.refno " +
												  "		 and enotes.data_type (+) = 'EXPNOT'";
	
	public static final String SQL_BCAC_STATIC_BENEFIT_STATEMENT = "select  trim(leading '0' from initcap(to_char(schben.ASD50D, 'DD MON YYYY'))) as \"BenefitStatementDate\"" +
												 "		 ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD50R, 0),'9,999,999')) as \"PensionableSalary.Pound\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD51R, 0),'9,999,999')) as \"PensionToDate\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD63R, 0),'9,999,999')) as \"DeathInServiceCash\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as \"DeathInServicePension\"" +
												 		",trim(both ' ' from to_char(nvl(schben.ASD56R, 0),'990.99'))||'%' as \"PensionVsSalary\"" +
												 		",trim(both ' ' from to_char(nvl(schben.ASD62R, 0),'990.99'))||'%' as \"PensionVsLta\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD52R, 0),'9,999,999')) as \"NpaReducedPension\"" +
												 		",trim(both ' ' from to_char(nvl(schben.ASD57R, 0),'990.99'))||'%' as \"NpaPensionvsSalary\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD53R, 0),'9,999,999')) as \"NpaSpousesPension\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD60R, 0),'9,999,999')) as \"NraReducedPension\"" +
												 		",trim(both ' ' from to_char(nvl(schben.ASD59R, 0),'990.99'))||'%' as \"NraPensionvsSalary\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as \"NraSpousesPension\"" +
												 		",nvl(schben.ASD64R, 3) as \"BenefitStatementType\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD54R, 0),'9,999,999')) as \"GrossNpaPension\"" +
												 		",trim(both ' ' from to_char(nvl(schben.ASD58R, 0),'990.99'))||'%' as \"GrossNpaPensionVsSalary\"" +
												 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD55R, 0),'9,999,999')) as \"GrossNpaSpousesPension\"  " +
												 "from basic, add_static_data schben  " +
												 "where basic.bgroup = :bgroup  " +
												 "		and basic.refno = :refno" +
												 "		and schben.bgroup (+) = basic.bgroup" +
												 "		and schben.REFNO (+) = basic.refno" +
												 "		and schben.DATA_TYPE (+) = 'SCHBEN'";
	
	public static final String SQL_SUBS_GENERAL = "select 'pay' as \"SalaryOrPay\"" +
												  "		 ,'fund' as \"SchemeOrFund\"" +
												  		",'final pensionable salary' as \"CurrentSalaryOrPay\"" +
												  		",'final pensionable salary' as \"FinalSalaryOrPay\"" +
												  		",'Final pensionable salary' as \"CurrentSalaryOrPayCaps\"" +
												  		",'Final pensionable salary' as \"FinalSalaryOrPayCaps\"" +
												  		", 3 as \"BenefitStatementType\"  " +
												  "from DUAL ";
	
	public static final String SQL_SUBSAC_AVC_LTA = "select chr(163)||trim(both ' ' from to_char(nvl(Avc.Total, 0),'9,999,999')) as \"AvcTotal\"" +
													"		,trim(both '0' from to_char(nvl(Avc.Total/Lta.Amount*100, 0),'90.99'))||'%' as \"AvcVsLta\"" +
													"		,trim(both '0' from to_char(nvl((Avc.Total/Lta.Amount)*100, 0) + Pension.PensionVsLta,'90.99'))||'%' as \"TotalVsLta\" " +
													"from (select sum(AVC_history.AH10P) Total " +
													"	   from AVC_history, mp_fund   " +
													"	   where mp_fund.FUNDCODE = AVC_history.AH09X " +
													"			 AND AVC_history.BGROUP = :bgroup " +
													"			 AND AVC_history.REFNO = :refno " +
													"			 AND AVC_history.AH01X in ('AVC','AVER')) Avc," +
													"	  (select trim(to_char(rf.REVFAC, '999999999.99')) as Amount " +
													"	   from (select to_number(to_char(sysdate, 'YYYY'),'9999') year " +
													"			 	from dual " +
													"			 	where to_char(sysdate, 'MMDD') >= to_number('0406', '9999') " +
													"		     union " +
													"			 select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year " +
													"			 	from dual " +
													"			 	where to_char(sysdate, 'MMDD') < to_number('0406', '9999')) taxyear" +
													"			,revfac rf" +
													"	   where rf.FACTYPE = 'LTAA' and rf.YOE = taxyear.year) Lta," +
													"	  (select nvl(schben.ASD62R, 0) PensionVsLta " +
													"	   from basic, add_static_data schben " +
													"	   where basic.bgroup = :bgroup" +
													"			 and basic.refno  = :refno " +
													"			 and schben.bgroup (+) = basic.bgroup " +
													"			 and schben.REFNO (+) = basic.refno " +
													"			 and schben.DATA_TYPE (+) = 'SCHBEN') Pension";
	
	public static final String SQL_SUBSAC_EXPLANATORY_NOTES = "select decode(nvl(enotes.ASD01R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;accuracy of the data&lt;/h2&gt;&lt;p&gt;All the figures shown on your benefit statement are for illustration only. Although every effort has been made to ensure the accuracy of the data, this statement does not confer any rights to benefits. Your benefits are subject to the provisions of the Burmah Castrol Pension Fund Trust Deed and Rules, any changes in legislation and, if you have benefits under the BP Supplementary Pension Plan (SPP), the Deed governing the SPP. &lt;/p&gt;') as ASD01R," +
															  " 	  decode(nvl(enotes.ASD02R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;final pensionable pay&lt;/h2&gt;&lt;p&gt;For the purposes of this statement, your final pensionable pay is the average of your base pay over the last 12 months, less the average of the basic State pension over the last 12 months. &lt;/p&gt;') as ASD02R," +
															  " 	  decode(nvl(enotes.ASD03R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;final pensionable pay&lt;/h2&gt;&lt;p&gt;For the purposes of this statement, your final pensionable pay is the average of your base pay over the last 12 months. &lt;/p&gt;') as ASD03R," +
															  "		  decode(nvl(enotes.ASD04R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;transfers into the Fund&lt;/h2&gt;&lt;p&gt;If you have transferred any final salary benefits from a previous pension arrangement into the Fund, these benefits will be included in this forecast. &lt;/p&gt;') as ASD04R," +
															  " 	  decode(nvl(enotes.ASD05R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;retained benefits&lt;/h2&gt;&lt;p&gt;Any benefits you may have in other pension arrangements outside of BP are not taken into account when calculating your benefits shown in this forecast. If you do have any pension benefits outside of BP, your Burmah Castrol pension benefits may be reduced when you come to leave or retire. &lt;/p&gt;') as ASD05R," +
															  "		  decode(nvl(enotes.ASD06R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;additional voluntary contributions (AVCs) &lt;/h2&gt;&lt;p&gt;This forecast does not include contributions or benefits held in any of the Fund''s AVC schemes or freestanding AVCs. If you have made AVCs, you will receive a separate statement. &lt;/p&gt;') as ASD06R," +
															  "		  decode(nvl(enotes.ASD07R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;pensions on divorce&lt;/h2&gt;&lt;p&gt;This forecast does not include any divorce orders that may be applied to your pension. &lt;/p&gt;') as ASD07R," +
															  "		  decode(nvl(enotes.ASD08R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;your benefits at retirement&lt;/h2&gt;&lt;p&gt;For your estimated benefits at retirement, we have assumed that you will continue in the Burmah Castrol Pension Fund until you reach age 60 or 65. We have assumed that your final pensionable pay and current pension build-up rate will remain unchanged over these periods. &lt;/p&gt;') as ASD08R," +
															  "		  decode(nvl(enotes.ASD09R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;pension supplement&lt;/h2&gt;&lt;p&gt;If you take your pension before your State pension age, you will be paid a pension supplement, in addition to your main pension. This supplement is designed to boost your income until your State pension comes into payment, at which time payment of the supplement will cease. &lt;/p&gt;&lt;p&gt;If you do not take your pension before your State pension age, the supplement will not be paid. &lt;/p&gt;') as ASD09R," +
															  "		  decode(nvl(enotes.ASD10R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;if you leave the Company&lt;/h2&gt;&lt;p&gt;If you leave the Company, you can leave your benefits in the Fund until you retire, known as ''deferring your pension''. Your benefits will increase in line with the Retail Prices Index, up to a maximum of 5% a year. Alternatively, you may be able to transfer the value of your benefits to another pension arrangement. &lt;/p&gt;') as ASD10R," +
															  "		  decode(nvl(enotes.ASD11R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;the lifetime allowance&lt;/h2&gt;&lt;p&gt;Your statement shows the value of your current final salary benefits as a percentage of the standard lifetime allowance for the 2007-08 tax year. The lifetime allowance is set by the Government and can change from year to year. &lt;/p&gt;&lt;p&gt;It is important that you keep track of your pension benefits from all sources (including employments before joining Burmah Castrol and any private arrangements you may have) in relation to the lifetime allowance. If you have any pension benefits from a previous employer or another provider, you will need to add these to the value of your Burmah Castrol Pension Fund benefits to get a more accurate view of your position against the lifetime allowance. &lt;/p&gt;') as ASD11R," +
															  "		  decode(nvl(enotes.ASD12R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;Supplementary Pension Plan&lt;/h2&gt;&lt;p&gt;If the Company has invited you to join the BP Supplementary Pension Plan (SPP), any pension benefits (on retirement or death) in excess of the capped benefits payable from the Burmah Castrol Pension Fund will be paid through the SPP. This will also include any Burmah Castrol final salary benefits you earned in a particular year, the value of which is in excess of the annual allowance (&pound;225,000 for the 2007-08 tax year). You may take your SPP benefits in lump sum form if you wish. &lt;/p&gt;') as ASD12R," +
															  "		  decode(nvl(enotes.ASD13R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;spouse''s or civil partner''s pension&lt;/h2&gt;&lt;p&gt;Your spouse''s or civil partner''s pension will not be affected by any reduction in your own pension that resulted from exchanging part of your pension for cash or retiring early. If your spouse or civil partner is more than 20 years younger than you, their pension will be reduced. &lt;/p&gt;&lt;p&gt;Where applicable, pensions are also payable to qualifying children. &lt;/p&gt;') as ASD13R," +
															  "		  decode(nvl(enotes.ASD14R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;lump sum death in service benefit&lt;/h2&gt;&lt;p&gt;The lump sum death benefit payable from the Burmah Castrol Pension Fund is allocated at the discretion of the Trustee. You can let the Trustee know who you would like to receive the money by completing an expression of wishes form (available on https://pensionline.bp.com) and keeping it regularly updated. &lt;/p&gt;') as ASD14R," +
															  "		  decode(nvl(enotes.ASD15R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;tax-free cash&lt;/h2&gt;&lt;p&gt;You may be able to take some of your pension benefits as tax-free cash. We will tell you how much tax-free cash is available to you, when you come to retire. &lt;/p&gt;') as ASD15R," +
															  "		  decode(nvl(enotes.ASD16R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;further information&lt;/h2&gt;&lt;p&gt;For more information on the calculation and payment of benefits, please refer to the explanatory booklet on https://pensionline.bp.com or contact the BP Pensions Administration Team. &lt;/p&gt;') as ASD16R," +
															  "		  decode(nvl(enotes.ASD17R, 0), 0, '&lt;span style=''display=none;''&gt;.&lt;/span&gt;', '&lt;h2&gt;data protection&lt;/h2&gt;&lt;p&gt;Under the terms of the Data Protection Act 1998, the Company and the Trustee hold personal details about you for the purposes of administering your pension benefits. This information will be made available to third party organizations who are working for and on behalf of the Company or the Trustee for the purposes of pension administration. &lt;/p&gt;') as ASD17R " +
															  "from basic b,add_static_data enotes  " +
															  "where b.bgroup = :bgroup " +
															  "		 and b.refno = :refno " +
															  "		 and enotes.bgroup (+) = b.bgroup " +
															  "		 and enotes.refno (+) = b.refno " +
															  "		 and enotes.data_type (+) = 'EXPNOT'";
	
	public static final String SQL_SUBSAC_STATIC_BENEFIT_STATEMENT = "select  trim(leading '0' from initcap(to_char(schben.ASD50D, 'DD MON YYYY'))) as \"BenefitStatementDate\"" +
																	 "		 ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD50R, 0),'9,999,999')) as \"PensionableSalary.Pound\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD51R, 0),'9,999,999')) as \"PensionToDate\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD63R, 0),'9,999,999')) as \"DeathInServiceCash\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as \"DeathInServicePension\"" +
																	 		",trim(both ' ' from to_char(nvl(schben.ASD56R, 0),'990.99'))||'%' as \"PensionVsSalary\"" +
																	 		",trim(both ' ' from to_char(nvl(schben.ASD62R, 0),'990.99'))||'%' as \"PensionVsLta\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD52R, 0),'9,999,999')) as \"NpaReducedPension\"" +
																	 		",trim(both ' ' from to_char(nvl(schben.ASD57R, 0),'990.99'))||'%' as \"NpaPensionvsSalary\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD53R, 0),'9,999,999')) as \"NpaSpousesPension\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD60R, 0),'9,999,999')) as \"NraReducedPension\"" +
																	 		",trim(both ' ' from to_char(nvl(schben.ASD59R, 0),'990.99'))||'%' as \"NraPensionvsSalary\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as \"NraSpousesPension\"" +
																	 		",nvl(schben.ASD64R, 3) as \"BenefitStatementType\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD54R, 0),'9,999,999')) as \"GrossNpaPension\"" +
																	 		",trim(both ' ' from to_char(nvl(schben.ASD58R, 0),'990.99'))||'%' as \"GrossNpaPensionVsSalary\"" +
																	 		",chr(163)||trim(both ' ' from to_char(nvl(schben.ASD55R, 0),'9,999,999')) as \"GrossNpaSpousesPension\"  " +
																	 "from basic,add_static_data schben  " +
																	 "where basic.bgroup = :bgroup " +
																	 "		and basic.refno = :refno " +
																	 "		and schben.bgroup (+) = basic.bgroup " +
																	 "		and schben.REFNO (+) = basic.refno " +
																	 "		and schben.DATA_TYPE (+) = 'SCHBEN'";
}
