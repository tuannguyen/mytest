/**
 * 
 */
package cmg.bp.pl.migration.bean;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author BachLe
 *
 */
public class MemberBean {
	private static final long serialVersionUID = 1L;
	
	public static final String MEMBER_REFNO   = "Refno";
	public static final String MEMBER_GROUP   = "BGROUP";
	public static final String MEMBER_SCHEME  = "SchemeCode";
	public static final String MEMBER_STATUS  = "MembershipStatusRaw";
	public static final String CALC_NAME	  = "CALC_NAME";
	
	
	public static String NraPension = "NraPension";
    public static String NpaPension = "NpaPension";
    public static String Pension = "Pension";
    public static String PensionToDate = "PensionToDate";
    public static String NraUnreducedPension = "NraUnreducedPension";
    public static String NpaUnreducedPension = "NpaUnreducedPension";
    public static String UnreducedPension = "UnreducedPension";
    public static String NraReducedPension = "NraReducedPension";
    public static String NpaReducedPension = "NpaReducedPension";
    public static String ReducedPension = "ReducedPension";
    public static String NraSpousesPension = "NraSpousesPension";
    public static String NpaSpousesPension = "NpaSpousesPension";
    public static String SpousesPension = "SpousesPension";
    public static String NraCashLumpSum = "NraCashLumpSum";
    public static String NraCashLumpSumCurrency = "NraCashLumpSumCurrency";
    public static String NpaCashLumpSum = "NpaCashLumpSum";
    public static String NpaCashLumpSumCurrency = "NpaCashLumpSumCurrency";
    public static String CashLumpSum = "CashLumpSum";
    public static String CashLumpSumCurrency = "CashLumpSumCurrency";
    public static String NraMaximumCashLumpSum = "NraMaximumCashLumpSum";
    public static String NpaMaximumCashLumpSum = "NpaMaximumCashLumpSum";
    public static String NraMaximumCashLumpSumExact = "NraMaximumCashLumpSumExact";
    public static String NpaMaximumCashLumpSumExact = "NpaMaximumCashLumpSumExact";
    public static String MaximumCashLumpSum = "MaximumCashLumpSum";
    public static String MaximumCashLumpSumExact = "MaximumCashLumpSumExact";
    
    public static String NraPensionWithChosenCash = "NraPensionWithChosenCash";
    public static String NpaPensionWithChosenCash = "NpaPensionWithChosenCash";
    public static String PensionWithChosenCash = "PensionWithChosenCash";
    public static String NraPensionWithMaximumCash = "NraPensionWithMaximumCash";
    public static String NpaPensionWithMaximumCash = "NpaPensionWithMaximumCash";
    public static String PensionWithMaximumCash = "PensionWithMaximumCash";

    public static String Lta = "Lta";
    public static String PensionVsLta = "PensionVsLta";
    public static String TotalVsLta = "TotalVsLta";
    public static String NpaPensionvsSalary = "NpaPensionvsSalary";
    public static String NraPensionvsSalary = "NraPensionvsSalary";
    public static String UnreducedPensionVsSalary = "UnreducedPensionVsSalary";
    public static String ReducedPensionVsSalary = "ReducedPensionVsSalary";
    public static String Fps = "FPS";
    public static String BasicPs = "BasicPs";
    public static String PensionVsSalary = "PensionVsSalary";
    public static String PensionableSalary="PensionableSalary";
    public static String AvcVsLta ="AvcVsLta";
    public static String totalAvc ="totalAvc";
    public static String overfundIndicator ="overfundIndicator";
    public static String veraIndicator = "veraIndicator";
    public static String NraOverfundIndicator ="NraOverfundIndicator";
    public static String NpaOverfundIndicator ="NpaOverfundIndicator";
    public static String NraVeraIndicator = "NraVeraIndicator";
    public static String NpaVeraIndicator = "NpaVeraIndicator";
    
    
    public static String AccruedPension="AccruedPension";
    public static String EgpCash="EgpCash";
    public static String SrpCash="SrpCash";
    public static String TaxFreeCash="TaxFreeCash";
    public static String TaxableCash="TaxableCash";
    public static String TaxPayable="TaxPayable";
    public static String RrReducedPension="RrReducedPension";
    public static String RrSpousesPension="RrSpousesPension";
    public static String RrMaxLumpSum="RrMaxLumpSum";
    public static String RrResidualPension="RrResidualPension";
    public static String RrAccruedPension="RrAccruedPension";
    public static String RedundancyDate="RedundancyDate";
    public static String OverMinRetireAge="OverMinRetireAge";
    
    
    public static String DeathInServiceCash = "DeathInServiceCash";
    public static String DeathInServicePension = "DeathInServicePension";

	/** Attribute values stored in this Member */
	public Map<String, String> valueMap;
	
	public Map<String, String> calcMap;
	
	/** default constructor */
	public MemberBean() {
		valueMap = new HashMap<String, String>();
		calcMap = new HashMap<String, String>();
	}

	/**
	 * constructor 
	 * @param refno
	 * @param bgroup
	 */
	public MemberBean(String refno, String bgroup) {

		valueMap = new HashMap<String, String>(); //Collections.synchronizedMap(new HashMap<String, String>());

		valueMap.put(MEMBER_REFNO, refno);
		valueMap.put(MEMBER_GROUP, bgroup);
		calcMap = new HashMap<String, String>();
	}

	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * @param valueMap
	 */
	public void replaceValueMap(Map<String, String> valueMap) {
		this.valueMap = valueMap;
	}

	
	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * NOTE: This does a deep copy of the contents.
	 * 
	 * @param valueMap
	 */
	public Map<String, String> getValueMap() {
		return this.valueMap;
	}
	
	public Map<String, String> getCalcMap() {
		return this.calcMap;
	}
	
	/**
	 * Update the existing member value and replace with a new set.
	 * 
	 * NOTE: This does a deep copy of the contents.
	 * 
	 * @param valueMap
	 */
	public Map<String, String> getCopyOfValueMap() {
		Map deepCopyValueMap = Collections.synchronizedMap(new HashMap<String, String>());
		
		//copy each of the items in the map not the references to memory
		//http://www.java2s.com/Code/Java/Language-Basics/DeepCopyTest.htm
		
        Iterator iterator = this.valueMap.keySet().iterator();
        while (iterator.hasNext()) {        	
        	Object key = iterator.next();
        	deepCopyValueMap.put(new String(""+key), new String(""+ valueMap.get(key)));
        }
		
		return deepCopyValueMap;
	}

	/**
	 * get a value for an Attribute, this is named this way as we have migrated the ofshore code
	 * 
	 * @param key
	 * @return String value for the attribute
	 */
	public String getMemberData(String key) {

		return get(0, key);

	}
	
	public String getCalcData(String key) {
		return get(1, key);
	}

	/**
	 * get a value for an Attribute
	 * 
	 * @param key
	 * @return String value for the attribute
	 */

	public String get(int map, String key) {
		Object obj = null;
		if (map == 1) {
			if(calcMap.containsKey(key)){
				obj = calcMap.get(key);	
				return obj == null ? new String("") : String.valueOf(obj);
				
			} else{
				return null;
			}
		} else {
			if(valueMap.containsKey(key)){
				obj = valueMap.get(key);	
				return obj == null ? new String("") : String.valueOf(obj);
				
			} else{
				return null;
			}
		}
		
	}

	/**
	 * store a value for a given key
	 * 
	 * @param key
	 * @param value
	 */
	public void put(String key, String value) {
		valueMap.put(key, value);
	}
	
	public void putCalcVal(String key, String value) {
		calcMap.put(key, value);
	}

	/**
	 * get the Refno
	 * 
	 * @return refno as a String
	 */
	public String getRefno() {
		return getMemberData(MEMBER_REFNO);
	}

	/**
	 * get the bgroup
	 * 
	 * @return bgroup
	 */
	public String getBgroup() {
		return getMemberData(MEMBER_GROUP);
	}

	/**
	 * over ride to show the attribute value contents
	 */
	public String toString() {
		return this.valueMap.toString();
	}
	
	/**
	 * deep copy the values of one map into another
	 * 
	 */
	public void addValues(Map tmpValues) {

		synchronized(this){
							
			//copy each of the items in the map not the references to memory
			//http://www.java2s.com/Code/Java/Language-Basics/DeepCopyTest.htm
			Iterator iterator = tmpValues.keySet().iterator();
			
	       // Iterator iterator = this.valueMap.keySet().iterator();
	        while (iterator.hasNext()) {        	
	        	Object key = iterator.next();
	        	valueMap.put(new String(""+key), new String(""+tmpValues.get(key)));
	        	//System.out.println(" MemberDao addValues ["+key+"], this="+this.get(""+key)+", memTemp="+tmpValues.get(""+key));
	        }
	        //System.out.println(" >>>>>> ["+Environment.MEMBER_REFNO+"], this="+this.get(""+Environment.MEMBER_REFNO)+", memTemp="+memTemp.get(""+Environment.MEMBER_REFNO));
			//deep copy the contents
		}
	}
	
	public MemberBean clone() {
		MemberBean newObj = new MemberBean();
		newObj.addValues(valueMap);
		newObj.addValues(calcMap);
		return newObj;
	}
}
