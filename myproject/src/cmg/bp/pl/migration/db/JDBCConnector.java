/**
 * 
 */
package cmg.bp.pl.migration.db;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.pool.OracleDataSource;

/**
 * @author BachLe
 *
 */
public class JDBCConnector {
	
	private static Properties       pros                  = new Properties();
	private static Connection       conn                  = null;
	private static OracleDataSource ods                   = null;
	
	public static final String CONNECTION_URL      = "url";
	public static final String CONNECTION_USERNAME = "username";
	public static final String CONNECTION_PASSWORD = "password";
	
	private String userName = "";
	private String password = "";
	private String IP = "";
	
	public static final String configFile          = "configure.properties";
	
	public JDBCConnector() throws Exception {
		FileInputStream file = new FileInputStream(configFile);
		pros.load(file);
		
		String url = pros.getProperty(CONNECTION_URL);
		url = url.substring(url.indexOf("@")+1);
		String ip = url.substring(0, url.indexOf(":"));
		
		IP = ip;
		userName = pros.getProperty(CONNECTION_USERNAME);
		password = pros.getProperty(CONNECTION_PASSWORD);
	
		//initDataSource();
	}
	
	private static void initDataSource() throws SQLException {
		ods = new OracleDataSource();
		ods.setURL(pros.getProperty(CONNECTION_URL));
        ods.setUser(pros.getProperty(CONNECTION_USERNAME));
        ods.setPassword(pros.getProperty(CONNECTION_PASSWORD));
        ods.setDriverType("thin");
        
        conn = ods.getConnection();
	}
	
	public static Connection getConnection() throws SQLException {
		if (conn == null) {
			if (ods == null) {
				initDataSource();
			}
			conn = ods.getConnection();
		}
		return conn;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * @param iP the iP to set
	 */
	public void setIP(String iP) {
		this.IP = iP;
	}

	/**
	 * @return the iP
	 */
	public String getIP() {
		return this.IP;
	}
}
