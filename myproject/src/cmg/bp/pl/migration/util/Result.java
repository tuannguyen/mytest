/**
 * 
 */
package cmg.bp.pl.migration.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cmg.bp.pl.migration.bean.MemberBean;

/**
 * @author BachLe
 *
 */
public class Result {
	
	public static void saveResult(String outputFile, List<MemberBean> memberList, int colNameMaxLength) {
		StringBuffer output = new StringBuffer();
		Calendar cal = Calendar.getInstance();
		String day = cal.get(Calendar.DAY_OF_MONTH)>9?""+cal.get(Calendar.DAY_OF_MONTH):"0"+cal.get(Calendar.DAY_OF_MONTH);
		String mon = cal.get(Calendar.MONTH)+1>9?""+(cal.get(Calendar.MONTH)+1):"0"+(cal.get(Calendar.MONTH)+1);
		String hou = cal.get(Calendar.HOUR_OF_DAY)>9?""+cal.get(Calendar.HOUR_OF_DAY):"0"+cal.get(Calendar.HOUR_OF_DAY);
		String min = cal.get(Calendar.MINUTE)>9?""+cal.get(Calendar.MINUTE):"0"+cal.get(Calendar.MINUTE);
		String sec = cal.get(Calendar.SECOND)>9?""+cal.get(Calendar.SECOND):"0"+cal.get(Calendar.SECOND);
		output.append("Date: "+day+"/"+mon+"/"+cal.get(Calendar.YEAR)+"\n");
		output.append("Time: "+hou+":"+min+":"+sec+"\n\n");
		output.append("----------------------------------\n");
		
		int spc = colNameMaxLength>9? colNameMaxLength-9+5: 5;
		output.append("MEMBER          DATA               ATTRIBUTE");
		for (int idx=0; idx<spc; idx++) {
			output.append(' ');
		}
		output.append("VALUE\n");
		
		for (int l=0; l<memberList.size(); l++) {
			MemberBean member = memberList.get(l);
			Map<String,String> values = member.valueMap;
			Set<String> keys = values.keySet();
			Map<String,String> calcVals = member.calcMap;
			Set<String> calcKeys = calcVals.keySet();
			
			String grf = member.getMemberData(MemberBean.MEMBER_GROUP)+"-"+member.getMemberData(MemberBean.MEMBER_REFNO);
			keys.remove(MemberBean.MEMBER_GROUP);
			keys.remove(MemberBean.MEMBER_REFNO);
			
			//System.out.println("save result for user: "+grf);
			String[] valueLines = new String[keys.size()];
			int count = 0;
			for (Iterator<String> iter = keys.iterator(); iter.hasNext();) {
				StringBuffer line = new StringBuffer();
				String key = iter.next();
				int blank = colNameMaxLength-key.length() + 5;
				//output.append("\n"+grf+"     MEMDATA            "+key.trim());
				line.append(grf+"     MEMDATA            "+key.trim());
				for (int x=0; x<blank; x++) {
					//output.append(' ');
					line.append(' ');
				}
				//output.append(values.get(key));
				line.append(values.get(key));
				valueLines[count++] = line.toString();
			}
			
			
			String[] calcLines = new String[calcKeys.size()];
			count = 0;
			for (Iterator<String> it = calcKeys.iterator(); it.hasNext(); ) {
				String key = it.next();
				int idx = key.indexOf("_");
				String calcAtt = key.substring(0, idx);
				String calcName = key.substring(idx+1);
				int blank = colNameMaxLength-calcAtt.length() + 5;
				//output.append("\n"+grf+"     "+calcName+"          "+calcAtt);
				StringBuffer buf = new StringBuffer();
				buf.append(grf+"     "+calcName+"          "+calcAtt);
				for (int x=0; x<blank; x++) {
					//output.append(' ');
					buf.append(' ');
				}
				//output.append(calcVals.get(key));
				buf.append(calcVals.get(key));
				calcLines[count++] = buf.toString();
			}
			
			//sort calculation result by calculation name
			Arrays.sort(valueLines);
			Arrays.sort(calcLines);
			for (count = 0; count<valueLines.length; count++) {
				output.append("\n"+valueLines[count]);
			}
			for (count = 0; count<calcLines.length; count++) {
				output.append("\n"+calcLines[count]);
			}
			
			output.append("\n");
		}
		
		//write the result
		try {
			File out = new File(outputFile);
			BufferedWriter writer = new BufferedWriter(new FileWriter(out));
			writer.write(output.toString());
			writer.close();
			
			/*
			File fo = new File(sqlFile);
			BufferedWriter wt = new BufferedWriter(new FileWriter(fo));
			wt.write(sqlOut.toString());
			wt.close();
			*/
			
			System.out.println("Output file: "+out.getAbsolutePath());
		} catch (FileNotFoundException fe) {
			System.out.println("File "+outputFile+" not found");
		} catch (IOException ie) {
			System.out.println("Exception: "+ie.toString());
		}
	}
}
