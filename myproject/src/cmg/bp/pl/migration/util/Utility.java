/**
 * 
 */
package cmg.bp.pl.migration.util;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
/**
 * @author BachLe
 *
 */
public class Utility {
	public static final int DEFAULT_INTVALUE=0;
	public static final double DEFAULT_DOUBLEVALUE=-1.0;
	public static final double DEFAULT_DOUBLEZEROVALUE=0.0;
	
	public static int YEARS_OF_SERVICE = 20;
	
	public static final String DELIMITER = " `~!@#$%^&*()-+=\\|{}[]:;'\"<,>.?/";
	
	public static String EMPTY_STRING = "";
	
	public static java.sql.Date getDate(String value) {
		//java.sql.Date date=null;
		java.util.Date d = null;
		java.sql.Date date=null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		try {
			if(value != null){
				d = formatter.parse(value);				
				date=new java.sql.Date(d.getTime());
			}
			return date;
		} catch (Exception e) {
			// TODO: handle exception
			date=new java.sql.Date(System.currentTimeMillis());
			return date;
			//throw e;
		}
	}
	
	public static  int getInt(String value){
    	try {
			int result=Integer.parseInt(value);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			
			return DEFAULT_INTVALUE;
			
		}
    	
    }
    public static double getDouble(String value) {
    	try{
    		double result=Double.parseDouble(value);
    		return result;
    	}catch (Exception e) {
    		return DEFAULT_DOUBLEVALUE;
    		
		}
    	
    }
    
    /**
	 * Gives you back a date object that has been reset to the first day of the next
	 * month.  The time ascepct is left at it so if you call this method at 16:07 on the 
	 * 21st October 2003 then the retruned date object will be "Wed Oct 01 16:07:41 BST 2003"
	 * @return The Date object for the first day of this month.
	 */
	public static Date getFirstDayOfNextMonth() {
		GregorianCalendar cal = new GregorianCalendar(); // now
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
		return cal.getTime();
	}
	
	/**
	 * Returns the age given their date of birth
	 * @param dob
	 * @return
	 */
	public static float getYearsBetweenAsFloat(Date startDate, Date endDate) throws Exception {
		if (startDate == null || endDate == null) {
			String msg = "Can't work with a NULL date!";
			throw new Exception(msg);
		}

		if (startDate.getTime() > endDate.getTime()) {
			String msg = "DateUtil.getYearsBetween: the end date seems to be before the start date!";
			throw new Exception(msg);
		}

		GregorianCalendar start = new GregorianCalendar();
		start.setTime(startDate);
		GregorianCalendar end = new GregorianCalendar();
		end.setTime(endDate);
		// Get number of years between these two dates
		int diff = end.get(Calendar.YEAR) - start.get(Calendar.YEAR);

		// Add the tentative num of years to the start date to get this year's anniversary
		start.add(Calendar.YEAR, diff);

		// If this year's anniversary has not happened yet, subtract one from num of years
		if (end.before(start)) {
			diff--;
			start.add(Calendar.YEAR, -1);
		}

		// diff is the number of total years, get days left over and convert to % of year
		float days = (float)(getElapsedDays(start, end) / 365.25);

		// add together total amount and return
		return (diff) + days;
	}
	
	/**
	 * The definition of time unit change is relatively simple: If you are counting days, you simply 
	 * count the number of times the date has changed. For example, if something starts on the 15th and 
	 * ends on the 17th, 2 days have passed. (The date changed first to the 16th, then to the 17th.) 
	 * Similarly, if a process starts at 3:25 in the afternoon and finishes at 4:10 p.m., 1 hour has 
	 * passed because the hour has changed once (from 3 to 4).
	 * Libraries often calculate time in this manner. For example, if I borrow a book from my library, 
	 * I don't need to have the book in my possession for a minimum of 24 hours for the library to consider 
	 * it borrowed for one day. Instead, the day that I borrow the book is recorded on my account. As soon 
	 * as the date switches to the next day, I have borrowed the book for one day, even though the amount 
	 * of time is often less than 24 hours. 
	 * @param g1
	 * @param g2
	 * @return
	 */
	public static long getElapsedDays(GregorianCalendar g1, GregorianCalendar g2) {
		long elapsed = 0;
		GregorianCalendar gc1, gc2;

		if (g2.after(g1)) {
			gc2 = (GregorianCalendar) g2.clone();
			gc1 = (GregorianCalendar) g1.clone();
		} else {
			gc2 = (GregorianCalendar) g1.clone();
			gc1 = (GregorianCalendar) g2.clone();
		}

		gc1.clear(Calendar.MILLISECOND);
		gc1.clear(Calendar.SECOND);
		gc1.clear(Calendar.MINUTE);
		gc1.clear(Calendar.HOUR_OF_DAY);

		gc2.clear(Calendar.MILLISECOND);
		gc2.clear(Calendar.SECOND);
		gc2.clear(Calendar.MINUTE);
		gc2.clear(Calendar.HOUR_OF_DAY);

		while (gc1.before(gc2)) {
			gc1.add(Calendar.DATE, 1);
			elapsed++;
		}
		
		return elapsed;
	}
	
	/**
    * @param cash
    * @return
    */
	public static String toLowestPound(double cash)  {
		int	 ncash = (int)cash;
		return toCurrencyNoDP("�", ncash);
	}
	
	/**
	    * @param currency
	    * @param cash
	    * @return
	    */
	public static String toCurrencyNoDP(String currency, double cash) {
		DecimalFormat format = new DecimalFormat("#,##0");
		FieldPosition f = new FieldPosition(0);
		StringBuffer s = new StringBuffer();
		format.format(cash, s, f);
		return currency + s.toString();
    }
	
	/**
	    * To no decimal places
	    * @param num
	    * @return
	    */
	public static String toNearestOne(double num)  {
		double rounded = Math.round(num);
		return ""+rounded;
	}
	
	/**
    * @param cash
    * @return
    */
	public static String toNearestPound(double cash)  {
		double ncash = Math.round(cash);
		return toCurrencyNoDP("�", ncash);
	}
	  
	/**
    * Returns the number dropped to the nearest ten
    * @param value
    * @return
    */
	public static double toLowestThousand(double value) {
		int result = (int)(value/1000.0);
		return result * 1000;
	}
	
	public static String to2DpPercentage(double dbl) {
		DecimalFormat format = new DecimalFormat("0.00");
		FieldPosition f = new FieldPosition(0);
		StringBuffer s = new StringBuffer();
		format.format(dbl, s, f);
		return s.toString()+"%";
	}
	   
	public static String getString(Object val){
		if(val == null){
			
			val = new String("");
			
		}
		return val.toString();
	}
	
	public static String Escape(String val){
		StringTokenizer tokens = new StringTokenizer(val, DELIMITER);
		StringBuffer buffer = new StringBuffer();
		
		while(tokens.hasMoreTokens()){
			buffer.append(tokens.nextToken()).append("_");
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		return buffer.toString();
	}
}
