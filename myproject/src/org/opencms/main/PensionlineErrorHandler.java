/*
 * Copyright (c) CMG Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of CMG
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with CMG.
 */
package org.opencms.main;

import org.apache.commons.logging.Log;

import org.opencms.configuration.CmsConfigurationManager;

import org.opencms.jsp.CmsJspLoginBean;
import org.opencms.jsp.util.CmsJspStatusBean;

import org.opencms.util.CmsStringUtil;

import java.io.IOException;

import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Please enter a short description for this class.
 *
 * <p>Optionally, enter a longer description.</p>
 *
 * @author	Binh Nguyen
 * @version	1.0.1 December 21, 2009
 */
public class PensionlineErrorHandler extends OpenCmsServlet {

    //~ Static fields/initializers ---------------------------------------------

    /** Serial version UID required for safe serialization. */
    private static final long serialVersionUID = 6316004893684482816L;

    /** The Log object. */
    public static final Log LOG = CmsLog.getLog(CmsJspLoginBean.class);

    //~ Methods ----------------------------------------------------------------

    /**
     * The doGet method.
     *
     * @param   req  the request
     * @param   res  the response
     *
     * @throws  IOException       the generic exception
     * @throws  ServletException  if having any errors while processing the
     *                            servlet
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
    	
        // check the error status
        Integer errorStatus = (Integer) req.getAttribute(
                CmsJspStatusBean.ERROR_STATUS_CODE);
        
        if (errorStatus != null) {
            String errorMessage = (String) req.getAttribute(
                    CmsJspStatusBean.ERROR_MESSAGE);
            String errorDes = CmsStringUtil.escapeHtml(Messages.get()
                    .getBundle().key(Messages.ERR_OPENCMS_NOT_INITIALIZED_2,
                        errorStatus, errorMessage));
            
            String errorRef = ("" + Math.random()).substring(0, 7);
            
            req.getSession().setAttribute("errorStatus",
                errorStatus.toString());
            req.getSession().setAttribute("errorRef", errorRef);
            req.getSession().setAttribute("errorMessage", errorMessage);
            req.getSession().setAttribute("errorDes", errorDes);
            
            
            LOG.error("PensionlineErrorHandler handle error: " + errorStatus.intValue());
//            if (errorStatus.intValue() != 404) {
//                LOG.info(errorRef + ":" + errorDes);
//            }
//            CmsConfigurationManager configMana = 
//            							OpenCmsCore.getInstance()
//            							.getConfigurationManager();
//            Map _map = configMana.getConfiguration();

            // Redirects
            //res.sendRedirect(_map.get("context.full.name") + "/errorPage.jsp");
            res.sendRedirect("/content/pl/errorPage.jsp");
        } else {
            // no status code set, this is an invalid request
            res.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    /**
     * @see  javax.servlet.Servlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) {
        // override super class to avoid default initialization
    }
}
