<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.*"%>
<%@ page import="java.util.*"%>
<%@ page import="org.opencms.xml.content.*"%>


<%

	// Create a JSP action element

	CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);

	// get actual folder

	String currentFld = cms.info("opencms.request.folder");

	// debug

	out.println("Current folder=" + currentFld);

	CmsObject cmsObject = cms.getCmsObject();

	List resources = cmsObject.getResourcesInFolder(currentFld, CmsResourceFilter.DEFAULT_FILES);
	
	Iterator iter = resources.iterator();
	
	for(;iter.hasNext();){
	
		CmsResource r = (CmsResource)iter.next();
	
		if(r.getRootPath().indexOf("html") > 0){

			CmsXmlContent content = CmsXmlContentFactory.unmarshal(cms.getCmsObject(), r, request);
%>
	<br><br><h1><%= content.toString()%></h1>
<%

		}

	}

%>
