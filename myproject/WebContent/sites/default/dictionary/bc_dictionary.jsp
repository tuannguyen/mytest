<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<cms:include property="template" element="head" />
<cms:editable />

<div class="element">

<cms:contentload collector="allInFolderPriorityTitleDesc" param="/dictionary/BC/dic_${number}.html|bp_pensionline_dictionary" editable="true">

<c:set var="localVar" >
    <cms:contentshow element="new_edited" />
</c:set>

<c:if test="${localVar ==true}">
<div class="element" style="background-color: #ffc ">
<h3><cms:contentshow element="Title" /></h3>
<cms:contentshow element="Definition" />
</div>
<br />
</c:if>

<c:if test="${localVar !=true}">
<div class="element">
<h3 name ="<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Title" /></h3>
<cms:contentshow element="Definition" />
</div>
<br />
</c:if>

</cms:contentload>

</div>
<cms:include property="template" element="foot" />