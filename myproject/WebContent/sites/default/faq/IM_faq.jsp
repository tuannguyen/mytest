<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<cms:include property="template" element="head" />
<cms:editable />

<cms:include page="./IM_header.html" element="bptitle" />
<cms:include page="./IM_header.html" element="bpblank" />

<!-- questions and answers -->
<div class="faq_questions">
<cms:contentload collector="allInFolderNavPos" param="/faq/IM/faq_${number}.html|bp_pensionline_faq" editable="true">

<div class="element" name='top' id='top' >
<p>
  <a href ="#<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Question" /></a>
</p>
</div>

</cms:contentload>
</div>


<!-- questions and answers -->
<div class="faq_list">
<cms:contentload collector="allInFolderNavPos" param="/faq/IM/faq_${number}.html|bp_pensionline_faq" editable="true">

<c:set var="localVar" >
    <cms:contentshow element="new_edited" />
</c:set>
<c:if test="${localVar ==true}">

<div class="element" style="background-color: #ffc ">

  <h3><a name ="<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Question" /></a></h3>
  <p>
    <cms:contentshow element="Answer" />
	<div class="totop"><a href="#top">Back to top of page</a></div>
  </p>
</div>
</c:if>
<c:if test="${localVar !=true}">

<div class="element">

  <h3><a name ="<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Question" /></a></h3>
  <p>
    <cms:contentshow element="Answer" />
	<div class="totop"><a href="#top">Back to top of page</a></div>
  </p>
</div>
</c:if>
</cms:contentload>
</div>

<cms:include property="template" element="foot" />
