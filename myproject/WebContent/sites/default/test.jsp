<%@ page import="java.util.*,org.opencms.jsp.*,org.opencms.workplace.*" %>
<%
// START - Login workplace messages
        /** The locale to use for display, this will not be the workplace locale, but the browser locale. */
        java.util.Locale m_locale = org.opencms.main.OpenCms.getWorkplaceManager().getDefaultLocale();
        // display login message if required
        org.opencms.db.CmsLoginMessage loginMessage = org.opencms.main.OpenCms.getLoginManager().getLoginMessage();

        String msgLogin = "";
	boolean msgLoginAvailable = false;
       
        if ((loginMessage != null) && (loginMessage.isActive())) {
            msgLoginAvailable = true;
            
            if (loginMessage.isLoginForbidden()) {
                // login forbidden for normal users, current user must be Administrator
                msgLogin= org.opencms.workplace.Messages.get().container(
                        org.opencms.workplace.Messages.GUI_LOGIN_SUCCESS_WITH_MESSAGE_2,
                    loginMessage.getMessage(),
                    new java.util.Date(loginMessage.getTimeEnd())).key(m_locale);
            } else {
                // just display the message
                msgLogin= loginMessage.getMessage();
            }
            msgLogin=(org.opencms.util.CmsStringUtil.escapeJavaScript(msgLogin));
	}
	out.println(msgLoginAvailable+"-"+msgLogin);
%>