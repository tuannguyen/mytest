<%@ page pageEncoding="UTF-8" %><%@page import="com.bp.pensionline.dao.RecordDao"%>
<%@page import="com.bp.pensionline.dao.CompanySchemeDao"%>
<%@page import="org.opencms.db.*" %>
<%@ page import="org.opencms.main.*, org.opencms.jsp.*,org.opencms.file.*,java.util.*" %>
<%@ page import="com.bp.pensionline.handler.*" %>
<%@ page import="com.bp.pensionline.util.*" %>
<%@ page import="com.bp.pensionline.constants.*" %>
<%@ page import="com.bp.pensionline.util.SystemAccount" %>
<%@ page import="com.bp.pensionline.util.CheckConfigurationKey" %>
<%@page import="com.bp.pensionline.dao.MemberDao"%>
<%@page import="com.bp.pensionline.factory.DataAccess"%>
<%@page import="com.bp.pensionline.webstats.producer.*"%>
<%@page import="com.bp.pensionline.webstats.*"%>
<%@page import="com.bp.pensionline.publishing.handler.EditorLandingHandler"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);
	//override the default value of login attempts, max bad attempts: 3, disable time: 15 mins
	//CmsLoginManager cmsLoginManager = new CmsLoginManager (15, 3);
	//int badAttempts = cmsLoginManager.getMaxBadAttempts();
	//System.out.println("login bad pwd" + badAttempts );

	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cmso=bean.getCmsObject();
    DataMigrationHandler handler=new DataMigrationHandler();
	
// begin webstats for Login
	WebstatsLoginProducer wbspProducer=new WebstatsLoginProducer();

	// read parameters from the request
	String username = request.getParameter("_request_username");
	String password = request.getParameter("_request_password");
// begin new with data migration
        // Taken from the welcome page if required
     String surname    = request.getParameter("surname");
     String payrollnum = request.getParameter("payrollnum");
     String hardreg    = request.getParameter("hardreg");
// end new with data migration
	//New	
	//String referingPage = request.getParameter("referingPage");referingPage=referingPage==null?new String(""):referingPage;
	String referingPage = (String)request.getSession().getAttribute("requestedResource");
	referingPage = referingPage==null ? new String("") : referingPage;
	System.out.println("REQUESTED: "+referingPage);
	//END New
	boolean isWebuser = false;
	
	try
	{		
   		isWebuser = handler.migrateUserDetails(username,password);
	}
	catch(Exception e)
	{
		// password not match or error in data migration
		System.out.println("Exception in data migration: " + e.toString());
		response.sendRedirect("_login_error.html");
		return;
	}
	
//set User-Agent info into session
	TextUtil.sessionParameters.set(request.getSession().getId(),request.getHeader("User-Agent"));
	System.out.println(request.getSession().getId() + "---" + request.getHeader("User-Agent"));

	if(isWebuser) {
		username = SystemAccount.webUnitName+username;
	}
	cms.login(username,password);
	
	if (cms.isLoginSuccess()) 
	{
		request.getSession().setAttribute("referingPage",referingPage);
		if (referingPage.trim().length()>0) {
			//response.sendRedirect(referingPage);
		}
		//set Login status -> success
       	request.getSession().setAttribute(WebstatsConstants.OUT_COME, WebstatsConstants.SUCCESS);
        
		
   		CmsUser cmsUser = cms.getRequestContext().currentUser();
   		String cmsUserName = username;
		cmsUser.setAdditionalInfo(Environment.MEMBER_USERNAME, username);
		cmsUser.setAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL,cms.user("description"));
		
		// Store user's password in if user in Publishing groups
		if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_GROUP) || 
				cmso.userInGroup(username, Environment.PUBLISHING_REVIEWER_GROUP) ||
				cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||				
				cmso.userInGroup(username, Environment.PUBLISHING_AUTHORISER_GROUP) ||
				cmso.userInGroup(username, Environment.PUBLISHING_DEPLOYER_GROUP))
		{
			System.out.println("User in publishing groups");
			cmsUser.setAdditionalInfo(Environment.MEMBER_USERPASSWORD, password);
		}		
	
		/*
        if (cmsUser.isWebuser() && !cmsUser.isSystemUser()&& !cmso.userInGroup(username,"Superusers") 
        	&& !cmso.userInGroup(username,"Testers"))
        */
        if (cmsUser.isWebuser() && !cmso.userInGroup(username,"Superusers") 
            	&& !cmso.userInGroup(username,"Testers"))
        {
			try 
			{

				String []acceptGroup={"Members","WALL_ALL_ACCESS"};
				// remove any group previous accept acceptGroup[]
				SwapUserHandler.getInstance().unSwapUser(cmsUserName,acceptGroup,SystemAccount.getAdminCmsObject());
					// run the webstats process
				request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Members");
				wbspProducer.runStats(request);
				response.sendRedirect("_login_periodofservice.jsp?referingPage="+referingPage);
			}
			catch (Exception e)
			{				
				response.sendRedirect("_login_error.html");
			} //end catch
		} //end if	
    	else if (cmso.userInGroup(username,"Superusers"))
    	{
    		String []superUserGroup={"Superusers","WALL_ALL_ACCESS"};
    		SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());

			request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"SuperUser");
			wbspProducer.runStats(request);					
			response.sendRedirect("superuser/index.html"); 
			       	
		}	
    	else if (cmso.userInGroup(username,"PL_REPORT_EDITOR"))
    	{			    			
			response.sendRedirect(cms.link("/sql_report/manage_report/index.html"));		
		} 
    	else if (cmso.userInGroup(username,"PL_REPORT_RUNNER"))
    	{
			response.sendRedirect(cms.link("/sql_report/access_reports.html"));
		}	        		
		else if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_GROUP))
		{
			//System.out.println("User in Editors group");
			String []superUserGroup = {Environment.PUBLISHING_EDITOR_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentEditor");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			//EditorLandingHandler.synchronizeResources (request);
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP))
		{
			//System.out.println("User in Editors_Reviewers group");
			String []superUserGroup = {Environment.PUBLISHING_EDITOR_REVIEWER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentEditor");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			//EditorLandingHandler.synchronizeResources (request);
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}		
		else if (cmso.userInGroup(username, Environment.PUBLISHING_REVIEWER_GROUP))
		{
			String []superUserGroup={Environment.PUBLISHING_REVIEWER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentChecker");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_AUTHORISER_GROUP))
		{
			String []superUserGroup = {Environment.PUBLISHING_AUTHORISER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentAuthoriser");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_DEPLOYER_GROUP))
		{
			String []superUserGroup = {Environment.PUBLISHING_DEPLOYER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "SystemDeployer");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username,"Testers"))
		{		
			String []superUserGroup={"Testers","Members"};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Tester");
			wbspProducer.runStats(request);
	
			response.sendRedirect("index.html");
		}
		else if (cmso.userInGroup(username,"Publishers"))
		{
			String []superUserGroup={"Publishers","Members"};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName,superUserGroup,SystemAccount.getAdminCmsObject());
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Publisher");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/system/workplace/views/workplace.jsp"));
			//response.sendRedirect(cms.link("index.html"));		
		}
		//else if (cmsUser.isSystemUser(1) )
		else if(cmso.userInGroup(username, "Administrators"))
		{
	
			request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"SystemUser");
			wbspProducer.runStats(request);
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/system/workplace/views/workplace.jsp"));
	    }
    } else 
    {
    	request.getSession().setAttribute(WebstatsConstants.OUT_COME,WebstatsConstants.FAILURE);
   	 	request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Guest");
        wbspProducer.runStats(request);    
    	// redirect users to login error page if authentication fails
        response.sendRedirect("_login_error.html");

 	}
%>
