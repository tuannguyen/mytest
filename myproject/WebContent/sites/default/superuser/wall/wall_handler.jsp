<%@ page import="org.opencms.jsp.*" %>
<%@ page import="org.opencms.file.*" %>
<%@ page import="com.bp.pensionline.handler.WallHandler" %>
<%@ page import="com.bp.pensionline.util.StringUtil" %>
<%@ page import="com.bp.pensionline.util.SystemAccount"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="java.util.StringTokenizer"%>


<cms:include property="template" element="head" />

<h1>user blocking update</h1>

<%
// get the system name eq: localhost..
String requestURL = request.getRequestURL().toString();
String requestURI = request.getRequestURI();
String systemName=requestURL = requestURL.substring(0, requestURL.indexOf(requestURI));

// If the user is not a superuser, redirect to the error page.

String block = "WALL_" + request.getParameter("block");
String rqArray=request.getParameter("list");


String list = request.getParameter("list");
CmsUser cmsUser=SystemAccount.getCurrentUser(request);
String cmsUserName=cmsUser.getFullName();


// split input into lines and trim leading and trailing whitespace

// foreach line:
// validate input
//   trimmed lines either 
//     * consist of <BGROUP>-<REFNO>   /^B[PC]F-[0-9]{7}/
//     * start with a # comment symbol /^#/
//     * are blank                     /^$/

// delete entries in block table

// foreach validated line, insert it into block table.

// foreach invalid line, report it.
//
// Output either
//   <h2>Details have been updated</h2>
//   <h2>Some details have been updated</h2> (and report errors)
//WallHandler.insert(list,block,cmsUserName,systemName);
String regular1="^B[PC]F-[0-9]{7}";
String regular2="^\\#.*?$";
String regular3="^$";
String error=StringUtil.EMPTY_STRING;
String result=StringUtil.EMPTY_STRING;
StringTokenizer token=new StringTokenizer(rqArray,"\n");

WallHandler wallhandler=new WallHandler();

wallhandler.deleteByGname(block);

while (token.hasMoreTokens()){
	String tem=String.valueOf(token.nextToken()).trim();
         if (tem.matches(regular1)||tem.matches(regular2)||tem.matches(regular3))	{
                           wallhandler.insert(tem,block,cmsUserName,systemName);
                           result+=tem+"\n";    
                        }  else{
                              error+=tem+"\n";

                                                     
		        }


	
}



%>
<%if (result!=StringUtil.EMPTY_STRING){%>

<p>Updating the <code><strong><%=block%></strong></code> table with the following.</p>
<pre style='margin:left:10px;background-color:#eee;border:solid #ccc 1px;'><%=result%></pre>
<%}else{%>
<p>All members in <code><strong><%=block%></strong></code> group has been remove.</p>
<%}%>
<%if(error!=StringUtil.EMPTY_STRING){%>
<p>Error line(s) fllow has not been updated.</p>
<pre style='margin:left:10px;background-color:#eee;border:solid #ccc 1px;'><%=error%></pre>
<%}%>

<cms:include property="template" element="foot" />
