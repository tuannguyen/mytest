<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.bp.pensionline.constants.Environment" %>
<%@ page import="com.bp.pensionline.util.Base64Encoder" %>
<%@ page import="com.bp.pensionline.util.Base64Decoder" %>
<%@ page import="com.bp.pensionline.database.DBConnector" %>
<cms:include property="template" element="head" />
<%

	String bgroup     = request.getParameter("_request_bgroup");
	String refno      = request.getParameter("_request_refno");
	String queryParam = bgroup + "-" + refno;

	String sqlQuerySelect  = "SELECT Date,GName,Superuser FROM BP_WALL_GROUPS WHERE Data=? order by Date asc";

	Connection con = null;

	try {
		DBConnector connector = DBConnector.getInstance();
		con = connector.getDBConnFactory(Environment.PENSIONLINE);
		con.setAutoCommit(false);
	} catch (Exception e) {
		// Don't particularly care
		System.out.println(e);
		response.getWriter().write(e + "<br />");
	}
	finally {
	}
%>

<h1>Member supression codes</h1>

<%
System.out.println("<pre>The SQL:\n");
System.out.println(sqlQuerySelect);
System.out.println("</pre>");

	if(con == null) {
%>
<p>You know, a strange thing happened, it seems as though I have lost something... Oh, the database, that'll be it. 
Give it bit of a whack for me please, and if it still isn't working right, best get onto them Systems slackers.</p>
<%
	} else {
		ResultSet rs = null;
		try {

			PreparedStatement pstm = con.prepareStatement(sqlQuerySelect);
			pstm.setString(1, queryParam);
			rs = pstm.executeQuery();

%>
<table width='100%' cellspacing='0' cellpadding='0' border="1" class="publishtable">
	<tr>
		<th class='label'>Date</th>
		<th class='label'>Group</th>
		<th class='label'>Superuser</th>
	</tr>
<%
				String added_date = "";
				String group_name = "";
				String superuser = "";
				int num_out = 0;

				while (rs.next()) {
					num_out ++;
					added_date = rs.getString(1);
					group_name = rs.getString(2);
					superuser  = rs.getString(3);
%>
	<tr>
		<td style='padding:5px;'><%=added_date%></td>
		<td style='padding:5px;'><%=group_name%></td>
		<td style='padding:5px;'><%=superuser%></td>
	</tr>
<%
				}

				if(num_out == 0) {
%>
	<tr>
		<td colspan='3' style='padding:5px;'>No data was returned.</td>
	</tr>
<%
				}
%>
</table>
<%
				rs.close();

		} catch (Exception e) {
			response.getWriter().write(e + "<br />");
		}
		finally {
		}
	}


	if (con != null) {
		try {
			con.close();
		} catch (Exception e) {
			response.getWriter().write(e + "<br />");
		} finally {
		}
	}
%>
<cms:include property="template" element="foot" />
