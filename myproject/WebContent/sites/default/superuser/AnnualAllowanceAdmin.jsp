<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="com.bp.pensionline.aataxmodeller.util.DefaultConfiguration" %>
<%@ page import="com.bp.pensionline.aataxmodeller.dto.DefaultInfo" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.bp.pensionline.aataxmodeller.util.ConfigurationUtil"%>

<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<cms:include property="template" element="head" />
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null && SystemAccount.getAdmCmsObject().userInGroup(currentUser.getName(), "Administrators"))
	{
%>
<H1>&nbsp;&nbsp;&nbsp;Tax Modeller configuration aaa</H1>

<div id="status" class="fakelink">
</div>

<div class="login_holder">
	<form method="post" action="" name="AATaxAdminForm">
	<%
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		DefaultConfiguration configLoad = new DefaultConfiguration();
		Hashtable<String, DefaultInfo> infos = configLoad.getDefaultValues();
		HashMap<String, String> contentAa = configLoad.loadConfigurationsForAaCarryConfig();
		String contentCar = configLoad.loadConfigurationsForCarConfig();
		HashMap<String, String> contentServiceSalary = configLoad.loadConfigurationsForServiceSalary();
		TreeMap<String,String> LTAValues = configLoad.getLTAValues();
		TreeMap<String,String> CapValues = configLoad.getCapValues();
		Vector<String> vecKeys = new Vector<String>();
		Enumeration<String> enums = infos.keys();
		while (enums.hasMoreElements()) {
		    vecKeys.add(enums.nextElement());
		}
		Collections.sort(vecKeys); 	
		try {
	     	String key = null;
	     	DefaultInfo defInfo = null;
	     	for(int i=0;i<vecKeys.size();i++){
			    key = (String)vecKeys.get(i);
			    defInfo = infos.get(key);
			    String unit = "";
			    String display = "display: none;";
		%>
    	<%
	    		if (key.equals("AnnualAllowanceTaxRate")){
	    			unit = "%";
	    			display = "";
	    		}
    	%>	    
			<div style="width: 85%; height: 12px; <%=display%>"  class="login_component_holder" style="margin-top: 4px">
			    <div style="width: 220px; height: 12px" class="login_label">
			    	<label for="<%=key%>"> 	<%=defInfo.getLabel()%> (<%=unit%>)</label>
			    </div>		
				<div class="login_value"><input id="<%=key%>" name="<%=key%>" type="text" value="<%=defInfo.getValue()%>"/></div>		    	
		    </div>		    	    
	<%             
	        }
		} 
		catch (Exception e) {
			// Don't particularly care
			System.out.println(e);
			response.getWriter().write(e + "<br />");
		}
		finally {
		}
	%>		
			<P>&nbsp;</P>
			<div class="login_component_holder">
			    <div class="login_label">&nbsp;</div>
			    <div class="login_value">&nbsp;</div>
			     <a href="#" onclick="document.AATaxAdminForm.reset(); return false">Cancel</a>&nbsp;
			    <input id="doauth" class="goButton" type="button" onclick="submitDefaultTaxModellerRequest();" name="doauth" value="Commit" />
			</div>
	</form>
</div>

<%
	}else{
%>
<h1>Access denied</h1>
<p>Sorry! You do not have enough permissions to view this section. Please contact the PensionLine administrator for more information.</p>
<div style="display: none;">Trick here for IE6</div>
<%
	}
%>
<cms:include property="template" element="foot" />
