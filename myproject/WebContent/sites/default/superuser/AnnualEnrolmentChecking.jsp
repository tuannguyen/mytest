<%@page session="false"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%@page import="com.bp.pensionline.aataxmodeller.dto.MemberDetail"%>
<%@page import="com.bp.pensionline.aataxmodeller.handler.MemberLoadingService"%>
<%@page import="com.bp.pensionline.aataxmodeller.modeller.Headroom"%><cms:include property="template" element="head" />
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	String today = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
	if (currentUser != null)
	{
%>
<h1>Annual Enrolment Debug</h1>
<p>This page allows you to check the Annual Enrolment data of an AC member</p>
<div class="login_component_holder">
<div class="login_label"><label for="bgroup">Bgroup</label></div>
<div class="login_value"><select id="bgroup" name="bgroup">
<option value="BPF" selected="selected">BPF</option>
<option value="BCF">BCF</option>
</select></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label for="refno">Refno</label></div>
<div class="login_value"><input id="refno" name="refno" type="text" /></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label for="sdate">System date</label></div>
<div class="login_value">
	<input id="sdate" readonly="readonly" value="<%=today %>"/>&nbsp;
	<button id="sdate_trigger">...</button>
	<script type="text/javascript">
             var links = document.getElementsByTagName("link");
             var skins = {};
             for (var i = 0; i < links.length; i++) {
                     if (/^skin-(.*)/.test(links[i].id)) {
                             var id = RegExp.$1;
                             skins[id] = links[i];
                     }
             }


             for (var i in skins) {
				if (skins.hasOwnProperty(i))
       			skins[i].disabled = true;
			 }
			 if (skins['gold']) skins['gold'].disabled = false;
             new Calendar({
                     inputField: "sdate",
                     dateFormat: "%d-%b-%Y",
                     trigger: "sdate_trigger",
                     bottomBar: false,
                     onSelect: function() {                         
                         this.hide();
                 }
             });
	</script>
</div>
</div>
<div class="login_component_holder">
<div class="login_label">&nbsp;</div>
<div class="login_value"><input id="doauth" class="goButton" type="button" onclick="loadMember(); return false;" name="doauth" value="Go" /></div>
</div><br/>
<hr style="float: left; width: 100%;" />
<div id="member_running_div" style="float: left; display: none;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif</cms:link>" alt="running..." /></div>
<div id="member_result_div" style="float: left; display: none;">
	<h2 style="float: left; width: 90%;">Member details</h2>
	<table class="member_detail_table" cellspacing="0" cellpadding="0">
	    <tbody>
	        <tr>
	            <th class="longlabel" colspan="4">Member general details</th>
	        </tr>
	        <tr>
	            <td class="label" style="width: 160px;">Reference</td>
	            <td id="reference" class="value number" >&nbsp;</td>
	            <td class="label" style="width: 160px;">Pensionable salary</td>
	            <td id="salary" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">NI</td>
	            <td id="nino" class="value number">&nbsp;</td>
	            <td class="label">AVC fund</td>
	            <td id="avc_fund" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Name</td>
	            <td id="name" class="value number">&nbsp;</td>
	            <td class="label">AVER fund</td>
	            <td id="aver_fund" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Gender</td>
	            <td id="gender" class="value number">&nbsp;</td>
	            <td class="label">TVIN PR86 days</td>
	            <td id="tvin_a" class="value number">&nbsp;</td>            
	        </tr>    
	        <tr>
	            <td class="label">Date of birth</td>
	            <td id="date_of_birth" class="value number">&nbsp;</td>
	            <td class="label">TVIN PR06 days</td>
	            <td id="tvin_b" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Joined company</td>
	            <td id="joined_company" class="value number">&nbsp;</td>
	            <td class="label">TVIN PO06 days</td>
	            <td id="tvin_c" class="value number">&nbsp;</td>            
	        </tr>     
	        <tr>
	            <td class="label">Joined scheme</td>
	            <td id="joined_scheme" class="value number">&nbsp;</td>
	            <td class="label">Augmentation days</td>
	            <td id="augmentation" class="value number">&nbsp;</td>            
	        </tr>
	        <tr>
	            <td class="label">55th birthday</td>
	            <td id="birthday_55th" class="value number">&nbsp;</td>
	            <td class="label">Lta</td>
	            <td id="lta" class="value number">&nbsp;</td>            
	        </tr>        
	        <tr>
	            <td class="label">60th birthday</td>
	            <td id="birthday_60th" class="value number">&nbsp;</td>
	            <td class="label">Banked EGP</td>
	            <td id="banked_egp" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">65th birthday</td>
	            <td id="birthday_65th" class="value number">&nbsp;</td>
	            <td class="label">&nbsp;</td>
	            <td class="value number">&nbsp;</td>            
	        </tr> 
	    </tbody>
	</table>
	<div style="float: left;" id="scheme_absence_table">&nbsp;</div>
	<div style="float: left;" id="salary_table">&nbsp;</div>	
</div>
<div id="headroom_running_div" style="display: none; float: left;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif</cms:link>" alt="running..." /></div>
<div id="headroom_result_div" style="display: none;">	
	<h2 style="float: left; width: 90%;">Headroom</h2>
	<div style="float: left;" id="headroom_table">&nbsp;</div>
	<h2 style="float: left; width: 90%;">Tax data</h2>	
	<div style="float: left;" id="current_taxyear_table">&nbsp;</div>
	<div style="float: left;" id="next_taxyear_table">&nbsp;</div>
	<h2 style="float: left; width: 90%;">Modeller</h2>	
	<table id="taxmodeller_table" class="taxmodeller_table" style="float: left;" cellspacing="0" cellpadding="0">
	    <tbody>
	        <tr>
	            <th class="empty" colspan="2">&nbsp;</th>
	            <th class="label">Modelled options</th>
	        </tr>
	        <tr>
	            <td class="label" colspan="2">Benefit accrued at <span id="retire_age">&nbsp;</span></td>
	            <td class="value number" id="accrued_to_date">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label" style="border-right: none;">
	            	Accrual rate at <span id="accrued_date">&nbsp;</span>
	            </td>
	            <td width="50" style="border-left: none;"><img style="cursor: pointer" id="info_AccrualChange" title="Add an accrual rate change" alt="Add an accrual rate change" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png" /></td>	            
	            <td class="value number" id="modelled_accrual">&nbsp;</td>
	        </tr>	        
	        <tr>
	            <td class="label" style="border-right: none;">
	            	Pensionable salary at <span id="salaried_date">&nbsp;</span></a>
	            </td>	            
            	<td width="50" style="border-left: none;"><img style="cursor: pointer" id="info_SalaryChange" title="Add a salary change" alt="Add a salary change" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png" /></td>	            
	            <td class="value number" id="modelled_salary">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label" style="border-right: none;">Retirement age</td>
	            <td width="50" style="border-left: none;"><img style="cursor: pointer" id="info_RetireAge" title="Select the retirement age" alt="Select the retirement age" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png" /></td>
	            <td class="value number" id="modelled_retire_age">&nbsp;</td>
	        </tr>		        
	        <tr>
	            <td class="label" style="border-right: none;">Cash lump sum</td>
	            <td width="50" style="border-left: none;"><img style="cursor: pointer" id="info_CashChange" title="Add a cash lump sum change" alt="Add a cash lump sum change" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png" /></td>
	            <td class="value number" id="cash_lump_sum">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label" colspan="2">Pension with chosen cash</td>
	            <td class="value number" id="pension">&nbsp;</td>
	        </tr>	        
	        <tr>
	            <td class="label" colspan="2">Pension pot</td>
	            <td class="value number" id="pension_pot">&nbsp;</td>
	        </tr>	        	                
	    </tbody>
	</table>			
</div>

<div id="popup_AccrualChange" class="aa_popup">
<h2>Accrual rate change</h2>
<table border="0" cellspacing="0" cellpadding="0" width="520" style="margin-bottom: 30px;">
    <tbody>
        <tr>
            <td width="360">Select the accrual rate change for <span name="next_tax_year">&nbsp;</span></td>
            <td width="160">
            <select id="input_accrual">
				<option value="60" selected="selected">0:60</option>
				<option value="54">2:54</option>
				<option value="51">3:51</option>
				<option value="45">5:45</option>
				<option value="40">10:40</option>
				<option value="35">15:35</option>
			</select> &nbsp;
			<input class="goButton" type="button" onclick="modelAccrualChange(); return false;" value="Model" />
            </td>
        </tr>
    </tbody>
</table>
</div>
<div id="popup_SalaryChange" class="aa_popup">
<h2>Salary change</h2>
<table border="0" cellspacing="0" cellpadding="0"style="margin-bottom: 30px;">
    <tbody>
        <tr>
            <td width="520">Select your salary change for&nbsp;<span id="next_tax_year">&nbsp;</span>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td>
            <div style="float: left">
            <div style="float: left" id="salary_slider">&nbsp;</div>
            <input style="text-align: right; width: 80px; float: left; margin-left: 10px" id="input_salary" class="slider_value" readonly="readonly" name="input_salary" type="text" />&nbsp;<input class="goButton" type="button" onclick="modelSalaryChange(); return false;" value="Model" /></div>
            </td>
        </tr>
    </tbody>
</table>
</div>
<div id="popup_RetireAge" class="aa_popup">
<h2>Retirement age</h2>
<table border="0" cellspacing="5" cellpadding="5" style="margin-bottom: 30px;">
    <tbody>
        <tr>
            <td width="520">Select the retirement age for the member</span>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td>
            <div style="float: left">
            <div style="float: left" id="retire_age_slider">&nbsp;</div>
            <input style="text-align: right; width: 80px; float: left; margin-left: 10px" id="input_retire_age" class="slider_value" readonly="readonly" name="input_retire_age" type="text" />&nbsp;<input class="goButton" type="button" onclick="modelRetireAgeChange(); return false;" value="Model" /></div>
            </td>
        </tr>
    </tbody>
</table>
</div>
<div id="popup_CashChange" class="aa_popup">
<h2>Cash lump sum</h2>
<table border="0" cellspacing="5" cellpadding="5" style="margin-bottom: 30px;">
    <tbody>
        <tr>
            <td width="360">Select proportion of cash lump sum</td>
            <td>
            <select id="input_cash">
				<option value="0" selected="selected">0%</option>
				<option value="50">50%</option>
				<option value="100">100%</option>
				</select> &nbsp; 
				<input class="goButton" type="button" onclick="modelCashChange(); return false;" value="Model" />
            </td>
        </tr>
    </tbody>
</table>
</div>

<%
	}
	else
	{
%>
<h1>Access denied</h1>
<p>Sorry! You do not have enough permissions to view this section. Please contact the PensionLine administrator for more information.</p>
<%
	}
%>
<cms:include property="template" element="foot" />