<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.bp.pensionline.constants.Environment" %>
<%@ page import="com.bp.pensionline.util.Base64Encoder" %>
<%@ page import="com.bp.pensionline.util.Base64Decoder" %>
<%@ page import="com.bp.pensionline.database.DBConnector" %>
<cms:include property="template" element="head" />
<%
                String sqlQuerySelectCasework  = "SELECT BGroup,Refno,CreationDate,FormType,FormData,CaseWorkId FROM BP_FORMS WHERE CaseWorkId=? order by CreationDate";
                String sqlQuerySelectRefno     = "SELECT BGroup,Refno,CreationDate,FormType,FormData,CaseWorkId FROM BP_FORMS WHERE Refno=? order by CreationDate";
                String sqlQuerySelect          = sqlQuerySelectRefno;


		String imailBgroup     = "";
		String imailRefno      = request.getParameter("_request_refno");
                if (imailRefno == null) imailRefno = "";
		String imailNI         = "";
		String imailSubject    = "";
		String imailMessage    = "";
		String imailFormData   = "";
		String imailCreationDate = "";
                String caseWorkId      = request.getParameter("_request_caseworkid");
                if (caseWorkId== null) caseWorkId= "";

                String queryParam = imailRefno;

                boolean useRefno = false;
                if(imailRefno.length() > 0){
                  useRefno = true;
                }

                boolean useCasework = false;
                if(caseWorkId.length() > 0){
                  useCasework = true;
                  sqlQuerySelect = sqlQuerySelectCasework;
                  queryParam = caseWorkId;                  
                }


%>

<h1>member form search</h1>

<p>Enter a casework id or refno (only BPF have access at the moment).</p>

<div id="casework_search">
  <form action="" method="get">
<!--    <input id="name="_request_caseworkid_or_refno" type="hidden" length=1 size=1 value="1" /> -->
    <div class="login_component_holder">
      <div class="login_label"><label for="_request_caseworkid">casework id</label> </div>
      <div class="login_value"><input id="_request_caseworkid" name="_request_caseworkid" type="text" value="<%= caseWorkId %>"/> </div>
      <div class="login_value"><input class="goButton" id="doauth" type="submit" name="doauth" value="Go" /> </div>
    </div>
  </form>
</div>

<div class="login_component_holder">
  <div class="login_label">&nbsp;</div>
  <div class="login_value">or</div>
</div>

<div id="refno_search">
  <form action="" method="get">
    <div class="login_component_holder">
      <div class="login_label"><label for="_request_refno">Refno</label> </div>
      <div class="login_value"><input id="_request_refno" type="text" name="_request_refno" value="<%= imailRefno %>"/> </div>
      <div class="login_value"><input class="goButton" id="doauth" type="submit" name="doauth" value="Go" /> </div>
    </div>
  </form>
</div>

<%
if(useRefno || useCasework) {
%>
<br /><hr /><br />
<%

		Connection con = null;
				
		try {
			DBConnector connector = DBConnector.getInstance();
			con = connector.getDBConnFactory(Environment.PENSIONLINE);
			con.setAutoCommit(false);
			PreparedStatement pstm = con.prepareStatement(sqlQuerySelect );
			pstm.setString(1, queryParam);

			ResultSet rs = pstm.executeQuery();        // Get the result table from the query


                        if (useCasework){

			  if (rs.next()) {                           // Position the cursor ONLY LOOKING FOR FIRST POSITION     

		            imailBgroup     = rs.getString(1);
		            imailRefno      = rs.getString(2);
		            imailCreationDate = rs.getString(3);
		            imailSubject    = rs.getString(4);
 		            imailFormData   = Base64Decoder.decode(rs.getString(5));
                            caseWorkId      = rs.getString(6);

			  }
			}else{
                           //show all caseworks imails for this single refno

                           %>
                             <table width='100%' cellspacing='0' cellpadding='0' border="1">
                                <tr>
                                   <th class='label'>Date</th>
                                   <th class='label'>Subject</th>
                                   <th class='label'>Case work</th>
                                </tr>
                           <%

                            while (rs.next()) {
		              imailBgroup     = rs.getString(1);
		              imailRefno      = rs.getString(2);
		              imailCreationDate = rs.getString(3);
		              imailSubject    = rs.getString(4);
		              imailMessage    = Base64Decoder.decode(rs.getString(5));
  	                      caseWorkId      = rs.getString(6);
                              %>
                                <tr>
                                   <td><%=imailCreationDate%></td>
                                   <td><%=imailSubject%></td>
                                   <td><a title='Click here to view details' href='formviewer.jsp?_request_caseworkid=<%=caseWorkId%>'><%=caseWorkId%></a></td>
                                </tr>
                            <%
                            }
                            %>
                             </table>
                            <%
                        }
			rs.close();                                // Close the ResultSet     
 
		} catch (Exception e) {

			try {
				con.rollback();
			} catch (Exception ex) {
                                System.out.println(e);

			}
			throw e;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					System.out.println(e);
				}

			}

		}
}

%>

<%
if (useCasework){
%>
    <div class="login_component_holder">
      <div class="login_label"><label for="_request_subject">Subject</label> </div>
      <div class="login_value"><input id="_request_subject" type="text" name="_request_subject" value="<%= imailSubject %>" /></div>
    </div>

    <div class="login_component_holder">
      <div class="login_label">
        <label for="query">Form Data</label>
      </div>
      <div class="login_value">
        <textarea id="_request_message" rows="5" cols="50"  ><%= imailFormData%></textarea>
      </div>
    </div>
<%
}
%>

<cms:include property="template" element="foot" />
