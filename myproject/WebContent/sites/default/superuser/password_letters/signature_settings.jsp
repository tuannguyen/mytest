<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page import="org.opencms.jsp.*"%>
<%@page import="org.opencms.file.*"%>
<%@page import="org.opencms.main.OpenCms"%>
<%@page import="com.bp.pensionline.letter.util.SignatureUtil"%>
<%@page import="com.bp.pensionline.letter.constants.Constants"%>
<%@page import="com.bp.pensionline.letter.constants.IconFile"%>
<%@page import="com.bp.pensionline.letter.dao.SignatureXmlDao"%>


<cms:include property="template" element="head" />

<%
//CmsJspActionElement cmsAction = new CmsJspActionElement(pageContext, request, response);
//cmsAction.include("/system/modules/com.bp.pensionline.template/templates/main.jsp", "style");

SignatureUtil sU = new SignatureUtil(Constants.SIGNATURE.IMAGE_GALLERY_LOCATION);
List icons = sU.getAllIcons();

SignatureXmlDao signature = new SignatureXmlDao(Constants.SIGNATURE.XML_PATH);

String savedPath = signature.getImageFileName();//(String)session.getAttribute(Constants.SIGNATURE.TAG_PATH);
//System.out.println(savedPath);
boolean wasOnline = signature.isUseOnlineImage();//(String)session.getAttribute(Constants.SIGNATURE.TAG_ONLINE);
String currentName = signature.getName();//(String)session.getAttribute(Constants.SIGNATURE.TAG_NAME);
String currentTitle = signature.getTitle();//(String)session.getAttribute(Constants.SIGNATURE.TAG_TITLE);

if (currentName==null) currentName="";
if (currentTitle==null) currentTitle="";
%>

<script language="javascript">
var ajaxRequest;

function expandCollapse(div1, div2) {
	var isOnline = "";
	for (var i=0; i < document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>.length; i++) {
   		if (document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>[i].checked){
      			var isOnline = document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>[i].value;
			break;
      		}
   	}
	var objD1 = document.getElementById(div1);
	var objD2 = document.getElementById(div2);
	var span = document.getElementById("informText");
	if (isOnline=="true") {
		objD1.style.display="";
		objD2.style.display="none";
		span.innerHTML="<strong>Signature gallery:</strong>";
	} else {
		objD1.style.display="none";
		objD2.style.display="";
		<%if (!wasOnline && (savedPath!=null&&savedPath.trim().length()>0)){%>
		span.innerHTML="Current location of the signature image:<br/> <strong><%=savedPath%>)</strong>:";
		<%}else{%>
		span.innerHTML="Please select your local image:";
		<%}%>
	}
	return;
}

// This function is for stripping leading and trailing spaces
function trim(str) { 
    if (str != null) {
        var i; 
        for (i=0; i<str.length; i++) {
            if (str.charAt(i)!=" ") {
                str=str.substring(i,str.length); 
                break;
            } 
        } 
    
        for (i=str.length-1; i>=0; i--) {
            if (str.charAt(i)!=" ") {
                str=str.substring(0,i+1); 
                break;
            } 
        } 
        
        if (str.charAt(0)==" ") {
            return ""; 
        } else {
            return str; 
        }
    }
}

function saveSignature(isOnline, path, name, title){
	var url= "/content/SignatureHandler";
	var params = "Online="+isOnline;
	params += "&Path="+path;
	params += "&Name="+name;	
	params += "&Title="+title;

	ajaxRequest = createXmlHttpRequestObject();
	ajaxRequest.open("POST", url, true );

	ajaxRequest.onreadystatechange = handleSignatureResponse;		
	
    	ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    	ajaxRequest.setRequestHeader("Content-length", params.length);
    	ajaxRequest.setRequestHeader("Connection", "close");		

	ajaxRequest.send(params);	
}

function handleSignatureResponse(){
   	if (ajaxRequest.readyState == 4) {								
		// Check that a successful server response was received
		if (ajaxRequest.status == 200) {
			//alert(ajaxRequest.responseText);
			var response = createResponseXML(ajaxRequest.responseText);					
			try{      
				//alert(ajaxRequest.responseText+"\n"+repsonse);                      
				//var ptbPageReload = response.getElementsByTagName("PtbPageReload")[0].firstChild.nodeValue;        
				var result = response.getElementsByTagName("Result")[0].firstChild.nodeValue;
				//alert(ajaxRequest.responseText+"\n"+result);
				if(result=="true") {
					var path = response.getElementsByTagName("Path")[0].firstChild.nodeValue;
					//path = path.replace(/@/g,"/")
					alert("Letter signature updated!");
					//window.close();
					window.history.back();
				}
				else {
					var error = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert("Error occurred while trying saving data.\nError details: "+error);
					//window.close();						
				}
			}
			catch(err){
				alert("XML Parser error." + err);	
				//window.close();	
			}						
		}		
	}	
}

function submitForm() {
	var isOnline = "";
	for (var i=0; i < document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>.length; i++) {
   		if (document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>[i].checked){
      			var isOnline = document.signatureForm.<%=Constants.SIGNATURE.TAG_ONLINE%>[i].value;
			break;
      		}
   	}
	var path=" ";
	if (isOnline=="true") {
		for (var j=0; j<document.signatureForm.<%=Constants.SIGNATURE.TAG_PATH%>.length; j++) {
			if (document.signatureForm.<%=Constants.SIGNATURE.TAG_PATH%>[j].checked){
      				path = document.signatureForm.<%=Constants.SIGNATURE.TAG_PATH%>[j].value;
				break;
      			}
		}
	} else {
		path = document.signatureForm.localFile.value;
	}
	var name = document.signatureForm.<%=Constants.SIGNATURE.TAG_NAME%>.value;
	var title = document.signatureForm.<%=Constants.SIGNATURE.TAG_TITLE%>.value;
	title = title.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');
	name = name.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');
	if (trim(path)=="") {
		if (isOnline=="true") {
			alert("Please select an image on the fly");
		} else	{
			alert("Please select the local image");
		}
		return;
	}
	//alert(isOnline+"-"+path+"\n"+name+"-"+title);
	saveSignature(isOnline,path,name,title);
}
</script>
<h1>Signature settings</h1>
<p>'Signature settings' page allows users to select an image as a signature for the letters generated. The signature image
can be selected from an secured gallery in CMS or can be pluged into the MS Word document letter in viewing time. To use
signature image from gallery, please select 'Use signature image on the fly' button. To use signature image in vewing tim as a plug-in,
please select 'Use signature image from user's local driver' button.
</p>
<p>Note:<br>
 The signature gallery is managed by system Administrators. Please contact them if you wish to add new image or remove
existing images for security reasons.<br>
 If you want to use the signature image in viewing time, it is required that you must store the image in the location specified
by 'Signature image' text box.
</p>
<form name="signatureForm">
<input type="radio" name="<%=Constants.SIGNATURE.TAG_ONLINE%>" value="true" onClick="expandCollapse('flyDiv','localDiv');" checked>Use signature image on the fly<br>
<input type="radio" name="<%=Constants.SIGNATURE.TAG_ONLINE%>" value="false" onClick="expandCollapse('flyDiv','localDiv');" <%if(!wasOnline){%>checked<%}%>>Use signature image from user's local driver
<br/>
<hr width="100%"/>
<span id="informText">
<%if(!wasOnline){%>
Current location of the signature image:<br/> <strong><%=savedPath.replace('@','\\')%> </strong>
<%
}else
{
%>
<strong>Signature gallery:</strong>
<%
}
%>
</span><br/><br/>
<div id="flyDiv" style="width:100%;height:150px;overflow-y:scroll;overflow-x:hidden;<%if(!wasOnline){%>display:none<%}%>">
<table width="100%" cellspacing="3">
<colgroup>
<col width="80%">
<col width="20%">
</colgroup>
<%for (int i=0; i<icons.size(); i++) {
	IconFile icon = (IconFile)icons.get(i);
%>
<tr>
<td><input type="radio" name="<%=Constants.SIGNATURE.TAG_PATH%>" value="<%=icon.getUrl()%>" <%if(icon.getUrl().equals(savedPath)){%>checked<%}%>><%=icon.getName()%></td>
<td><img height="35" width="150" border="0" alt="Click to enlarge" style="cursor:hand" src="/content/pl<%=icon.getUrl()%>" onClick="popupHelp('/content/pl<%=icon.getUrl()%>');"/></td>
</tr>
<%}%>
</table>
</div>
<div id="localDiv" style=" width:100%;<%if(!wasOnline){%>display:block<%}else{%>display:none<%}%>;">
<div class="login_component_holder">
<div class="login_label"><label>Signature image</label> </div>
<div class="login_value"><input type="file" name="localFile" value="" src="<%if(!wasOnline){%><%=savedPath%><%}%>"/></div>
</div>
</div>
<div class="login_component_holder">
<div class="login_label"><label>Name</label> </div>
<div class="login_value"><input type="text" name="<%=Constants.SIGNATURE.TAG_NAME%>" value="<%=currentName%>"/></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label>Title</label> </div>
<div class="login_value"><input type="text" name="<%=Constants.SIGNATURE.TAG_TITLE%>" value="<%=currentTitle%>"/></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label>&nbsp;</label> </div>
<div class="login_value" style="TEXT-ALIGN: right;">
	<input class="goButton" type="button" value="Save" onClick="submitForm()"/>&nbsp;
	<input class="goButton" type="button" value="Cancel" onClick="window.history.back()"/>
</div>
</div>
</form>

<cms:include property="template" element="foot" />