<%@page session="false"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%@page import="com.bp.pensionline.aataxmodeller.dto.MemberDetail"%>
<%@page import="com.bp.pensionline.aataxmodeller.handler.MemberLoadingService"%>
<%@page import="com.bp.pensionline.aataxmodeller.modeller.Headroom"%><cms:include property="template" element="head" />
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	String today = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
	if (currentUser != null)
	{
%>
<h1>AA Tax Modeller Debug</h1>
<p>This page allows you to check the headroom and AA tax status of an AC member</p>
<div class="login_component_holder">
<div class="login_label"><label for="bgroup">Bgroup</label></div>
<div class="login_value"><select id="bgroup" name="bgroup">
<option value="BPF" selected="selected">BPF</option>
<option value="BCF">BCF</option>
</select></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label for="refno">Refno</label></div>
<div class="login_value"><input id="refno" name="refno" type="text" /></div>
</div>
<div class="login_component_holder">
<div class="login_label"><label for="sdate">System date</label></div>
<div class="login_value">
	<input id="sdate" readonly="readonly" value="<%=today %>"/>&nbsp;
	<button id="sdate_trigger">...</button>
	<script type="text/javascript">
             var links = document.getElementsByTagName("link");
             var skins = {};
             for (var i = 0; i < links.length; i++) {
                     if (/^skin-(.*)/.test(links[i].id)) {
                             var id = RegExp.$1;
                             skins[id] = links[i];
                     }
             }


             for (var i in skins) {
				if (skins.hasOwnProperty(i))
       			skins[i].disabled = true;
			 }
			 if (skins['gold']) skins['gold'].disabled = false;
             new Calendar({
                     inputField: "sdate",
                     dateFormat: "%d-%b-%Y",
                     trigger: "sdate_trigger",
                     bottomBar: false,
                     onSelect: function() {                         
                         this.hide();
                 }
             });
	</script>
</div>
</div>
<div class="login_component_holder">
<div class="login_label">&nbsp;</div>
<div class="login_value"><input id="doauth" class="goButton" type="button" onclick="loadMember(); return false;" name="doauth" value="Go" /></div>
</div><br/>
<hr style="float: left; width: 100%;" />
<div id="running_div"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif</cms:link>" alt="running..." /></div>
<div id="result_div">
	<h2>Member details</h2>
	<table class="member_detail_table" cellspacing="0" cellpadding="0">
	    <tbody>
	        <tr>
	            <th class="longlabel" colspan="4">Member general details</th>
	        </tr>
	        <tr>
	            <td class="label" style="width: 160px;">Reference</td>
	            <td id="reference" class="value number" >&nbsp;</td>
	            <td class="label" style="width: 160px;">Pensionable salary</td>
	            <td id="salary" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">NI</td>
	            <td id="nino" class="value number">&nbsp;</td>
	            <td class="label">AVC fund</td>
	            <td id="avc_fund" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Name</td>
	            <td id="name" class="value number">&nbsp;</td>
	            <td class="label">AVER fund</td>
	            <td id="aver_fund" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Gender</td>
	            <td id="gender" class="value number">&nbsp;</td>
	            <td class="label">TVIN PR86 days</td>
	            <td id="tvin_a" class="value number">&nbsp;</td>            
	        </tr>    
	        <tr>
	            <td class="label">Date of birth</td>
	            <td id="date_of_birth" class="value number">&nbsp;</td>
	            <td class="label">TVIN PR06 days</td>
	            <td id="tvin_b" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">Joined company</td>
	            <td id="joined_company" class="value number">&nbsp;</td>
	            <td class="label">TVIN PO06 days</td>
	            <td id="tvin_c" class="value number">&nbsp;</td>            
	        </tr>     
	        <tr>
	            <td class="label">Joined scheme</td>
	            <td id="joined_scheme" class="value number">&nbsp;</td>
	            <td class="label">Augmentation days</td>
	            <td id="augmentation" class="value number">&nbsp;</td>            
	        </tr>
	        <tr>
	            <td class="label">55th birthday</td>
	            <td id="birthday_55th" class="value number">&nbsp;</td>
	            <td class="label">Lta</td>
	            <td id="lta" class="value number">&nbsp;</td>            
	        </tr>        
	        <tr>
	            <td class="label">60th birthday</td>
	            <td id="birthday_60th" class="value number">&nbsp;</td>
	            <td class="label">Banked EGP</td>
	            <td id="banked_egp" class="value number">&nbsp;</td>            
	        </tr> 
	        <tr>
	            <td class="label">65th birthday</td>
	            <td id="birthday_65th" class="value number">&nbsp;</td>
	            <td class="label">&nbsp;</td>
	            <td class="value number">&nbsp;</td>            
	        </tr> 
	    </tbody>
	</table>
	<div style="float: left;" id="scheme_absence_table">&nbsp;</div>	
	<h2>Headroom</h2>
	<div style="float: left;" id="headroom_table">&nbsp;</div>
	<h2>Tax Modeller</h2>	
	<table id="taxmodeller_table" class="taxmodeller_table" cellspacing="0" cellpadding="0">
	    <tbody>
	        <tr>
	            <th class="empty">&nbsp;</th>
	            <th class="label">Modelled options</th>
	        </tr>
	        <tr>
	            <td class="label">Benefit accrued to&nbsp;<span id="accrued_date">&nbsp;</span></td>
	            <td class="value number" id="accrued_to_date">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label">
	            	<a href="" onclick="popupSalarySlider(this, event); return false;">Pensionable salary at&nbsp;<span id="salary_date">&nbsp;</span></a>
	            </td>
	            <td class="value number" id="modelled_salary">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label">
	           	 	<a href="" onclick="popupTaxrateSlider(this, event); return false;">Marginal tax rate</a>						            
	            </td>
	            <td class="value number" id="modelled_taxrate">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="label">Tax due for&nbsp;<span id="tax_year">&nbsp;</span></td>
	            <td class="value number" id="tax_amount">&nbsp;</td>
	        </tr>
	        <tr>
	            <td class="empty" colspan="2">&nbsp;</td>
	        </tr>
			<tr>
	            <td class="label">Tax status</td>
	            <td class="status" id="tax_status">&nbsp;</td>
	        </tr>        
	    </tbody>
	</table>		
</div>	
<div class="model_dialog" id="salary_slider_panel">
	<div class="model_dialog_inner">
	<div class="model_dialog_title">Pensionable salary</div>
	<div class="model_dialog_content">
		<div style="float: left; width: 190px; padding-top: 10px;">
			<input id="input_salary" class="slider_value" name="input_salary" type="text" style="width: 114px;"/>&nbsp;&nbsp;
			<!--<div id="salary_slider">&nbsp;</div>-->
			<a href="" onclick="hideSalarySlider(); return false;">Cancel</a>
		</div>
		<div style="float: left; width: 120px; vertical-align: middle;">
			<a href="" onclick="modelSalary(); return false;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/model_button.png</cms:link>"></img></a>
		</div>		
	</div>
	</div>
</div>
<div class="model_dialog" id="taxrate_slider_panel">
	<div class="model_dialog_inner">
	<div class="model_dialog_title">Marginal tax rate</div>
	<div class="model_dialog_content">
		<div style="float: left; width: 190px; padding-top: 10px">
		<input id="input_taxrate" class="slider_value" name="input_taxrate" type="text" style="width: 114px;"/>&nbsp;&nbsp;
		<!-- <div id="taxrate_slider">&nbsp;</div> -->
			<a href="" onclick="hideTaxrateSlider(); return false;">Cancel</a>&nbsp;
		</div>
		<div style="float: left; width: 120px; vertical-align: middle;">
			<a href="" onclick="modelTaxrate(); return false;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/model_button.png</cms:link>"></img></a>
		</div>		
	</div>
	</div>
</div>
<%
	}
	else
	{
%>
<h1>Access denied</h1>
<p>Sorry! You do not have enough permissions to view this section. Please contact the PensionLine administrator for more information.</p>
<%
	}
%>
<cms:include property="template" element="foot" />