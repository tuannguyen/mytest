<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="com.bp.pensionline.aataxmodeller.util.DefaultConfiguration" %>
<%@ page import="com.bp.pensionline.aataxmodeller.dto.DefaultInfo" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.bp.pensionline.aataxmodeller.util.ConfigurationUtil"%>

<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>

<%@page import="com.bp.pensionline.aataxmodeller.util.DateUtil"%>
<%@page import="java.util.Date"%><cms:include property="template" element="head" />
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null && SystemAccount.getAdmCmsObject().userInGroup(currentUser.getName(), "Administrators"))
	{
%>
<H1>&nbsp;&nbsp;&nbsp;Tax Modeller configuration aaa</H1>

<div id="status" class="fakelink">
</div>

<div class="login_holder">
	<form method="post" action="" name="AATaxAdminForm">
	<%
		Calendar cal = Calendar.getInstance();
		String currentTaxYear = DateUtil.getTaxYearAsString(cal.getTime());
		
		// get current tax year's first year
		String firstYearStr = currentTaxYear.substring(0, currentTaxYear.indexOf("_"));
		int firstYearOfCurrentTaxYear = Integer.parseInt(firstYearStr);
		
		DefaultConfiguration configLoad = new DefaultConfiguration();
		Hashtable<String, DefaultInfo> infos = configLoad.getDefaultValues();
		HashMap<String, String> contentAa = configLoad.loadConfigurationsForAaCarryConfig();
		String contentCar = configLoad.loadConfigurationsForCarConfig();
		HashMap<String, String> contentServiceSalary = configLoad.loadConfigurationsForServiceSalary();
		TreeMap<String,String> LTAValues = configLoad.getLTAValues();
		TreeMap<String,String> CapValues = configLoad.getCapValues();
		Vector<String> vecKeys = new Vector<String>();
		Enumeration<String> enums = infos.keys();
		while (enums.hasMoreElements()) {
		    vecKeys.add(enums.nextElement());
		}
		Collections.sort(vecKeys); 	
		try {
	     	String key = null;
	     	DefaultInfo defInfo = null;
	     	for(int i=0;i<vecKeys.size();i++){
			    key = (String)vecKeys.get(i);
			    defInfo = infos.get(key);
			    String unit = "";
			    String display = "";
	%>
    	<%
	    		if (key.equals("AnnualAllowanceTaxRate")){
	    			unit = "%";
	    			display = "display: none;";
	    		}else if (key.equals("AnnualAllowance")){
	    			unit = "£";
	    		}else if (key.equals("AverageSalaryIncrease")){
	    			unit = "%";
	    			display = "display: none;";
	    		}else if (key.equals("AnnualInflation")){
	    			unit = "%";
	    		}else if (key.equals("LTA")){
	    			unit = "£";
	    		}
    	%>	    
		<div style="width: 85%; height: 12px; <%=display%>"  class="login_component_holder" style="margin-top: 4px">
			<div style="width: 220px; height: 12px" class="login_label">
			    	<label for="<%=key%>"> 	<%=defInfo.getLabel()%> (<%=unit%>)</label>
			    </div>		
			    <%if(key.equalsIgnoreCase("LTA")){ %> 
			    <div class="login_value"><input id="<%="LTA_" + currentTaxYear%>" name="<%=key%>" type="text" value="<%=defInfo.getValue()%>" readonly="true"/></div>	
			    <%}else if(key.equalsIgnoreCase("Capitalisation")){ %> 
			    <div class="login_value"><input id="<%="Capitalisation_" + currentTaxYear%>" type="text" value="<%=defInfo.getValue()%>" readonly="true"/></div>	
			    <%} else {%> 
		    	<div class="login_value"><input id="<%=key%>" name="<%=key%>" type="text" value="<%=defInfo.getValue()%>"/></div>
		    	<%} %>
		    	<%if(key.equalsIgnoreCase("LTA")){ %>
		    	<div class="login_value"> <img id="lta_update_image" title="Click to update values" alt="info button" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/info32.png" height="16px;" width="16px;"/></div>
		    	<%} %>
		    	<%if(key.equalsIgnoreCase("Capitalisation")){ %>
		    	<div class="login_value"> <img id="cap_update_image" title="Click to update values" alt="info button" src="/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/info32.png" height="16px;" width="16px;"/></div>
		    	<%} %>
		    	
		</div>		    	    
	<%             
	        }
		} 
		catch (Exception e) {
			// Don't particularly care
			System.out.println(e);
			response.getWriter().write(e + "<br />");
		}
		finally {
		}
	%>		
			<P>&nbsp;</P>
			<div class="login_component_holder">
			    <div class="login_label">&nbsp;</div>
			    <div class="login_value">&nbsp;</div>
			     <a href="#" onclick="document.AATaxAdminForm.reset(); return false">Cancel</a>&nbsp;
			    <input id="doauth" class="goButton" type="button" onclick="submitDefaultTaxModellerRequest();" name="doauth" value="Commit" />
			</div>
	</form>
</div>
<P>&nbsp;</P>
<form method="post" action="" name="AEAdminCarForm">
<div>
	<H1>&nbsp;&nbsp;&nbsp;My service car configuration</H1>
	<div id="statusCarConfig" class="fakelink"></div>
	<div style="width: 85%; height: 12px;"  class="login_component_holder" style="margin-top: 4px">
		<div style="width: 220px; height: 12px" class="login_label">
		    <label> Set options</label>				
		</div>	
<%
	String saloonSelected = "";
	String urbanSelected = "";
	String sportSelected = "";
	String variableSelected = "";
	if (contentCar != null && contentCar.equalsIgnoreCase("saloon"))
	{
		saloonSelected = "selected=\"selected\"";
	}
	else if (contentCar != null && contentCar.equalsIgnoreCase("urban"))
	{
		urbanSelected = "selected=\"selected\"";
	}
	else if (contentCar != null && contentCar.equalsIgnoreCase("sport"))
	{
		sportSelected = "selected=\"selected\"";
	}
	else if (contentCar != null && contentCar.equalsIgnoreCase("variable"))
	{
		variableSelected = "selected=\"selected\"";
	}
%>			
		<div class="login_value">
			<select id="serviceCar" name="serviceCar">
				<option value="saloon" <%=saloonSelected%>> Saloon </option>
				<option value="urban" <%=urbanSelected%>> Urban </option>
				<option value="sport" <%=sportSelected%>> Sport </option>
				<option value="variable" <%=variableSelected%>> Variable </option>
			</select>
		</div>				
	</div>
	<P>&nbsp;</P>
	<div class="login_component_holder">
	    <div class="login_label">&nbsp;</div>
	    <div class="login_value">&nbsp;</div>
	    <a href="#" onclick="document.AEAdminCarForm.reset(); return false">Cancel</a>&nbsp;
	    <input id="doServiceCarConf" class="goButton" type="button" onclick="submitCarConfig();" name="doServiceCarConf" value="Commit" />
	</div>
</div>
</form>
<P>&nbsp;</P>
<form method="post" action="" name="AEAdminSalaryForm">
<div>
	<H1>&nbsp;&nbsp;&nbsp;My service salary modelling limits</H1>
	<H1>&nbsp;&nbsp;&nbsp;&nbsp;(% of current salary)</H1>
	<div id="statusServiceSalary" class="fakelink"></div>
		<div style="width: 85%; height: 12px;"  class="login_component_holder" style="margin-top: 4px">
			<div style="width: 220px; height: 12px" class="login_label">
			    <label> <b> Lower bound </b> </label>				
			</div>		
			<div class="login_value"><input type="text" name="lowerBound" id="lowerBound" value="<%=contentServiceSalary.get(ConfigurationUtil.LOWER_BOUND)%>"> </input> </div>			
		</div>
		<P>&nbsp;</P>
		<div style="width: 85%; height: 12px;"  class="login_component_holder" style="margin-top: 4px">
			<div style="width: 220px; height: 12px" class="login_label">
			    <label> <b> Upper bound </b> </label>				
			</div>		
			<div class="login_value"><input type="text" name="uperBound" id="uperBound" value="<%=contentServiceSalary.get(ConfigurationUtil.UPER_BOUND)%> "> </input> </div>			
		</div>
		<P>&nbsp;</P>
		<div class="login_component_holder">
		    <div class="login_label">&nbsp;</div>
		    <div class="login_value">&nbsp;</div>
		    <a href="#" onclick="document.AEAdminSalaryForm.reset(); return false">Cancel</a>&nbsp;
		    <input id="doServiceSalary" class="goButton" type="button" onclick="submitServiceSalaryConfig();" name="doServiceSalary" value="Commit" />
		</div>
	
</div>
</form>

<div id="cap_popup" class="aa_popup">
<table border="0" width="100%">
		<tr>
			<td style="padding-top: 15px;"> <h2> Capitalisation Factor </h2></td>
		</tr>
		<tr>
			<td>
				<table width='100%' cellspacing='0' cellpadding='0' border="1" class="publishtable" style="margin-top: 30px;" >
					<tr>
					<th class='label'> Tax Year</th>
					<th class='label'> Value</th>
					<th class='label'> Add new value</th>
					</tr>
					<%
						if(CapValues.size()>0)
						{ 
							Iterator it = CapValues.keySet().iterator(); int i=0; 
							while(it.hasNext())
							{ 
								String key = (String)it.next();
								String yearForamt = key.substring(key.indexOf("_") + 1, key.length());
			    				yearForamt = yearForamt.replace('_','\\');
					%>
						<tr>
						<td><%=yearForamt %> </td>
						<td><input type="text" id="oldcap_<%=key%>" name="oldcap_<%=key%>" value="<%=CapValues.get(key)%>" readonly="true"> </input>  </td> </td>
						
						<td><input type="text" id="cap_<%=key%>" name="cap_<%=key%>"></input>&nbsp;&nbsp;
						<%
								String yearString = key.substring(key.indexOf("_") + 1, key.length());
								yearString = yearString.substring(0, yearString.indexOf("_"));
								int pipYear = Integer.parseInt(yearString);
								if(pipYear >= firstYearOfCurrentTaxYear)
								{
						%>
						<a href="#" id="cap<%=key%>link" onclick="updateCap('<%=key.toString()%>', '<%=pipYear%>');"> Update </a>
						<%
								} 
						%>
						</td>
						</tr>
					<%}} %>
				</table>
			</td>
		</tr>
	</table>
</div>
<div id="lta_popup" class="aa_popup">
<table border="0" width="100%">
		<tr>
			<td style="padding-top: 15px;"><h2> Lifetime allowance</h2></td>
		</tr>
		<tr>
			<td>
				<table width='100%' cellspacing='0' cellpadding='0' border="1" class="publishtable" style="margin-top: 30px;" >
					<tr>
					<th class='label'> Tax Year</th>
					<th class='label'> Value</th>
					<th class='label'> Add new value</th>
					</tr>
					
					<%
						if(LTAValues.size()>0)
						{ 
							Iterator it = LTAValues.keySet().iterator(); int i=0; 
							while(it.hasNext())
							{ 
								String key = (String)it.next();
								String yearForamt = key.substring(key.indexOf("_") + 1, key.length());
			    				yearForamt = yearForamt.replace('_','\\');
					%>
						<tr>
						<td><%=yearForamt %> </td>
						<td><input type="text" id="oldlta_<%=key%>" name="oldlta_<%=key%>" value="<%=LTAValues.get(key)%>" readonly="true"> </input>  </td>
						<td><input type="text" id="lta_<%=key%>" name="lta_<%=key%>"></input>&nbsp;&nbsp;
						<% 
								String yearString = key.substring(key.indexOf("_") + 1, key.length());
								yearString = yearString.substring(0, yearString.indexOf("_"));
								int pipYear = Integer.parseInt(yearString);
								if(pipYear >= firstYearOfCurrentTaxYear)
								{
						%>
						<a href="#" id="<%=key%>link" onclick="updateLTA('<%=key.toString()%>', '<%=pipYear%>');"> Update </a>
						<%
								} 
						%>
						</td>
						</tr>
					<%}} %>
				</table>
			</td>
		</tr>
	</table>
</div>
<script>
window.onload = initPopups;
</script>
<%
	}else{
%>
<h1>Access denied</h1>
<p>Sorry! You do not have enough permissions to view this section. Please contact the PensionLine administrator for more information.</p>
<div style="display: none;">Trick here for IE6</div>
<%
	}
%>
<cms:include property="template" element="foot" />
