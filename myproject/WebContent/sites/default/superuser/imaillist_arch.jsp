<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.bp.pensionline.constants.Environment" %>
<%@ page import="com.bp.pensionline.util.Base64Encoder" %>
<%@ page import="com.bp.pensionline.util.Base64Decoder" %>
<%@ page import="com.bp.pensionline.database.DBConnector" %>
<%@page import="java.text.DecimalFormat"%>
<cms:include property="template" element="head" />
<%

	String sqlQueryArchive = "UPDATE BP_IMAIL set Archived=NULL WHERE CaseWorkId=?";
	//String sqlQuerySelect  = "SELECT CreationDate,BGroup,Refno,NI,Subject,CaseWorkId FROM BP_IMAIL WHERE Archived is not null order by CreationDate desc Limit 0,50";
	String sqlQuerySelect  = "SELECT TOP 50 CreationDate,BGroup,Refno,NI,Subject,CaseWorkId FROM BP_IMAIL WHERE Archived is not null order by CaseWorkId desc";

	String unarchivestr = request.getParameter("_request_unarchivestr");
	boolean unarchive = true;
	if (unarchivestr == null || unarchivestr.trim().length() == 0) {
		unarchive = false;
	}

	String caseWorkId = request.getParameter("_request_caseworkid");
	if (caseWorkId== null) {
		caseWorkId= "0";
	}

	String queryParam = caseWorkId;

	Connection con = null;
	String imailCreationDate = "";
	String imailBgroup       = "";
	String imailRefno        = "";
	String imailNI           = "";
	String imailSubject      = "";
				
	try {
		DBConnector connector = DBConnector.getInstance();
		con = connector.getDBConnFactory(Environment.PENSIONLINE);
		con.setAutoCommit(false);
	} catch (Exception e) {
		// Don't particularly care
		System.out.println(e);
		response.getWriter().write(e + "<br />");
	}
	finally {
	}

	if(con != null && unarchive) {
		try {
			System.out.println("IMAIL SQL: "+queryParam+"-"+sqlQueryArchive);
			PreparedStatement pstm = con.prepareStatement(sqlQueryArchive);
			pstm.setString(1, queryParam);
			pstm.executeUpdate();
			con.commit();
		} catch (Exception e) {
			response.getWriter().write(e + "<br />");
			e.printStackTrace();
		}
		finally {
		}
	}







%>

<h1>Archived iMail list</h1>

<p>The last 50 archived iMails are listed below with the most recently requested at the top.</p>

<%
	if(con == null) {
%>
<p>The service is not available at the moment</p>
<%
	} else {
		try {

			PreparedStatement pstm = con.prepareStatement(sqlQuerySelect);

			ResultSet rs = pstm.executeQuery();        // Get the result table from the query

%>
<table width='100%' cellspacing='0' cellpadding='0' border="1">
	<tr>
		<th class='label'>Date</th>
		<th class='label'>BGroup</th>
		<th class='label'>RefNo</th>
		<th class='label'>NI</th>
		<th class='label'>Subject</th>
		<th class='label'>IMailNo</th>
	</tr>
<%
			DecimalFormat decForm = new DecimalFormat("0000000");
			while (rs.next()) {
				imailCreationDate = rs.getString(1);
				imailBgroup       = rs.getString(2);
				imailRefno        = rs.getString(3);
				imailNI           = rs.getString(4);
				imailSubject      = Base64Decoder.decode(rs.getString(5));
				int caseWorkIdInt = rs.getInt(6);
				caseWorkId 		  = decForm.format(caseWorkIdInt);
%>
	<tr>
		<td><%=imailCreationDate%></td>
		<td><%=imailBgroup%></td>
		<td><%=imailRefno%></td>
		<td><%=imailNI%></td>
		<td><%=imailSubject%></td>
		<td>
			<a title='Click here to view details' 
				href='imailviewer.jsp?_request_caseworkid=<%=caseWorkId%>'><%=caseWorkId%>
			</a>
			&nbsp;
			<a title='Click here to unarchive this iMail' 
				href='imaillist_arch.jsp?_request_caseworkid=<%=caseWorkId%>&amp;_request_unarchivestr=<%=caseWorkId%>'>[U]
			</a>
		</td>
	</tr>
<%
			}
%>
</table>
<%
			rs.close();
		} catch (Exception e) {
			response.getWriter().write(e + "<br />");
		} finally {
		}
	}


	if (con != null) {
		try {
			con.close();
		} catch (Exception e) {
			response.getWriter().write(e + "<br />");
		} finally {
		}
	}
%>
<cms:include property="template" element="foot" />
