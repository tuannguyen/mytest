<%@ page pageEncoding="UTF-8" %><%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.CmsJspLoginBean"%>
<%@ page import="org.opencms.main.CmsSessionInfo"%>
<%@ page import="java.util.*"%>
<%@page import="com.bp.pensionline.util.CheckConfigurationKey"%>

<%
	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request,	response);
	if (!cmsLogin.isLoggedIn()) {
		String username = CheckConfigurationKey.getStringValue("adminName");
		String password = CheckConfigurationKey.getStringValue("adminPassword");
		cmsLogin.login(username,password);
	}
	response.sendRedirect("/content/pl/system/workplace/editors/fckeditor/plugins/InsertSignature/signatureGallery.jsp");
%>