
<%@page import="com.bp.pensionline.authenticate.util.AuthenticationUtil"%>
<%@page import="com.bp.pensionline.authenticate.service.PLSelfService"%>
<%@page import="com.bp.pensionline.database.MigrationDataHandler"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="com.bp.pensionline.authenticate.BP1LDAPCallWorker"%>
<%@page import="com.bp.pensionline.authenticate.ldap.LDAPAuthenticateImpl"%>
<%@page import="com.bp.pensionline.authenticate.AuthObject"%>
<%@page import="com.bp.pensionline.authenticate.AuthenticationFactory"%>
<%@page import="com.bp.pensionline.authenticate.AuthenticationHandler"%>
<%@page import="com.bp.pensionline.authenticate.exception.AuthenticationException"%>
<%@page import="com.bp.pensionline.authenticate.util.StringUtil"%>
<%@page import="com.bp.pensionline.authenticate.exception.DAOException"%>
<%@page import="com.bp.pensionline.authenticate.util.AuthenticationHelper"%>
<%@ page pageEncoding="UTF-8" %>
<%@page import="org.opencms.db.*" %>
<%@ page import="org.opencms.main.*, org.opencms.jsp.*,org.opencms.file.*,java.util.*" %>
<%@ page import="com.bp.pensionline.handler.*" %>
<%@ page import="com.bp.pensionline.util.*" %>
<%@ page import="com.bp.pensionline.constants.*" %>
<%@ page import="com.bp.pensionline.util.SystemAccount" %>
<%@page import="com.bp.pensionline.webstats.producer.*"%>
<%@page import="com.bp.pensionline.webstats.*"%>
<%@page import="com.bp.pensionline.publishing.handler.EditorLandingHandler"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);

	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cmso=bean.getCmsObject();
    DataMigrationHandler handler=new DataMigrationHandler();
	
	// begin webstats for Login
	WebstatsLoginProducer wbspProducer=new WebstatsLoginProducer();

	// read parameters from the request
	String username = request.getParameter("_request_username");
	String password = request.getParameter("_request_password");
	
	// begin new with data migration
	// Taken from the welcome page if required
	String surname    = request.getParameter("surname");
	String payrollnum = request.getParameter("payrollnum");
	String hardreg    = request.getParameter("hardreg");
	
	String referingPage = (String)request.getSession().getAttribute("requestedResource");
	request.getSession().removeAttribute("requestedResource"); //remove immediately
	referingPage = referingPage==null ? new String("") : referingPage;
	System.out.println("REQUESTED: "+referingPage);

	//-------- The User exists in ASD56X -----------// - updated by Binh Nguyen
	boolean ldapAuth = false;
	boolean ninoAuth = false;
	AuthObject authObj = null;
	boolean user_asd = false;
	boolean multiRows = false;
    	boolean onlyNINO = false;
	
	if (!AuthenticationUtil.isSpecialUser(username)) {	
		if (AuthenticationHelper.isNINOUser(username)) {
			
			System.out.println("LH::NINO User - " + username);
			try {
				AuthenticationHandler authHandler = 
					AuthenticationFactory.getAuthenticationHandler(username, password);
				if (authHandler.authenticate()) {
					ninoAuth = true;
					onlyNINO = true;
				} else {
					System.out.println("LH:NINO fails to authenticate");
					response.sendRedirect("_login_error.html");
					return;
				}
			} catch (Exception ex) {
				System.out.println("LH:NINO auth fail: " + ex.getMessage());
				response.sendRedirect("_login_error.html");
				return;
			}
		} else {
			System.out.println("LH::BP1 User: Start checking user in ASD56X - " + username);
			try {
				username = AuthenticationHelper.correctBp1Name(username);
				authObj = AuthenticationHelper.getAuthObject(username);
				if (authObj != null) {
					System.out.println("LH::Check in ASD - " + username);
					user_asd = AuthenticationHelper.checkBP1UserExist(username, authObj);
				}
	
				if (user_asd) {
					System.out.println("LH::Existed in ASD - " + username);
					multiRows = AuthenticationHelper.checkBP1MultiRows(username);
					if (multiRows) {
						System.out.println("LH::Multi-Rows in ASD - " + username);
						response.sendRedirect("_login_error.html");
						return;
					}
					
					if (!AuthenticationHelper.isBlankPassword(username, password)) {
						LDAPAuthenticateImpl authHandler = 
							new LDAPAuthenticateImpl(username, password);
						
						System.out.println("LH::Authenticate with BP1 LDAP - " + username);
						ldapAuth = authHandler.ldapAuthenticate();
					}
				} // end-if
			} catch (DAOException de) {
				System.out.println("LH::Data access excption: " + de.getMessage());
			} catch (Exception ex) {
				System.out.println("LH::General excption: " + ex.getMessage());
			}
			
			System.out.println("LH::BP1 User: End checking user in ASD56X - " + username 
					+ " - " + user_asd);
		}
	}
	// Post-Process
	boolean isWebuser = false;
	if (ldapAuth) {
		System.out.println("LH::BP1 User Identified - " + username);
		if (authObj != null) { // again for surely
			String tempName = username;
			String ninoName = authObj.getNino().toLowerCase().trim();
			if ((authObj.getPlId() == null)
					|| (authObj.getPlId().trim().equals(""))) {
				ninoAuth = true;
				tempName = ninoName;
				username = AuthUtil.addBp1LdapPrefix(ninoName);
				System.out.println("LH::LDAP NINO - " + username);
			} else {
				tempName = authObj.getPlId().toLowerCase().trim();
				username = AuthUtil.addBp1LdapPrefix(authObj.getPlId().toLowerCase().trim());
				System.out.println("LH::LDAP PL ID - " + username);
			}
			
			try
			{		
				MigrationDataHandler mhl = new MigrationDataHandler();
				password = mhl.getPasswordForuser(ninoName);
				isWebuser = handler.migrateUserDetailsBP1User(tempName, password, ninoName);
				System.out.println("LH::isWebuser - " + isWebuser);
			}
			catch(Exception e)
			{
				response.sendRedirect("_login_error.html");
				System.out.println("LH::Error - " + e.getMessage());
				return;
			} // end-try-catch
			System.out.println("LH::Come from LDAP - " + username);
		} // end-if
	} else {
		if (user_asd) { // LDAP ID in Oracle but may be a wrong password
			System.out.println("LH::BP1 ID in Oracle but not authenticated: " 
					+ username);
			response.sendRedirect("_login_error.html");
			return;
		}
		
		if (AuthUtil.findBp1LdapPrefix(username)) {
			username = AuthUtil.removeBp1LdapPrefix(username);
		}
		System.out.println("LH::NOT Come from LDAP - " + username);
		try
		{		
			if (!AuthenticationUtil.isSpecialUser(username)) {
				System.out.println("LH::Check web user - " + username);
				if (onlyNINO) {
					isWebuser = handler.migrateUserDetails(username, password);
				} else {
					AuthenticationHelper.migrateUserDetails(username, password);
				}
	   			
			}
		}
		catch(Exception e)
		{
			// password not match or error in data migration
			System.out.println("LH::Exception in data migration: " + e.toString());
			response.sendRedirect("_login_error.html");
			return;
		}
	}
	//-------- End User exists in ASD56X -----------//

	//set User-Agent info into session
	TextUtil.sessionParameters.set(request.getSession().getId(),request.getHeader("User-Agent"));
	System.out.println(request.getSession().getId() 
			+ "---" 
			+ request.getHeader("User-Agent"));

	if ((isWebuser) || (AuthenticationUtil.isWebuser(username))){
		username = SystemAccount.webUnitName + username;
		System.out.println("LH::Web user name: " + username);
	}
	
	cms.login(username,password);
	System.out.println("LH::after logging in: " + username);
	// Make sure you remove marked user after a login action above
	if (AuthUtil.findBp1LdapPrefix(username)) {
		username = AuthUtil.removeBp1LdapPrefix(username);
		System.out.println("LH::Remove prefix: " + username);
	}
	
	if (cms.isLoginSuccess()) 
	{
		//request.getSession().setAttribute("referingPage", referingPage);
		//set Login status -> success
       	request.getSession().setAttribute(WebstatsConstants.OUT_COME, 
       				WebstatsConstants.SUCCESS);
        
		
   		CmsUser cmsUser = cms.getRequestContext().currentUser();
   		String cmsUserName = username;
		cmsUser.setAdditionalInfo(Environment.MEMBER_USERNAME, username);
		cmsUser.setAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL,cms.user("description"));
		
		// Store user's password in if user in Publishing groups
		if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_GROUP) || 
				cmso.userInGroup(username, Environment.PUBLISHING_REVIEWER_GROUP) ||
				cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||				
				cmso.userInGroup(username, Environment.PUBLISHING_AUTHORISER_GROUP) ||
				cmso.userInGroup(username, Environment.PUBLISHING_DEPLOYER_GROUP))
		{
			System.out.println("User in publishing groups");
			cmsUser.setAdditionalInfo(Environment.MEMBER_USERPASSWORD, password);
		}		
	
		// Add or remove BP1 group
		try {
			String bp1_user_group = "BP1_User_group";
			SwapUserHandler sw = SwapUserHandler.getInstance();
			if (ldapAuth) {
				sw.swapUser(cmsUserName, bp1_user_group, 
						SystemAccount.getAdminCmsObject());
			} else {
				AuthenticationUtil.unswap(cmsUserName, bp1_user_group, 
						SystemAccount.getAdminCmsObject());
			}
		} catch (Exception ex) {
			System.out.println("Error in managing BP1 group: " + ex.getMessage());
		}
		
        if (cmsUser.isWebuser() && !cmso.userInGroup(username,"Superusers") 
            	&& !cmso.userInGroup(username,"Testers")) {
			try {
				String fss_user_group = Environment.GROUP_FORCE_SELF_SERVICE_USER;
				if (onlyNINO) {
					System.out.println("LH:NINO user and go to change nino: ");
					cmsUser.setAdditionalInfo(Environment.MEMBER_USERPASSWORD, password);
					SwapUserHandler sw = SwapUserHandler.getInstance();
					
					sw.swapUser(cmsUserName, fss_user_group, 
							SystemAccount.getAdminCmsObject());
					response.sendRedirect("Self_services/nino_self_service.html");
					return;
				} else {
					AuthenticationUtil.unswap(cmsUserName, fss_user_group, 
							SystemAccount.getAdminCmsObject());
				}
				
				String []acceptGroup={"Members","WALL_ALL_ACCESS"};
				
				// remove any group previous accept acceptGroup[]
				SwapUserHandler.getInstance().unSwapUser(cmsUserName, acceptGroup, 
						SystemAccount.getAdminCmsObject());

				// run the webstats process
				request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Members");
				wbspProducer.runStats(request);
				//referingPage = "";
				response.sendRedirect("_login_periodofservice.jsp?referingPage=" + referingPage);
			}
			catch (Exception e) {		
				System.out.println("LH:Error in redirecting web user: " + e.getMessage());
				response.sendRedirect("_login_error.html");
			} //end catch
		} //end if	
    	else if (cmso.userInGroup(username,"Superusers"))
    	{
    		String []superUserGroup = {"Superusers","WALL_ALL_ACCESS"};
    		SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
    				SystemAccount.getAdminCmsObject());

    		request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"SuperUser");
			wbspProducer.runStats(request);					
			if (referingPage.trim().length()>0) {
				response.sendRedirect(cms.link(referingPage));
			} else {			
				response.sendRedirect("superuser/index.html"); 
			}    	
		}	
    	else if (cmso.userInGroup(username,"PL_REPORT_EDITOR")) {			    			
    		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/sql_report/manage_report/index.html"));		
		}
	} 
    	else if (cmso.userInGroup(username,"PL_REPORT_RUNNER") || cmso.userInGroup(username,"PL_REPORT_SCHEDULER")) {
    		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/sql_report/index.html"));
		}
	}	        		
	else if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_GROUP)) {
			String []superUserGroup = {Environment.PUBLISHING_EDITOR_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	    			SystemAccount.getAdminCmsObject());
	
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentEditor");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
	}
	else if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)) {
		String []superUserGroup = {Environment.PUBLISHING_EDITOR_REVIEWER_GROUP};
	   	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	   			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentEditor");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
	 	if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
	}		
	else if (cmso.userInGroup(username, Environment.PUBLISHING_REVIEWER_GROUP)) {
		String []superUserGroup={Environment.PUBLISHING_REVIEWER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	    			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentChecker");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
	}
	else if (cmso.userInGroup(username, Environment.PUBLISHING_AUTHORISER_GROUP)) {
		String []superUserGroup = {Environment.PUBLISHING_AUTHORISER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	    			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "ContentAuthoriser");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}		
	}
	else if (cmso.userInGroup(username, Environment.PUBLISHING_DEPLOYER_GROUP)) {
		String []superUserGroup = {Environment.PUBLISHING_DEPLOYER_GROUP};
	   	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	   			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE, "SystemDeployer");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/publishing/index.html"));	
		}	
	}
	else if (cmso.userInGroup(username,"Testers")) {		
		String []superUserGroup={"Testers","Members"};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	    			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Tester");
		wbspProducer.runStats(request);
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect("index.html");
		}
	}
	else if (cmso.userInGroup(username,"Publishers")) {
		String []superUserGroup={"Publishers","Members"};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, superUserGroup,
	    			SystemAccount.getAdminCmsObject());
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Publisher");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/system/workplace/views/workplace.jsp"));
		}
		//response.sendRedirect(cms.link("index.html"));		
	}
		//else if (cmsUser.isSystemUser(1) )
	else if(cmso.userInGroup(username, "Administrators")) {
		request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"SystemUser");
		wbspProducer.runStats(request);
		cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
		if (referingPage.trim().length()>0) {
			response.sendRedirect(cms.link(referingPage));
		} else {
			response.sendRedirect(cms.link("/system/workplace/views/workplace.jsp"));
		}
	}
    } else {//logged in unsuccessful
    	request.getSession().setAttribute(WebstatsConstants.OUT_COME,WebstatsConstants.FAILURE);
   	 	request.getSession().setAttribute(WebstatsConstants.USER_TYPE,"Guest");
        wbspProducer.runStats(request);    
    	// redirect users to login error page if authentication fails
        response.sendRedirect("_login_error.html");

    }
%>
