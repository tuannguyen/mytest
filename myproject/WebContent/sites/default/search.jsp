<%@ page pageEncoding="UTF-8" %><%@ page import="org.opencms.jsp.*" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>


<cms:include property="template" element="head" />

<%@ page buffer="none" import="org.opencms.main.*, org.opencms.search.*, org.opencms.file.*, org.opencms.jsp.*, java.util.*" %><%   
    
    // Create a JSP action element
    org.opencms.jsp.CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
    
    // Get the search manager
    CmsSearchManager searchManager = OpenCms.getSearchManager(); 
%>

<jsp:useBean id="search" scope="request" class="org.opencms.search.CmsSearch">
    <jsp:setProperty name = "search" property="*"/>
    <% 
    		search.init(cms.getCmsObject());     		
    %>
</jsp:useBean>

<TABLE><TR><TD width=10px></TD><TD>

<form method="post" action="result.jsp">
<h1>Search</h1>
<table>
	<tr>
		<th valign="top">Query:</th>
		<td><input type="text" name="query" size="50" value="<%=(cms.property("query") != null) ? cms.property("query") : ""%>"></td>
	</tr>
	<tr>
		<th valign="top"></th>
		<td>	                      
	             <%
                      Iterator k = searchManager.getIndexNames().iterator(); 
                      String indexName2 = (String)k.next();
	             %>
                      <input type="hidden" name="index" value='<%=indexName2%>'>
		</td>
	</tr>
	<tr>
		<th valign="top">Matches per Page:</th>
		<td>
			<select name="matchesperpage">
			<% for(int i=1; i<21; i++) { %>
				<option value="<%=i%>"><%=i%></option>
			<% } %>
			</select>
		</td>
	</tr>
	<tr>
		<th valign="top">Display Pages:</th>
		<td>
			<select name="displaypages">
			<% for(int j=1; j<11; j++) { %>
				<option value="<%=j%>"><%=j%></option>
			<% } %>
			</select>
		</td>
	</tr>
	
	


<tr><th valign="top">Fields:</th><td>
<input type="checkbox" name="field" value="title" 
    <%=(cms.property("field") == null || cms.property("field").indexOf("title") >= 0) ? "checked" : "" %>>Title<br>
<input type="checkbox" name="field" value="keywords" 
    <%=(cms.property("field") == null || cms.property("field").indexOf("keywords") >= 0) ? "checked" : "" %>>Keywords<br>
<input type="checkbox" name="field" value="description" 
    <%=(cms.property("field") == null || cms.property("field").indexOf("description") >= 0) ? "checked" : "" %>>Description<br>
<input type="checkbox" name="field" value="content" 
    <%=(cms.property("field") == null || cms.property("field").indexOf("content") >= 0) ? "checked" : "" %>>Content<br>
</td></tr>
<tr><td colspan=2 align=center><input type="submit" value="Submit"></td>
</tr>
</table>
<p>




</form>

</TD></TR></TABLE>


<cms:include property="template" element="foot" />
