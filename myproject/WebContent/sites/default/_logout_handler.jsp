<%@ page pageEncoding="UTF-8" %><%@ page import="org.opencms.jsp.*" %>
<%@ page import="org.opencms.file.*" %>
<%@ page import="com.bp.pensionline.handler.*" %>
<%@ page import="com.bp.pensionline.util.*" %>
<%@ page import="com.bp.pensionline.webstats.producer.*" %>
<%@ page import="com.bp.pensionline.constants.*" %>
<%@ page import="java.util.*" %>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="com.bp.pensionline.letter.handler.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", -1);
  %>

 <HEAD> 
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"> 
  </HEAD> 

	
	


<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);


	if (cms.isLoggedIn())
	{
	    CmsUser cmsUser = cms.getRequestContext().currentUser();
		try {
			if (cmsUser !=null){
				
				WebstatsLogoutProducer logoutProducer = new WebstatsLogoutProducer();
	            logoutProducer.runStats(request);

				CmsObject obj=SystemAccount.getAdminCmsObject();
				String currentGroup=String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_CURRENT_GROUP));
				
				String userName=String.valueOf(cmsUser.getName());
				
				String bGroup=String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_BGROUP));
				String refNo=String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_CREFNO));
				
				// Unswap publishing groups
				String editorGroup = "";
				String reviewerGroup = "";
				String editorReviewrGroup = "";
				String authoriserGroup = "";
				String deployerGroup = "";	
				String reportEditorGroup = "";
				String reportRunnerGroup = "";		
				String reportSchedulerGroup = "";				
				if (obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP)){
					editorGroup = Environment.PUBLISHING_EDITOR_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP)){
					reviewerGroup = Environment.PUBLISHING_REVIEWER_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)){
					editorReviewrGroup = Environment.PUBLISHING_EDITOR_REVIEWER_GROUP;
				}				
				if (obj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP)){
					authoriserGroup = Environment.PUBLISHING_AUTHORISER_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP)){
					deployerGroup = Environment.PUBLISHING_DEPLOYER_GROUP;
				}	
				if (obj.userInGroup(userName,"PL_REPORT_EDITOR"))
				{
					reportEditorGroup = "PL_REPORT_EDITOR";			
				}
				else if (obj.userInGroup(userName,"PL_REPORT_RUNNER"))
				{
					reportRunnerGroup = "PL_REPORT_RUNNER";
				}
				if (obj.userInGroup(userName,"PL_REPORT_SCHEDULER"))
				{
					reportSchedulerGroup = "PL_REPORT_SCHEDULER";
				}
							
				
				if (obj.userInGroup(userName,"Administrators")){
					String acceptGroup[]={"Administrators", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup, deployerGroup, reportEditorGroup, reportRunnerGroup, reportSchedulerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}
				else if (obj.userInGroup(userName,"Projectmanagers")){
					String acceptGroup[]={"Projectmanagers", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup, deployerGroup, reportEditorGroup, reportRunnerGroup, reportSchedulerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}				
				else if (obj.userInGroup(userName,"Superusers")){
					String acceptGroup[]={"Superusers", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup, reportEditorGroup, reportRunnerGroup, reportSchedulerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
					// Added by Huy
					PasswordLetterHandler.clearAllPasswordSessionData(request);
				}	
				else if (obj.userInGroup(userName,"PL_REPORT_EDITOR") || obj.userInGroup(userName,"PL_REPORT_RUNNER") || obj.userInGroup(userName,"PL_REPORT_SCHEDULER"))
				{
					String acceptGroup[]={reportEditorGroup, reportRunnerGroup, reportSchedulerGroup };
					SwapUserHandler.getInstance().swapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
					// Added by Huy
					request.getSession().setAttribute("SQLReportContent", null);					
				}			
				else if (obj.userInGroup(userName,"Publishers"))
				{
					String acceptGroup[]={"Publishers","Members", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}	
				else if (	obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP) || 
						obj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP) ||
						obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||						
						obj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP) ||
						obj.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP))
				{
					String acceptGroup[]={editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().swapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}
				else if (obj.userInGroup(userName,"Testers")){
					String acceptGroup[]={"Testers","Members"};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}
				else 
				{
					String acceptGroup[]={"Members"};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}								
			
				WallHandler.removeUserFromWallBlock(userName, bGroup, refNo);
				
			}
			
			

			SystemAccount.logout(request);

			
		}catch (Exception e){
			//cms.logout();
			e.printStackTrace();
		}
		
	}
	
	response.sendRedirect("_logout_confirm.html");
%>


