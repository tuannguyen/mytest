<%@ page pageEncoding="UTF-8" %><%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.file.CmsUser"%>
<%@ page import="org.opencms.util.CmsUUID"%>
<%@ page import="org.opencms.main.CmsSessionInfo"%>
<%@ page import="com.bp.pensionline.handler.*"%>
<%@ page import="com.bp.pensionline.util.*"%>
<%@ page import="com.bp.pensionline.constants.*"%>
<%@ page import="java.util.*"%>
<%@page import="com.bp.pensionline.dao.RecordDao"%>
<%@page import="com.bp.pensionline.dao.CompanySchemeDao"%>
<%@page import="com.bp.pensionline.dao.MemberDao"%>
<%@page import="com.bp.pensionline.factory.DataAccess"%>
<%@page import="com.bp.pensionline.database.DBMemberConnectorExt"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="com.bp.pensionline.webstats.producer.*"%>
<cms:include property="template" element="head" />
<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request,	response);



	CmsUser cmsUser = SystemAccount.getCurrentUser(request);
	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cmso=bean.getCmsObject();
	WebstatsPosProducer wbspProducer=new WebstatsPosProducer();

	String userName = null;
	String referingPage = "mydetails/index.html";
	String action = request.getParameter("action");action=action==null?new String(""):action;
	//get the refereing page to forward the user onto 
	String referingPagePassedIn = request.getParameter("referingPage"); // should be mydetails/this_is_me.html
	//String referingPagePassedIn = (String)request.getSession().getAttribute("referingPage");

	
	if (referingPagePassedIn != null&& referingPagePassedIn.trim() != "") {
		referingPage = cms.link(referingPagePassedIn);
	}

	if (cmsUser!=null){
		userName=String.valueOf(cmsUser.getName());
		
	
	
	List recordList = (List) cmsUser.getAdditionalInfo(Environment.RECORD_LIST);

	if (recordList == null || recordList.size() == 0) {		
		RecordLoader recordLoader = new RecordLoader(cms.user("description"), request);
		recordList = recordLoader.getRecords();
		RecordLoader.LOG.info("Load POS service for member: " + cms.user("description") + ". Number of records: " + recordList.size());
		cmsUser.setAdditionalInfo(Environment.RECORD_LIST, recordList);


	}else {

	}

	System.out.println("LOGIN PERIOD OF SERVICE. Action: "+action+". Current user: "+userName);
	if (!"posted".equals(action) && recordList.size() == 1) {
		RecordDao record = (RecordDao) recordList.get(0);

		String schemeRaw = record.get(Environment.MEMBER_SCHEME_RAW);
		String statusRaw = record.getStatusRaw();


		//String status=record.getStatusRaw();
		cmsUser.setAdditionalInfo(Environment.MEMBER_BGROUP, record
		.getBgroup());
		cmsUser.setAdditionalInfo(Environment.MEMBER_REFNO, record
		.getRefno());
		MemberDao memberDao = DataAccess.getDaoFactory().getMemberDao(
		record.getBgroup(), record.getRefno(), null);

		memberDao.set("SessionId", ((CmsUUID)request.getSession().getAttribute(CmsSessionInfo.ATTRIBUTE_SESSION_ID)).getStringValue());
		cmsUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
		request.getSession().setAttribute("Nino", memberDao.get("Nino"));
		// Huy modified - Move calcs to pages (28/02/2010)
		//CalcProducerFactory producerFactory = new CalcProducerFactory();
		//producerFactory.setMemberDao(memberDao);
		//producerFactory.start();
		//--- end of modify
		try
		{
			MemberDao memberDaoExt = (MemberDao)memberDao.clone();
			
			memberDaoExt.setCmsUUID(memberDao.getCmsUUID());
			memberDaoExt.setCurrentUser(memberDao.getCurrentUser());
			
			DBMemberConnectorExt dbMemberConnectorExt = new DBMemberConnectorExt();
			dbMemberConnectorExt.setMemberDao(memberDaoExt);
			dbMemberConnectorExt.start();
		}
		catch (Exception e)
		{
			System.err.println("Error while getting extension data for this member from login period of services");
		}	
	
		String companyGroup = CompanySchemeRawHandler.getInstance().getCompanyGroup(record.getBgroup(), schemeRaw);
	
		
		String firstNewGroup = "MEM_" + companyGroup + "_" + statusRaw;
		String secondNewGroup = "MEM_" + statusRaw;
		String thirdNewGroup = "MEM_" + companyGroup;
		String []acceptGroup={"Members",thirdNewGroup,secondNewGroup,firstNewGroup};
		//		 remove any group previous accept acceptGroup[]
		SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup,
		SystemAccount.getAdminCmsObject());
		WallHandler.addUserToWallBlock(userName, record.getBgroup(),
		record.getRefno());

		wbspProducer.runStats(request);

		if (UserExpriedHandler.checkUserExit(userName)){
			System.out.println("\n\n\n +++++++++ member in FORCE_PASSWORD_CHANGE group 1 +++++ \n\n");
			if (cmso.userInGroup(userName, Environment.GROUP_BP1_USER)) {
				System.out.println("Refering page: " + referingPage);
				response.sendRedirect(referingPage);
			} else {
				System.out.println("Refering page: I_want_to/change_password.html");
				SwapUserHandler.getInstance().swapUser(userName,Environment.FORCE_PASSWORD_CHANGE,SystemAccount.getAdminCmsObject());
				response.sendRedirect("I_want_to/change_password.html");
			}
		}else {
			System.out.println("\n\n\n +++++++++ member not  in FORCE_PASSWORD_CHANGE group +++++ \n\n");
			response.sendRedirect(referingPage);
		}
		

		//response.sendRedirect(referingPage);

	} else if ("posted".equals(action)) {

		String bGroup = request.getParameter("tmpBGROUP");
		String refNo = request.getParameter("tmpCREFNO");
		String status = request.getParameter("tmpSTATUS");
		String schemeRaw = request.getParameter("tmpSCHEMERAW");

		CompanySchemeDao companySchemeDao=(CompanySchemeDao)cmsUser.getAdditionalInfo(Environment.COMPANY_SCHEME);
		String companyGroup = CompanySchemeRawHandler.getInstance()
		.getCompanyGroup(bGroup, schemeRaw);
		//set bgroup, refno to the session
		cmsUser.setAdditionalInfo(Environment.MEMBER_BGROUP, bGroup);
		cmsUser.setAdditionalInfo(Environment.MEMBER_REFNO, refNo);

		//set member dao
		MemberDao memberDao = DataAccess.getDaoFactory().getMemberDao(
		bGroup, refNo, null);
		//memberDao.set("SessionId",request.getSession().getId());
		memberDao.set("SessionId", ((CmsUUID)request.getSession().getAttribute(CmsSessionInfo.ATTRIBUTE_SESSION_ID)).getStringValue());
		cmsUser.setAdditionalInfo(Environment.MEMBER_KEY, memberDao);
		request.getSession().setAttribute("Nino", memberDao.get("Nino"));
		
		// Huy modified - Move calcs to pages (28/02/2010)
		//CalcProducerFactory producerFactory = new CalcProducerFactory();
		//producerFactory.setMemberDao(memberDao);
		//producerFactory.start();
		//--- end of modify
		try
		{
			MemberDao memberDaoExt = (MemberDao)memberDao.clone();
			
			memberDaoExt.setCmsUUID(memberDao.getCmsUUID());
			memberDaoExt.setCurrentUser(memberDao.getCurrentUser());
			
			DBMemberConnectorExt dbMemberConnectorExt = new DBMemberConnectorExt();
			dbMemberConnectorExt.setMemberDao(memberDaoExt);
			dbMemberConnectorExt.start();
		}
		catch (Exception e)
		{
			System.err.println("Error while getting extension data for this member from login period of services");
		}
		
		Object currentGroup = cmsUser
		.getAdditionalInfo(Environment.MEMBER_CURRENT_GROUP);
		if (currentGroup != null) {
			String[] currentGroups = { "Members" };
			SwapUserHandler.getInstance().unSwapUser(userName,
			currentGroups, SystemAccount.getAdminCmsObject());
		}
		String firstNewGroup = "MEM_" + companyGroup + "_" + status;
		String secondNewGroup = "MEM_" + status;
		String thirdNewGroup = "MEM_" + companyGroup;
		String []acceptGroup={"Members",thirdNewGroup,secondNewGroup,firstNewGroup};
		SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup,
		SystemAccount.getAdminCmsObject());
		WallHandler.addUserToWallBlock(userName, bGroup, refNo);
		wbspProducer.runStats(request);

		if (UserExpriedHandler.checkUserExit(userName)){
			System.out.println("\n\n\n +++++++++ member in FORCE_PASSWORD_CHANGE group 3 +++++ \n\n");
			if (cmso.userInGroup(userName, Environment.GROUP_BP1_USER)) {
				System.out.println("Refering page: " + referingPage);
				response.sendRedirect(referingPage);
			} else {
				System.out.println("Refering page: I_want_to/change_password.html");
				SwapUserHandler.getInstance().swapUser(userName,Environment.FORCE_PASSWORD_CHANGE,SystemAccount.getAdminCmsObject());
				response.sendRedirect("I_want_to/change_password.html");
			}
		}else {
			System.out.println("\n\n\n +++++++++ member not  in FORCE_PASSWORD_CHANGE group +++++ \n\n");
			response.sendRedirect(referingPage);
		}
		

		
		

		//cmsUser.setAdditionalInfo(Environment.MEMBER_CURRENT_GROUP,
		//firstNewGroup);

		//redirect to this is me page
		

	} else{
		if (UserExpriedHandler.checkUserExit(userName)){
			wbspProducer.runStats(request);

			System.out.println("\n\n\n +++++++++ member in FORCE_PASSWORD_CHANGE group 3 +++++ \n\n");
			if (cmso.userInGroup(userName, Environment.GROUP_BP1_USER)) {
				System.out.println("Refering page: " + referingPage);
				response.sendRedirect(referingPage);
			} else {
				System.out.println("Refering page: I_want_to/change_password.html");
				SwapUserHandler.getInstance().swapUser(userName,Environment.FORCE_PASSWORD_CHANGE,SystemAccount.getAdminCmsObject());
				response.sendRedirect("I_want_to/change_password.html");
			}
		}else {
			wbspProducer.runStats(request);

			System.out.println("\n\n\n +++++++++ member not  in FORCE_PASSWORD_CHANGE group +++++ \n\n");
			response.sendRedirect("mydetails/index.html");
		}

		
	}
	}else {
		response.sendRedirect("_login_ask.html");
	}
	%>

