<%@ page import="org.opencms.jsp.*" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
	String errorStatus = (String)request.getSession().getAttribute("errorStatus");
	String errorRef = (String)request.getSession().getAttribute("errorRef");
	String errorMessage = (String)request.getSession().getAttribute("errorMessage");
	String errorDes = (String)request.getSession().getAttribute("errorDes");

%>

<cms:include property="template" element="head" />

<%
if (errorStatus != null && errorStatus.equals("404")){
%>
<h1>The requested page was not found</h1>
<div id='pagebody'>
	<p>Sorry, the page you are looking for could not be found</p>
</div>
<p class="login_holder">If you require access to a specific area of the site,&nbsp;
please contact the <a href="/content/pl/contactus.html">PensionLine</a> team on +44 (0) 845 601 1142.</p>
<%
}
else {
%>


<h1>Pensionline Error</h1>
<h2>Unfortunately, it has not been possible to complete your request.</h2>
<p>   An error has occurred.  For further details please contact the PensionLine team on +44 (0) 845 601 1142.</p>
<p>The error that occurred was </p><p><%=errorMessage%></p>
<p> Quote the error reference: <%=errorRef%>
<p>The Pensions Administration Team address and contact details are as follows:</p>
<p> Address:<br />
Chertsey Road<br />
Sunbury-on-Thames<br />
Middlesex<br />
TW16 7LN
</p>
<p>E mail: pensionlineuk@bp.com</p>
<p> Telephone:
<ul>
<li>BP Pension Fund : +44 (0) 845 602 1063</li>
<li>Burmah Castrol Pension Fund : +44 (0) 845 600 0295</li>
</ul>
</p>
<p>Thank you for your patience.</p>



<!-- PENSIONLINE DEBUG -->
<!-- MESSAGE: <%=errorMessage%> --> 
<!-- STACKTRACE: <%=errorDes%>--> 


<%
}
%>

<cms:include property="template" element="foot" />