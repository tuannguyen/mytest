<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="com.bp.pensionline.constants.Environment" %>
<%@ page import="com.bp.pensionline.database.DBConnector" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.sql.DataSource" %>


<%

	String sqlQuerySelect  = "SELECT SYSDATE FROM DUAL";
	String sqlMySQLQuerySelect  = "SELECT getDate() as Now";
	String sqlWebstatQuerySelect  = "Select event_date as event_timestamp from BP_STATS_ACCESS where seqno = ( select max(seqno) from BP_STATS_ACCESS)";

	Connection con = null;
	String oracleDate = "0000-00-00 00:00:00.0";	
	String oracleDataSourceDate = "0000-00-00 00:00:00.0";
	String mysqlDate = "0000-00-00 00:00:00.0";	
	String webstatDate = "0000-00-00 00:00:00.0";
	Statement stm = null;
	ResultSet rs = null; 	

	try {
/*		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
					"org.jnp.interfaces.NamingContextFactory");
		env.put(Context.PROVIDER_URL, "jnp://127.0.0.1:1099");
		env.put(Context.URL_PKG_PREFIXES,
					"org.jboss.naming:org.jnp.interfaces");
		Context initContext = new InitialContext(env);
		DataSource ds = (DataSource) initContext
					.lookup("PensionLine_XAOracleDS");
		Connection conn = ds.getConnection();

		stm = conn.createStatement();

		rs = stm.executeQuery(sqlQuerySelect);
		
		if (rs.next()) {
			oracleDataSourceDate = rs.getString("SYSDATE");
		}
		
*/		
		DBConnector connector = DBConnector.getInstance();
	
		con = connector.getDBConnFactory(Environment.AQUILA);

		stm = con.createStatement();

		rs = stm.executeQuery(sqlQuerySelect);
		
		if (rs.next()) {
			oracleDate = rs.getString("SYSDATE");
		}

                	
                connector.close(con);

		con = connector.getDBConnFactory(Environment.WEBSTATS);
		stm = con.createStatement();
		rs = stm.executeQuery(sqlMySQLQuerySelect);
		
		if (rs.next()) {
			mysqlDate = rs.getString("Now");
		}

		stm = con.createStatement();
		rs = stm.executeQuery(sqlWebstatQuerySelect);
		
		if (rs.next()) {
/*
			String pad = "0";
			String year = rs.getString("event_year");
			String month = pad + rs.getString("event_month");
			String day = pad + rs.getString("event_day");
			String hour = pad + rs.getString("event_hour");
			String minute = pad + rs.getString("event_minute");
			String second = pad + rs.getString("event_second");


			webstatDate = year;
			webstatDate += "-" + month.substring(month.length()-2, month.length());
			webstatDate += "-" + day.substring(day.length()-2, day.length());
			webstatDate += " " + hour.substring(hour.length()-2, hour.length());
			webstatDate += ":" + minute.substring(minute.length()-2, minute.length());
			webstatDate += ":" + second.substring(second.length()-2, second.length());
			webstatDate += ".0";
*/
			webstatDate = rs.getString("event_timestamp");
		}



	} catch (Exception e) {
		// Don't particularly care
		e.printStackTrace();
	} finally {
		if (con != null) {
			try {
				DBConnector connector = DBConnector.getInstance();
				connector.close(con);

			} catch (Exception ex) {
				// Don't particularly care
				ex.printStackTrace();
			}
		}
	}	


%>


<html>
<head>
<title>Big brother monitor</title>
</head>
<body>
<table>
	<tbody>
		<tr>
			<th>Option</th>
			<th>Value</th>
		</tr>
		<tr>
			<td class='value'>Oracle sysdate</td><td><%=oracleDate%></td>
		</tr>
		<tr>
			<td class='value'>MS SQL sysdate</td><td><%=mysqlDate%></td>
		</tr>
		<tr>
			<td class='value'>Last webstat</td><td><%=webstatDate%></td>
		</tr> 
			</tbody>
</table>
</body>
</html>