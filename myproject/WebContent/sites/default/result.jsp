<%@page import="com.bp.pensionline.webstats.producer.*"%>
<%@page import="com.bp.pensionline.webstats.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<cms:include property="template" element="head" />
<%@ page pageEncoding="UTF-8" %><%@ page buffer="none" import="org.opencms.main.*, org.opencms.search.*, org.opencms.file.*, org.opencms.jsp.*, java.util.*, java.text.SimpleDateFormat" %><%   
    
    // Create a JSP action element
    org.opencms.jsp.CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
    
    // Get the search manager
    CmsSearchManager searchManager = OpenCms.getSearchManager(); 
%>

<jsp:useBean id="search" scope="request" class="org.opencms.search.CmsSearch">
    <jsp:setProperty name = "search" property="*"/>
    <% 
    		search.init(cms.getCmsObject()); 		
    %>
</jsp:useBean>

<!--<div id='advanced_search'><a href="<cms:link>/search.jsp</cms:link>">Advanced Search</a></div>-->

<%!
   public String getQueryString(String searchQuery)
   {
      	String specialChars = "<>\"'%;)(&+-";		
		String[] replaceChars = {"&lt;","&gt;","&quot;","'","%",";",")","(","&amp;","+","-"};
		
		int i = 0;
		int indexSearched = -1;
		StringBuffer queryBuffer = new StringBuffer(searchQuery);
		while(i<queryBuffer.length()){
			indexSearched = specialChars.indexOf(queryBuffer.charAt(i));
			if(indexSearched>=0){
				queryBuffer.replace(i, i+1, replaceChars[indexSearched]);
				i = i + replaceChars[indexSearched].length();
			}
			else
				i++;
		}
	
	return queryBuffer.toString();
   }
%>


<h1>Search results</h1>
<%
	String[] searchRoots = new String[] {"/system/galleries/download/","/"};
	search.setSearchRoots(searchRoots);
	int resultno = 1;
	int pageno = 0;
	if (request.getParameter("searchPage")!=null) {		
		pageno = Integer.parseInt(request.getParameter("searchPage"))-1;
	}
	resultno = (pageno*search.getMatchesPerPage())+1;
	
	String fields = search.getFields();
   if (fields==null) {
   	fields = request.getParameter("fields");
   }
   
   //System.out.println("search query: " + getQueryString(search.getQuery())); 
    
   List result = search.getSearchResult();
   
  if (result == null || result.size() == 0) {
%>
<h3>No results found for '<strong><%=getQueryString(search.getQuery())%></strong>'</h3>
<%    
  } else {
    
        ListIterator iterator = result.listIterator();
%>

<h3>Displaying results <%= resultno %> to <%= resultno + result.size() - 1 %> for '<strong><%

	out.print(getQueryString(search.getQuery()));
	
%></strong>'</h3>


<%
	WebstatsSearchProducer wbsProducer=new WebstatsSearchProducer();
        request.getSession().setAttribute(WebstatsConstants.SEARCH_STRING, search.getQuery());
        request.getSession().setAttribute(WebstatsConstants.RESULT_COUNT, String.valueOf(search.getSearchResult().size()));
	wbsProducer.runStats(request);
%>
<%
        while (iterator.hasNext()) {
            CmsSearchResult entry = (CmsSearchResult)iterator.next();
		SimpleDateFormat date_formatter = new SimpleDateFormat("dd MMM yyyy");
		String last_mod = date_formatter.format(entry.getDateLastModified());
		SimpleDateFormat time_formatter = new SimpleDateFormat("kk:mm");
		last_mod += " at about " + time_formatter.format(entry.getDateLastModified());
		String entryTitle = entry.getTitle();
		String entryLink = cms.link(cms.getRequestContext().removeSiteRoot(entry.getPath()));		
		String anchor = "";
		
		if (entryLink != null && entryLink.indexOf("/faq/") >= 0)
		{
			
			String faqFolder = entryLink.substring(0, entryLink.indexOf("/faq/") + "/faq/".length());
			String fileName = entryLink.substring(entryLink.indexOf("/faq/") + "/faq/".length());
			
			
			// if entry got is the header file eg LTA_header.html
			if (fileName.indexOf("_header.html") != -1)
			{
				fileName = fileName.replace("_header.html", "_faq.jsp");
			}
			else
			{
				anchor = "#/faq/" + fileName;
			}
			
			// if entry got is the the struture content
			if (fileName.indexOf("/") >= 0)
			{
				fileName = fileName.substring(0, fileName.indexOf("/")) + "_faq.jsp";
			}
			
			if (entryTitle != null)
			{
				entryTitle = entryTitle.replace(" header", "");
			}
			
			entryLink = faqFolder + fileName;
		}		
		else if (entryLink != null && entryLink.indexOf("/dictionary/") >= 0)
		{
			// if entry got is the header file eg LTA_header.html
			if (entryLink.indexOf("_header.html") == -1)
			{
				anchor = "#" + entryLink.substring(entryLink.indexOf("/dictionary/"));
			}
			
			if (entryLink.indexOf("/dictionary/BP/") >= 0)
			{
				entryLink = entryLink.substring(0, entryLink.indexOf("/dictionary/") + "/dictionary/".length()) + "bp_dictionary.jsp";
			}
			else if (entryLink.indexOf("/dictionary/BC/") >= 0)
			{
				entryLink = entryLink.substring(0, entryLink.indexOf("/dictionary/") + "/dictionary/".length()) + "bc_dictionary.jsp";
			}
						
			if (entryTitle != null)
			{
				entryTitle = entryTitle.replace(" header", "");
			}
			
							
		}	
		else if (entryLink != null && entryLink.indexOf("/RSS/") >= 0)
		{
			// if entry got is the header file eg LTA_header.html
			if (entryLink.indexOf("_header.html") == -1)
			{
				anchor = "#" + entryLink.substring(0, entryLink.indexOf("/RSS/"));
			}
			if (entryLink.indexOf("/RSS/eventsRss/") >= 0)
			{
				entryLink = entryLink.substring(0, entryLink.indexOf("/RSS/") + "/RSS/".length()) + "eventsChannel.jsp";
			}
			else if (entryLink.indexOf("/RSS/newsRss/") >= 0)
			{
				entryLink = entryLink.substring(0, entryLink.indexOf("/RSS/") + "/RSS/".length()) + "newsChannel.jsp";
			}
						
			if (entryTitle != null)
			{
				entryTitle = entryTitle.replace(" header", "");
			}			
					
		}		
		
		entryLink = entryLink + "?query=" + getQueryString(search.getQuery()).trim() + anchor;	
		if (entryTitle == null && entry.getExcerpt() != null)
		{
			entryTitle = entry.getExcerpt().substring(0, 40) + "...";
		}
%>
                                <hr>  
				<h3><%= resultno %>.&nbsp;<a href="<%=entryLink  %>"><%=entryTitle %></a>&nbsp;(<%= entry.getScore() %>%)</h3>
                                <% if (entry.getKeywords()!=null){%>
				       <h6>Keywords</h6>
				       <%= entry.getKeywords() %>
                                <%}%>

                                <% if (entry.getDescription()!=null){%>
				       <h6>Description</h6>
				       <%= entry.getDescription() %>
                                <%}%>

                               <% if (entry.getExcerpt()!=null){
					if (entry.getDescription()!=null || entry.getKeywords()!=null){%>
				       <h6>Excerpt</h6>
					<%}
						String m_excerpt = entry.getExcerpt();
						m_excerpt = m_excerpt.replace("[[","[");
    						m_excerpt = m_excerpt.replace("]]","]");
    					out.println ("<p>" + m_excerpt + "</p>");%>
				            <%}%>
       
                                <% if (entry.getDateLastModified()!=null){%> 
				       <h6>Last modified on <%= last_mod %></h6>
			        <%}%> 

<%
        resultno++;            
        }
    }
%> 
<hr>
<%
	if (search.getPreviousUrl() != null) {
%>
		<input class="goButton" type="button" value="previous page" onclick="location.href='<%= cms.link(search.getPreviousUrl()) %>&fields=<%= fields %>';" />
<%
	} else {
%>
		<input class="disabledButton" type="button" value="previous page" />
<%
	}
	
	Map pageLinks = search.getPageLinks();		
	int numberOfPages = pageLinks.size();
	boolean needAdjust = true;
	
	String lastSearchQuery = (String)request.getSession().getAttribute("SEARCH_QUERY");
	
	if (lastSearchQuery != null && lastSearchQuery.equalsIgnoreCase(search.getQuery().trim()))
   	{
		needAdjust = false;
   	}
	else
	{
		request.getSession().setAttribute("SEARCH_QUERY", search.getQuery().trim());
	}
	
	if (needAdjust == true)
	{
		numberOfPages = numberOfPages - 1;
		request.getSession().setAttribute("SEARCH_PAGES", new Integer(numberOfPages));
	}
	else
	{
		Integer numberOfPagesInt = ((Integer)request.getSession().getAttribute("SEARCH_PAGES"));
		if (numberOfPagesInt != null)
		{
			numberOfPages = numberOfPagesInt.intValue();
		}
	}	
		   
	// Modified by Lampt for re-calculate number of pages need to be displayed
	/*
	if (pageLinks.size() > 1)
		numberOfPages = pageLinks.size() - 1; 
	else 
		numberOfPages = pageLinks.size();
	*/
	for (int pageNumber=1; pageNumber <= numberOfPages; pageNumber++)
	{
		String pageLink = cms.link((String)pageLinks.get(new Integer(pageNumber)));       		
		out.print("&nbsp;");
		if (pageNumber != search.getSearchPage()) {
%>
			<a href="<%= pageLink %>&fields=<%= fields %>"><%= pageNumber %></a>
<%
		} else {
%>
			<span class="currentpage"><%= pageNumber %></span>
<%
		}		
	}
	if (search.getSearchPage() < numberOfPages ) {
%>
		&nbsp;<input class="goButton" type="button" value="next page" onclick="location.href='<%= cms.link(search.getNextUrl()) %>&fields=<%= fields %>';" />
<%
	} else {
	
%>
		&nbsp;<input class="disabledButton" type="button" value="next page" />
<%
	}
%>

<cms:include property="template" element="foot" />