<%@ page pageEncoding="UTF-8" %><%@page import="com.bp.pensionline.dao.RecordDao"%>
<%@page import="com.bp.pensionline.dao.CompanySchemeDao"%>
<%@page import="org.opencms.db.*" %>
<%@ page import="org.opencms.main.*, org.opencms.jsp.*,org.opencms.file.*,java.util.*" %>
<%@ page import="com.bp.pensionline.handler.*" %>
<%@ page import="com.bp.pensionline.util.*" %>
<%@ page import="com.bp.pensionline.constants.*" %>
<%@ page import="com.bp.pensionline.util.SystemAccount" %>
<%@page import="com.bp.pensionline.dao.MemberDao"%>
<%@page import="com.bp.pensionline.factory.DataAccess"%>
<%@page import="com.bp.pensionline.webstats.producer.*"%>
<%@page import="com.bp.pensionline.webstats.*"%>
<%@page import="com.bp.pensionline.publishing.handler.EditorLandingHandler"%>
<%@page import="com.bp.pensionline.letter.handler.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);
	//override the default value of login attempts, max bad attempts: 3, disable time: 15 mins
	//CmsLoginManager cmsLoginManager = new CmsLoginManager (15, 3);
	//int badAttempts = cmsLoginManager.getMaxBadAttempts();
	//System.out.println("login bad pwd" + badAttempts );

	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cmso=bean.getCmsObject();
    DataMigrationHandler handler=new DataMigrationHandler();

	// read parameters from the request
	CmsUser currentCmsUser = cms.getRequestContext().currentUser();
	
	// Get publishing account with TB_ prefix
	String username = (String)currentCmsUser.getAdditionalInfo(Environment.MEMBER_USERNAME);
	String password = (String)currentCmsUser.getAdditionalInfo(Environment.MEMBER_USERPASSWORD);

	if (cms.isLoggedIn())
	{
		try 
		{
			if (currentCmsUser != null)
			{

				CmsObject obj=SystemAccount.getAdminCmsObject();
				String currentGroup=String.valueOf(currentCmsUser.getAdditionalInfo(Environment.MEMBER_CURRENT_GROUP));
				
				String userName=String.valueOf(currentCmsUser.getName());
				
				String bGroup=String.valueOf(currentCmsUser.getAdditionalInfo(Environment.MEMBER_BGROUP));
				String refNo=String.valueOf(currentCmsUser.getAdditionalInfo(Environment.MEMBER_CREFNO));
				
				
				// Unswap publishing groups
				String editorGroup = "";
				String reviewerGroup = "";
				String editorReviewrGroup = "";
				String authoriserGroup = "";
				String deployerGroup = "";				
				if (obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP)){
					editorGroup = Environment.PUBLISHING_EDITOR_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP)){
					reviewerGroup = Environment.PUBLISHING_REVIEWER_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)){
					editorReviewrGroup = Environment.PUBLISHING_EDITOR_REVIEWER_GROUP;
				}				
				if (obj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP)){
					authoriserGroup = Environment.PUBLISHING_AUTHORISER_GROUP;
				}
				if (obj.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP)){
					deployerGroup = Environment.PUBLISHING_DEPLOYER_GROUP;
				}				
				
				if (obj.userInGroup(userName,"Administrators")){
					String acceptGroup[]={"Administrators", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}
				else if (obj.userInGroup(userName,"Projectmanagers")){
					String acceptGroup[]={"Projectmanagers", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}				
				else if (obj.userInGroup(userName,"Superusers")){
					String acceptGroup[]={"Superusers", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
					// Added by Huy
					PasswordLetterHandler.clearAllPasswordSessionData(request);
				}	
				else if (obj.userInGroup(userName,"Publishers"))
				{
					String acceptGroup[]={"Publishers","Members", editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}	
				else if (obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP) || 
						obj.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP) ||
						obj.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||						
						obj.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP) ||
						obj.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP))
				{
					String acceptGroup[]={editorGroup, reviewerGroup, editorReviewrGroup, authoriserGroup, deployerGroup};
					SwapUserHandler.getInstance().swapUser(userName, acceptGroup, SystemAccount.getAdminCmsObject());
				}
				else if (obj.userInGroup(userName,"Testers")){
					String acceptGroup[]={"Testers","Members"};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}
				else 
				{
					String acceptGroup[]={"Members"};
					SwapUserHandler.getInstance().unSwapUser(userName,acceptGroup,SystemAccount.getAdminCmsObject());	
				}	

			
				WallHandler.removeUserFromWallBlock(userName, bGroup, refNo);
				
			}
						
			SystemAccount.logout(request);			

			
		}catch (Exception e){
			//cms.logout();
			e.printStackTrace();
		}
		
	}
	
	cms.login(username,password);


	if (cms.isLoginSuccess()) 
	{				
   		String cmsUserName = cms.getUserName();
		// reset current username
       	currentCmsUser = cms.getRequestContext().currentUser();
       	currentCmsUser.setAdditionalInfo(Environment.MEMBER_USERNAME, username);
       	currentCmsUser.setAdditionalInfo(Environment.MEMBER_USERPASSWORD, password);
       	currentCmsUser.setAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL, cms.user("description"));	
       	      			
		if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_GROUP))
		{
			String[] publishingGroups = {Environment.PUBLISHING_EDITOR_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, publishingGroups, SystemAccount.getAdminCmsObject());
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			//EditorLandingHandler.synchronizeResources (request);
			//System.out.println("Editor: Redirect to recent changes...");
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_REVIEWER_GROUP))
		{
			String[] publishingGroups={Environment.PUBLISHING_REVIEWER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, publishingGroups, SystemAccount.getAdminCmsObject());
	    	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			
			//System.out.println("Reviewer: Redirect to recent changes...");
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP))
		{
			String[] publishingGroups={Environment.PUBLISHING_EDITOR_REVIEWER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, publishingGroups, SystemAccount.getAdminCmsObject());
	    	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			
			//System.out.println("Reviewer: Redirect to recent changes...");
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}		
		else if (cmso.userInGroup(username, Environment.PUBLISHING_AUTHORISER_GROUP))
		{
			String[] publishingGroups = {Environment.PUBLISHING_AUTHORISER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, publishingGroups, SystemAccount.getAdminCmsObject());
	
			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
		else if (cmso.userInGroup(username, Environment.PUBLISHING_DEPLOYER_GROUP))
		{
			String[] publishingGroups = {Environment.PUBLISHING_DEPLOYER_GROUP};
	    	SwapUserHandler.getInstance().swapUser(cmsUserName, publishingGroups, SystemAccount.getAdminCmsObject());

			cmso.getRequestContext().setCurrentProject(cmso.readProject("Offline"));
			response.sendRedirect(cms.link("/publishing/index.html"));		
		}
    } 
	else 
    {
    	// redirect users to login error page if authentication fails
        response.sendRedirect("_login_error.html");
 	}
%>
