<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<cms:include property="template" element="head" />
<cms:editable />

<cms:include page="./reportFAQs_header.html" element="bptitle" />
<cms:include page="./reportFAQs_header.html" element="bpblank" />

<!-- questions and answers -->
<div>
<cms:contentload collector="allInFolderNavPos" param="/sql_report/faq/faq_${number}.html|bp_pensionline_faq" editable="true">

<div class="element" name='top'>
<p>
  <a href ="#<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Question" /></a>
</p>
</div>

</cms:contentload>
</div>
<br/><br/>
<div class="faq_list">
<!-- questions and answers -->
<cms:contentload collector="allInFolderNavPos" param="/sql_report/faq/faq_${number}.html|bp_pensionline_faq" editable="true">
<hr/>
<div class="elements">
  <h3><a name ="<cms:contentshow element="%(opencms.filename)" />"><cms:contentshow element="Question" /></a></h3>
  <p>
    <cms:contentshow element="Answer" />
	<div class="totop"><a href="#top">Back to top of page</a></div>
  </p>
</div>

</cms:contentload>
</div>
<cms:include property="template" element="foot" />
