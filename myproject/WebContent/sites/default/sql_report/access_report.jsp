<%@ page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.sqlreport.dao.ReportGroupDao"%>
<cms:include property="template" element="head" />
<h1>Access reports</h1>
<p>The list contains all available reports assigned to your account.</p>
<p>Please select a group of reports to run.</p>
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null)
	{
		String userName = currentUser.getName();
		Vector reportGroups = new Vector();
		if (ReportGroupDao.isUserInCmsGroup(userName, "PL_REPORT_EDITOR"))
		{
			reportGroups = ManageReportGroupHandler.listAllReportGroups();
		}
		else if (ReportGroupDao.isUserInCmsGroup(userName, "PL_REPORT_RUNNER"))
		{
			reportGroups = ManageReportGroupHandler.listReportGroupsOfUser(currentUser.getId().toString());
		}
%>
<div class="ws_report_list" style="BORDER-TOP: #ccc 1px solid; MARGIN-TOP: 2em">
<%
		for (int i = 0; i < reportGroups.size(); i++)
		{
			ReportGroup report = (ReportGroup)reportGroups.elementAt(i);
			if (report != null)
			{
				String groupId = report.getId();
				String name = report.getName();
				if (name == null || name.trim().equals(""))
				{
					name = "Unknown";
				}
%>
<div class="ws_report" style="BORDER-TOP: #ccc 1px solid; PADDING-TOP: 10px">
<div class="ws_report_icon"><img alt="<%=name%>" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/reportGroupIcon.gif" /></div>
<div class="ws_report_text">
<a style='float:right;' href="schedule_report_ingroup.jsp?groupId=<%=groupId%>">Schedule reports</a>
<h2><a href="access_report_ingroup.jsp?groupId=<%=groupId%>"><%=name%></a></h2>
</div>
<div class="ws_report_text">
<a style='float:right;' href="scheduled_reports_inbox.jsp?groupId=<%=groupId%>">Scheduled inbox</a>
</div>
</div>
<%
			}
		}
%>
</div>
</div>
<%
	}
%>
<cms:include property="template" element="foot" />