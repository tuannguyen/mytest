<%@page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.sqlreport.app.jasper.PLReportProducer"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="com.bp.pensionline.sqlreport.dao.ScheduleDao"%>
<%@page import="org.opencms.jsp.CmsJspLoginBean"%> 

<cms:include property="template" element="head" />
<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);
	String groupId = request.getParameter("groupId");
	String reportRunner = "";
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null)
	{
		reportRunner = currentUser.getName();
		String userId = currentUser.getId().toString();
		if (UpdateUserInReportGroupHandler.isUserInCmsGroup(reportRunner, "PL_REPORT_EDITOR") || 
				(UpdateUserInReportGroupHandler.isUserInCmsGroup(reportRunner, "PL_REPORT_RUNNER") && UpdateUserInReportGroupHandler.isUserAddedToGroup(userId, groupId)))
		{
%>
<script type="text/javascript">
	reportGroupId = '<%=groupId%>';
	reportRunnerId = '<%=userId%>';
</script>
<%
			Vector reports = ManageSQLReportHandler.listAllReportsInGroup(groupId);
			String groupName = "";
			ReportGroup reportGroupDTO = ManageReportGroupHandler.getReportGroupInfo(groupId);
			if (reportGroupDTO != null)
			{
				groupName = reportGroupDTO.getName();
			}
			String outputType = "XLS";
			String startDate = "";
			String endDate = "";
			String isCopyDateCheck = "checked = 'checked'";
			String crondExp = "";
			String destEmails = "";
			Schedule schedulerDTO = ScheduleDao.getReportScheduler(groupId, userId);
			Vector selectedReports = null;
			if (schedulerDTO != null)
			{
				outputType = schedulerDTO.getOutputType();
				startDate = schedulerDTO.getStartDate();
				endDate = schedulerDTO.getEndDate();
				isCopyDateCheck = schedulerDTO.isCopyDate() ? " checked=\"checked\"" : "";
				crondExp = schedulerDTO.getCrondExpression();
				destEmails = schedulerDTO.getDestEmails().replaceAll(";", "\n");
				selectedReports = ScheduleSQLReportHandler.getReportsFromCommand(schedulerDTO.getCommand());
			}
%>
<script type="text/javascript">
runningReportArray = new Array();
reportStartDate = '<%=startDate%>';
reportEndDate = '<%=endDate%>';
selectedRunningReportArray = new Array();
	<%
			for (int i = 0; i < reports.size(); i++)
			{
				Report reportDTO = (Report)reports.elementAt(i);
				if (reportDTO != null)
				{
					String reportId = reportDTO.getReportId();
					String reportTitle = reportDTO.getReportTitle();
					Vector sections = ManageSQLReportHandler.listSectionsOfMaster(reportId);
	%>			
	var runningSectionArray = new Array();
	<%	
					for (int j=0; j < sections.size(); j++)
					{
						ReportSection section = (ReportSection)sections.get(j);
						String sectionId = section.getSectionId();
						String sectionTitle= section.getSectionTitle();
						Vector sectionParams = PLReportProducer.getSectionParams(reportId, sectionId);
						if (sectionParams != null && sectionParams.size() > 0)
						{
	%>
	var runningParamArray = new Array();
	<%
							for (int k=0; k < sectionParams.size(); k++)
							{
								SQLSectionParameterDTO param = (SQLSectionParameterDTO)sectionParams.elementAt(k);
								String paramName = param.getName();
								int paramType = param.getType();
								String paramDefaultValue = param.getDefaultValue();
	%>
	runningParamArray.push(Array("<%=paramName%>", <%=paramDefaultValue%>, "<%=paramType%>"));
	<%
							}
	%>
	runningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>", runningParamArray));
	<%
						}
						else
						{
	%>
	runningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>"));
	<%
						}
					}
	%>	
	runningReportArray.push(Array("<%=reportId%>", "<%=reportTitle%>", runningSectionArray));
	<%
			
				}
			}
	%>	
	
	<%
			if (selectedReports != null)
			{
				for (int i = 0; i < selectedReports.size(); i++)
				{
					Report reportDTO = (Report)selectedReports.elementAt(i);
					if (reportDTO != null)
					{
						String reportId = reportDTO.getReportId();
						String reportTitle = reportDTO.getReportTitle();
						Vector sections = reportDTO.getSections();
	%>			
	var selectedRunningSectionArray = new Array();
	<%	
						for (int j=0; j < sections.size(); j++)
						{
							ReportSection section = (ReportSection)sections.get(j);
							String sectionId = section.getSectionId();
							String sectionTitle= section.getSectionTitle();
							Vector sectionParams = section.getParams();
							if (sectionParams != null && sectionParams.size() > 0)
							{
	%>
	var selectedRunningParamArray = new Array();
	<%
								for (int k=0; k < sectionParams.size(); k++)
								{
									SQLSectionParameterDTO param = (SQLSectionParameterDTO)sectionParams.elementAt(k);
									String paramName = param.getName();
									int paramType = param.getType();
									String paramValue = param.getValue();
	%>
	selectedRunningParamArray.push(Array("<%=paramName%>", "<%=paramValue%>", "<%=paramType%>"));
	<%
								}
	%>
	selectedRunningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>", selectedRunningParamArray));
	<%
							}
							else
							{
	%>
	selectedRunningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>"));
	<%
							}
						}
	%>	
	selectedRunningReportArray.push(Array("<%=reportId%>", "<%=reportTitle%>", selectedRunningSectionArray));
	<%
			
					}
				}
			}
	%>	
</script>
<form id="access_report_form">
<h1>Schedule report running: <%=groupName%></h1>
<p>Please fill in the fields below for the schedule the report.</p>
<p>Choose which sections from the Group of reports to include.</p>
<p>A link to the report will be sent via email (please separate emails with a newline).</p>
<p>Set the schedule time in the crond section:</p>
<p>
Seconds (* 0 -60)<br>
minute (* 0 - 59)<br>
hour (* 0 - 23)<br>
day of month (* 1 - 31)<br>
month (* 1 - 12)<br>
day of week (* 0 - 6) (Sun=0 or 7)<br>
e.g. * 15 18 ? * MON - indicates run every Monday at 06:15pm
</p>
<p>After you press 'Schedule' button, the schedule will be activated.</p>
<p>You can activate or deactivate the schedule by click 'Activate' or 'Deactivate' button.</p>
<h2>General report parameters</h2>
<div class="login_component_holder">
	<div class="login_label"><label>Report runner</label> </div>
	<div class="login_value"><input type="text" id="report_runner" size="25" value="<%=reportRunner%>"/></div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label for="output">Report output type</label> </div>
	<div class="login_value"><select id="report_output_type">
<%
			if (outputType.equals("XLS"))
			{
%>	
	<option value="XLS" selected="selected">Download as a XLS document</option>	
	<option value="PDF">Download as a PDF document</option>	
<%
			}
			else if (outputType.equals("PDF"))
			{
%>		
	<option value="XLS">Download as a XLS document</option>	
	<option value="PDF" selected="selected">Download as a PDF document</option>	
<%
			}
%>		
	</select>
	</div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label>Report start date</label> </div>
	<div class="login_value">
		<input type="radio" onclick="showHideDateSelector('report_start_date_value', true);" name="report_start_date" value="FIXED_DATE"/>Fixed date &nbsp;
		<input style="visibility: hidden;" type="text" id="report_start_date_value" onclick="return showCalendar('report_start_date_value', '%e-%b-%y');"/><br>
		<input type="radio" onclick="showHideDateSelector('report_start_date_value', false);" name="report_start_date" value="RUNNING_DATE"/>Date at running<br>		
	</div>
	<div class="login_label">&nbsp;</div>
	<div class="login_value">&nbsp;</div>	
</div>
<div class="login_component_holder">
	<div class="login_label"><label>Report end date</label> </div>
	<div class="login_value">
		<input type="radio" onclick="showHideDateSelector('report_end_date_value', true);" name="report_end_date" value="FIXED_DATE"/>Fixed date &nbsp;
		<input style="visibility: hidden;" type="text" id="report_end_date_value" onclick="return showCalendar('report_end_date_value', '%e-%b-%y');"/><br>
		<input type="radio" onclick="showHideDateSelector('report_end_date_value', false);" name="report_end_date" value="RUNNING_DATE"/>Date at running<br>
		<input type="radio" onclick="showHideDateSelector('report_end_date_value', false);" name="report_end_date" value="RUNNING_DATE_PLUS_1WEEK"/>After date running 1 week	<br>		
		<input type="radio" onclick="showHideDateSelector('report_end_date_value', false);" name="report_end_date" value="RUNNING_DATE_PLUS_1MONTH"/>After date running 1 month	<br>	
		<input type="radio" onclick="showHideDateSelector('report_end_date_value', false);" name="report_end_date" value="RUNNING_DATE_PLUS_1YEAR"/>After date running 1 year	<br>					
	</div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label>Use date in reports</label> </div>
	<div class="login_value"><input type="checkbox" id="copyDateToSection" <%=isCopyDateCheck%>/></div>
</div>
<h2>Schedule settings</h2>
<div class="login_component_holder">
	<div class="login_label"><label>Crond expression(*)</label> </div>
	<div class="login_value"><input type="text" id="crond_expression" size="25" value="<%=crondExp%>"/></div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label>Send to emails(*)</label> </div>
	<div class="login_value"><textarea id="destination_emails"  rows="5" cols="50"><%=destEmails%></textarea></div>
</div>

<h2>Select reports</h2>
<div class="report_para" style="border-top: 0px;">
	<div>
	<span class="report_table_select_all"><input id="cb_all_reports" type="checkbox" checked="checked" onclick="SelectAllReports();"/>&nbsp;Select all reports</span>
	</div>
</div>
<div class="report_list">
<%
			for (int i = 0; i < reports.size(); i++)
			{
				Report report = (Report)reports.elementAt(i);
				if (report != null)
				{
					String reportId = report.getReportId();
					String title = report.getReportTitle();
					String description = report.getReportDescription();
					if (title == null || title.trim().equals(""))
					{
						title = "Unknown";
					}
					if (description == null) description = "&nbsp;";
					Vector sections = ManageSQLReportHandler.listSectionsOfMaster(reportId);
%>
	<div class="report_para">
		<h3><%=title%></h3>
		<p><%=description%></p>	
		<div id="rp_comment_<%=i%>">		
			<div>
			<span class="report_table_select_all"><input id="cb_<%=reportId%>" type="checkbox" checked="checked" onclick="SelectAllSections('<%=reportId%>');"/>&nbsp;Select all sections</span>
			<%	
					for (int j=0; j < sections.size(); j++)
					{
						ReportSection section = (ReportSection)sections.get(j);
			%>	
			<span class="report_table_sub_select" id="span_<%=reportId%>_<%=section.getSectionId()%>">
				<input id="cb_<%=reportId%>_<%=section.getSectionId()%>" type="checkbox" checked="checked" onclick="UpdateSelectAllSections('<%=reportId%>');"/>
				&nbsp;<%=section.getSectionTitle()%>
			</span>
			<%
					}
			%>			
			</div>			
		</div>		
	</div>
<%
				}
			}
%>
</div>
<div class="report_component_holder">
<%
			if (schedulerDTO == null)
			{
%>
<div style="TEXT-ALIGN: right"><input class="goButton" id="_schedule_report" type="button" onclick="scheduleSelectedReports('ADD');" value="Add scheduler" />&nbsp;</div>
<%
			}
			else
			{
%>
<div style="TEXT-ALIGN: right">
<%
				if (schedulerDTO.getState() == 1)
				{
%>
	<input class="goButton" id="_schedule_report_de" type="button" onclick="scheduleSelectedReports('DEACTIVATE');" value="Deactivate" />&nbsp;
<%
				}
				else
				{
%>	
	<input class="goButton" id="_schedule_report_de" type="button" onclick="scheduleSelectedReports('ACTIVATE');" value="Activate" />&nbsp;
<%
				}
%>
	<input class="goButton" id="_schedule_report_up" type="button" onclick="scheduleSelectedReports('EDIT');" value="Update" />&nbsp;
	<input class="goButton" id="_schedule_report_re" type="button" onclick="scheduleSelectedReports('DELETE');" value="Remove" />&nbsp;	
</div>
<%
			}
%>
</div>
</form>
<div style="display: none;" id="waiting_message"></div>
<script>
updateDateSelected ('report_start_date', reportStartDate);
updateDateSelected ('report_end_date', reportEndDate);
updateScheduleSelectedReports();
function showHideDateSelector(ele, shown)
{
	var dateSelector = document.getElementById(ele);
	if (dateSelector)
	{
		if (shown)
		{		
			dateSelector.style.visibility = 'visible';
		}
		else
		{
			dateSelector.style.visibility = 'hidden';
		}
	}
	
}
</script>
<%
		}
		else
		{
			response.sendRedirect(cms.link("/sql_report/manage_report/access_denied.html "));		
		}		
	}
%>
<cms:include property="template" element="foot" />