var CronJob = (function(){
    
    var undefined;
    
    function CronTime(time) {
        
        this.source = time;
        
        this.map = ['second', 'minute', 'hour', 'dayOfMonth', 'month', 'dayOfWeek'];
        this.constraints = [[0,59],[0,59],[0,23],[1,31],[1,12],[1,7]];
        this.aliases = {
            jan:1,feb:2,mar:3,apr:4,may:5,jun:6,jul:7,aug:8,sep:9,oct:10,nov:11,dec:12,
            sun:1,mon:2,tue:3,wed:4,thu:5,fri:6,sat:7
        };
        
        this.second = {};
        this.minute = {};
        this.hour = {};
        this.dayOfMonth = {};
        this.month = {};
        this.dayOfWeek = {};
        
        this._parse();
        
    };
    
    CronTime.prototype = {
        _parse: function() {
            
            var aliases = this.aliases,
                source = this.source.replace(/[a-z]{3}/i, function(alias){
                    
                    alias = alias.toLowerCase();
                    
                    if (alias in aliases) {
                        return aliases[alias];
                    }
                    
                    throw ('Unknown alias: ' + alias);
                    
                }),
                split = source.replace(/^\s\s*|\s\s*$/g, '').split(/\s+/),
                cur, len = 6;
            
            while (len--) {
                cur = split[len] || '*';
                this._parseField(cur, this.map[len], this.constraints[len]);
            }
            
        },
        _parseField: function(field, type, constraints) {
            if(field == '?'){
            	field = '5';
            }
            var rangePattern = /(\d+?)(?:-(\d+?))?(?:\/(\d+?))?(?:,|$)/g,
                typeObj = this[type],
                diff,
                low = constraints[0],
                high = constraints[1];
            
            // * is a shortcut to [lower-upper] range
            field = field.replace(/\*/g, low + '-' + high);
                
            if (field.match(rangePattern)) {
                
                field.replace(rangePattern, function($0, lower, upper, step) {
                    
                    step = step || 1;
                    
                    // Positive integer higher than constraints[0]
                    lower = Math.max(low, ~~Math.abs(lower));
                    
                    // Positive integer lower than constraints[1]
                    upper = upper ? Math.min(high, ~~Math.abs(upper)) : lower;
                    
                    diff = step + upper - lower;
                    
                    while ((diff-=step) > -1) {
                        typeObj[diff + lower] = true;
                    }
                    
                });
                
            } else {
                
                throw ('Field (' + field + ') cannot be parsed');
                
            }
            
        }
    };
    
    function CronJob(cronTime, event) {
        
        if (!(this instanceof CronJob)) {
            return new CronJob(cronTime, event);
        }
        
        this.events = [event];
        this.cronTime = new CronTime(cronTime);
        this.now = {};
        this.initiated = false;
        
        this.clock();
        
    }
    
    CronJob.prototype = {
        
        addEvent: function(event) {
            this.events.push(event);
        },
        
        runEvents: function() {
            for (var i = -1, l = this.events.length; ++i < l; ) {
                if (typeof this.events[i] === 'function') {
                    this.events[i]();
                }
            }
        },
        
        clock: function() {
            
            var date = new Date,
                now = this.now,
                self = this,
                cronTime = this.cronTime,
                i;
            
            if (!this.initiated) {
                // Make sure we start the clock precisely ON the 0th millisecond
                setTimeout(function(){
                    self.initiated = true;
                    self.clock();
                }, Math.ceil(+date / 1000) * 1000 - +date);
                return;
            }
            
            this.timer = this.timer || setInterval(function(){self.clock();}, 1000);
            
            now.second = date.getSeconds();
            now.minute = date.getMinutes();
            now.hour = date.getHours();
            now.dayOfMonth = date.getDate();
            now.month = date.getMonth() + 1;
            now.dayOfWeek = date.getDay();
            
            for (i in now) {
                if (!(now[i] in cronTime[i])) {
                    return;
                }
            }
            
            this.runEvents();
            
        }
        
    };
    
    return CronJob;
    
})();


var NumericStepper = (function(){
   var undefined;
	function NumericStepper(name, minValue, maxValue, stepSize, id){
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.stepSize = stepSize;
		var holder = document;
		if(id){
	 		holder = document.getElementById(id);
	 		//alert(holder.id);
		}
		var elements = getElementsByClassName(holder, "*", name);
		for (var i=0; i<elements.length; i++){
			var textbox = elements[i].getElementsByTagName('input')[0];
			//alert(textbox.name);
			if (textbox){
				if (textbox.value == undefined || textbox.value == '' || isNaN(textbox.value)){
					textbox.value = (this.maxValue > 9) ? "00" : "0";
				}
				textbox.onkeypress = function(e){
					if(window.event){
						keynum = e.keyCode; // IE
					} else if(e.which){
						keynum = e.which; // Netscape/Firefox/Opera
					}
					keychar = String.fromCharCode(keynum);
					numcheck = /[0-9\-]/;
					if (keynum==8 || keynum==46)
						return true;
					else
						return numcheck.test(keychar);
				};
				textbox.minValue = this.minValue;
				textbox.maxValue = this.maxValue;
				textbox.onblur = function(){
	 				var iVal = parseInt(this.value, 10);
					if (iVal < this.minValue)
						this.value = this.minValue;
					if (iVal > this.maxValue)
						this.value = this.maxValue;
				};
				var buttons = elements[i].getElementsByTagName('button');
				if (buttons[0]){
					this.addButtonEvent(buttons[0], textbox, this.stepUp);
				}
				if (buttons[1])
					this.addButtonEvent(buttons[1], textbox, this.stepDown);
			}
		}
	};

	NumericStepper.prototype = {
		addButtonEvent:function(o,textbox, func){
			o.textbox = textbox;
			o.holder = this;
			// convert button type to button to prevent form submission onclick
			if (o.getAttribute("type")=="submit"){
				o.removeAttribute("type"); // IE fix
				o.setAttribute("type","button");
			}
			o.onclick = func;
		}
		,step:function(textbox, val){
			if (textbox == undefined) 
				return;
			if (val == undefined || isNaN(val)) 
				val = 1;
			if (textbox.value == undefined || textbox.value == '' || isNaN(textbox.value)){
				textbox.value = "0";
			}
			var curVal = parseInt(textbox.value, 10) + val;
			if (curVal < this.minValue){
				textbox.value = this.minValue;
			} else if (curVal >this.maxValue){
				textbox.value = this.maxValue;
			} else if (curVal < 10){
				textbox.value = curVal;
			} else{
				textbox.value = curVal;
			}
			if(this.maxValue > 9 && textbox.value.length < 2){
				textbox.value = "0" + textbox.value;
			}
		}
		,stepUp:function(){
			this.holder.step(this.textbox, this.holder.stepSize);
		}
		,stepDown:function(){
			this.holder.step(this.textbox, -this.holder.stepSize);
		}
	};

	return NumericStepper;
})();

var CronEditor = (function(){
	 function CronEditor(containerId, hourPickerId, minPickerId, numDays, recurPickerId){
		 this.cronFields = ["0", "0", "0", "*", "*", "?", "*"];
		 this.container = document.getElementById(containerId);
		 if(recurPickerId){
		 	this.recurPicker = document.getElementById(recurPickerId);
		 }
		 this.hourPicker = document.getElementById(hourPickerId);
		 this.minPicker = document.getElementById(minPickerId);
		 switch(numDays)
		 {
		 case 0://Daily
			 this.dayPickers = false;
			 this.cronFieldIdx = 2;
			 break;
		 case 8://Weekly
			 this.dayPickers = new Array(8);
			 for(var i = 0; i < 8; i++){
				 this.dayPickers[i] = document.getElementById("dow" + i);
			 }
			 this.cronFields[3] = "?";
			 this.cronFields[5] = "*";
			 this.cronFieldIdx = 5;
		   break;
		 case 32://Monthly
			 this.dayPickers = new Array(32);
			 for(var i = 0; i < 32; i++){
				 this.dayPickers[i] = document.getElementById("dom" + i);
			 }
			 this.cronFieldIdx = 4;
		   break;
		 default:
			 this.cronFieldIdx = 7;
		 }
	 };
	 CronEditor.prototype = {
		 show: function(){
		 	this.reset();
		 	this.container.style.display = "block";
	 	 }
	 	 , hide: function(){
		 	this.container.style.display = "none";
	 	 }
	 	 , reset: function(){
	 		 this.hourPicker.value = 23;
	 		 this.minPicker.value = 59;
	 		 if(this.recurPicker){
	 			this.recurPicker.value = "01";
	 		 }
	 		 if(this.dayPickers){
	 		 	 for(var i = 1; i < this.dayPickers.length; i++){
	  	  		 	if(this.dayPickers[i].checked){
	  	  		 		this.dayPickers[i].click();
	  	  		 	}
  	  		 		if(__isFireFox){
  	  		 			this.dayPickers[i].style.display = 'none';
  	  		 		}
	  		 	 }
	 		 }
	 	 }
 		 , buildCronExpression: function(){
		 	 this.cronFields[0] = "0";
 		 	 this.cronFields[1] = this.minPicker.value;
 		 	 this.cronFields[2] = this.hourPicker.value;
			 switch(this.cronFieldIdx)
			 {
			 case 2://Daily
 		 	 	this.cronFields[3] = "1/" + this.recurPicker.value;
	  		 	this.cronFields[4] = "*";
 		 	 	this.cronFields[5] = "?";
	  		 	this.cronFields[6] = "*";
			   	break;
			 case 5://Weekly
				var cronField = this.buildCronField(this.dayPickers);
				if(cronField == ""){
					cronField = "1";
				}
 		 	 	this.cronFields[3] = "?";
	  		 	this.cronFields[4] = "*";
 		 	 	this.cronFields[5] = cronField;
	  		 	this.cronFields[6] = "*";
			   	break;
			 case 4://Monthly
				var cronField = this.buildCronField(this.dayPickers);
				if(cronField == ""){
					cronField = "1";
				}
 		 	 	this.cronFields[3] = cronField;
	  		 	this.cronFields[4] = "1/" + this.recurPicker.value;
 		 	 	this.cronFields[5] = "?";
	  		 	this.cronFields[6] = "*";
			   	break;
			 default:
			   	break;
			 }
 		 	 var cron = this.cronFields[0];
 		 	 for(var i = 1; i < this.cronFields.length; i++){
 	  		 	cron += " " + this.cronFields[i];
 		 	 }
 		 	 return cron;
	 	 }
	 	 , buildCronField: function(checkboxes){
			var i = 0;
			var getRange = function(){
				while(i < checkboxes.length && !checkboxes[i].checked){
					i++;
				}
				var start = i, end = i;
				if(start == checkboxes.length){
					return "";
				}
				while(i < checkboxes.length && checkboxes[i].checked){
					end = i;
					i++;
				}
				if(start == end){
					return end;
				} else if((start + 1) == end){
					return start + "," + end;
				} else{
					return start + "-" + end;
				}
			};
			var cronExpr = "";
			while(i < checkboxes.length){
				var range = "" + getRange();
				//alert(cronExpr);
				if(range.length > 0){
					if(cronExpr.length > 0){
						cronExpr = cronExpr + ","+ range;
					} else{
						cronExpr = range;
					}
				}
			}
			return cronExpr;
		}

	 };
	 return CronEditor;
})();
function getElementsByClassName(oElm, strTagName, strClassName){
  var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
  var arrReturnElements = new Array();
  strClassName = strClassName.replace(/-/g, "\-");
  var oRegExp = new RegExp("(^|\s)" + strClassName + "(\s|$)");
  var oElement;
  for(var i=0; i<arrElements.length; i++){
    oElement = arrElements[i];
    if(oRegExp.test(oElement.className)){
      arrReturnElements.push(oElement);
    }
  }
  return (arrReturnElements)
}
