var fromBoxArray = new Array();
var toBoxArray = new Array();
var selectBoxIndex = 0;
var arrayOfItemsToSelect = new Array();
var buttonAllLeft;
var buttonAllRight;
var buttonRight;

function moveSingleElement()
{
	var selectBoxIndex = this.parentNode.parentNode.id.replace(/[^\d]/g,'');
	var tmpFromBox;
	var tmpToBox;
	if(this.tagName.toLowerCase()=='select'){			
		tmpFromBox = this;
		if(tmpFromBox==fromBoxArray[selectBoxIndex])tmpToBox = toBoxArray[selectBoxIndex]; 
		else tmpToBox = fromBoxArray[selectBoxIndex];
	}else{
	
		if(this.value.indexOf('>')>=0){
			tmpFromBox = fromBoxArray[selectBoxIndex];
			tmpToBox = toBoxArray[selectBoxIndex];			
		}else{
			tmpFromBox = toBoxArray[selectBoxIndex];
			tmpToBox = fromBoxArray[selectBoxIndex];	
		}
	}
	
	for(var no=0;no<tmpFromBox.options.length;no++){
		if(tmpFromBox.options[no].selected && !tmpFromBox.options[no].disabled){
			tmpFromBox.options[no].selected = false;
			var newOption = new Option(tmpFromBox.options[no].text,tmpFromBox.options[no].value);
			newOption.id = tmpFromBox.options[no].id;
			tmpToBox.options[tmpToBox.options.length] = newOption;
			//tmpToBox.options[tmpToBox.options.length].id = tmpFromBox.options[no].id;
			
			for(var no2=no;no2<(tmpFromBox.options.length-1);no2++){
				tmpFromBox.options[no2].value = tmpFromBox.options[no2+1].value;
				tmpFromBox.options[no2].text = tmpFromBox.options[no2+1].text;
				tmpFromBox.options[no2].selected = tmpFromBox.options[no2+1].selected;
				tmpFromBox.options[no2].disabled = tmpFromBox.options[no2+1].disabled;
				tmpFromBox.options[no2].id = tmpFromBox.options[no2+1].id;
			}
			no = no -1;
			tmpFromBox.options.length = tmpFromBox.options.length-1;
										
		}			
	}
	
	sortAllElement(tmpFromBox);
	sortAllElement(tmpToBox);
/*	var tmpTextArray = new Array();
	for(var no=0;no<tmpFromBox.options.length;no++){
		tmpTextArray.push(tmpFromBox.options[no].text + '___' + tmpFromBox.options[no].value);			
	}
	tmpTextArray.sort();
	var tmpTextArray2 = new Array();
	for(var no=0;no<tmpToBox.options.length;no++){
		tmpTextArray2.push(tmpToBox.options[no].text + '___' + tmpToBox.options[no].value);			
	}		
	tmpTextArray2.sort();
	
	for(var no=0;no<tmpTextArray.length;no++){
		var items = tmpTextArray[no].split('___');
		tmpFromBox.options[no] = new Option(items[0],items[1]);
		
	}		
	
	for(var no=0;no<tmpTextArray2.length;no++){
		var items = tmpTextArray2[no].split('___');
		tmpToBox.options[no] = new Option(items[0],items[1]);			
	}
*/
}

function sortAllElement(boxRef)
{
	var tmpTextArray2 = new Array();
	for(var no=0;no<boxRef.options.length;no++){
		tmpTextArray2.push(
				boxRef.options[no].text
				+ '___' + boxRef.options[no].value
				+ '___' + boxRef.options[no].id
				+ '___' + boxRef.options[no].disabled);			
	}		
	tmpTextArray2.sort();		
	for(var no=0;no<tmpTextArray2.length;no++){
		var items = tmpTextArray2[no].split('___');
		boxRef.options[no] = new Option(items[0],items[1]);
		boxRef.options[no].id = items[2];
		boxRef.options[no].disabled = (items[3] == "true");
	}		

	if( BrowserDetect.browser == "Explorer" && (BrowserDetect.version == "6" || BrowserDetect.version == "7")) {
		disableOptionsIE6();		
	}	
}
function moveAllElements()
{
	var selectBoxIndex = this.parentNode.parentNode.id.replace(/[^\d]/g,'');
	var tmpFromBox;
	var tmpToBox;		
	if(this.value.indexOf('>')>=0){
		tmpFromBox = fromBoxArray[selectBoxIndex];
		tmpToBox = toBoxArray[selectBoxIndex];			
	}else{
		tmpFromBox = toBoxArray[selectBoxIndex];
		tmpToBox = fromBoxArray[selectBoxIndex];	
	}
	
	for(var no=0;no<tmpFromBox.options.length;no++){
//		if(tmpFromBox.options[no].disabled){
//			continue;
//		}
		var newOption = new Option(tmpFromBox.options[no].text,tmpFromBox.options[no].value);
		newOption.id = tmpFromBox.options[no].id;
		newOption.disabled = tmpFromBox.options[no].disabled;
		tmpToBox.options[tmpToBox.options.length] = newOption;
//		tmpToBox.options[tmpToBox.options.length] = new Option(tmpFromBox.options[no].text,tmpFromBox.options[no].value);
//		tmpToBox.options[tmpToBox.options.length].id = tmpFromBox.options[no].id;
	}	
	
	tmpFromBox.options.length=0;
	sortAllElement(tmpToBox);
	
}


/* This function highlights options in the "to-boxes". It is needed if the values should be remembered after submit. Call this function onsubmit for your form */
function multipleSelectOnSubmit()
{
	for(var no=0;no<arrayOfItemsToSelect.length;no++){
		var obj = arrayOfItemsToSelect[no];
		for(var no2=0;no2<obj.options.length;no2++){
			obj.options[no2].selected = (!obj.options[no2].disabled);
		}
	}
	
}

function createMovableOptions(fromBox,toBox,totalWidth,totalHeight,labelLeft,labelRight)
{		
	fromObj = document.getElementById(fromBox);
	toObj = document.getElementById(toBox);
	
	arrayOfItemsToSelect[arrayOfItemsToSelect.length] = toObj;

	
	fromObj.ondblclick = moveSingleElement;
	toObj.ondblclick = moveSingleElement;

	
	fromBoxArray.push(fromObj);
	toBoxArray.push(toObj);
	
	var parentEl = fromObj.parentNode;
	
	var parentDiv = document.createElement('DIV');
	parentDiv.className='multipleSelectBoxControl';
	parentDiv.id = 'selectBoxGroup' + selectBoxIndex;
	parentDiv.style.width = totalWidth + 'px';
	parentDiv.style.height = totalHeight + 'px';
	parentEl.insertBefore(parentDiv,fromObj);
	
	
	var subDiv = document.createElement('DIV');
	subDiv.style.width = (Math.floor(totalWidth/2) - 15) + 'px';
	fromObj.style.width = (Math.floor(totalWidth/2) - 15) + 'px';

	var label = document.createElement('SPAN');
	label.innerHTML = labelLeft;
	subDiv.appendChild(label);
	
	subDiv.appendChild(fromObj);
	subDiv.className = 'multipleSelectBoxDiv';
	parentDiv.appendChild(subDiv);
	
	
	var buttonDiv = document.createElement('DIV');
	buttonDiv.style.verticalAlign = 'middle';
	buttonDiv.style.paddingTop = (totalHeight/2) - 50 + 'px';
	buttonDiv.style.width = '30px';
	buttonDiv.style.textAlign = 'center';
	parentDiv.appendChild(buttonDiv);
	
	buttonRight = document.createElement('INPUT');
	buttonRight.type='button';
	buttonRight.value = '>';
	buttonDiv.appendChild(buttonRight);	
	buttonRight.onclick = moveSingleElement;	
	
	buttonAllRight = document.createElement('INPUT');
	buttonAllRight.type='button';
	buttonAllRight.value = '>>';
	buttonAllRight.onclick = moveAllElements;
	buttonDiv.appendChild(buttonAllRight);		
	
	var buttonLeft = document.createElement('INPUT');
	buttonLeft.style.marginTop='10px';
	buttonLeft.type='button';
	buttonLeft.value = '<';
	buttonLeft.onclick = moveSingleElement;
	buttonDiv.appendChild(buttonLeft);		
	
	buttonAllLeft = document.createElement('INPUT');
	buttonAllLeft.type='button';
	buttonAllLeft.value = '<<';
	buttonAllLeft.onclick = moveAllElements;
	buttonDiv.appendChild(buttonAllLeft);
	
	var subDiv = document.createElement('DIV');
	subDiv.style.width = (Math.floor(totalWidth/2) - 15) + 'px';
	toObj.style.width = (Math.floor(totalWidth/2) - 15) + 'px';

	var label = document.createElement('SPAN');
	label.innerHTML = labelRight;
	subDiv.appendChild(label);
			
	subDiv.appendChild(toObj);
	parentDiv.appendChild(subDiv);		
	
	toObj.style.height = (totalHeight - label.offsetHeight) + 'px';
	fromObj.style.height = (totalHeight - label.offsetHeight) + 'px';

		
	selectBoxIndex++;
	sortAllElement(fromObj);
}

var __isIE =  navigator.appVersion.match(/MSIE/);
var __userAgent = navigator.userAgent;
var __isFireFox = __userAgent.match(/firefox/i);
var __isFireFoxOld = __isFireFox && 
   (__userAgent.match(/firefox\/2./i) || __userAgent.match(/firefox\/1./i));
var __isFireFoxNew = __isFireFox && !__isFireFoxOld;
function __parseBorderWidth(width) {
    var res = 0;
    if (typeof(width) == "string" && width != null 
                && width != "" ) {
        var p = width.indexOf("px");
        if (p >= 0) {
            res = parseInt(width.substring(0, p));
        }
        else {
             //do not know how to calculate other 
             //values (such as 0.5em or 0.1cm) correctly now
             //so just set the width to 1 pixel
            res = 1; 
        }
    }
    return res;
}

//returns border width for some element
function __getBorderWidth(element) {
    var res = new Object();
    res.left = 0; res.top = 0; res.right = 0; res.bottom = 0;
    if (window.getComputedStyle) {
        //for Firefox
        var elStyle = window.getComputedStyle(element, null);
        res.left = parseInt(elStyle.borderLeftWidth.slice(0, -2));  
        res.top = parseInt(elStyle.borderTopWidth.slice(0, -2));  
        res.right = parseInt(elStyle.borderRightWidth.slice(0, -2));  
        res.bottom = parseInt(elStyle.borderBottomWidth.slice(0, -2));  
    }
    else {
        //for other browsers
        res.left = __parseBorderWidth(element.style.borderLeftWidth);
        res.top = __parseBorderWidth(element.style.borderTopWidth);
        res.right = __parseBorderWidth(element.style.borderRightWidth);
        res.bottom = __parseBorderWidth(element.style.borderBottomWidth);
    }
   
    return res;
}
function getElementAbsolutePos(element) {
    var res = new Object();
    res.x = 0; res.y = 0;
    if (element !== null) {
        res.x = element.offsetLeft;
        res.y = element.offsetTop;
        
        var offsetParent = element.offsetParent;
        var parentNode = element.parentNode;
        var borderWidth = null;

        while (offsetParent != null) {
            res.x += offsetParent.offsetLeft;
            res.y += offsetParent.offsetTop;
            
            var parentTagName = offsetParent.tagName.toLowerCase();    

            if ((__isIE && parentTagName != "table") || 
                (__isFireFoxNew && parentTagName == "td")) {            
                borderWidth = __getBorderWidth(offsetParent);
                res.x += borderWidth.left;
                res.y += borderWidth.top;
            }
            
            if (offsetParent != document.body && 
                offsetParent != document.documentElement) {
                res.x -= offsetParent.scrollLeft;
                res.y -= offsetParent.scrollTop;
            }

            //next lines are necessary to support FireFox problem with offsetParent
               if (!__isIE) {
                while (offsetParent != parentNode && parentNode !== null) {
                    res.x -= parentNode.scrollLeft;
                    res.y -= parentNode.scrollTop;
                    
                    if (__isFireFoxOld) {
                        borderWidth = __getBorderWidth(parentNode);
                        res.x += borderWidth.left;
                        res.y += borderWidth.top;
                    }
                    parentNode = parentNode.parentNode;
                }    
            }

            parentNode = offsetParent.parentNode;
            offsetParent = offsetParent.offsetParent;
        }
    }
    return res;
}
function getDocHeight() {
    var ret = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight
        , document.body.offsetHeight, document.documentElement.offsetHeight
        , document.body.clientHeight, document.documentElement.clientHeight
    );
    return ret;
}	
function getElementWidth(element){
	var ret = Math.max(
        element.scrollWidth
        , element.offsetWidth
        , element.clientWidth
    );
    return ret;
}	
function getElementHeight(element){
	var ret = Math.max(
        element.scrollHeight
        , element.offsetHeight
        , element.clientHeight
    );
    return ret;
}	
function getScrollY() {
	  var scrOfY = 0;
	  if( typeof( window.pageYOffset ) == 'number' ) {
	    //Netscape compliant
	    scrOfY = window.pageYOffset;
	  } else if( document.body && document.body.scrollTop ) {
	    //DOM compliant
	    scrOfY = document.body.scrollTop;
	  } else if( document.documentElement && document.documentElement.scrollTop ) {
	    //IE6 standards compliant mode
	    scrOfY = document.documentElement.scrollTop;
	  }
	  return scrOfY;
}
function showScheduleDialog(dialogId, paddingTop){
	var dialog = document.getElementById(dialogId); 
	//var top = getScrollY() + paddingTop;
	var top = paddingTop;
	dialog.style.top = top + "px";

	var layer = dialog.parentNode;
	layer.style.display = 'block';
	var docHeight = 1*getDocHeight();
	layer.style.height = docHeight + "px";
	
	// disable add schedule button
	if (document.getElementById('add_schedule_btn'))
	{
		document.getElementById('add_schedule_btn').disabled = true;
	}
}
function showDialogByTopRightOfEles(dialogId, eleTopId, eleRightId){
	var eleTopPos = false;
	var eleTop = false;
	if(eleTopId){
		eleTop = document.getElementById(eleTopId);
		eleTopPos = getElementAbsolutePos(eleTop);
	}
	
	var eleRightPos = false;
	var eleRight = false;
	if(eleRightId){
		eleRight = document.getElementById(eleRightId);	
		eleRightPos = getElementAbsolutePos(eleRight);
	}
	
	var dialog = document.getElementById(dialogId); 
	var top = 0, left = 0;
	
	if(eleTopPos){
		top = eleTopPos.y + 1;
		dialog.style.top = top + "px";
	}

	if(eleRightPos){
		left = eleRightPos.x + getElementWidth(eleRight) + 1;
		dialog.style.left = left + "px";
	} else{
		left = eleTopPos.x + getElementWidth(eleTop) + 1;
		dialog.style.left = left + "px";
	}

	dialog.style.display = 'block';
}
function showDialogByBottomLeftOfEles(dialogId, eleBottomId, eleLeftId){
	var eleBottomPos = false;
	var eleBottom = false;
	if(eleBottomId){
		eleBottom = document.getElementById(eleBottomId);
		eleBottomPos = getElementAbsolutePos(eleBottom);
	}
	
	var eleLeftPos = false;
	var eleLeft = false;
	if(eleLeftId){
		eleLeft = document.getElementById(eleLeftId);	
		eleLeftPos = getElementAbsolutePos(eleLeft);
	}
	
	var dialog = document.getElementById(dialogId); 
	var top = 0, left = 0;
	
	if(eleBottomPos){
		top = eleBottomPos.y + getElementHeight(eleBottom) + 1;
		dialog.style.top = top + "px";
	}

	if(eleLeftPos){
		left = eleLeftPos.x + 1;
		dialog.style.left = left + "px";
	} else{
		left = eleBottomPos.x + 1;
		dialog.style.left = left + "px";
	}

	dialog.style.display = 'block';
}
function hideDialogOnly(dialogId){
	document.getElementById(dialogId).style.display = 'none';	
}
function hideScheduleDialog(dialogId){
	document.getElementById(dialogId).parentNode.style.display = 'none';
	// disable add schedule button
	if (document.getElementById('add_schedule_btn'))
	{
		document.getElementById('add_schedule_btn').disabled = false;
	}	
}

function disableOptionsIE6() {
	if (document.getElementsByTagName) {
		var s = document.getElementsByTagName("select");

		if (s.length > 0) {
			window.select_current = new Array();

			for (var i=0, select; select = s[i]; i++) {
				select.onfocus = function(){ window.select_current[this.id] = this.selectedIndex; }
				//select.onchange = function(){ restore(this); }
				emulate(select);
			}
		}
	}
}

function restore(e) {
	if (e.options[e.selectedIndex].disabled) {
		e.selectedIndex = window.select_current[e.id];
	}
}

function emulate(e) {
	for (var i=0, option; option = e.options[i]; i++) {
		if (option.disabled) {
			option.style.color = "gray";
		}
		else {
			option.style.color = "black";
		}
	}
}

function pad2(number) {
	   
    var str = '' + number;
    while (str.length < 2) {
        str = '0' + str;
    }
   
    return str;

}