var CONFIG = {
	AVAILABLE_DBS:["mysql","sqlite"],
	DEFAULT_DB:"mysql",

	AVAILABLE_LOCALES:["en","fr","de","cs"],
	DEFAULT_LOCALE:"en",
	
	AVAILABLE_BACKENDS:["php-mysql","php-blank","php-file"],
	DEFAULT_BACKEND:["php-mysql"],

	RELATION_THICKNESS:2,
	RELATION_SPACING:15
}
