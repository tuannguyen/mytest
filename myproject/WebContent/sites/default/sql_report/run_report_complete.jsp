<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<cms:include property="template" element="head" />

<%	
	String lastRun = (String) request.getParameter("time");
	String outputFormat = (String) request.getParameter("output");
	String target = "";
	if (outputFormat != null && outputFormat.equalsIgnoreCase("XLS"))
	{
		target = "pensionline_report.xls";
	}
	else if (outputFormat != null && outputFormat.equalsIgnoreCase("PDF"))
	{
		target = "pensionline_report.pdf";
	}
	
	String lastRunFormatted = "";
	if (lastRun != null)
	{
		try
		{
			long lastRunL = Long.parseLong(lastRun);
			SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss");
			lastRunFormatted = formater.format(new Date(lastRunL)) + " at " + formater2.format(new Date(lastRunL));
		}
		catch (NumberFormatException nfe)
		{
			System.err.println("Last run is not a number: " + nfe);
		}
		
		
	}
	
 %>

</script>
<h1>Report generation completed</h1>
<p>The report you have requested is available.</p>
<p>It was generated on <%=lastRunFormatted%></p>
<form action="<%=target %>" method="post">
	<div class="login_value">
		<input class="goButton" id="doauth" type="submit" name="doauth" value="View Report"/>
	</div>
</form>
<cms:include property="template" element="foot" />
