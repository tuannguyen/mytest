<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.sqlreport.app.daemons.ReportScheduler"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.Schedule"%>
<%@page import="java.util.List"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%><cms:include property="template" element="head" />

<%	
	String scheduleId = request.getParameter("scheduleId");
	String lastRun = "-1";
	String numRuns = "0";
	String lastRunFormatted = "";
	String outputFormat = (String) request.getAttribute("ext");
	
	if(scheduleId != null)
	{
		// Huy modify to put last run value to request to /DownloadScheduleOutput
		
		Schedule schedule = ReportScheduler.getInstance().getSchedule(scheduleId);
		if(schedule != null)
		{
			List<ScheduleRunLog> runlogs = schedule.getPrevRuns();
			
			if(runlogs != null && runlogs.size() > 0){
				long lastRunLong = runlogs.get(runlogs.size() - 1).getRunningTime().getTime();
				SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss");
				lastRun = "" + lastRunLong;
				lastRunFormatted = formater.format(new Date(lastRunLong)) + " at " + formater2.format(new Date(lastRunLong));
				
				numRuns = "" + runlogs.size();
			}	
			
			if (outputFormat == null)
			{
				String url = "/content/DownloadScheduleOutput?&action=getExt&scheduleId="+ scheduleId + "&lastRun=" + lastRun;
				response.sendRedirect(url);
				return;
			}
			else
			{
				outputFormat = outputFormat.toUpperCase().replaceAll("\\.", "");				
			/*			if(schedule.getOutputFormat().toLowerCase().indexOf("pdf") >= 0){
					outputFormat = "PDF";
				}
			*/
			}			
		}
	} 
	else
	{
		scheduleId = "-1";
	}
	
 %>
<script type="text/javascript">
<%
	if(scheduleId.equals("-1"))
	{	
 %>
 		alert("This schedule has been removed.");
<%
	} 
	else{
 %>
	if(window.opener){
		window.opener.updateLastRun("<%=scheduleId %>", "<%=lastRun %>", "<%=numRuns %>"); //Refresh parent window to get last run
	}
<%
}
 %>
</script>
<h1>Report generation completed</h1>
<p>The report you have requested is available.</p>
<p>It was generated on <%=lastRunFormatted%></p>
<form action="/content/DownloadScheduleOutput" method="post">
	<input type="hidden" name="scheduleId" value="<%=scheduleId %>" />
	<div class="login_value">
		<input class="goButton" id="doauth" type="submit" name="doauth" 
<%
if(scheduleId.equals("-1")){
	
 %>
 				disabled="disabled" 
<%
}
 %>
				value="View Report"/>
	</div>
</form>
<cms:include property="template" element="foot" />
