<%@page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.sqlreport.app.jasper.PLReportProducer"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.jsp.CmsJspLoginBean"%>
<cms:include property="template" element="head" />
<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);
	String groupId = request.getParameter("groupId");
	String reportRunner = "";
	String todayDateStr = new SimpleDateFormat("d-MMM-yy").format(new Date());
	
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null)
	{
		String reportRunnerId = currentUser.getName();
		
		String userId = currentUser.getId().toString();
		String reportRunnerDesc = currentUser.getDescription();
		if (reportRunnerDesc == null || currentUser.getDescription().trim().length() == 0)
		{
			reportRunnerDesc = "Default";
		}
		reportRunner = currentUser.getFirstname() + " " + currentUser.getLastname() + " (" + reportRunnerDesc + ")";
		if (UpdateUserInReportGroupHandler.isUserInCmsGroup(reportRunnerId, "PL_REPORT_EDITOR") || 
				(UpdateUserInReportGroupHandler.isUserInCmsGroup(reportRunnerId, "PL_REPORT_RUNNER") && UpdateUserInReportGroupHandler.isUserAddedToGroup(userId, groupId)))
		{
%>
<script type="text/javascript">
	reportGroupId = '<%=groupId%>';
	var todayDateStr = '<%=todayDateStr%>';
</script>
<%	
			Vector reports = ManageSQLReportHandler.listAllReportsInGroup(groupId);
			String groupName = "";
			ReportGroup reportGroupDTO = ManageReportGroupHandler.getReportGroupInfo(groupId);
			if (reportGroupDTO != null)
			{
				groupName = reportGroupDTO.getName();
			}
%>
<script type="text/javascript">
runningReportArray = new Array();
	<%
			for (int i = 0; i < reports.size(); i++)
			{
				Report reportDTO = (Report)reports.elementAt(i);
				if (reportDTO != null)
				{
					String reportId = reportDTO.getReportId();
					String reportTitle = reportDTO.getReportTitle();
					String reportDescription = reportDTO.getReportDescription();
					Vector sections = ManageSQLReportHandler.listSectionsOfMaster(reportId);
	%>			
	var runningSectionArray = new Array();
	<%	
					for (int j=0; j < sections.size(); j++)
					{
						ReportSection section = (ReportSection)sections.get(j);
						String sectionId = section.getSectionId();
						String sectionTitle= section.getSectionTitle();
						Vector sectionParams = PLReportProducer.getSectionParams(reportId, sectionId);
						if (sectionParams != null && sectionParams.size() > 0)
						{
	%>
	var runningParamArray = new Array();
	<%
							for (int k=0; k < sectionParams.size(); k++)
							{
								SQLSectionParameterDTO param = (SQLSectionParameterDTO)sectionParams.elementAt(k);
								String paramName = param.getName();
								int paramType = param.getType();
								String paramDefaultValue = param.getDefaultValue();
								String paramDescription = param.getDescription();
	%>
	runningParamArray.push(Array("<%=paramName%>", <%=paramDefaultValue%>, "<%=paramType%>", "<%=paramDescription%>"));
	<%
							}
	%>
	runningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>", runningParamArray));
	<%
						}
						else
						{
	%>
	runningSectionArray.push(Array("<%=sectionId%>", "<%=sectionTitle%>"));
	<%
						}
					}
	%>	
	runningReportArray.push(Array("<%=reportId%>", "<%=reportTitle%>", runningSectionArray, "<%=reportDescription%>"));
	<%
			
				}
			}
	%>	
</script>
<form id="access_report_form">
<h1>Access reports: <%=groupName%></h1>
<p>Please fill in the variable fields below; then select the reports and sections in each report that you wish to
include in the single report file.</p>
<p>Click 'Run' at the bottom of the page to produce your report.</p>
<div class="login_component_holder">
	<div class="login_label"><label>Report runner</label> </div>
	<div class="login_value"><input type="text" id="report_runner" size="25" value="<%=reportRunner%>" readonly="readonly"/></div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label for="output">Report output type</label> </div>
	<div class="login_value"><select id="report_output_type">
	<option value="DOWNLOAD_XLS"  selected="selected">Download as a XLS document</option>	
	<option value="DOWNLOAD_PDF">Download as a PDF document</option>	
	</select>
	</div>
</div>
<!-- 
<div class="login_component_holder">
	<div class="login_label"><label>Report start date</label> </div>
	<div class="login_value"><input type="text" id="report_start_date" size="25" readonly="readonly" value="<%=todayDateStr%>"/><input type="button" value=" ... " onclick="return showCalendar('report_start_date', '%e-%b-%y');"/></div>
</div>
<div class="login_component_holder">
	<div class="login_label"><label>Report end date</label> </div>
	<div class="login_value"><input type="text" id="report_end_date" size="25" readonly="readonly" value="<%=todayDateStr%>"/><input type="button" value=" ... " onclick="return showCalendar('report_end_date', '%e-%b-%y');"/></div>
</div>
 -->
<div class="login_component_holder" style="display: none;">
	<div class="login_label"><label>&nbsp;</label> </div>
	<div class="login_value"><input type="checkbox" id="copyDateToSection"/>&nbsp;<label>Copy date to sections</label></div>
</div>
<div class="report_para" style="border-top: 0px;">
	<div>
	<span class="report_table_select_all"><input id="cb_all_reports" type="checkbox" checked="checked" onclick="SelectAllReports();"/>&nbsp;Select all reports</span>
	</div>
</div>
<div class="report_list">
<%
			for (int i = 0; i < reports.size(); i++)
			{
				Report report = (Report)reports.elementAt(i);
				if (report != null)
				{
					String reportId = report.getReportId();
					String title = report.getReportTitle();
					String description = report.getReportDescription();
					if (title == null || title.trim().equals(""))
					{
						title = "Unknown";
					}
					if (description == null) description = "&nbsp;";
					Vector sections = ManageSQLReportHandler.listSectionsOfMaster(reportId);
%>
	<div class="report_para">
		<h2><%=title%></h2>
		<p><%=description%></p>	
		<div id="rp_comment_<%=i%>">		
			<div>
			<span class="report_table_select_all"><input id="cb_<%=reportId%>" type="checkbox" checked="checked" onclick="SelectAllSections('<%=reportId%>');"/>&nbsp;Select all sections</span>
			<%	
					for (int j=0; j < sections.size(); j++)
					{
						ReportSection section = (ReportSection)sections.get(j);
			%>	
			<span class="report_table_sub_select" id="span_<%=reportId%>_<%=section.getSectionId()%>">
				<input id="cb_<%=reportId%>_<%=section.getSectionId()%>" type="checkbox" checked="checked" onclick="UpdateSelectAllSections('<%=reportId%>');"/>
				&nbsp;<%=section.getSectionTitle()%>
			</span>
			<span class="report_table_sub_select"  id="spin_<%=reportId%>_<%=section.getSectionId()%>" style="display: none;"><img alt="running report..." src='/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/spinning_image.gif'/></span>											
			<%
					}
			%>			
			</div>			
		</div>		
	</div>
<%
				}
			}
%>
</div>
<div class="report_component_holder">
<div style="TEXT-ALIGN: right"><input class="goButton" id="_add_report" type="button" onclick="runSelectedReports();" value="Run" />&nbsp;</div>
</div>
</form>
<form id="render_params_form" style="display: none;">
</form>
<div style="display: none;" id="waiting_message"></div>
<%
		}
		else
		{
			response.sendRedirect(cms.link("/sql_report/manage_report/access_denied.html "));		
		}		
	}
%>
<cms:include property="template" element="foot" />