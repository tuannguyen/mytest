<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%
	int fromChar = 0, toChar = 3;
	String selectedCharParam = request.getParameter("startwith");
	if (selectedCharParam != null && selectedCharParam.length() > 0)
	{
		// update fromChar and toChar based on char selected
		char selectedChar = selectedCharParam.toUpperCase().charAt(0);
		
		fromChar = ((selectedChar - 'A')/4) * 4;
		toChar = fromChar + 3;
		if (toChar >  25)
		{
			toChar =  25;
		}
	}
	
%>
<%@page import="java.util.Vector"%>
<%@page import="com.bp.pensionline.sqlreport.handler.DataDicAdminHandler"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.DataMappingDTO"%>
<cms:include property="template" element="head" />
<h1>Data Dictionary</h1>
<p>Details of the mapping of human readable <strong>aliases</strong> to the <strong>attributes</strong> name in the tables. You can use the <a href="/content/pl/sql_report/data_dictionary/data_mapper.html">Data Mapper</a> tool to update the mappings.</p>
<p>
<a href="" onclick="displayMapping(this); return false;">A</a>, <a href="" onclick="displayMapping(this); return false;">B</a>, 
<a href="" onclick="displayMapping(this); return false;">C</a>, <a href="" onclick="displayMapping(this); return false;">D</a>, 
<a href="" onclick="displayMapping(this); return false;">E</a>, <a href="" onclick="displayMapping(this); return false;">F</a>, 
<a href="" onclick="displayMapping(this); return false;">G</a>, <a href="" onclick="displayMapping(this); return false;">H</a>, 
<a href="" onclick="displayMapping(this); return false;">I</a>, <a href="" onclick="displayMapping(this); return false;">J</a>, 
<a href="" onclick="displayMapping(this); return false;">K</a>, <a href="" onclick="displayMapping(this); return false;">L</a>, 
<a href="" onclick="displayMapping(this); return false;">M</a>, <a href="" onclick="displayMapping(this); return false;">N</a>, 
<a href="" onclick="displayMapping(this); return false;">O</a>, <a href="" onclick="displayMapping(this); return false;">P</a>, 
<a href="" onclick="displayMapping(this); return false;">Q</a>, <a href="" onclick="displayMapping(this); return false;">R</a>, 
<a href="" onclick="displayMapping(this); return false;">S</a>, <a href="" onclick="displayMapping(this); return false;">T</a>, 
<a href="" onclick="displayMapping(this); return false;">U</a>, <a href="" onclick="displayMapping(this); return false;">V</a>, 
<a href="" onclick="displayMapping(this); return false;">W</a>, <a href="" onclick="displayMapping(this); return false;">X</a>, 
<a href="" onclick="displayMapping(this); return false;">Y</a>, <a href="" onclick="displayMapping(this); return false;">Z</a>
</p>
<h2>Aquila</h2>
<%
	int i = fromChar;
	for (; i<=toChar; i++)
	{		
%>
<div class="dd_details_holder"><%=(char)(i + 'A') %><br />
<% 
		Vector mappings = DataDicAdminHandler.sortMappingByAlias("AQUILLA", i);
	    if (mappings != null && mappings.size() > 0)
	    {
%>
	<table class="dd_table">
	    <tbody>
	        <tr>
	            <td width="25%"><strong>Alias</strong> </td>
	            <td width="25%"><strong>Attribute</strong> </td>
	            <td width="50%"><strong>Description</strong> </td>
	        </tr>
	 <% 	
		 	for (int j=0; j<mappings.size(); j++)
		 	{
		 		DataMappingDTO mapping = (DataMappingDTO)mappings.elementAt(j);
	 %>
	        <tr>
	            <td><%=mapping.getAlias() %></td>
	            <td><%=(mapping.getTable() + "." + mapping.getAttribute())%></td>
	            <td><%=mapping.getDescription() %></td>
	        </tr>
<% 	
		 	}
%>
	    </tbody>
	</table>
<%
		}
%>
	</div>
<%
	}
%>
<script type='text/javascript'>
function displayMapping(this1)
{
	if (this1)
	{
		var selectedChar = this1.innerHTML;
		window.location.replace('/content/pl/sql_report/data_dictionary/index.html?startwith=' + selectedChar);
	}
}
</script>
<cms:include property="template" element="foot" />