<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Query designer</title>
<STYLE>

div {
	padding:0px;
	margin:0px;
	border-collapse:collapse;
	FONT-SIZE: 10pt;
	font-weight: normal;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana, Helvetica, sans-serif;
	color: #333;
}
#query_holder_frame
{
	BORDER: 0;
	HEIGHT: 580px;
	WIDTH: 700px;
	BORDER-TOP: #93BEE2 1PX SOLID;
	padding-left: 5px;
	overflow: auto;	
}

#query_outline_frame
{
	BORDER: 0;
	HEIGHT: 230px;
	WIDTH: 220px;
	overflow: auto;
}
#table_list_frame
{
	BORDER: 0;
	HEIGHT: 328px;
	WIDTH: 220px;
	overflow: auto;
}

.tabOff
{
	FONT-FAMILY: Verdana;
	FONT-SIZE: 11;
	FONT-WEIGHT: 700;
	TEXT-ALIGN: CENTER;
	COLOR: #003399;
	BACKGROUND-COLOR: #c4e0f0;
	BORDER-BOTTOM: #c4e0f0 1PX SOLID;
	HEIGHT: 25;
	CURSOR: HAND;
}

.tabOn
{
	FONT-FAMILY: Verdana;
	FONT-SIZE: 11;
	FONT-WEIGHT: 700;
	TEXT-ALIGN: CENTER;
	COLOR: #003399;
	BACKGROUND-COLOR: #93BEE2;
	BORDER-BOTTOM: #93BEE2 1PX SOLID;
	HEIGHT: 35;
	CURSOR: HAND;
}

#contextMenu{	/* The menu container */
	border:1px solid #202867;	/* Border around the entire menu */
	background-color:#FFF;	/* White background color of the menu */
	margin:0px;
	padding:0px;
	width:175px;	/* Width of context menu */
	font-family:arial;
	font-size:12px;
	background-image:url('gradient.gif');
	background-repeat:repeat-y;
	
	/* Never change these two values */
	display:none;
	position:absolute;

}
#contextMenu a{	/* Links in the context menu */
	color: #000;
	text-decoration:none;
	line-height:15px;
	vertical-align:middle;	
	height:18px;
	
	/* Don't change these 3 values */
	display:block;	
	width:100%;
	clear:both;
	
}
#contextMenu li{	/* Each menu item */
	list-style-type:none;
	padding:1px;
	margin:1px;
	cursor:pointer;	
	clear:both;
}
#contextMenu li div{	/* Dynamically created divs */
	cursor:pointer;	
}
#contextMenu .contextMenuHighlighted{	/* Highlighted context menu item */
	border:1px solid #000;
	padding:0px;
	background-color:#E2EBED;

}
#contextMenu img{
	border:0px;
}
#contextMenu .imageBox{	/* Dynamically created divs for images in the menu */

	float:left;
	padding-left:2px;
	padding-top:3px;
	vertical-align:middle;
	
	width: 30px;	/* IE 5.x */
	width/* */:/**/28px;	/* Other browsers */
	width: /**/28px;
}
#contextMenu .itemTxt{
	float:left;		
	width: 120px;	/* IE 5.x */
	width/* */:/**/140px;	/* Other browsers */
	width: /**/140px;		
}

.dialog {
	position:absolute;
	display: none;
	width:400px;
	padding-top:5px;
	padding-left:0px;
	padding-right:0px;
	padding-bottom:10px;
	background-color:#eee;
	border:solid #E2EBED 2px;
	z-index: 999;
}

#overlay{
	display:none;
	position:absolute;
	top:0;
	left:0;
	width:1600px;
	height:1600px;
	z-index:500;
	background-color:#000;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
</STYLE>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/AjaxQueryDesigner.js</cms:link>" ></script>
</head>

<body onload="initDesignerPanel();">
<table width="920" height="600" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="220" height="250" valign="top">
    	<div align="left" height="20" style="width: 220px; background-color: #93BEE2;"><font color="#003399" size="4" style="font-weight: bold;">Outline</font></div>
    	<div align="left" id="query_outline_frame">
    		<div align="left" style="padding-left: 5px;">Root Query</div>
    		<div align="left" style="padding-left: 20px;" id="query_outline_select">Select</div>
    		<div align="left" style="padding-left: 20px;" id="query_outline_from">From</div>   
    		<div align="left" style="padding-left: 20px;" id="query_outline_where">Where</div>   
    		<div align="left" style="padding-left: 20px;" id="query_outline_groupby">Group by</div>  
    		<div align="left" style="padding-left: 20px;" id="query_outline_having">Having</div>      		    		   		 		
    		<div align="left" style="padding-left: 5px;"  id="query_outline_orderby">Order by</div>    		
    	</div>
    </td>
    <td width="700" rowspan="2" valign="top"><div id="divTabButtons"></div><div align="left" id="query_holder_frame"></div></td>
  </tr>
  <tr>
    <td height="350" valign="top"><div align="left" height="20" style="background-color: #93BEE2;"><font color="#003399" size="4" style="font-weight: bold;">Tables</font></div><div align="left" id="table_list_frame"></div></td>
  </tr>
</table>

<ul id="contextMenu">
</ul>

<div class="dialog" id="addConditionWhere">
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<textarea id="condition_left_part_where" rows="5" cols="46"></textarea>
</div>
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<select id="condition_comparator_where" style="PADDING-LEFT: 4px">
<option value="EQUAL" selected="selected">=</option>
<option value="LESS_THAN">&lt;</option>
<option value="GREATER_THAN">&gt;</option>
<option value="EQUAL_LESS_THAN">&lt;=</option>
<option value="EQUAL_GREATER_THAN">=&gt;</option>
<option value="NOT_EQUAL">&lt;&gt;</option>
<option value="LIKE">LIKE</option>
<option value="NOT_LIKE">NOT LIKE</option>
</select>
</div>
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<textarea id="condition_right_part_where" rows="5" cols="46"></textarea>
</div>
<div style="float: left; width: 100%; text-align: right; padding-right: 10px; padding-top: 5px;" >
<input type="button" onclick="AddConditionWhere();" name="dobookmark" value="Ok" /> <input class="goButton" type="button" onclick="AddConditionCancel();" value="Cancel" />
</div>
</div>

<div class="dialog" id="addConditionHaving">
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<textarea id="condition_left_part_having" rows="5" cols="46"></textarea>
</div>
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<select id="condition_comparator_having" style="PADDING-LEFT: 4px">
<option value="EQUAL" selected="selected">=</option>
<option value="LESS_THAN">&lt;</option>
<option value="GREATER_THAN">&gt;</option>
<option value="EQUAL_LESS_THAN">&lt;=</option>
<option value="EQUAL_GREATER_THAN">=&gt;</option>
<option value="NOT_EQUAL">&lt;&gt;</option>
<option value="LIKE">LIKE</option>
<option value="NOT_LIKE">NOT LIKE</option>
</select>
</div>
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<textarea id="condition_right_part_having" rows="5" cols="46"></textarea>
</div>
<div style="float: left; width: 100%; text-align: right; padding-right: 10px; padding-top: 5px;" >
<input type="button" onclick="AddConditionHaving();" name="dobookmark" value="Ok" /> <input class="goButton" type="button" onclick="AddConditionCancel();" value="Cancel" />
</div>
</div>

<div class="dialog" id="addExpression">
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<input type="hidden" id="expression_table"/>
<input type="hidden" id="expression_attribute"/>
</div>
<div style="float: left; width: 100%; text-align: left; padding-left: 5px;" >
<select id="expression_name" style="PADDING-LEFT: 4px">
<option value="0" selected="selected">count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
<option value="1">max</option>
<option value="2">min</option>
<option value="3">sum</option>
</select>
</div>
<div style="float: left; width: 100%; text-align: right; padding-right: 10px; padding-top: 5px;" >
<input type="button" onclick="AddExpression();" name="dobookmark" value="Ok" /> <input class="goButton" type="button" onclick="AddConditionCancel();" value="Cancel" />
</div>
</div>

</body>

</html>