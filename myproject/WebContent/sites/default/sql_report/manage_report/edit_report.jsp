<%@ page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.ManageSQLReportHandler"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@page import="com.bp.pensionline.sqlreport.app.jasper.PLReportProducer"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.sqlreport.dao.ReportGroupDao"%>
<%@page import="com.bp.pensionline.sqlreport.constants.Constant"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<cms:include property="template" element="head" />
<%
	CmsUser currentUser = SystemAccount.getCurrentUser(request);

	String reportId = request.getParameter("id");	
	if (currentUser != null && reportId != null && reportId.trim().length() == 36)
	{
		Report report = PLReportProducer.getMasterReportAllInfo(reportId);
		String title = report.getReportTitle();
		String startCharStr = "";
		if (title != null && title.trim().length() > 0)
		{
			startCharStr = "?startwith=" + title.trim().charAt(0);
		}
		
		
		String description = report.getReportDescription();
		String datasource = report.getDatasource();
		Vector sections = report.getSections();
		if (datasource != null)
		{
			if ((datasource.equals("webstats") || datasource.equals("opencms") || datasource.equals("pensionline")) &&
					!ReportGroupDao.isUserInCmsGroup(currentUser.getName(), "Administrators"))
			{
%>
<h1>Access denied.</h1>
<p>Sorry, you do not have enough permissions to edit this report.</p>
<%
			}
			else
			{
%>
<script type="text/javascript">
titleStartCharStr = '<%=startCharStr%>';
reportSections = new Array();
var section = null;
var sectionParamsUpdate = null;
<%		
		for (int i=0; i< sections.size(); i++)
		{
			ReportSection section = (ReportSection)sections.elementAt(i);
			if (section != null)
			{
				String sectionTitle = section.getSectionTitle();
				String sectionQuery = section.getSectionQuery();
				String sectionDescription = section.getSectionDescription();

				if (sectionTitle != null)
				{
					sectionTitle = sectionTitle.replaceAll("\\\"", "\\\\\"");
					sectionTitle = sectionTitle.replaceAll("\\n", "\\\\n");
				}
				else
				{
					sectionTitle = "";
				}
				if (sectionQuery != null)
				{
					sectionQuery = sectionQuery.replaceAll("\\\"", "\\\\\"");
					sectionQuery = sectionQuery.replaceAll("[\\n\\r]+", "\\\\n");
				}
				else
				{
					sectionQuery = "";
				}
				if (sectionDescription != null)
				{
					sectionDescription = sectionDescription.replaceAll("\\\"", "\\\\\"");
					sectionDescription = sectionDescription.replaceAll("\\n", "\\\\n");
				}
				else
				{
					sectionDescription = "";
				}
				Vector params = section.getParams();
%>
section = new Array();
sectionParamsUpdate = new Array();
<%			
				for (int j=0; j<params.size(); j++)
				{
					SQLSectionParameterDTO param = (SQLSectionParameterDTO)params.elementAt(j);
					String paramName = param.getName();
					int paramType = param.getType();
					String paramTypeStr = Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_STRING;
					if (paramType == SQLSectionParameterDTO.PARAMETER_TYPE_NUMERIC)
					{
						paramTypeStr = Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_NUMBER;
					}
					else if (paramType == SQLSectionParameterDTO.PARAMETER_TYPE_DATE)
					{
						paramTypeStr = Constant.AJAX_SQL_REPORT_SECTION_PARAM_TYPE_DATE;
					}
					String paramDefaultValue = param.getDefaultValue();
					String paramDescription = param.getDescription();
%>
sectionParamsUpdate.push(new Array("<%=paramName%>", "<%=paramTypeStr%>", "", <%=paramDefaultValue%>, "<%=paramDescription%>"));
<%
				}
%>
section.push("<%=sectionTitle%>", "<%=sectionDescription%>", "<%=sectionQuery%>", sectionParamsUpdate);
reportSections.push(section);
<%
			}
		}
%>
</script>
<h1>Edit report</h1>
<p>Please input the Title, Description (optional) and add the Section(s) of your report.</p>
    <div class="report_component_holder">
    <div class="report_label"><label for="report_datasource">Data source</label> </div>
    <div class="report_value">
    <select id="report_datasource" disabled="disabled">
<%
			if (datasource.equals("AQUILLA"))
			{
%>    
	<option value="AQUILLA">Aquila(Oracle)</option>
<% 
			}
			else if (datasource.equals("webstats"))
			{
%>
	<option value="webstats">Webstats(MySQL)</option>
<% 
			}
			else if (datasource.equals("opencms"))
			{
%>
	<option value="opencms">CMS data(MySQL)</option>
<% 
			}
			else if (datasource.equals("pensionline"))
			{
%>
	<option value="pensionline">PensionLine operational(MySQL)</option>
<% 
			}
			else if (datasource.equals("aviary"))
			{
%>
	<option value="aviary">Aviary (MSSQL Server)</option>
<% 
			}
			else if (datasource.equals("ev"))
			{
%>
	<option value="ev">ExtraView (MSSQL Server)</option>
<% 
			}
			else if (datasource.equals("release"))
			{
%>
	<option value="release">Releases (MSSQL Server)</option>
<% 
			}
			else if (datasource.equals("report"))
			{
%>
	<option value="report">Reporting (MSSQL Server)</option>
<% 
			}
%>
    </select> 
    </div>
    </div>
    <div class="report_component_holder">
    <div class="report_label"><label for="report_title"><%=datasource %></label> </div>
    <div class="report_value"><input id="report_title" size="50" name="_report_title" type="text" value="<%=title %>"/> </div>
    </div>
    <div class="report_component_holder">
    <div class="report_label"><label for="report_description">Description</label> </div>
    <div class="report_value"><textarea id="report_description" rows="5" cols="74"><%=description %></textarea> </div>
    </div>
    <br />
    <div class="report_component_holder">
    <div class="report_label"><label for="report_label">Section(s)</label> </div>
    <div class="report_value">
    <div>You can add more than one section to the report. A section will be built with the title and query input and rendered based on the order you supply.</div>
    <br />
    <div id="report_sections">
    </div>
    </div>
    </div>
    <div class="report_component_holder">
    <div class="report_label"><label>&nbsp;</label> </div>
    <div class="report_value"><input class="goButton" id="_add_table" type="button" onclick="openSection(); return false;" name="_add_table" value="Add section" /> </div>
    </div>
    <br />
    <div class="add_section_holder" id="add_section_id" style="DISPLAY: none">
    <form id="add_section_form">
    <h2>Add section</h2>
    <p>Please input title and description for the section.</p>
    <div class="report_component_holder">
    <div class="report_label"><label for="section_title">Title</label> </div>
    <div class="report_value"><input id="section_title" size="50" name="_report_title" type="text" /> </div>
    </div>
    <div class="report_component_holder">
    <div class="report_label"><label for="section_description">Description</label> </div>
    <div class="report_value"><textarea id="section_description" rows="5" cols="50"></textarea> </div>
    </div>
    <br />
    <div class="report_component_holder">
    <h2>Query builder</h2>
    </div>
    <div class="rb_query_holder">
    <div class="rb_query_lable_holder" style="display: none;">    
    <a href="" onclick="popUpQueryHelp(); return false;"><img alt="Help" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/question.gif" /> </a>
    <h3>Query</h3>
    </div>
    <textarea class="rb_query" id="section_query"></textarea> </div>
    <div class="rb_list_holder">
    <div class="rb_list_lable_holder">
    <a onclick="showParamDialog(-1, this); return false;" href=""><img alt="Add report parameters" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/add.gif" /> </a>    
    <h3>Parameters</h3>
    </div>
    <div class="rb_list_att" id="section_param_list"></div>
    </div>
    <div class="rb_list_lable_holder">
    <a onclick="extractParamFromQuery(document.getElementById('section_query').value, 'section_query'); return false;" href="http://localhost:8080/"><img alt="Update parameters from query" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/arrow_r.gif" /> </a></div>    
    <h4>Update param</h4>
    <div class="report_component_holder">
    <div class="report_label"><label for="section_title">Load query</label> </div>
    <div class="report_value"><input id="query_file" type="file" onchange="uploadQuery();" name="query_file" />&nbsp;(*.txt,*.sql)</div>
    </div>  
    <!--
    <div class="report_component_holder">
    <div class="report_label"><label for="section_title">&nbsp;</label> </div>
    <div class="report_value"><input class="goButton" id="designer" type="button" onclick="popUpQueryDesigner();" name="query_designer" value="Query designer" /> </div>
    </div> 
     -->         
    <br />
    <div class="report_component_holder">
    <div class="login_label"><label>&nbsp;</label> </div>
    <div class="login_value"><input class="goButton" id="section_preview" type="button" onclick="popUpParamInputDialog(this);" name="section_preview" value="Preview" />&nbsp; <input class="goButton" id="section_save" type="button" onclick="saveSection(); return false;" name="section_save" value="Save Query" />&nbsp; <input class="goButton" id="dd_save" type="button" onclick="closeSection(); return false;" name="dd_save" value="Cancel" />&nbsp;</div>
    </div> 
    </form> 
    </div>        
    <br />
    <br />
    <div class="report_component_holder">
    <div class="report_label"><label>&nbsp;</label> </div>
    <div class="report_value" style="TEXT-ALIGN: right">
    	<input class="goButton" id="_edit_report" type="button" onclick="sendEditReportRequest('<%=reportId%>');" name="_edit_report" value="Save" />&nbsp; 
    	<input class="goButton" id="_remove_report" type="button" onclick="sendRemoveReportRequest('<%=reportId%>');" name="_remove_report" value="Remove" />
    	<input class="goButton" id="_cancel_report" type="button" name="_cancel_report" value="Cancel" onclick="cancelAddEditReport();"/>
    </div>
    </div>
<div class="dialog" id="add_param_dialog">
<div class="login_component_holder">
<div class="login_label">Name</div>
<div class="login_value"><input id="section_param_name" type="text" /> </div>
</div>
<div class="login_component_holder">
<div class="login_label">Type</div>
<div class="login_value"><select id="section_param_type" style="PADDING-LEFT: 4px" onchange="updateParamTypeHelpers(document.getElementById('section_param_type').value, 'section_param_default_value');">
<option value="PARAM_TYPE_STRING" selected="selected">String</option>
<option value="PARAM_TYPE_NUMBER">Number</option>
<option value="PARAM_TYPE_DATE">Date</option>
</select> </div>
</div>
<div class="login_component_holder">
<div class="login_label">Default value</div>
<div class="login_value"><input id="section_param_default_value" type="text" /> </div>
</div>
<div class="login_component_holder">
<div class="login_label">Description</div>
<div class="login_value"><input id="section_param_description" size="35" type="text" /> </div>
</div>
<div class="login_component_holder">
<div class="login_label">&nbsp;</div>
<div class="login_value">
<input class="goButton" type="button" onclick="removeSectionParam(document.getElementById('section_param_name').value);" value="Delete" />
<input class="goButton" type="button" onclick="AddParamConfirm();" value="Ok" /> 
<input class="goButton" type="button" onclick="AddParamCancel();" value="Cancel" /></div>
</div>
</div>
<div class="dialog" id="input_param_dialog"></div>
<script>
updateSectionListHTML();
</script>
<%					
			}
		}
%>
<%
	}
	else
	{
%>
<h1>Access denied.</h1>
<p>Sorry, you do not have enough permissions to edit this report.</p>
<%
	}
%>
<cms:include property="template" element="foot" />