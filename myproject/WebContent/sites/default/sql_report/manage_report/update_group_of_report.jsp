<%@page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<cms:include property="template" element="head" />
<%
	Vector reportGroups = ManageReportGroupHandler.listAllReportGroups();
	String reportId = request.getParameter("reportId");
%>
<script type="text/javascript">
	reportGroupArray = new Array();
	<%
		for (int i = 0; i < reportGroups.size(); i++)
		{
			ReportGroup reportGroupDTO = (ReportGroup)reportGroups.elementAt(i);
			if (reportGroupDTO != null)
			{
				String groupId = reportGroupDTO.getId();				
	%>			
	reportGroupArray.push("<%=groupId%>");
	<%
			
			}
		}
	%>	
</script>
<h1>Edit groups of report</h1>
<p>The table bellow contains the all the groups created. Please check in the box to add the report to the group.</p>
<table cellspacing="0" cellpadding="0" class="publishtable" width="100%">
<tbody>
<%
	for (int i = 0; i < reportGroups.size(); i++)
	{
		ReportGroup report = (ReportGroup)reportGroups.elementAt(i);
		if (report != null)
		{
			String groupId = report.getId();
			String name = report.getName();
			if (name == null || name.trim().equals(""))
			{
				name = "Unknown";
			}
			boolean checked = UpdateGroupsForReportHandler.isReportAddedToGroup(reportId, groupId);
			String checkedStr = "";
			if (checked)
			{
				checkedStr = "checked=\"checked\"";
			}
%>
	<tr>
		<td class="label" width="15"><input type="checkbox" <%=checkedStr %> id="<%=groupId%>"/> </td>
		<td class="label" width="250"><%=name%></td>
	</tr>
<%
		}
	}
%>
</tbody>
</table>
<br/>
<div class="report_component_holder">
<div style="TEXT-ALIGN: right">
	<input class="goButton" id="_save_report_group" type="button" onclick="saveGroupForReport('<%=reportId%>');" value="Add Report Group" />&nbsp;
	<input class="goButton" id="_cancel_report_group" type="button" onclick="cancelAddEditReport();" value="Back" />&nbsp;	
</div>
</div>
</div>
<cms:include property="template" element="foot" />