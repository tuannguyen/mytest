<%@ page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<cms:include property="template" element="head" />
<%
	Vector reports = ManageSQLReportHandler.listAvailableReports();
	String groupId = request.getParameter("groupId");
%>
<script type="text/javascript">
	reportGroupArray = new Array();
	<%
		for (int i = 0; i < reports.size(); i++)
		{
			Report reportDTO = (Report)reports.elementAt(i);
			if (reportDTO != null)
			{
				String reportId = reportDTO.getReportId();				
	%>			
	reportInGroupArray.push("<%=reportId%>");
	<%
			
			}
		}
	%>	
</script>
<h1>Update reports of group</h1>
<p>The table bellow contains the all the reports created. Please check in the box to add the report to this group.</p>
<table cellspacing="0" cellpadding="0" class="publishtable" width="100%">
<tbody>
<%
	for (int i = 0; i < reports.size(); i++)
	{
		Report report = (Report)reports.elementAt(i);
		if (report != null)
		{
			String reportId = report.getReportId();
			String name = report.getReportTitle();
			if (name == null || name.trim().equals(""))
			{
				name = "Unknown";
			}
			boolean checked = UpdateGroupsForReportHandler.isReportAddedToGroup(reportId, groupId);
			String checkedStr = "";
			if (checked)
			{
				checkedStr = "checked=\"checked\"";
			}
%>
	<tr>
		<td class="label" width="15"><input type="checkbox" <%=checkedStr %> id="<%=reportId%>"/> </td>
		<td class="label" width="250"><a href="edit_report.jsp?id=<%=reportId%>"><%=name%> </a></td>
	</tr>
<%
		}
	}
%>
</tbody>
</table>
<br/>
<div class="report_component_holder">
<div style="TEXT-ALIGN: right">
	<input class="goButton" id="_save_report_group" type="button" onclick="saveReportForGroup('<%=groupId%>');" value="Add To This Group" />&nbsp;
	<input class="goButton" id="_cancel_report_group" type="button" onclick="cancelAddEditReportGroup();" value="Back" />&nbsp;	
</div>
</div>
</div>
<cms:include property="template" element="foot" />