<%@ page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="org.opencms.file.CmsUser"%>
<cms:include property="template" element="head" />
<%
	String reportRunnerGroup = "PL_REPORT_RUNNER";
	List usersInGroup = UpdateUserInReportGroupHandler.getUsersOfCmsGroup(reportRunnerGroup);
	String groupId = request.getParameter("groupId");
	String groupName = "";
	ReportGroup groupDTO = ManageReportGroupHandler.getReportGroupInfo(groupId);
	if (groupDTO != null)
	{
		groupName = groupDTO.getName();
	}
%>
<script type="text/javascript">
	userInGroupArray = new Array();
	<%
		for (int i = 0; i < usersInGroup.size(); i++)
		{
			CmsUser cmsUser = (CmsUser)usersInGroup.get(i);
			if (cmsUser != null)
			{
				String userId = cmsUser.getId().toString();				
	%>			
	userInGroupArray.push("<%=userId%>");
	<%
			
			}
		}
	%>	
</script>
<h1>Manage users who are able to run the report</h1>
<p>The table lists all users in the <%=reportRunnerGroup%> group.
Please check the box to assign the users you wish to run the reports in the group <%=groupName %></p>
<table cellspacing="0" cellpadding="0" class="publishtable" width="100%">
<tbody>
<%
	for (int i = 0; i < usersInGroup.size(); i++)
	{
		CmsUser cmsUser = (CmsUser)usersInGroup.get(i);
		if (cmsUser != null)
		{
	String userId = cmsUser.getId().toString();	
	String userName = cmsUser.getName();
	if (userName == null || userName.trim().equals(""))
	{
		userName = "Unknown";
	}
	boolean checked = UpdateUserInReportGroupHandler.isUserAddedToGroup(userId, groupId);
	String checkedStr = "";
	if (checked)
	{
		checkedStr = "checked=\"checked\"";
	}
%>
	<tr>
		<td class="label" width="15"><input type="checkbox" <%=checkedStr %> id="<%=userId%>"/> </td>
		<td class="label" width="250"><%=userName%></td>
	</tr>
<%
		}
	}
%>
</tbody>
</table>
<br/>
<div class="report_component_holder">
<div style="TEXT-ALIGN: right">
	<input class="goButton" id="_save_report_group" type="button" onclick="saveUserForReportGroup('<%=groupId%>');" value="Save" />&nbsp;
	<input class="goButton" id="_cancel_report_group" type="button" onclick="cancelAddEditReportGroup();" value="Back" />&nbsp;	
</div>
</div>
</div>
<cms:include property="template" element="foot" />