<%@ page session="true" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.*"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="org.opencms.file.CmsUser"%>
<%@page import="org.opencms.main.OpenCms" %>
<cms:include property="template" element="head" />

		
<style>
p.css-vertical-text {
	writing-mode:tb-rl;
	-webkit-transform:rotate(90deg);
	-moz-transform:rotate(90deg);
	-o-transform: rotate(90deg);
	padding: 0px;
	float: left;
}

</style>

<script type="text/javascript">
	userInGroupArray = new Array();
<%
	OpenCms.getMemoryMonitor().clearCache();

	String reportRunnerGroup = "PL_REPORT_RUNNER";
	List unsortedRunners = UpdateUserInReportGroupHandler.getUsersOfCmsGroup(reportRunnerGroup);
	// sort runners by alphabet
	List runners = new ArrayList();
	int numberOfRunners = unsortedRunners.size();
	for (int size = 0; size < numberOfRunners; size++)
	{
		String minUserName = "~";
		CmsUser minCmsUser = null;

		for (int i = 0; i < unsortedRunners.size(); i++)
		{
			CmsUser cmsUser = (CmsUser)unsortedRunners.get(i);
			if (cmsUser != null && !runners.contains(cmsUser) && cmsUser.isEnabled() == true)
			{
				String userId = cmsUser.getId().toString();	
				String userName = cmsUser.getFirstname() + " " + cmsUser.getLastname();
				if (userName == null || userName.trim().equals(""))
				{
					userName = "Unknown";
				}
								
				if (userName.compareToIgnoreCase(minUserName) < 0)
				{
					minUserName = userName;
					minCmsUser = cmsUser;
				}
			}
		}
		if (minCmsUser != null && minCmsUser.isEnabled() == true)
		{
			runners.add(minCmsUser);		
		}
	}
	Vector groups = ManageReportGroupHandler.listAllReportGroups();
	for (int i = 0; i < runners.size(); i++)
	{
		CmsUser cmsUser = (CmsUser)runners.get(i);
		if (cmsUser != null && cmsUser.isEnabled() == true)
		{
			String userId = cmsUser.getId().toString();	
			for (int j = 0; j < groups.size(); j++)
			{
				ReportGroup groupDTO = (ReportGroup)groups.elementAt(j);
				if (groupDTO != null)
				{
					String groupId = groupDTO.getId();										
%>
	userInGroupArray.push("<%=userId+"_"+groupId%>");
<%
				}
			}
		}
	}
%>
</script>
<h1>Assign users to run the reports in the group</h1>
<p>The table below lists all the users in rows and report groups in columns. Please select the check box to
assign the user privileges to run the reports in a group.</p>
<table cellspacing="0" cellpadding="0" class="publishtable" width="100%" >
<tbody >
	<tr>
		<td class="label"></td>
<% 	
	for (int i = 0; i < groups.size(); i++)
	{
		ReportGroup groupDTO = (ReportGroup)groups.elementAt(i);
		String groupName = (groupDTO == null) ? "" : (groupDTO.getName());
%>
		<td class="label_v" width="15"><p class="css-vertical-text"><%=groupName%>
		</td>
<% 			
	}
%>		
	</tr>
<%
	for (int i = 0; i < runners.size(); i++)
	{
		CmsUser cmsUser = (CmsUser)runners.get(i);
		if (cmsUser != null && cmsUser.isEnabled() == true)
		{
			String userId = cmsUser.getId().toString();	
			String userName = cmsUser.getFirstname() + " " + cmsUser.getLastname();
			if (userName == null || userName.trim().equals(""))
			{
				userName = "Unknown";
			}
%>
	<tr>
		<td class="label_h"><%=userName%></td>
<% 	
			for (int j = 0; j < groups.size(); j++)
			{
				ReportGroup groupDTO = (ReportGroup)groups.elementAt(j);
				String groupId = (groupDTO == null) ? "" : (groupDTO.getId());
				boolean checked = UpdateUserInReportGroupHandler.isUserAddedToGroup(userId, groupId);
				String checkedStr = "";
				if (checked)
				{
					checkedStr = "checked=\"checked\"";
				}				
%>
		<td class="label" width="15"><input type="checkbox" <%=checkedStr %> id="<%=userId+"_"+groupId%>"/></td>
<% 			
			}
%>			
	</tr>
<%
		}
	}
%>
</tbody>
</table>
<br/>
<div class="report_component_holder">
<div style="TEXT-ALIGN: right">
	<input class="goButton" id="_save_report_group" type="button" onclick="saveUserForReportGroup('');"value="Save" />&nbsp;
	<input class="goButton" id="_cancel_report_group" type="button" onclick="cancelAddEditReport();" value="Back" />&nbsp;	
</div>
</div>
</div>
<cms:include property="template" element="foot" />