<%@page session="false" %>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.handler.ManageReportGroupHandler"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.*"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.sqlreport.constants.Constant"%>
<cms:include property="template" element="head" />
<%
	String groupId = request.getParameter("id");
	if (groupId != null && !groupId.trim().equals(""))
	{
		ReportGroup reportGroup = ManageReportGroupHandler.getReportGroupInfo(groupId);
		if (reportGroup != null)
		{
			String name = reportGroup.getName();
%>
<h1>Edit report group</h1>
<p>Please fill in the report group name.</p>
<form action="" method="get">
    <div class="report_component_holder">
    <div class="report_label"><label for="report_group_name">Name</label> </div>
    <div class="report_value"><input id="report_group_name" size="50" name="report_group_name" type="text" value="<%=name %>"/> </div>
    </div>
	<br/>
    <div class="report_component_holder">
    <div class="report_label"><label>&nbsp;</label> </div>
    <div class="report_value" style="TEXT-ALIGN: right">
    	<input class="goButton" id="_edit_report" type="button" onclick="sendEditReportGroupRequest('<%=groupId%>');" name="_edit_report" value="Save" />&nbsp; 
    	<input class="goButton" id="_remove_report" type="button" onclick="sendRemoveReportGroupRequest('<%=groupId%>');" name="_remove_report" value="Remove" />
    	<input class="goButton" id="_cancel_report" type="button" name="_cancel_report" value="Cancel" onclick="cancelAddEditReportGroup();"/></div>
    </div>
</form>
<%
		}
	}
%>
<cms:include property="template" element="foot" />