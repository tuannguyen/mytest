<%@page pageEncoding="UTF-8" %>
<%@page session="false" %>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="com.bp.pensionline.sqlreport.dto.db.User"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.Schedule"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.Schedule.Status"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.ScheduleRunLog"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>

<%! 
private String formatString(HttpServletRequest request, String value){
	if(value == null || value.trim().isEmpty()){
		return "";
	}
	String ret = value.replaceAll("(\r\n|\r|\n|\n\r)", "\\\\n");
	ret = ret.replaceAll("(\")", "\\\\\"");
	if (ret.endsWith("\\"))
	{
		ret = ret + "\\";
	}
	
	return ret;
}

private String escapeHTML(String s){
   StringBuffer sb = new StringBuffer();
   int n = s.length();
   for (int i = 0; i < n; i++) {
      char c = s.charAt(i);
      switch (c) {
         case '<': sb.append("&lt;"); break;
         case '>': sb.append("&gt;"); break;
         case '&': sb.append("&amp;"); break;
         case '"': sb.append("&quot;"); break;
         case 'à': sb.append("&agrave;");break;
         case 'À': sb.append("&Agrave;");break;
         case 'â': sb.append("&acirc;");break;
         case 'Â': sb.append("&Acirc;");break;
         case 'ä': sb.append("&auml;");break;
         case 'Ä': sb.append("&Auml;");break;
         case 'å': sb.append("&aring;");break;
         case 'Å': sb.append("&Aring;");break;
         case 'æ': sb.append("&aelig;");break;
         case 'Æ': sb.append("&AElig;");break;
         case 'ç': sb.append("&ccedil;");break;
         case 'Ç': sb.append("&Ccedil;");break;
         case 'é': sb.append("&eacute;");break;
         case 'É': sb.append("&Eacute;");break;
         case 'è': sb.append("&egrave;");break;
         case 'È': sb.append("&Egrave;");break;
         case 'ê': sb.append("&ecirc;");break;
         case 'Ê': sb.append("&Ecirc;");break;
         case 'ë': sb.append("&euml;");break;
         case 'Ë': sb.append("&Euml;");break;
         case 'ï': sb.append("&iuml;");break;
         case 'Ï': sb.append("&Iuml;");break;
         case 'ô': sb.append("&ocirc;");break;
         case 'Ô': sb.append("&Ocirc;");break;
         case 'ö': sb.append("&ouml;");break;
         case 'Ö': sb.append("&Ouml;");break;
         case 'ø': sb.append("&oslash;");break;
         case 'Ø': sb.append("&Oslash;");break;
         case 'ß': sb.append("&szlig;");break;
         case 'ù': sb.append("&ugrave;");break;
         case 'Ù': sb.append("&Ugrave;");break;         
         case 'û': sb.append("&ucirc;");break;         
         case 'Û': sb.append("&Ucirc;");break;
         case 'ü': sb.append("&uuml;");break;
         case 'Ü': sb.append("&Uuml;");break;
         case '®': sb.append("&reg;");break;         
         case '©': sb.append("&copy;");break;   
         case '€': sb.append("&euro;"); break;
         // be carefull with this one (non-breaking whitee space)
         case ' ': sb.append("&nbsp;");break;         
         
         default:  sb.append(c); break;
      }
   }
   return sb.toString();
}

%>
<% 
	SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	List<Schedule> schedules = (List<Schedule>) request.getAttribute("schedules");
	String reportGroupId = request.getParameter("reportGroupId");
	String reportGroupName = request.getParameter("reportGroupName");
	if(schedules == null){ 
		String servletUrl = "/content/ManageSchedulesHandler";
		if(reportGroupId != null && !(reportGroupId = reportGroupId.trim()).equals("")){
			servletUrl = servletUrl + "?reportGroupId=" + reportGroupId + "&reportGroupName=" + reportGroupName;
		}
		System.out.println("Redirecting to " + servletUrl);
		response.sendRedirect(servletUrl);
		//this.getServletContext().getRequestDispatcher(servletUrl).forward(request, response);
		return;
	}
	String requestNo = (String) request.getSession().getAttribute("requestNo");
	if(requestNo == null){
		requestNo = "0";
		request.getSession().setAttribute("requestNo", requestNo);
	}		
%>


<%@page import="java.net.URLDecoder"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.opencms.file.CmsUser"%>
<%@page import="com.bp.pensionline.constants.Environment"%>
<%@page import="com.bp.pensionline.sqlreport.handler.RunSQLReportHandler"%>
<%@page import="com.bp.pensionline.sqlreport.app.impl.SchedulingAppImpl"%><cms:include property="template" element="head" />
<style>
<!--
.numeric-stepper {
	width:33px;
	height:17px;
	display:block;
	position:relative;
	overflow:hidden;
	border:1px inset #F0F0F0;
	margin-top: 1px;
	margin-bottom: 1px;
}
.numeric-stepper-label {
	height:17px;
	line-height:17px;
	vertical-align: bottom;
	text-align: left;
	float: left;
	font-size:14px;
}
.numeric-stepper input {
	width:65%;
	height:100%;
	float:left;
	text-align:center;
	vertical-align:center;
	font-size:14px;
	border:none;
	background:none;
	padding: none;
}

.numeric-stepper button {
	width:35%;
	height:50%;
	font-size:7px;
	padding:0;
	margin:0;
	z-index:100;
	text-align:center;
	vertical-align: text-top;
	position:absolute;
	border:none;
	background:none;
	right:0;
	
}
.numeric-stepper button.minus {
	bottom:0;
}
.pickable{
	font-size:14px;
	background-color: white;
}
label.pickable:hover{
	background-color: #B7B7B7;
}
 -->
</style>
<script type="text/javascript" src="/content/pl/sql_report/js/multi_select_box.js"></script>
<script type="text/javascript" src="/content/pl/sql_report/js/cron.js"></script>
<script type="text/javascript">
	var fileref=document.createElement("link");
	fileref.setAttribute("rel", "stylesheet");
	fileref.setAttribute("type", "text/css");
	fileref.setAttribute("href", "/content/pl/sql_report/css/multi_select_box.css");
	document.getElementsByTagName("head")[0].appendChild(fileref);
</script>
<h1>Manage report group schedules</h1>
<p>The list contains all available schedules assigned to the report group:<br>"<b><%= URLDecoder.decode(reportGroupName, "UTF-8")%></b>".</p>

<div>
<table width='100%' cellspacing='0' cellpadding='0' >
<thead>
	<tr height='20px'>
		<td width='12%' style='background-color:#037e01; color:#FFFFFF; font-weight: bold'> </td>
		<td width='55%' style='background-color:#037e01; color:#FFFFFF; font-weight: bold'>Schedule title</td>
		<td width='15%' style='background-color:#037e01; color:#FFFFFF; font-weight: bold'>Run level</td>
		<td style='background-color:#037e01; color:#FFFFFF; font-weight: bold'>Format</td>
	</tr>
</thead>
<tbody>
	<tr>
		<td colspan='4' style='padding: 0px;'>
		<div style='width:100%; height:220px; overflow-x: hidden; overflow-y: scroll;'>
		<table class='publishtable' cellpadding='0' cellspacing='1' style='width: 100%; table-layout: fixed;'>
<%
	if(schedules.size() == 0){ 
%>
		<tr>
			<td width='100%' colspan="4" style='word-wrap: break-word; break-word: break-all; border: 0px;'>
				No schedules available for this report group
			</td>
		</tr>
<%
	} else{
		for (Schedule schedule : schedules){
			if (schedule != null)
			{
				String sRunLevel = "";
				int runLevel = schedule.getRunLevel();
				switch(runLevel){
				case 50: 
					sRunLevel = "Default";
					break;
				case 75:
					sRunLevel = "Execs";
					break;
				case 100:
					sRunLevel = "Team";
					break;
					default:
						break;
				}
%>
			<tr>
				<td width='12%' style='width:12%;'>
					<a title="Edit this schedule" onclick="editSchedule(schedules['<%=schedule.getId() %>']);"><img title="Edit this schedule" alt="E" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/edit.png" /></a>
					<a title="Delete this schedule" onclick="deleteSchedule(schedules['<%=schedule.getId() %>']);"><img title="Delete this schedule" alt="D" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/delete.png" /></a>
					<a title="View details of this schedule" onclick="viewScheduleDetails(schedules['<%=schedule.getId() %>']);"><img title="View details of this schedule" alt="I" src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/info.png" /></a>
				</td>
				<td width='58%' style='width:58%; word-wrap: break-word; break-word: break-all;' id="td<%= schedule.getId()%>">
<%				if(schedule.getPrevRuns() != null && schedule.getPrevRuns().size() > 0){ %>
					<a href="/content/pl/sql_report/download_report.jsp?scheduleId=<%= schedule.getId()%>"><%=escapeHTML(schedule.getTitle()) %></a>
<%				} else{ %>
					<%=escapeHTML(schedule.getTitle()) %>
<%				} %>
				</td>
				<td width='15%' style='width:15%; word-wrap: break-word; break-word: break-all;'>
					<%=sRunLevel %>
				</td>
				<td width='15%' style='width:15%; word-wrap: break-word; break-word: break-all;'>
					<%=schedule.getOutputFormat() %>
				</td>
			</tr>
<%
			}//End if
		}//End for
	}//End Else
%>
		</table>
		</div>
		</td>
	</tr>
</tbody>
</table>
</div>
<div style="text-align: right">
	<br>
	<input type="button" value="Add schedule" onclick="addSchedule();" class="goButton" id='add_schedule_btn'>
</div>
<div id="lbScheduleInfoLayer"
	style="display: none; position: absolute; top: 0; left: 0; right: 0; bottom: 0; width: 100%; height:100%; text-align: center; ">
	<div id="dialogSchedule" class="dialog" align="center"
		style="display: block; position: absolute; vertical-align: middle;">
		<div class="dialog_title" id="infoTitle" style="word-wrap: break-word; break-word: break-all;"> </div>
	    <div class="component_holder">
	        <div id="lastRun" style="width: 90%; text-align: left;"></div>
	        <div id="nextRun" style="width: 90%; text-align: left;"></div>
	        <div id="firstRun" style="width: 90%; text-align: left;"></div>
	        <div id="numRuns" style="width: 90%; text-align: left;"></div>
	    </div>
	    <br>
        <div style="width: 90%; height: 20px; text-align: left;"><u>Recipients:</u></div>
		<div id="scrollWrapper" class="component_holder" style='width:90%; height:200px; overflow-x: hidden; overflow-y: auto;'></div>
		<div class="component_holder" style="width: 90%;">
			<div class="component_value" style="text-align: right; width: 100%;">
				<input type="button" value="Run now" id="btnRunSchedule" class="goButton">
				<input type="button" value="Close" onclick="javascript: hideScheduleDialog('dialogSchedule'); return false;" class="goButton">
			</div>
		</div>
	</div>
</div>

<div id="lbAddScheduleLayer" 
	style="display: none; position: absolute; top: 0; left: 0; right: 0; bottom: 0; width: 100%; height:100%; text-align: center; ">

<div id="dialogCronHelp" class="dialog" style="width: 265px; text-align: left; display: none; position: absolute; vertical-align: middle; top: 360px; left: 153px; background-color: lightyellow; z-index:100;">
<!--<pre style="font-size:11px;"> *&nbsp;&nbsp;*&nbsp;&nbsp;*&nbsp;&nbsp;*&nbsp;&nbsp;*&nbsp;&nbsp;*&nbsp;&nbsp;*&nbsp;&nbsp; command to be executed
 -&nbsp;&nbsp;-&nbsp;&nbsp;-&nbsp;&nbsp;-&nbsp;&nbsp;-&nbsp;&nbsp;-&nbsp;&nbsp;-
 |&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;+--years (optional) 
 |&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;+----days of week (1 - 7)(Sunday=1)
 |&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;+-------months (1 - 12)
 |&nbsp;&nbsp;|&nbsp;&nbsp;|&nbsp;&nbsp;+----------days of month (1 - 31)
 |&nbsp;&nbsp;|&nbsp;&nbsp;+-------------hours (0 - 23)
 |&nbsp;&nbsp;+----------------minutes (0 - 59)
 +-------------------seconds (0 - 59)
</pre>	
-->
<pre style="font-size:11px;">[1] [2] [3] [4] [5] [6] [7] 

[1]: seconds (0 - 59) 
[2]: minutes (0 - 59) 
[3]: hours (0 - 23) 
[4]: days of month (1 - 31) 
[5]: months (1 - 12) 
[6]: days of week (1 - 7)(Sunday=1) 
[7]: year (optional field) </pre>	
<div style="text-align: right;">
<input type="button" value="Close" onclick="javascript: hideDialogOnly('dialogCronHelp');document.getElementById('runLevel').style.display='inline';document.getElementById('outputFormat').style.display='inline'; return false;" class="goButton">&nbsp;&nbsp;
</div>
</div>
	<div id="dialogCronEditor" style="width: 202px; text-align: right; display: none; position: absolute; vertical-align: top; top: 360px; left: 153px; z-index:99; border: 2px outset silver; background-color: white;">
		<div style="clear: both;">
			<div style="float: left;">
				<label for="frequence0"><input type="radio" name="frequence" id="frequence0" value="0" onclick="setFrequence(0);" style="border: none;"/>Daily</label>&nbsp;
			</div>		
			<div style="float: left;">
				<label for="frequence1"><input type="radio" name="frequence" id="frequence1" value="1" onclick="setFrequence(1);" style="border: none;"/>Weekly</label>&nbsp;
			</div>		
			<div style="float: left;">
				<label for="frequence2"><input type="radio" name="frequence" id="frequence2" value="2" onclick="setFrequence(2);" style="border: none;" checked="checked"/>Monthly</label>
			</div>
			<br /><br />
		</div>
		<div style="clear: both;">
			<div class="numeric-stepper-label">Run at: &nbsp;</div>		
			<div id="hour" style="float: left; ">
				<span class="numeric-stepper" style="border-right: 0px;">
					<input type="text" name="ns_textbox1" id="ns_textbox1" size="2" maxlength="2" value="23"/>
					<button type="submit" name="ns_button_11" value="1" class="plus">+</button>
					<button type="submit" name="ns_button_12" value="-1" class="minus">-</button>
				</span>
			</div>
			<div id="minute" style="float: left">
				<span class="numeric-stepper" style="border-left: 0px">
					<input type="text" name="ns_textbox2" id="ns_textbox2" size="2" maxlength="2" value="59"/>
					<button type="submit" name="ns_button_21" value="1" class="plus">+</button>
					<button type="submit" name="ns_button_22" value="-1" class="minus">-</button>
				</span>
			</div>
		</div>
		<div id="daily" style="display: none;">
			<div style="clear: both; text-align: right;">
				<div class="numeric-stepper-label" style="clear: left;">Recur every&nbsp;</div>		
				<div id="recurenceD" style="float: left; ">
					<span class="numeric-stepper" style="border-right: 0px;">
						<input type="text" name="ns_textbox1d" id="ns_textbox1d" size="2" maxlength="2" value="01"/>
						<button type="submit" name="ns_button_11d" value="1" class="plus">+</button>
						<button type="submit" name="ns_button_12d" value="-1" class="minus">-</button>
					</span>
				</div>
				<div  class="numeric-stepper-label">&nbsp;&nbsp; day(s)</div>		
			</div>
		</div>
		<div id="weekly" style="clear: both; display: none;">
			<input type="checkbox" id="dow0"  style="display: none;"/>
			<div class="numeric-stepper-label" style="clear: both;">Run on:</div>
			<div style="width: 100%; text-align: center;">
			<table cellpadding="0" cellspacing="0" border="0" style="margin-left: auto; margin-right: auto; margin-bottom: 3px; border-collapse: collapse; border: 1px outset silver; clear: both;">
				<tbody>
					<tr>
						<td><label for="dow1" class="pickable" >&nbsp;&nbsp;S&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow1" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow2" class="pickable" >&nbsp;&nbsp;M&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow2" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow3" class="pickable" >&nbsp;&nbsp;T&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow3" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow4" class="pickable" >&nbsp;&nbsp;W&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow4" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow5" class="pickable" >&nbsp;&nbsp;T&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow5" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow6" class="pickable" >&nbsp;&nbsp;F&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow6" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dow7" class="pickable" >&nbsp;&nbsp;S&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dow7" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
				</tbody>
			</table>
			</div>
			<!--
			<div style="clear: both; text-align: right;">
				<div class="numeric-stepper-label" style="clear: left;">Recur every&nbsp;</div>		
				<div id="recurenceW" style="float: left; ">
					<span class="numeric-stepper" style="border-right: 0px;">
						<input type="text" name="ns_textbox1w" size="1" maxlength="1" value="1"/>
						<button type="submit" name="ns_button_11w" value="1" class="plus">+</button>
						<button type="submit" name="ns_button_12w" value="-1" class="minus">-</button>
					</span>
				</div>
				<div  class="numeric-stepper-label">&nbsp;week(s)</div>		
			</div>
			-->
		</div>
		<div id="monthly" style="display: block;">
			<div class="numeric-stepper-label" style="clear: both;">Run on:</div>
			<div style="width: 100%; text-align: center;">
			<table cellpadding="0" cellspacing="0" border="0" style="margin-left: auto; margin-right: auto; margin-bottom: 3px; border-collapse: collapse; border: 1px outset silver; clear: both;">
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input type="checkbox" id="dom0"  style="display: none;"/></td>
						<td><label for="dom1" class="pickable" id="lbl1">&nbsp;01&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom1" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom2" class="pickable" id="lbl2">&nbsp;02&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom2" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom3" class="pickable" id="lbl3">&nbsp;03&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom3" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
					<tr>
						<td><label for="dom4" class="pickable" id="lbl4">&nbsp;04&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom4" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom5" class="pickable" id="lbl5">&nbsp;05&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom5" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom6" class="pickable" id="lbl6">&nbsp;06&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom6" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom7" class="pickable" id="lbl7">&nbsp;07&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom7" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom8" class="pickable" id="lbl8">&nbsp;08&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom8" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom9" class="pickable" id="lbl9">&nbsp;09&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom9" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom10" class="pickable" id="lbl10">&nbsp;10&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom10" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
					<tr>
						<td><label for="dom11" class="pickable" id="lbl11">&nbsp;11&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom11" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom12" class="pickable" id="lbl12">&nbsp;12&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom12" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom13" class="pickable" id="lbl13">&nbsp;13&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom13" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom14" class="pickable" id="lbl14">&nbsp;14&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom14" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom15" class="pickable" id="lbl15">&nbsp;15&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom15" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom16" class="pickable" id="lbl16">&nbsp;16&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom16" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom17" class="pickable" id="lbl17">&nbsp;17&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom17" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
					<tr>
						<td><label for="dom18" class="pickable" id="lbl18">&nbsp;18&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom18" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom19" class="pickable" id="lbl19">&nbsp;19&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom19" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom20" class="pickable" id="lbl20">&nbsp;20&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom20" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom21" class="pickable" id="lbl21">&nbsp;21&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom21" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom22" class="pickable" id="lbl22">&nbsp;22&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom22" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom23" class="pickable" id="lbl23">&nbsp;23&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom23" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom24" class="pickable" id="lbl24">&nbsp;24&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom24" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
					<tr>
						<td><label for="dom25" class="pickable" id="lbl25">&nbsp;25&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom25" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom26" class="pickable" id="lbl26">&nbsp;26&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom26" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom27" class="pickable" id="lbl27">&nbsp;27&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom27" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom28" class="pickable" id="lbl28">&nbsp;28&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom28" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom29" class="pickable" id="lbl29">&nbsp;29&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom29" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom30" class="pickable" id="lbl30">&nbsp;30&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom30" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
						<td><label for="dom31" class="pickable" id="lbl31">&nbsp;31&nbsp;<input onclick="onCheckChange(this);" type="checkbox" id="dom31" style="width: 0px; border: 0px; padding: 0px; margin: 0px;"/></label></td>
					</tr>
				</tbody>
			</table>
			</div>
			<div style="clear: both;">
				<div class="numeric-stepper-label" style="clear: left;">Recur every&nbsp;</div>		
				<div id="recurenceM" style="float: left; ">
					<span class="numeric-stepper" style="border-right: 0px;">
						<input type="text" name="ns_textbox1m" id="ns_textbox1m" size="2" maxlength="2" value="01"/>
						<button type="submit" name="ns_button_11m" value="1" class="plus">+</button>
						<button type="submit" name="ns_button_12m" value="-1" class="minus">-</button>
					</span>
				</div>
				<div  class="numeric-stepper-label">month(s)</div>		
			</div>
		</div> <br /><br />
		<div style="clear: both; text-align: right; margin-bottom: 3px;">
			<div style="float: left; width: 140px;">
				<input type="button" value="&nbsp;Set&nbsp;" onclick="setCron();"/>&nbsp;
			</div>		
			<div style="float: left;">
				<input type="button" value="Close" onclick="javascript: hideDialogOnly('dialogCronEditor'); return false;">&nbsp;
			</div>		
		</div>
	</div>
	
	<div id="lbAddSchedule" class="dialog" align="center"
	style="display: block; position: absolute; vertical-align: middle; z-index: 98;">
	
<!--
	<div id="lbCronHelpLayer"
		style="z-index: 1; display: none; position: absolute; top: 0; left: 0; right: 0; bottom: 0; width: 100%; height:100%; text-align: center; ">
	</div>
-->
	<div class="dialog_title" id="editTitle">Schedule settings</div>
<div class="component_holder">
<p>Items marked with <span class="required_field">#</span> are required fields.</p>
</div>
<form method="post" id="schedule" action="/content/EditScheduleHandler" onsubmit="return onSubmitSchedule();">
<input type="hidden" id="reportGroupId" name="reportGroupId" value="<%=reportGroupId %>">
<input type="hidden" id="reportGroupName" name="reportGroupName" value="<%=reportGroupName %>">
<input type="hidden" id="id" name="id" value="">
<input type="hidden" id="status" name="status" value="">
<input type="hidden" id="requestNo" name="requestNo" value="<%= requestNo %>">
<div class="component_holder">
<table cellpadding="1" cellspacing="1" border="0" width="98%" style="width: 98%; table-layout: fixed;" >
	<tr>
		<td width="120px"></td>
		<td></td>
		<td width="18px"></td>
		<td width="18px"></td>
	</tr>
	<tr>
		<td align="right" class="td_left" height="25px">
			<span class="required_field">#</span>
			<label for="title">Title</label>
		</td>
		<td align="right" colspan="3" class="td_right">
			<input type="text" name="title" id="title" width="95%" style="width: 95%;" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="right" class="td_left" height="25px">
			<span class="required_field">#</span>
			<label for="cronExpr">Cron expression</label>
		</td>
		<td align="right" class="td_middle" colspan="2">
			<input type="text" name="cronExpr" maxlength="50" id="cronExpr" width="94%" style="width: 94%;">
		</td>
		<td align="right" class="td_right"> <!-- Original: class="td_middle" -->
			<input type="button" name="btnCronHelp" id="btnCronHelp" value="?" class="btnHelp" width="95%" style="width: 95%;" onclick="showDialogByBottomLeftOfEles('dialogCronHelp', 'cronExpr');document.getElementById('runLevel').style.display='none';document.getElementById('outputFormat').style.display='none';">
		</td>
		<!-- Temporarily disable CronEditor 
		<td align="right" class="td_right">
			<input type="button" name="btnCronDialog" id="btnCronDialog" value="#" class="btn1" width="95%" style="width: 95%;" onclick="showDialogByTopRightOfEles('dialogCronEditor', 'cronExpr', 'lbAddSchedule'); document.getElementById('frequence2').click();">
		</td>
		-->
	</tr>
	<tr>
		<td align="right" class="td_left" height="25px">
			<span class="required_field">#</span> 
			<label for="runLevel">Run level</label>
		</td>
		<td align="right" colspan="3" class="td_right">
			<select id="runLevel" name="runLevel">
				<option selected="selected" value="50"> Default </option>
				<option value="75"> Execs </option>
				<option value="100"> Team </option>
			</select>
		</td>
	</tr>
	<tr>
		<td align="right" class="td_left" height="25px">
			<span class="required_field">#</span> 
			<label for="outputFormat"> Format</label>
		</td>
		<td align="right" colspan="3" class="td_right">
			<select id="outputFormat" name="outputFormat">
				<option selected="selected" value="XLS"> XLS </option>
				<option value="PDF"> PDF </option>
				
			</select>
		</td>
	</tr>
	<tr>
		<td align="right" class="td_left" height="70px">
			<label for="notes">Note</label>
		</td>
		<td align="right" colspan="3" class="td_right">
			<textarea rows="3" cols="27" id="notes" name="notes" style="width: 94%;"></textarea>
		</td>
	</tr>
	<tr>
		<td align="right" class="td_left" height="25px">
			<label for="emailFrom">Sender's Email</label>
		</td>
		<td align="right" colspan="3" class="td_right">
			<input type="text" name="emailFrom" maxlength="50" id="emailFrom" width="94%" style="width: 94%;">
		</td>
	</tr>
	<tr>
		<td align="left" colspan="4" height="25px">
			<fieldset>
				<legend>Recipient(s):</legend>
				<select multiple name="selRecipientCandidates" id="selRecipientCandidates">
<%	
	List<CmsUser> recipients = (List<CmsUser>) request.getAttribute("recipients");
	String[] siNames = {"default", "execs", "team"};
	int[] siValues = {50, 75, 100};
	int optNo = 0;
	for(CmsUser user : recipients){
		String username = user.getName();
		//System.out.println("user name: " + username);
		// String siDescriptions = String.valueOf(user.getAdditionalInfo(Environment.MEMBER_ACCESS_LEVEL)).toLowerCase().trim();
		//Huy modified
		String siDescriptions = RunSQLReportHandler.getUserSecurityIndicatorDescription(user).toLowerCase().trim();
		int si = 50;
		for(int i = siNames.length - 1; i >=0; i--){
			if(siDescriptions.indexOf(siNames[i]) >= 0){
				si = siValues[i];
				break;
			}
		}
%>
					<option id="<%=optNo %><%=si %>" value="<%=username %>" ><%=username %></option>
<%	
	optNo++;
	} //End for
 %>
<!--
					<option value="2">Bob Vu M.D.</option>
					<option value="3">Epkozoson Rmonko</option>
					<option value="4">Xataranha</option>
					<option value="5">Ceradion</option>	
					<option value="6">Xayxam Saukhixin</option>
					<option value="7">Ton That Hoc</option>
					<option value="8">Ton That Tinh</option>
					<option value="9">Banh Thi Bu</option>
-->
				</select>
				<select multiple name="recipients" id="recipients">
				</select>
			</fieldset>
		</td>
	</tr>
</table>
</div>

<div style="clear: both; width: 98%; text-align:right;">
    <input type="submit" class="goButton" value="Save"> 
    <input type="button" class="goButton" onclick="javascript: hideDialogOnly('dialogCronEditor'); hideScheduleDialog('lbAddSchedule'); return false;" value="Close">
</div>
</form>

</div>
</div>
<script type="text/javascript">
	createMovableOptions("selRecipientCandidates","recipients",341,180,'Available users','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');

	var hourStepper = new NumericStepper("numeric-stepper", 0, 23, 1,"hour");
	var minuteStepper = new NumericStepper("numeric-stepper", 0, 59, 1,"minute");
	var recurenceStepperD = new NumericStepper("numeric-stepper", 1, 31, 1,"recurenceD");
	//var recurenceStepperW = new NumericStepper("numeric-stepper", 1, 4, 1,"recurenceW");
	var recurenceStepperM = new NumericStepper("numeric-stepper", 1, 12, 1,"recurenceM");
	var curIdx = 2;
	var cronEditors = new Array(3);
	cronEditors[0] = new CronEditor("daily", "ns_textbox1", "ns_textbox2", 0, "ns_textbox1d");
	cronEditors[1] = new CronEditor("weekly", "ns_textbox1", "ns_textbox2", 8);
	cronEditors[2] = new CronEditor("monthly", "ns_textbox1", "ns_textbox2", 32, "ns_textbox1m");
	
	var schedules = new Array();
<%	//JAVA CODE***************************************
	System.out.println("Number of schedules: " + schedules.size());
	for (Schedule schedule : schedules){
		if (schedule == null) continue;
		
 		List<User> receivers = schedule.getReceivers();
		StringBuilder sbReceivers = new StringBuilder("[");
		for(int j = 0; j < receivers.size(); j++){
			User receiver = receivers.get(j);
			if (receiver != null)
			{
				sbReceivers.append("\"").append(receiver.getUserName()).append("\",");
			}
		}
		if(sbReceivers.length() > 1){
			sbReceivers.deleteCharAt(sbReceivers.length() - 1);
		}
		sbReceivers.append("]");

		String lastRun = "";
		List<ScheduleRunLog> prevRuns = schedule.getPrevRuns();
		if(prevRuns != null && prevRuns.size() > 0){
			ScheduleRunLog lastRunLog = schedule.getPrevRuns().get(schedule.getPrevRuns().size() - 1);
			Date lastRunDate = lastRunLog.getRunningTime();
			lastRun = formater.format(lastRunDate);
		}
		
		String nextRun = "";
		ScheduleRunLog nextRunObj = schedule.getNextRun();
		if(nextRunObj != null){
			Date nextRunDate = nextRunObj.getRunningTime();
			nextRun = formater.format(nextRunDate);
		}
		
		// Added by Huy, first run and number of runs
		String firstRun = "";
		if(prevRuns != null && prevRuns.size() > 0){
			ScheduleRunLog firstRunLog = schedule.getPrevRuns().get(0);
			Date firstRunDate = firstRunLog.getRunningTime();
			firstRun = formater.format(firstRunDate);
		}
		int numRuns = 0;
		if(prevRuns != null){
			numRuns = prevRuns.size();
		}

		String notes = schedule.getNotes();

		if(notes != null){
			notes = formatString(request, notes);
		} else {
			notes = "";
		}
		
		//schedule.getNextRun()
	//**********************************************
%>
	schedules["<%=schedule.getId() %>"] = new Array();
	schedules["<%=schedule.getId() %>"]["id"] = "<%=schedule.getId() %>";
	schedules["<%=schedule.getId() %>"]["status"] = "<%=schedule.getStatus().getValue() %>";

	schedules["<%=schedule.getId() %>"]["title"] = "<%=formatString(request, schedule.getTitle()) %>";
	schedules["<%=schedule.getId() %>"]["cronExpr"] = "<%=schedule.getCronExpr() %>";
	schedules["<%=schedule.getId() %>"]["runLevel"] = "<%=schedule.getRunLevel() %>";
	schedules["<%=schedule.getId() %>"]["outputFormat"] = "<%=schedule.getOutputFormat() %>";
	schedules["<%=schedule.getId() %>"]["emailFrom"] = "<%=schedule.getEmailFrom() %>";
	schedules["<%=schedule.getId() %>"]["lastRun"] = "<%= lastRun%>";
	schedules["<%=schedule.getId() %>"]["nextRun"] = "<%= nextRun%>";
	schedules["<%=schedule.getId() %>"]["firstRun"] = "<%= firstRun%>";
	schedules["<%=schedule.getId() %>"]["numRuns"] = <%= numRuns%>;	
	schedules["<%=schedule.getId() %>"]["recipients"] = <%=sbReceivers.toString() %>;
	schedules["<%=schedule.getId() %>"]["notes"] = "<%=notes %>";
<%	} %>					

	function isContained(array, item){
		var answer = false;
		for(var i = 0; (i < array.length) && (!answer); i++){
			answer = (array[i] == item);
		}
		return answer;
	}
	function setSingleSelectValue(select, value){
		var options = select.options;
		for(var i = 0; i < options.length; i++){
			options[i].selected = (options[i].value == value);
		}
	}
	function setMultiSelectValue(select, values){
		var options = select.options;
		for(var i = 0; i < options.length; i++){
			options[i].selected = isContained(values, options[i].value);
		}
	}
	function setFormValues(schedule){
		var form = document.getElementById("schedule");
		//Set text inputs
		form.elements["id"].value = schedule["id"];
		form.elements["status"].value = schedule["status"];
		form.elements["title"].value = schedule["title"];
		form.elements["cronExpr"].value = schedule["cronExpr"];
		form.elements["notes"].value = schedule["notes"];
		form.elements["emailFrom"].value = schedule["emailFrom"];

		//Set single select input
		setSingleSelectValue(form.elements["outputFormat"], schedule["outputFormat"]);
		setSingleSelectValue(form.elements["runLevel"], schedule["runLevel"]);

		//Reset multi select input for source & dest selects
		buttonAllLeft.onclick();

		//Set multi select input for source
		resetSelectBox(form.elements["selRecipientCandidates"]);
		resetSelectBox(form.elements["recipients"]);
		setMultiSelectValue(form.elements["selRecipientCandidates"],schedule["recipients"]);

		//Move selected options to dest select
		buttonRight.onclick();
		//onRunLevelChanged(schedule["runLevel"]);
		return form;
	}

	function addSchedule(){
		//var titleDiv = document.getElementById("editTitle");
		//titleDiv.innerHTML = "Add new schedule";
		
		var form = document.getElementById("schedule");
		form.elements["id"].value = "-1";
		form.elements["status"].value = "<%= Schedule.Status.STOP.getValue() %>";
		form.elements["title"].value = "";
		form.elements["cronExpr"].value = "0 0 0 * * ? *";
		form.elements["notes"].value = "";
		form.elements["emailFrom"].value = "<%=SchedulingAppImpl.IT_SUPPORT_MAIL_BOX%>";

		//Reset single select input
		setSingleSelectValue(form.elements["outputFormat"], "XLS");
		setSingleSelectValue(form.elements["runLevel"], "50");

		//form.elements["recipients"].value = "";
		buttonAllLeft.onclick();
		//onRunLevelChanged("50");

		showScheduleDialog('lbAddSchedule', 280); 
		return false;
	}
	function editSchedule(schedule){
		//var titleDiv = document.getElementById("editTitle");
		//titleDiv.innerHTML = "Schedule " + schedule["title"];
		
		setFormValues(schedule);
		showScheduleDialog('lbAddSchedule', 280);
		return false;
	}
	function encodeHtml(text) {
		var textneu = text.replace(/&/g,"&amp;");
		textneu = textneu.replace(/</g,"&lt;");
		textneu = textneu.replace(/>/g,"&gt;");
		//textneu = textneu.replace("\"","\\\"");
		//textneu = textneu.replace(/\r\n/,"<br>");
		//textneu = textneu.replace(/\n/,"<br>");
		//textneu = textneu.replace(/\r/,"<br>");
		return(textneu);
	}
	function viewScheduleDetails(schedule){
		var titleDiv = document.getElementById("infoTitle");
		titleDiv.innerHTML = encodeHtml(schedule["title"]);

		//Create receiver table
		var tbl=document.createElement('table');
		tbl.className="publishtable";
		tbl.style.width='100%';
		tbl.style.tableLayout='fixed';
		tbl.cellpadding='0';
		tbl.cellspacing='1';
		var tbody=document.createElement('tbody');
		tbl.appendChild(tbody);
		for(var i=0;i<schedule["recipients"].length;i++){
			var td=document.createElement('td');
			td.style.textAlign = 'left';
			td.appendChild(document.createTextNode(schedule["recipients"][i]))
			
			var tr=document.createElement('tr');
			tr.appendChild(td);
			tbody.appendChild(tr);
		}

		//Add table to scrollable wrapper in DOM
		var div = document.getElementById('scrollWrapper');
		while(div.firstChild) {
			div.removeChild(div.firstChild);
		}
		div.appendChild(tbl);

		//Set value for static elements
		var lastRun = document.getElementById('lastRun');
		while(lastRun.firstChild) {
			lastRun.removeChild(lastRun.firstChild);
		}
		lastRun.appendChild(document.createTextNode("Last run: " + schedule["lastRun"]));
		var nextRun = document.getElementById('nextRun');
		while(nextRun.firstChild) {
			nextRun.removeChild(nextRun.firstChild);
		}
		nextRun.appendChild(document.createTextNode("Next run: " + schedule["nextRun"]));

		// Added by Huy to include first run and number of runs
		var firstRun = document.getElementById('firstRun');		
		if(firstRun) {
			firstRun.innerHTML = "First run: " + schedule["firstRun"];
		}

		var numRuns = document.getElementById('numRuns');
		while(numRuns &&  numRuns.firstChild) {
			numRuns.removeChild(numRuns.firstChild);
		}
		numRuns.appendChild(document.createTextNode("Number of runs: " + schedule["numRuns"]));			
		
		var btnRun = document.getElementById('btnRunSchedule');
		btnRun.scheduleId = schedule["id"];
		btnRun.onclick = runSchedule;
		
		showScheduleDialog('dialogSchedule', 280);
		return false;
	}
	function deleteSchedule(schedule){
		var ok = confirm("Do you really want to delete this schedule?");
		if(ok){
			schedule["status"] = "<%= Schedule.Status.DELETED.getValue() %>";
			var form = setFormValues(schedule);
			multipleSelectOnSubmit();
			form.submit();
		}
	}
	function runSchedule(){
		var ok = confirm("Are you sure you want to run the group of reports now? Please note that this can use a lot of resources and take a lot of time");
		if(ok){
			window.open('/content/RunSchedule?scheduleId=' + this.scheduleId, 'Report_Complete');
		}
		return false;
	}
	function validateCronExpression(cronString){
		try{
			if(!cronString || cronString == ''){
				throw ("Cron expression is missing");
			}
			var cron = new CronJob(cronString, function(){});
			var count = 0;
			for(var i in cron.cronTime.second){
				if(i){
					count++;
				}
			}
			if(count > 1){
				throw ("This schedule run too often.");
			}

			count = 0;
			for(var i in cron.cronTime.minute){
				if(i){
					count++;
				}
			}
			if(count > 1){
				throw ("This schedule run too often.");
			}
			
			count = 0;
			for(var i in cron.cronTime.hour){
				if(i){
					count++;
				}
			}
			if(count > 1){
				return confirm("Do you really want this schedule to run " + count + " times a day?");
			}
		}
		catch(err){
			alert(err);
			return false;
		}
		return true;
	}
	function onSubmitSchedule(){
		var form = document.getElementById("schedule");
		//Set text inputs
		if (form.submitted) {
			return false; 
		}

		if(!validateCronExpression(form.elements["cronExpr"].value)){
			return false;
		}
		if(!validateEmailAddress("emailFrom")){
			return false;
		}
		multipleSelectOnSubmit();
		//form.elements["submitted"].value = "true";
		form.submitted = true;
		return true;
	}
	function onRunLevelChanged(siValue){
		siValue = 0 + siValue;
		var selectBox1 = document.getElementById("selRecipientCandidates");		
		for(var i = 0; i < selectBox1.options.length; i++){
			var si = 0 + selectBox1.options[i].id;
			si = si%100;
			if(si == 0){
				si = 100;
			}
			if(si < siValue){
				selectBox1.options[i].disabled = true;
				selectBox1.options[i].selected = false;
			} else{
				selectBox1.options[i].disabled = false;
			}
		} 		
		
		var selectBox2 = document.getElementById("recipients");
		for(var i = 0; i < selectBox2.options.length; i++){
			var si = 0 + selectBox2.options[i].id;
			si = si%100;
			if(si == 0){
				si = 100;
			}
			if(si < siValue){
				selectBox2.options[i].disabled = true;
				selectBox2.options[i].selected = false;
			} else{
				selectBox2.options[i].disabled = false;
			}
		} 

		if( BrowserDetect.browser == "Chrome") {
			var currentFocus = document.activeElement;
			selectBox1.focus();
			currentFocus.focus();
		}
		
		if( BrowserDetect.browser == "Explorer" && (BrowserDetect.version == "6" || BrowserDetect.version == "7")) {
			disableOptionsIE6();
		}	
	}
	function resetSelectBox(selectBox){
		for(var i = 0; i < selectBox.options.length; i++){
			selectBox.options[i].disabled = false;
			selectBox.options[i].selected = false;
		} 

		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) {
			disableOptionsIE6();
		}		
	}
	function validateEmailAddress(emailEleId) {
		var email = document.getElementById(emailEleId);
		if(!email.value){ //Allow empty
			return true;
		}
		if(email.value == ""){  //Allow empty
			return true;
		}
		//var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
		var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
		if (!filter.test(email.value)) {
			alert('Please provide a valid email address');
			email.focus();
			return false;
		}
		return true;
	}
	function updateLastRun(scheduleId, lastRun, numRuns){
		//alert(scheduleId + " " + lastRun);
		if (scheduleId && lastRun)
		{
			var dt = new Date();
			dt.setTime(lastRun);
			var dtstring = pad2(dt.getDate())
			    + '/' + pad2(dt.getMonth()+1)
			    + '/' + dt.getFullYear()
			    + ' ' + pad2(dt.getHours())
			    + ':' + pad2(dt.getMinutes())
			    + ':' + pad2(dt.getSeconds());
			    
			schedules[scheduleId]["lastRun"] = dtstring;	
			if (numRuns && numRuns.length > 0)
			{		
				schedules[scheduleId]["numRuns"] = parseInt(numRuns);
			}
			var btnRun = document.getElementById('btnRunSchedule');
			if(btnRun.scheduleId == scheduleId){
				var lastRun = document.getElementById('lastRun');
				if (lastRun)
				{
					lastRun.innerHTML = "Last run: " + schedules[scheduleId]["lastRun"];
				}				

				// if this is the first time
				var firstRun = document.getElementById('firstRun');
				if (schedules[scheduleId]["numRuns"] == 1 && firstRun)
				{
					schedules[scheduleId]["firstRun"] = dtstring;
					firstRun.innerHTML = "First run: " + schedules[scheduleId]["firstRun"];
				}				
			}
	
			var td = document.getElementById('td' + scheduleId);
			td.innerHTML = "<a href=\"/content/pl/sql_report/download_report.jsp?scheduleId=" + scheduleId + "\">" + encodeHtml(schedules[scheduleId]["title"]) + "</a>"
			//alert(scheduleId + " " + lastRun + " done");
		}
	}
	function onCheckChange(target){
		 if(target.checked){
			 target.parentNode.style.backgroundColor = "#B0B0B0";
		 } else{
			 target.parentNode.style.backgroundColor = "white";
		 }
	}
	function setFrequence(idx){
		//alert(idx);
		 for(var i = 0; i < cronEditors.length; i++){
			 cronEditors[i].hide();
		 }
		 cronEditors[idx].show();
		 curIdx = idx;
	}
	function setCron(){
		var txtCron = document.getElementById('cronExpr');
		txtCron.value = cronEditors[curIdx].buildCronExpression();
	}				
</script>
<%	
	String errors = (String) request.getAttribute("errors");
	System.out.println(errors);
	if(errors != null && errors.length() > 0){ 
		String currentAction = (String) request.getAttribute("currentAction");
		if(currentAction != null){
			//currentAction = currentAction.trim();
			if(currentAction.equals("add") || currentAction.equals("edit")){
				Schedule schedule = (Schedule) request.getAttribute("editingSchedule");
		 		
				StringBuilder sbReceivers = new StringBuilder("[");
				List<User> receivers = schedule.getReceivers();
				if(receivers != null){
					for(int j = 0; j < receivers.size(); j++){
						sbReceivers.append("\"").append(receivers.get(j).getUserName()).append("\",");
					}
				}
				if(sbReceivers.length() > 1){
					sbReceivers.deleteCharAt(sbReceivers.length() - 1);
				}
				sbReceivers.append("]");

				String notes = schedule.getNotes();
				if(notes != null){
					notes = formatString(request, notes);
				} else {
					notes = "";
				}
 %>
<script type="text/javascript"> 
	var editingSchedule = new Array();
	editingSchedule["id"] = "<%=schedule.getId() %>";
	editingSchedule["status"] = "<%=schedule.getStatus().getValue() %>";	
	editingSchedule["title"] = "<%=formatString(request, schedule.getTitle()) %>";k
	editingSchedule["cronExpr"] = "<%=schedule.getCronExpr() == null ? "0 0 0 * * ? *" : schedule.getCronExpr() %>";
	editingSchedule["runLevel"] = "<%=schedule.getRunLevel() %>";
	editingSchedule["outputFormat"] = "<%=schedule.getOutputFormat() %>";
	editingSchedule["emailFrom"] = "<%=schedule.getEmailFrom() == null ? "" : schedule.getEmailFrom() %>";
	editingSchedule["lastRun"] = "";
	editingSchedule["nextRun"] = "";
	editingSchedule["firstRun"] = "";
	editingSchedule["numRuns"] = 0;	
	editingSchedule["recipients"] = <%=sbReceivers.toString() %>;
	editingSchedule["notes"] = "<%=notes %>";	
	editSchedule(editingSchedule);
</script>
<%			}
		}
 %>
<script type="text/javascript"> alert("<%= errors %>");</script>
<%	
 	}
 %>
<cms:include property="template" element="foot" />
