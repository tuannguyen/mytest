<%@ page pageEncoding="UTF-8" %>
<%@page import="org.opencms.file.*"%>
<%@page import="org.opencms.jsp.*"%>
<%@page import="java.util.*"%>
<%@page import="com.bp.pensionline.sqlreport.dao.ScheduleDao"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="com.bp.pensionline.sqlreport.dto.db.SQLReportScheduledInboxItemDTO"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@page import="java.text.SimpleDateFormat"%>
<cms:include property="template" element="head" />
<h1>Scheduled inbox</h1>

<p>Your scheduled reports for this report group are listed bellow</p>

<table width='100%' cellspacing='0' cellpadding='0' border="1">
	<tr>
		<th class='label'>URL</th>
		<th class='label'>Created</th>
		<th class='label'>&nbsp;</th>
	</tr>
<%
	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request, response);
	String groupId = request.getParameter("groupId");
	CmsUser currentUser = SystemAccount.getCurrentUser(request);
	if (currentUser != null)
	{
		String userId = currentUser.getId().toString();
		Vector inboxItems = ScheduleDao.getAllScheduledInboxItems(userId, groupId);
		for (int i=0; i<inboxItems.size(); i ++)
		{
			SQLReportScheduledInboxItemDTO inboxItem = (SQLReportScheduledInboxItemDTO)inboxItems.elementAt(i);
			if (inboxItem != null)
			{
				String itemId = inboxItem.getId();
				String url = inboxItem.getUrl();
				long creationTime = inboxItem.getCreationTime();
				SimpleDateFormat dateFormat = new SimpleDateFormat("d-MMM-yy hh:mm aaa");
				String creationTimeStr = dateFormat.format(new Date(creationTime));
				if (url != null && url.indexOf("/sites/default/") > -1)
				{
					url = url.substring("/sites/default/".length());
				}
%>
	<tr>
		<td>
			<a title='Click here to view details' 
				href='<%="/content/pl/" + url%>'><%=url%>
			</a>
		</td>
		<td>
			<%=creationTimeStr%>
		</td>		
		<td>
			<a title='Click here to delete this item' 
				href='#' onclick="removeScheduledInboxItem('<%=itemId%>'); return false;">[R]
			</a>
		</td>		
	</tr>
<%
			}
		}
%>
</table>
<%
	}
%>
<cms:include property="template" element="foot" />
