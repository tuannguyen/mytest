<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<cms:include property="template" element="head" />
<cms:editable />

<cms:include page="./events_header.html" element="bptitle" />
<cms:include page="./events_header.html" element="bpblank" />

<div class="content_list">

<cms:contentload collector="allInFolderNavPos" param="/RSS/eventsRss/rss_%(number).html|bp_pensionline_rssfeed" editable="true">

<c:set var="localVar" >
    <cms:contentshow element="new_edited" />
</c:set>
<c:if test="${localVar ==true}">
<div class="element" style="background-color: #ffc ">
<h2><cms:contentshow element="title" /></h2>
<cms:contentshow element="description" />
</div>
</c:if>
<c:if test="${localVar !=true}">
<div class="element" >
<h2><cms:contentshow element="title" /></h2>
<cms:contentshow element="description" />
</div>
</c:if>


</cms:contentload>

</div>

<cms:include property="template" element="foot" />
