<%@ page pageEncoding="UTF-8" %><%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.file.CmsUser"%>
<%@ page import="com.bp.pensionline.handler.*"%>
<%@ page import="com.bp.pensionline.util.*"%>
<%@ page import="com.bp.pensionline.constants.*"%>
<%@ page import="java.util.*"%>
<%@page import="com.bp.pensionline.dao.RecordDao"%>
<%@page import="com.bp.pensionline.dao.CompanySchemeDao"%>
<%@page import="com.bp.pensionline.dao.MemberDao"%>
<%@page import="com.bp.pensionline.factory.DataAccess"%>
<%@page import="com.bp.pensionline.factory.CalcProducerFactory"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>

<%


	CmsJspLoginBean cms = new CmsJspLoginBean(pageContext, request,	response);
	//CmsUser cmsUser = cms.getRequestContext().currentUser();

	CmsUser cmsUser = SystemAccount.getCurrentUser(request);

	
	
	if (cmsUser!=null){
		
		String userName = String.valueOf(cmsUser.getName());
		List records = (List) cmsUser.getAdditionalInfo(Environment.RECORD_LIST);


	if (null != records && records.size() > 1) {
			String currentGroup = String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_CURRENT_GROUP));
			String sessionBgroup = String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_BGROUP));
			String sessionRefNo = String.valueOf(cmsUser.getAdditionalInfo(Environment.MEMBER_REFNO));
%>
<h2>period of service</h2>
<form action="../_login_periodofservice.jsp" method="post"
	name="frmPosOfService"><input type="hidden" name="tmpBGROUP"
	id="tmpBGROUP" value=""> <input type="hidden" name="tmpCREFNO"
	id="tmpCREFNO" value=""> <input type="hidden" name="tmpSTATUS"
	id="tmpSTATUS" value=""> <input type="hidden"
	name="tmpSCHEMERAW" id="tmpSCHEMERAW" value=""> <input
	type="hidden" name="action" value="posted"> <%
 		for (int i = 0; i < records.size(); i++) {

 		RecordDao record = (RecordDao) records.get(i);

 		String tmpSchemeRaw = String.valueOf(record
 				.get(Environment.MEMBER_SCHEME_RAW));

 		StringBuffer tmpGroup = new StringBuffer();
 		tmpGroup.append(record.getBgroup()).append("_").append(
 				record.getStatusRaw());

 		String checked = sessionBgroup.equals(record
 				.getBgroup())
 				&& sessionRefNo.equals(record.getRefno()) ? new String(
 				"checked=\"checked\"")
 				: new String(" ");

 		StringBuffer outText = new StringBuffer();
 		outText.append(record.getStatus()).append(" ").append(
 				record.getScheme()).append(" service from ")
 				.append(record.getPosStart());
 %> <br />
<input type="radio" name="pos" <%=checked %> onClick="submitForm('<%=record.getBgroup()%>','<%=record.getRefno()%>','<%=record.getStatusRaw() %>','<%=tmpSchemeRaw%>')">
<%=outText%> <%
 }
 %>
</form> <br />
<p>Please note that if you want to update something, for example, your contact details, 
only the information relevant to the period of service you are viewing will be updated.</p>
<%
	}
	}
	
%>
