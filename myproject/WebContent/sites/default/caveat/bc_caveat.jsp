<%@ page session="false" %>

<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>


<cms:include property="template" element="head" />
<cms:editable />

<div class="element">

<cms:contentload collector="allInFolderPriorityDateDesc" param="/caveat/BC/caveat_${number}.html|bp_pensionline_caveat" editable="true">

<div class="element">

<h3><cms:contentshow element="Title" /></h3>

<p>
<cms:contentshow element="Description" /></br>
</p>
</div>

</cms:contentload>


</div>

<cms:include property="template" element="foot" />