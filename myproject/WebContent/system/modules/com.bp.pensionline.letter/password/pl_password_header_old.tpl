<w:p/><w:p/><w:p/>
<w:tbl><w:tblPr><w:tblStyle w:val="TableGrid"/><w:tblW w:w="9288" w:type="dxa"/><w:tblLook w:val="01E0"/></w:tblPr>
	<w:tblGrid><w:gridCol w:w="5631"/><w:gridCol w:w="3657"/></w:tblGrid>
	
	<w:tr><w:trPr><w:trHeight w:val="960"/></w:trPr>
	<w:tc><w:tcPr><w:tcW w:w="5688" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="24"/></w:rPr></w:pPr></w:p>
	</w:tc>
	<w:tc><w:tcPr><w:tcW w:w="3600" w:type="dxa"/><w:vmerge w:val="restart"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="18"/><w:sz-cs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="18"/><w:sz-cs w:val="18"/></w:rPr>
			<w:t>BP UK Pensions &amp; Benefits</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><st1:Street w:st="on"><st1:address w:st="on"><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Chertsey Road</w:t></w:r></st1:address></st1:Street>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Sunbury on </w:t></w:r><st1:place w:st="on"><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/>
			<w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr><w:t>Thames</w:t></w:r></st1:place>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Middlesex TW16 7LN</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><st1:country-region w:st="on"><st1:place w:st="on"><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>United Kingdom</w:t></w:r></st1:place></st1:country-region>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr></w:p><w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Phone: 0845 602 1063</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>If phoning from abroad: +44 1932 767 730</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Fax: 01932 763 630</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:spacing w:line="360" w:line-rule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:b/><w:sz w:val="14"/><w:sz-cs w:val="14"/></w:rPr>
			<w:t>Email: pensions@bp.com</w:t></w:r>
		</w:p>
	</w:tc>
	</w:tr>
	
	<w:tr><w:trPr><w:trHeight w:val="2098"/></w:trPr>
	<w:tc><w:tcPr><w:tcW w:w="5688" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:sz-cs w:val="24"/></w:rPr></w:pPr>
			<w:r><w:rPr><w:sz-cs w:val="24"/></w:rPr><w:t>[[Name]]</w:t></w:r>
		</w:p>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="22"/></w:rPr></w:pPr>
			<w:r><w:rPr><w:sz-cs w:val="24"/></w:rPr><w:t>[[Address]]</w:t></w:r>
		</w:p>
	</w:tc>
	<w:tc><w:tcPr><w:tcW w:w="3600" w:type="dxa"/><w:vmerge/><w:tcBorders><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="22"/></w:rPr></w:pPr>
		</w:p>
	</w:tc>
	</w:tr>
	
	<w:tr><w:trPr><w:trHeight w:val="426"/></w:trPr>
	<w:tc><w:tcPr><w:tcW w:w="5688" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:sz-cs w:val="24"/></w:rPr>
			<w:t>Membership number: [[Refno]]</w:t></w:r>
		</w:p>
	</w:tc>
	<w:tc><w:tcPr><w:tcW w:w="3686" w:type="dxa"/><w:tcBorders><w:top w:val="nil"/><w:left w:val="nil"/><w:bottom w:val="nil"/><w:right w:val="nil"/></w:tcBorders></w:tcPr>
		<w:p><w:pPr><w:pStyle w:val="Footer"/><w:rPr><w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/><wx:font wx:val="Arial"/><w:sz-cs w:val="22"/></w:rPr></w:pPr>
			<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:fldChar w:fldCharType="begin"/></w:r>
			<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:instrText> DATE  \@ "dd MMMM yyyy"  \* MERGEFORMAT </w:instrText></w:r>
			<w:r><w:rPr><w:noProof/><w:sz-cs w:val="22"/></w:rPr><w:fldChar w:fldCharType="separate"/></w:r>
			<w:r><w:rPr><w:noProof/><w:sz-cs w:val="22"/></w:rPr><w:t>21 November 2007</w:t></w:r><w:r><w:rPr><w:noProof/><w:sz-cs w:val="22"/></w:rPr><w:fldChar w:fldCharType="end"/>
			</w:r>
		</w:p>
	</w:tc>
	</w:tr>

</w:tbl>