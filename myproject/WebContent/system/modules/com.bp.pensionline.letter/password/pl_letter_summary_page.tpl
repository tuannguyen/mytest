<w:p>
	<w:pPr><w:keepNext/><w:spacing w:before="240" w:after="240"/></w:pPr>
	<w:r>
		<w:rPr><w:sz w:val="48"/></w:rPr>
		<w:t>PensionLine letter generation summary report</w:t>
	</w:r>
</w:p>
<w:p>
	<w:pPr><w:listPr><w:ilvl w:val="0"/><w:ilfo w:val="1"/></w:listPr></w:pPr>
	<w:r><w:t>Total welcome pack requests: TotalWelcomePack</w:t></w:r>
</w:p>
<w:p>
	<w:pPr>
		<w:listPr><w:ilvl w:val="1"/><w:ilfo w:val="2"/></w:listPr>
		<w:tabs><w:tab w:val="clear" w:pos="360"/><w:tab w:val="list" w:pos="540"/></w:tabs>
		<w:ind w:left="540" w:hanging="540"/>
	</w:pPr>
	<w:r><w:t>- </w:t></w:r>
	<w:r><w:t>Success: SuccessWelcomePack</w:t></w:r>
</w:p>
<w:p>
	<w:pPr>
		<w:listPr><w:ilvl w:val="1"/><w:ilfo w:val="2"/></w:listPr>
		<w:tabs><w:tab w:val="clear" w:pos="360"/><w:tab w:val="list" w:pos="540"/></w:tabs>
		<w:ind w:left="540" w:hanging="540"/>
	</w:pPr>
	<w:r><w:t>- </w:t></w:r>
	<w:r><w:t>Failure: FailureWelcomePack</w:t></w:r>
</w:p>
<w:p>
	<w:pPr><w:listPr><w:ilvl w:val="0"/><w:ilfo w:val="1"/></w:listPr></w:pPr>
	<w:r><w:t>Total password requests: TotalPasswordReset</w:t></w:r>
</w:p>
<w:p>
	<w:pPr>
		<w:listPr><w:ilvl w:val="1"/><w:ilfo w:val="2"/></w:listPr>
		<w:tabs><w:tab w:val="clear" w:pos="360"/><w:tab w:val="list" w:pos="540"/></w:tabs>
		<w:ind w:left="540" w:hanging="540"/>
	</w:pPr>
	<w:r><w:t>- </w:t></w:r>
	<w:r><w:t>Success: SuccessPasswordReset</w:t></w:r>
</w:p>
<w:p>
	<w:pPr>
		<w:listPr><w:ilvl w:val="1"/><w:ilfo w:val="2"/></w:listPr>
		<w:tabs><w:tab w:val="clear" w:pos="360"/><w:tab w:val="list" w:pos="540"/></w:tabs>
		<w:ind w:left="540" w:hanging="540"/>
	</w:pPr>
	<w:r><w:t>- </w:t></w:r>
	<w:r><w:t>Failure: FailurePasswordReset</w:t></w:r>
</w:p>
<w:p/>
<w:p><w:r><w:t>____________________________________________</w:t></w:r></w:p>
<w:p>
	<w:pPr><w:rPr><w:sz w:val="40"/><w:sz-cs w:val="40"/></w:rPr></w:pPr>
	<w:r><w:rPr><w:sz w:val="40"/><w:sz-cs w:val="40"/></w:rPr><w:t>Error Log </w:t></w:r>
</w:p>
<w:p/>