<w:tbl>
	<w:tblPr>
		<w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="margin" w:tblpX="144" w:tblpY="52"/>
		<w:tblW w:w="6817" w:type="dxa"/>
		<w:tblBorders>
			<w:top w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
			<w:left w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
			<w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
			<w:right w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
			<w:insideH w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
			<w:insideV w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/>
		</w:tblBorders>
		<w:tblLook w:val="01E0"/>
	</w:tblPr>
	<w:tblGrid>
		<w:gridCol w:w="2988"/>
		<w:gridCol w:w="3829"/>
	</w:tblGrid>
	<w:tr>
		<w:trPr><w:trHeight w:val="650"/></w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2988" w:type="dxa"/>
				<w:tcBorders><w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/></w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="CCCCCC"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:jc w:val="left"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>Name</w:t></w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="3829" w:type="dxa"/>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:pStyle w:val="Footer"/>
					<w:framePr w:wrap="auto" w:hanchor="text" w:x="144"/>
					<w:rPr><w:sz-cs w:val="24"/></w:rPr>
				</w:pPr>
				<w:r><w:t>[[FirstName]] [[Surname]]</w:t></w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr>
		<w:trPr><w:trHeight w:val="650"/></w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2988" w:type="dxa"/>
				<w:tcBorders><w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/></w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="CCCCCC"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:jc w:val="left"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>Signature</w:t></w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr><w:tcW w:w="3829" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr>
		<w:trPr><w:trHeight w:h-rule="exact" w:val="650"/></w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2988" w:type="dxa"/>
				<w:tcBorders><w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/></w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="CCCCCC"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:jc w:val="left"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>Scheme name</w:t></w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="3829" w:type="dxa"/>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>[[SchemeName]]</w:t></w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr>
		<w:trPr><w:trHeight w:val="650"/></w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2988" w:type="dxa"/>
				<w:tcBorders><w:bottom w:val="single" w:sz="12" wx:bdrwidth="30" w:space="0" w:color="auto"/></w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="CCCCCC"/>
				<w:vAlign w:val="center"/>
			</w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/><w:jc w:val="left"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>Pension reference number</w:t></w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr><w:tcW w:w="3829" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>[[Refno]]</w:t></w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr>
		<w:trPr><w:trHeight w:val="650"/></w:trPr>
		<w:tc>
			<w:tcPr><w:tcW w:w="2988" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="CCCCCC"/><w:vAlign w:val="center"/></w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/><w:jc w:val="left"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
				<w:r><w:rPr><w:sz-cs w:val="22"/></w:rPr><w:t>National insurance number</w:t></w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr><w:tcW w:w="3829" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr>
			<w:p>
				<w:pPr>
					<w:framePr w:hspace="180" w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x="144" w:y="52"/>
					<w:spacing w:before="60" w:after="60"/>
					<w:rPr><w:sz-cs w:val="22"/></w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
	</w:tr>
</w:tbl>
<w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/><w:p/>