select 
  b.bgroup as "Bgroup"
  ,b.refno as "Refno"
  ,b.BD08X as "Nino"
  ,nvl(b.BD04X, 'No information available') as "EmployeeNumber"
  ,b.BD19A as "MembershipStatusRaw"
  ,sd.SD02X as "SchemeName"
  ,initcap(b.BD07A || ' ' || b.BD05A || ' ' || b.BD38A) as "Name"
  ,initcap(b.BD07A || ' ' || b.BD38A) as "FirstName"
  ,replace(
    trim(trailing chr(13) from 
      replace(
        replace(initcap(address1) || chr(13)
          || initcap(address2) || chr(13)
          || initcap(address3) || chr(13)
          || initcap(address4) || chr(13)
          || initcap(address5) || chr(13)
          || postcode || chr(13)
          , chr(13)||chr(13)||chr(13), chr(13)), 
        chr(13)||chr(13), chr(13))),     
      chr(13), '&lt;br/&gt;') as "Address"  
from 
  basic b
  ,SCHEME_DETAIL sd
  ,CATEGORY_DETAIL cd
  ,PS_ADDRESSDETAILS ad
  ,PS_ADDRESS psad
  ,MP_ADDCOMM mpac 
  ,sd_domain_list sddl 
where 
  b.bgroup= :bgroup
  and b.refno= :refno
  and cd.bgroup= b.bgroup
  and cd.ca26x= b.ca26x
  and psad.bgroup= b.bgroup
  and psad.refno= b.refno
  and psad.addcode='GENERAL'
  and psad.endd is null
  and ad.bgroup = b.bgroup
  and ad.addno= psad.ADDNO
  and sd.bgroup= b.bgroup
  and sd.sd01x= b.sd01x
  and sddl.domain = 'MAR01'
  and sddl.bgroup = b.bgroup
  and sddl.dol_listval = b.BD10A
  and mpac.bgroup (+) = b.bgroup  
  and mpac.refno  (+) = b.refno  
  and mpac.mpam01x (+) = 'GENERAL'