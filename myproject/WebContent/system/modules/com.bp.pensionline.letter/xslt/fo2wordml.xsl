<?xml version="1.0" encoding="UTF-8"?>

<!--

fo2wordml.xsl - XSLT stylsheet for converting XSL-FO into WordML format

Copyright (c) 2004, Jiri Pachman <jirka.pachman@tele2.cz>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

The names of contributors may not be used to endorse or promote
products derived from this software without specific prior written
permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<!-- Styl pro převod formátovacích objektů do formátu WordML. Vytvořil Jiří Pachman. E-mail jiri.pachman@tele2.cz. -->

<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
     xmlns:fo='http://www.w3.org/1999/XSL/Format'
     xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
     xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
     xmlns:w10="urn:schemas-microsoft-com:office:word"
     xmlns:v="urn:schemas-microsoft-com:vml">
<xsl:strip-space elements="*"/>
<xsl:output method="xml" indent="no"/>

<xsl:template match="/"> <!-- Vytvoří základní strukturu dokumentu -->
  <w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
                  xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                  xmlns:w10="urn:schemas-microsoft-com:office:word"
                  xmlns:v="urn:schemas-microsoft-com:vml" xml:space="preserve">
  <w:lists>
   <xsl:apply-templates select="//fo:list-block[not(ancestor::fo:list-block)]" mode="def"/>
   <xsl:apply-templates select="//fo:list-block[not(ancestor::fo:list-block)]" mode="odk"/>
  </w:lists>
  <w:styles>
   <xsl:for-each select="//fo:marker/@marker-class-name">
    <xsl:variable name="mcn" select="."/>
    <xsl:if test="not(../preceding::fo:marker/@marker-class-name[.=$mcn])">
     <w:style w:type="character" w:styleId="{$mcn}">
      <w:name w:val="{$mcn}"/>
      <w:rPr>
       <w:sz w:val="0"/>
       <xsl:call-template name="b"/>
      </w:rPr>
     </w:style>
    </xsl:if>
   </xsl:for-each>
  </w:styles>
  <w:docPr>
   <w:view w:val="print"/>
   <w:zoom w:percent="100"/>
  </w:docPr>
  <w:body>
   <xsl:apply-templates select="fo:root/fo:page-sequence/fo:flow/*"/>
   <xsl:apply-templates select="fo:root/fo:page-sequence[last()]"/>
  </w:body>
  </w:wordDocument>
</xsl:template>

<xsl:template match="fo:list-block" mode="odk">
 <w:list w:ilfo="{count(preceding::fo:list-block)+1}">
  <w:ilst w:val="{count(preceding::fo:list-block)+1}"/>
 </w:list>
</xsl:template>

<xsl:template match="fo:list-block" mode="def">
 <w:listDef w:listDefId="{count(preceding::fo:list-block)+1}">
  <xsl:apply-templates select="fo:list-item[1]" mode="def"/>
 </w:listDef>
</xsl:template>

<xsl:template match="fo:list-item" mode="def">
 <xsl:variable name="si" select="fo:list-item-label[1]/*/ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
 <xsl:variable name="pd">
  <xsl:if test="ancestor::node()/@provisional-distance-between-starts">
   <xsl:value-of select="ancestor::node()[@provisional-distance-between-starts][1]/@provisional-distance-between-starts"/>
  </xsl:if>
  <xsl:if test="not(ancestor::node()/@provisional-distance-between-starts)">
   <xsl:value-of select="'24pt'"/>
  </xsl:if>
 </xsl:variable>
 <xsl:variable name="sitw">
  <xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="$si"/></xsl:call-template>
 </xsl:variable>
 <xsl:variable name="pdtw">
  <xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="$pd"/></xsl:call-template>
 </xsl:variable>
 <w:lvl w:ilvl="{count(ancestor::fo:list-block)-1}">
  <xsl:if test="count(fo:list-item-label)=1 or fo:list-item-label[1]/fo:block/text()=fo:list-item-label[2]/fo:block/text()">
   <w:lvlText w:val="{fo:list-item-label/fo:block/text()}"/>
  </xsl:if>
  <w:pPr>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
   <w:tabs>
    <w:tab w:val="list" w:pos="{($sitw)+($pdtw)}"/>
   </w:tabs>
  </w:pPr>
  <w:rPr>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@color][1]/@color"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-family][1]/@font-family"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-size][1]/@font-size"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-weight][1]/@font-weight"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-style][1]/@font-style"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-stretch][1]/@font-stretch"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@font-variant][1]/@font-variant"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@text-transform][1]/@text-transform"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@text-decoration][1]/@text-decoration"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@text-shadow][1]/@text-shadow"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/ancestor-or-self::node()[@letter-spacing][1]/@letter-spacing"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/@vertical-align"/><xsl:apply-templates select="fo:list-item-label[1]/*/@background-color"/>
   <xsl:apply-templates select="fo:list-item-label[1]/*/@border-style" mode="inline"/>
  </w:rPr>
 </w:lvl>
 <xsl:apply-templates select="descendant::fo:list-item" mode="def"/>
</xsl:template>

<xsl:template name="b">
<w:color>
 <xsl:attribute name="w:val">
  <xsl:if test="../../@background-color">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../../@background-color"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../../@background-color)">
   <xsl:value-of select="'ffffff'"/> 
  </xsl:if>
 </xsl:attribute>
</w:color>
</xsl:template>

<xsl:template match="fo:static-content">
 <xsl:if test="@flow-name='xsl-region-before'">
  <w:hdr w:type="even"><xsl:apply-templates select="fo:block"/></w:hdr>
  <w:hdr w:type="odd"><xsl:apply-templates select="fo:block"/></w:hdr>
 </xsl:if>
 <xsl:if test="@flow-name='xsl-region-after'">
  <w:ftr w:type="even"><xsl:apply-templates select="fo:block"/></w:ftr>
  <w:ftr w:type="odd"><xsl:apply-templates select="fo:block"/></w:ftr>
 </xsl:if>
</xsl:template>

<xsl:template match="fo:page-sequence">
<xsl:variable name="master" select="@master-reference"/>
 <w:sectPr>
  <xsl:apply-templates select="../fo:layout-master-set/fo:simple-page-master[@master-name=$master]"/>
  <xsl:apply-templates select="fo:static-content"/>
  <xsl:apply-templates select="@initial-page-number"/>
 </w:sectPr>
</xsl:template>

<xsl:template match="@initial-page-number">
 <w:pgNumType>
  <xsl:if test="number(.)"><xsl:attribute name="w:start"><xsl:value-of select="."/></xsl:attribute></xsl:if>
 </w:pgNumType>
</xsl:template>

<xsl:template match="fo:simple-page-master">
 <w:pgSz>
  <xsl:apply-templates select="@page-height"/><xsl:apply-templates select="@page-width"/>
 </w:pgSz>
 <w:pgMar>
  <xsl:apply-templates select="@margin"/><xsl:apply-templates select="@margin-top"/><xsl:apply-templates select="@margin-bottom"/>
  <xsl:apply-templates select="@margin-left"/><xsl:apply-templates select="@margin-right"/>
 </w:pgMar> 
</xsl:template>

<xsl:template match="@page-height">
 <xsl:attribute name="w:h"><xsl:call-template name="prevodtwips"/></xsl:attribute>
</xsl:template>

<xsl:template match="@page-width">
 <xsl:attribute name="w:w"><xsl:call-template name="prevodtwips"/></xsl:attribute>
</xsl:template>

<xsl:template match="@margin">
 <xsl:variable name="hranice"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <xsl:variable name="zahlavi">
  <xsl:if test="../fo:region-before/@extent"><xsl:apply-templates select="../fo:region-before/@extent"/></xsl:if>
  <xsl:if test="not(../fo:region-before/@extent)"><xsl:value-of select="0"/></xsl:if>
 </xsl:variable>
 <xsl:variable name="zapati">
  <xsl:if test="../fo:region-after/@extent"><xsl:apply-templates select="../fo:region-after/@extent"/></xsl:if>
  <xsl:if test="not(../fo:region-after/@extent)"><xsl:value-of select="0"/></xsl:if>
 </xsl:variable>
 <xsl:attribute name="w:top"><xsl:value-of select="$hranice+$zahlavi"/></xsl:attribute>
 <xsl:attribute name="w:bottom"><xsl:value-of select="$hranice+$zapati"/></xsl:attribute>
 <xsl:attribute name="w:right"><xsl:value-of select="$hranice"/></xsl:attribute>
 <xsl:attribute name="w:left"><xsl:value-of select="$hranice"/></xsl:attribute>
 <xsl:attribute name="w:header"><xsl:value-of select="$hranice"/></xsl:attribute>
 <xsl:attribute name="w:footer"><xsl:value-of select="$hranice"/></xsl:attribute>
</xsl:template>

<xsl:template match="@extent">
 <xsl:call-template name="prevodtwips"/>
</xsl:template>

<xsl:template match="@margin-top">
 <xsl:variable name="hranice"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <xsl:variable name="zahlavi">
  <xsl:if test="../fo:region-before/@extent"><xsl:apply-templates select="../fo:region-before/@extent"/></xsl:if>
  <xsl:if test="not(../fo:region-before/@extent)"><xsl:value-of select="0"/></xsl:if>
 </xsl:variable>
 <xsl:attribute name="w:top"><xsl:value-of select="$hranice+$zahlavi"/></xsl:attribute>
 <xsl:attribute name="w:header"><xsl:value-of select="$hranice"/></xsl:attribute>
</xsl:template>

<xsl:template match="@margin-bottom">
 <xsl:variable name="hranice"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <xsl:variable name="zapati">
  <xsl:if test="../fo:region-after/@extent"><xsl:apply-templates select="../fo:region-after/@extent"/></xsl:if>
  <xsl:if test="not(../fo:region-after/@extent)"><xsl:value-of select="0"/></xsl:if>
 </xsl:variable>
 <xsl:attribute name="w:bottom"><xsl:value-of select="$hranice+$zapati"/></xsl:attribute>
 <xsl:attribute name="w:footer"><xsl:value-of select="$hranice"/></xsl:attribute>
</xsl:template>

<xsl:template match="@margin-left">
 <xsl:attribute name="w:left"><xsl:call-template name="prevodtwips"/></xsl:attribute>
</xsl:template>

<xsl:template match="@margin-right">
 <xsl:attribute name="w:right"><xsl:call-template name="prevodtwips"/></xsl:attribute>
</xsl:template>

<xsl:template match="fo:root/fo:page-sequence/*/fo:block|fo:block[parent::fo:table-cell|parent::fo:table-caption|parent::fo:list-item-body]"> <!-- Výskyt elementu fo:block jako potomka elementu fo:flow -->
 <xsl:if test="node()[1]=text() or fo:inline[1] or fo:page-number[1] or fo:leader[1] or fo:basic-link[1] or fo:page-number-citation[1] or fo:retrieve-marker[1] or fo:marker[1]"> <!-- Jestliže prvním potomkem je text nebo element fo:inline, potom vytvoříme odstavec -->
  <xsl:text disable-output-escaping="yes">&lt;w:p&gt;</xsl:text> 
   <w:pPr> <!-- Vlastnosti odstavce. U dědičných vlastností voláme výskyt vlastnosti u nejbližšího předchůdce, u ostatních u akt. elementu -->
    <xsl:variable name="akt" select="."/>
    <xsl:if test="(not(fo:marker) or not(fo:block)) and not(ancestor::fo:list-item-body)">
     <xsl:apply-templates select="ancestor-or-self::node()[@text-align][1]/@text-align"/>
     <xsl:apply-templates select="ancestor-or-self::node()[@text-indent][1]/@text-indent"/>
     <xsl:apply-templates select="ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
     <xsl:apply-templates select="ancestor-or-self::node()[@end-indent][1]/@end-indent"/>
     <xsl:if test="count(fo:block)=0"><xsl:apply-templates select="@space-after"/></xsl:if>
     <xsl:apply-templates select="ancestor-or-self::node()[@line-height][1]/@line-height"/>
     <xsl:apply-templates select="@background-color"/><xsl:apply-templates select="@border-top-style"/><xsl:apply-templates select="@border-before-style"/>
     <xsl:apply-templates select="@border-bottom-style"/><xsl:apply-templates select="@border-after-style"/>
     <xsl:apply-templates select="@border-left-style"/><xsl:apply-templates select="@border-start-style"/>
     <xsl:apply-templates select="@border-right-style"/><xsl:apply-templates select="@border-end-style"/><xsl:apply-templates select="@border-style"/>
     <xsl:apply-templates select="ancestor-or-self::node()[@orphans][1]/@orphans"/><xsl:apply-templates select="ancestor-or-self::node()[@widows][1]/@widows"/>
     <xsl:apply-templates select="ancestor-or-self::node()[@keep-together][1]/@keep-together"/>
     <xsl:apply-templates select="@keep-with-next"/><xsl:apply-templates select="following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
     <xsl:apply-templates select="following-sibling::node()[1]/descendant::node()[1][@keep-with-previous]/@keep-with-previous"/>
     <xsl:apply-templates select="ancestor::node()[not(child::text())]/following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
     <xsl:if test="not(following-sibling::node()) and not(child::fo:block or descendant::fo:table) and (ancestor::fo:flow) and (ancestor::fo:page-sequence/following-sibling::node())">
      <xsl:apply-templates select="ancestor::fo:page-sequence"/>
     </xsl:if>
     <xsl:apply-templates select="descendant::fo:leader" mode="blok"/>
     <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@background-color"/></xsl:if>
     <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@margin-left" mode="blok"/></xsl:if>
     <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@margin-right" mode="blok"/></xsl:if>
    </xsl:if>
    <xsl:choose>
     <xsl:when test="not(following-sibling::node())and (not(fo:marker) or not(name(node())='fo:block')) and not(ancestor::fo:table)"><xsl:apply-templates select="ancestor-or-self::node()[$akt=descendant::node()[last()]][@space-after][1]/@space-after"/></xsl:when>
     <xsl:when test="not(fo:marker) or not(fo:block)"><xsl:apply-templates select="@space-after"/></xsl:when>
    </xsl:choose>
    <xsl:apply-templates select="@space-before"/>
    <xsl:apply-templates select="@break-before"/><xsl:apply-templates select="preceding-sibling::node()[1][@break-after]/@break-after"/>
    <xsl:apply-templates select="preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
    <xsl:if test="ancestor::fo:list-item-body">
     <w:listPr>
      <w:ilvl w:val="{count(ancestor::fo:list-item-body)-1}"/>
      <w:ilfo w:val="{count(preceding::fo:list-block)+1}"/>
     </w:listPr>
    </xsl:if>
   </w:pPr>
 </xsl:if>
   <xsl:if test="@id">
    <aml:annotation aml:id="{translate(@id,' ','-')}" w:type="Word.Bookmark.Start" w:name="{translate(@id,' ','-')}"/>
    <aml:annotation aml:id="{translate(@id,' ','-')}" w:type="Word.Bookmark.End"/>
   </xsl:if>
   <xsl:apply-templates select="fo:inline|fo:block|fo:page-number|fo:leader|fo:table|fo:table-and-caption|fo:basic-link|fo:page-number-citation|fo:retrieve-marker|fo:marker|fo:list-block"/> <!-- Voláme šablony, které obslouží potomky elementu fo:block -->
   <xsl:if test="node()[last()]=text()"> <!-- Jestliže posledním uzlem je text, musíme ho zkopírovat na výstup -->
    <xsl:if test="name(node()[last()-1])='fo:block' or name(node()[last()-1])='fo:table' or name(node()[last()-1])='fo:table-and-caption' or name(node()[last()-1])='fo:list-block'"> <!-- Jestliže však před tímto textem byl jiný element fo:block, musíme vytvořit odstavec -->
     <xsl:text disable-output-escaping="yes">&lt;w:p&gt;</xsl:text>
     <w:pPr>
      <xsl:apply-templates select="ancestor-or-self::node()[@text-align][1]/@text-align"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@text-indent][1]/@text-indent"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@end-indent][1]/@end-indent"/>
      <xsl:apply-templates select="@space-before"/><xsl:apply-templates select="@space-after"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@line-height][1]/@line-height"/>
      <xsl:apply-templates select="@background-color"/><xsl:apply-templates select="@border-top-style"/><xsl:apply-templates select="@border-before-style"/>
      <xsl:apply-templates select="@border-bottom-style"/><xsl:apply-templates select="@border-after-style"/>
      <xsl:apply-templates select="@border-left-style"/><xsl:apply-templates select="@border-start-style"/>
      <xsl:apply-templates select="@border-right-style"/><xsl:apply-templates select="@border-end-style"/><xsl:apply-templates select="@border-style"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@orphans][1]/@orphans"/><xsl:apply-templates select="ancestor-or-self::node()[@widows][1]/@widows"/>
      <xsl:apply-templates select="@break-before"/><xsl:apply-templates select="preceding-sibling::node()[1][@break-after]/@break-after"/>
      <xsl:apply-templates select="preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@keep-together][1]/@keep-together"/>
      <xsl:apply-templates select="@keep-with-next"/><xsl:apply-templates select="following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
      <xsl:apply-templates select="following-sibling::node()[1]/descendant::node()[1][@keep-with-previous]/@keep-with-previous"/>
      <xsl:apply-templates select="ancestor::node()[not(child::text())]/following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
      <xsl:if test="not(following-sibling::node()) and (ancestor::fo:flow) and (ancestor::fo:page-sequence/following-sibling::node())">
       <xsl:apply-templates select="ancestor::fo:page-sequence"/>
      </xsl:if>
      <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@background-color"/></xsl:if>
      <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@margin-left" mode="blok"/></xsl:if>
      <xsl:if test="parent::fo:table-caption"><xsl:apply-templates select="ancestor::fo:table-and-caption/@margin-right" mode="blok"/></xsl:if>
      <xsl:if test="ancestor::fo:list-item-body">
       <w:listPr>
        <w:ilvl w:val="{count(ancestor::fo:list-item-body)-1}"/>
        <w:ilfo w:val="{count(preceding::fo:list-block)+1}"/>
       </w:listPr>
      </xsl:if>
     </w:pPr>
    </xsl:if>
    <w:r>
     <w:rPr> <!-- Vlastnosti textu. Voláme výskyt vlastnosti u nejbližšího předchůdce. -->
      <xsl:apply-templates select="ancestor-or-self::node()[@color][1]/@color"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-family][1]/@font-family"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-size][1]/@font-size"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-weight][1]/@font-weight"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-style][1]/@font-style"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-stretch][1]/@font-stretch"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@font-variant][1]/@font-variant"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@text-transform][1]/@text-transform"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@text-decoration][1]/@text-decoration"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@text-shadow][1]/@text-shadow"/>
      <xsl:apply-templates select="ancestor-or-self::node()[@letter-spacing][1]/@letter-spacing"/>
      <xsl:apply-templates select="@vertical-align"/>
     </w:rPr>
     <w:t><!-- Výpis posledního textového uzlu, který je potomkem akt. uzlu -->
      <xsl:if test="name(node()[last()-1])!='fo:block' and substring(text()[last()],1,1)=' '">
       <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:value-of select="normalize-space(text()[last()])"/>
     </w:t>
    </w:r>
    <xsl:if test="name(node()[last()-1])='fo:block' or name(node()[last()-1])='fo:table' or name(node()[last()-1])='fo:table-and-caption' or name(node()[last()-1])='fo:list-block'"> <!-- Konec odstavce, jestliže předposledním potomkem byl fo:block -->
     <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text>
    </xsl:if>
   </xsl:if>
 <xsl:if test="(node()[1]=text() or fo:inline[1] or fo:page-number[1] or fo:leader[1] or fo:basic-link[1] or fo:page-number-citation[1] or fo:retrieve-marker[1] or fo:marker[1]) and not(fo:block[last()])">
  <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text>
 </xsl:if> <!-- Jestliže první potomek je text nebo fo:inline a poslední potomek není fo:block, uzavřeme odstavec -->
</xsl:template>

<xsl:template match="fo:list-block">
 <xsl:apply-templates select="fo:list-item"/>
</xsl:template>

<xsl:template match="fo:list-item">
 <xsl:apply-templates select="fo:list-item-body"/>
</xsl:template>

<xsl:template match="fo:list-item-body">
 <xsl:apply-templates select="fo:block|fo:table|fo:table-and-caption|fo:list-block"/>
</xsl:template>

<xsl:template match="fo:table-and-caption">
 <xsl:apply-templates select="fo:table-caption[../@caption-side='top' or ../@caption-side='left' or ../@caption-side='before' or ../@caption-side='start' or not(../@caption-side)]"/>
 <xsl:apply-templates select="fo:table"/>
 <xsl:apply-templates select="fo:table-caption[../@caption-side='bottom' or ../@caption-side='right' or ../@caption-side='after' or ../@caption-side='end']"/>
</xsl:template>

<xsl:template name="fo:table-caption">
 <xsl:apply-templates select="fo:block|fo:table|fo:table-and-caption|fo:list-block"/>
</xsl:template>

<xsl:template match="fo:table">
 <xsl:variable name="akt" select="."/>
 <xsl:call-template name="predchoziodstavec"/>
 <w:p><w:pPr>
  <w:spacing w:line="1" w:line-rule="exact"/>
  <xsl:apply-templates select="@space-before|ancestor::node()[count(child::node())=1][@space-before][1]/@space-before"/>
  <xsl:apply-templates select="@break-before|ancestor::node()[count(child::node())=1][@break-before][1]/@break-before"/>
  <xsl:apply-templates select="ancestor::node()[count(child::node())=1]/preceding-sibling::node()[1][@break-after]/@break-after"/>
  <xsl:apply-templates select="ancestor::node()[count(child::node())=1]/preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
 </w:pPr></w:p>
 <w:tbl>
  <w:tblPr>
   <xsl:if test="fo:table-column or @table-layout='fixed'"><w:tblLayout w:type="Fixed"/></xsl:if>
   <xsl:if test="@width"><w:tblW><xsl:attribute name="w:w"><xsl:apply-templates select="@width"/></xsl:attribute>
    <xsl:attribute name="w:type"><xsl:value-of select="'dxa'"/></xsl:attribute></w:tblW></xsl:if>
   <xsl:apply-templates select="@border-style" mode="tab"/><xsl:apply-templates select="@border-top-style" mode="tab"/>
   <xsl:apply-templates select="@border-before-style" mode="tab"/>
   <xsl:apply-templates select="@border-bottom-style" mode="tab"/><xsl:apply-templates select="@border-after-style" mode="tab"/>
   <xsl:apply-templates select="@border-left-style" mode="tab"/><xsl:apply-templates select="@border-start-style" mode="tab"/>
   <xsl:apply-templates select="@border-right-style" mode="tab"/><xsl:apply-templates select="@border-end-style" mode="tab"/>
   <xsl:apply-templates select="@background-color"/>
   <xsl:apply-templates select="@border-separation"/>
   <xsl:apply-templates select="@margin-left|parent::fo:table-and-caption/@margin-left" mode="tab"/>
  </w:tblPr>
  <w:tblGrid><xsl:apply-templates select="fo:table-column"/></w:tblGrid>
  <xsl:apply-templates select="fo:table-header"/>
  <xsl:apply-templates select="fo:table-body"/>
  <xsl:apply-templates select="fo:table-footer"/>
 </w:tbl>
 <w:p><w:pPr>
  <w:spacing w:line="1" w:line-rule="exact"/>
  <xsl:apply-templates select="@space-after|ancestor::node()[count(child::node())=1][@space-after][1]/@space-after"/>
  <xsl:if test="not(@space-after|ancestor::node()[count(child::node())=1][@space-after][1]/@space-after)">
   <xsl:apply-templates select="ancestor::node()[@space-after][self::node()[$akt=descendant::node()[not(following-sibling::node())]]][1]/@space-after"/>
  </xsl:if>
 </w:pPr></w:p>
</xsl:template>

<xsl:template match="fo:table-column">
 <xsl:variable name="opakovani">
  <xsl:if test="@number-columns-repeated"><xsl:value-of select="@number-columns-repeated"/></xsl:if>
  <xsl:if test="not(@number-columns-repeated)"><xsl:value-of select="'1'"/></xsl:if>
 </xsl:variable>
 <xsl:call-template name="sloupec"><xsl:with-param name="opak" select="$opakovani"/></xsl:call-template>
</xsl:template>

<xsl:template name="sloupec">
<xsl:param name="opak"/>
 <w:gridCol>
  <xsl:attribute name="w:w">
   <xsl:apply-templates select="@column-width"/>
  </xsl:attribute>
 </w:gridCol>
<xsl:if test="$opak&gt;1"><xsl:call-template name="sloupec"><xsl:with-param name="opak" select="($opak)-1"/></xsl:call-template></xsl:if>
</xsl:template>

<xsl:template match="@column-width|@width">
 <xsl:variable name="twidth">
  <xsl:if test="ancestor::fo:table[@width]/@width"><xsl:value-of select="ancestor::fo:table/@width"/></xsl:if>
  <xsl:if test="not(ancestor::fo:table[@width]/@width)"><xsl:value-of select="'nil'"/></xsl:if>
 </xsl:variable>
 <xsl:variable name="master" select="ancestor::fo:page-sequence/@master-reference"/>
 <xsl:variable name="pgwidth" select="ancestor::fo:root/fo:layout-master-set/fo:simple-page-master[@master-name=$master]/@page-width"/>
 <xsl:variable name="mlefttwips"><xsl:call-template name="prevodtwipsp">
  <xsl:with-param name="h" select="ancestor::fo:root/fo:layout-master-set/fo:simple-page-master[@master-name=$master]/@margin-left"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:variable name="mrighttwips"><xsl:call-template name="prevodtwipsp">
  <xsl:with-param name="h" select="ancestor::fo:root/fo:layout-master-set/fo:simple-page-master[@master-name=$master]/@margin-right"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:variable name="twidthtwips">
  <xsl:choose>
   <xsl:when test="$twidth='nil' or $twidth='auto'"><xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="$pgwidth"/></xsl:call-template></xsl:when>
   <xsl:when test="contains($twidth,'%')"><xsl:call-template name="prevodtwipsp">
    <xsl:with-param name="h" select="concat(string((number(substring-before($twidth,'%')) div 100)*number(substring($pgwidth,1,string-length($pgwidth)-2))),substring($pgwidth,string-length($pgwidth)-1,string-length($pgwidth)))"/>
   </xsl:call-template></xsl:when>
   <xsl:otherwise><xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="$twidth"/></xsl:call-template></xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <xsl:variable name="soucet">
  <xsl:for-each select="../../fo:table-column">
   <xsl:variable name="dalsi" select="number(substring(@column-width,27,string-length(@column-width)-27))"/>
   <xsl:value-of select="concat($dalsi,'+')"/>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="secteno">
  <xsl:call-template name="scitani"><xsl:with-param name="s" select="'0'"/><xsl:with-param name="r" select="$soucet"/></xsl:call-template>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="parent::fo:table and(contains($twidth,'%') or contains($twidth,'nil'))"><xsl:value-of select="(($twidthtwips)-($mlefttwips)-($mrighttwips))"/></xsl:when>
  <xsl:when test="parent::fo:table and not(contains($twidth,'%')) and not(contains($twidth,'nil'))"><xsl:value-of select="$twidthtwips"/></xsl:when>
  <xsl:when test="contains(.,'%') and not(parent::fo:table)"><xsl:value-of select="(($twidthtwips)-($mlefttwips)-($mrighttwips))*(substring-before(.,'%')) div 100"/></xsl:when>
  <xsl:when test="contains(.,'proportional-column-width')"><xsl:value-of select="(($twidthtwips)-($mlefttwips)-($mrighttwips)) div ($secteno)*number(substring(.,27,string-length(.)-27))"/></xsl:when>
  <xsl:otherwise><xsl:call-template name="prevodtwips"/></xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="scitani">
 <xsl:param name="s"/><xsl:param name="r"/>
 <xsl:if test="string-length($r)&gt;0">
  <xsl:call-template name="scitani">
   <xsl:with-param name="s" select="$s+number(substring-before($r,'+'))"/>
   <xsl:with-param name="r" select="substring-after($r,'+')"/>
  </xsl:call-template>
 </xsl:if>
 <xsl:if test="string-length($r)=0"><xsl:value-of select="$s"/></xsl:if>
</xsl:template>

<xsl:template match="fo:table-header|fo:table-body|fo:table-footer">
 <xsl:apply-templates select="fo:table-row"/>
 <xsl:if test="not(fo:table-row)"><w:tr>
  <xsl:if test="(not(ancestor::node()[@table-omit-header-at-break])or(ancestor::node()[@table-omit-header-at-break]/@table-omit-header-at-break='false'))
                 and self::fo:table-header">
    <w:trPr><w:tblHeader w:val="on"/></w:trPr>
  </xsl:if>
  <xsl:apply-templates select="fo:table-cell"/></w:tr>
 </xsl:if>
</xsl:template>

<xsl:template match="fo:table-row">
 <w:tr>
  <w:tblPrEx>
   <xsl:apply-templates select="@border-style" mode="tab"/><xsl:apply-templates select="@border-top-style" mode="tab"/>
   <xsl:apply-templates select="@border-before-style" mode="tab"/>
   <xsl:apply-templates select="@border-bottom-style" mode="tab"/><xsl:apply-templates select="@border-after-style" mode="tab"/>
   <xsl:apply-templates select="@border-left-style" mode="tab"/><xsl:apply-templates select="@border-start-style" mode="tab"/>
   <xsl:apply-templates select="@border-right-style" mode="tab"/><xsl:apply-templates select="@border-end-style" mode="tab"/>
  </w:tblPrEx>
  <w:trPr>
   <xsl:apply-templates select="@height"/>
   <xsl:if test="(not(ancestor::node()[@table-omit-header-at-break])or(ancestor::node()[@table-omit-header-at-break]/@table-omit-header-at-break='false'))
                 and ancestor::fo:table-header">
    <w:tblHeader w:val="on"/>
   </xsl:if>
  </w:trPr>
  <xsl:apply-templates select="fo:table-cell"/>
 </w:tr>
</xsl:template>

<xsl:template match="fo:table-cell">
 <xsl:variable name="pocetsl"> <!--Celkový počet sloupců tabulky-->
  <xsl:choose>
   <xsl:when test="fo:table-column">
    <xsl:value-of select="count(ancestor::fo:table-column)+sum(ancestor::fo:table-column/@number-columns-repeated)-count(ancestor::fo:table-column[@number-columns-repeated])"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="count(ancestor::fo:table-body/fo:table-row[1][not(fo:table-cell/@number-rows-spanned)]/fo:table-cell)+
                          sum(ancestor::fo:table-body/fo:table-row[1][not(fo:table-cell/@number-rows-spanned)]/fo:table-cell/@number-columns-spanned)-
                          count(ancestor::fo:table-body/fo:table-row[1][not(fo:table-cell/@number-rows-spanned)]/fo:table-cell[@number-columns-spanned])"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <xsl:variable name="pom" select="count(../preceding-sibling::fo:table-row[fo:table-cell/@number-rows-spanned])"/><!--Celkový počet řádků tabulky, které obsahují atribut number-rows-spanned-->
 <xsl:variable name="pozicep"><!--Číslo nejbližšího řádku tabulky k právě zpracovávanému řádku, jehož buňka obsahuje atribut number-rows-spanned-->
  <xsl:for-each select="../preceding-sibling::fo:table-row">
   <xsl:if test="self::node()[fo:table-cell/@number-rows-spanned] and ($pom)-1=count(preceding-sibling::fo:table-row[fo:table-cell/@number-rows-spanned])"><xsl:value-of select="position()"/></xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="pozicer" select="count(../preceding-sibling::fo:table-row)+1-$pozicep"/><!--Vzdálenost nejbližšího řádku s atr. number-rows-spanned od právě zprac. řádku-->
 <xsl:variable name="pozices"><!--Číslo sloupce nejbližšího řádku s atributem number-rows-spanned-->
  <xsl:for-each select="../preceding-sibling::fo:table-row">
   <xsl:if test="self::node()[fo:table-cell/@number-rows-spanned] and ($pom)-1=count(preceding-sibling::fo:table-row[fo:table-cell/@number-rows-spanned])">
    <xsl:value-of select="($pocetsl)-count(fo:table-cell[@number-rows-spanned]/following-sibling::node())-
                          sum(fo:table-cell[@number-rows-spanned]/following-sibling::node()[@number-columns-spanned]/@number-columns-spanned)+
                          count(fo:table-cell[@number-rows-spanned]/following-sibling::node()[@number-columns-spanned])"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="pocetr" select="../preceding-sibling::fo:table-row/fo:table-cell/@number-rows-spanned"/><!--Počet řádků, které se mají spojit-->
 <xsl:if test="(count(../fo:table-cell)&lt;$pocetsl) and $pocetr&gt;$pozicer and $pozices=position()">
  <w:tc><w:tcPr><w:vmerge w:val="continue"/><xsl:apply-templates select="@padding"/>
                <xsl:apply-templates select="@padding-top|@padding-before"/><xsl:apply-templates select="@padding-bottom|@padding-after"/>
                <xsl:apply-templates select="@padding-left|@padding-start"/><xsl:apply-templates select="@padding-right|@padding-end"/>
        </w:tcPr><w:p/></w:tc>
 </xsl:if>
 <w:tc>
  <w:tcPr>
   <xsl:if test="@width"><w:tcW><xsl:attribute name="w:w"><xsl:apply-templates select="@width"/></xsl:attribute>
    <xsl:attribute name="w:type"><xsl:value-of select="'dxa'"/></xsl:attribute></w:tcW></xsl:if>
   <xsl:apply-templates select="@number-columns-spanned"/>
   <xsl:apply-templates select="@number-rows-spanned"/>
   <xsl:apply-templates select="@background-color|../@background-color"/>
   <xsl:if test="not(@background-color)and not(../@background-color)"><xsl:apply-templates select="ancestor::fo:table/@background-color"/></xsl:if>
   <xsl:apply-templates select="@border-style" mode="bunka"/><xsl:apply-templates select="@border-top-style" mode="bunka"/>
   <xsl:apply-templates select="@border-before-style" mode="bunka"/>
   <xsl:apply-templates select="@border-bottom-style" mode="bunka"/><xsl:apply-templates select="@border-after-style" mode="bunka"/>
   <xsl:apply-templates select="@border-left-style" mode="bunka"/><xsl:apply-templates select="@border-start-style" mode="bunka"/>
   <xsl:apply-templates select="@border-right-style" mode="bunka"/><xsl:apply-templates select="@border-end-style" mode="bunka"/>
   <xsl:apply-templates select="ancestor-or-self::node()[@display-align][1]/@display-align"/>
   <xsl:apply-templates select="@padding"/><xsl:apply-templates select="@padding-top|@padding-before"/>
   <xsl:apply-templates select="@padding-bottom|@padding-after"/>
   <xsl:apply-templates select="@padding-left|@padding-start"/><xsl:apply-templates select="@padding-right|@padding-end"/>
  </w:tcPr>
  <xsl:apply-templates select="fo:block|fo:table|fo:table-and-caption|fo:list-block"/>
  <xsl:if test="(count(fo:block)=1 and fo:block[not(child::node())])"><w:p></w:p></xsl:if>
 </w:tc>
 <xsl:if test="@number-columns-spanned">
  <xsl:call-template name="hmerge"><xsl:with-param name="kolik" select="(@number-columns-spanned)-1"/></xsl:call-template>
 </xsl:if>
 <xsl:if test="(count(../fo:table-cell)&lt;$pocetsl) and $pocetr&gt;$pozicer and $pozices=position()+1 and position()=last()">
  <w:tc><w:tcPr><w:vmerge w:val="continue"/></w:tcPr><w:p/></w:tc>
 </xsl:if>
</xsl:template>

<xsl:template match="@margin-left" mode="tab">
 <w:tblInd>
  <xsl:attribute name="w:w"><xsl:call-template name="prevodtwips"/></xsl:attribute>
  <xsl:attribute name="w:type"><xsl:value-of select="'dxa'"/></xsl:attribute>
 </w:tblInd>
</xsl:template>

<xsl:template match="@border-separation">
 <w:tblCellSpacing>
  <xsl:attribute name="w:w"><xsl:call-template name="prevodtwips"/></xsl:attribute>
  <xsl:attribute name="w:type"><xsl:value-of select="'dxa'"/></xsl:attribute>
 </w:tblCellSpacing>
</xsl:template>

<xsl:template match="@height">
 <w:trHeight>
  <xsl:attribute name="w:val"><xsl:call-template name="prevodtwips"/></xsl:attribute>
 </w:trHeight>
</xsl:template>

<xsl:template match="@padding">
 <xsl:variable name="w"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <w:tcMar>
  <w:top w:w="{$w}" w:type="dxa"/>
  <w:bottom w:w="{$w}" w:type="dxa"/>
  <w:left w:w="{$w}" w:type="dxa"/>
  <w:right w:w="{$w}" w:type="dxa"/>
 </w:tcMar>
</xsl:template>

<xsl:template match="@padding-top|@padding-before">
 <xsl:variable name="w"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <w:tcMar>
  <w:top w:w="{$w}" w:type="dxa"/>
 </w:tcMar>
</xsl:template>

<xsl:template match="@padding-bottom|@padding-after">
 <xsl:variable name="w"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <w:tcMar>
  <w:bottom w:w="{$w}" w:type="dxa"/>
 </w:tcMar>
</xsl:template>

<xsl:template match="@padding-left|@padding-start">
 <xsl:variable name="w"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <w:tcMar>
  <w:left w:w="{$w}" w:type="dxa"/>
 </w:tcMar>
</xsl:template>

<xsl:template match="@padding-right|@padding-end">
 <xsl:variable name="w"><xsl:call-template name="prevodtwips"/></xsl:variable>
 <w:tcMar>
  <w:right w:w="{$w}" w:type="dxa"/>
 </w:tcMar>
</xsl:template>

<xsl:template match="@display-align">
 <xsl:choose>
  <xsl:when test=".='auto' or .='before'">
   <w:vAlign w:val="top"/>
  </xsl:when>
  <xsl:when test=".='after'">
   <w:vAlign w:val="bottom"/>
  </xsl:when>
  <xsl:otherwise>
   <w:vAlign w:val="{.}"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template match="@border-style" mode="bunka">
 <w:tcBorders>
  <xsl:call-template name="border-style"/>
 </w:tcBorders>
</xsl:template>

<xsl:template match="@border-bottom-style|border-after-style" mode="bunka">
 <w:tcBorders>
  <xsl:call-template name="bbottom"/>
 </w:tcBorders>
</xsl:template>

<xsl:template match="@border-top-style|border-before-style" mode="bunka">
 <w:tcBorders>
  <xsl:call-template name="btop"/>
 </w:tcBorders>
</xsl:template>

<xsl:template match="@border-left-style|border-start-style" mode="bunka">
 <w:tcBorders>
  <xsl:call-template name="bleft"/>
 </w:tcBorders>
</xsl:template>

<xsl:template match="@border-right-style|border-end-style" mode="bunka">
 <w:tcBorders>
  <xsl:call-template name="bright"/>
 </w:tcBorders>
</xsl:template>

<xsl:template match="@border-style" mode="tab">
 <w:tblBorders>
  <xsl:call-template name="border-style"/>
 </w:tblBorders>
</xsl:template>

<xsl:template match="@border-bottom-style|border-after-style" mode="tab">
 <w:tblBorders>
  <xsl:call-template name="bbottom"/>
 </w:tblBorders>
</xsl:template>

<xsl:template match="@border-top-style|border-before-style" mode="tab">
 <w:tblBorders>
  <xsl:call-template name="btop"/>
 </w:tblBorders>
</xsl:template>

<xsl:template match="@border-left-style|border-start-style" mode="tab">
 <w:tblBorders>
  <xsl:call-template name="bleft"/>
 </w:tblBorders>
</xsl:template>

<xsl:template match="@border-right-style|border-end-style" mode="tab">
 <w:tblBorders>
  <xsl:call-template name="bright"/>
 </w:tblBorders>
</xsl:template>

<xsl:template match="@number-columns-spanned">
 <w:hmerge w:val="restart"/>
</xsl:template>

<xsl:template match="@number-rows-spanned">
 <w:vmerge w:val="restart"/>
</xsl:template>

<xsl:template name="hmerge">
 <xsl:param name="kolik"/>
 <w:tc><w:tcPr><w:hmerge w:val="continue"/></w:tcPr><w:p/></w:tc>
 <xsl:if test="($kolik)&gt;1"><xsl:call-template name="hmerge"><xsl:with-param name="kolik" select="($kolik)-1"/></xsl:call-template></xsl:if>
</xsl:template>

<xsl:template match="@text-align"> <!-- Šablony pro odstavcové vlastnosti -->
 <xsl:choose>
  <xsl:when test=".='justify'">
   <w:jc w:val="both"/>
  </xsl:when>
  <xsl:when test=".='start'">
   <w:jc w:val="left"/>
  </xsl:when>
  <xsl:when test=".='end'">
   <w:jc w:val="right"/>
  </xsl:when>
  <xsl:otherwise>
   <w:jc w:val="{.}"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template match="@text-indent">
 <w:ind>
  <xsl:if test="substring(.,1,1)!='-'">
   <xsl:attribute name="w:first-line">
    <xsl:call-template name="prevodtwips"/>
   </xsl:attribute>
  </xsl:if>
  <xsl:if test="substring(.,1,1)='-'">
   <xsl:attribute name="w:hanging">
    <xsl:call-template name="prevodtwips"/>
   </xsl:attribute>
  </xsl:if>
 </w:ind>
</xsl:template>

<xsl:template match="@start-indent">
 <w:ind>
  <xsl:attribute name="w:left">
   <xsl:call-template name="prevodtwips"/>
  </xsl:attribute>
 </w:ind>
</xsl:template>

<xsl:template match="@margin-left" mode="blok">
 <w:ind>
  <xsl:attribute name="w:left">
   <xsl:call-template name="prevodtwips"/>
  </xsl:attribute>
 </w:ind>
</xsl:template>

<xsl:template match="@end-indent">
 <w:ind>
  <xsl:attribute name="w:right">
   <xsl:call-template name="prevodtwips"/>
  </xsl:attribute>
 </w:ind>
</xsl:template>

<xsl:template match="@margin-right" mode="blok">
 <w:ind>
  <xsl:attribute name="w:right">
   <xsl:call-template name="prevodtwips"/>
  </xsl:attribute>
 </w:ind>
</xsl:template>

<xsl:template match="@space-before">
 <!--
 <w:spacing>  
  <xsl:attribute name="w:before">
   <xsl:variable name="akt" select="parent::node()"/>
   <xsl:variable name="before"><xsl:call-template name="prevodtwips"/></xsl:variable>
   <xsl:for-each select="../ancestor::node()[@space-before][self::node()[$akt=descendant::node()[1]]]/@space-before">
    <xsl:variable name="dalsi"><xsl:call-template name="prevodtwips"/></xsl:variable>
    <xsl:variable name="before2" select="$before+$dalsi"/>
    <xsl:if test="self::node()[last()]"><xsl:if test="$before2&gt;0"><xsl:value-of select="$before2"/></xsl:if>
                                        <xsl:if test="$before2&lt;0"><xsl:value-of select="'0'"/></xsl:if></xsl:if>
   </xsl:for-each>
   <xsl:if test="not(../ancestor::node()[@space-before][self::node()[$akt=descendant::node()[1]]]/@space-before)">
    <xsl:if test="$before&gt;0"><xsl:value-of select="$before"/></xsl:if>
    <xsl:if test="$before&lt;0"><xsl:value-of select="'0'"/></xsl:if>
   </xsl:if>
  </xsl:attribute>
 </w:spacing> -->
</xsl:template>

<xsl:template match="@space-after">
<!-- 
 <w:spacing> 
  <xsl:attribute name="w:after">
   <xsl:variable name="akt" select="parent::node()"/>
   <xsl:variable name="after"><xsl:call-template name="prevodtwips"/></xsl:variable>
   <xsl:for-each select="../ancestor::node()[@space-after][self::node()[$akt=descendant::node()[last()]]]/@space-after">
    <xsl:variable name="dalsi"><xsl:call-template name="prevodtwips"/></xsl:variable>
    <xsl:variable name="after2" select="$after+$dalsi"/>
    <xsl:if test="self::node()[last()]"><xsl:value-of select="$after2"/></xsl:if>
   </xsl:for-each>
   <xsl:if test="not(../ancestor::node()[@space-after][self::node()[$akt=descendant::node()[last()]]]/@space-after)"><xsl:value-of select="$after"/></xsl:if>
  </xsl:attribute>
 </w:spacing> -->
</xsl:template>

<xsl:template match="@line-height">
<xsl:if test=".!='normal'">
 <w:spacing w:after="240" w:before="240"/>
</xsl:if>
</xsl:template>

<xsl:template match="@background-color">
 <w:shd>
  <xsl:attribute name="w:val"><xsl:value-of select="'clear'"/></xsl:attribute>
  <xsl:attribute name="w:fill">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
 </w:shd>
</xsl:template>

<xsl:template match="@border-style">
 <w:pBdr>
  <xsl:call-template name="border-style"/>
 </w:pBdr>
</xsl:template>

<xsl:template name="border-style">
 <xsl:variable name="val">
  <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
 </xsl:variable>
 <xsl:variable name="color">
  <xsl:if test="../@border-color">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../@border-color)and ancestor-or-self::node()[@color]">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="ancestor-or-self::node()[@color][1]/@color"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../@border-color)and not(ancestor-or-self::node()[@color])"><xsl:value-of select="'auto'"/></xsl:if>
 </xsl:variable>
 <xsl:variable name="sz">
  <xsl:if test="../@border-width">
   <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../@border-width)">
   <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="'nil'"/></xsl:call-template>
  </xsl:if>
 </xsl:variable>
 <xsl:variable name="space">
  <xsl:choose>
   <xsl:when test="contains(../@padding,'pt') or contains(../@padding,'in') or contains(../@padding,'mm') or contains(../@padding,'cm') or contains(../@padding,'pc')">
    <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
   </xsl:when>
   <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <w:top>
  <xsl:attribute name="w:val"><xsl:value-of select="$val"/></xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-top-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-top-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-before-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-before-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$sz"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-top">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-top"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-before">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-before"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$space"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-top-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-top-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-before-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-before-color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$color"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </w:top>
 <w:left>
  <xsl:attribute name="w:val"><xsl:value-of select="$val"/></xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-left-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-left-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-start-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-start-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$sz"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-left">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-left"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-start">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-start"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$space"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-left-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-left-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-start-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-start-color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$color"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </w:left>
 <w:bottom>
  <xsl:attribute name="w:val"><xsl:value-of select="$val"/></xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-bottom-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-bottom-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-after-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-after-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$sz"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-bottom">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-bottom"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-after">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-after"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$space"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-bottom-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-bottom-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-after-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-after-color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$color"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </w:bottom>
 <w:right>
  <xsl:attribute name="w:val"><xsl:value-of select="$val"/></xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-right-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-right-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-end-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-end-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$sz"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-right">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-right"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-end">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-end"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$space"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-right-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-right-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-end-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-end-color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$color"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </w:right>
</xsl:template>

<xsl:template match="@border-top-style|@border-before-style">
 <w:pBdr>
  <xsl:call-template name="btop"/>
 </w:pBdr>
</xsl:template>

<xsl:template name="btop">
  <w:top>
  <xsl:attribute name="w:val">
   <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-top-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-top-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-before-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-before-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'12'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-top">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-top"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-before">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-before"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-top-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-top-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-before-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-before-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="ancestor-or-self::node()[@color] and not(../@border-top-color)and not(../@border-before-color)and not(../@border-color)">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="ancestor-or-self::node()[@color][1]/@color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'auto'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
  </w:top>
</xsl:template>

<xsl:template match="@border-bottom-style|@border-after-style">
 <w:pBdr>
  <xsl:call-template name="bbottom"/>
 </w:pBdr>
</xsl:template>

<xsl:template name="bbottom">
  <w:bottom>
  <xsl:attribute name="w:val">
   <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-bottom-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-bottom-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-after-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-after-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'12'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-bottom">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-bottom"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-after">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-after"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-bottom-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-bottom-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-after-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-after-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="ancestor-or-self::node()[@color] and not(../@border-bottom-color)and not(../@border-after-color)and not(../@border-color)">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="ancestor-or-self::node()[@color][1]/@color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'auto'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
  </w:bottom>
</xsl:template>

<xsl:template match="@border-left-style|@border-start-style">
 <w:pBdr>
  <xsl:call-template name="bleft"/>
 </w:pBdr>
</xsl:template>

<xsl:template name="bleft">
  <w:left>
  <xsl:attribute name="w:val">
   <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-left-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-left-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-start-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-start-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'12'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-left">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-left"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-start">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-start"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-left-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-left-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-start-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-start-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="ancestor-or-self::node()[@color] and not(../@border-left-color)and not(../@border-start-color)and not(../@border-color)">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="ancestor-or-self::node()[@color][1]/@color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'auto'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
  </w:left>
</xsl:template>

<xsl:template match="@border-right-style|@border-end-style">
 <w:pBdr>
  <xsl:call-template name="bright"/>
 </w:pBdr>
</xsl:template>

<xsl:template name="bright">
  <w:right>
  <xsl:attribute name="w:val">
   <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
  <xsl:attribute name="w:sz">
   <xsl:choose>
    <xsl:when test="../@border-right-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-right-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-end-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-end-width"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-width">
     <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'12'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 <xsl:if test="parent::fo:block">
  <xsl:attribute name="w:space">
   <xsl:choose>
    <xsl:when test="../@padding-right">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-right"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding-end">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding-end"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@padding">
     <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </xsl:if>
  <xsl:attribute name="w:color">
   <xsl:choose>
    <xsl:when test="../@border-right-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-right-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-end-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-end-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="../@border-color">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
    </xsl:when>
    <xsl:when test="ancestor-or-self::node()[@color] and not(../@border-right-color)and not(../@border-end-color)and not(../@border-color)">
     <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="ancestor-or-self::node()[@color][1]/@color"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="'auto'"/></xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
  </w:right>
</xsl:template>

<xsl:template match="@orphans">
<w:widowControl>
 <xsl:attribute name="w:val">
  <xsl:if test=".&gt;0 and ../@widows and ../@widows&gt;0"><xsl:value-of select="'on'"/></xsl:if>
  <xsl:if test="not(.&gt;0 and ../@widows and ../@widows&gt;0)"><xsl:value-of select="'off'"/></xsl:if>
 </xsl:attribute>
</w:widowControl>
</xsl:template>

<xsl:template match="@widows">
<xsl:if test="not(../@orphans)">
 <w:widowControl>
  <xsl:attribute name="w:val">
   <xsl:if test=".&gt;0"><xsl:value-of select="'on'"/></xsl:if>
   <xsl:if test="not(.&gt;0)"><xsl:value-of select="'off'"/></xsl:if>
  </xsl:attribute>
 </w:widowControl>
</xsl:if>
</xsl:template>

<xsl:template match="@break-before|@break-after">
 <xsl:if test=".='page' or .='even-page' or .='odd-page'"><w:pageBreakBefore w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@keep-together">
 <xsl:if test=".='auto' or .='0'"><w:keepLines w:val="off"/></xsl:if>
 <xsl:if test="not(.='auto' or .='0')"><w:keepLines w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@keep-with-next|@keep-with-previous">
 <xsl:if test=".='auto' or .='0'"><w:keepNext w:val="off"/></xsl:if>
 <xsl:if test="not(.='auto' or .='0')"><w:keepNext w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="fo:leader" mode="blok">
 <xsl:variable name="indent">
  <xsl:if test="ancestor-or-self::node()/@start-indent">
   <xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="ancestor-or-self::node()[@start-indent][1]/@start-indent"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(ancestor-or-self::node()/@start-indent)">
   <xsl:value-of select="'0'"/>
  </xsl:if>
 </xsl:variable>
 <w:tabs>
  <w:tab>
   <xsl:attribute name="w:val"><xsl:value-of select="'center'"/></xsl:attribute>
   <xsl:attribute name="w:leader">
    <xsl:choose>
     <xsl:when test="@leader-pattern='space'"><xsl:value-of select="'none'"/></xsl:when>
     <xsl:when test="@leader-pattern='dots'"><xsl:value-of select="'dot'"/></xsl:when>
     <xsl:when test="@leader-pattern='rule' and @rule-style='none'"><xsl:value-of select="'none'"/></xsl:when>
     <xsl:when test="@leader-pattern='rule' and @rule-style='dotted'"><xsl:value-of select="'dot'"/></xsl:when>
     <xsl:when test="@leader-pattern='rule' and @rule-style='dashed'"><xsl:value-of select="'hyphen'"/></xsl:when>
     <xsl:when test="@leader-pattern='rule' and @rule-style='solid'"><xsl:value-of select="'underscore'"/></xsl:when>
     <xsl:when test="not(@leader-pattern)"><xsl:value-of select="'none'"/></xsl:when>
     <xsl:when test="@leader-pattern='rule' and not(@rule-style)"><xsl:value-of select="'underscore'"/></xsl:when>
     <xsl:otherwise><xsl:value-of select="'middle-dot'"/></xsl:otherwise>
    </xsl:choose>
   </xsl:attribute>
   <xsl:attribute name="w:pos">
    <xsl:choose>
     <xsl:when test="@leader-length and (not(preceding-sibling::node()) or name(preceding-sibling::node())='fo:block')">
       <xsl:call-template name="prevodtwipsp"><xsl:with-param name="h" select="@leader-length"/></xsl:call-template>
     </xsl:when>
     <xsl:otherwise><xsl:value-of select="10205-($indent)-100"/></xsl:otherwise>
    </xsl:choose>
   </xsl:attribute>
  </w:tab>
 </w:tabs>
</xsl:template>

<xsl:template match="fo:marker">
 <w:r>
  <w:rPr>
   <w:rStyle w:val="{@marker-class-name}"/>
  </w:rPr>
  <w:t>
   <xsl:value-of select="descendant::text()"/>
  </w:t>
 </w:r>
</xsl:template>

<xsl:template match="fo:inline"> <!-- Rekurzivně volaná šablona pro element fo:inline -->
 <xsl:call-template name="predchozitext"/>
 <xsl:apply-templates select="fo:inline|fo:page-number|fo:leader|fo:basic-link|fo:page-number-citation|fo:retrieve-marker|fo:marker"/> <!-- Rekurzivní volání elementu fo:inline -->
 <xsl:if test="node()[last()]=text() or count(node())=1"> <!-- Výpis posledního uzlu, pokud je textový -->
 <w:r>
  <xsl:call-template name="vlastnostiip"/>
  <w:t>
   <xsl:if test="substring(text()[last()],1,1)=' '">
    <xsl:text> </xsl:text>
   </xsl:if>
   <xsl:value-of select="normalize-space(text()[last()])"/>
   <xsl:if test="substring(text()[last()],string-length(text()[last()]),1)=' '">
    <xsl:text> </xsl:text>
   </xsl:if>
  </w:t>
 </w:r>
 </xsl:if>
</xsl:template>

<xsl:template match="fo:basic-link"> 
 <xsl:call-template name="predchozitext"/>
 <w:hlink> 
  <xsl:if test="@external-destination"><xsl:attribute name="w:dest"><xsl:value-of select="@external-destination"/></xsl:attribute></xsl:if>
  <xsl:if test="@internal-destination"><xsl:attribute name="w:bookmark"><xsl:value-of select="translate(@internal-destination,' ','-')"/></xsl:attribute></xsl:if>
  <xsl:apply-templates select="fo:inline|fo:page-number|fo:leader|fo:basic-link|fo:page-number-citation|fo:retrieve-marker|fo:marker"/> 
  <xsl:if test="node()[last()]=text() or count(node())=1"> 
   <w:r>
    <xsl:call-template name="vlastnostiip"/>
    <w:t>
     <xsl:if test="substring(text()[last()],1,1)=' '">
      <xsl:text> </xsl:text>
     </xsl:if>
     <xsl:value-of select="normalize-space(text()[last()])"/>
     <xsl:if test="substring(text()[last()],string-length(text()[last()]),1)=' '">
      <xsl:text> </xsl:text>
     </xsl:if>
    </w:t>
   </w:r>
  </xsl:if>
 </w:hlink>
</xsl:template>

<xsl:template match="fo:page-number">
 <xsl:call-template name="predchozitext"/>
 <w:r>
  <xsl:call-template name="vlastnostiip"/>
  <w:fldChar w:fldCharType="begin"/>
  <w:instrText>PAGE</w:instrText>
  <w:fldChar w:fldCharType="end"/>
 </w:r>
</xsl:template>

<xsl:template match="fo:page-number-citation">
 <xsl:call-template name="predchozitext"/>
 <w:r>
  <xsl:call-template name="vlastnostiip"/>
  <w:fldChar w:fldCharType="begin"/>
  <w:instrText><xsl:value-of select="concat('PAGEREF ',translate(@ref-id,' ','-'))"/></w:instrText>
  <w:fldChar w:fldCharType="end"/>
 </w:r>
</xsl:template>

<xsl:template match="fo:retrieve-marker">
 <xsl:call-template name="predchozitext"/>
 <w:r>
  <xsl:call-template name="vlastnostiip"/>
  <w:fldChar w:fldCharType="begin"/>
  <w:instrText>
   <xsl:value-of select="concat('STYLEREF ',@retrieve-class-name)"/>
   <xsl:if test="@retrieve-position='last-starting-within-page' or @retrieve-position='last-ending-within-page'">
    <xsl:value-of select="' \l'"/>
   </xsl:if>
  </w:instrText>
  <w:fldChar w:fldCharType="end"/>
 </w:r>
</xsl:template>

<xsl:template match="fo:leader">
 <xsl:call-template name="predchozitext"/>
 <w:r>
  <xsl:call-template name="vlastnostiip"/>
  <w:tab/>
 </w:r>
</xsl:template>

<xsl:template match="fo:block"> <!-- Rekurzivně volaná šablona pro element fo:block. Podobná předchozí šabloně, navíc se musí obstarat otevírání a uzavírání odstavců. -->
 <xsl:call-template name="predchoziodstavec"/>
 <xsl:if test="node()[1]=text() or fo:inline[1] or fo:page-number[1] or fo:leader[1] or fo:basic-link[1] or fo:page-number-citation[1] or fo:retrieve-marker[1] or fo:marker[1]"> <!-- Začátek odstavce, pokud je prvním potomkem text nebo fo:inline -->
  <xsl:text disable-output-escaping="yes">&lt;w:p&gt;</xsl:text>
  <w:pPr>
   <xsl:variable name="akt" select="."/>
   <xsl:if test="(not(fo:marker) or not(fo:block)) and not(ancestor::fo:list-item-body)">
    <xsl:apply-templates select="ancestor-or-self::node()[@text-align][1]/@text-align"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-indent][1]/@text-indent"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@end-indent][1]/@end-indent"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@line-height][1]/@line-height"/>
    <xsl:apply-templates select="@background-color"/><xsl:apply-templates select="@border-top-style"/><xsl:apply-templates select="@border-before-style"/>
    <xsl:apply-templates select="@border-bottom-style"/><xsl:apply-templates select="@border-after-style"/>
    <xsl:apply-templates select="@border-left-style"/><xsl:apply-templates select="@border-start-style"/>
    <xsl:apply-templates select="@border-right-style"/><xsl:apply-templates select="@border-end-style"/><xsl:apply-templates select="@border-style"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@orphans][1]/@orphans"/><xsl:apply-templates select="ancestor-or-self::node()[@widows][1]/@widows"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@keep-together][1]/@keep-together"/>
    <xsl:apply-templates select="@keep-with-next"/><xsl:apply-templates select="following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="following-sibling::node()[1]/descendant::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="ancestor::node()[not(child::text())]/following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:if test="not(following-sibling::node()) and not(child::fo:block or descendant::fo:table) and not(ancestor::fo:block/following-sibling::node()) and (ancestor::fo:flow) and (ancestor::fo:page-sequence/following-sibling::node())">
     <xsl:apply-templates select="ancestor::fo:page-sequence"/>
    </xsl:if>
    <xsl:apply-templates select="descendant::fo:leader" mode="blok"/>
   </xsl:if>
   <xsl:choose>
     <xsl:when test="not(following-sibling::node()) and (not(fo:marker) or not(fo:block)) and not(ancestor::fo:table)"><xsl:apply-templates select="ancestor-or-self::node()[$akt=descendant::node()[last()]][@space-after][1]/@space-after"/></xsl:when>
     <xsl:when test="not(fo:marker) or not(fo:block)"><xsl:apply-templates select="@space-after"/></xsl:when>
   </xsl:choose>
   <xsl:choose>
    <xsl:when test="@space-before"><xsl:apply-templates select="@space-before"/></xsl:when>
    <xsl:otherwise>
     <xsl:if test="not(preceding-sibling::node())"><xsl:apply-templates select="ancestor-or-self::node()[@space-before][self::node()[$akt=descendant::node()[1]]][1]/@space-before"/></xsl:if>
    </xsl:otherwise>
   </xsl:choose>
   <xsl:apply-templates select="@break-before"/><xsl:apply-templates select="preceding-sibling::node()[1][@break-after]/@break-after"/>
   <xsl:apply-templates select="preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
   <xsl:apply-templates select="ancestor::node()[$akt[(not(preceding-sibling::node())) and parent::node()[not(preceding-sibling::node()) or preceding-sibling::node()/@break-after]]]/preceding-sibling::node()[1][@break-after]/@break-after"/>
   <xsl:if test="ancestor::fo:list-item-body">
     <w:listPr>
      <w:ilvl w:val="{count(ancestor::fo:list-item-body)-1}"/>
      <w:ilfo w:val="{count(preceding::fo:list-block)+1}"/>
     </w:listPr>
   </xsl:if>
  </w:pPr>
 </xsl:if>
 <xsl:if test="@id">
  <aml:annotation aml:id="{translate(@id,' ','-')}" w:type="Word.Bookmark.Start" w:name="{translate(@id,' ','-')}"/>
  <aml:annotation aml:id="{translate(@id,' ','-')}" w:type="Word.Bookmark.End"/>
 </xsl:if>
 <xsl:apply-templates select="fo:inline|fo:block|fo:page-number|fo:leader|fo:table|fo:table-and-caption|fo:basic-link|fo:page-number-citation|fo:retrieve-marker|fo:marker|fo:list-block"/> <!-- Rekurzivní volání dalších elementů potomků -->
 <xsl:if test="node()[last()]=text()">
  <xsl:if test="name(node()[last()-1])='fo:block' or name(node()[last()-1])='fo:table' or name(node()[last()-1])='fo:table-and-caption' or name(node()[last()-1])='fo:list-block'">
   <xsl:text disable-output-escaping="yes">&lt;w:p&gt;</xsl:text>
   <w:pPr>
    <xsl:variable name="akt" select="."/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-align][1]/@text-align"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-indent][1]/@text-indent"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@start-indent][1]/@start-indent"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@end-indent][1]/@end-indent"/>
    <xsl:apply-templates select="@space-before"/>
    <xsl:choose>
     <xsl:when test="not(following-sibling::node())"><xsl:apply-templates select="ancestor-or-self::node()[@space-after][self::node()[$akt=descendant::node()[last()]]][1]/@space-after"/></xsl:when>
     <xsl:otherwise><xsl:apply-templates select="@space-after"/></xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="ancestor-or-self::node()[@line-height][1]/@line-height"/>
    <xsl:apply-templates select="@background-color"/><xsl:apply-templates select="@border-top-style"/><xsl:apply-templates select="@border-before-style"/>
    <xsl:apply-templates select="@border-bottom-style"/><xsl:apply-templates select="@border-after-style"/>
    <xsl:apply-templates select="@border-left-style"/><xsl:apply-templates select="@border-start-style"/>
    <xsl:apply-templates select="@border-right-style"/><xsl:apply-templates select="@border-end-style"/><xsl:apply-templates select="@border-style"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@orphans][1]/@orphans"/><xsl:apply-templates select="ancestor-or-self::node()[@widows][1]/@widows"/>
    <xsl:apply-templates select="@break-before"/><xsl:apply-templates select="preceding-sibling::node()[1][@break-after]/@break-after"/>
    <xsl:apply-templates select="preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@keep-together][1]/@keep-together"/>
    <xsl:apply-templates select="@keep-with-next"/><xsl:apply-templates select="following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="following-sibling::node()[1]/descendant::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="ancestor::node()[not(child::text())]/following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:if test="not(following-sibling::node()) and not(ancestor::fo:block/following-sibling::node()) and (ancestor::fo:flow) and (ancestor::fo:page-sequence/following-sibling::node())">
     <xsl:apply-templates select="ancestor::fo:page-sequence"/>
    </xsl:if>
    <xsl:if test="ancestor::fo:list-item-body">
     <w:listPr>
      <w:ilvl w:val="{count(ancestor::fo:list-item-body)-1}"/>
      <w:ilfo w:val="{count(preceding::fo:list-block)+1}"/>
     </w:listPr>
    </xsl:if>
   </w:pPr>
  </xsl:if>
  <w:r>
   <w:rPr>
    <xsl:apply-templates select="ancestor-or-self::node()[@color][1]/@color"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-family][1]/@font-family"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-size][1]/@font-size"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-weight][1]/@font-weight"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-style][1]/@font-style"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-stretch][1]/@font-stretch"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@font-variant][1]/@font-variant"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-transform][1]/@text-transform"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-decoration][1]/@text-decoration"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@text-shadow][1]/@text-shadow"/>
    <xsl:apply-templates select="ancestor-or-self::node()[@letter-spacing][1]/@letter-spacing"/>
    <xsl:apply-templates select="@vertical-align"/>
   </w:rPr>
   <w:t>
    <xsl:if test="name(node()[last()-1])!='fo:block' and substring(text()[last()],1,1)=' '">
     <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:value-of select="normalize-space(text()[last()])"/>
   </w:t>
  </w:r>
  <xsl:if test="name(node()[last()-1])='fo:block' or name(node()[last()-1])='fo:table' or name(node()[last()-1])='fo:table-and-caption' or name(node()[last()-1])='fo:list-block'">
   <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text>
  </xsl:if>
 </xsl:if>
 <xsl:if test="(node()[1]=text() or fo:inline[1] or fo:page-number[1] or fo:leader[1] or fo:basic-link[1] or fo:page-number-citation[1] or fo:retrieve-marker[1] or fo:marker[1]) and not(fo:block[last()])">
   <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text>
 </xsl:if>
</xsl:template>

<xsl:template match="@color"> <!-- Šablony pro textové vlastnosti -->
 <w:color>
  <xsl:attribute name="w:val">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
  </xsl:attribute>
 </w:color>
</xsl:template>

<xsl:template match="@font-family">
 <w:rFonts w:ascii="{.}" w:h-ansi="{.}"/>
</xsl:template>

<xsl:template match="@font-size">
 <w:sz>
  <xsl:choose>
   <xsl:when test=".='xx-small'"><xsl:attribute name="w:val">14</xsl:attribute></xsl:when>
   <xsl:when test=".='x-small'"><xsl:attribute name="w:val">16</xsl:attribute></xsl:when>
   <xsl:when test=".='small'"><xsl:attribute name="w:val">20</xsl:attribute></xsl:when>
   <xsl:when test=".='large'"><xsl:attribute name="w:val">28</xsl:attribute></xsl:when>
   <xsl:when test=".='x-large'"><xsl:attribute name="w:val">34</xsl:attribute></xsl:when>
   <xsl:when test=".='xx-large'"><xsl:attribute name="w:val">42</xsl:attribute></xsl:when>
   <xsl:otherwise>
    <xsl:attribute name="w:val">
     <xsl:call-template name="prevodpulbod"/>
    </xsl:attribute>
   </xsl:otherwise>
  </xsl:choose>
 </w:sz>
</xsl:template>

<xsl:template match="@font-weight">
 <xsl:if test=".='bold'or .='bolder' or number(.)&gt;400"><w:b w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@font-style">
 <xsl:if test=".!='normal'"><w:i w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@font-stretch">
<xsl:choose>
 <xsl:when test=".='ultra-condensed'"><w:w w:val="45"/></xsl:when>
 <xsl:when test=".='extra-condensed'"><w:w w:val="57"/></xsl:when>
 <xsl:when test=".='condensed'"><w:w w:val="69"/></xsl:when>
 <xsl:when test=".='semi-condensed'"><w:w w:val="81"/></xsl:when>
 <xsl:when test=".='semi-expanded'"><w:w w:val="120"/></xsl:when>
 <xsl:when test=".='expanded'"><w:w w:val="140"/></xsl:when>
 <xsl:when test=".='extra-expanded'"><w:w w:val="160"/></xsl:when>
 <xsl:when test=".='ultra-expanded'"><w:w w:val="200"/></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template match="@font-variant">
 <xsl:if test=".='small-caps'"><w:smallCaps w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@text-transform">
 <xsl:if test=".='uppercase'"><w:caps w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@text-decoration">
 <xsl:if test=".='line-through'"><w:strike w:val="on"/></xsl:if>
 <xsl:if test=".='underline'">
  <w:u>
   <xsl:attribute name="w:val"><xsl:apply-templates select="../ancestor-or-self::node()[@score-spaces][1]/@score-spaces"/>
     <xsl:if test="not(../ancestor-or-self::node()[@score-spaces])">single</xsl:if></xsl:attribute>
   <xsl:attribute name="w:color">auto</xsl:attribute>
  </w:u>
 </xsl:if>
</xsl:template>

<xsl:template match="@score-spaces">
 <xsl:if test=".='false'">words</xsl:if>
 <xsl:if test=".='true'">single</xsl:if>
</xsl:template>

<xsl:template match="@text-shadow">
 <xsl:if test=".!='none'"><w:shadow w:val="on"/></xsl:if>
</xsl:template>

<xsl:template match="@letter-spacing">
 <w:spacing>
  <xsl:attribute name="w:val">
   <xsl:call-template name="prevodtwips"/>
  </xsl:attribute>
 </w:spacing>
</xsl:template>

<xsl:template match="@vertical-align">
 <xsl:variable name="font">
  <xsl:choose>
   <xsl:when test="../ancestor::node()[1][@font-size]/@font-size"> 
    <xsl:value-of select="../ancestor::node()[@font-size][1]/@font-size"/>
   </xsl:when> 
   <xsl:otherwise>
    <xsl:value-of select="concat('12','pt')"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <w:position>
  <xsl:attribute name="w:val">
   <xsl:choose>
    <xsl:when test="contains(.,'pt') or contains(.,'in') or contains(.,'mm') or contains(.,'cm') or contains(.,'pc')">
     <xsl:call-template name="prevodpulbod"/>
    </xsl:when>
    <xsl:when test=".='sub'"><xsl:call-template name="prevodpulbodp">
     <xsl:with-param name="h" select="$font"/><xsl:with-param name="uprava" select="-0.4"/></xsl:call-template></xsl:when>
    <xsl:when test=".='super'"><xsl:call-template name="prevodpulbodp">
     <xsl:with-param name="h" select="$font"/><xsl:with-param name="uprava" select="0.8"/></xsl:call-template></xsl:when>
    <xsl:when test="contains(.,'%')"><xsl:call-template name="prevodpulbodp">
     <xsl:with-param name="h" select="$font"/><xsl:with-param name="uprava" select="number(substring-before(.,'%')) div 100"/>
     </xsl:call-template></xsl:when>
    <xsl:otherwise>0</xsl:otherwise>
   </xsl:choose>
  </xsl:attribute>
 </w:position>
</xsl:template>

<xsl:template match="@border-style" mode="inline">
<xsl:variable name="val">
  <xsl:call-template name="stylhranice"><xsl:with-param name="hodnota" select="."/></xsl:call-template>
 </xsl:variable>
 <xsl:variable name="color">
  <xsl:if test="../@border-color">
   <xsl:call-template name="barva"><xsl:with-param name="hodnota" select="../@border-color"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../@border-color)"><xsl:value-of select="'auto'"/></xsl:if>
 </xsl:variable>
 <xsl:variable name="sz">
  <xsl:if test="../@border-width">
   <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="../@border-width"/></xsl:call-template>
  </xsl:if>
  <xsl:if test="not(../@border-width)">
   <xsl:call-template name="sirkahranice"><xsl:with-param name="hodnota" select="'nil'"/></xsl:call-template>
  </xsl:if>
 </xsl:variable>
 <xsl:variable name="space">
  <xsl:choose>
   <xsl:when test="contains(../@padding,'pt') or contains(../@padding,'in') or contains(../@padding,'mm') or contains(../@padding,'cm') or contains(../@padding,'pc')">
    <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="../@padding"/><xsl:with-param name="u" select="8"/></xsl:call-template>
   </xsl:when>
   <xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
<w:bdr w:val="{$val}" w:sz="{$sz}" w:space="{$space}" w:color="{$color}"/>
</xsl:template>

<xsl:template name="prevodtwips">
<xsl:choose>
 <xsl:when test="contains(.,'pt')"><xsl:call-template name="znamenko"><xsl:with-param name="twip" select="number(substring-before(.,'pt'))*20"/></xsl:call-template></xsl:when>
 <xsl:when test="contains(.,'in')"><xsl:call-template name="znamenko"><xsl:with-param name="twip" select="number(substring-before(.,'in'))*1440"/></xsl:call-template></xsl:when>
 <xsl:when test="contains(.,'mm')"><xsl:call-template name="znamenko"><xsl:with-param name="twip" select="number(substring-before(.,'mm'))*1440 div 25.4"/></xsl:call-template></xsl:when>
 <xsl:when test="contains(.,'cm')"><xsl:call-template name="znamenko"><xsl:with-param name="twip" select="number(substring-before(.,'cm'))*1440 div 2.54"/></xsl:call-template></xsl:when>
 <xsl:when test="contains(.,'pc')"><xsl:call-template name="znamenko"><xsl:with-param name="twip" select="number(substring-before(.,'pc'))*20 div 12"/></xsl:call-template></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="znamenko">
 <xsl:param name="twip"/>
 <xsl:if test="name(.)='text-indent' and substring(.,1,1)='-'"><xsl:value-of select="substring-after(string($twip),'-')"/></xsl:if>
 <xsl:if test="name(.)!='text-indent' or substring(.,1,1)!='-'"><xsl:value-of select="$twip"/></xsl:if>
</xsl:template>

<xsl:template name="prevodtwipsp">
<xsl:param name="h"/>
<xsl:choose>
 <xsl:when test="contains($h,'pt')"><xsl:value-of select="number(substring-before($h,'pt'))*20"/></xsl:when>
 <xsl:when test="contains($h,'in')"><xsl:value-of select="number(substring-before($h,'in'))*1440"/></xsl:when>
 <xsl:when test="contains($h,'mm')"><xsl:value-of select="number(substring-before($h,'mm'))*1440 div 25.4"/></xsl:when>
 <xsl:when test="contains($h,'cm')"><xsl:value-of select="number(substring-before($h,'cm'))*1440 div 2.54"/></xsl:when>
 <xsl:when test="contains($h,'pc')"><xsl:value-of select="number(substring-before($h,'pc'))*20 div 12"/></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="prevodpulbod">
<xsl:choose>
 <xsl:when test="contains(.,'pt')"><xsl:value-of select="number(substring-before(.,'pt'))*2"/></xsl:when>
 <xsl:when test="contains(.,'in')"><xsl:value-of select="number(substring-before(.,'in'))*144"/></xsl:when>
 <xsl:when test="contains(.,'mm')"><xsl:value-of select="number(substring-before(.,'mm'))*144 div 25.4"/></xsl:when>
 <xsl:when test="contains(.,'cm')"><xsl:value-of select="number(substring-before(.,'cm'))*144 div 2.54"/></xsl:when>
 <xsl:when test="contains(.,'pc')"><xsl:value-of select="number(substring-before(.,'pc'))*2 div 12"/></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="prevodpulbodp">
<xsl:param name="h"/><xsl:param name="uprava"/>
<xsl:choose>
 <xsl:when test="contains($h,'pt')"><xsl:value-of select="number(substring-before($h,'pt'))*2*$uprava"/></xsl:when>
 <xsl:when test="contains($h,'in')"><xsl:value-of select="number(substring-before($h,'in'))*144*$uprava"/></xsl:when>
 <xsl:when test="contains($h,'mm')"><xsl:value-of select="number(substring-before($h,'mm'))*144 div 25.4*$uprava"/></xsl:when>
 <xsl:when test="contains($h,'cm')"><xsl:value-of select="number(substring-before($h,'cm'))*144 div 2.54*$uprava"/></xsl:when>
 <xsl:when test="contains($h,'pc')"><xsl:value-of select="number(substring-before($h,'pc'))*2 div 12*$uprava"/></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="prevodosminabod">
<xsl:param name="h"/><xsl:param name="u"/>
<xsl:choose>
 <xsl:when test="contains($h,'pt')"><xsl:value-of select="number(substring-before($h,'pt'))*8 div $u"/></xsl:when>
 <xsl:when test="contains($h,'in')"><xsl:value-of select="number(substring-before($h,'in'))*576 div $u"/></xsl:when>
 <xsl:when test="contains($h,'mm')"><xsl:value-of select="number(substring-before($h,'mm'))*576 div 25.4 div $u"/></xsl:when>
 <xsl:when test="contains($h,'cm')"><xsl:value-of select="number(substring-before($h,'cm'))*576 div 2.54 div $u"/></xsl:when>
 <xsl:when test="contains($h,'pc')"><xsl:value-of select="number(substring-before($h,'pc'))*8 div 12 div $u"/></xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="barva">
<xsl:param name="hodnota"/>
<xsl:choose>
 <xsl:when test="$hodnota='black'"><xsl:value-of select="'000000'"/></xsl:when>
 <xsl:when test="$hodnota='silver'"><xsl:value-of select="'C0C0C0'"/></xsl:when>
 <xsl:when test="$hodnota='gray'"><xsl:value-of select="'808080'"/></xsl:when>
 <xsl:when test="$hodnota='white'"><xsl:value-of select="'ffffff'"/></xsl:when>
 <xsl:when test="$hodnota='maroon'"><xsl:value-of select="'800000'"/></xsl:when>
 <xsl:when test="$hodnota='red'"><xsl:value-of select="'ff0000'"/></xsl:when>
 <xsl:when test="$hodnota='purple'"><xsl:value-of select="'800080'"/></xsl:when>
 <xsl:when test="$hodnota='fuchsia'"><xsl:value-of select="'ff00ff'"/></xsl:when>
 <xsl:when test="$hodnota='green'"><xsl:value-of select="'008000'"/></xsl:when>
 <xsl:when test="$hodnota='lime'"><xsl:value-of select="'00ff00'"/></xsl:when>
 <xsl:when test="$hodnota='olive'"><xsl:value-of select="'808000'"/></xsl:when>
 <xsl:when test="$hodnota='yellow'"><xsl:value-of select="'ffff00'"/></xsl:when>
 <xsl:when test="$hodnota='navy'"><xsl:value-of select="'000080'"/></xsl:when>
 <xsl:when test="$hodnota='blue'"><xsl:value-of select="'0000ff'"/></xsl:when>
 <xsl:when test="$hodnota='teal'"><xsl:value-of select="'008080'"/></xsl:when>
 <xsl:when test="$hodnota='aqua'"><xsl:value-of select="'00ffff'"/></xsl:when>
 <xsl:otherwise><xsl:value-of select="substring-after($hodnota,'#')"/></xsl:otherwise>
</xsl:choose> 
</xsl:template>

<xsl:template name="stylhranice">
<xsl:param name="hodnota"/>
<xsl:choose>
 <xsl:when test="$hodnota='hidden'"><xsl:value-of select="'nil'"/></xsl:when>
 <xsl:when test="$hodnota='solid'"><xsl:value-of select="'single'"/></xsl:when>
 <xsl:when test="$hodnota='groove'"><xsl:value-of select="'three-d-engrave'"/></xsl:when>
 <xsl:when test="$hodnota='ridge'"><xsl:value-of select="'three-d-emboss'"/></xsl:when>
 <xsl:otherwise><xsl:value-of select="$hodnota"/></xsl:otherwise>
</xsl:choose> 
</xsl:template>

<xsl:template name="sirkahranice">
<xsl:param name="hodnota"/>
<xsl:choose>
 <xsl:when test="$hodnota='thin'"><xsl:value-of select="'4'"/></xsl:when>
 <xsl:when test="$hodnota='medium'"><xsl:value-of select="'12'"/></xsl:when>
 <xsl:when test="$hodnota='thick'"><xsl:value-of select="'40'"/></xsl:when>
 <xsl:when test="contains($hodnota,'pt') or contains($hodnota,'in') or contains($hodnota,'mm') or contains($hodnota,'cm') or contains($hodnota,'pc')">
  <xsl:call-template name="prevodosminabod"><xsl:with-param name="h" select="$hodnota"/><xsl:with-param name="u" select="1"/></xsl:call-template>
 </xsl:when>
 <xsl:otherwise><xsl:value-of select="'12'"/></xsl:otherwise>
</xsl:choose> 
</xsl:template>

<xsl:template name="predchozitext">
 <xsl:if test="preceding-sibling::node()[1][self::text()]"> <!-- Jestliže předcházejícím uzlem je text, musíme ho zkopírovat na výstup -->
  <w:r>
    <w:rPr>
     <xsl:apply-templates select="ancestor::node()[@color][1]/@color"/>
     <xsl:apply-templates select="ancestor::node()[@font-family][1]/@font-family"/>
     <xsl:apply-templates select="ancestor::node()[@font-size][1]/@font-size"/>
     <xsl:apply-templates select="ancestor::node()[@font-weight][1]/@font-weight"/>
     <xsl:apply-templates select="ancestor::node()[@font-style][1]/@font-style"/>
     <xsl:apply-templates select="ancestor::node()[@font-stretch][1]/@font-stretch"/>
     <xsl:apply-templates select="ancestor::node()[@font-variant][1]/@font-variant"/>
     <xsl:apply-templates select="ancestor::node()[@text-transform][1]/@text-transform"/>
     <xsl:apply-templates select="ancestor::node()[@text-decoration][1]/@text-decoration"/>
     <xsl:apply-templates select="ancestor::node()[@text-shadow][1]/@text-shadow"/>
     <xsl:apply-templates select="ancestor::node()[@letter-spacing][1]/@letter-spacing"/>
     <xsl:apply-templates select="parent::node()[@vertical-align]/@vertical-align"/>
     <xsl:apply-templates select="parent::node()[@background-color]/@background-color"/>
     <xsl:apply-templates select="parent::node()[name(.)='fo:inline']/@border-style" mode="inline"/>
    </w:rPr>
    <w:t>
     <xsl:if test="substring(preceding-sibling::text()[1],1,1)=' '">
      <xsl:text> </xsl:text>
     </xsl:if>
     <xsl:value-of select="normalize-space(preceding-sibling::text()[1])"/>
     <xsl:if test="substring(preceding-sibling::text()[1],string-length(preceding-sibling::text()[1]),1)=' '">
      <xsl:text> </xsl:text>
     </xsl:if>
    </w:t>
   </w:r> 
 </xsl:if>
</xsl:template>

<xsl:template name="vlastnostiip">
 <w:rPr>
  <xsl:apply-templates select="ancestor-or-self::node()[@color][1]/@color"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-family][1]/@font-family"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-size][1]/@font-size"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-weight][1]/@font-weight"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-style][1]/@font-style"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-stretch][1]/@font-stretch"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@font-variant][1]/@font-variant"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@text-transform][1]/@text-transform"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@text-decoration][1]/@text-decoration"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@text-shadow][1]/@text-shadow"/>
  <xsl:apply-templates select="ancestor-or-self::node()[@letter-spacing][1]/@letter-spacing"/>
  <xsl:apply-templates select="@vertical-align"/><xsl:apply-templates select="@background-color"/>
  <xsl:apply-templates select="@border-style" mode="inline"/>
 </w:rPr>
</xsl:template>

<xsl:template name="predchoziodstavec">
 <xsl:if test="preceding-sibling::node()[1][self::text()]">
  <xsl:if test="name(preceding-sibling::node()[2])='fo:block' or name(preceding-sibling::node()[2])='fo:table' or name(preceding-sibling::node()[2])='fo:table-and-caption' or name(preceding-sibling::node()[2])='fo:list-block'"> <!-- Začátek odstavce, pokud je před textovým elementem element fo:block -->
   <xsl:text disable-output-escaping="yes">&lt;w:p&gt;</xsl:text>
   <w:pPr>
    <xsl:apply-templates select="ancestor::node()[@text-align][1]/@text-align"/>
    <xsl:apply-templates select="ancestor::node()[@text-indent][1]/@text-indent"/>
    <xsl:apply-templates select="ancestor::node()[@start-indent][1]/@start-indent"/>
    <xsl:apply-templates select="ancestor::node()[@end-indent][1]/@end-indent"/>
    <xsl:apply-templates select="../@space-before"/><xsl:apply-templates select="../@space-after"/>
    <xsl:apply-templates select="ancestor::node()[@line-height][1]/@line-height"/>
    <xsl:apply-templates select="../@background-color"/><xsl:apply-templates select="../@border-top-style"/><xsl:apply-templates select="../@border-before-style"/>
    <xsl:apply-templates select="../@border-bottom-style"/><xsl:apply-templates select="../@border-after-style"/>
    <xsl:apply-templates select="../@border-left-style"/><xsl:apply-templates select="../@border-start-style"/>
    <xsl:apply-templates select="../@border-right-style"/><xsl:apply-templates select="../@border-end-style"/><xsl:apply-templates select="../@border-style"/>
    <xsl:apply-templates select="ancestor::node()[@orphans][1]/@orphans"/><xsl:apply-templates select="ancestor::node()[@widows][1]/@widows"/>
    <xsl:apply-templates select="../@break-before"/><xsl:apply-templates select="../preceding-sibling::node()[1][@break-after]/@break-after"/>
    <xsl:apply-templates select="../preceding-sibling::node()[1]/descendant::node()[not(self::text())][last()][@break-after]/@break-after"/>
    <xsl:apply-templates select="ancestor::node()[@keep-together][1]/@keep-together"/>
    <xsl:apply-templates select="../@keep-with-next"/><xsl:apply-templates select="../following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="../following-sibling::node()[1]/descendant::node()[1][@keep-with-previous]/@keep-with-previous"/>
    <xsl:apply-templates select="../ancestor::node()[not(child::text())]/following-sibling::node()[1][@keep-with-previous]/@keep-with-previous"/>
   </w:pPr>
  </xsl:if>
  <w:r>
    <w:rPr>
     <xsl:apply-templates select="ancestor::node()[@color][1]/@color"/>
     <xsl:apply-templates select="ancestor::node()[@font-family][1]/@font-family"/>
     <xsl:apply-templates select="ancestor::node()[@font-size][1]/@font-size"/>
     <xsl:apply-templates select="ancestor::node()[@font-weight][1]/@font-weight"/>
     <xsl:apply-templates select="ancestor::node()[@font-style][1]/@font-style"/>
     <xsl:apply-templates select="ancestor::node()[@font-stretch][1]/@font-stretch"/>
     <xsl:apply-templates select="ancestor::node()[@font-variant][1]/@font-variant"/>
     <xsl:apply-templates select="ancestor::node()[@text-transform][1]/@text-transform"/>
     <xsl:apply-templates select="ancestor::node()[@text-decoration][1]/@text-decoration"/>
     <xsl:apply-templates select="ancestor::node()[@text-shadow][1]/@text-shadow"/>
     <xsl:apply-templates select="ancestor::node()[@letter-spacing][1]/@letter-spacing"/>
     <xsl:apply-templates select="../@vertical-align"/>
    </w:rPr>
    <w:t>
     <xsl:if test="substring(preceding-sibling::text()[1],1,1)=' '">
      <xsl:text> </xsl:text>
     </xsl:if>
     <xsl:value-of select="normalize-space(preceding-sibling::text()[1])"/>
    </w:t>
  </w:r>
  <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text> <!-- Konec odstavce, pokud je předcházejícím elementem text -->
 </xsl:if>
 <xsl:if test="name(preceding-sibling::node()[1])='fo:inline' or name(preceding-sibling::node()[1])='fo:page-number' or
        name(preceding-sibling::node()[1])='fo:leader' or name(preceding-sibling::node()[1])='fo:basic-link' or name(preceding-sibling::node()[1])='fo:page-number-citation'
        or name(preceding-sibling::node()[1])='fo:retrieve-marker' or name(preceding-sibling::node()[1])='fo:marker'"> <!-- Konec odstavce, pokud je předcházejícím elementem fo:inline -->
  <xsl:text disable-output-escaping="yes">&lt;/w:p&gt;</xsl:text>
 </xsl:if>
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2007. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->