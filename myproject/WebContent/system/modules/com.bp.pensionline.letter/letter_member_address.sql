  select 
    CASE WHEN  UPPER(trim(country)) in ('ENGLAND','UK','UNITED KINGDOM','U.K','SCOTLAND', 'WALES', 'NOTHERN IRELAND','GRB')
	then replace( 
      trim(trailing chr(13) from 
        replace( 
          replace(initcap(address1) || chr(13) 
            || initcap(address2) || chr(13) 
            || initcap(address3) || chr(13) 
            || initcap(address4) || chr(13) 
            || initcap(address5) || chr(13) 
            || postcode || chr(13), chr(13)||chr(13)||chr(13), chr(13)), 
          chr(13)||chr(13), chr(13))), 
        chr(13), '&lt;br/&gt;')
	ELSE replace( 
        trim(trailing chr(13) from 
          replace( 
            replace(initcap(address1) || chr(13) 
              || initcap(address2) || chr(13) 
              || initcap(address3) || chr(13) 
              || initcap(address4) || chr(13) 
              || initcap(address5) || chr(13) 
              || postcode || chr(13) 
              || country, chr(13)||chr(13)||chr(13), chr(13)), 
              chr(13)||chr(13), chr(13))), 
          chr(13), '&lt;br/&gt;') 	
	END  												
	as "Address",
    replace(substr(ad.postcode, 0, 4), ' ', '') as "Postcode"
  from 
    basic b 
    ,PS_ADDRESSDETAILS ad 
    ,PS_ADDRESS psad 
  where 
    b.bgroup= :bgroup 
    and b.refno= :refno
    and psad.bgroup= b.bgroup 
    and psad.refno= b.refno 
    and psad.addcode='GENERAL' 
    and psad.endd is null 
    and ad.bgroup = b.bgroup 
    and ad.addno= psad.ADDNO