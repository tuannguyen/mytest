select 
  b.bgroup as "Bgroup"
  ,b.refno as "Refno"
  ,b.BD08X as "Nino"
  ,nvl(b.BD04X, 'No information available') as "EmployeeNumber"
  ,b.BD19A as "MembershipStatusRaw"
  ,sd.SD02X as "SchemeName"
  ,initcap(b.BD07A || ' ' || b.BD05A || ' ' || b.BD38A) as "Name"
  ,initcap(b.BD05A) as "FirstName"
  ,b.BD07A as "Title"
  ,b.BD24P as "BD24P"
  ,initcap(b.BD38A) as "Surname"
from 
  basic b
  ,SCHEME_DETAIL sd
  ,CATEGORY_DETAIL cd
  ,MP_ADDCOMM mpac 
  ,sd_domain_list sddl 
where 
  b.bgroup= :bgroup
  and b.refno= :refno
  and cd.bgroup= b.bgroup
  and cd.ca26x= b.ca26x
  and sd.bgroup= b.bgroup
  and sd.sd01x= b.sd01x
  and sddl.domain = 'MAR01'
  and sddl.bgroup = b.bgroup
  and sddl.dol_listval = nvl(b.BD10A, 'U')
  and mpac.bgroup (+) = b.bgroup  
  and mpac.refno  (+) = b.refno  
  and mpac.mpam01x (+) = 'GENERAL'