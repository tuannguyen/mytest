select 'BP' as "Company", bgroup || '-' || sd01x as "SchemeId"
from basic b
where b.bgroup = 'BPF' 
  and b.bgroup = :bgroup
  and b.refno = :refno
  and b.sd01x = '0001'
union
select 'Subs' as "Company", bgroup || '-' || sd01x as "SchemeId"
from basic b
where b.bgroup = 'BPF' 
  and b.bgroup = :bgroup
  and b.refno = :refno
  and b.sd01x != '0001'
union
select 'BC' as "Company", bgroup || '-' || sd01x as "SchemeId"
from basic b
where b.bgroup = 'BCF' 
  and b.bgroup = :bgroup
  and b.refno = :refno
