select 
  decode(nvl(state_benefits.ASD71X, 'Y'), 'Y', 'false', 'N', 'true')  
     as "StateBenefitsAvailable"

  ,substr(state_benefits.ASD70X, 1, 2) 
     || ' years and ' 
     || to_number(substr(state_benefits.ASD70X, 3, 2)) 
     || ' months'
     as "StatePensionAge"

  ,chr(163)
     || trim(both ' ' from to_char(nvl(state_benefits.ASD71R, 0),'9,999,999')) 
     || ' a year' 
     as "CurrentBasicStatePension"

  ,chr(163) 
     || trim(both ' ' from to_char(nvl(state_benefits.ASD72R, 0),'9,999,999')) 
     || ' a year' 
     as "CurrentAdditionalStatePension"

  ,chr(163) 
     || trim(both ' ' from to_char(nvl(state_benefits.ASD70R, 0),'9,999,999')) 
     || ' a year' 
     as "CurrentTotalStatePension"

  ,chr(163) 
     || trim(both ' ' from to_char(nvl(state_benefits.ASD74R, 0),'9,999,999')) 
     || ' a year' 
     as "FutureBasicStatePension"

  ,chr(163) 
     || trim(both ' ' from to_char(nvl(state_benefits.ASD75R, 0),'9,999,999')) 
     || ' a year' 
     as "FutureAdditionalStatePension"

  ,chr(163) 
     || trim(both ' ' from to_char(nvl(state_benefits.ASD73R, 0),'9,999,999')) 
     || ' a year' 
     as "FutureTotalStatePension"

from 
  basic
  ,add_static_data state_benefits

where 
  basic.bgroup                       = :bgroup
  and basic.refno                    = :refno
  and state_benefits.bgroup      (+) = basic.bgroup
  and state_benefits.REFNO       (+) = basic.refno
  and state_benefits.DATA_TYPE   (+) = 'STATBEN'
