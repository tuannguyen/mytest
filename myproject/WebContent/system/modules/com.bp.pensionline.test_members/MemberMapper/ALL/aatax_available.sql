select * from ( 
                select decode(nvl(AHD01X, 'false'),'','false','true') as "AaDataAvailable" 
                from add_hist_data 
                where 
                                bgroup = :BGROUP 
                                and refno = :REFNO 
                                and data_type = 'AADATA2' 
                union 
                select decode(num,0,'false','true') as "AaDataAvailable" 
                from ( 
                                select count(*) as num 
                                from add_hist_data 
                                where bgroup = :BGROUP 
                                                and refno = :REFNO 
                                                and data_type = 'AADATA2'
                                ) 
                ) 
where rownum = 1
;
