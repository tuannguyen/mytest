SELECT nvl(pmd.BKANO, '') as "BankAccountNumber"
  ,decode(pmd.BKSCODE, null, '[Not specified]'
    , substr(pmd.BKSCODE,1,2)|| '-' || substr(pmd.BKSCODE,3,2)|| '-' || substr(pmd.BKSCODE,5,2)
    ) as "SortCode"
  ,nvl(pmd.BSROLLNO, '') as "RollNumber"
  ,nvl(initcap(pmd.BKANAME), '[Not specified]') as "BankAccountName"
  ,nvl(initcap(bank.bkname), '[Not specified]') as "BankName"
FROM 
  basic b
  ,PS_PMDETAILS pmd
  ,PS_PM pm
  ,PS_UKBANK bank
WHERE 
  b.bgroup= :bgroup
  AND b.refno= :refno
  AND pm.BGROUP    (+) = b.bgroup
  AND pm.REFNO     (+) = b.refno
  AND pmd.PMDNO    (+) = pm.PMDNO 
  AND pmd.BGROUP   (+) = pm.BGROUP
  AND bank.bkscode (+) = pmd.BKSCODE
