select 
  decode(
    nvl(wd.WD05X, 'N')
      , 'Y', 'true'
      , 'N', 'false'
    ) as "PloDetailsAvailable" 
  ,nvl(wd.wd01x, 'AR37') as "PloArea"
  ,mp.mpfm15x as "PloName"
  ,replace(
    trim(trailing chr(13) from 
      replace(
        replace(MPFM03X || chr(13)
          || MPFM04X || chr(13)
          || MPFM05X || chr(13)
          || MPFM06X || chr(13)
          || MPFM07X || chr(13)
          || MPFM11X || chr(13)
          || MPFM13X, chr(13)||chr(13)||chr(13), chr(13)), 
        chr(13)||chr(13), chr(13))), 
      chr(13), '&lt;br/&gt;') as "PloAddress"
    ,mp.MPFM08X as "PloTelephone"
from 
  basic b
  ,welfare_detail wd
  ,mp_manager mp
where 
  b.bgroup= :bgroup
  and b.refno= :refno
  and wd.bgroup (+) = b.bgroup
  and wd.refno  (+) = b.refno
  and mp.bgroup     = b.bgroup
  and mp.manager    = nvl(wd.wd01x, 'AR37')
