select tvin.tvindays as "TvinBenefit"
  ,heritage.tvindays as "HeritageBenefit"
  ,sqs.days as "AugmentedBenefit"
  ,esb.days as "EasternServiceBenefit"
from 
  (select max(tvindays) as tvindays from (
  (select 
    REGEXP_REPLACE(
      nvl(floor(sum(nvl(ti.ti22i,0))/365), 0) 
      || ' years ' 
      || nvl(sum(ti.ti22i) - (floor(sum(ti.ti22i)/365) * 365),0)
      || ' days', '^0 years ') 
      as tvindays
    from transfer_in ti
    where bgroup= :bgroup
      and refno= :refno
      and ti.SUB in ('0010','0011','0012','0014','0015'))
union      
      (select (ASD01X || ' years') as tvindays from add_static_data where data_type = 'SCHBEN' and bgroup = :bgroup and refno = :refno))
  ) tvin
  ,(select 
    REGEXP_REPLACE(
      nvl(floor(sum(nvl(ben.days,0))/365), 0) 
      || ' years ' 
      || nvl(sum(ben.days) - (floor(sum(ben.days)/365) * 365),0) 
      || ' days', '^0 years ') 
      as tvindays
    from ( select sum(nvl(ti.ti22i,0)) as days
      from transfer_in ti
      where bgroup= :bgroup
        and refno= :refno
        and ti.SUB in ('0022','0023')
      union select sum(nvl(ab.ab06i,0))  as days
      from augmentation_benefit ab
      where bgroup= :bgroup
        and refno= :refno
        and ab.SUB in ('OLDN','NEWE','NEWF','NEWK','NEWL')
    ) ben
  ) heritage
  ,( select 
    REGEXP_REPLACE(
      nvl(floor(sum(nvl(ab.ab06i,0))/365), 0) 
      || ' years ' 
      || nvl(sum(ab.ab06i) - (floor(sum(ab.ab06i)/365) * 365),0) 
      || ' days', '^0 years ') 
      as days
    from augmentation_benefit ab
    where bgroup= :bgroup
      and refno= :refno 
      and ab.SUB in ('OLDA','OLDB','OLDG')
  ) sqs
  ,( select 
    REGEXP_REPLACE(
      nvl(floor(sum(nvl(ab.ab06i,0))/365), 0) 
      || ' years ' 
      || nvl(sum(ab.ab06i) - (floor(sum(ab.ab06i)/365) * 365),0) 
      || ' days', '^0 years ') 
      as days
    from augmentation_benefit ab
    where bgroup= :bgroup
      and refno= :refno
      and ab.SUB in ('OLDM','OLDH')
  ) esb
;