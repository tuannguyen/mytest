select 
a.ASD01X as "SPipYear",
a.ASD02X as "SPipTaxYear",
chr(163) || trim(to_char(a.ASD01R, '999,999,999')) as "SPipEndSalary",
chr(163) || trim(to_char(a.ASD02R, '999,999,999')) as "SPipEndBenefit",
chr(163) || trim(to_char(a.ASD03R, '999,999,999')) as "SPipYearAvcs",
chr(163) || trim(to_char(a.ASD04R, '999,999,999')) as "SAaSavings",
chr(163) || trim(to_char(a.ASD05R, '999,999,999')) as "SAaLimit",
a.ASD04X as "SPipYear1",
a.ASD05X as "SPipTaxYear1",
chr(163) || trim(to_char(a.ASD07R, '999,999,999')) as "SPipStartBenefit",
chr(163) || trim(to_char(a.ASD09R, '999,999,999')) as "SAaSavings1",
chr(163) || trim(to_char(a.ASD10R, '999,999,999')) as "SAaLimit1",
a.ASD07X as "SPipYear2",
a.ASD08X as "SPipTaxYear2",
chr(163) || trim(to_char(a.ASD15R, '999,999,999')) as "SAaSavings2",
chr(163) || trim(to_char(a.ASD16R, '999,999,999')) as "SAaLimit2",
a.ASD10X as "SPipYear3",
a.ASD11X as "SPipTaxYear3",
chr(163) || trim(to_char(a.ASD21R, '999,999,999')) as "SAaSavings3",
chr(163) || trim(to_char(a.ASD22R, '999,999,999')) as "SAaLimit3"
from 
add_static_data a
where
a.data_type = 'AAST11'
and a.bgroup = :BGROUP AND a.refno = :REFNO
;