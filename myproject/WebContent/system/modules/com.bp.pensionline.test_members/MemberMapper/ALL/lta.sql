select 
  trim(to_char(rf.REVFAC, '999999999.99')) as "Lta"
  , chr(163)|| trim(to_char(rf.REVFAC, '999,999,999')) as "Lta.Pound"
from 
  (select 
      to_number(to_char(sysdate, 'YYYY'),'9999') year
    from 
      dual 
    where 
      to_char(sysdate, 'MMDD') >= to_number('0406', '9999')
    union select 
      nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year
    from 
      dual 
    where 
      to_char(sysdate, 'MMDD') < to_number('0406', '9999')
  ) taxyear
  ,revfac rf
where 
  rf.FACTYPE = 'LTAA'
  and rf.YOE = taxyear.year
