select
  trim(to_char(ps.mh04c,'999999999.99')) as "BasicSalary"
  ,chr(163)||trim(to_char(ps.mh04c,'999,999,999')) as "BasicSalary.Pound"
from 
  basic b
  ,(select seqno
    from 
      (select seqno, mh02d 
       from
         MISCELLANEOUS_HISTORY 
       where 
         bgroup = :bgroup
         and refno = :refno
         and mh04c is not null
         order by mh02d desc
      )
      where rownum < 2
  ) pss
  ,MISCELLANEOUS_HISTORY ps
where 
  b.bgroup = :bgroup
  and b.refno = :refno
  and ps.bgroup (+)= b.bgroup
  and ps.refno  (+)= b.refno
  and ps.seqno     = pss.seqno
