select * from (
  select
    initcap(addcode) as "AddressType"
    ,replace(
      trim(trailing chr(13) from 
        replace(
          replace(address1 || chr(13)
            ||address2 || chr(13)
            || address3|| chr(13)
            || address4|| chr(13)
            || address5|| chr(13)
            || postcode || chr(13)
            || country, chr(13)||chr(13)||chr(13), chr(13)), 
          chr(13)||chr(13), chr(13))), 
        chr(13), '&lt;br/&gt;') as "Address"
    ,replace(substr(ad.postcode, 0, 4), ' ', '') as "WsPostcode"
    ,decode(nvl(ad.country_co, 'null'), 'Y', 'true', 'N', 'false', 'false') as "OverseasIndicator.Text"
  from  
    basic b
    ,PS_ADDRESSDETAILS ad
    ,PS_ADDRESS psad
  where
    b.bgroup= :bgroup
    and b.refno= :refno
    and psad.bgroup= b.bgroup
    and psad.refno= b.refno
    and psad.addcode='GENERAL'
    and psad.endd is null
    and ad.bgroup = b.bgroup
    and ad.addno= psad.ADDNO
  union
  select
    initcap('NONE') as "AddressType"
    ,'No details available' as "Address"
    ,'XX99' as "WsPostcode"
    ,'Unknown' as "OverseasIndicator.Text"
  from dual
) 
where rownum = 1
