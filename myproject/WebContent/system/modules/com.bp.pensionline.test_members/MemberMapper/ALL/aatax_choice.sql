Select * From ( 
        Select 
		To_Char(Ahd01d,'YYYY/MM/DD HH24:MI') As "AaCurrentChoiceTimestamp",
		To_Char(Ahd01d,'HH24:MI') As "AaCurrentChoiceTime",
		To_Char(Ahd01d,'DD Month YYYY') As "AaCurrentChoiceDate",
    (Case When Ahd01x = 'green' Then 'Reduce accrual rate'
          When Ahd01x = 'red' Then 'Retain current accrual rate'
          Else 'Unknown'
          end) "AaCurrentChoice"
        From Add_Hist_Data 
        Where Bgroup = :Bgroup 
                        And Refno = :Refno 
                        And Data_Type = 'AACHOICE'
                        And Ahd01d > to_date('01-Jan-2012', 'DD-MON-YYYY')
	Order By Ahd01d Desc  
    ) 
Where Rownum = 1
;