SELECT nvl(fte, 1) AS "FTE"
FROM
  (SELECT ta.ta10p / ta.ta11p AS "FTE"
   FROM temporary_absence ta
   WHERE bgroup = :BGROUP
   AND refno = :REFNO
   AND ta03a = 'PT'
   AND ta05d IS NULL
   UNION
   SELECT NULL as "FTE"
   FROM dual)
a
WHERE rownum < 2
;