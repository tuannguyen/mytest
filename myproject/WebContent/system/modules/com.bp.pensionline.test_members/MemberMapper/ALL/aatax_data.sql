WITH
base_data as (
SELECT *
FROM add_hist_data
WHERE 1 = 1
AND bgroup = :BGROUP
AND refno = :REFNO
AND ahd01d = (SELECT MAX(ahd01d) FROM add_hist_data WHERE bgroup = :BGROUP AND refno = :REFNO AND data_type = 'AADATA1')
),
tax_data_1 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA1'
),
tax_data_2 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA2'
),
tax_data_3 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA3'
),
tax_data_4 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA4'
),
tax_data_5 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA5'
),
tax_data_6 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA6'
),
tax_data_7 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA7'
),
tax_data_8 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA8'
),
tax_data_9 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA9'
),
tax_data_10 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA10'
),
tax_data_11 as (
SELECT *
FROM base_data
WHERE 1=1
AND data_type = 'AADATA11'
)
SELECT
to_char(aadata1.ahd01d,'DD Month YYYY') as "AaEffectiveDate",
to_char(aadata1.ahd02d,'YYYY') as "PipYear",
to_char(aadata1.ahd03d,'YYYY') || '/' || (to_number(to_char(aadata1.ahd03d,'YY')) + 1) as "PipTaxYear",
chr(163) || trim(to_char(aadata1.ahd01r, '999,999,999')) as "PipStartBenefit",
chr(163) || trim(to_char(aadata1.ahd02r, '999,999,999')) as "PipEndSalary",
'1/' || trim(to_char(aadata1.ahd03r, '99')) "AaAccrualRate",
chr(163) || trim(to_char(aadata1.ahd04r, '999,999,999')) as "PipEndBenefit",
chr(163) || trim(to_char(aadata1.ahd05r, '999,999,999')) as "AaTaxableAmt",
lower(aadata1.ahd01x) as "AaTaxStatus",
lower(aadata1.ahd02x) as "AltAaTaxStatus",
to_char(aadata2.ahd02d,'DD Month YYYY') as "PipStartDate",
to_char(aadata2.ahd03d,'DD Month YYYY') as "PipEndDate",
chr(163) || trim(to_char(aadata2.ahd01r, '999,999,999')) as "AaCarryFwdUsed",
chr(163) || trim(to_char(aadata2.ahd02r, '999,999,999')) as "AaNetThisYear",
trim(to_char(aadata2.ahd03r*100, '90.00')) || '%' as "AaCashAlternative",
chr(163) || trim(to_char(aadata2.ahd04r, '999,999,999')) as "AaGrossThisYear",
trim(to_char(aadata2.ahd05r*100, '90.00')) || '%' as "AaAccrualDeduct",
lower(aadata2.ahd01x) as "BulkCalculation",
lower(aadata2.ahd02x) as "AaDarkRed",
'1/' || trim(to_char(aadata3.ahd01r, '99999')) "AltAaAccrualRate",
chr(163) || trim(to_char(aadata3.ahd02r, '999,999,999')) as "AltPipEndBenefit",
chr(163) || trim(to_char(aadata3.ahd03r, '999,999,999')) as "AltAaCarryFwdUsed",
trim(to_char(aadata3.ahd04r*100, '90.00')) || '%' as "AltAaAccrualDeduct",
trim(to_char(aadata3.ahd05r*100, '90.00')) || '%' as "AltAaCashAlternative",
lower(aadata3.ahd01x) as "AlternateVisible",
chr(163) || trim(to_char(aadata4.ahd01r, '999,999,999')) as "AltAaTaxableAmt",
chr(163) || trim(to_char(aadata4.ahd02r, '999,999,999')) as "AltAaGrossThisYear",
chr(163) || trim(to_char(aadata4.ahd03r, '999,999,999')) as "AltAaNetThisYear",
chr(163) || trim(to_char(aadata4.ahd04r, '999,999,999')) as "AaLimit",
to_char(aadata5.ahd02d,'YYYY') as "PipYearM1",
chr(163) || trim(to_char(aadata5.ahd01r, '999,999,999')) as "AaCarryFwdBeforeUseM1",
chr(163) || trim(to_char(aadata5.ahd02r, '999,999,999')) as "AaCarryFwdUsedM1",
chr(163) || trim(to_char(aadata5.ahd03r, '999,999,999')) as "AaCarryFwdTotalM1",
chr(163) || trim(to_char(aadata5.ahd04r, '999,999,999')) as "AltAaCarryFwdUsedM1",
chr(163) || trim(to_char(aadata5.ahd05r, '999,999,999')) as "AltAaCarryFwdTotalM1",
to_char(aadata6.ahd02d,'YYYY') as "PipYearM2",
chr(163) || trim(to_char(aadata6.ahd01r, '999,999,999')) as "AaCarryFwdBeforeUseM2",
chr(163) || trim(to_char(aadata6.ahd02r, '999,999,999')) as "AaCarryFwdUsedM2",
chr(163) || trim(to_char(aadata6.ahd03r, '999,999,999')) as "AaCarryFwdTotalM2",
chr(163) || trim(to_char(aadata6.ahd04r, '999,999,999')) as "AltAaCarryFwdUsedM2",
chr(163) || trim(to_char(aadata6.ahd05r, '999,999,999')) as "AltAaCarryFwdTotalM2",
to_char(aadata7.ahd02d,'YYYY') as "PipYearM3",
chr(163) || trim(to_char(aadata7.ahd01r, '999,999,999')) as "AaCarryFwdBeforeUseM3",
chr(163) || trim(to_char(aadata7.ahd02r, '999,999,999')) as "AaCarryFwdUsedM3",
chr(163) || trim(to_char(aadata7.ahd04r, '999,999,999')) as "AltAaCarryFwdUsedM3",
to_char(aadata8.ahd02d,'YYYY') as "PipYearM0",
chr(163) || trim(to_char(aadata8.ahd01r, '999,999,999')) as "AaCarryFwdBeforeUseM0",
chr(163) || trim(to_char(aadata8.ahd02r, '999,999,999')) as "AaCarryFwdUsedM0",
chr(163) || trim(to_char(aadata8.ahd03r, '999,999,999')) as "AaCarryFwdTotalM0",
chr(163) || trim(to_char(aadata8.ahd04r, '999,999,999')) as "AltAaCarryFwdUsedM0",
chr(163) || trim(to_char(aadata8.ahd05r, '999,999,999')) as "AltAaCarryFwdTotalM0",
chr(163) || trim(to_char(aadata9.ahd01r, '999,999,999')) as "AaGrossM0",
chr(163) || trim(to_char(aadata9.ahd02r, '999,999,999')) as "AaGrossM1",
chr(163) || trim(to_char(aadata9.ahd03r, '999,999,999')) as "AaGrossM2",
chr(163) || trim(to_char(aadata9.ahd04r, '999,999,999')) as "AaGrossM3",
chr(163) || trim(to_char(aadata9.ahd05r, '999,999,999')) as "AaCarryFwdTotal",
chr(163) || trim(to_char(aadata7.ahd03r, '999,999,999')) as "AaCarryFwdTotalM3",
to_char(aadata10.ahd02d,'YYYY') as "PipTaxYearM0",
to_char(aadata10.ahd03d,'YYYY') as "PipTaxYearM1",
chr(163) || trim(to_char(aadata10.ahd01r, '999,999,999')) as "AaGrossM0IncAVC",
chr(163) || trim(to_char(aadata10.ahd02r, '999,999,999')) as "AaGrossM1IncAVC",
chr(163) || trim(to_char(aadata10.ahd03r, '999,999,999')) as "AaGrossM2IncAVC",
chr(163) || trim(to_char(aadata10.ahd04r, '999,999,999')) as "AaGrossM3IncAVC",
chr(163) || trim(to_char(aadata10.ahd05r, '999,999,999')) as "CpiPipYearM0",
to_char(aadata11.ahd02d,'YYYY') as "PipTaxYearM2",
to_char(aadata11.ahd03d,'YYYY') as "PipTaxYearM3",
chr(163) || trim(to_char(aadata11.ahd01r, '999,999,999')) as "AaLimitM0",
chr(163) || trim(to_char(aadata11.ahd02r, '999,999,999')) as "AaLimitM1",
chr(163) || trim(to_char(aadata11.ahd03r, '999,999,999')) as "AaLimitM2",
chr(163) || trim(to_char(aadata11.ahd04r, '999,999,999')) as "AaLimitM3"
FROM
tax_data_1 aadata1,
tax_data_2 aadata2,
tax_data_3 aadata3,
tax_data_4 aadata4,
tax_data_5 aadata5,
tax_data_6 aadata6,
tax_data_7 aadata7,
tax_data_8 aadata8,
tax_data_9 aadata9,
tax_data_10 aadata10,
tax_data_11 aadata11
WHERE 1=1
AND aadata1.bgroup = aadata2.bgroup (+)
AND aadata1.refno = aadata2.refno (+)
AND aadata1.bgroup = aadata3.bgroup (+)
AND aadata1.refno = aadata3.refno (+)
AND aadata1.bgroup = aadata4.bgroup (+)
AND aadata1.refno = aadata4.refno (+)
AND aadata1.bgroup = aadata5.bgroup (+)
AND aadata1.refno = aadata5.refno (+)
AND aadata1.bgroup = aadata6.bgroup (+)
AND aadata1.refno = aadata6.refno (+)
AND aadata1.bgroup = aadata7.bgroup (+)
AND aadata1.refno = aadata7.refno (+)
AND aadata1.bgroup = aadata8.bgroup (+)
AND aadata1.refno = aadata8.refno (+)
AND aadata1.bgroup = aadata9.bgroup (+)
AND aadata1.refno = aadata9.refno (+)
AND aadata1.bgroup = aadata10.bgroup (+)
AND aadata1.refno = aadata10.refno (+)
AND aadata1.bgroup = aadata11.bgroup (+)
AND aadata1.refno = aadata11.refno (+)
;