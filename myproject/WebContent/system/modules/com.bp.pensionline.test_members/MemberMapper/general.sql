select 
  b.bgroup as "Bgroup"
  ,b.refno as "Refno"
  ,b.BD08X as "Nino"
  ,nvl(b.BD04X, 'No information available') as "EmployeeNumber"
  ,nvl(b.BD29X, 0) as "SecurityIndicator"
  ,b.BD19A as "MembershipStatusRaw"
  ,b.SD01X as "SchemeRaw"
  ,b.SD01X as "Scheme"
  ,sd.SD02X as "SchemeName"
  ,trim(leading '0' from initcap(to_char(b.BD22D, 'DD MON YYYY'))) as "DateJoinedScheme"
  ,trim(leading '0' from initcap(to_char(b.BD18D, 'DD MON YYYY'))) as "DateOfLeaving"     -- for derferreds
  ,trim(leading '0' from initcap(to_char(b.BD18D, 'DD MON YYYY'))) as "PensionStartDate"  -- for pensioners
  ,cd.CA03I as "Nra"
  ,nvl(cd.CA71I, cd.CA03I) as "Npa"
  ,trim(leading '0' from initcap(to_char(b.bd11d, 'DD MON YYYY'))) as "Dob"
  ,trim(leading '0' from initcap(to_char(ADD_MONTHS(b.bd11d, cd.ca03i * 12), 'DD MON YYYY'))) as "Nrd"
  ,trim(leading '0' from initcap(to_char(ADD_MONTHS(b.bd11d, nvl(cd.CA71I, cd.CA03I) * 12), 'DD MON YYYY'))) as "Npd"
  ,initcap(b.BD07A || ' ' || b.BD05A || ' ' || b.BD38A) as "Name"
  ,initcap(b.BD07A) as "Title"
  ,initcap(b.BD38A) as "Surname"
  ,initcap(b.BD07A || ' ' || b.BD38A) as "ShortName"
  ,nvl(mpac.MPAM06X, 'No information available') as "TelephoneNumber" 
  ,nvl(mpac.MPAM07X, 'No information available') as "MobileNumber" 
  ,decode((MPAM02X ||'@'|| MPAM04X), '@', 'No information available', (MPAM02X ||'@'|| MPAM04X)) as "EmailAddress"
  ,nvl(mpac.MPAM05X, 'No information available') as "FaxNumber" 
  ,nvl(b.BD48X, 'P') as "Newsletter"
  ,decode(nvl(b.BD57X, 'P'), 'P', 'Paper', 'E', 'Electronically by e-mail', 'A', 'Audio tape', 'None') as "Newsletter.Text"
  ,'false' as "CurrentlyContributing"
  ,nvl(b.BD10A, 'U') as "MaritalStatus"
  ,sddl.dol_description as "MaritalStatus.Text"
  ,nvl(b.BD09A, 'null') as "Gender"
  ,decode(nvl(b.BD09A, 'null'), 'M', 'Male', 'F', 'Female', 'Unknown') as "Gender.Text"
  , 0 as "BenefitStatementType"
  ,trim(leading '0' from initcap(to_char(sysdate, 'DD MON YYYY'))) as "BenefitStatementDate"
  ,'salary' as "SalaryOrPay"
  ,'scheme' as "SchemeOrFund"
  ,'current pensionable salary' as "CurrentSalaryOrPay"
  ,'current pensionable salary' as "FinalSalaryOrPay"
  ,'Current pensionable salary' as "CurrentSalaryOrPayCaps"
  ,'Current pensionable salary' as "FinalSalaryOrPayCaps"
  ,'The Directors of BP Pension Trustees Limited' as "EowAddressee"
  ,'BP Pension Fund Trustee' as "EowTrustee"
  ,trim(leading '0' from initcap(to_char(sysdate, 'DD MONTH YYYY'))) as "LetterDate"
  ,'XyzAbc123' as "InitialPassword"
from 
  basic b
  ,SCHEME_DETAIL sd
  ,CATEGORY_DETAIL cd
  ,MP_ADDCOMM mpac 
  ,sd_domain_list sddl 
where 
  b.bgroup= :bgroup
  and b.refno= :refno
  and cd.bgroup= b.bgroup
  and cd.ca26x= b.ca26x
  and sd.bgroup= b.bgroup
  and sd.sd01x= b.sd01x
  and sddl.domain = 'MAR01'
  and sddl.bgroup = b.bgroup
  and sddl.dol_listval = nvl(b.BD10A, 'U')
  and mpac.bgroup (+) = b.bgroup  
  and mpac.refno  (+) = b.refno  
  and mpac.mpam01x (+) = 'GENERAL'