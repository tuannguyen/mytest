select 
  'pay' as "SalaryOrPay"
  ,'fund' as "SchemeOrFund"
  ,'final pensionable salary' as "CurrentSalaryOrPay"
  ,'final pensionable salary' as "FinalSalaryOrPay"
  ,'Final pensionable salary' as "CurrentSalaryOrPayCaps"
  ,'Final pensionable salary' as "FinalSalaryOrPayCaps"
  , 3 as "BenefitStatementType"
from 
  DUAL 