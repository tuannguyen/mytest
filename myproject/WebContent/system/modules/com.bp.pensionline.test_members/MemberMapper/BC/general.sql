select 
  'pay' as "SalaryOrPay"
  ,'fund' as "SchemeOrFund"
  ,'final pensionable pay' as "CurrentSalaryOrPay"
  ,'final pensionable pay' as "FinalSalaryOrPay"
  ,'Final pensionable pay' as "CurrentSalaryOrPayCaps"
  ,'Final pensionable pay' as "FinalSalaryOrPayCaps"
  ,'The Directors of Burmah Castrol Pension Trustees Limited' as "EowAddressee"
  ,'Burmah Castrol Pension Fund Trustee' as "EowTrustee"
  , 2 as "BenefitStatementType"
from 
  DUAL 