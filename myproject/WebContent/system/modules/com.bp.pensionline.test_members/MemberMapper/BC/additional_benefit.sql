select 
    REGEXP_REPLACE(
      nvl(floor(sum(nvl(ti.ti22i,0))/365), 0) 
      || ' years ' 
      || nvl(sum(ti.ti22i) - (floor(sum(ti.ti22i)/365) * 365),0)
      || ' days', '^0 years ') 
      as "TvinBenefit"
    from transfer_in ti
    where bgroup= :bgroup
      and refno= :refno
      and ti.SUB in ('1161','1162','1163','1164')
;