select  
  trim(leading '0' from initcap(to_char(schben.ASD50D, 'DD MON YYYY'))) as "BenefitStatementDate"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD50R, 0),'9,999,999')) as "PensionableSalary.Pound"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD51R, 0),'9,999,999')) as "PensionToDate"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD63R, 0),'9,999,999')) as "DeathInServiceCash"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as "DeathInServicePension"
  ,trim(both ' ' from to_char(nvl(schben.ASD56R, 0),'990.99'))||'%' as "PensionVsSalary"
  ,trim(both ' ' from to_char(nvl(schben.ASD62R, 0),'990.99'))||'%' as "PensionVsLta"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD52R, 0),'9,999,999')) as "NpaReducedPension"
  ,trim(both ' ' from to_char(nvl(schben.ASD57R, 0),'990.99'))||'%' as "NpaPensionvsSalary"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD53R, 0),'9,999,999')) as "NpaSpousesPension"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD60R, 0),'9,999,999')) as "NraReducedPension"
  ,trim(both ' ' from to_char(nvl(schben.ASD59R, 0),'990.99'))||'%' as "NraPensionvsSalary"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD61R, 0),'9,999,999')) as "NraSpousesPension"
  ,nvl(schben.ASD64R, 3) as "BenefitStatementType"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD54R, 0),'9,999,999')) as "GrossNpaPension"
  ,trim(both ' ' from to_char(nvl(schben.ASD58R, 0),'990.99'))||'%' as "GrossNpaPensionVsSalary"
  ,chr(163)||trim(both ' ' from to_char(nvl(schben.ASD55R, 0),'9,999,999')) as "GrossNpaSpousesPension"
from 
  basic
  ,add_static_data schben
where 
  basic.bgroup               = :bgroup
  and basic.refno            = :refno
  and schben.bgroup      (+) = basic.bgroup
  and schben.REFNO       (+) = basic.refno
  and schben.DATA_TYPE   (+) = 'SCHBEN'
; 