select 
  chr(163)||trim(both ' ' from to_char(nvl(Avc.Total, 0),'9,999,999')) as "AvcTotal"
  ,trim(both '0' from to_char(nvl(Avc.Total/Lta.Amount*100, 0),'90.99'))||'%' as "AvcVsLta"
  ,trim(both '0' from to_char(nvl((Avc.Total/Lta.Amount)*100, 0) + Pension.PensionVsLta,'90.99'))||'%' as "TotalVsLta"
from 
( 
  select sum(AVC_history.AH10P) Total
    from AVC_history, mp_fund
    where mp_fund.FUNDCODE = AVC_history.AH09X
    AND AVC_history.BGROUP = :bgroup
    AND AVC_history.REFNO = :refno
    AND AVC_history.AH01X in ('AVC','AVER') 
) Avc
, ( select 
      trim(to_char(rf.REVFAC, '999999999.99')) as Amount
    from 
      (select 
          to_number(to_char(sysdate, 'YYYY'),'9999') year
        from 
          dual 
        where 
          to_char(sysdate, 'MMDD') >= to_number('0406', '9999')
        union select 
          nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year
        from 
          dual 
        where 
          to_char(sysdate, 'MMDD') < to_number('0406', '9999')
      ) taxyear
      ,revfac rf
    where 
      rf.FACTYPE = 'LTAA'
      and rf.YOE = taxyear.year
  ) Lta
, ( select 
      nvl(schben.ASD62R, 0) PensionVsLta
    from 
      basic
      ,add_static_data schben
    where 
      basic.bgroup               = :bgroup
      and basic.refno            = :refno
      and schben.bgroup      (+) = basic.bgroup
      and schben.REFNO       (+) = basic.refno
      and schben.DATA_TYPE   (+) = 'SCHBEN'
  ) Pension
;