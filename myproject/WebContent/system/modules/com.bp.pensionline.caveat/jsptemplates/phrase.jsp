<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<cms:editable />

<div class="element">

<cms:contentload collector="singleFile" param="${opencms.uri}" editable="true">

	<h1><cms:contentshow element="Title" /></h1>

	<p>
		<cms:contentshow element="Description" />
	</p>

</cms:contentload>

</div>
