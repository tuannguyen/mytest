var changePasswordURL = ajaxurl + "/ChangePassword?xml=";

function ToAsciiChar(ch){

	var arrUni = new Array('£','+');
	var arrAscii = new Array('&pound;','&plusH;');

	var k= 0;
	for(k=0; k< arrUni .length; k++){
		if(ch == arrUni[k]){
			return arrAscii[k];
		}
	}
	return ch;
}

function ToAsciiCode(str){
	var tmp = "";
	var i =0;
	for(i=0; i<str.length; i++){
		tmp += ToAsciiChar(str.charAt(i));
	}
	return	tmp;
}

function ChangePassword(oldPassword, newPassword, confirmPassword)
{
	showloading(true);
	var xml = "";
	xml += "<AjaxParameterRequest>\n";		
	xml += "		<OldPassword><![CDATA[" + oldPassword + "]]></OldPassword>\n";
	xml += "		<NewPassword><![CDATA[" + newPassword + "]]></NewPassword>\n";
	xml += "		<ConfirmPassword><![CDATA[" + confirmPassword + "]]></ConfirmPassword>\n";
	xml += "</AjaxParameterRequest>\n";

	xml = ToAsciiCode(xml);
	//alert(xml);		

	var url = changePasswordURL + escape(xml);

	ChangePasswordHttp.open("POST", url);

	ChangePasswordHttp.onreadystatechange = ChangePasswordHandle;
	
	ChangePasswordHttp.send(null);
	
	showloading(false);
}

function showloading(bval) {
	// Loading variables
	var ninoLoadID = document.getElementById('loading');
	var ninoButtonID = document.getElementById('dochange');
	
	if (bval == true) {
		ninoLoadID.style.display = "";
		ninoButtonID.style.display = "none";
	} else {
		ninoLoadID.style.display = "none";
		ninoButtonID.style.display = "";
	}
}


