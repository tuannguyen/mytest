// JavaScript Document

var iMailHttp = null;
var subHttp = null;
var iMailCurrentPage= 1;
var totalImails= 0;
var iMailPerPages = 20;
var totalPages = 1;

var iMailHandlerURL = ajaxurl + "/ViewImailHandler?xml=";
var iMailTableHead = "<table class='datatable'><tbody><tr><th class='blank'>&nbsp;</th><th class='label'>Subject</th><th class='label'>Submitted</th><th class='label'>Member</th></tr>";
var iMailTableFoot = "</tbody></table>";
var iMailTableBody = "";

var archivedImailTableHead = "<table class='datatable'><tbody><tr><th class='label'>Subject</th><th class='label'>Submitted</th><th class='label'>Archived</th><th class='label'>Member</th></tr>";
var archivedImailTableBody = "";
var archivedImailTableFoot = "</tbody></table>";

var isDebugImail = false;

function generateXmlForActiveiMail(requestPage){
	
	var xml = "";
	
	xml += "<AjaxRequestXml>";
	xml += "	<ViewActiveImail />";
	xml += "	<RequestPage>" + requestPage +"</RequestPage>";
	xml += "</AjaxRequestXml>";
	
	return xml;
	
}
	
function generateXmlForArchivediMail(requestPage){
	var xml = "";
	xml += "<AjaxRequestXml>";
	xml += "	<ViewArchivedImail />";
	xml += "	<RequestPage>" + requestPage + "</RequestPage>";
	xml += "</AjaxRequestXml>";
	return xml;
}

function generateXmlToArchiveiMail(caseworkId){

	var xml = "";
	
	xml += "<AjaxRequestXml>";
	xml += "<UpdateImail>" + caseworkId + "</UpdateImail>";
	xml += "</AjaxRequestXml>";

	return xml;
}
	

function handleResponseForActiveiMail(){
	if (iMailHttp .readyState == 4) {
								
			// Check that a successful server response was received
	      	if (iMailHttp .status == 200) 
			{
				DebugImail(iMailHttp.responseText);

				var response = iMailHttp.responseXML;

				try{
					
					var totalNodes = response.getElementsByTagName("Total");

					DebugImail("Total nodes = " + totalNodes.length);
	
					if(totalNodes && totalNodes.length > 0){
						totalImails = totalNodes[0].firstChild.nodeValue;
						var nPage = Math.floor(totalImails / iMailPerPages );
						if((totalImails % iMailPerPages) > 0){
							nPage += 1;
						}
						totalPages = nPage ;

						DebugImail(totalImails);
					}

					DebugImail("Total imails = " + totalImails);

					iMailTableBody = "";

					if(totalImails > 0){

						//view content	
						
						var aciMails = response.getElementsByTagName("Imail");

						var rowsCount = aciMails.length;

						DebugImail("rows count = " + rowsCount);

						if(rowsCount   > 0){

							for(i = 0; i<rowsCount; i++){
									
								var iMail = aciMails[i];
								
								var sLength = iMail.childNodes.length;
								var caseworkid= "";
								var subject = "";
								var submitted = "";
								var member = "";
		
								for(j=0; j<sLength; j++){

									if(iMail.childNodes[j].nodeType == 1){

										if(iMail.childNodes[j].nodeName == "Subject"){

											if(iMail.childNodes[j].childNodes.length > 0){
												subject = iMail.childNodes[j].firstChild.nodeValue; 
											}
										}else if(iMail.childNodes[j].nodeName == "Submitted"){

											if(iMail.childNodes[j].childNodes.length > 0){
												submitted = iMail.childNodes[j].firstChild.nodeValue;

											}
										}else if(iMail.childNodes[j].nodeName == "Member"){

											if(iMail.childNodes[j].childNodes.length > 0){
												member = iMail.childNodes[j].firstChild.nodeValue;
										
											}
										}else if(iMail.childNodes[j].nodeName == "CaseworkId"){

											if(iMail.childNodes[j].childNodes.length > 0){
												caseworkid = iMail.childNodes[j].firstChild.nodeValue;
										
											}
										}

									}
									
								}	
								iMailTableBody += "<tr><td><img height='20' alt='Archive this imail' width='20' src='/pdf.gif' style='cursor: hand;' onClick='onArchivedImail("+ caseworkid+");' /></td>";
								iMailTableBody += "<td>" + subject + "</td>";
								iMailTableBody += "<td>"+ submitted  +"</td>";
								iMailTableBody += "<td>"+ member +"</td></tr>";
	
																	
															
							}
							
						}else{
							DebugImail("There's no imail.");
						}

					}else{

						DebugImail("There's no imail.");
						iMailTableBody +="<tr><td>&nbsp;</td><td colspan='3'>there are no iMails pending</td></tr>";

					}
					
					var iMailTable = document.getElementById("active_imail");

					DebugImail("InnerHTML=" + iMailTable.innerHTML);
					
					iMailTable.innerHTML = "";

					iMailTable.innerHTML = iMailTableHead + iMailTableBody + iMailTableFoot;
					
					DebugImail("InnerHTML=" +iMailTable.innerHTML);

					var imailStart = iMailCurrentPage*iMailPerPages -iMailPerPages ;
					var imailStop = (totalImails-imailStart <= iMailPerPages)? totalImails : (iMailCurrentPage*iMailPerPages);

					document.getElementById("ThisYearHolder").innerHTML = "Showing " + imailStart  + " to " + imailStop  + " of " + totalImails;

					visiWhizzy("ActiveImail", false);					
				}
				catch(err){
					DebugImail(err.description);
					visiWhizzy("ActiveImail", false);

				}					
			}
		}

	}
	

function handleResponseForArchivediMail(){
	if (iMailHttp .readyState == 4) {
								
			// Check that a successful server response was received
	      	if (iMailHttp .status == 200) 
			{
				DebugImail(iMailHttp.responseText);

				var response = iMailHttp.responseXML;

				try{
					
					var totalNodes = response.getElementsByTagName("Total");

					DebugImail("Total nodes = " + totalNodes.length);
	
					if(totalNodes && totalNodes.length > 0){
						totalImails = totalNodes[0].firstChild.nodeValue;
						var nPage = Math.floor(totalImails / iMailPerPages );
						if((totalImails % iMailPerPages) > 0){
							nPage += 1;
						}
						totalPages = nPage ;
						DebugImail("Total pages = " + totalPages);
					}

					DebugImail("Total imails = " + totalImails);

					archivedImailTableBody  = "";

					if(totalImails > 0){

						//view content	
						
						var aciMails = response.getElementsByTagName("Imail");

						var rowsCount = aciMails.length;

						DebugImail("rows count = " + rowsCount);

						if(rowsCount   > 0){

							for(i = 0; i<rowsCount; i++){
									
								var iMail = aciMails[i];
								
								var sLength = iMail.childNodes.length;
		
								for(j=0; j<sLength; j++){

									if(iMail.childNodes[j].nodeType == 1){
										
										if(iMail.childNodes[j].nodeName == "Subject"){
											if(iMail.childNodes[j].childNodes.length > 0){
												subject = iMail.childNodes[j].firstChild.nodeValue;
											}
										}else if(iMail.childNodes[j].nodeName == "Submitted"){
											if(iMail.childNodes[j].childNodes.length > 0){
												submitted = iMail.childNodes[j].firstChild.nodeValue ;
											}
										}else if(iMail.childNodes[j].nodeName == "Archived"){
											if(iMail.childNodes[j].childNodes.length > 0){
												archived = iMail.childNodes[j].firstChild.nodeValue;
											}
										}else if(iMail.childNodes[j].nodeName == "Member"){
											if(iMail.childNodes[j].childNodes.length > 0){
												member = iMail.childNodes[j].firstChild.nodeValue ;	
											}
										}
									}
									
								}
								archivedImailTableBody += "<tr><td>" + subject + "</td>";
								archivedImailTableBody += "<td>" + submitted + "</td>";
								archivedImailTableBody += "<td>"+ archived +"</td>";
								archivedImailTableBody += "<td>"+ member +"</td></tr>";	
																									
				
							}
							
						}else{
							DebugImail("There's no imail.");
						}

					}else{

						DebugImail("There's no imail.");
						archivedImailTableBody +="<tr><td colspan='4'>there are no iMails pending</td></tr>";

					}
					
					var iMailTable = document.getElementById("archived_imail");

					DebugImail("InnerHTML=" +iMailTable.innerHTML);
					
					iMailTable.innerHTML = archivedImailTableHead + archivedImailTableBody + archivedImailTableFoot ;
					
					DebugImail("InnerHTML=" +iMailTable.innerHTML);

					var imailStart = iMailCurrentPage*iMailPerPages -iMailPerPages ;
					var imailStop = (totalImails-imailStart <= iMailPerPages)? totalImails : (iMailCurrentPage*iMailPerPages);

					document.getElementById("ThisYearHolder").innerHTML = "Showing " + imailStart  + " to " + imailStop  + " of " + totalImails;
					visiWhizzy("ArchivedImail", false);
					
				}
				catch(err){
					DebugImail(err.description);
					visiWhizzy("ArchivedImail", false);	
				}					
			}
		}
	}
	
	
//function to make ajax
function makeAjaxForiMail(xmlRequest, callBack){
	
		//create new HttpRequest object
		iMailHttp = createXmlHttpRequestObject();

		//build url
		var url = iMailHandlerURL + xmlRequest;

		DebugImail(url);

		//request data
		iMailHttp.open("POST", url);

		iMailHttp.onreadystatechange = callBack;

		
		iMailHttp.send(null);
	
	}

//implement function to get active imail
function getActiveiMail(requestPage){

	visiWhizzy("ActiveImail", true);	

	var xml = generateXmlForActiveiMail(requestPage);

	DebugImail(xml);		

	makeAjaxForiMail(xml, handleResponseForActiveiMail);
	
	}

function getNextActiveiMail(){

	DebugImail("Current page = " + iMailCurrentPage);

	if(iMailCurrentPage < totalPages ){
		getActiveiMail( ++iMailCurrentPage );
	}else{
		DebugImail("You are at last page");	
	}	
}

function getPrevActiveiMail(){

	DebugImail("Current page = " + iMailCurrentPage);

	if(iMailCurrentPage > 1){
		getActiveiMail( --iMailCurrentPage);
	}else{
		DebugImail("You are at first page");	
	}
}
//implement function to get archived imail
function getArchivediMail(requestPage){

	visiWhizzy("ArchivedImail", true);	

	var xml = generateXmlForArchivediMail(requestPage);
	
	makeAjaxForiMail(xml, handleResponseForArchivediMail);
	
}	

function getNextArchivediMail(){

	DebugImail("Current page = " + iMailCurrentPage);

	if(iMailCurrentPage < totalPages){
		getArchivediMail( ++iMailCurrentPage);
	}else{
		DebugImail("You are at last page");
	}	
}

function getPrevArchivediMail(){

	DebugImail("Current page = " + iMailCurrentPage);

	if(iMailCurrentPage > 1){
		getArchivediMail( --iMailCurrentPage);
	}else{
		DebugImail("You are at firs page");	
	}
}
function onViewActiveImail(){
	getActiveiMail(1);	
}

function onViewArchivedImail(){
	getArchivediMail(1);
}

function DebugImail(msg){
	if(isDebugImail){
		alert(msg);
	}
}

function generateXmlToSubmitImail(caseworkid){
	var xml = "";
	xml += "<AjaxRequestXml>";
	xml += "<UpdateImail>" + caseworkid + "</UpdateImail>";
	xml += "</AjaxRequestXml>"
	return xml;
}

function onArchivedImail(caseworkid){
	visiWhizzy("ActiveImail", true);
	var xml = generateXmlToSubmitImail(caseworkid);
	DebugImail(xml);
	makeAjaxForiMail(xml, handleResponseForSubmitImail);

}

function handleResponseForSubmitImail(){
	if (iMailHttp .readyState == 4) {
								
		// Check that a successful server response was received
	      	if (iMailHttp .status == 200) 
		{
			var response = iMailHttp.responseXML;

			DebugImail(iMailHttp.responseText);

			var rstag = response.getElementsByTagName("Success");

			if(rstag.length > 0){
				var msg = rstag[0].firstChild.nodeValue;
				alert(msg);
			}else{
				rstag = response.getElementsByTagName("Error");
				var msg = rstag[0].firstChild.nodeValue;
				alert(msg);
			}
			visiWhizzy("ActiveImail", false);
			
			if((totalImails - (iMailCurrentPage-1)*iMailPerPages) == 1){
				getActiveiMail(iMailCurrentPage - 1);	
			}else{
				getActiveiMail(iMailCurrentPage);	
			}
		}
	}
}
