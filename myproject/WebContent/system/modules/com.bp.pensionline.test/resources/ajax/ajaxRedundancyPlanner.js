var rrHttp = null;

var isRrDebug = false;

var arr = new Array ("RedundancyDate","EgpCash", "SrpCash", "TaxFreeCash", "TaxableCash", "TaxPayable", "RrAccruedPension", "RrReducedPension", "RrSpousesPension", "RrMaxLumpSum", "RrResidualPension");

/**
 * Assumption: rrDate is a date String formated with day, month, year,... seperated by space, and day beginning, then month
 */
function generateXmlForRrPlanner(rrdate){
	var dates = rrdate.split(" ");
	var formated = "";
	for(var i = 0, c = 0; i < dates.length; i++){
		if(dates[i].length > 0){
			if(c == 1){ //Second word
				dates[i] = dates[i].substring(0,3);
			}
			formated += dates[i] + " ";
			c++;
		}
	}
	var xml = "";
	xml += "<AjaxParameterRequest>\n";
	xml += "	<RedundancyDate>" + formated + "</RedundancyDate>";
	xml += "</AjaxParameterRequest>\n";
	return xml;
}            

function clearDataForRR(){
	var i = 0;
	for(i=0; i<arr.length; i++){
		var tmpId = "f_" + arr[i] + "_01";
		var target = document.getElementById(tmpId);
		if(target) target.innerHTML = "";
	}
}

function showImagesForRR(vis){
	var i = 0;
	for(i=0; i<arr.length; i++){
		var tmpId = "f_" + arr[i] + "_01";
		visiWhizzy(tmpId, vis);
	}
}

function submitRedundancyRequest(){
	clearDataForRR();
	showImagesForRR(true);
	var rrdate = "";
	rrdate = document.getElementById("input_co_nra").value;
	if(rrdate.length == 0){
		alert("Redundancy date can not be blank.");
		return;
	}
	DebugRrPlanner("Redundancy date = " + rrdate);
	var xml = generateXmlForRrPlanner(rrdate);
	var rrUrl = ajaxurl + "/RedundancyHandler?xml="+xml;
	DebugRrPlanner(rrUrl);
	rrHttp = createXmlHttpRequestObject();

	rrHttp.open("POST", rrUrl );

	rrHttp.onreadystatechange = handleResponseForRedundancyPlanner;
	
	rrHttp.send(null);

	modeller_popupControlPanel('date_panel', false); 


}
function loadRedundancyRequest(){

	clearDataForRR();
	showImagesForRR(true);

	var rrdate = "";
        var cDate=new Date();
	rrdate = FormatDateTime(cDate,1);
	if(rrdate.length == 0){
		alert("Redundancy date can not be blank.");
		return;
	}
	DebugRrPlanner("Redundancy date = " + rrdate);
	var xml = generateXmlForRrPlanner(rrdate);
	var rrUrl = ajaxurl + "/RedundancyHandler?xml="+xml;
	DebugRrPlanner(rrUrl);
	rrHttp = createXmlHttpRequestObject();

	rrHttp.open("POST", rrUrl );

	rrHttp.onreadystatechange = handleResponseForRedundancyPlanner;
	
	rrHttp.send(null);

}

function handleResponseForRedundancyPlanner(){

   	if (rrHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (rrHttp.status == 200) {

		try{
			//var response = rrHttp.responseXML;
			var response = createResponseXML(rrHttp.responseText); 

			DebugRrPlanner("Response = " + rrHttp.responseText);
			var error = response.getElementsByTagName("Error").length;
						
			if(error > 0){								
				alert("Error");
				
			}
			else
			{
																
				//window.location = "index.html";
	
				var i=0;

				for(i=0; i<arr.length; i++){

					var tags = response.getElementsByTagName(arr[i]);

					DebugRrPlanner(arr[i] + "   " + tags.length);

					if(tags.length > 0){
						
						var tmpId = "f_" + arr[i] + "_01";

						var target = document.getElementById(tmpId);

						if(target)
						{
							if (tags[0].childNodes.length > 0)
							{	
								target.innerHTML = tags[0].firstChild.nodeValue;
							}else{
								//alert("Target is null");
								target.innerHTML = "";
							}
						}

						visiWhizzy(tmpId , true);		
						
					}
				}

				DebugRrPlanner("get calcrr");
				var rrtag = response.getElementsByTagName("CalcRR");
				if(rrtag.length > 0 && rrtag[0].childNodes.length > 0){
					var visCaveat = rrtag[0].firstChild.nodeValue;
					DebugRrPlanner("caveat=" + visCaveat);
					if(visCaveat == "true"){
						document.getElementById("red_retire").className = "";

						document.getElementById("red_retire").style.visibility = "visible";
					DebugRrPlanner("visible");

					}else{
						document.getElementById("red_retire").className = "caveat";
						document.getElementById("red_retire").style.visibility = "hidden";
					DebugRrPlanner("hidden");
					}
				}
			}	
			
			showImagesForRR(false);

		}catch(err){
			alert(err.description);
			showImagesForRR(false);
		}
		}
		
	}

}

function DebugRrPlanner(msg){
	if(isRrDebug){
		alert(msg);
	}
}
/* ----------------------------------------------------------------
 FormatDateTime: Returns an expression formatted as a date or
                 time. If DateTime is null then false is
                 returned.

 Parameters:
      DateTime   = Date/Time expression to be formatted
      FormatType = Numeric value that indicates the date/time
                   format used. If omitted, GeneralDate is used
                   0 = Very Long Date/Time Format (Mon Jul 10, 12:02:30 am EDT 2000)
                   1 = short Date/Time Format ( 10 Jul 2007)
                   2 = Short Date (1/10/00)
                   3 = Long Time (4:20 PM)
                   4 = Military Time (14:43)

 Returns: String
---------------------------------------------------------------- */
function FormatDateTime(DateTime, FormatType)
{
        if (DateTime == null)
                return (false);

        if (FormatType < 0)
                FormatType = 1;

        if (FormatType > 4)
                FormatType = 1;

        var strDate = DateTime.toGMTString();

        if (strDate.toUpperCase() == "NOW")
        {
                var myDate = new Date();
                strDate = myDate.toGMTString();
        }
        else
        {
                var myDate = new Date(DateTime);
                strDate = myDate.toGMTString();
        }
        
        var units = strDate.split(" ");

        var Day = units[0].substring(0, 3);
        if (Day == "Sun") Day = "Sunday";
        if (Day == "Mon") Day = "Monday";
        if (Day == "Tue") Day = "Tuesday";
        if (Day == "Wed") Day = "Wednesday";
        if (Day == "Thu") Day = "Thursday";
        if (Day == "Fri") Day = "Friday";
        if (Day == "Sat") Day = "Saturday";

        var Month = units[2], MonthNumber = 0;
        if (Month == "Jan") { Month = "Jan"; MonthNumber = 1; }
        if (Month == "Feb") { Month = "Feb"; MonthNumber = 1; }
        if (Month == "Mar") { Month = "Mar"; MonthNumber = 1; }
        if (Month == "Apr") { Month = "Apr"; MonthNumber = 1; }
        if (Month == "May") { Month = "May"; MonthNumber = 1; }
        if (Month == "Jun") { Month = "Jun"; MonthNumber = 1; }
        if (Month == "Jul") { Month = "Jul"; MonthNumber = 1; }
        if (Month == "Aug") { Month = "Aug"; MonthNumber = 1; }
        if (Month == "Sep") { Month = "Sep"; MonthNumber = 1; }
        if (Month == "Oct") { Month = "Oct"; MonthNumber = 1; }
        if (Month == "Nov") { Month = "Nov"; MonthNumber = 1; }
        if (Month == "Dec") { Month = "Dec"; MonthNumber = 1; }

        var curPos = 11;
        var MonthDay = units[1];
        if (MonthDay.charAt(1) == " ")
        {
                MonthDay = "0" + MonthDay.charAt(0);
                curPos--;
        }

        var MilitaryTime = units[4];
        var Year = units[3];

        // Format Type decision time!
        if (FormatType == 1)
                strDate = MonthDay + " " + Month +  " " + Year;
        else if (FormatType == 2)
                strDate = MonthNumber + "/" + MonthDay + "/" + Year.substring(2,4);
        else if (FormatType == 3)
        {
                var AMPM = MilitaryTime.substring(0,2) >= 12 && MilitaryTime.substring(0,2) != "24" ? " PM" : " AM";
                if (MilitaryTime.substring(0,2) > 12)
                        strDate = (MilitaryTime.substring(0,2) - 12) + ":" + MilitaryTime.substring(3,MilitaryTime.length) + AMPM;
                else
                {
                        if (MilitaryTime.substring(0,2) < 10)
                                strDate = MilitaryTime.substring(1,MilitaryTime.length) + AMPM;
                        else
                        strDate = MilitaryTime + AMPM;
                }
        }
        else if (FormatType == 4)
                strDate = MilitaryTime;

        return strDate;
}