var publishingHttp = null;

var isRrDebug = false;

function initEdittingPages(){
	
	var xml = "Hi";
	
	var publishingURL = ajaxurl + "/EditorLandingHandler?xml="+xml;
	
	//alert(publishingURL);

	publishingHttp = createXmlHttpRequestObject();

	publishingHttp.open("POST", publishingURL );

	publishingHttp.onreadystatechange = handleInitEdittingPages;		
	
	publishingHttp.send(null); 

}

function displayChangeExp(pageId, isDisplayed)
{
	var changeExp = document.getElementById(pageId);
	var trHolder = document.getElementById('tr_' + pageId);
	if (changeExp && trHolder)
	{
		if (isDisplayed)
		{		
			changeExp.style.left = trHolder.style.left;
			changeExp.style.top = trHolder.style.top + trHolder.style.height;	
			changeExp.style.display = "block";		
		}	
		else
		{
			changeExp.style.display = "none";	
		}
	}
}

function handleInitEdittingPages(){

   	if (publishingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (publishingHttp.status == 200) {
			try{                                    
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function generatePublishingXML(pageId, pageURI, action, reason, bug_id)
{
		reason = reason.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');	
		var xml = "";
		xml += "<AjaxPublishingParameterRequest>\n";
		xml += "	<ptb_pageId>" + pageId + "</ptb_pageId>\n";
		xml += "	<ptb_pageURI>" + pageURI + "</ptb_pageURI>\n";		
	    xml += "	<ptb_action>" + action + "</ptb_action>\n";
		xml += "	<ptb_reason><![CDATA[" + reason + "]]></ptb_reason>\n";
	    xml += "	<ptb_bugId>" + bug_id + "</ptb_bugId>\n";    	    	    
		xml += "</AjaxPublishingParameterRequest>\n";
		return xml;	
}

function ptb_submitRequest(pageId, pageURI, action, reason, bug_id) {
	// Send to the backend handler to update the table.
	//	alert('pageId: ' + pageId);
	//	alert('action: ' + action);
	//	alert('reason: ' + reason);
	//	alert('bug_id: ' + bug_id);
	
	var xml = generatePublishingXML(pageId, pageURI, action, reason, bug_id);
	
	var publishingURL = ajaxurl + "/PublishingToolbarHandler";
	var params = "xml=" + xml;
	//alert(publishingURL);	
	
	publishingHttp = createXmlHttpRequestObject();

	publishingHttp.open("POST", publishingURL, true);

	publishingHttp.onreadystatechange = handlePublishingResult;			
	    
    publishingHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    publishingHttp.setRequestHeader("Content-length", params.length);
    publishingHttp.setRequestHeader("Connection", "close");	
	
	publishingHttp.send(params);		
}

function handlePublishingResult(){

   	if (publishingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (publishingHttp.status == 200) {
			var response = createResponseXML(publishingHttp.responseText);					
			//alert(publishingHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
				}
				else
				{
					var ptbActionResult = response.getElementsByTagName("PtbActionResult")[0].firstChild.nodeValue;
					var ptbPageReload = response.getElementsByTagName("PtbPageReload")[0].firstChild.nodeValue;
					if (ptbActionResult == 'true')
					{
						if (ptbPageReload == 'true')
						{
							window.location.reload();							
						}
						else
						{
							window.location.replace(ajaxurl + '/pl/publishing/recent_changes.jsp');
						}
					}
					else
					{
						alert('Pulishing request failed!');
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function ptbNotifyEditing()
{
	var publishingURL = ajaxurl + "/ProjectSwitchingHandler?ptb_role=Authors&ptb_action=Check_in";
	
	//alert(publishingURL);

	publishingHttp = createXmlHttpRequestObject();

	publishingHttp.open("POST", publishingURL );

	publishingHttp.onreadystatechange = ptbHandleNotifyEditingResponse;		
	
	publishingHttp.send(null); 		
}

function ptbHandleNotifyEditingResponse(){	// not used

   	if (publishingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (publishingHttp.status == 200) {					
		}
	}

}

function updateToolbarStatus(){	// not used
	var url = document.URL;
	var pageURI = url;
	var pageURIStart = url.indexOf('content/pl');
	if (pageURIStart >= 0)
	{
		 pageURI = url.substring(pageURIStart + 'content/pl'.length);
	}
	if (pageURI.lastIndexOf('/') == pageURI.length -1)
	{
		pageURI = pageURI + 'index.html';
	}
	//alert(pageURI);
	
	var publishingURL = ajaxurl + "/EditorLandingHandler?ptb_pageURI=" + pageURI;
	
	publishingHttp = createXmlHttpRequestObject();

	publishingHttp.open("POST", publishingURL );

	publishingHttp.onreadystatechange = handleUpdateToolbarStatusResponse;		
	
	publishingHttp.send(null); 	
}

function handleUpdateToolbarStatusResponse(){ // not used

   	if (publishingHttp.readyState == 4) {									
		// Check that a successful server response was received
		if (publishingHttp.status == 200) 
		{			
			var response = createResponseXML(publishingHttp.responseText);					
			//alert(publishingHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
				}
				else
				{
					var ptbResourceUpdated = response.getElementsByTagName("PtbResourceUpdated")[0].firstChild.nodeValue;
					if (ptbResourceUpdated == 'true')
					{
						toolbar_displayed = true;	// set this to true for toolbar_displayed in publishing_toolbar.jsp
						document.getElementById('publishing_toolbar').style.display = 'block';
					}
					else
					{
						toolbar_displayed = false;
						document.getElementById('publishing_toolbar').style.display = 'none';
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}								
		}
	}

}

function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}