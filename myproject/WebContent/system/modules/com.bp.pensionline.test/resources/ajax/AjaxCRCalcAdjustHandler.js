var calcAdjustHttp = null;
var calc_adjust_fields   = new Array(
	"calc_date",
	"Orginal_UnreducedPension",
	"Orginal_Pension",
	"Orginal_ReducedPension",
	"DoR",
	"FPS",
	"CINN91",
	"ERF",
	"ComFactor",
	"cash",
	"acc_date",
	"acc_rate",
	"lta",
	"FTE",
	"addService",
	"addPTService",
	"unreducedUplift",
	"uplift",
	"newReducedPension",
	"_2rdsLimit",
	"ComFactor",
	"Adjusted_UnreducedPension",
	"Adjusted_ReducedPension",
	"Adjusted_SpousesPension",
	"schemeCash",
	"Adjusted_MaximumCashLumpSum",
	"residualMaxCash",
	"Adjusted_PensionWithChosenCash",
	"Adjusted_PensionWithMaximumCash"
	);

function generateUnitTestCalcAdjustXML(bgroup, refno, nra, accRate, cash, adjustUsed)
{
	var xml = "";
	xml += "<AjaxCalcAdjustRequest>";
	xml += "<BGroup>" + bgroup + "</BGroup>";
	xml += "<RefNo>" + refno + "</RefNo>";
	xml += "<cal_nra>" + nra + "</cal_nra>";
	xml += "<cal_accRate>" + accRate + "</cal_accRate>";
	xml += "<cal_cash>" + cash + "</cal_cash>";		
	xml += "<cal_adjustUsed>" + adjustUsed + "</cal_adjustUsed>";	
	xml += "</AjaxCalcAdjustRequest>";
	return xml;
}

function submitUnitTestCalcAdjustRequest()
{
	var bgroup = document.getElementById("bgroup").value;
	var refno = document.getElementById("refno").value;	
	if (refno == null || refno.length == 0)
	{
		alert('Please input an AC reference number');
		return false;
	}
	var nra = document.getElementById("input_co_nra").value;		
	var accRate = document.getElementById("input_co_option").value;	
	var cash = document.getElementById("input_co_cash").value;
	var adjustUsed = document.getElementById("use_calc_adjust").checked;		
	
	// if oldName is null or empty, consisder this is add report request
	var xml = generateUnitTestCalcAdjustXML(bgroup, refno, nra, accRate, cash, adjustUsed);	
	
	var unitTestCalcURL = ajaxurl + "/ACCRCalcAdjustModeller";
	var params = "xml=" + xml;

	//alert(xml);

	calcAdjustHttp = createXmlHttpRequestObject();

	calcAdjustHttp.open("POST", unitTestCalcURL, true);

	calcAdjustHttp.onreadystatechange = handleRunUnitTestResult;		
	    
    calcAdjustHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    calcAdjustHttp.setRequestHeader("Content-length", params.length);
    calcAdjustHttp.setRequestHeader("Connection", "close");	
    
	for(var i=0; i<calc_adjust_fields.length; i++) 
	{
		var tag_name = calc_adjust_fields[i];	
		var update_element = document.getElementById(tag_name);			
		update_element.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"running...\" />";
	}    
	
	calcAdjustHttp.send(params);	
}

function handleRunUnitTestResult(){

   	if (calcAdjustHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (calcAdjustHttp.status == 200) {
			var response = createResponseXML(calcAdjustHttp.responseText);					
			//alert(calcAdjustHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					for(var i=0; i<calc_adjust_fields.length; i++) 
					{
						var tag_name = calc_adjust_fields[i];	
						var update_element = document.getElementById(tag_name);			
						update_element.innerHTML = "&nbsp;";
					}    				
					if(errors[0].childNodes[0].nodeValue.indexOf('Unknown')>-1)
					{
						alert('There was an error while running the test, please can you check your entered data');
					}else
					{
						alert(errors[0].childNodes[0].nodeValue);
					}
					return false;
				}
				else
				{
					for(var i=0; i<calc_adjust_fields.length; i++) 
					{
						var tag_name = calc_adjust_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						//alert(tag_name + ': ' + update_nodes.length);						
						if (update_nodes.length > 0)
						{
							var update_element = document.getElementById(tag_name);
							if (update_element != null)
							{
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
						}
						else
						{
							var update_element = document.getElementById(tag_name);
							if (update_element != null)
							{
								update_element.innerHTML = "&nbsp;";
							}							
						}
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}

function getGlobalCalcAdjustFlag()
{
	var url = ajaxurl + "/ACCRCalcAdjustUpdater";
	calcAdjustHttp = createXmlHttpRequestObject();

	calcAdjustHttp.open("POST", url );

	calcAdjustHttp.onreadystatechange = handleResponseGetCalcAdjustFlag;
	calcAdjustHttp.send(null);	
}

function handleResponseGetCalcAdjustFlag(){

   	if (calcAdjustHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (calcAdjustHttp.status == 200) {
			var response = createResponseXML(calcAdjustHttp.responseText);					
			//alert(calcAdjustHttp.responseText);
			try{                                    
				var adjustText = response.getElementsByTagName("Adjust");
				var text = 'No';
				if (adjustText.length > 0 && adjustText[0].firstChild.nodeValue.toLowerCase() == 'true')
				{					
					text = 'Yes';
				}
				
				var updateText = document.getElementById('calc_adjust_flag');
				if (updateText != null)
				{
					updateText.innerHTML = text;
				}
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}