var dataDicHttp = null;

var ptb_mandatory = true;
var selectedTableIndex = -1; // default is add
var selectedDB = null;
var selectedTable = null;
var selectedAttId = null;
var ddMappingArray = new Array();

function getTablesOfDBRequest(db_name) 
{
	// if oldName is null or empty, consisder this is add report request
	var xml = generateGetDDRequestXML(db_name, '', 'Init');	
	
	var ddMapperURL = ajaxurl + "/DataDicAdminHandler";
	var params = "xml=" + xml;

	//alert(xml);

	dataDicHttp = createXmlHttpRequestObject();

	dataDicHttp.open("POST", ddMapperURL, true);

	dataDicHttp.onreadystatechange = getTablesOfDBResponse;		
	    
    dataDicHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    dataDicHttp.setRequestHeader("Content-length", params.length);
    dataDicHttp.setRequestHeader("Connection", "close");	
	
	dataDicHttp.send(params);
}

function generateGetDDRequestXML(db_name, db_table, dd_action)
{
	var xml = "";
	xml += "<AjaxDDParameterRequest>";
	xml += "<dd_action>" + dd_action + "</dd_action>";
	xml += "<dd_dbname>" + db_name + "</dd_dbname>";
	xml += "<dd_table>" + db_table + "</dd_table>";	
	xml += "</AjaxDDParameterRequest>";
	return xml;
}

function getTablesOfDBResponse(){

   	if (dataDicHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (dataDicHttp.status == 200) {
			var response = createResponseXML(dataDicHttp.responseText);					
			//alert(dataDicHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Data dictionary initialization failed!');
					return false;
				}
				else
				{
					// get db_name requested
					var dbNameNodeList = response.getElementsByTagName("DD_DBName");
					if (dbNameNodeList && dbNameNodeList.length > 0)
					{
						selectedDB = dbNameNodeList[0].firstChild.nodeValue;
					} 

					var tblNodeList = response.getElementsByTagName("DD_Table");
					if (tblNodeList != null)
					{
						var numTables = tblNodeList.length;
						// update table list box
						var tbList = document.getElementById('dd_table');
						if (tbList)
						{
							selectedTable = tblNodeList[0].firstChild.nodeValue;						
							// clear the list						
							var firstChild = tbList.firstChild;
							while (firstChild)
							{
								tbList.removeChild(firstChild);
								firstChild = tbList.firstChild;
							}
																			
							for(var i=0; i<numTables; i++) 
							{
								var tbName = tblNodeList[i].firstChild.nodeValue;
								var optionEle = document.createElement('OPTION');
								optionEle.value = tbName;
								optionEle.innerHTML = tbName;	
								tbList.appendChild(optionEle);
							} 
						}
					}
					
					// get att-alias maps for selected table
					getDDRequest(selectedDB, selectedTable);
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function getDDRequest(db_name, db_table) 
{
	// if oldName is null or empty, consisder this is add report request
	var xml = generateGetDDRequestXML(db_name, db_table, 'Fetch');	
	
	var ddMapperURL = ajaxurl + "/DataDicAdminHandler";
	var params = "xml=" + xml;

	//alert(xml);

	dataDicHttp = createXmlHttpRequestObject();

	dataDicHttp.open("POST", ddMapperURL, true);

	dataDicHttp.onreadystatechange = getDDResponse;		
	    
    dataDicHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    dataDicHttp.setRequestHeader("Content-length", params.length);
    dataDicHttp.setRequestHeader("Connection", "close");	
	
	dataDicHttp.send(params);
}

function getDDResponse(){

   	if (dataDicHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (dataDicHttp.status == 200) {
			var response = createResponseXML(dataDicHttp.responseText);					
			//alert(dataDicHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Data dictionary initialization failed!');
					return false;
				}
				else
				{
					// update selected DB and table
					var dbNameNodeList = response.getElementsByTagName("DD_DBName");
					if (dbNameNodeList && dbNameNodeList.length > 0)
					{
						selectedDB = dbNameNodeList[0].firstChild.nodeValue;
					} 

					var tblNodeList = response.getElementsByTagName("DD_Table");
					if (tblNodeList && tblNodeList.length > 0)
					{
						selectedTable = tblNodeList[0].firstChild.nodeValue;
					}			
							
					// get the mappings ffrom the response
					var mappingNodeList = response.getElementsByTagName("DD_Mapping");
					if (mappingNodeList)
					{
						
						ddMappingArray = new Array();
						
						for(var i=0; i<mappingNodeList.length; i++) 
						{
							var mappingNode = mappingNodeList[i];							
							var attribute = '';
							var alias = '';
							var description = '';
							for(var j=0; j<mappingNode.childNodes.length; j++) 
							{
								if (mappingNode.childNodes[j].nodeName == 'attribute')
								{
									if (mappingNode.childNodes[j].firstChild)
									{
										attribute = mappingNode.childNodes[j].firstChild.nodeValue;
									}									
								}
								if (mappingNode.childNodes[j].nodeName == 'alias')
								{
									if (mappingNode.childNodes[j].firstChild)
									{									
										alias = mappingNode.childNodes[j].firstChild.nodeValue;
									}
								}
								if (mappingNode.childNodes[j].nodeName == 'description')
								{
									if (mappingNode.childNodes[j].firstChild)
									{									
										description = mappingNode.childNodes[j].firstChild.nodeValue;
									}
								}																
							}
												
							// add to mappings array
							if (attribute != null && attribute.length > 0)
							{		
								//alert(attribute + ' - ' + alias + ' - ' + description);							
								ddMappingArray.push(Array(attribute, alias, description));
							} 
						}
					}
					
					// update attribute list
					updateAttributeListHtml();
				}	
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}

}

function updateAttributeListHtml ()
{
	if (selectedTable)
	{
		var tableSelectedEle = document.getElementById('dd_table_selected');
		if (tableSelectedEle)
		{
			tableSelectedEle.value = selectedTable;
		}
		
		var attributeListEle = document.getElementById('dd_list_att');
		if (attributeListEle)
		{
			attributeListEle.innerHTML = '';
			for(var i=0; i<ddMappingArray.length; i++) 
			{
				var ddMapping = ddMappingArray[i];
				var attribute = ddMapping[0];
				var attId = attribute;
				attributeListEle.innerHTML = attributeListEle.innerHTML + 
					'<div id=\'' + attId + '\' onclick="attSelect(\'' +attId +'\')"><label>' +
					attribute + '</label></div>'
			}
		} 
	}
}

function attSelect(attId)
{
	var alias = null; // alias for selected attribute
	var description = null; // description for selected attribute
	// reset all attributes and also find the alias
	for(var i=0; i<ddMappingArray.length; i++) 
	{
		var ddMapping = ddMappingArray[i];
		var attribute = ddMapping[0];
		var tempAttDiv = document.getElementById(attribute);				
		if (tempAttDiv)
		{
			tempAttDiv.style.backgroundColor = '#fff';
		} 
		
		if (attribute == attId)
		{
			alias = ddMapping[1];
			description = ddMapping[2];
		}
	}	
	
	//alert(attId + ' - ' + alias + ' - ' + description)
	var attSelectedDiv = document.getElementById(attId);
	if (attSelectedDiv)
	{
		attSelectedDiv.style.backgroundColor = '#CCC';
		selectedAttId = attId;
	}
	
	// update alias & description
	var aliasEle = document.getElementById('dd_alias');
	if (aliasEle)
	{
		aliasEle.value = alias;
	} 
	
	var descriptionEle = document.getElementById('dd_description');
	if (descriptionEle)
	{
		descriptionEle.value = description;
	} 	
	
}

function updateSelectedMapping ()
{
	var alias = null; // alias for selected attribute
	var description = null; // description for selected attribute
	
	var aliasEle = document.getElementById('dd_alias');
	if (aliasEle)
	{
		alias = aliasEle.value;
	} 
	
	var descriptionEle = document.getElementById('dd_description');
	if (descriptionEle)
	{
		description = descriptionEle.value;
	}
	
	if (selectedAttId == null || document.getElementById(selectedAttId) == null)
	{
		alert('Please select an attribute!');
	} 
	
	// reset all attributes and also find the alias
	for(var i=0; i<ddMappingArray.length; i++) 
	{
		var ddMapping = ddMappingArray[i];
		var attribute = ddMapping[0];
		
		if (attribute == selectedAttId)
		{
			ddMapping[1] = alias;
			ddMapping[2] = description;
			alert('The attribute has been marked as to be updated.\nTo save all the updates, please click \'Save\' button bellow.');
			break;
		}
	}	
}

function saveMappingsRequest() 
{
	// if oldName is null or empty, consisder this is add report request
	var xml = generateSaveMappingsRequestXML(selectedDB, selectedTable, 'Update');	
	
	var ddMapperURL = ajaxurl + "/DataDicAdminHandler";
	var params = "xml=" + xml;

	//alert(xml);

	dataDicHttp = createXmlHttpRequestObject();

	dataDicHttp.open("POST", ddMapperURL, true);

	dataDicHttp.onreadystatechange = saveMappingsResponse;		
	    
    dataDicHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    dataDicHttp.setRequestHeader("Content-length", params.length);
    dataDicHttp.setRequestHeader("Connection", "close");	
	
	dataDicHttp.send(params);
	ddWhizyVisi (true);
}

function generateSaveMappingsRequestXML(db_name, db_table, dd_action)
{
	var xml = "";
	xml += "<AjaxDDParameterRequest>";
	xml += "<dd_action>" + dd_action + "</dd_action>";
	xml += "<dd_dbname>" + db_name + "</dd_dbname>";
	xml += "<dd_table>" + db_table + "</dd_table>";	
	xml += "<dd_mappings>";		
	if (ddMappingArray)
	{
		for(var i=0; i<ddMappingArray.length; i++) 
		{
			var ddMapping = ddMappingArray[i];
			var attribute = ddMapping[0];
			var alias = ddMapping[1];
			var description = ddMapping[2];
			description = description.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
			xml += "<dd_mapping>";
			xml += "<attribute>" + attribute + "</attribute>";			
			xml += "<alias>" + alias + "</alias>";
			xml += "<description>" + description + "</description>";						
			xml += "</dd_mapping>";			
		}
	}
	xml += "</dd_mappings>";			
	xml += "</AjaxDDParameterRequest>";
	return xml;
}

function saveMappingsResponse(){

   	if (dataDicHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (dataDicHttp.status == 200) {
			var response = createResponseXML(dataDicHttp.responseText);					
			//alert(dataDicHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Data dictionary initialization failed!');
					return false;
				}
				else
				{
					// update selected DB and table
					var resultNodeList = response.getElementsByTagName("Result");
					if (resultNodeList && resultNodeList.length > 0)
					{
						var result = resultNodeList[0].firstChild.nodeValue;
						if (result == 'true')
						{
							alert('Data dictionary has been updated!');
						}
						else
						{
							alert('Data dictionary update failed!');
						}
					} 					
				}
				ddWhizyVisi (false);	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}
}

function cancelMapper()
{
	window.location.replace('/content/pl/sql_report/data_dictionary.html');
}

function ddWhizyVisi (visible)
{
	var spinImg = document.getElementById('dd_spining_img');
	var formContainer = document.getElementById('dd_mapper_form');
	if (spinImg && formContainer)
	{
		if (visible)
		{		
			spinImg.innerHTML = "<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"running...\" /></span>";		
			spinImg.style.display = "";			
			formContainer.style.display = "NONE";
		}
		else
		{
			spinImg.style.display = "NONE";
			formContainer.style.display = "";								
		}	
	}
}