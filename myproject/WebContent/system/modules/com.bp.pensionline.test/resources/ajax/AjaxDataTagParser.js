// JavaScript Document
// JavaScript Document 
/*Base to handle tag*/
var startDataChars = "[[";
var stopDataChars = "]]";
var startMetaDataChars = "((";
var stopMetaDataChars = "))";	
var PL_PARAM = "{PL_PARAM}";
var PL_UPDATE_FIELD = "f_{PL_PARAM}_0";
var request_array = new Array();
var conditionTagArray= new Array();
var tableTagArray= new Array();
var dictionaryArr = new Array();
//var shortDictionary = new Array();
var caveatArr=new Array();
//var shortCaveat=new Array();
var faqArray=new Array();
//var shortFaqArray = new Array();
    
var ErrorTag = new Array();
var CondErrorTag = new Array();    
var isIE = false; /*Check is Internet Explorer*/
var errorCount = 0;
var condErrorCount = 0;
var limitErrorCount = 3;
var ErrTimeOut = 5000;
var idTimeOut = 0;
var idCondTimeOut = 0;
/*Url to send xml*/

var staticURL = ajaxurl + "/GetParameterHandler?xml=";
var staticCondURL = ajaxurl + "/GetConditionalValueHandler?xml=";
var staticTableURL = ajaxurl + "/GetHTMLTableHandler?xml=";
var staticDicURL = ajaxurl + "/GetParameterHandlerDictionary?xml=";
var staticCaveURL = ajaxurl + "/GetParameterHandlerCaveat?xml=";
var newUrl = ajaxurl + "/ChangeAddressHandler?xml=";
var faqUrl = ajaxurl + "/GetParameterHandlerFaq?xml=";	
/*End Url*/

var scriptTemplate = "<span id=\"{PL_PARAM}\"></span>" + "<span id=\"{PL_PARAM}_spinning\" style=\"visibility: hidden;\"><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"waiting\" /></span>";
var offsetfromcursorX = 12;
var offsetfromcursorY = 10;
var offsetdivfrompointerX = 10;
var offsetdivfrompointerY = 14;
var ie = document.all;
var ns6 = document.getElementById && ! document.all;
var enabletip = false;	
var ns6 = document.getElementById && ! document.all;
var enabletip = false;

if (ie || ns6)
	var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";

var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";

function ietruebody() {
	return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
}
	
/*Detech browser*/	

	isIE = detectBrowser();
    function detectBrowser(){
    
        var browser=navigator.appName
        
        var b_version=navigator.appVersion
        
        version=parseFloat(b_version)
        
        if (browser=="Netscape"){
//            return false;
        }else if(browser=="Microsoft Internet Explorer"){
            return true;
        }
    }
	
/*End detech browser*/

function replaceAllNew(strReplaceAll, replacement, newstr){
	var intIndexOfMatch = strReplaceAll.indexOf( replacement );
  
	// Loop over the string value replacing out each matching
	// substring.
	while (intIndexOfMatch != -1){
		// Relace out the current instance.
		strReplaceAll = strReplaceAll.replace( replacement, newstr )
  
		// Get the index of any next matching substring.
		intIndexOfMatch = strReplaceAll.indexOf( replacement );
	}

	return strReplaceAll;
}
function encodeHTMLNew(str){
	str = replaceAll(str, "&gt;", "greater_than");
	str = replaceAll(str, "&lt;", "less_than");
	str = replaceAll(str, "Â£", "pound");
 	str = replaceAll(str, "%", "percentage");
	str = replaceAll(str, "&", "and");
	return str;
}

/*Get all normal tag from html pages*/

function ParseHtml()
{		
	if(!isIE){

		var rootNode = document.body;
		TreeWalkIE(rootNode);
		
	}else{

		var rootNode = document.body;
		TreeWalkIE(rootNode);
		
	}
}
function callTagHandle()
{
	if(dictionaryArr.length)
	{
		var next = dictionaryArr[0];		
		dictionaryArr.shift();			
		DictionaryGetData(next[0], next[1], next[2]);	
	}
	if(caveatArr.length)
	{
		var next = caveatArr[0];		
		caveatArr.shift();			
		CaveAtGetData(next[0], next[1]);	
	}
	if(faqArray.length)
	{
		var next = faqArray[0];		
		faqArray.shift();			
		FAQGetData(next[0], next[1]);	
	}
	if(tableTagArray.length){
		//call ajax to get data	as stack
		var next = tableTagArray.shift();
		
		startGetTableTagData(next[0], next[1]);
	}	
	if(conditionTagArray.length){
		//call ajax to get data	as stack
		var next = conditionTagArray.pop();
		
		startGetConditionTagData(next[0], next[1]);
     	idCondTimeOut=setInterval ("retryCondErrorTag()",ErrTimeOut );
     	condErrorCount = 0;
     	CondErrorTag = new Array();
	}

	if(request_array.length){
		//affter replace all [[Parameter]] then call ajax to get data	
		var next = request_array[0];
		
		request_array.shift();
	
		startGetData(next[0], next[1]);	
		idTimeOut=setInterval ("retryErrorTag()",ErrTimeOut );	
		errorCount = 0;	
		ErrorTag = new Array();	
	}   
}
function retryErrorTag()
{
	// return if request array still contain attributes not submitted
	if (request_array.length) return;
	
	if(ErrorTag.length)
	{
		request_array = ErrorTag;
		//request_array = ErrorTag; HUY: push it to request_array not replace
		ErrorTag=new Array();
		if(request_array.length)
		{
	        var next = request_array[0];	
			request_array.shift();	
			startGetData(next[0], next[1]);
		}		
		if(errorCount>=limitErrorCount )
		{	
			clearInterval ( idTimeOut);
		}		
		errorCount++;
	}
}

function retryCondErrorTag()
{
	// return if request array still contain attributes not submitted
	if (conditionTagArray.length) return;
	
	if(CondErrorTag.length)
	{
		// rebuild up the condition tag stack
		while (CondErrorTag.length) {
			conditionTagArray.push(CondErrorTag.pop());
		}
		
		CondErrorTag=new Array();
		
		if(conditionTagArray.length)
		{
			var next = conditionTagArray.pop();	
			startGetConditionTagData(next[0], next[1]);
		}		
		if(condErrorCount>=limitErrorCount )
		{	
			clearInterval ( idCondTimeOut);
		}		
		condErrorCount++;
	}
}
/*End Get All tag from html*/
/*Walk with normal tag*/
function TreeWalkIE(rootNode){
        
        if(rootNode == null){
            return;
        }
        
        var length = rootNode.childNodes.length;
		var i=0;
		    		    
		for(i=0; i<length; i++){
		    var child = rootNode.childNodes[i];
		    
		    if(child.nodeType == 1)//is element node
		    {		        
		        TreeWalkIE(child);
		        
		    }else if(child.nodeType == 3)//is text node
		    {
		        ExtractParameterIE(child);

		    }
        }    
    }
/*End Walk*/
/**
*Start modify tag 
/**/
function ExtractParameterIE(child){
    var parameter = child.nodeValue;
	var startCharPro= "(";
	var stopCharPro=")";
	
	// To keep the changes at minimum, conditional and table are handled in a diffirrent part. //HUY: MODIFY HERE
	var startMetaDataPos = parameter.indexOf(startMetaDataChars);
	var endMetaDataPos = parameter.indexOf(stopMetaDataChars);
	if(startMetaDataPos >= 0 && endMetaDataPos > startMetaDataPos)
	{		
		var metaString = parameter.substring(startMetaDataPos + 2, endMetaDataPos);
		var conditionPos = metaString.toLowerCase().indexOf("conditional:");	
		var tablePos = metaString.toLowerCase().indexOf("table(");	
		if(conditionPos >= 0)
		{
			// Parse string Conditional:expression|divId|message
			child.parentNode.style.display="none";
			metaString = metaString.substring("Conditional:".length);
			var conditionParts = metaString.split('|');
			var appliedElement = null;
			if (conditionParts.length > 1) {							
				appliedElement = document.getElementById(conditionParts[1]);										
			}
			
			if(appliedElement == null)
			{
				appliedElement = child.parentNode.nextSibling;
				var appliedElementFound = false;
				while (!appliedElementFound && appliedElement)			// FireFox has a text node with empty value
				{
					if (appliedElement.nodeType == 1) {					// Element node in both IE and FF
						appliedElementFound = true;
					}
					else {
						appliedElement = appliedElement.nextSibling;
					}				
				}
				
				// if cannot find the next sibling of parent (<SPAN>), find the next sibling of grand-parent
				// in the next version of OpenCMS, FCKEditor sometimes translate the <SPAN> store conditional
				// tag inside a <p> tag.
				if (!appliedElementFound && child.parentNode.parentNode != null)
				{
					appliedElement = child.parentNode.parentNode.nextSibling;						
					while (!appliedElementFound && appliedElement)			// FireFox has a text node with empty value
					{
						if (appliedElement.nodeType == 1) {					// Element node in both IE and FF
							appliedElementFound = true;
						}
						else {
							appliedElement = appliedElement.nextSibling;
						}										
					}					
				}
				if (appliedElementFound) {	
					appliedElement.id = generateNextID("conditional");					
				}
			}
			
			if (appliedElement != null)
			{
				var tmp = conditionParts[0].replace(/</g, '&lt;').replace(/>/g,'&gt;');
				conditionTagArray.push(Array("<![CDATA[" + noWhiteSpace(tmp) + "]]>", appliedElement.id));	
			}		
		}
		else if(tablePos >= 0)
		{	
			//alert(metaString);
			// Parse the string Table(member value):table header|message
			var startTablePos = metaString.indexOf(startCharPro);
			var endTablePos = metaString.indexOf(stopCharPro);;						
			if (startTablePos >= 0 && endTablePos > startTablePos) {	
				var tableName = metaString.substring(startTablePos + 1, endTablePos);	
				var appliedElement = child.parentNode;
				if (!appliedElement.id)
				{
					appliedElement.id = generateNextID("table");
				}	
				tableTagArray.push(Array(tableName, appliedElement.id));										
			}			
		}	
		return;
	}
	else
	{		
		startDataPos = parameter.indexOf(startDataChars);
		stopDataPos = parameter.indexOf(stopDataChars);
		if(startDataPos >= 0 && stopDataPos > startDataPos)	// Assume each text node only have 1 meta data tag
		{		
			var metaString	= parameter.substring(startDataPos + 2, stopDataPos);
			var beforeText 	= parameter.substring(0, startDataPos);
			var afterText 	= parameter.substring(stopDataPos + stopDataChars.length);
			var parent = child.parentNode;
						
			if(metaString.indexOf("Dictionary:")>=0)
			{						
				var startParam=tmpParam.substring(0, startDataPos);
				var contParam=tmpParam.substring(stopDataPos + stopDataChars.length);
				var phrase = metaString.substring("Dictionary:".length);
				var tagID = generateNextID("dictionary");
				
				insertElement(child, beforeText, afterText, tagID, 'span');
				removeRedundantTag(parent);	
				//shortDictionary.push(Array("", PhraseName, tagID));
				dictionaryArr.push(Array("", phrase, tagID));
			}				
			else if(metaString.indexOf("Dictionary(") >=0 )
			{
				var startProPos = metaString.indexOf(startCharPro);
				var stopProPos = metaString.indexOf(stopCharPro);
				
				var scheme = metaString.substring(startProPos + startCharPro.length, stopProPos);
				var phrase = metaString.substring(stopProPos + stopCharPro.length + 1); // '):'

				if(phrase.length > 0)
				{
					var tagID = generateNextID("dictionary");					
					insertElement(child, beforeText, afterText, tagID, 'span');
					removeRedundantTag(parent);						
					dictionaryArr.push(Array(scheme , phrase, tagID));			
				}
			}
			else if(metaString.indexOf("Caveat:")>=0)
			{
				var startParam=tmpParam.substring(0,tmpParam.indexOf(startDataChars));
				var contParam=tmpParam.substring(tmpParam.indexOf(stopDataChars) + 2,tmpParam.length);
				var PhraseName = metaString.substring("Caveat:".length);
				var tagID = generateNextID("caveat");
				insertElement(child, beforeText, afterText, tagID, 'span');
				removeRedundantTag(parent);
				caveatArr.push(Array("", PhraseName));
			}				
			else if(metaString.indexOf("Caveat(") >=0)
			{
				var startProPos = metaString.indexOf(startCharPro);
				var stopProPos = metaString.indexOf(stopCharPro);
				
				var scheme = metaString.substring(startProPos + startCharPro.length, stopProPos);
				var phrase = metaString.substring(stopProPos + stopCharPro.length + 1); // '):'
				
				if(phrase.length>0)
				{
					var tagID = generateNextID("caveat");				
					insertElement(child, beforeText, afterText, tagID, 'span');
					removeRedundantTag(parent);		
					caveatArr.push(Array(scheme, phrase));
				}	
			}
			else if(metaString.indexOf("FAQ:") >=0)
			{        
				var phrase = metaString.substring("FAQ:".length, metaString.length);
				var tagID = generateNextID('faq');

				insertElement(child, beforeText, afterText, tagID, 'span');
				removeRedundantTag(parent);	
				
				faqArray.push(Array("", phrase));
			}				
			else if(metaString.indexOf("FAQ(") >=0)
			{
				var startProPos = metaString.indexOf(startCharPro);
				var stopProPos = metaString.indexOf(stopCharPro);
				
				var scheme = metaString.substring(startProPos + startCharPro.length, stopProPos);
				var phrase = metaString.substring(stopProPos + stopCharPro.length + 1); // '):'
				
				if(phrase.length>0)
				{
					var tagID = generateNextID('faq');
					insertElement(child, beforeText, afterText, tagID, 'span');
					removeRedundantTag(parent);				
					faqArray.push (Array(scheme, phrase));
				}	
			}
			else if(metaString.toLowerCase().indexOf("payslip:") >=0)
			{
				// Ignore
			}
			else
			{
				//generate update field
				var updateField = PL_UPDATE_FIELD;
				
				updateField = updateField.replace(PL_PARAM, metaString);

				//alert(updateField);
				var i = 1;
				while(true){				
					if(document.getElementById(updateField + i) == null){
						updateField += i;					
						break;
					}else{
						++i;
					}
				}
				
				//replacement body
				var strBody = child.nodeValue;
				
				var strContentReplacement = scriptTemplate;
				
				strContentReplacement = strContentReplacement.replace(PL_PARAM, updateField);
				
				strContentReplacement = strContentReplacement.replace(PL_PARAM, updateField);
		
				strBody = strBody.replace(startDataChars + metaString + stopDataChars, strContentReplacement);
				
				child.parentNode.innerHTML = strBody;
				
				//play spinning image
				visiWhizzy(updateField, true);
				
				//add new request to array
				request_array.push(Array(metaString, updateField));
			}
		}	
	}
}

function insertElement (child, leftText, rightText, id, elementName)
{
	var parent = child.parentNode;
	
	if (leftText.length)
	{
		var leftTextNode = document.createTextNode();
		leftTextNode.nodeValue = leftText;
		parent.insertBefore(leftTextNode, child);
	}
	
	var element = document.createElement(elementName);
	element.id = id;
	parent.insertBefore(element, child);
	
	if (rightText.length)
	{
		var rightTextNode = document.createTextNode();
		rightTextNode.nodeValue = rightText;
		parent.insertBefore(rightTextNode, child);
	}
	
	parent.removeChild(child);	
}

function removeRedundantTag(pa)
{
	var grandPa = pa.parentNode;
	var children = pa.childNodes;
	var numElementChild = 0;
	var hasTextNode = false;
	var onlyChild = null;
	for(var i=0; i<children.length; i++) {
		var child = children[i];
		if (child.nodeType == 1)
		{ 
			numElementChild += 1;
			onlyChild = child;
		}
		else if (child.nodeType == 3)
		{
			if(child.nodeValue.length)
			{ 
				hasTextNode = true;
				break;
			}
		}
	}
	
	if (!hasTextNode && numElementChild == 1 && pa.tagName == onlyChild.tagName)
	{
		grandPa.insertBefore(onlyChild, pa);
		grandPa.removeChild(pa);
	}
}
/*
 * Show spining image while waiting for server data
 */
function visiWhizzyNew(name, vis)
{
	e = document.getElementById( name );
	if(e){				
		if(vis) e.innerHTML = waitingImage;				
		else    e.innerHTML = '';
	}	
}

/*End*/
/*Parse ConditionTag*/
/*hadle show tooltip */

function findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }
function ViewTag(tagID,this1)
{
	if(document.getElementById(tagID).style.display=="none")
	{
                document.getElementById(tagID).style.display="";
	}

	if(document.getElementById(tagID).style.visibility=="visible")
	{
                document.getElementById(tagID).style.visibility="hidden";
	}
	else
	{
		document.getElementById(tagID).style.visibility="visible";	
		document.getElementById(tagID).style.pixelTop=parseInt(findPosY(this1)+20);
		document.getElementById(tagID).style.pixelLeft=parseInt(findPosX(this1));
	}
}
function HideTag(tagID)
{	
	document.getElementById(tagID).style.visibility="hidden";
}
/*End*/
/*Old function*/
function formatNumber(value){
			value = Math.round(value);
			var num = new NumberFormat();
			num.setInputDecimal(',');
			num.setNumber(value); 
			num.setPlaces('0', false);
			num.setCurrencyValue('?');
			num.setCurrency(true);
			num.setCurrencyPosition(num.LEFT_OUTSIDE);
			num.setNegativeFormat(num.LEFT_DASH);
			num.setNegativeRed(false);
			num.setSeparators(true, ',', ',');
			value = num.toFormatted();
			return value;
		}
		
		// no decimal place
		function formatNoDecimal(value)
		{
			var num = new NumberFormat();
			num.setInputDecimal('.');
			num.setNumber(value); 
			num.setPlaces('0', false);
			num.setCurrencyValue('?');
			num.setCurrency(false);
			num.setCurrencyPosition(num.LEFT_OUTSIDE);
			num.setNegativeFormat(num.LEFT_DASH);
			num.setNegativeRed(false);
			num.setSeparators(false, ',', ',');
			value = num.toFormatted();
			return value;
		}
		
		function populateFieldValue(which, what, format) {
		  var e = document.getElementById(which);
		  if(e) {			
		  	e.innerHTML = formatData(what, format);
		  	visiWhizzy(which, false);	
		  }
		}
		
		//To hide or display text
		function display(){

		    var display = document.getElementById("parameter");
			display.style.display = "inline";
		
		}
		
		function hide(){
		    var veraDisplay = document.getElementById("vera");
			veraDisplay.style.display = "none";
		}		
		
		function generateXML(name, update_field){
			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<Member>\n";
			xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
		    xml += "		<Refno>" + crefno + "</Refno>\n";
		    xml += "	</Member>\n";
			xml += "	<RequestedTag>" + name + "</RequestedTag>\n";
		    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
			xml += "</AjaxParameterRequest>\n";
			return xml;
		}
		function generateCaveAtXML(scheme, phare){

			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<Caveat>\n";
			xml += "		<Phrase>" + phare + "</Phrase>\n";
		    xml += "		<Scheme>" + scheme + "</Scheme>\n";
		    xml += "	</Caveat>\n";
		
			xml += "</AjaxParameterRequest>\n";
			return xml;
		}
		function generateFAQXML(scheme, phare){

			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<Faq>\n";
			xml += "		<Phrase>" + phare + "</Phrase>\n";
		    xml += "		<Scheme>" + scheme + "</Scheme>\n";
		    xml += "	</Faq>\n";
		
			xml += "</AjaxParameterRequest>\n";
			return xml;
		}
		function generateDictionaryXML(scheme, phare, updateField){

			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<Dictionary>\n";
			xml += "		<Phrase>" + phare + "</Phrase>\n";
		    xml += "		<Scheme>" + scheme + "</Scheme>\n";
		    xml += "		<UpdateField>" + updateField+ "</UpdateField>\n";
		    xml += "	</Dictionary>\n";
		
			xml += "</AjaxParameterRequest>\n";
			return xml;
		}
		function generateAddressXML(strHome,strStreet,strTown,strDistrict,strCounty,strPostcode,strCountry){

			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<RequestedTag>Address</RequestedTag>\n";
			xml += "	<Home>" + strHome + "</Home>\n";
		    xml += "		<Street>" + strStreet + "</Street>\n";
		    xml += "		<Town>" + strTown + "</Town>\n";
		    xml += "		<District>" + strDistrict + "</District>\n";
		    xml += "		<County>" + strCounty + "</County>\n";
		    xml += "		<Postcode>" + strPostcode + "</Postcode>\n";
		    xml += "		<Country>" + strCountry + "</Country>\n";

		
			xml += "</AjaxParameterRequest>\n";
			return xml;
		}

		function visiWhizzy(name, vis) {
			e = document.getElementById( name + "_spinning");
			if(e){				
				if(vis)
				{
					e.style.visibility = 'visible';
					e.style.display = 'block';
				}
				else
				{					
					e.style.display = 'none';
				}
				
//				if(vis) e.style.display = 'block';				
//				else    e.style.display = 'none';
			}
		}
		
		//---------------------
		//Convert Functions
		function formatData(value, type){
			if(type == "Shortdate")
				return formatTime(value, "Shortdate");
			if(type == "Currency")
				return formatCurrency(value);
			if(type == "Text")
				return formatText(value);
			if(type == "Number")
				return formatNumber(value);

			//Unformated value
			return value;
		}
		
		function formatNumber(value){
			return value;
		}
	
		function formatText(value){
			return value;
		}

		function formatCurrency(num){
			num = num.toString().replace(/\$|\,/g,'');
			if(isNaN(num))
				num = "0";
			sign = (num == (num = Math.abs(num)));
			num = Math.floor(num*100+0.50000000001);
			cents = num%100;
			num = Math.floor(num/100).toString();
			
			if(cents<10)
				cents = "0" + cents;
			for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+','+
			num.substring(num.length-(4*i+3));
			
			return (((sign)?'':'-') + '?Ã¯Â¿Â½ÃÂ£' + num + '.' + cents);
		}

		function formatTime(value,format){

		    var theDate = new Date();
		    theDate.setTime(value);
		    if(format == "Shortdate")
			    dateString = theDate.getDate() + "/" + (theDate.getMonth() + 1) + "/" + theDate.getFullYear();
			else
			    dateString = theDate.getDate() + "/" + (theDate.getMonth() + 1) + "/" + theDate.getFullYear();
			    
			return dateString;
		}

function UpdateAddress(strHome,strStreet,strTown,strDistrict,strCounty,strPostcode,strCountry)
{
	var update_field = "Address";
//alert(document.getElementById("new_option").value);
	var xml = generateAddressXML(strHome,strStreet,strTown,strDistrict,strCounty,strPostcode,strCountry);
//alert(xml);	
		// Build the URL to connect to
		var url = newUrl + escape(xml);  
//alert(url); 
		AddressHttp.open("POST", url);
		
		AddressHttp.onreadystatechange = AddressHandle;
		
		AddressHttp.send(null);/**/
}


/*End*/
/*No White space*/
function noWhiteSpace(s){

if((s==null)||(typeof(s)!='string')||!s.length)return'';return s.replace(/\s+/g,'');
}
/*End*/
var httpImail = createXmlHttpRequestObject();
function ImailHandler()
{
   var strName = document.getElementById("f_Name_01").innerHTML;
   var strNino = document.getElementById("f_Nino_01").innerHTML;
   var strAddress = document.getElementById("f_Address_01").innerHTML;
   var strEmailAddress = document.getElementById("f_EmailAddress_01").innerHTML;
   var strSubject = document.getElementById("Icategory").value;
   var strQuery = document.getElementById("Iquery").innerHTML;
      
   var strSubjectNew = encodeHTMLNew(strSubject);
   var strQueryNew = encodeHTMLNew(strQuery);

   
   if(strName.indexOf("[[Name]]")>0)
   {
     strName="";
   }
   if(strAddress.indexOf("[[Address]]")>0)
   {
     strAddress="";
   }
   if(strNino.indexOf("[[Nino]]")>0)
   {
     strNino ="";
   }
   if(strEmailAddress.indexOf("[[EmailAddress]]")>0)
   {
     strEmailAddress ="";
   }
  var xml = "";
   xml += "<AjaxParameterRequest>\n";
   xml += "<New>\n";
   xml += "<Form>Imail</Form>\n";
   xml += "<Imail>\n";
   xml += "<Name>"+strName+"</Name >\n";            
   xml += "<NI>"+strNino+"</NI>\n";                 
   //xml += "<Address>"+strAddress+"</Address>\n";       
   xml += "<emailaddress>"+strEmailAddress +"</emailaddress>\n";
   xml += "<Subject>"+strSubjectNew+"</Subject>\n";       
   xml += "<Query>"+strQueryNew+"</Query>\n";           
   xml += "</Imail>\n";
   xml += "</New>\n";
   xml += "</AjaxParameterRequest>\n";
//alert(xml);

   var imailUrl = ajaxurl + "/ImailHandler?xml="+xml;
		httpImail.open("POST", imailUrl );

		httpImail.onreadystatechange = httpImailHandlerResponse;
		
		httpImail.send(null);
}
function httpImailHandlerResponse()
{
   if (httpImail.readyState == 4) {								
		// Check that a successful server response was received
      	if (httpImail.status == 200) {
			var response = createResponseXML(httpImail.responseText);	
			var error = response.getElementsByTagName("Error").length;
			var divimail=document.getElementById("imail");				
			if(error > 0)
			{								
				var lblmess=document.getElementById("ERROR");
				lblmess.style.display = "block";
				divimail.style.display="none";
			}
			else
			{
				var lblmess=document.getElementById("SUCCESS");
				lblmess.style.display = "block";
				divimail.style.display="none";
			}
		}
	}	
}
/*
*ChangePhone
*/
// JavaScript Document
var httpChangePhone = createXmlHttpRequestObject();
function ChangePhoneHandle()
{
	var intPhone = document.getElementById("tbPhone");
	var intMobile = document.getElementById("tbMobile");
	var intFax = document.getElementById("tbFax");

	if(intFax.value =="" && intPhone.value=="" && intMobile.value=="")
	{
		alert("Fields cannot be blank !");
		return false;
	}	
	else 
	{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";
		xml += "	<New>\n";
		xml += "<Form>ChangePhone</Form>\n"
		xml += "<ChangePhone>\n";
		xml += "<TelephoneNumber>"+intPhone.value+"</TelephoneNumber>\n";
		xml += "<MobileNumber>"+intMobile.value+"</MobileNumber>\n";
		xml += "<FaxNumber>"+intFax.value+"</FaxNumber>\n";
		xml += " </ChangePhone>\n";
		xml += "	</New>\n";		
		xml += "</AjaxParameterRequest>\n";
		var eowUrl = ajaxurl + "/ChangePhoneHandler?xml="+xml;
		httpChangePhone.open("POST", eowUrl);

		//alert(eowUrl);
		httpChangePhone.onreadystatechange = ChangePhoneHandlerResponse;
		
		httpChangePhone.send(null);
	}
}
function ChangePhoneHandlerResponse()
{
	if (httpChangePhone.readyState == 4) {								
		// Check that a successful server response was received
      	if (httpChangePhone.status == 200) {
			var response = createResponseXML(httpChangePhone.responseText);
			var error = response.getElementsByTagName("Error").length;
				
			if(error > 0){								
				var lblmess=document.getElementById("ERROR");
				lblmess.style.display = "block";				
			}
			else
			{
				var lblmess=document.getElementById("SUCCESS");
				var lblPhone=document.getElementById("phone");
				lblmess.style.display = "block";
				lblPhone.style.display = "none";
			}
		}
				
	}	
}
/*End*/
/*
*Handler Email
*/
// JavaScript Document
var httpChangeEmail = createXmlHttpRequestObject();
function ChangeEmailHandle()
{
	var strEmail = document.getElementById("tbEmail");
	
	if(!strEmail)
	{
		alert("Field cannot be blank !");
		return false;
	}	
	else 
	{
		if(isValidEmail(strEmail))
		{
			var xml = "";
			xml += "<AjaxParameterRequest>\n";
			xml += "	<New>\n";
			xml += "<Form>ChangeEmail</Form>\n"
			xml += " <ChangeEmail>\n";
		    xml += " <emailaddress>"+strEmail.value+"</emailaddress>\n";
		    xml += " </ChangeEmail>\n";
			xml += "	</New>\n";		
			xml += "</AjaxParameterRequest>\n";
			var eowUrl = ajaxurl + "/ChangeEmailHandler?xml="+xml;
			httpChangeEmail.open("POST", eowUrl);
			//alert(xml);
			httpChangeEmail.onreadystatechange = ChangeEmailHandlerResponse;
			
			httpChangeEmail.send(null);
		}
		else
		{
			strEmail.focus();	
		}
	}
}

function ChangeEmailHandlerResponse()
{
	if (httpChangeEmail.readyState == 4) {								
		// Check that a successful server response was received
		if (httpChangeEmail.status == 200) {
			var response = createResponseXML(httpChangeEmail.responseText);
			//alert(httpChangeEmail.responseText);
			var error = response.getElementsByTagName("Error").length;				
			if(error > 0)
			{								
				var lblmess=document.getElementById("ERROR");
				lblmess.innerHTML = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
								lblmess.style.display = "block";
				var lblmessErr=document.getElementById("SUCCESS");
 				lblmessErr.style.display = "none";
							
			}
			else
			{
				var lblmessErr=document.getElementById("ERROR");
 				lblmessErr.style.display = "none";
				var lblmess=document.getElementById("SUCCESS");
				var lblEmail=document.getElementById("email")
				lblmess.style.display = "block";
				lblEmail.style.display = "none";
			}
		}
				
	}	
}
//function to check valid email address
function isValidEmail(strEmail){

  var validRegExp = new RegExp("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,3})$");
  strEmail = strEmail.value;

   // search email text for regular exp matches

    if (!strEmail.match(validRegExp)) 
   {
      alert('A valid e-mail address is required.\nPlease amend and retry');
      return false;
    } 
    return true; 
}

/*End*/
function checkValidChar(strCh)
{
	var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
  	for (var i = 0; i < strCh.length; i++) {
	      	if (iChars.indexOf(strCh.charAt(i)) != -1) {
      			alert ("The search field has special characters.\n Please remove them and try again");
      			return false;
      		}
	}
	frm.submit();
}

var httpChangeBank = createXmlHttpRequestObject();
function submitChangeBank()
{
     var tbBank = document.getElementById("institution");
	 var sortcode1 = document.getElementById("sortcode1");
 	 var sortcode2 = document.getElementById("sortcode2");
	 var sortcode3 = document.getElementById("sortcode3");
	 var accountNumber = document.getElementById("account_number");
	 var rollNumb = document.getElementById("roll_number");
	 var AccName = document.getElementById("account_name");
	 var sortCode = sortcode1.value+"-"+sortcode2.value+"-"+sortcode3.value;
	 var isCheck = document.getElementById("Bank");
	 var isBank = "checked";	
	 
	 if(tbBank.value=="")
	 {
		alert("Required field BankName is missing!"); 
		return false;
	 }
	 else{
		var validRegExp = new RegExp("[-a-z A-Z0-9_]{0,30}");
		if (!tbBank.value.match(validRegExp)) 
		{
		  alert(' Bank Name is not valid \n Must be 0 - 30 chars');
		  return false;
		} 
	}
	
	
	 if(!isCheck.checked && !document.getElementById("Building").checked)
	 {  
		alert("Required select Bank or Building society is missing!"); 
		return false;
	 }
	 
	 if(!isCheck.checked)
	 {		 
		isBank ="unchecked";
		if(rollNumb.value=="")
		{
			alert("Required field Building Society roll number is missing!"); 
			return false;
		}
		
	 }
	 else{ 
	 	
		if(sortcode1.value=="" || sortcode2.value=="" || sortcode3.value==""){
		 	alert("Required field Sort Code is missing!");
			return false;
		 }
		 else{
			var validRegExp = new RegExp("[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{2}");
			  if (!sortCode.match(validRegExp)) 
		   {
			  alert(' SortCode is not valid \n Must be 6 digits');
			  return false;
			} 
		} 
		
		if (accountNumber.value=="")
		{
			alert("Required field Account Number is missing!"); 
			return false;
		}
		else{
			
			var validRegExp = new RegExp("\\d{8}");
			  if (!accountNumber.value.match(validRegExp)) 
		   {
			  alert(' Account Number is not valid \n Must be 8 digits');
			  return false;
			} 
		}
		
		if (AccName.value.trim()=="")
		{
			alert("Required field Name on account is missing!"); 
			return false;
		}
		
		
	}
	
	 

	var xml = "";
	xml += "<AjaxParameterRequest>\n";
	xml += "	<New>\n";
	xml += "<Form>ChangeBank</Form>\n"
	xml += " <ChangeBank>\n";
	xml += " <AccountName>"+AccName.value+"</AccountName > \n";     
	xml += " <AccountNumber>"+accountNumber.value+"</AccountNumber>  \n";   
	xml += " <Bank>"+tbBank.value+"</Bank>    \n";         
	xml += " <SortCode>"+sortCode+"</SortCode>    \n"; 
	xml += " <IsBank>"+isBank+"</IsBank>\n";
	xml += " <RollNumber>"+rollNumb.value+"</RollNumber>\n";		   
	xml += " </ChangeBank>\n";
	xml += "	</New>\n";		
	xml += "</AjaxParameterRequest>\n";
	//alert(xml);
	var bankUrl = ajaxurl + "/ChangeBankHandler?xml="+xml;
	httpChangeBank.open("POST", bankUrl);
	//alert(xml);
	httpChangeBank.onreadystatechange = ChangeBankHandlerResponse;
	
	httpChangeBank.send(null);
	 
}
function ChangeBankHandlerResponse()
{
	if (httpChangeBank.readyState == 4) {								
		// Check that a successful server response was received
		if (httpChangeBank.status == 200) {
			var response = createResponseXML(httpChangeBank.responseText);
			var error = response.getElementsByTagName("Error").length;
			if(error > 0){								
				var lblmess=document.getElementById("ERROR");																
				//lblmess.innerHTML = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
				lblmess.style.display= "none";
				alert(response.getElementsByTagName("Error")[0].firstChild.nodeValue);
				var lblmessError=document.getElementById("SUCCESS");
				lblmessError.style.display= "none";
						
			}
			else
			{
				var lblmessError=document.getElementById("ERROR");
				lblmessError.style.display= "none";
				var lblmess=document.getElementById("SUCCESS");
                var lblEmail=document.getElementById("Divbank_details")
				lblmess.style.display = "block";
                lblEmail.style.display = "none";
			}
		}
	}	
}
function hide(){
	document.getElementById("done").style.visibility = "hidden";
	document.getElementById("fail").style.visibility = "hidden";
}
function isValidInteger(stringToValid)
{
	var isOk=true;
   	for(i=0;i<stringToValid.length;i++)
	{
  		var oneChar = stringToValid.charAt(i);
 		if (oneChar < "0" || oneChar > "9")
		{
			isOk = false;
		}
	}
	return isOk;
}

function generateNextID (coreName)
{
	var i = 1;
	coreName = coreName + "_";
	while(true){
	
		if(document.getElementById(coreName + i) == null){
			coreName += i;					
			break;
		}else{
			++i;
		}
	}
	
	return coreName;
}

function replacePlusChar(ch){

	var arrUni = new Array('+');
	var arrAscii = new Array('&plus;');

	var k= 0;
	for(k=0; k< arrUni .length; k++){
		if(ch == arrUni[k]){
			return arrAscii[k];
		}
	}
	return ch;
}

function replacePlusString(str){
	var tmp = "";
	var i =0;
	for(i=0; i<str.length; i++){
		tmp += ToAsciiChar(str.charAt(i));
	}
	return	tmp;
}

// JavaScript Document