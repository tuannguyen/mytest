var reportingHttp = null;

var reportTableArray = new Array();
var reportArray = new Array();
var ptb_mandatory = true;
var selectedTableIndex = -1; // default is add

function AddTableConfirm(title, query, chartType, note) 
{
	if (ptb_mandatory && (title == null || title.length == 0 || query == null || query.length == 0)) 
	{
		alert("Please input title and query for the table");
		return false;
	}	
	
	if (query.indexOf(';') >= 0){
		query = query.substring(0, query.indexOf(';'));
	}
	
	query = query.replace(/\n/g, " ").replace(/\r/g, " ");
	//alert(query);
	if(!validateReportQuery(query))
	{
		alert("Please input a valid 'Select' SQL query.");
		return false;
	}
	
	//note = note.replace(/\n/g, "\\r").replace(/\r/g, "\\r");
	//alert(note);
	
	var reportTablesEle = document.getElementById('report_tables');
	if (selectedTableIndex > -1) // table edited
	{
		var tableSelected = reportTableArray[selectedTableIndex];
		tableSelected[0] = title;
		tableSelected[1] = query;
		tableSelected[2] = chartType;
		tableSelected[3] = note;		
		reportTableArray[selectedTableIndex] = tableSelected;
	} 
	else	// table added
	{
		reportTableArray.push(Array(title, query, chartType, note)) ;
	}
	document.getElementById('report_table_dialog').style.display='none';
	updateReportHTML();
}

function updateReportHTML()
{
	var tableHolder = document.getElementById('report_tables');
	if (reportTableArray && reportTableArray.length > 0)
	{		
		var tableHolderInnerHtml = '<table border="0" width="100%"><tbody>';
		for(var index=0; index < reportTableArray.length; index++) {
			tableHolderInnerHtml = tableHolderInnerHtml + '<tr><td class=\'table_title\'>' + reportTableArray[index][0] + 
				'</td><td><a href="javascript:editReportTable(' + index + ', this)">Edit...</a></td>' +
				'<td><a href="javascript:removeReportTable(' + index + ')">Remove...</a></td></tr>'
		}
		tableHolderInnerHtml = tableHolderInnerHtml + '</tbody></table>'
		tableHolder.innerHTML = tableHolderInnerHtml;
		//alert(tableHolderInnerHtml);	
	}
	else
	{
		tableHolder.innerHTML = '';
	}
}

function AddTableCancel () 
{
	document.getElementById('report_table_dialog').style.display='none';
}

function editReportTable (tableIndex, this1)
{
	selectedTableIndex = tableIndex;
	document.getElementById('report_table_dialog').style.display='block';
	//document.getElementById('report_table_dialog').style.pixelTop=parseInt(findPosY(this1) + 40);
	//document.getElementById('report_table_dialog').style.pixelLeft=parseInt(findPosX(this1) - 60);
	if (tableIndex < 0)
	{
		document.getElementById('table_title').value = '';
		document.getElementById('table_query').value = '';
		document.getElementById('table_charttype').value = 'CHART_TYPE_NONE';
		document.getElementById('table_note').value = '';		
	}
	else if (tableIndex < reportTableArray.length)
	{
		document.getElementById('table_title').value = reportTableArray[selectedTableIndex][0];
		document.getElementById('table_query').value = reportTableArray[selectedTableIndex][1];
		document.getElementById('table_charttype').value = reportTableArray[selectedTableIndex][2];
		document.getElementById('table_note').value = reportTableArray[selectedTableIndex][3];		
	}
}

function removeReportTable (tableIndex)
{
	document.getElementById('report_table_dialog').style.display='none';
	if (tableIndex > -1)
	{
		var tmReportTableArray = reportTableArray;
		reportTableArray = new Array();
		
		for(var index = 0; index < tmReportTableArray.length; index++) {
			if (index != tableIndex) {
				reportTableArray.push(tmReportTableArray[index]);
			}
		}		
	}
	
	updateReportHTML();
}

function generateEditReportRequestXML(rp_action, rp_id, rp_newName, rp_comment, rp_tables)
{
	rp_newName = rp_newName.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	rp_comment = rp_comment.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	rp_newName = rp_newName.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	rp_newName = rp_newName.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_action>" + rp_action + "</rp_action>";
	xml += "<rp_id>" + rp_id + "</rp_id>";
	xml += "<rp_title><![CDATA[" + rp_newName + "]]></rp_title>";
	xml += "<rp_comment><![CDATA[" + rp_comment + "]]></rp_comment>";
	xml += "<rp_tables>";
	for(var index = 0; index < rp_tables.length; index ++) 
	{
		var tableTitle = rp_tables[index][0].replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
		var tableQuery = rp_tables[index][1].replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)');
		var tableNote = rp_tables[index][3].replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');		
		xml += "<rp_table>";
		xml += "<rp_table_title><![CDATA[" + tableTitle + "]]></rp_table_title>";
		xml += "<rp_table_query><![CDATA[" + tableQuery + "]]></rp_table_query>";
		xml += "<rp_table_charttype>" + rp_tables[index][2] + "</rp_table_charttype>";
		xml += "<rp_table_note><![CDATA[" + tableNote + "]]></rp_table_note>";		
		xml += "</rp_table>";
	}
	xml += "</rp_tables>";
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function submitEditReportRequest(rp_action, rp_id)
{
	var rp_title = document.getElementById('_report_title').value;
	var rp_comment = document.getElementById('_report_comment').value;

	if (rp_action == 'Add' || rp_action == 'Edit')
	{
		if (rp_title == null || rp_title.length == 0 || reportTableArray == null || reportTableArray.length == 0) {
			alert('Please input report title and at least one table');
			return false;
		}		
	}

	// if oldName is null or empty, consisder this is add report request
	var xml = generateEditReportRequestXML(rp_action, rp_id, rp_title, rp_comment, reportTableArray);	
	
	var reportingURL = ajaxurl + "/ManageReportHandler";
	var params = "xml=" + xml;

	//alert(xml);

	reportingHttp = createXmlHttpRequestObject();

	reportingHttp.open("POST", reportingURL, true);

	reportingHttp.onreadystatechange = handleSaveReportResult;		
	    
    reportingHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    reportingHttp.setRequestHeader("Content-length", params.length);
    reportingHttp.setRequestHeader("Connection", "close");	
	
	reportingHttp.send(params);	
}

function handleSaveReportResult(){

   	if (reportingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (reportingHttp.status == 200) {
			var response = createResponseXML(reportingHttp.responseText);					
			//alert(reportingHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Saving report failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{
						window.location.replace(ajaxurl + '/pl/webstats/manage_reports.jsp');
					}
					else
					{
						alert('Saving report failed!');
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function SelectAllReports()
{	
	var allReportCb = document.getElementById('cb_all_reports');
	
	if (allReportCb != null)
	{
		var checked = false;
		if (allReportCb.checked)
		{
			checked = true;
		}
		
		for(var i=0; i<reportArray.length; i++) 
		{
			var reportIdStored = reportArray[i][0];
			var reportCb = document.getElementById('cb_' + reportIdStored);
			if (reportCb)
			{
				if (checked)
				{
					reportCb.checked = true;
				}
				else
				{
					reportCb.checked = false;
				}
			}
			
			// find tables belong to report
			var tables = reportArray[i][1];
			for(var j=0; j<tables.length; j++) 
			{
				var subCb = document.getElementById('cb_' + reportIdStored + '_' + tables[j]);
				if (subCb) 
				{
					if (checked)
					{
						subCb.checked = true;
					}
					else
					{
						subCb.checked = false;
					}
				}
			}
		}		
	}
	else
	{
		return false;
	}

}

function SelectAllTables(reportId)
{	
	var reportCb = document.getElementById('cb_' + reportId);
	
	if (reportCb != null)
	{
		var checked = false;
		if (reportCb.checked)
		{
			checked = true;
		}
		
		for(var i=0; i<reportArray.length; i++) 
		{
			var reportIdStored = reportArray[i][0];
			if (reportId == reportIdStored) 
			{
				// find tables belong to report
				var tables = reportArray[i][1];
				for(var j=0; j<tables.length; j++) 
				{
					var subCb = document.getElementById('cb_' + reportId + '_' + tables[j]);
					if (subCb) 
					{
						if (checked)
						{
							subCb.checked = true;
						}
						else
						{
							subCb.checked = false;
						}
					}
				}
				
				break;
			}
		}		
	}
	else
	{
		return false;
	}
	
	UpdateSelectAllReports();
}

function UpdateSelectAllTables(reportId)
{	
	var reportCb = document.getElementById('cb_' + reportId);
	
	if (reportCb != null)
	{
		var checked = true;		
		
		for(var i=0; i<reportArray.length; i++) 
		{
			var reportIdStored = reportArray[i][0];
			if (reportId == reportIdStored) 
			{
				// find tables belong to report
				var tables = reportArray[i][1];
				for(var j=0; j<tables.length; j++) 
				{
					var subCb = document.getElementById('cb_' + reportId + '_' + tables[j]);
					if (subCb.checked == false) 
					{
						checked = false;
						break;
					}				
				}
				
				break;
			}
		}
		
		if (checked)		
		{
			reportCb.checked = true;
		}
		else
		{
			reportCb.checked = false;
		}
	}
	else
	{
		return false;
	}
	UpdateSelectAllReports();

}

function UpdateSelectAllReports()
{	
	var allReportCb = document.getElementById('cb_all_reports');
	
	if (allReportCb != null)
	{
		var checked = true;		
		
		for(var i=0; i<reportArray.length; i++) 
		{
			var reportIdStored = reportArray[i][0];
			var reportCb = document.getElementById('cb_' + reportIdStored);
			if (reportCb)
			{
				if (reportCb.checked == false)
				{
					checked = false;
					break;
				}
			}						
		}
		
		if (checked)		
		{
			allReportCb.checked = true;
		}
		else
		{
			allReportCb.checked = false;
		}
	}
	else
	{
		return false;
	}

}

function generateRunQueryRequestXML(tableName, query, note, tableChartType)
{
	tableName = tableName.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	query = query.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
	note = note.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)');
		
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_table_title><![CDATA[" + tableName + "]]></rp_table_title>";
	xml += "<rp_table_query><![CDATA[" + query + "]]></rp_table_query>";
	xml += "<rp_table_note><![CDATA[" + note + "]]></rp_table_note>";	
	xml += "<rp_table_charttype>" + tableChartType + "</rp_table_charttype>";
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function runQueryPreview(tableTitle, query, note, tableChartType)
{
	var xml = generateRunQueryRequestXML(tableTitle, query, note, tableChartType);
	
	var reportingURL = ajaxurl + "/RunQueryHandler";
	var params = "xml=" + xml;
	
	reportingHttp = createXmlHttpRequestObject();

	reportingHttp.open("POST", reportingURL, true );

	reportingHttp.onreadystatechange = handleRunQueryResult;		
	
    reportingHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    reportingHttp.setRequestHeader("Content-length", params.length);
    reportingHttp.setRequestHeader("Connection", "close");		

	reportingHttp.send(params);
}

function handleRunQueryResult(){

   	if (reportingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (reportingHttp.status == 200) {
			var response = createResponseXML(reportingHttp.responseText);					
			//alert(reportingHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Run query failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						if (rpReportOutput == 'HTML')
						{
							window.open(ajaxurl + '/pl/webstats/webstats_report.jsp', 'PensionLine_WebStat_Report');
						}						
					}
					else
					{
						alert('Run query failed!');
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function generateRunReportRequestXML(startDate, endDate, outputType)
{	
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_reports>";
	for(var i=0; i<reportArray.length; i++) 
	{
		var reportIdStored = reportArray[i][0];
		var reportXml = generateSelectedReportXML(reportIdStored);
		if (reportXml != null) 
		{
			xml += reportXml;
		}
	}	
	xml += "</rp_reports>";
	xml += "<rp_startDate>" + startDate + "</rp_startDate>";
	xml += "<rp_endDate>" + endDate + "</rp_endDate>";        
	xml += "<rp_outputType>" + outputType + "</rp_outputType>";	
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function generateSelectedReportXML(reportId)
{
	var tableSelected = false;
	var reportXml = "";
	reportXml += "<rp_report>";
	reportXml += "<rp_id>" + reportId + "</rp_id>";
	reportXml += "<rp_tables>";
		
	for(var i=0; i<reportArray.length; i++) 
	{
		var reportIdStored = reportArray[i][0];
		if (reportId == reportIdStored) 
		{
			// find tables belong to report
			var tables = reportArray[i][1];
			for(var j=0; j<tables.length; j++) 
			{
				var subCb = document.getElementById('cb_' + reportId + '_' + tables[j]);
				if (subCb) 
				{
					if (subCb.checked)
					{
						reportXml += "<rp_table>" + tables[j] + "</rp_table>";
						tableSelected = true;
					}
				}
			}
			
			break;
		}
	}	
	reportXml += "</rp_tables>";
	reportXml += "</rp_report>";
	if (tableSelected) 
	{
		return reportXml;
	}
	else
	{
		return null;
	}
}

function runReports()
{	
	var startDate = document.getElementById('report_start_date').value;
	var endDate = document.getElementById('report_end_date').value;
	var outputType = document.getElementById('report_output_type').value;
	
	if (startDate != null && startDate != '' && endDate != null && endDate != '')
	{
		if (!isDateAfter(endDate, startDate)) {
			alert('Start date must be before end date!');
			return false;
		}
	}
	else if ((startDate == null || startDate == '') && (endDate == null || endDate == ''))
	{
		// ignore, use default date
	}
	else
	{
		alert ('You must specify both start and end date and make sure the start date before the end date!');
		return false;
	}

	if (reportArray == null || reportArray.length == 0)
	{
		alert('Please select at least one report to run!');
		return false;
	}
	var tableSelected = false;
	for(var i=0; i<reportArray.length; i++) 
	{
		var reportId = reportArray[i][0];
		// find tables belong to report
		var tables = reportArray[i][1];
		for(var j=0; j<tables.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportId + '_' + tables[j]);
			if (subCb && subCb.checked) 
			{
				tableSelected = true;
				break;
			}
		}
		if (tableSelected) 
		{
			break;
		}				
	}	
	
	if (tableSelected == false)
	{
		alert('Please select at least one report to run!');
		return false;
	}
	
	//alert('Getting xml requests');
	var xml = generateRunReportRequestXML(startDate, endDate, outputType);
	
	
	//alert(xml);
	var reportingURL = ajaxurl + "/RunReportHandler";
	var params = "xml=" + xml;

	//alert(xml.length);

	reportingHttp = createXmlHttpRequestObject();

	reportingHttp.open("POST", reportingURL, true);

	reportingHttp.onreadystatechange = handleRunReportResult;		
	    
    reportingHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    reportingHttp.setRequestHeader("Content-length", params.length);
    reportingHttp.setRequestHeader("Connection", "close");	
	
	reportingHttp.send(params);		
	reportWhizyVisi(true);
}

function runAllReportsPDF(numReports)
{
	var startDate = document.getElementById('report_start_date').value;
	var endDate = document.getElementById('report_end_date').value;
	var chartType = document.getElementById('report_chart_type').value;
	var outputType = 'DOWNLOAD_PDF';
	
	if (startDate != null && startDate != '' && endDate != null && endDate != '')
	{
		if (!isDateAfter(endDate, startDate)) {
			alert('Start date must be before end date!');
			return;
		}
	}
	
	var xml = generateRunReportRequestXML('ALL_REPORTS', startDate, endDate, chartType, outputType);

	var reportingURL = ajaxurl + "/RunReportHandler?xml="+xml;
	
	numberOfReports = numReports;
	for(var index=0; index<numReports; index++) 
	{
		// display spinning image for selected report
		var reportCommentEl = document.getElementById('rp_comment_' + index);
		var reportSpinImgEl = document.getElementById('rp_spinning_img_' + index);
		if (reportCommentEl)
		{
			reportCommentEl.style.display = "none";
		}
		if (reportSpinImgEl) 
		{
			reportSpinImgEl.style.display = "";
			reportSpinImgEl.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"running...\" />";			
		}		
	}


	reportingHttp = createXmlHttpRequestObject();

	reportingHttp.open("POST", reportingURL );

	reportingHttp.onreadystatechange = handleRunReportResult;		
	
	reportingHttp.send(null);
}

function handleRunReportResult(){

   	if (reportingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (reportingHttp.status == 200) {
			var response = createResponseXML(reportingHttp.responseText);					
			//alert(reportingHttp.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert('Run report failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						if (rpReportOutput == 'HTML')
						{
							window.open(ajaxurl + '/pl/webstats/webstats_report.jsp', 'PensionLine_WebStat_Report');
						}
						else if (rpReportOutput == 'PDF')
						{
							window.open(ajaxurl + '/pl/webstats/report_complete.html', 'PensionLine_WebStat_Report');
						}
						
						// hide spining images
						reportWhizyVisi(false);
					}
					else
					{
						alert('Run report failed!');
						reportWhizyVisi(false);
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function reportWhizyVisi (visible)
{
	// display spinning image for selected report
	for(var i=0; i<reportArray.length; i++) 
	{
		var reportId = reportArray[i][0];
		if (reportId) 
		{
			// find tables belong to report
			var tables = reportArray[i][1];
			for(var j=0; j<tables.length; j++) 
			{
				var subCb = document.getElementById('cb_' + reportId + '_' + tables[j]);
				if (subCb && subCb.checked) 
				{
					var cbSpan = document.getElementById('span_' + reportId + '_' + tables[j]);
					var spinImg = document.getElementById('spin_' + reportId + '_' + tables[j]);
					if (cbSpan != null && spinImg != null) 
					{
						if (visible)
						{
							cbSpan.style.display = "none";						
							spinImg.style.display = "";
							spinImg.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"running...\" />";
						}
						else
						{
							cbSpan.style.display = "";						
							spinImg.style.display = "none";
							spinImg.innerHTML = "";						
						}						
					}
				}
			}
		}
	}	
}

function isDateAfter (date, comparedDate)
{
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	
	var getMonth = function(monthStr)
	{
		for(var index=0; index<monthNames.length; index++) {
			if (monthStr == monthNames[index])
			{
				return (index + 1);				
			}			
		}
		return -1;
	}
	
	// compare year
	if (date.indexOf(',') > 0 && comparedDate.indexOf(',') > 0) {
		var year = parseInt(date.substring(date.indexOf(',') + 1));
		var comparedYear = parseInt(comparedDate.substring(comparedDate.indexOf(',') + 1));
		//alert(year + ' with ' + comparedYear);
		if (year < comparedYear)
		{
			return false;
		}
		else if (year > comparedYear)
		{
			return true;
		}
	}
	else
	{
		return false;
	}
	
	// compare month
	if (date.indexOf(' ') > 0 && comparedDate.indexOf(' ') > 0) {
		var month = getMonth(date.substring(0, date.indexOf(' ')));
		var comparedMonth = getMonth(comparedDate.substring(0, comparedDate.indexOf(' ')));
		//alert(month + ' with ' + comparedMonth);
		if (month == -1 || comparedMonth == -1) return false;
		
		if (month < comparedMonth) 
		{
			return false;
		} else if (month > comparedMonth)
		{
			return true;
		}

	}
	else
	{
		return false;
	}
	
	// compare date
	var day = parseInt(date.substring(date.indexOf(' ') + 1, date.indexOf(',')));
	var comparedDay = parseInt(comparedDate.substring(comparedDate.indexOf(' ') + 1, comparedDate.indexOf(',')));
	//alert(day + ' with ' + comparedDay);
	if (day < comparedDay){
		//alert('reutrn false');
		return false;		
	}
	return true;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "report_start_date" || cal.sel.id == "report_end_date"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar
  return false;
}

function validateReportQuery (query)
{
	if (query != null)
	{		
		query = query.toLowerCase();
		query = trim(query);
		//alert(query);
		
		if (query.indexOf('select') == 0)
		{
			return true;
		}
	}
	return false;
}

function trim(str) {
    var trimmed = str.replace(/^\s+|\s+$/g, '') ;
    return trimmed;
}