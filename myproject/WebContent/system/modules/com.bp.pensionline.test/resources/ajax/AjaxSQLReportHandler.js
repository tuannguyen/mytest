var sqlReportHttp = null;

var sessionTimeoutId = '';

// report variables
var reportSections = new Array(); // contain information of all sections in the report. Each section is stored in an array
var selectedSectionIndex = -1;

// Section variables
var sectionParams = new Array();
var selectedParamIndex = -1;

// running reports
var runningReportArray = new Array();
var selectedRunningReportArray = new Array();
var reportGroupId = '';
var reportRunnerId = '';
var reportRunner = '';
var reportOutputFormat = 'DOWNLOAD_PDF';
var hasParamInput = false;
var reportStartDate = '';
var reportEndDate = '';
var isCopyDateToSection = false;

//array to hold start date param name
var STARTDATE_PARAMNAME_ARRAY = new Array('start_date', 'startdate', 'date_start','datestart', 'from_date', 'fromdate', 'date_from', 'datefrom');
var ENDDATE_PARAMNAME_ARRAY = new Array('end_date', 'enddate', 'date_end','dateend','to_date', 'todate', 'date_to', 'dateto');

// report grouping
var reportGroupArray = new Array();
var reportInGroupArray = new Array();

// user in group
var userInGroupArray = new Array();

// schedule report
var crondExpresson = '';
var destEmails = '';
var scheduleAction = '';

// handling 'Back' button'
var lastContentHTML = '';
var titleStartCharStr = '';

/****** handle add and remove section parameters ****/
function openSection() 
{ 
	selectedSectionIndex = -1;
	document.getElementById('add_section_id').style.display='block'; 
	
	// clear all embeded information with last section
	sectionParams = new Array();
	updateParamListHtml();
	document.getElementById('section_title').value = '';
	document.getElementById('section_description').value = '';
	document.getElementById('section_query').value = '';	
}
function closeSection() 
{ 
	selectedSectionIndex = -1;
	document.getElementById('add_section_id').style.display='none';
	
	// clear all embeded information with last section 
	// clear all embeded information with last section
	sectionParams = new Array();
	updateParamListHtml();
	
	document.getElementById('section_title').value = '';
	document.getElementById('section_description').value = '';
	document.getElementById('section_query').value = '';
		
}

function cancelAddEditReport()
{
	window.location.replace('/content/pl/sql_report/manage_report/index.html' + titleStartCharStr);
}

function selectUser()
{
    var selectWindow = window.open('/content/pl/sql_report/manage_report/select_user.jsp',
'select_user','height=400,width=250,scrollbars=1');
    selectWindow.moveTo(380, 200);
}
function selectGroup()
{
    var selectWindow = window.open('/content/pl/sql_report/manage_report/select_group.jsp',
'select_user','height=400,width=250,scrollbars=1');
    selectWindow.moveTo(380, 200);
}
function showParamDialog(paramIndex, _this)
{	
	selectedParamIndex = paramIndex;
	
	var overlay = document.getElementById('overlay');
    if (overlay == null)
    {
    	var bod 			= document.getElementsByTagName('body')[0];
    	overlay 			= document.createElement('div');
    	overlay.id			= 'overlay';
    	bod.appendChild(overlay);   	
    }
    overlay.style.display = 'block';    

    var addParamDialog = document.getElementById('add_param_dialog');
    if (addParamDialog )
    {
        addParamDialog.style.display='block';
        addParamDialog.style.position = 'absolute';
        addParamDialog.style.pixelTop = _findPosY(_this) + 20;
        addParamDialog.style.pixelLeft = _findPosX(_this) - 100;
        addParamDialog.style.top = (_findPosY(_this) + 20) + "px";	//firefox
        addParamDialog.style.left = (_findPosX(_this) - 100) + "px"; // firefox
    }
    
    
    if (selectedParamIndex == -1)	// add
    {
	    document.getElementById('section_param_name').value = '';
	    document.getElementById('section_param_type').value = 'PARAM_TYPE_STRING';
	    document.getElementById('section_param_default_value').value = '';
	    document.getElementById('section_param_description').value = ''; 
	    updateParamTypeHelpers('PARAM_TYPE_STRING', 'section_param_default_value');     	
    } 
    else	// edit
    {
    	var sectionParamSelected = sectionParams[selectedParamIndex];
	    document.getElementById('section_param_name').value = sectionParamSelected[0];
	    document.getElementById('section_param_type').value = sectionParamSelected[1];
	    document.getElementById('section_param_default_value').value = sectionParamSelected[3];
	    document.getElementById('section_param_description').value = sectionParamSelected[4]; 
	    updateParamTypeHelpers(sectionParamSelected[1], 'section_param_default_value');
    }
        
}

function AddParamConfirm()
{
    //update param list
    var name = document.getElementById('section_param_name').value;
    var type = document.getElementById('section_param_type').value;
    var defaultValue = document.getElementById('section_param_default_value').value;  
    var description = document.getElementById('section_param_description').value;
	if (name == null || name.length == 0 || type == null || type.length == 0)
	{
		alert("Please specify parameter name and its type!");
		return false;
	}
	
	if (!validateParamName(name))    
	{
		alert('The parameter name must contains character in a-zA-Z0-9_ sequence with a \':\' as prefix.');
		return false;
	}
	
	if (type == 'PARAM_TYPE_NUMBER' && !validateParamTypeNumber(defaultValue))
	{
		alert('The default value field must be a valid number!');
		return false;
	} 
	
	if (selectedParamIndex == -1)
	{
    	addSectionParam(name, type, '', defaultValue, description);
	}
	else
	{
		editSectionParam(selectedParamIndex, name, type, '', defaultValue, description);
	}
    
    var addParamDialog = document.getElementById('add_param_dialog');
    if (addParamDialog )
    {
        addParamDialog.style.display='';
    }
    var overlay = document.getElementById('overlay');
    if (overlay)
    {
    	overlay.style.display = 'none';
    }       

	updateParamListHtml();    
    selectedParamIndex = -1;    
}

function AddParamCancel()
{  
    //update param list
    document.getElementById('section_param_name').value = '';
    document.getElementById('section_param_type').value = 'PARAM_TYPE_STRING';
    document.getElementById('section_param_default_value').value = '';
    document.getElementById('section_param_description').value = '';
    
    var addParamDialog = document.getElementById('add_param_dialog');
    if (addParamDialog )
    {
        addParamDialog.style.display='';
    }
    var overlay = document.getElementById('overlay');
    if (overlay)
    {
    	overlay.style.display = 'none';
    }     
    
    selectedParamIndex = -1; 
}

function addSectionParam(name, type, value, defaultValue, description)
{
	// check if param with the same name existed
	var paramExisted = false;
	for(var i=0; i<sectionParams.length; i++) 
	{
		var paramName = sectionParams[i][0];
		if (paramName == name)
		{
			paramExisted = true;			
			break;
		} 
	}
	
	if (paramExisted)
	{
		alert('This parameter name has been used. Please use another name!');
		return false;
	} 
	else
	{
		sectionParams.push(new Array(name, type, value, defaultValue, description));
	}
}

function removeSectionParam(name)
{
	var newSectionParams = new Array();
	for(var i=0; i<sectionParams.length; i++) 
	{
		var paramName = sectionParams[i][0];
		if (paramName != name)
		{
			newSectionParams.push(new Array(sectionParams[i][0], 
											sectionParams[i][1], 
											sectionParams[i][2], 
											sectionParams[i][3]));
		} 		
	}
	
	sectionParams = newSectionParams;	
	updateParamListHtml();
}

function editSectionParam(paramIndex, name, type, value, defaultValue, description)
{
	var paramExisted = false;
	for(var i=0; i<sectionParams.length; i++) 
	{
		var paramName = sectionParams[i][0];
		if (paramName == name && i != paramIndex)
		{
			paramExisted = true;			
			break;
		} 
	}
	
	if (paramExisted)
	{
		alert('This parameter name has been used. Please use another name!');
		return false;
	} 
	else
	{
		sectionParams[paramIndex] = new Array(name, type, value, defaultValue, description);
	}	
	
	updateParamListHtml();
}

function updateParamListHtml ()
{
	var paramListEle = document.getElementById('section_param_list');
	if (paramListEle)
	{
			paramListEle.innerHTML = '';
			for(var i=0; i<sectionParams.length; i++) 
			{
				var sectionParam = sectionParams[i];
				var name = sectionParam[0];
				paramListEle.innerHTML = paramListEle.innerHTML + 
					'<div>' +
					'<label>' + name + '</label>' +
					'<a href="" onclick="showParamDialog(' + i + ', this); return false;">' +
					'<img src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/iconEdit.gif" alt="Edit parameter"/>' +
					'</a>' +					
					'<a href="" onclick="removeSectionParam(\'' + name + '\'); return false;">' +
					'<img src="/content/pl/system/modules/com.bp.pensionline.template/resources/gfx/iconDelete.gif" alt="Remove parameter"/>' +
					'</a>' +
					'</div>'
			}		
	} 
}

function popUpParamInputDialog(_this)
{
	// submit to server if there is no parameters
//	if (sectionParams.length == 0)
//	{
//		var sendOk = sendPreviewSectionRequest(	document.getElementById('section_title').value, 
//						document.getElementById('section_query').value,
//						document.getElementById('section_description').value);
//		if (!sendOk) return;
//		
//		var inputParamDialog = document.getElementById('input_param_dialog');
//		if (inputParamDialog)
//		{
//	    	inputParamDialog.innerHTML = '<div class="dialog_title">Please wait...</div>';
//	    	
//	    	inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
//	    			'<p>' +
//	    			'<span><img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
//	    			'<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
//	    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    			
//	    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    			
//	    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    				    		    	    		    
//	    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    				    		    	    		    
//	    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    				    		    	    		    	    		    	    		    
//	    			'</p>';
//		}
//				
//		var overlay = document.getElementById('overlay');
//	    if (overlay == null)
//	    {
//	    	var bod 			= document.getElementsByTagName('body')[0];
//	    	overlay 			= document.createElement('div');
//	    	overlay.id			= 'overlay';
//	    	bod.appendChild(overlay);
//	    }
//	    overlay.style.display = 'block'; 
//	    	    			    	
//		inputParamDialog.style.position = 'fixed';		    	    			    	
//        inputParamDialog.style.display='block';
//        inputParamDialog.style.pixelTop = parseInt(200);
//        inputParamDialog.style.pixelLeft = parseInt(250);							
//	}
//	else
//	{
//		var inputParamDialog = document.getElementById('input_param_dialog');
//	    if (inputParamDialog )
//	    {
//	    	inputParamDialog.innerHTML = '<div class="dialog_title">Please input parameter values</div>';
//	    	
//	    	for(var i=0; i<sectionParams.length; i++) 
//	    	{
//	    		var sectionParam = sectionParams[i];
//	    		inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
//	    			'<div class="login_component_holder">' +
//	    			'<div class="login_label">' + sectionParam[0] + '</div>' + 
//	    			'<div class="login_value"><input id="section_param_value_' + sectionParam[0] + '" type="text" ' +
//	    			'value="' + sectionParam[3] + '"/> </div>' +
//	    			'</div>';
//	    	}
//	    	inputParamDialog.innerHTML = inputParamDialog.innerHTML +
//	    			'<div class="login_component_holder">' +
//	    			'<div class="login_label">&nbsp;</div>' + 
//	    			'<div class="login_value"><input class="goButton" type="button" onclick="InputParamComplete();" name="dobookmark" value="Ok" />' +
//	    			'<input class="goButton" type="button" onclick="InputParamCancel();" name="dobookmark" value="Cancel" />' +
//	    			'</div>' +
//	    			'</div>';	
//	    			    	
//			var overlay = document.getElementById('overlay');
//		    if (overlay == null)
//		    {
//		    	var bod 			= document.getElementsByTagName('body')[0];
//		    	overlay 			= document.createElement('div');
//		    	overlay.id			= 'overlay';
//		    	bod.appendChild(overlay);
//		    }
//		    overlay.style.display = 'block'; 
//		    	    			    	
//			inputParamDialog.style.position = 'fixed';		    	    			    	
//	        inputParamDialog.style.display='block';
//	        inputParamDialog.style.pixelTop = parseInt(200);
//	        inputParamDialog.style.pixelLeft = parseInt(250);
//	    }
//	}
	
	var inputParamDialog = document.getElementById('input_param_dialog');
    if (inputParamDialog )
    {
    	inputParamDialog.innerHTML = '<div class="dialog_title">Please input parameter values</div>';
    	
    	for(var i=0; i<sectionParams.length; i++) 
    	{
    		var sectionParam = sectionParams[i];
    		inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
    			'<div class="login_component_holder">' +
    			'<div class="login_label">' + sectionParam[0] + '</div>' + 
    			'<div class="login_value"><input id="section_param_value_' + sectionParam[0] + '" type="text" ' +
    			'value="' + sectionParam[3] + '"/> </div>' +
    			'</div>';
    	}
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML +
    			'<div class="login_component_holder">' +
    			'<div class="login_label">Output</div>' + 
    			'<div class="login_value"><select id="section_output_type">' +
				'<option value="DOWNLOAD_XLS"  selected="selected">Download as a XLS document</option>' +	
				'<option value="DOWNLOAD_PDF">Download as a PDF document</option></select>' +
    			'</div>' +
    			'</div>';	    	
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML +
    			'<div class="login_component_holder">' +
    			'<div class="login_label">&nbsp;</div>' + 
    			'<div class="login_value"><input class="goButton" type="button" onclick="InputParamComplete();" name="dobookmark" value="Ok" />' +
    			'<input class="goButton" type="button" onclick="InputParamCancel();" name="dobookmark" value="Cancel" />' +
    			'</div>' +
    			'</div>';	
    			    	
		var overlay = document.getElementById('overlay');
	    if (overlay == null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	overlay 			= document.createElement('div');
	    	overlay.id			= 'overlay';
	    	bod.appendChild(overlay);
	    }
	    overlay.style.display = 'block'; 
	    	    			    	
//		inputParamDialog.style.position = 'absolute';		    	    			    	
//        inputParamDialog.style.display='block';
//        inputParamDialog.style.pixelTop = 200;
//        inputParamDialog.style.pixelLeft = 250;
        inputParamDialog.style.display='block';
        inputParamDialog.style.position = 'absolute';
        inputParamDialog.style.pixelTop = _findPosY(_this) - 140;
        inputParamDialog.style.pixelLeft = _findPosX(_this) - 100;    
        inputParamDialog.style.top = (_findPosY(_this) - 140) + "px";;
        inputParamDialog.style.left = (_findPosX(_this) - 100) + "px";; 
    }	
}

function InputParamComplete ()
{
	// get the value input by user
	for(var i=0; i<sectionParams.length; i++) 
	{
		var sectionParam = sectionParams[i];
		var paramName = sectionParam[0];
		// set the value
		if (document.getElementById('section_param_value_' + paramName))
		{
			sectionParam[2] = document.getElementById('section_param_value_' + paramName).value;
		}		
	}
	var sectionOutputType = 'DOWNLOAD_XLS'; //default
	if (document.getElementById('section_output_type'))
	{
		sectionOutputType = document.getElementById('section_output_type').value;
	}
	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
    	inputParamDialog.innerHTML = '<div class="dialog_title">Please wait...</div>';
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
    			'<p>' +
    			'<span><img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    			'<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +    
    			'</p>';
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML + '<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="" onclick="javascript: cancelPreviewSynchRequest(); return false;">Cancel</a></div>';
	}
		
	// submit request	
	sendPreviewSectionSynchRequest(	
					document.getElementById('section_title').value, 
					document.getElementById('section_query').value,
					document.getElementById('section_description').value,
					sectionOutputType);					
						
}

function InputParamCancel ()
{
	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
		inputParamDialog.style.display='';
	} 

    var overlay = document.getElementById('overlay');
    if (overlay)
    {
    	overlay.style.display = 'none';
    } 	
}


var idPreviewSectionLooking;
var previewCancelled = false;

function cancelPreviewRequest()
{	
	var xml = "<AjaxReportParameterRequest><rp_action>STOP</rp_action></AjaxReportParameterRequest>";
	//alert(xml);
	var sqlReportURL = ajaxurl + "/PreviewSectionThreadHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleCancelPreviewSectionResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);

	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
		inputParamDialog.style.display='';
	} 

    var overlay = document.getElementById('overlay');
    if (overlay)
    {
    	overlay.style.display = 'none';
    } 
}

function cancelPreviewSynchRequest()
{	
	previewCancelled = true;
	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
		inputParamDialog.style.display='';
	} 

    var overlay = document.getElementById('overlay');
    if (overlay)
    {
    	overlay.style.display = 'none';
    } 
}

function handleCancelPreviewSectionResponse()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				clearInterval (idPreviewSectionLooking);
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}			
	}	
}

function lookingPreviewRequest()
{	
	var xml = "<AjaxReportParameterRequest><rp_action>LOOKING</rp_action></AjaxReportParameterRequest>";
	//alert(xml);
	var sqlReportURL = ajaxurl + "/PreviewSectionThreadHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleLookingPreviewSectionResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);	
}

function handleLookingPreviewSectionResponse()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					alert('Query preview error: ' + errors[0].firstChild.nodeValue);
					var inputParamDialog = document.getElementById('input_param_dialog');
					if (inputParamDialog)
					{
						inputParamDialog.style.display='';
					} 
				
				    var overlay = document.getElementById('overlay');
				    if (overlay)
				    {
				    	overlay.style.display = 'none';
				    } 	
				    
				    clearInterval (idPreviewSectionLooking);		
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpActionStatus = response.getElementsByTagName("rp_reportStatus")[0].firstChild.nodeValue;					
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;
					//alert('rpActionStatus: ' + rpActionStatus);
					if (rpActionResult == 'true')
					{
						if (rpActionStatus != null && rpActionStatus == '1')
						{
							// donothing. Keep looking
						}
						else if (rpActionStatus != null && rpActionStatus == '2')
						{
							if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_PDF')
							{
								window.open('/content/pl/sql_report/report_pdf_complete.html', 'Preview_Section');
							}
							else if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_XLS')
							{
								window.open('/content/pl/sql_report/report_xls_complete.html', 'Preview_Section');							
							}
		
	
							var inputParamDialog = document.getElementById('input_param_dialog');
							if (inputParamDialog)
							{
								inputParamDialog.style.display='';
							} 
						
						    var overlay = document.getElementById('overlay');
						    if (overlay)
						    {
						    	overlay.style.display = 'none';
						    } 	
						    
						    clearInterval (idPreviewSectionLooking);																				
						}		
					}
					else
					{
						alert('Query execution failed:\n' + rpReportOutput);
						var inputParamDialog = document.getElementById('input_param_dialog');
						if (inputParamDialog)
						{
							inputParamDialog.style.display='';
						} 
					
					    var overlay = document.getElementById('overlay');
					    if (overlay)
					    {
					    	overlay.style.display = 'none';
					    } 	
					    
					    clearInterval (idPreviewSectionLooking);						
					}
				}	
			}
			catch(err){
				alert("XML Parser error. " + err);
				var inputParamDialog = document.getElementById('input_param_dialog');
				if (inputParamDialog)
				{
					inputParamDialog.style.display='';
				} 
			
			    var overlay = document.getElementById('overlay');
			    if (overlay)
			    {
			    	overlay.style.display = 'none';
			    } 	
			    
			    clearInterval (idPreviewSectionLooking);						
			}						
		}			
	}	
}


function generatePreviewSectionRequestXML(datasource, title, query, description, params, sectionOutputType)
{
	title = formatToSendableString(title);
	query = formatToSendableString(query);
	description = formatToSendableString(description);
	description = description.replace(/[\n\r\t]/g, ' ');
	title = title.replace(/[\n\r\t]/g, ' ');
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_action>" + "PREVIEW" + "</rp_action>";	
	xml += "<rp_datasource>" + datasource + "</rp_datasource>";
	xml += "<rp_output>" + sectionOutputType + "</rp_output>";	
	xml += "<rp_section_title><![CDATA[" + title + "]]></rp_section_title>";
	xml += "<rp_section_description><![CDATA[" + description + "]]></rp_section_description>";	
	xml += "<rp_section_query><![CDATA[" + query + "]]></rp_section_query>";	
	xml += "<rp_section_params>";
	for(var i=0; i<params.length; i++) 
	{
		var param = params[i];
		var name = param[0];
		var type = param[1];
		var value = param[2]
		var defaultValue = param[3];
		var paramDesc = param[4];
		xml += "<rp_section_param>";
		xml += "<name><![CDATA[" + name + "]]></name>";
		xml += "<type><![CDATA[" + type + "]]></type>";
		xml += "<value><![CDATA[" + value + "]]></value>";
		xml += "<defaultValue><![CDATA[" + defaultValue + "]]></defaultValue>";	
		xml += "<description><![CDATA[" + paramDesc + "]]></description>";	
		xml += "</rp_section_param>";		
	}
	xml += "</rp_section_params>";
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function sendPreviewSectionRequest(title, query, description, sectionOutputType)
{	
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");		
		return false;
	}		
	
	if (!validateReportQuery(query))
	{
		var inputParamDialog = document.getElementById('input_param_dialog');
		if (inputParamDialog)
		{
			inputParamDialog.style.display='';
		} 
	
	    var overlay = document.getElementById('overlay');
	    if (overlay)
	    {
	    	overlay.style.display = 'none';
	    } 		
		return false;
	}
	var datasource = '';
	if (document.getElementById('report_datasource'))
	{
		datasource = document.getElementById('report_datasource').value;
	}
	
	
	var xml = generatePreviewSectionRequestXML(datasource, title, query, description, sectionParams, sectionOutputType);
	
	//alert(xml);
	var sqlReportURL = ajaxurl + "/PreviewSectionThreadHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handlePreviewSectionResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);
}

function handlePreviewSectionResponse(){
	
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					alert('Query preview error: ' + errors[0].firstChild.nodeValue);
					var inputParamDialog = document.getElementById('input_param_dialog');
					if (inputParamDialog)
					{
						inputParamDialog.style.display='';
					} 
				
				    var overlay = document.getElementById('overlay');
				    if (overlay)
				    {
				    	overlay.style.display = 'none';
				    } 	
				    
				    clearInterval (idPreviewSectionLooking);						
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpActionStatus = response.getElementsByTagName("rp_reportStatus")[0].firstChild.nodeValue;					
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						if (rpActionStatus != null && rpActionStatus == '1')
						{
							idPreviewSectionLooking = setInterval ("lookingPreviewRequest()", 2000);
						}
						else if (rpActionStatus != null && rpActionStatus == '2')
						{
							if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_PDF')
							{
								window.open('/content/pl/sql_report/report_pdf_complete.html', 'Preview_Section');
							}
							else if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_XLS')
							{
								window.open('/content/pl/sql_report/report_xls_complete.html', 'Preview_Section');							
							}														
						}		
					}
					else
					{
						alert('Query execution failed:\n' + rpReportOutput);
						var inputParamDialog = document.getElementById('input_param_dialog');
						if (inputParamDialog)
						{
							inputParamDialog.style.display='';
						} 
					
					    var overlay = document.getElementById('overlay');
					    if (overlay)
					    {
					    	overlay.style.display = 'none';
					    } 	
					    
					    clearInterval (idPreviewSectionLooking);						
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				var inputParamDialog = document.getElementById('input_param_dialog');
				if (inputParamDialog)
				{
					inputParamDialog.style.display='';
				} 
			
			    var overlay = document.getElementById('overlay');
			    if (overlay)
			    {
			    	overlay.style.display = 'none';
			    } 	
			    
			    clearInterval (idPreviewSectionLooking);						
			}						
		}		
	}
}

function sendPreviewSectionSynchRequest(title, query, description, sectionOutputType)
{			
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");
		return false;
	}
	previewCancelled = false;
	
	if (!validateReportQuery(query))
	{
		var inputParamDialog = document.getElementById('input_param_dialog');
		if (inputParamDialog)
		{
			inputParamDialog.style.display='';
		} 
	
	    var overlay = document.getElementById('overlay');
	    if (overlay)
	    {
	    	overlay.style.display = 'none';
	    } 		
		return false;
	}
	var datasource = '';
	if (document.getElementById('report_datasource'))
	{
		datasource = document.getElementById('report_datasource').value;
	}
	
	
	var xml = generatePreviewSectionRequestXML(datasource, title, query, description, sectionParams, sectionOutputType);
	
	//alert(xml);
	var sqlReportURL = ajaxurl + "/PreviewSectionHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handlePreviewSectionSynchResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);
}

function handlePreviewSectionSynchResponse(){

	if (previewCancelled) 
	{
		//alert('Dont tell any one..');
		return false;	// silently return
	}
	
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					alert('Query preview error: ' + errors[0].firstChild.nodeValue);
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;				
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_PDF')
						{
							window.open('/content/pl/sql_report/report_pdf_complete.html', 'Preview_Section');
						}
						else if (rpReportOutput != null && rpReportOutput == 'DOWNLOAD_XLS')
						{
							window.open('/content/pl/sql_report/report_xls_complete.html', 'Preview_Section');							
						}		
					}
					else
					{
						alert('Query execution failed:\n' + rpReportOutput);	// report output now contains the error message				    				
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);				
			}	
			
			var inputParamDialog = document.getElementById('input_param_dialog');
			if (inputParamDialog)
			{
				inputParamDialog.style.display='';
			} 
		
		    var overlay = document.getElementById('overlay');
		    if (overlay)
		    {
		    	overlay.style.display = 'none';
		    } 		
		}		
	}
}

function saveSection ()
{	
	// construc a array to hold section information
	var section = new Array();
	var section_title = document.getElementById('section_title').value;	
	var section_description = document.getElementById('section_description').value;
	var section_query = document.getElementById('section_query').value;
	if (!validateReportQuery(section_query))
	{
		return false;
	}
	section.push(section_title);
	section.push(section_description);
	section.push(section_query);
	section.push(sectionParams);		
	if (selectedSectionIndex == -1)	// add
	{
		// add this section to global report section array
		reportSections.push(section);		
	}
	else	// edit
	{
		reportSections[selectedSectionIndex] = section;
	}

	
	// update section list in HTML
	updateSectionListHTML();
	
	// disable datasource selection
	if (document.getElementById('report_datasource'))
	{
		document.getElementById('report_datasource').disabled = 'disabled';
	}
	
	closeSection();
}

function removeSection (sectionIndex)
{
	var confirmRemove = confirm('Are you sure to remove this section?');
	if (confirmRemove)
	{
		if (sectionIndex > -1)
		{
			var tmReportSections = reportSections;
			reportSections = new Array();
			
			for(var index = 0; index < tmReportSections.length; index++) {
				if (index != sectionIndex) {
					reportSections.push(tmReportSections[index]);
				}
			}		
		}
		
		updateSectionListHTML();
	}	
}

function editSection (sectionIndex)
{
	// select the section stored
	selectedSectionIndex = sectionIndex;
	if (0 <= sectionIndex && sectionIndex < reportSections.length)
	{
		document.getElementById('section_title').value = reportSections[sectionIndex][0];
		document.getElementById('section_description').value = reportSections[sectionIndex][1];
		document.getElementById('section_query').value = reportSections[sectionIndex][2];
		
		// update params
		sectionParams = reportSections[sectionIndex][3];
	}	
	updateParamListHtml();
	document.getElementById('add_section_id').style.display='block';
}

function updateSectionListHTML ()
{
	// update section list in HTML
	var sectionHolder = document.getElementById('report_sections');
	if (!sectionHolder)
	{
		return;
	}
	if (reportSections && reportSections.length > 0)
	{		
		var sectionHolderInnerHtml = '<table border="0" width="100%"><tbody>';
		for(var index=0; index < reportSections.length; index++) {
			var sectionTitle = reportSections[index][0];
			if (!sectionTitle || trim(sectionTitle) == "")
			{
				sectionTitle = 'Unknown title';
			}
			sectionTitle = safe_tags_replace(sectionTitle);
			
			sectionHolderInnerHtml = sectionHolderInnerHtml + '<tr><td class=\'table_title\'>' + sectionTitle + 
				'</td><td><a href="" onclick="editSection(' + index + '); return false;">Edit...</a></td>' +
				'<td><a href="" onclick ="removeSection(' + index + '); return false;">Remove...</a></td></tr>'
		}
		sectionHolderInnerHtml = sectionHolderInnerHtml + '</tbody></table>'
		sectionHolder.innerHTML = sectionHolderInnerHtml;
		//alert(sectionHolderInnerHtml);	
	}
	else
	{
		sectionHolder.innerHTML = '';
	}	
}

// Used for both Add or Edit request action
function generateAddEditReportRequestXML(action, rpId, datasource, title, description, sections)
{
	title = formatToSendableString(title);
	var tempTitle = trim(title);
	titleStartCharStr = '?startwith=' + tempTitle.charAt(0);
	title = title.replace(/[\n\r\t]/g, ' ');
	description = formatToSendableString(description);
	description = description.replace(/[\n\r\t]/g, ' ');
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_action>" + action + "</rp_action>";
	xml += "<rp_id>" + rpId + "</rp_id>";
	xml += "<rp_datasource>" + datasource + "</rp_datasource>";
	xml += "<rp_title><![CDATA[" + title + "]]></rp_title>";
	xml += "<rp_description><![CDATA[" + description + "]]></rp_description>";	
	xml += "<rp_sections>";
	for(var i=0; i<sections.length; i++) 
	{
		var section = sections[i];
		var params = section[3];
		var sectionTitle = formatToSendableString(section[0]);
		sectionTitle = sectionTitle.replace(/[\n\r\t]/g, ' ');
		var sectionDescription= formatToSendableString(section[1]);
		sectionDescription = sectionDescription.replace(/[\n\r\t]/g, ' ');
		xml += "<rp_section>";
		xml += "<rp_section_title><![CDATA[" + sectionTitle + "]]></rp_section_title>";
		xml += "<rp_section_description><![CDATA[" + sectionDescription + "]]></rp_section_description>";
		var query = formatToSendableString(section[2]);
		xml += "<rp_section_query><![CDATA[" + query + "]]></rp_section_query>";	
		xml += "<rp_section_params>";
		for(var j=0; j<params.length; j++) 
		{
			var param = params[j];
			var name = param[0];
			var type = param[1];
			var value = param[2]
			var defaultValue = param[3];
			var paramDesc = param[4];
			xml += "<rp_section_param>";
			xml += "<name><![CDATA[" + name + "]]></name>";
			xml += "<type><![CDATA[" + type + "]]></type>";
			xml += "<value><![CDATA[" + value + "]]></value>";
			xml += "<defaultValue><![CDATA[" + defaultValue + "]]></defaultValue>";	
			xml += "<description><![CDATA[" + paramDesc + "]]></description>";	
			xml += "</rp_section_param>";		
		}
		xml += "</rp_section_params>";		
		xml += "</rp_section>";		
	}
	xml += "</rp_sections>";
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function sendAddReportRequest()
{
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");
		return false;
	}
	
	var xml = '';
	var datasource = document.getElementById('report_datasource').value;
	var title = document.getElementById('report_title').value;
	var description = document.getElementById('report_description').value;
	if (!title || title.length == 0)
	{
		alert("Please supply a title for this report!");
		return false;
	}
	xml = generateAddEditReportRequestXML(0, '', datasource, title, description, reportSections);		
	//var 
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ManageSQLReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);	
	
	var overlay = document.getElementById('overlay');
    if (overlay == null)
    {
    	var bod 			= document.getElementsByTagName('body')[0];
    	overlay 			= document.createElement('div');
    	overlay.id			= 'overlay';
    	bod.appendChild(overlay);
    }
    overlay.style.display = 'block';	
	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
    	inputParamDialog.innerHTML = '<div class="dialog_title">Please wait...</div>';
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
    			'<p>' +
    			'<span><img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    			'<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    
    			'</p>';
        inputParamDialog.style.display='block';
        inputParamDialog.style.position = 'absolute';
        inputParamDialog.style.pixelTop = 350;
        inputParamDialog.style.pixelLeft = 300;    	
	}	
}

function sendEditReportRequest(rpId)
{
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");
		return false;
	}
	
	var xml = '';	
	var title = document.getElementById('report_title').value;
	var description = document.getElementById('report_description').value;
	var datasource = document.getElementById('report_datasource').value;
	if (!title || title.length == 0)
	{
		alert("Please supply a title for this report!");
		return false;
	}
	xml = generateAddEditReportRequestXML(1, rpId, datasource, title, description, reportSections);	
	
	//var 
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ManageSQLReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);	
	
	var overlay = document.getElementById('overlay');
    if (overlay == null)
    {
    	var bod 			= document.getElementsByTagName('body')[0];
    	overlay 			= document.createElement('div');
    	overlay.id			= 'overlay';
    	bod.appendChild(overlay);
    }
    overlay.style.display = 'block';	
	var inputParamDialog = document.getElementById('input_param_dialog');
	if (inputParamDialog)
	{
    	inputParamDialog.innerHTML = '<div class="dialog_title">Please wait...</div>';
    	inputParamDialog.innerHTML = inputParamDialog.innerHTML + 
    			'<p>' +
    			'<span><img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    			'<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +
    		    '<span>&nbsp;<img src="/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif" style="padding-top: 4px;"/></span>' +	    
    			'</p>';
    	inputParamDialog.style.display='block';
        inputParamDialog.style.position = 'absolute';
        inputParamDialog.style.pixelTop = 350;
        inputParamDialog.style.pixelLeft = 300;
	}	
}

function handleManageReportResponse(){

   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				//alert(errors.length);
				if(errors.length > 0)
				{
					alert('Action failed. Reason: \n' + errors[0].firstChild.nodeValue);
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{
						window.onbeforeunload = '';
						window.location.replace('/content/pl/sql_report/manage_report/index.html' + titleStartCharStr);
					}
					else
					{
						alert('Action failed: Unknown reason!');
					}					
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
		
		var inputParamDialog = document.getElementById('input_param_dialog');
		if (inputParamDialog)
		{
			inputParamDialog.style.display='';
		} 
	
	    var overlay = document.getElementById('overlay');
	    if (overlay)
	    {
	    	overlay.style.display = 'none';
	    }		
	}
}

// Running report

function SelectAllReports()
{	
	var allReportCb = document.getElementById('cb_all_reports');
	
	if (allReportCb != null)
	{
		var checked = false;
		if (allReportCb.checked)
		{
			checked = true;
		}
		
		for(var i=0; i<runningReportArray.length; i++) 
		{
			var reportIdStored = runningReportArray[i][0];
			var reportCb = document.getElementById('cb_' + reportIdStored);
			if (reportCb)
			{
				if (checked)
				{
					reportCb.checked = true;
				}
				else
				{
					reportCb.checked = false;
				}
			}
			
			// find sections belong to report
			var sections = runningReportArray[i][2];
			for(var j=0; j<sections.length; j++) 
			{
				var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
				if (subCb) 
				{
					if (checked)
					{
						subCb.checked = true;
					}
					else
					{
						subCb.checked = false;
					}
				}
			}
		}		
	}
	else
	{
		return false;
	}

}

function SelectAllSections(reportId)
{	
	var reportCb = document.getElementById('cb_' + reportId);
	
	if (reportCb != null)
	{
		var checked = false;
		if (reportCb.checked)
		{
			checked = true;
		}
		
		for(var i=0; i<runningReportArray.length; i++) 
		{
			var reportIdStored = runningReportArray[i][0];
			if (reportId == reportIdStored) 
			{
				// find sections belong to report
				var sections = runningReportArray[i][2];
				for(var j=0; j<sections.length; j++) 
				{
					var subCb = document.getElementById('cb_' + reportId + '_' + sections[j][0]);
					if (subCb) 
					{
						if (checked)
						{
							subCb.checked = true;
						}
						else
						{
							subCb.checked = false;
						}
					}
				}
				
				break;
			}
		}		
	}
	else
	{
		return false;
	}
	
	UpdateSelectAllReports();
}

function UpdateSelectAllSections(reportId)
{	
	var reportCb = document.getElementById('cb_' + reportId);
	
	if (reportCb != null)
	{
		var checked = true;		
		
		for(var i=0; i<runningReportArray.length; i++) 
		{
			var reportIdStored = runningReportArray[i][0];
			if (reportId == reportIdStored) 
			{
				// find sections belong to report
				var sections = runningReportArray[i][2];
				for(var j=0; j<sections.length; j++) 
				{
					var subCb = document.getElementById('cb_' + reportId + '_' + sections[j][0]);
					if (subCb.checked == false) 
					{
						checked = false;
						break;
					}				
				}
				
				break;
			}
		}
		
		if (checked)		
		{
			reportCb.checked = true;
		}
		else
		{
			reportCb.checked = false;
		}
	}
	else
	{
		return false;
	}
	UpdateSelectAllReports();

}


function UpdateSelectAllReports()
{	
	var allReportCb = document.getElementById('cb_all_reports');
	
	if (allReportCb != null)
	{
		var checked = true;		
		
		for(var i=0; i<runningReportArray.length; i++) 
		{
			var reportIdStored = runningReportArray[i][0];
			var reportCb = document.getElementById('cb_' + reportIdStored);
			if (reportCb)
			{
				if (reportCb.checked == false)
				{
					checked = false;
					break;
				}
			}						
		}
		
		if (checked)		
		{
			allReportCb.checked = true;
		}
		else
		{
			allReportCb.checked = false;
		}
	}
	else
	{
		return false;
	}

}

function generateRunSelectedReportXML()
{
	if (reportGroupId == null || reportGroupId.length == 0 || 
		!selectedRunningReportArray || selectedRunningReportArray == null)
	{
		alert('You need to select at least one report to run.');
		return null;
	} 


	var reportXml = "";
	reportXml += "<AjaxReportParameterRequest>";	
	reportXml += "<rp_group_id>" + reportGroupId + "</rp_group_id>";		
	reportXml += "<rp_runner>" + reportRunner + "</rp_runner>";	
	reportXml += "<rp_output>" + reportOutputFormat + "</rp_output>";	
	reportXml += "<rp_startdate>" + reportStartDate + "</rp_startdate>";	
	reportXml += "<rp_enddate>" + reportEndDate + "</rp_enddate>";		
	for(var i=0; i<selectedRunningReportArray.length; i++) 
	{
		var reportId = selectedRunningReportArray[i][0];
		var sections = selectedRunningReportArray[i][2];
		reportXml += "<rp_report>";
		reportXml += "<rp_id>" + reportId + "</rp_id>"
		reportXml += "<rp_sections>";
		for(var j=0; j<sections.length; j++) 
		{
			reportXml += "<rp_section>";
			var section = sections[j];
			var sectionId = section[0];
			reportXml += "<rp_section_id>" + sections[j][0] + "</rp_section_id>";			
			if (section.length > 2)
			{
				reportXml += "<rp_section_params>";
				var sectionParams = section[2];
				for(var k=0; k<sectionParams.length; k++) 
				{
					reportXml += "<rp_section_param>";		
					reportXml += "<name>" + sectionParams[k][0] + "</name>";									
					reportXml += "<value>" + sectionParams[k][1] + "</value>";	
					reportXml += "<type>" + sectionParams[k][2] + "</type>";						
					reportXml += "</rp_section_param>";										
				}
				reportXml += "</rp_section_params>";				
			} 
			reportXml += "</rp_section>";			
		}
		reportXml += "</rp_sections>";
		reportXml += "</rp_report>";
	}
	
	reportXml += "</AjaxReportParameterRequest>";
	
	return reportXml;
}

function runSelectedReports()
{
	if (runningReportArray.length > 0)
	{	
		var reportRunnerEle = document.getElementById('report_runner');
		if (reportRunnerEle)
		{
			reportRunner = reportRunnerEle.value;
		} 	
		
		var reportOutputSelect = document.getElementById('report_output_type');
		if (reportOutputSelect)
		{
			reportOutputFormat = reportOutputSelect.value;
		} 

		renderParamsInputFormColapse();				
	}
}

function sendRunReportRequest()
{	
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");
		return false;
	}	
			
	if (!InputReportDateValidate())
	{
		return false;	
	}
		
	var xml = generateRunSelectedReportXML();
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/RunSQLReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleRunReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);		
	reportWhizyVisi(true);
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);		
}

function handleRunReportResponse(){

   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					//alert('Run report failed!');
					alert("" + response.getElementsByTagName("Error")[0].firstChild.nodeValue);
					reportWhizyVisi(false);
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;
					var rpTime = response.getElementsByTagName("rp_reportTime")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						window.open('/content/pl/sql_report/run_report_complete.jsp?output=' + rpReportOutput + '&time=' + rpTime, 'Report_Complete');						
					}
					else
					{
						alert('Query execution failed:\n' + rpReportOutput);
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}
			
			reportWhizyVisi(false);						
		}
	}

}

function renderParamsInputFormColapse ()
{
	selectedRunningReportArray = new Array();
	var accessReportForm = document.getElementById('access_report_form');
	var renderParamsReportForm = document.getElementById('render_params_form');
	if (!accessReportForm || !renderParamsReportForm) return false;
		
	// backup last content
	lastContentHTML = accessReportForm.innerHTML;
	
	var updateContentHTML = '<h1>Input report parameter values</h1>';
	updateContentHTML += '<p>Based on the parameters of the queries in the report the form bellow is rendered which contains all input parameters' + 
			'to run the reports you have requested that have been grouped by their name. Paramters with the same name will share a value supplied. ' +
			'If you want to use values for parameters individually please select \'Use parameter values individually\'.';	
	updateContentHTML += '<p>To produce the report you must complete the form by filling all parameter values then click \'Ok\' button.</p>';
	updateContentHTML += '<p>To go back to run report page click \'Back\' button.</p>';
//	updateContentHTML += '<p><input type="checkbox" id="expanseSections" checked="checked" onclick="renderParamsInputFormExpanse();"/>&nbsp;' +
//			'<label>Use parameter values individually</label></p>';		
	updateContentHTML += '<div class="report_list">';
	updateContentHTML += '<div class="report_param_input">';
		
	// please refer to access_report_ingroup.jsp to get understandings of runningReportArray.	
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		var reportTitleStored = runningReportArray[i][1];
		
		var sectionTitleLabel = '';		
		var hasSectionsSelected = false;
		var selectedSections = new Array();
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb && subCb.checked) 
			{
				hasSectionsSelected = true;
				selectedSections.push(sections[j]);
				var sectionTitleStored = sections[j][1];
				sectionTitleLabel = sectionTitleLabel + sectionTitleStored;				
				sectionTitleLabel += '&nbsp;&nbsp;&nbsp;&nbsp; ';					
			}
		}
		
		if (hasSectionsSelected)
		{
			updateContentHTML += '<h2>' + reportTitleStored + '</h2>';
			updateContentHTML += '<h3>' + sectionTitleLabel + '</h3>';
		}			
		

		if (selectedSections.length > 0)
		{
			selectedRunningReportArray.push(new Array (reportIdStored, reportTitleStored, selectedSections));
		}  
	}
	
	if (selectedRunningReportArray == null || !selectedRunningReportArray || selectedRunningReportArray.length == 0)
	{
		alert('Please select a report!');
		return false;
	} 
	else
	{						
//			if (!hasParamInput)
//			{
//				sendRunReportRequest();
//			}
		
	}	
	
	updateContentHTML += '<div class="login_component_holder">'+
			'<div class="login_label"><label>Report start date</label> </div>' +
			'<div class="login_value"><input type="text" id="report_start_date" size="25" readonly="readonly" value="' + todayDateStr + '"/><input type="button" value=" ... " onclick="return showCalendar(\'report_start_date\', \'%e-%b-%y\');"/></div>' +
			'</div>';
	updateContentHTML += '<div class="login_component_holder">'+
			'<div class="login_label"><label>Report end date</label> </div>' +
			'<div class="login_value"><input type="text" id="report_end_date" size="25" readonly="readonly" value="' + todayDateStr + '"/><input type="button" value=" ... " onclick="return showCalendar(\'report_end_date\', \'%e-%b-%y\');"/></div>' +
			'</div>';					
	
	var paramRenderArray = new Array();
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		
		var selectedSections = new Array();
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb && subCb.checked) 
			{
				if (sections[j].length > 2)	// has params input contain at section[j][2]
				{
					hasParamInput = true;								
										
					var selectedParams = sections[j][2];
					for(var k=0; k<selectedParams.length; k++) 
					{
						var selectedParam = selectedParams[k];
						var paramValueEleId = reportIdStored + '_' + sections[j][0] + '_' + selectedParam[0];
						var paramValue = selectedParam[1];
						// check if param name is start date or end date first
						if (checkParamNameIsStartDate(selectedParam[0]) || checkParamNameIsEndDate(selectedParam[0]))
						{
							// render as hidden
							updateContentHTML = updateContentHTML + 
								'<div class="login_component_holder" style="display: none;"><input id="' + paramValueEleId + '" type="text""/> </div>';									
						}
						else
						{
							var paramExist = false;
							for(var m=0; m<paramRenderArray.length; m++)
							{
								var paramRender = paramRenderArray[m];
								if (paramRender[0] == selectedParam[0])
								{								
									paramExist = true;
									break;	
								}
							}
							if (!paramExist)
							{				
								paramRenderArray.push(new Array(selectedParam[0], ''));
								
								if (selectedParam[2] == '3')
								{																					
									updateContentHTML = updateContentHTML + 
									'<div class="login_component_holder">' +
									'<div class="login_label">' + selectedParam[0] + '</div>' + 
									'<div class="login_value"><input id="' + paramValueEleId + '" type="text"  onclick="return showCalendar(\'' + paramValueEleId + '\', \'%e-%b-%y\');"/> </div>' +
									'</div>';							
								}
								else
								{
									updateContentHTML = updateContentHTML + 
									'<div class="login_component_holder">' +
									'<div class="login_label">' + selectedParam[0] + '</div>' + 
									'<div class="login_value"><input id="' + paramValueEleId + '" type="text"/> </div>' +
									'</div>';								
								}
							}
							else
							{
								// render as hidden
								updateContentHTML = updateContentHTML + 
									'<div class="login_component_holder" style="display: none;"><input id="' + paramValueEleId + '" type="text""/> </div>';								
							}
						}
					}						
				}
			}
		}
	}	
	
	updateContentHTML += '</div></div>';
	updateContentHTML = updateContentHTML +
			'<div class="report_component_holder">' +
			'<div style="TEXT-ALIGN: right"><input class="goButton" id="_run_report_confirm" type="button" onclick="updateSelectedReportParamValues();" value="Ok" />&nbsp;' +
			'<input class="goButton" id="_back_to_run_report" type="button" onclick="backToForm();" value="Back" />&nbsp;' +			
			'</div>' + 
			'</div>';

	accessReportForm.style.display = "none";
	renderParamsReportForm.innerHTML = updateContentHTML;	
	renderParamsReportForm.style.display = "block";
	
}

function renderParamsInputFormExpanse ()
{
	selectedRunningReportArray = new Array();
	var accessReportForm = document.getElementById('access_report_form');
	var renderParamsReportForm = document.getElementById('render_params_form');
	if (!accessReportForm || !renderParamsReportForm) return false;
	
	// backup last content
	lastContentHTML = accessReportForm.innerHTML;
	
	var updateContentHTML = '<h1>Input report parameter values</h1>';
	updateContentHTML += '<p>Based on the parameters of the queries in the report the form bellow is rendered which contains all input parameters' + 
			'to run the reports you have requested that have been listed by sections. If you want to use a sole value for parameters with the same name' +
			' please select \'Use sole value for parameters with the same name\'.';	
	updateContentHTML += '<p>To produce the report you must complete the form by filling all parameter values then click \'Ok\' button.</p>';
	updateContentHTML += '<p>To go back to run report page click \'Back\' button.</p>';
	updateContentHTML += '<p><input type="checkbox" id="expanseSections" onclick="renderParamsInputFormColapse();"/>&nbsp;' +
			'<label>Use sole value for parameters with the same name</label></p>';
	updateContentHTML += '<div class="report_list">';
	
	// store current values set to parameters  to storedParamRenderArray array
	var storedParamRenderArray = new Array();
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		
		var selectedSections = new Array();
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			if (sections[j].length > 2)	// has params input contain at section[j][2]
			{							
				var selectedParams = sections[j][2];
				for(var k=0; k<selectedParams.length; k++) 
				{
					var selectedParam = selectedParams[k];
					var paramValueEleId = reportIdStored + '_' + sections[j][0] + '_' + selectedParam[0];
					if (document.getElementById(paramValueEleId))
					{
						var currentParamValue = document.getElementById(paramValueEleId).value;
					
						var paramExist = false;
						for(var m=0; m<storedParamRenderArray.length; m++)
						{
							var paramRender = storedParamRenderArray[m];
							if (paramRender[0] == selectedParam[0])
							{								
								paramExist = true;
								break;	
							}
						}
						if (!paramExist)
						{	
							storedParamRenderArray.push(new Array(selectedParam[0], currentParamValue));
						}						
					}					
				}
			}						
		}
	}
			
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		var reportTitleStored = runningReportArray[i][1];

		var reportTitleDrawn = false;
		var selectedSections = new Array();
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb && subCb.checked) 
			{
				selectedSections.push(sections[j]);
				if (!reportTitleDrawn)
				{
					updateContentHTML += '<div class="report_param_input">';
					updateContentHTML += '<h2>' + reportTitleStored + '</h2>';
				}
				reportTitleDrawn = true;	
								
				var sectionTitleStored = sections[j][1];
																	
				if (sections[j].length > 2)	// has params input contain at section[j][2]
				{
					hasParamInput = true;	
					updateContentHTML += '<h3>' + sectionTitleStored + '</h3>';										
					var selectedParams = sections[j][2];
					for(var k=0; k<selectedParams.length; k++) 
					{
						var selectedParam = selectedParams[k];
						var paramValueEleId = reportIdStored + '_' + sections[j][0] + '_' + selectedParam[0];
						var paramValue = selectedParam[1];
						
						// find the stored value for this param
						for(var m=0; m<storedParamRenderArray.length; m++)
						{
							var paramRender = storedParamRenderArray[m];
							if (paramRender[0] == selectedParam[0])
							{								
								paramValue = paramRender[1];
								break;	
							}
						}
												
						if (selectedParam[2] == '3')
						{
							updateContentHTML = updateContentHTML + 
							'<div class="login_component_holder">' +
							'<div class="login_label">' + selectedParam[0] + '</div>' + 
							'<div class="login_value"><input id="' + paramValueEleId + '" type="text" ' +
							'value="' + paramValue + '" onclick="return showCalendar(\'' + paramValueEleId + '\', \'%e-%b-%y\');"/> </div>' +
							'</div>';							
						}
						else
						{
							updateContentHTML = updateContentHTML + 
							'<div class="login_component_holder">' +
							'<div class="login_label">' + selectedParam[0] + '</div>' + 
							'<div class="login_value"><input id="' + paramValueEleId + '" type="text" ' +
							'value="' + paramValue + '"/> </div>' +
							'</div>';								
						}
					}						
				}
				else
				{
					updateContentHTML += '<h3>' + sectionTitleStored + ' (No parameters)</h3>';
													
				}
				updateContentHTML += '<br/>';				
			}
		}
		
		if (reportTitleDrawn)
		{			
			updateContentHTML += '</div>';
		}
		
		if (selectedSections.length > 0)
		{
			selectedRunningReportArray.push(new Array (reportIdStored, reportTitleStored, selectedSections));
		}  
	}
	
	updateContentHTML += '</div>';
	updateContentHTML = updateContentHTML +
			'<div class="report_component_holder">' +
			'<div style="TEXT-ALIGN: right"><input class="goButton" id="_run_report_confirm" type="button" onclick="updateSelectedReportParamValues();" value="Ok" />&nbsp;' +
			'<input class="goButton" id="_back_to_run_report" type="button" onclick="backToForm();" value="Back" />&nbsp;' +			
			'</div>' + 
			'</div>';
		
	renderParamsReportForm.innerHTML = updateContentHTML;	
	renderParamsReportForm.style.display = "block";
	
}

function findDefaultRenderParamValue(paramName)
{
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb && subCb.checked) 
			{
				if (sections[j].length > 2)	// has params input contain at section[j][2]
				{				
					var selectedParams = sections[j][2];	
					for(var k=0; k<selectedParams.length; k++) 
					{
						var selectedParam = selectedParams[k];						
						var paramValue = selectedParam[1];
						if (selectedParam[0] == paramName && trim(paramValue).length > 0)
						{
							return paramValue;
						}						
					}						
				}			
			}
		}
	}
	
	return '';	
}

function updateSelectedReportParamValues ()
{
	var paramValidated = true;
	if (!selectedRunningReportArray || selectedRunningReportArray == null)
	{
		return false;
	} 

	reportStartDate = document.getElementById('report_start_date').value;
	reportEndDate = document.getElementById('report_end_date').value;
	
//	if (document.getElementById('expanseSections') != null && document.getElementById('expanseSections').checked )
	{
		var renderParams = new Array();
		for(var i=0; i<selectedRunningReportArray.length; i++) 
		{
			var reportId = selectedRunningReportArray[i][0];
			var sections = selectedRunningReportArray[i][2];
			for(var j=0; j<sections.length; j++) 
			{
				var section = sections[j];
				var sectionId = section[0];		
				if (section.length > 2)
				{
					var sectionParams = section[2];
					for(var k=0; k<sectionParams.length; k++) 
					{
						var paramName = sectionParams[k][0];
						var paramType = sectionParams[k][2]
						var paramValueEleId = reportId + '_' + sectionId + '_' + paramName;
						var paramValueEle = document.getElementById(paramValueEleId);
						if (paramValueEle)
						{
							// check if param is start date or end date first
							if (checkParamNameIsStartDate(paramName))
							{
								sectionParams[k][1] = reportStartDate;
							}
							else if (checkParamNameIsEndDate(paramName))
							{
								sectionParams[k][1] = reportEndDate;
							}
							else
							{
								var paramValue = trim(paramValueEle.value);
								if (paramValue.length > 0)
								{
									// add to paramRenders array
									renderParams.push(new Array(paramName, paramValueEle.value, paramType));
									sectionParams[k][1] = paramValueEle.value;
								} 
								else
								{
									// find value in renderParams array
									for(var m=0; m<renderParams.length; m++) 
									{
										var renderParam = renderParams[m];
										if (renderParam[0] == paramName)
										{
											sectionParams[k][1] = renderParam[1];
											paramValue = renderParam[1];
										}
									}								
								}
								
								if (paramValue.length == 0)
								{
									alert ('Please fill in value for parameter ' + paramName);
									return false;
								}		
							}					
						}
						if (paramType == '2' && !validateParamTypeNumber (paramValueEle.value))
						{
							alert ('Parameter ' + paramName + ' is typed as number. Please in put a valid number!');
							return false;
						}
					}				
				} 		
			}
		}		
	} 
//	else
//	{
//		for(var i=0; i<selectedRunningReportArray.length; i++) 
//		{
//			var reportId = selectedRunningReportArray[i][0];
//			var sections = selectedRunningReportArray[i][2];
//			for(var j=0; j<sections.length; j++) 
//			{
//				var section = sections[j];
//				var sectionId = section[0];		
//				if (section.length > 2)
//				{
//					var sectionParams = section[2];
//					for(var k=0; k<sectionParams.length; k++) 
//					{
//						var paramName = sectionParams[k][0];
//						var paramType = sectionParams[k][2]
//						var paramValueEleId = reportId + '_' + sectionId + '_' + paramName;
//						var paramValueEle = document.getElementById(paramValueEleId);
//						if (paramValueEle)
//						{
//							sectionParams[k][1] = paramValueEle.value;
//						}
//						if (paramType == '2' && !validateParamTypeNumber (paramValueEle.value))
//						{
//							alert ('Parameter ' + paramName + ' is typed as number. Please in put a valid number!');
//							return  false;
//						}
//					}				
//				} 		
//			}
//		}		
//	}
//	var renderParamsReportForm = document.getElementById('render_params_form');
//	if (renderParamsReportForm)
//	{
//		renderParamsReportForm.style.display = "block";
//	}
	sendRunReportRequest();
}

function backToForm()
{
	var accessReportForm = document.getElementById('access_report_form')
	var renderParamsReportForm = document.getElementById('render_params_form')
	
	if (accessReportForm)
	{
		accessReportForm.style.display = "block";
	}
	if (renderParamsReportForm)
	{
		renderParamsReportForm.style.display = "none";
	}
}

function cleanupReports()
{		

	//alert(xml);
	
	var sqlReportURL = "/content/pl/sql_report/cleanupReports.jsp";
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL);
	
	sqlReportHttp.onreadystatechange = handleCleanupReportsRepsponse;
	
	sqlReportHttp.send(null);
}

function handleCleanupReportsRepsponse ()
{
	// do nothing
}

function generateRemoveReportXML (reportId)
{
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_action>2</rp_action>";
	xml += "<rp_id>" + reportId + "</rp_id>";
	xml += "</AjaxReportParameterRequest>";
	return xml;	
}

function sendRemoveReportRequest (reportId)
{
	if (isSessionTimeout) {
		window.location.replace("/content/pl/_login_ask.html");
		return false;
	}
	
	var confirmRemove = confirm('Are you sure to remove this report?');
	if (!confirmRemove)
	{
		return false;
	}
	
	var xml = generateRemoveReportXML(reportId);
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ManageSQLReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
	// reset session Timeout
	clearTimeout(sessionTimeoutId);
	sessionTimeoutId = setTimeout('sessionTimeout()', 1800000);				
}

function reportWhizyVisi (visible)
{
	var waitingImage = 
	"<p><span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
	"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +	
	"</p>";	
	var updateContentHTML = '<h1>Generating report...</h1>';
	
	if (visible)
	{
		document.getElementById('render_params_form').style.display = "none";
		document.getElementById('waiting_message').style.display = "block";
		document.getElementById('waiting_message').innerHTML = updateContentHTML + waitingImage;
	}
	else
	{
		document.getElementById('render_params_form').style.display = "block";
		document.getElementById('waiting_message').style.display = "none";		
	}		
}

function _findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
        
    return curleft;
  }  
function _findPosY(obj)
{
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
        
    return curtop;
}

function isDateAfter (date, comparedDate)
{
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	
	var getMonth = function(monthStr)
	{
		for(var index=0; index<monthNames.length; index++) {
			if (monthStr == monthNames[index])
			{
				return (index + 1);				
			}			
		}
		return -1;
	}
	
	// compare year
	if (date.indexOf(',') > 0 && comparedDate.indexOf(',') > 0) {
		var year = parseInt(date.substring(date.indexOf(',') + 1));
		var comparedYear = parseInt(comparedDate.substring(comparedDate.indexOf(',') + 1));
		//alert(year + ' with ' + comparedYear);
		if (year < comparedYear)
		{
			return false;
		}
		else if (year > comparedYear)
		{
			return true;
		}
	}
	else
	{
		return false;
	}
	
	// compare month
	if (date.indexOf(' ') > 0 && comparedDate.indexOf(' ') > 0) {
		var month = getMonth(date.substring(0, date.indexOf(' ')));
		var comparedMonth = getMonth(comparedDate.substring(0, comparedDate.indexOf(' ')));
		//alert(month + ' with ' + comparedMonth);
		if (month == -1 || comparedMonth == -1) return false;
		
		if (month < comparedMonth) 
		{
			return false;
		} else if (month > comparedMonth)
		{
			return true;
		}

	}
	else
	{
		return false;
	}
	
	// compare date
	var day = parseInt(date.substring(date.indexOf(' ') + 1, date.indexOf(',')));
	var comparedDay = parseInt(comparedDate.substring(comparedDate.indexOf(' ') + 1, comparedDate.indexOf(',')));
	//alert(day + ' with ' + comparedDay);
	if (day < comparedDay){
		//alert('reutrn false');
		return false;		
	}
	return true;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked)
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
  //cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {  
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.element.style.zIndex = '999';
  _dynarch_popupCalendar.element.style.position = 'absolute';
  var pos = Calendar.getAbsolutePos(el);
  _dynarch_popupCalendar.showAt(pos.x, pos.y + el.offsetHeight);   
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendarAt(id, format, showsTime, showsOtherMonths, anchorEle) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.element.style.zIndex = '999';
  _dynarch_popupCalendar.element.style.position = 'absolute';
  var pos = Calendar.getAbsolutePos(el);
  _dynarch_popupCalendar.showAt(pos.x, pos.y + el.offsetHeight);        // show the calendar
  return false;
}

function validateReportQuery (query)
{
	if (query != null)
	{		
		if (query.indexOf(';') >= 0)
		{
			alert('The delimeter character(\';\') is not allowed to appeared in the query!');
			return false;
		}
		if (query.trim().toLowerCase().indexOf('update') == 0 || query.toLowerCase().indexOf('insert') == 0 )
		{
			alert ('The query must be an \'SELECT\' query!');
			return false;
		}
		
		//param name can be: :Param_name, '&param_name', @Variable('param_name')
		
//		var params = query.match(/([<]{1}|[<]{1}[=]{1}|[>]{1}|[>]{1}[=]{1}|[=]{1}|[<]{1}[>]{1}|[!]{1}[=]{1}|[Bb]{1}[Ee]{1}[Tt]{1}[Ww]{1}[Ee]{1}[Ee]{1}[Nn]{1}[ ]+|[Aa]{1}[Nn]{1}[Dd]{1}[ ]+|[Ll]{1}[Ii]{1}[Kk]{1}[Ee]{1}[ ]+|[Ii]{1}[Nn]{1}[ ]*[\(]?[ ]*)[ ]*(([ ]*[:]{1}[\w]+)|([']{1}[\&]{1}[\w]+[']{1})|([@]{1}[vV]{1}[aA]{1}[rR]{1}[iI]{1}[aA]{1}[bB]{1}[lL]{1}[eE]{1}[(]{1}[']{1}[\w]+[']{1}[)]{1}))/g);
//		if (params != null && params.length > 0)
//		{
//			for(var i=0; i<params.length; i++) 
//			{
//				var paramName = params[i];					
//				if (paramName && paramName.indexOf(':') > -1)
//				{
//					paramName = paramName.substring(paramName.indexOf(':') + 1);
//				}
//				else if (paramName && paramName.indexOf('\'&') > -1)
//				{
//					paramName = paramName.substring(paramName.indexOf('\'&') + 2, paramName.length-1);
//				}
//				else if (paramName && paramName.indexOf('@variable(\'') > -1)
//				{
//					paramName = paramName.substring(paramName.indexOf('@variable(\'') + 11, paramName.length-2);;																				
//				}
//				
//				paramName = paramName.replace(/[ ]+/g, '_');
//				if (!isParamExist(paramName))
//				{
//					alert('Parameter ' + paramName + " doesn't exist!");
//					return false;
//				}										
//			}
//			
//			return true;
//		}
//		else
//		{
//			return true;
//		}

		return true;
	}
	
	alert('The SQL query is not valid! Please check it again.');
	return false;
}

function validateParamName (param)
{
	if (!param || param == null || param.length == 0)
	{
		return false;	
	} 
	
	if (/[\W]/.test(param))
	{
		return false;
	}

	return true;
}

function validateParamTypeNumber (defaultValue)
{	
	if (defaultValue && defaultValue.length > 0)
	{
		for(var i=0; i<defaultValue.length; i++) 
		{
			var numberAt = defaultValue.charAt(i);
			if (!(/[\.0-9]/.test(numberAt)))
			{
				return false;
			}
		}
				
	}

	return true;
}

function isParamExist(param)
{
	if (sectionParams != null)
	{
		for(var i=0; i<sectionParams.length; i++) 
		{
			var paramName = sectionParams[i][0];
			if (paramName.toUpperCase() == param.toUpperCase()) return true;
		}
	} 

	return false;
}

function updateParamTypeHelpers (paramType, updateTextFieldId)
{
	if (paramType != null)
	{
		var updateEle = document.getElementById(updateTextFieldId);			
		if (updateEle)
		{					
			if (paramType == 'PARAM_TYPE_DATE')
			{
				updateEle.onclick=function(){return showCalendarAt(updateTextFieldId, '%e-%b-%y');}; 
			}
			else if (paramType == 'PARAM_TYPE_STRING')
			{
				updateEle.onclick=function(){};
			}
			else if (paramType == 'PARAM_TYPE_NUMBER')
			{
				updateEle.onclick=function(){};
			}
		}
	}	
}

function generateManageReportGroupRequestXML (action, groupId, groupName)
{
	groupName = formatToSendableString(groupName);
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_action>" + action + "</rp_action>";
	xml += "<rp_group_id>" + groupId + "</rp_group_id>";
	xml += "<rp_group_name><![CDATA[" + groupName + "]]></rp_group_name>";
	xml += "</AjaxReportParameterRequest>";
	return xml;	
}

function sendAddReportGroupRequest()
{
	var xml = '';
	var name = document.getElementById('report_group_name').value;
	if (!name || name.length == 0)
	{
		alert("Please supply a name for this report group!");
		return false;
	}
	xml = generateManageReportGroupRequestXML(0, '', name);
	
	//var 
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ManageReportGroupHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportGroupResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
}

function sendEditReportGroupRequest(groupId)
{
	var xml = '';
	var name = document.getElementById('report_group_name').value;
	if (!name || name.length == 0)
	{
		alert("Please supply a name for this report group!");
		return false;
	}
	xml = generateManageReportGroupRequestXML(1, groupId, name);
	
	//var 
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ManageReportGroupHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportGroupResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
}

function sendRemoveReportGroupRequest(groupId)
{
	var xml = generateManageReportGroupRequestXML(2, groupId, '');
	
	var confirmRemove = confirm('Are you sure to remove this group?');
	if (!confirmRemove)
	{
		return false;
	}
	
	var sqlReportURL = ajaxurl + "/ManageReportGroupHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleManageReportGroupResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
}

function handleManageReportGroupResponse ()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0)
				{
					alert('Action failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{
						window.location.replace('index.html');
					}
					else
					{
						alert('Action failed!');
					}					
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}				
	}			
}

function cancelAddEditReportGroup()
{
	window.location.replace('index.html');
}

function generateSaveGroupForReportRequestXML (reportId)
{
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_id>" + reportId + "</rp_id>";
	xml += "<rp_groups>";
	for(var i=0; i<reportGroupArray.length; i++) 
	{
		var groupId = reportGroupArray[i];
		var groupCbElement = document.getElementById(groupId);
		if (groupCbElement && groupCbElement.checked)
		{
			xml += "<rp_group_id>" + groupId + "</rp_group_id>";			
		} 
	}
	xml += "</rp_groups>";
	xml += "</AjaxReportParameterRequest>";
	return xml;	
}

function saveGroupForReport (reportId)
{
	var xml = generateSaveGroupForReportRequestXML(reportId);

	var sqlReportURL = ajaxurl + "/UpdateGroupsForReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleSaveGroupForReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);	
}

function handleSaveGroupForReportResponse ()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0)
				{
					alert('Action failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{
						window.location.replace('/content/pl/sql_report/manage_report/index.html');
					}
					else
					{
						alert('Action failed!');
					}					
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}				
	}		
}

function generateSaveReportForGroupRequestXML (groupId)
{
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_group_id>" + groupId + "</rp_group_id>";
	xml += "<rp_reports>";
	for(var i=0; i<reportInGroupArray.length; i++) 
	{
		var reportInGroupId = reportInGroupArray[i];
		var reportCbElement = document.getElementById(reportInGroupId);
		if (reportCbElement && reportCbElement.checked)
		{
			xml += "<rp_id>" + reportInGroupId + "</rp_id>";			
		} 
	}
	xml += "</rp_reports>";
	xml += "</AjaxReportParameterRequest>";
	return xml;	
}

function saveReportForGroup (reportId)
{
	var xml = generateSaveReportForGroupRequestXML(reportId);

	var sqlReportURL = ajaxurl + "/UpdateReportsForGroupHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleSaveReportForGroupResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);	
}

function handleSaveReportForGroupResponse ()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0)
				{
					alert('Action failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{
						window.location.replace('/content/pl/sql_report/manage_report/manage_report_group.html');
					}
					else
					{
						alert('Action failed!');
					}					
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}				
	}		
}

function popUpQueryHelp ()
{
	var datasource = '';
	if (document.getElementById('report_datasource'))
	{
		datasource = document.getElementById('report_datasource').value;
	}
	
	if (datasource == 'webstats')
	{
		window.open('http://dev.mysql.com/doc/refman/5.0/en/select.html', 'MySQL_Help');
	}
	else if (datasource == 'AQUILLA')
	{
		window.open('http://68.142.116.70/docs/cd/B19306_01/server.102/b14200/statements_10002.htm#SQLRF01702', 'Oracle_Help');
	}
	 //Use the <input type='file'...> object to get a filename without showing the object.

}

function popUpQueryDesigner()
{
	var datasource = '';
	if (document.getElementById('report_datasource'))
	{
		datasource = document.getElementById('report_datasource').value;
	}
	var designerWindow = null;
	
	if (datasource == 'webstats')
	{
		designerWindow = window.open('/content/pl/sql_report/manage_report/query_designer.jsp',
'select_user','height=800,width=1000,scrollbars=1');
	}
	else if (datasource == 'AQUILLA')
	{
		designerWindow = window.open('/content/pl/sql_report/manage_report/query_designer.jsp',
'select_user','height=800,width=1000,scrollbars=1');
	}	
	
    designerWindow.moveTo(100, 100);	
}

function uploadQuery()
{
	var fileName = document.getElementById('query_file').value;
	if (fileName && fileName.length > 4 && 
		(fileName.indexOf('.txt') == fileName.length - 4 || fileName.indexOf('.sql') == fileName.length - 4))
	{	
		micoxUpload(document.forms['add_section_form'], ajaxurl + '/QueryUploadHandler', 'section_query', 'Loading...','Error in upload');
	}
	else
	{
		alert('Please select a sql or txt file!');
		return false;
	}
}

/******
* micoxUpload - Submit a form to hidden iframe. Can be used to upload
* Use but dont remove my name. Creative Commons.
* Parametros:
* form - the form to submit or the ID
* url_action - url to submit the form. like action parameter of forms.
* id_element - element that will receive return of upload.
* html_show_loading - Text (or image) that will be show while loading
* html_error_http - Text (or image) that will be show if HTTP error.
*******/

/* standard small functions */
function $m(quem){
 	return document.getElementById(quem)
}
function remove(quem){
 	quem.parentNode.removeChild(quem);
}
function addEvent(obj, evType, fn){
 // elcio.com.br/crossbrowser
    if (obj.addEventListener)
        obj.addEventListener(evType, fn, true)
    if (obj.attachEvent)
        obj.attachEvent("on"+evType, fn)
}
function removeEvent( obj, type, fn ) {
  	if ( obj.detachEvent ) {
    	obj.detachEvent( 'on'+type, fn );
  	} else {
    	obj.removeEventListener( type, fn, false ); }
} 
/* THE UPLOAD FUNCTION */
function micoxUpload(form,url_action,id_element,html_show_loading,html_error_http)
{
 	//testing if 'form' is a html object or a id string
 	form = typeof(form)=="string"?$m(form):form;
 
 	var erro="";
 	if(form==null || typeof(form)=="undefined")
 	{ 
 		erro += "The form of 1st parameter does not exists.\n";
 	}
 	else if(form.nodeName.toLowerCase()!="form")
 	{ 
 		erro += "The form of 1st parameter its not a form.\n";
 	}
 	if($m(id_element)==null)
 	{ 
 		erro += "The element of 3rd parameter does not exists.\n";
 	}
 	if(erro.length>0) 
 	{
  		alert("Error in call micoxUpload:\n" + erro);
  		return;
 	}

	 //creating the iframe
	 var iframe = document.createElement("iframe");
	 iframe.setAttribute("id","micox-temp");
	 iframe.setAttribute("name","micox-temp");
	 iframe.setAttribute("width","0");
	 iframe.setAttribute("height","0");
	 iframe.setAttribute("border","0");
	 iframe.setAttribute("style","width: 0; height: 0; border: none;");
	 
	 //add to document
	 form.parentNode.appendChild(iframe);
	 window.frames['micox-temp'].name="micox-temp"; //ie sucks
 
 	//add event
 	var updateQuery = function() 
 	{ 
	   	removeEvent( $m('micox-temp'),"load", updateQuery);
	   	var cross = "javascript: ";
	   	cross += "window.parent.extractParamFromQuery(document.getElementsByTagName('PRE')[0].firstChild.nodeValue, '" + id_element + "'); ";

	   	//cross += "alert(document.body.innerHTML); ";
	   	$m(id_element).innerHTML = html_error_http;
	   	$m('micox-temp').src = cross;
	   //del the iframe
	   	setTimeout(function(){remove($m('micox-temp'))}, 1250);
    }
  	
	 addEvent( $m('micox-temp'),"load", updateQuery)
	 
	 //properties of form
	 form.setAttribute("target","micox-temp");
	 form.setAttribute("action",url_action);
	 form.setAttribute("method","post");
	 form.setAttribute("enctype","multipart/form-data");
	 form.setAttribute("encoding","multipart/form-data");
	 //submit
	 form.submit();
 
	 //while loading
	 if(html_show_loading.length > 0)
	 {
  		$m(id_element).innerHTML = html_show_loading;
 	 }
}

function formatUploadedQueryToNodeValue (uploadedQuery)
{
	var queryHtmlValue = uploadedQuery;
	if (uploadedQuery != null)
	{
		queryHtmlValue = queryHtmlValue.replace(/(\&amp;)/g, '&').replace(/(\&lt;)/g, '<').replace(/(\&gt;)/g, '>');
	}
	return queryHtmlValue;
}

function InputReportDateValidate()
{
	var isCopyDateEle = document.getElementById('copyDateToSection');
	if (isCopyDateEle)
	{
		if (isCopyDateEle.checked)
		{
			if ((document.getElementById('report_start_date') && document.getElementById('report_start_date').value.length == 0) ||
				(document.getElementById('report_end_date') && document.getElementById('report_end_date').value.length == 0))
			{
				alert ('Please select both report start and end date first!');	
				return false;							
			}	
			isCopyDateToSection = true;
		}
		else
		{
			isCopyDateToSection = false;
		}
	}
	
	return true;
}

function checkParamNameIsStartDate (paramName)
{
	if (paramName && paramName.length > 0)
	{
		var compoundParamNamed = trim(paramName.toLowerCase()).replace(/[ ]+/g, '_');
		for(var i=0; i<STARTDATE_PARAMNAME_ARRAY.length; i++) 
		{
			var startdateTemplate = STARTDATE_PARAMNAME_ARRAY[i];			
			if (compoundParamNamed == startdateTemplate)
			{
				return true;
		}
		}
	}
		
	return false;
}

function checkParamNameIsEndDate (paramName)
{
	if (paramName && paramName.length > 0)
	{
		for(var i=0; i<ENDDATE_PARAMNAME_ARRAY.length; i++) 
		{
			var compoundParamNamed = trim(paramName.toLowerCase()).replace(/[ ]+/g, '_');
			var enddateTemplate = ENDDATE_PARAMNAME_ARRAY[i];
			if (compoundParamNamed == enddateTemplate)
			{
				return true;
			}
		}
	}
		
	return false;
}

function generateExtractParamRequestXML(datasource, query)
{
	query = formatToSendableString(query);
	var xml = "";
	xml += "<AjaxReportParameterRequest>";	
	xml += "<rp_datasource>" + datasource + "</rp_datasource>";	
	xml += "<rp_section_query><![CDATA[" + query + "]]></rp_section_query>";		
	xml += "</AjaxReportParameterRequest>";
	return xml;
}

function extractParamFromQuery (query, updateElementId)
{
	//alert('extractParamFromQuery: ' + query);
	//var newParams = new Array();
//	sectionParams = new Array();
//	var formattedQuery = query;
//	
//	if (query != null)
//	{
//		// extract param set 1 with :Param_name
//		var paramSet = query.match(/([<]{1}|[<]{1}[=]{1}|[>]{1}|[>]{1}[=]{1}|[=]{1}|[<]{1}[>]{1}|[!]{1}[=]{1}|[Bb]{1}[Ee]{1}[Tt]{1}[Ww]{1}[Ee]{1}[Ee]{1}[Nn]{1}[ ]+|[Aa]{1}[Nn]{1}[Dd]{1}[ ]+|[Ll]{1}[Ii]{1}[Kk]{1}[Ee]{1}[ ]+|[Ii]{1}[Nn]{1}[ ]*[\(]?[ ]*)[ ]*(([ ]*[:]{1}[\w]+)|([']{1}[\&]{1}[\w]+[']{1})|([@]{1}[vV]{1}[aA]{1}[rR]{1}[iI]{1}[aA]{1}[bB]{1}[lL]{1}[eE]{1}[(]{1}[']{1}[\w]+[']{1}[)]{1}))/g);
//		var paramNameEmpty = false;
//		var paramNameHasSpace = false;
//		//alert(paramSet);
//		if (paramSet != null)
//		{
//			for(var i=0; i<paramSet.length; i++) 
//			{
//				var paramName = paramSet[i];
//				var paramNameExtracted = '';
//				var paramTypeExtracted = 'PARAM_TYPE_STRING'; // default				
//				if (paramName && paramName.indexOf(':') > -1)
//				{
//					paramNameExtracted = paramName.substring(paramName.indexOf(':') + 1);
//					if (checkParamNameIsStartDate(paramNameExtracted) || checkParamNameIsEndDate(paramNameExtracted))
//					{
//						paramTypeExtracted = 'PARAM_TYPE_DATE';
//					}
//				}
//				else if (paramName && paramName.indexOf('\'&') > -1)
//				{
//					paramNameExtracted = paramName.substring(paramName.indexOf('\'&') + 2, paramName.length-1);
//					if (checkParamNameIsStartDate(paramNameExtracted) || checkParamNameIsEndDate(paramNameExtracted))
//					{
//						paramTypeExtracted = 'PARAM_TYPE_DATE';
//					}	
//					// replace white space of param name by underscore _	
//					var replacedParamNameExtracted = paramNameExtracted.replace(/[ ]+/g, '_');	
//					var updateParamName = paramName.replace(paramNameExtracted, replacedParamNameExtracted);
//					formattedQuery = formattedQuery.replace(paramName, updateParamName);
//				}
//				else if (paramName && paramName.indexOf('@variable(\'') > -1)
//				{
//					paramNameExtracted = paramName.substring(paramName.indexOf('@variable(\'') + 11, paramName.length-2);;
//					if (checkParamNameIsStartDate(paramNameExtracted) || checkParamNameIsEndDate(paramNameExtracted))
//					{
//						paramTypeExtracted = 'PARAM_TYPE_DATE';
//					}	
//					// replace white space of param name by underscore _	
//					var replacedParamNameExtracted = paramNameExtracted.replace(/[ ]+/g, '_');	
//					var updateParamName = paramName.replace(paramNameExtracted, replacedParamNameExtracted);
//					formattedQuery = formattedQuery.replace(paramName, updateParamName);																				
//				}
//					
//				if (trim(paramNameExtracted).length > 0)
//				{
//					//sectionParams.push(new Array(paramNameExtracted.replace(/[ ]+/g, '_'), paramTypeExtracted, '', '', ''));
//					addNewParam(sectionParams, paramNameExtracted, paramTypeExtracted);
//					if (hasWhiteSpace(paramNameExtracted))
//					{
//						paramNameHasSpace = true;
//					}					
//				}
//				else
//				{
//					paramNameEmpty = true;
//				}
//			}	
//		} 
//		
//		if (paramNameEmpty)
//		{
//			alert('Some of parameters in your query contains only white space characters and make your query invalid.' +
//				  '\nPlease use non white space parameter name.')
//		}
//		if (paramNameHasSpace)
//		{
//			alert('Some of parameters in your query contains white spaces. It is required that white spaces are converted to underscore(\'_\') to make the parameter to be a word!')
//		}
//	}

	
	
//	updateParamListHtml();
//	var updateElement = document.getElementById(updateElementId);
//	if (updateElement)
//	{
//		updateElement.value = formattedQuery;
//	}

	var datasource = '';
	if (document.getElementById('report_datasource'))
	{
		datasource = document.getElementById('report_datasource').value;
	}

	var xml = generateExtractParamRequestXML(datasource, query);
	
	//alert(xml);
	var sqlReportURL = ajaxurl + "/ExtractQueryParamsHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleExtractQueryParamsResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);
	
}

function handleExtractQueryParamsResponse(){
	
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					alert('Extract query parameter error: ' + errors[0].firstChild.nodeValue);										
				}
				else
				{
					var paramNodes = response.getElementsByTagName("Param");
					for(var i=0; i<paramNodes.length; i++) 
					{
						var paramName = paramNodes[i].firstChild.nodeValue;
						
						if (trim(paramName).length == 0)
						{
							alert('Query parameter not valid (EMPTY PARAMETER): ' + paramName);
							return false;
						}
						else if (hasWhiteSpace(paramName))
						{
							alert('Query parameter not valid (CONTAIN SPACE): ' + paramName);
							return false;
						}
						else
						{
							var paramType = 'PARAM_TYPE_STRING'; // default
							
							if (checkParamNameIsStartDate(paramName) || checkParamNameIsEndDate(paramName))
							{
								paramType = 'PARAM_TYPE_DATE';
							}
							addNewParam(sectionParams, paramName, paramType);
						}						
					}
					
					updateParamListHtml();
				}	
			}
			catch(err){
				alert("XML Parser error." + err);						
			}						
		}		
	}
}

function formatQueryToStandardFormat(query)
{
	var formattedQuery = query;
	
	if (query != null)
	{
		// extract param set 1 with :Param_name
		var paramSet = query.match(/[']{1}[\&]{1}[\w]+[']{1}/g);
		var paramNameExtractedArray = new Array();
		if (paramSet != null)
		{
			for(var i=0; i<paramSet.length; i++) 
			{
				var paramName = paramSet[i];
				var paramNameExtracted = paramName.substring(2, paramName.length-1);
				paramNameExtractedArray.push(paramNameExtracted);
				formattedQuery = formattedQuery.replace(paramSet[i], ':' + paramNameExtracted);			
			}	
		}
	}
	
	return formattedQuery;	
}


function generateSaveUserForReportGroupRequestXML (groupId)
{
	var xml = "";
	xml += "<AjaxReportParameterRequest>";
	xml += "<rp_group_id>" + groupId + "</rp_group_id>";	// if exist, update only the users of this group
	xml += "<rp_group_users>";
	for(var i=0; i<userInGroupArray.length; i++) 
	{
		var userGroupId = userInGroupArray[i];
		var userCbElement = document.getElementById(userGroupId);
		if (userCbElement && userCbElement.checked)
		{
			xml += "<rp_group_user_id>" + userGroupId + "</rp_group_user_id>";			
		} 
	}
	xml += "</rp_group_users>";
	xml += "</AjaxReportParameterRequest>";
	return xml;	
}

function saveUserForReportGroup (groupId)
{
	var xml = generateSaveUserForReportGroupRequestXML(groupId);

	var sqlReportURL = ajaxurl + "/UpdateUserInReportGroupHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleSaveUserForReportGroupResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);	
}

function handleSaveUserForReportGroupResponse ()
{
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0)
				{
					alert('Action failed!');
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("RpActionResult")[0].firstChild.nodeValue;
					if (rpActionResult == 'true')
					{						
						alert('Access settings is updated!');
					}
					else
					{
						alert('Action failed!');
					}					
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}				
	}		
}

/************** Scheduling **********************/
function generateScheduleSelectedReportXML(action)
{
	if (reportGroupId == null || reportGroupId.length == 0 || 
		!selectedRunningReportArray || selectedRunningReportArray == null)
	{
		alert('You need to select at least one report to run.');
		return null;
	} 


	var reportXml = "";
	reportXml += "<AjaxReportParameterRequest>";
	reportXml += "<rp_action>" + action + "</rp_action>";	
	reportXml += "<rp_group_id>" + reportGroupId + "</rp_group_id>";		
	reportXml += "<rp_runner>" + reportRunnerId + "</rp_runner>";	
	reportXml += "<rp_output>" + reportOutputFormat + "</rp_output>";	
	reportXml += "<rp_startdate>" + reportStartDate + "</rp_startdate>";
	reportXml += "<rp_enddate>" + reportEndDate + "</rp_enddate>";
	reportXml += "<rp_copy_date>" + isCopyDateToSection + "</rp_copy_date>";		
	reportXml += "<rp_crond_exp>" + crondExpresson + "</rp_crond_exp>";
	reportXml += "<rp_dest_emails>" + destEmails + "</rp_dest_emails>";	
	reportXml += "<rp_reports>";
	for(var i=0; i<selectedRunningReportArray.length; i++) 
	{
		var reportId = selectedRunningReportArray[i][0];
		var sections = selectedRunningReportArray[i][2];
		reportXml += "<rp_report>";
		reportXml += "<rp_id>" + reportId + "</rp_id>"
		reportXml += "<rp_sections>";
		for(var j=0; j<sections.length; j++) 
		{
			reportXml += "<rp_section>";
			var section = sections[j];
			var sectionId = section[0];
			reportXml += "<rp_section_id>" + sections[j][0] + "</rp_section_id>";			
			if (section.length > 2)
			{
				reportXml += "<rp_section_params>";
				var sectionParams = section[2];
				for(var k=0; k<sectionParams.length; k++) 
				{
					reportXml += "<rp_section_param>";		
					reportXml += "<name>" + sectionParams[k][0] + "</name>";									
					reportXml += "<value>" + sectionParams[k][1] + "</value>";	
					reportXml += "<type>" + sectionParams[k][2] + "</type>";						
					reportXml += "</rp_section_param>";										
				}
				reportXml += "</rp_section_params>";				
			} 
			reportXml += "</rp_section>";			
		}
		reportXml += "</rp_sections>";
		reportXml += "</rp_report>";
	}
	reportXml += "</rp_reports>";
	reportXml += "</AjaxReportParameterRequest>";
	
	return reportXml;
}

function scheduleSelectedReports(action)
{
	scheduleAction = action;
	if (action == 'ADD' || action == 'EDIT')
	{
		if (runningReportArray.length > 0)
		{
			var reportRunnerEle = document.getElementById('report_runner');
			if (reportRunnerEle)
			{
				reportRunner = reportRunnerEle.value;
			} 	
			
			var reportOutputSelect = document.getElementById('report_output_type');
			if (reportOutputSelect)
			{
				reportOutputFormat = reportOutputSelect.value;
			} 
			reportStartDate = getSelectedDateValue('report_start_date');
	//		if (reportStartDate == null || reportStartDate == '')
	//		{
	//			alert('Please select report start date');
	//			return false;
	//		}
			reportEndDate = getSelectedDateValue('report_end_date');
	//		if (reportEndDate == null || reportEndDate == '')
	//		{
	//			alert('Please select report end date');
	//			return false;
	//		}
			if (!CopyDateToSectionsScheduleValidate())
			{
				return false;
			}
			
			var condExpressionText = document.getElementById('crond_expression');
			if (condExpressionText)
			{
				crondExpresson = condExpressionText.value;
			}		
			if (crondExpresson == null || crondExpresson == '')
			{
				alert('Please specify a crond job expression to run the reports.');
				return false;
			}
			
			var destEmailArea = document.getElementById('destination_emails');
			if (destEmailArea)
			{
				destEmails = destEmailArea.value;
			}
			if (destEmails == null || destEmails == '')
			{
				alert('Please specify at least one email address to send the reports to');
				return false;
			}
			destEmails = destEmails.replace(/[\s]+/g,';');
			
			// render the input parameter form
			renderParamsInputFormSchedule();
			
			if (selectedRunningReportArray == null || !selectedRunningReportArray || selectedRunningReportArray.length == 0)
			{
				alert('Please select a report!');
				return false;
			} 
			else
			{
				if (!hasParamInput)
				{
					sendScheduleReportRequest();
				}
			}
		}
	}
	else if (action == 'DELETE')
	{
		var confirmRemove = confirm('Are you sure to remove this scheduler?');
		if (confirmRemove)
		{
			sendScheduleReportRequest();
		}
	}
	else if (action == 'ACTIVATE' || action == 'DEACTIVATE')
	{
		sendScheduleReportRequest();
	}
}

function renderParamsInputFormSchedule ()
{
	selectedRunningReportArray = new Array();
	var accessReportForm = document.getElementById('access_report_form');
	if (!accessReportForm) return false;
	
	lastContentHTML = accessReportForm.innerHTML;
	
	var updateContentHTML = '<h1>Input report parameter values</h1>';
	updateContentHTML += '<p>Based on the parameters of the queries in the reports the form bellow is rendered which contains all input parameters to run the reports you have requested. ' +
			'Value of parameters is initialized with its default value.</p>';	
	updateContentHTML += '<p>It is required that you must complete the form then click \'Ok\' button to supply values to those paramters.</p>';	
	updateContentHTML += '<p>To go back to schedule report page click \'Back\' button.</p>';		
	updateContentHTML += '<div class="report_list">';
	
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		var reportTitleStored = runningReportArray[i][1];

		var reportTitleDrawn = false;
		var selectedSections = new Array();
		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb && subCb.checked) 
			{
				selectedSections.push(sections[j]);
				if (sections[j].length > 2)	// has params input contain at section[j][2]
				{				
					if (!reportTitleDrawn)
					{
						updateContentHTML += '<div class="report_param_input">';
						updateContentHTML += '<h2>' + reportTitleStored + '</h2>';
					}
					reportTitleDrawn = true;						
										
					var sectionTitleStored = sections[j][1];
					updateContentHTML += '<h3>' + sectionTitleStored + '</h3>';	
					var selectedParams = sections[j][2];
					for(var k=0; k<selectedParams.length; k++) 
					{
						var selectedParam = selectedParams[k];
						var paramValueEleId = reportIdStored + '_' + sections[j][0] + '_' + selectedParam[0];
						var paramValue = selectedParam[1];

						if (selectedParam[2] == '3')
						{
							if (isCopyDateToSection)
							{
								if (checkParamNameIsStartDate(selectedParam[0]))
								{
									paramValue = reportStartDate;
								} 
								else if (checkParamNameIsEndDate(selectedParam[0]))
								{
									paramValue = reportEndDate;
								}
							}							
							updateContentHTML = updateContentHTML + 
							'<div class="login_component_holder">' +
							'<div class="login_label">' + selectedParam[0] + '</div>' + 
							'<div class="login_value"><input id="' + paramValueEleId + '" type="text" ' +
							'value="' + paramValue + '" onclick="return showCalendar(\'' + paramValueEleId + '\', \'%e-%b-%y\');"/> </div>' +
							'</div>';							
						}
						else
						{
							updateContentHTML = updateContentHTML + 
							'<div class="login_component_holder">' +
							'<div class="login_label">' + selectedParam[0] + '</div>' + 
							'<div class="login_value"><input id="' + paramValueEleId + '" type="text" ' +
							'value="' + paramValue + '"/> </div>' +
							'</div>';								
						}
					}						
				}
			}
		}
		
		if (reportTitleDrawn)
		{			
			updateContentHTML += '</div>';
		}
		
		if (selectedSections.length > 0)
		{
			selectedRunningReportArray.push(new Array (reportIdStored, reportTitleStored, selectedSections));
		}  
	}
	
	updateContentHTML += '</div>';
	updateContentHTML = updateContentHTML +
			'<div class="report_component_holder">' +
			'<div style="TEXT-ALIGN: right">' +
			'<input class="goButton" id="_run_report_confirm" type="button" onclick="updateSelectedReportParamValueSchedule();" value="Ok" />&nbsp;' + 
			'<input class="goButton" id="_back_to_run_report" type="button" onclick="backToForm();" value="Back" />&nbsp;' +
			'</div>';
		
	accessReportForm.innerHTML = updateContentHTML;	
	
}

function updateSelectedReportParamValueSchedule ()
{
	var paramValidated = true;
	if (!selectedRunningReportArray || selectedRunningReportArray == null)
	{
		return false;
	} 

	for(var i=0; i<selectedRunningReportArray.length; i++) 
	{
		var reportId = selectedRunningReportArray[i][0];
		var sections = selectedRunningReportArray[i][2];
		for(var j=0; j<sections.length; j++) 
		{
			var section = sections[j];
			var sectionId = section[0];		
			if (section.length > 2)
			{
				var sectionParams = section[2];
				for(var k=0; k<sectionParams.length; k++) 
				{
					var paramName = sectionParams[k][0];
					var paramType = sectionParams[k][2]
					var paramValueEleId = reportId + '_' + sectionId + '_' + paramName;
					var paramValueEle = document.getElementById(paramValueEleId);
					if (paramValueEle)
					{
						sectionParams[k][1] = paramValueEle.value;
					}
					if (paramType == '2' && !validateParamTypeNumber (paramValueEle.value))
					{
						alert ('Parameter ' + paramName + ' is typed as number. Please in put a valid number!');
						paramValidated = false;
					}
				}				
			} 		
		}
	}
	
	if (paramValidated)
	{
		sendScheduleReportRequest();
	}
	else
	{
		return false;
	}
}

function sendScheduleReportRequest()
{		
	var xml = generateScheduleSelectedReportXML(scheduleAction);
	//alert(xml);
	
	var sqlReportURL = ajaxurl + "/ScheduleSQLReportHandler";
	var params = "xml=" + xml;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleScheduleReportResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);		
	//reportWhizyVisi(true);
}

function handleScheduleReportResponse(){

   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			var response = createResponseXML(sqlReportHttp.responseText);					
			//alert(sqlReportHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					alert('schedule report failed!');
					reportWhizyVisi(false);
					return false;
				}
				else
				{
					var rpActionResult = response.getElementsByTagName("rp_actionResult")[0].firstChild.nodeValue;
					var rpReportOutput = response.getElementsByTagName("rp_reportOutput")[0].firstChild.nodeValue;

					if (rpActionResult == 'true')
					{
						window.location.replace('/content/pl/sql_report/access_reports.html');
					}
					else
					{
						alert('schedule reports failed:\n' + rpReportOutput);
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}
			
			//reportWhizyVisi(false);						
		}
	}

}

function getSelectedDateValue (elementName)
{
	var dateRadios = document.getElementsByName(elementName);
	var rad_val = null;
	for (var i=0; i < dateRadios.length; i++)
   	{
   		if (dateRadios[i].checked)
      	{
      		rad_val = dateRadios[i].value;
      		break;
      	}
   	}
   	
   	if (rad_val == 'FIXED_DATE')
   	{
   		var rad_val_ext = document.getElementById(elementName + '_value');
   		if (rad_val_ext)
   		{
   			rad_val = rad_val_ext.value;
   		}
   	}
   	
   	return rad_val;	
}

function CopyDateToSectionsScheduleValidate()
{
	var isCopyDateEle = document.getElementById('copyDateToSection');
	if (isCopyDateEle)
	{
		if (isCopyDateEle.checked)
		{
			var startdate_val = getSelectedDateValue ('report_start_date');
			var enddate_val = getSelectedDateValue ('report_end_date');	
			if ((startdate_val == null || startdate_val.length == 0) ||
				(enddate_val == null || enddate_val.length == 0))
			{
				alert ('Please specify both report start and end date values first!');
				return false;							
			}	
			isCopyDateToSection = true;
		}
		else
		{
			isCopyDateToSection = false;
		}
	} 
	
	return true;
}

function updateDateSelected (elementName, dateValue)
{
	
	var dateRadios = document.getElementsByName(elementName);
	var rad_val = null;
	for (var i=0; i < dateRadios.length; i++)
   	{
   		if (dateRadios[i].value == dateValue)
      	{
      		dateRadios[i].checked = 'checked';
      		return;
      	}
   	}
	
	// consider date value is fixed
	if (dateRadios.length > 0 && dateValue != null && dateValue.length > 0)
	{
		dateRadios[0].checked = 'checked';
		var rad_val_ext = document.getElementById(elementName + '_value');
   		if (rad_val_ext)
   		{   			
   			rad_val_ext.value = dateValue;
   			rad_val_ext.style.visibility = "visible";
   		}		
	}
}

function updateScheduleSelectedReports()
{
	for(var i=0; i<runningReportArray.length; i++) 
	{
		var reportIdStored = runningReportArray[i][0];
		var reportTitleStored = runningReportArray[i][1];

		// find sections belong to report
		var sections = runningReportArray[i][2];				
		for(var j=0; j<sections.length; j++) 
		{
			var subCb = document.getElementById('cb_' + reportIdStored + '_' + sections[j][0]);
			if (subCb) 
			{
				if (isSectionOfReportSelected(reportIdStored, sections[j][0]))
				{
					subCb.checked = true;
				}
				else
				{				
					subCb.checked = false;
				}
			}
		}
		
		UpdateSelectAllSections(reportIdStored);
	}
	
	UpdateSelectAllReports();	
}

function isSectionOfReportSelected(selected_reportId, selected_sectionId)
{
	for(var i=0; i<selectedRunningReportArray.length; i++) 
	{
		var reportIdStored = selectedRunningReportArray[i][0];
		if (reportIdStored == selected_reportId)
		{
			var selectedSections = selectedRunningReportArray[i][2];
			for(var j=0; j<selectedSections.length; j++) 
			{
				var sectionIdStored = selectedSections[j][0];
				if (sectionIdStored == selected_sectionId)
				{
					return true;
				}
			}
		}		
	}
	
	return false;
}

function removeScheduledInboxItem (itemId)
{	
	var sqlReportURL = ajaxurl + "/RemoveInboxItemHandler";
	var params = "id=" + itemId;
	
	sqlReportHttp = createXmlHttpRequestObject();

	sqlReportHttp.open("POST", sqlReportURL, true );

	sqlReportHttp.onreadystatechange = handleRemoveInboxIteamResponse;		
	
    sqlReportHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    sqlReportHttp.setRequestHeader("Content-length", params.length);
    sqlReportHttp.setRequestHeader("Connection", "close");		

	sqlReportHttp.send(params);		
	//reportWhizyVisi(true);	
}

function handleRemoveInboxIteamResponse()
{
	
   	if (sqlReportHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (sqlReportHttp.status == 200) {
			window.location.reload();
		}
	}
}

/*end*/
/Old function*/
function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}
// trim both end
function trim(str) {
    var trimmed = str.replace(/^\s+|\s+$/g, '') ;
    return trimmed;
}

function hasWhiteSpace (str)
{
	if (str != null)
	{
		if (str.match(/[\s]/g))
		{
			return true;
		}
	}
	
	return false;
}

function formatToSendableString(str)
{
	var newStr = str.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');
	
	return newStr;
}

//BachLe added on Feb-19-2009, fix REPORTING-58
function addNewParam(sectionParams, paramNameExtracted, paramTypeExtracted) {
	var isExisting = false;
	for (var i=0; i<sectionParams.length; i++) 
	{
		var sectionParam = sectionParams[i];
		var name = sectionParam[0];
		/*
		var type = sectionParam[1];
		if (type == paramTypeExtracted) {
			if (name.toUpperCase() == paramNameExtracted.replace(/[ ]+/g, '_').toUpperCase()) {			
			//if (name == paramNameExtracted.replace(/[ ]+/g, '_')) {
				isExisting = true;
				break;
			}
		}
		*/
		
		if (name.toUpperCase() == paramNameExtracted.replace(/[ ]+/g, '_').toUpperCase()) 
		{			
		//if (name == paramNameExtracted.replace(/[ ]+/g, '_')) {
			isExisting = true;
			break;
		}		
	}
	if (!isExisting) 
	{
		sectionParams.push(new Array(paramNameExtracted.replace(/[ ]+/g, '_').toUpperCase(), paramTypeExtracted, '', '', ''));
	}
	return;
}

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

function replaceTag(tag) {
    return tagsToReplace[tag] || tag;
}

function safe_tags_replace(str) {
    return str.replace(/[&<>]/g, replaceTag);
}