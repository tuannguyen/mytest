// JavaScript Document 
	var PaySlipTagArray= new Array();
	var startYear = "";		// year
	var startMonth ="";		// start month
	var beginYear = "";
	var runningYear = "";
	var endYear="";
        var startInitialized = false;
	var staticPaySlipURL = ajaxurl + "/PaySlipHandler?xml=";
	var PaySlipWorking=false;
	var PaySlipHttp= createXmlHttpRequestObject();
	var TaskyearTmp="[[Payslip: CurrentTaxYear]]";
	var YearHolder ="Tax year [[Payslip: CurrentTaxYear]]";
	var tmpTable="<table cellspacing='0' id='tblFiscalyears' cellpadding='0' id='PaymentHistory' class='datatable'><tbody><tr><th class='label_left'>date</th><th class='label_left'>status</th><th class='label_left'>gross</th><th class='label_left adjustments'>adjustments</th><th class='label_left'>tax</th><th class='label_left'>nett</th></tr><tr><td class='pvalue date'>[[Payslip: MayDate]]</td><td class='pvalue status'>[[Payslip: MayStatus]]</td><td class='pvalue number'>[[Payslip: MayGross]]</td><td class='pvalue number'>[[Payslip: MayAdjust]]</td><td class='pvalue number tax'>[[Payslip: MayTax]]</td><td class='value number total'>[[Payslip: MayNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: JunDate]]</td><td class='pvalue status'>[[Payslip: JunStatus]]</td><td class='pvalue number'>[[Payslip: JunGross]]</td><td class='pvalue number'>[[Payslip: JunAdjust]]</td><td class='pvalue number tax'>[[Payslip: JunTax]]</td><td class='value number total'>[[Payslip: JunNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: JulDate]]</td><td class='pvalue status'>[[Payslip: JulStatus]]</td><td class='pvalue number'>[[Payslip: JulGross]]</td><td class='pvalue number'>[[Payslip: JulAdjust]]</td><td class='pvalue number tax'>[[Payslip: JulTax]]</td><td class='value number total'>[[Payslip: JulNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: AugDate]]</td><td class='pvalue status'>[[Payslip: AugStatus]]</td><td class='pvalue number'>[[Payslip: AugGross]]</td><td class='pvalue number'>[[Payslip: AugAdjust]]</td><td class='pvalue number tax'>[[Payslip: AugTax]]</td><td class='value number total'>[[Payslip: AugNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: SepDate]]</td><td class='pvalue status'>[[Payslip: SepStatus]]</td><td class='pvalue number'>[[Payslip: SepGross]]</td><td class='pvalue number'>[[Payslip: SepAdjust]]</td><td class='pvalue number tax'>[[Payslip: SepTax]]</td><td class='value number total'>[[Payslip: SepNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: OctDate]]</td><td class='pvalue status'>[[Payslip: OctStatus]]</td><td class='pvalue number'>[[Payslip: OctGross]]</td><td class='pvalue number'>[[Payslip: OctAdjust]]</td><td class='pvalue number tax'>[[Payslip: OctTax]]</td><td class='value number total'>[[Payslip: OctNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: NovDate]]</td><td class='pvalue status'>[[Payslip: NovStatus]]</td><td class='pvalue number'>[[Payslip: NovGross]]</td><td class='pvalue number'>[[Payslip: NovAdjust]]</td><td class='pvalue number tax'>[[Payslip: NovTax]]</td><td class='value number total'>[[Payslip: NovNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: DecDate]]</td><td class='pvalue status'>[[Payslip: DecStatus]]</td><td class='pvalue number'>[[Payslip: DecGross]]</td><td class='pvalue number'>[[Payslip: DecAdjust]]</td><td class='pvalue number tax'>[[Payslip: DecTax]]</td><td class='value number total'>[[Payslip: DecNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: JanDate]]</td><td class='pvalue status'>[[Payslip: JanStatus]]</td><td class='pvalue number'>[[Payslip: JanGross]]</td><td class='pvalue number'>[[Payslip: JanAdjust]]</td><td class='pvalue number tax'>[[Payslip: JanTax]]</td><td class='value number total'>[[Payslip: JanNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: FebDate]]</td><td class='pvalue status'>[[Payslip: FebStatus]]</td><td class='pvalue number'>[[Payslip: FebGross]]</td><td class='pvalue number'>[[Payslip: FebAdjust]]</td><td class='pvalue number tax'>[[Payslip: FebTax]]</td><td class='value number total'>[[Payslip: FebNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: MarDate]]</td><td class='pvalue status'>[[Payslip: MarStatus]]</td><td class='pvalue number'>[[Payslip: MarGross]]</td><td class='pvalue number'>[[Payslip: MarAdjust]]</td><td class='pvalue number tax'>[[Payslip: MarTax]]</td><td class='value number total'>[[Payslip: MarNett]]</td></tr><tr><td class='pvalue date'>[[Payslip: AprDate]]</td><td class='pvalue status'>[[Payslip: AprStatus]]</td><td class='pvalue number'>[[Payslip: AprGross]]</td><td class='pvalue number'>[[Payslip: AprAdjust]]</td><td class='pvalue number tax'>[[Payslip: AprTax]]</td><td class='value number total'>[[Payslip: AprNett]]</td></tr><tr><td class='blank date'>&nbsp;</td><td class='blank'>&nbsp;</td><td class='value number total'>[[Payslip: TotGross]]</td><td class='value number total'>[[Payslip: TotAdjust]]</td><td class='value number tax total'>[[Payslip: TotTax]]</td><td class='value number total'>[[Payslip: TotNett]]</td></tr></tbody></table>";
	
	var isPreviousAble = false;
	var isNextAble = false;
	function GetYears(year)
	{	
		year = parseInt(year);
		var	date = new Date();
		var currentYear = date.getFullYear();
		var currMonth = date.getMonth() + 1;		
		if (beginYear == "" )
		{
			if (currMonth < 4)
			{
				year = currentYear - 1;
				runningYear = currentYear - 1;
				endYear = currentYear - 1;
			}
			else
			{
				year = currentYear;
				runningYear = currentYear;
				endYear = currentYear;								
			}
			isNextAble = false;
		}
 		
 		if (endYear != "" & beginYear != "")
 		{
			if ((runningYear + 1) > endYear)
			{
				isNextAble = false;
			}
			else
			{
				isNextAble = true;
			} 
			if ((runningYear - 1) < beginYear)
			{
				isPreviousAble = false;
			}
			else
			{
				isPreviousAble = true;
			}
 		}
                // Make 2 buttons modal
		document.getElementById("NextTaxYear").style.visibility="hidden";
		document.getElementById("PreviousTaxYear").style.visibility="hidden";
		
		var tmp1stTaskYear=document.getElementById("1stTaskYear");
		tmp1stTaskYear.innerHTML=TaskyearTmp;
		
		var tmp2ndTaskYear=document.getElementById("ThisYearHolder");
		tmp2ndTaskYear.innerHTML=YearHolder;
		
		var tblTableToReplace=document.getElementById("tblFiscalyears");
		if(document.createRange){
		        var range = document.createRange();
			range.selectNode(tblTableToReplace);
			tblTableToReplace.parentNode.replaceChild(range.createContextualFragment(tmpTable), tblTableToReplace);	
		} else if (typeof(tblTableToReplace.outerHTML)){
			tblTableToReplace.outerHTML=tmpTable;
		} else {
			tblTableToReplace.innerHTML = tmpTable;
		}
		ParsePayslipHtml(year);
	}
	function loadNextTaxTear()
	{
		//document.getElementById("PreviousTaxYear").style.visibility="visible";
		if ((runningYear + 1) > endYear)
		{
			return;
		}
		runningYear += 1;
		
		var tmp1stTaskYear=document.getElementById("1stTaskYear");
		tmp1stTaskYear.innerHTML=TaskyearTmp;
		var tmp2ndTaskYear=document.getElementById("ThisYearHolder");
		tmp2ndTaskYear.innerHTML=YearHolder;			
		var tblTableToReplace=document.getElementById("tblFiscalyears");
		
		if(document.createRange){
		        var range = document.createRange();
			range.selectNode(tblTableToReplace);
			tblTableToReplace.parentNode.replaceChild(range.createContextualFragment(tmpTable), tblTableToReplace);	
		} else if (typeof(tblTableToReplace.outerHTML)){
			tblTableToReplace.outerHTML=tmpTable;
		} else {
			tblTableToReplace.innerHTML = tmpTable;
		}
		GetYears(runningYear);
			
	
	}
	function loadPreviousTaxTear()
	{		
		if ((runningYear - 1) < beginYear)
		{
			return;
		}		
		runningYear -= 1;
		//alert("runningYear: " + runningYear);
		var tmp1stTaskYear=document.getElementById("1stTaskYear");
		tmp1stTaskYear.innerHTML=TaskyearTmp;
		var tmp2ndTaskYear=document.getElementById("ThisYearHolder");
		tmp2ndTaskYear.innerHTML=YearHolder;
		var tblTableToReplace=document.getElementById("tblFiscalyears");
		if(document.createRange){
		        var range = document.createRange();
			range.selectNode(tblTableToReplace);
			tblTableToReplace.parentNode.replaceChild(range.createContextualFragment(tmpTable), tblTableToReplace);	
		} else if (typeof(tblTableToReplace.outerHTML)){
			tblTableToReplace.outerHTML=tmpTable;
		} else {
			tblTableToReplace.innerHTML = tmpTable;
		}
		GetYears(runningYear);
	}
	
	function ParsePayslipHtml(year)
	{		


       if(!isIE){

            var rootNode = document.body;
		    PaySlipWalkIE(rootNode,year);
		    
		}else{

		    var rootNode = document.body;
		    PaySlipWalkIE(rootNode,year);
		    
		}

		if(PaySlipTagArray.length)
		{
			next = PaySlipTagArray[0];
		
			PaySlipTagArray.shift();
		
			GetPaySlipData(next[0], next[1], next[2]);
		}		
	}
	function PaySlipWalkIE(rootNode,year){
        
        if(rootNode == null){
            return;
        }
        
        var length = rootNode.childNodes.length;
		var i=0;
		    		    
		for(i=0; i<length; i++){
		    var child = rootNode.childNodes[i];
		    
		    if(child.nodeType == 1)//is element node
		    {
		        
		        PaySlipWalkIE(child,year);
		        
		    }else if(child.nodeType == 3)//is text node
		    {
		        ExtractPaySlipParameterIE(child,year);

		    }
        }    
    }
	function ExtractPaySlipParameterIE(child,year){
	    var parameter = child.nodeValue;
		parameter =noWhiteSpace(parameter);
		var startPos = 0;
		var stopPos = 0;		
		var startChars = "[[", startRegexp = "\\[\\[";
		var stopChars = "]]", stopRegexp = "\\]\\]";		
		startPos = parameter.indexOf(startChars);
		parameter = parameter.substring(parameter.indexOf(startChars)+2,parameter.indexOf(stopChars));
		if(startPos >= 0 ){
		
			if(parameter.indexOf("Payslip:")>=0)
			{	
				var parameter=parameter.substring("Payslip:".length,parameter.length);
				var updateField = PL_UPDATE_FIELD;
				updateField = updateField.replace(PL_PARAM,parameter);
								var i = 1;
					while(true){
					
						if(document.getElementById(updateField + i) == null){
							updateField += i;					
							break;
						}else{
							++i;
						}
					}
				var strBody = child.nodeValue;
					
				var strContentReplacement = scriptTemplate;		
					
				strContentReplacement = strContentReplacement.replace(PL_PARAM,updateField);
				strContentReplacement = strContentReplacement.replace(PL_PARAM,updateField);
				var regexpStr = startRegexp +"Payslip: *"+ parameter + stopRegexp;
				var regexp = new RegExp(regexpStr);
				strBody = strBody.replace(regexp, strContentReplacement);
				
				child.parentNode.innerHTML = strBody;
				
				//parameter = parameter.toLowerCase();
				
				//play spinning image
				visiWhizzy(updateField, true);	                        

				//add new request to array
				PaySlipTagArray.push(Array(parameter, updateField, year));
			}
		}
		
	}
	function GetPaySlipData(name , update_field, year){
		
		PaySlipWorking = true;
		
		//generate the xml content
		var xml = generatePaySlipXML(name, update_field, year);
		
		// Build the URL to connect to
		var url = staticPaySlipURL + escape(xml);   //goes to Java Servlet
		
		PaySlipHttp.open("POST", url);
		
		PaySlipHttp.onreadystatechange = handlePaySlipResponse;
		
		PaySlipHttp.send(null);
		
	}
	function generatePaySlipXML(name, update_field, year){
			var xml = "";
			xml += "<AjaxParameterRequest>\n";
		    xml += "	<Member>\n";
			xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
		    xml += "		<Refno>" + crefno + "</Refno>\n";
		    xml += "	</Member>\n";
			xml += "	<FiscalYear>" + year + "</FiscalYear>\n";
			xml += "	<RequestedTag>" + name + "</RequestedTag>\n";
		    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
			xml += "</AjaxParameterRequest>\n";
			return xml;
	}
	
	function handlePaySlipResponse(){

			if (PaySlipHttp.readyState == 4) {
								
				// Check that a successful server response was received
		      	if (PaySlipHttp.status == 200) {
		      			//alert(PaySlipHttp.responseText);
					var response = createResponseXML(PaySlipHttp.responseText);					

					try{
						// parse the XML to get the values from XML above
						var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
						var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
						if (!startInitialized)
						{
							startYear  = response.getElementsByTagName("StartYear")[0].firstChild.nodeValue;
							startMonth = response.getElementsByTagName("StartMonth")[0].firstChild.nodeValue;
						
							var	date = new Date();
							if(startYear == "null"){							
								startYear = date.getFullYear();
							}
						
							if (startMonth < 4)
							{
								beginYear = startYear - 1;
							}
							else 
							{
								beginYear = startYear;
							}
							if (beginYear < runningYear) isPreviousAble = true;
							startInitialized = true;
						}
						
						var fieldType;
						var value ;
						var error = response.getElementsByTagName("Error").length;
						
						
						if(error > 0){
							value = "<font color='red'>" + response.getElementsByTagName("Message")[0].firstChild.nodeValue + "</font>";
							fieldType = "";
						}
						else{
							if(response.getElementsByTagName("Value")[0].childNodes.length > 0){							
								value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
								var hiphenPos = value.indexOf('-'); 
								if (hiphenPos > 0)
								{
									value = value.substring(0, hiphenPos)+ '<span>-</span>' + value.substring(hiphenPos+1);
								}
							}
							else{
								value = "&nbsp;";
							}
							
							if(response.getElementsByTagName("Type")[0].childNodes.length > 0){
								var tlength = response.getElementsByTagName("Type")[0].childNodes.length;
								var i = 0;
								for (i=0; i<tlength; i++){
									if(response.getElementsByTagName("Type")[0].childNodes[i].nodeType == 1)
									fieldType = response.getElementsByTagName("Type")[0].childNodes[i].nodeName;
								}
							}

						}
						
						if(PaySlipTagArray.length) { // TEST
							// Pop the top element off the array.
							var next = PaySlipTagArray[0];
							PaySlipTagArray.shift();
		            
							// set this as late as possible to avoid conflict
							PaySlipWorking = false;
							GetPaySlipData(next[0], next[1], next[2]);
						} else {
							// nothing in the queue so signal complete.
							PaySlipWorking = false;
							updateButtons();
						}						

						populateFieldValue(elementToUpdate, value, fieldType);

					}
					catch(err){
						//hide spinning image
						visiWhizzy(elementToUpdate, false);
					}
					
					//hide spinning image
					visiWhizzy(elementToUpdate, false);
					document.getElementById(elementToUpdate+'_spinning').style.display='none';
					
				
				}else {
				
			        // An HTTP problem has occurred
					//hide spinning image
					visiWhizzy(elementToUpdate, false);
					
			    }
				
			}
	}
	
	function updateButtons()
	{
		if (!isNextAble) 
			document.getElementById("NextTaxYear").style.visibility="hidden";
		else
			document.getElementById("NextTaxYear").style.visibility="visible";
			
		if (!isPreviousAble) 
			document.getElementById("PreviousTaxYear").style.visibility="hidden";
		else
			document.getElementById("PreviousTaxYear").style.visibility="visible";			
		
	}