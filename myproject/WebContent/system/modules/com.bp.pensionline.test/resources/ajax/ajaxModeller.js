var modellerHttp = null;
var isDebugModeller = false;

var npas   = new Array("Npa","CINN91","NpaCashLumpSumCurrency","NpaPensionWithChosenCash","NpaSpousesPension", "ReducedPension", "MaximumCashLumpSumExact", "PensionWithMaximumCash");
var tagids = new Array("modelled_nra","modelled_cont_opt","modelled_cash","modelled_pension","modelled_spouses_pension", "pension_with_chosen_cash", "maximum_cash_lump_sum", "pension_with_max_cash" );

var arMemberTagArr = new Array();
var arConditionalTagArr = new Array();

var nraSlider;
var cashSlider;

function initModellers()
{
	nraSlider = new slider('slider_co_nra', A_INIT, A_TPL);
	cashSlider = new slider('slider_co_cash', A_INIT2, A_TPL);
}

var cashLumpSumErrCount = 0;
var idCashLumpSumTimeOut;
var httpCashLumpSumRequest = createXmlHttpRequestObject();


function getCashLumpSum(){
	
	var xml = generateXML('NpaMaximumCashLumpSum', '');
	//alert(xml);
	// Build the URL to connect to
	var url = '/content/GetParameterHandler?xml=' + xml;   //goes to Java Servlet
	
	httpCashLumpSumRequest.open("POST", url);
	
	httpCashLumpSumRequest.onreadystatechange = handleGetCashLumpSumResponse;
	
	httpCashLumpSumRequest.send(null);
}

function handleGetCashLumpSumResponse()
{
	if (httpCashLumpSumRequest.readyState == 4) {
		
		// Check that a successful server response was received
      	if (httpCashLumpSumRequest.status == 200) {
			var response = createResponseXML(httpCashLumpSumRequest.responseText);
			//alert(httpCashLumpSumRequest.responseText);					
			try{         
				var error = response.getElementsByTagName("Error").length;
				var value = "10000";
				if(error > 0){
					if (cashLumpSumErrCount < 5)
					{
						idCashLumpSumTimeOut = setTimeout('getCashLumpSum()', 5000);
						cashLumpSumErrCount++;
					}
					else
					{
						cashSlider = new slider('slider_co_cash', A_INIT2, A_TPL);
						clearTimeout(idCashLumpSumTimeOut);
					}
				}
				else{
        			if(response.getElementsByTagName("Value")[0].childNodes.length > 0)
        			{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;

						if (value == "null") value = "10000";

						A_INIT2 = {
				                's_name': 'input_co_cash',
				                'n_minValue' : 0,
				                'n_maxValue' : value,      
				                'n_value' : 0,
				                'n_step' : 1000
				        }

						cashSlider = new slider('slider_co_cash', A_INIT2, A_TPL);
						clearTimeout(idCashLumpSumTimeOut);
                    }
				} 
                                      
			}
			catch(err){
				alert("XML Parser error." );			
			}			
		}else {		
			//hide spinning image
			//visiWhizzy(elementToUpdate, false);
	    }                            
	}
}

function storeParsedTags(){
	for(var i=0; i<request_array.length; i++){
		arMemberTagArr.push(request_array[i]);
	}	
	for(var i=0; i<conditionTagArray.length; i++){
		arConditionalTagArr.push(conditionTagArray[i]);
	}		
}

function useStoredTags(){
	for(var i=0; i<arMemberTagArr.length; i++){
		request_array.push(arMemberTagArr[i]);
	}	
	for(var i=0; i<arConditionalTagArr.length; i++){
		conditionTagArray.push(arConditionalTagArr[i]);
	}
}

function getTagData(tagName){
	try{
		var xml = generateXML(tagName, "Sample");

		var sUrl= ajaxurl + "/GetParameterHandler?xml="+xml;

		var updateHttp = createXmlHttpRequestObject();

		updateHttp .open("POST", sUrl, false);
	
		var tagValue = 0;

		updateHttp.onreadystatechange = function () {
	  		if (updateHttp .readyState == 4) {
				if (updateHttp .status == 200) {
					var response = updateHttp.responseXML;
					var values = response.getElementsByTagName("Value");
					if(values.length > 0 && values[0].childNodes.length > 0)
					{
						tagValue = values[0].firstChild.nodeValue;
					}
				}
			}
		}

		updateHttp.send(null);
		return tagValue;	
	}catch(err){
		alert("getTagData : " + err.description );
	}
	
}

function updateTagData(tagName)
{
    var xml = generateXML(tagName, '');
    var modellerUrl = ajaxurl + "/GetParameterHandler?xml="+xml;

    modellerHttp = createXmlHttpRequestObject();

    modellerHttp.open("POST", modellerUrl );

    modellerHttp.onreadystatechange = handleUpdateTagData;
   
    modellerHttp.send (null);
}

function handleUpdateTagData()
{

	if (modellerHttp.readyState == 4) {                               
   
        // Check that a successful server response was received
        if (modellerHttp.status == 200) {
            //var response = modellerHttp.responseXML;
        	//alert(modellerHttp.responseText);
            var response = createResponseXML(modellerHttp.responseText);	
            
            try{            	
            	var tagNames = response.getElementsByTagName("RequestedTag");
    			var values = response.getElementsByTagName("Value");
    			
    			if(tagNames.length > 0 && tagNames[0].childNodes.length > 0 && values.length > 0 && values[0].childNodes.length > 0)
    			{
    				var tagName = tagNames[0].firstChild.nodeValue;
    				var tagValue = values[0].firstChild.nodeValue;
    				if (tagName == "CINN91")
    				{
    					var theSelect = document.getElementById("input_co_option");
    					if(theSelect)
    					{
    						var length = theSelect.options.length;
    						var i=0;
    						for(i=0; i<length ; i++)
    						{
    							var opt = theSelect.options[i];
    							
    							if(opt.value == tagValue)
    							{
    								opt.selected = 'selected';
    							}
    						}
    					}
    					
    					updateTagData("NpaVeraIndicator");
    				}
    				else if (tagName == "NpaVeraIndicator")
    				{
    					if(tagValue=="true")
    					{
    						document.getElementById("ar_vera").className = "";
    						document.getElementById("ar_vera").style.visibility = "visible";									
    					}
    					else
    					{
    						document.getElementById("ar_vera").className = "caveat";
    						document.getElementById("ar_vera").style.visibility = "hidden";									
    					}
    					
    					updateTagData("NpaOverfundIndicator");
    				}
    				else if (tagName == "NpaOverfundIndicator")
    				{
    					if(tagValue=="true")
    					{
    						
    						document.getElementById("ar_overfund").className = "";

    						document.getElementById("ar_overfund").style.visibility = "visible";									
    					}
    					else
    					{
    						document.getElementById("ar_overfund").className = "caveat";
    						document.getElementById("ar_overfund").style.visibility = "hidden";
    					}
    				}
    			}
            }
            catch(err)
            {
				alert(err);
            }

        }
               
    }   
}

function setContribOptionDefaultValue()
{
	//alert('setContribOptionDefaultValue');
	updateTagData("CINN91");
}

function generateXmlForModeller(ageValue, option, cashValue){   
    var xml = "";
    xml += "<AjaxParameterRequest>\n";
    xml += "    <Modeller>\n";
    xml += "        <RetirementAge>"+ ageValue +"</RetirementAge> \n";           
    xml += "        <ContributoryOption>"+ option +"</ContributoryOption>   \n";
    xml += "        <CashLumpSum>"+ cashValue +"</CashLumpSum> \n";               
    xml += "    </Modeller>\n";
    xml += "</AjaxParameterRequest>\n";
    return xml;
}
   

function DebugModeller(msg){
    if(isDebugModeller){
        alert(msg);
    }
}

function replaceTags(){
	var i = 0;
	for(i=0; i<=tagids.length; i++){

		var tmpId = PL_UPDATE_FIELD;
		tmpId = tmpId.replace(PL_PARAM, tagids[i]); 
		tmpId += "1";

		var tmp = scriptTemplate;
		tmp = tmp.replace(PL_PARAM, tmpId );
		tmp = tmp.replace(PL_PARAM, tmpId );

		var target = document.getElementById(tagids[i]);
		if(target){
			target.innerHTML = tmp;
		}	

		visiWhizzy(tmpId , true);

	}
}

function showImages(vis){
	var i = 0;
	for(i=0; i<=tagids.length; i++){

		var tmpId = PL_UPDATE_FIELD;
		tmpId = tmpId.replace(PL_PARAM, tagids[i]); 
		tmpId += "1";

		visiWhizzy(tmpId, vis);
	}
}

function submitModellerValue()
{
    replaceTags();

    var ageValue = document.getElementById("input_co_nra").value;
    var cashValue =  document.getElementById ("input_co_cash").value;
    var option =document.getElementById("input_co_option").value;

    var xml = generateXmlForModeller(ageValue, option, cashValue);
	//alert(xml);
    var modellerUrl = ajaxurl + "/ActiveRetirement?xml="+xml;

    modellerHttp = createXmlHttpRequestObject();

    modellerHttp.open("POST", modellerUrl );

    modellerHttp.onreadystatechange = ChangeModellerHandlerResponse;
   
    modellerHttp.send (null);

	modeller_popupControlPanel('age_panel', false);
	modeller_popupControlPanel('contrib_option_panel', false);
	modeller_popupControlPanel('cash_panel', false);

}

function ChangeModellerHandlerResponse()
{

       if (modellerHttp.readyState == 4) {                               
   
        // Check that a successful server response was received
        if (modellerHttp.status == 200) {
            //var response = modellerHttp.responseXML;
            var response = createResponseXML(modellerHttp.responseText);	
            //alert(modellerHttp.responseText);
            try{

                if(response.getElementsByTagName("Error").length > 0){                               
                    alert("Error: " + response.getElementsByTagName("Error")[0].firstChild.nodeValue);               
                }
                else
                {
                    var i=0;
                    for(i=0; i<npas.length ; i++)
                    {                    	
                    	//alert(npas[i]);
                        var tags = response.getElementsByTagName(npas[i]);

                        if(tags.length > 0)
                        {
                        	//alert('' + npas[i] + ' has value ' + tags[0].firstChild.nodeValue);
                            //var target = document.getElementById(tagids[i]);
							var tmpId = PL_UPDATE_FIELD;
							tmpId = tmpId.replace(PL_PARAM, tagids[i]); 
							tmpId += "1";
							//alert(tmpId);
							var target = document.getElementById(tmpId);
							if(target!=null)
							{
												
	                            if(target && tags[0].childNodes.length > 0)
	                            {
	                                target.innerHTML = tags[0].firstChild.nodeValue;
	                            }
	                            else
	                            {
	                                target.innerHTML = "";
	                            }
	                         }
                         	                            	
       						visiWhizzy(tmpId , false);
                        }                                                                        

                    }

					var overfundIndicator = response.getElementsByTagName("overfundIndicator");
					if(overfundIndicator.length > 0 && overfundIndicator[0].childNodes.length > 0)
					{
						var overfundWarning = overfundIndicator[0].firstChild.nodeValue;
						if(overfundWarning == "true"){
							document.getElementById("ar_overfund").className = "";
	
							document.getElementById("ar_overfund").style.visibility = "visible";
						
						}else{
							document.getElementById("ar_overfund").className = "caveat";
							document.getElementById("ar_overfund").style.visibility = "hidden";
						}
					}
	
					var veraIndicator = response.getElementsByTagName("veraIndicator");
					if(veraIndicator.length > 0 && veraIndicator[0].childNodes.length > 0)
					{
						var veraWarning = veraIndicator[0].firstChild.nodeValue;
						if(veraWarning == "true"){
							document.getElementById("ar_vera").className = "";
	
							document.getElementById("ar_vera").style.visibility = "visible";
						
	
						}else{
							document.getElementById("ar_vera").className = "caveat";
							document.getElementById("ar_vera").style.visibility = "hidden";
						}
					}
			
					var maxtaxcash = response.getElementsByTagName("MaximumCashLumpSum")[0].firstChild.nodeValue;

					var cashlumpsum = response.getElementsByTagName("NpaCashLumpSum")[0].firstChild.nodeValue;
					//alert(cashlumpsum);
			
					cashSlider.n_maxValue = parseInt(maxtaxcash);
					
					cashSlider.n_pix2value = cashSlider.n_pathLength / (cashSlider.n_maxValue - cashSlider.n_minValue);
					
					cashSlider.f_setValue(cashlumpsum);	

                }
               
                showImages(false);
   
            }
            catch(err)
            {
                DebugModeller(err.description);
                showImages(false);
		//alert(err);
            }

        }
               
    }   
}