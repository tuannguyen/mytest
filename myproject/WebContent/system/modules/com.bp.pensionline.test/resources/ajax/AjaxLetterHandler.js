var letterHttp = null;

var isRrDebug = false;

var total_bp_unreg = 0;
var total_bc_unreg = 0;
var total_subs_unreg = 0;
var total_pension_reset = 0;
var num_bp_unreg_submitted = 0;
var num_bc_unreg_submitted = 0;
var num_subs_unreg_submitted = 0;
var num_pension_reset_submitted = 0;

var waitingImage = 
"<p><span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"&nbsp;&nbsp;&nbsp;<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/whizzy.gif\" alt=\"waiting...\" /></span>" +
"</p>";
var waitingMessage = "<p>Please wait for the letters being generated. The generation time depends upon the number of members you have requested.</p>";
var spinningImage = "<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"waiting...\" /></span>";

function generatePasswordLetterXML(num_bp_unreg, num_bc_unreg, num_subs_unreg, num_pension_reset, 
	add_signature, commit_changes)
{
		var xml = "";
		xml += "<AjaxLetterParameterRequest>\n";
	    xml += "	<Password>\n";
		xml += "		<num_bp_unreg>" + num_bp_unreg + "</num_bp_unreg>\n";
	    xml += "		<num_bc_unreg>" + num_bc_unreg+ "</num_bc_unreg>\n";
		xml += "		<num_subs_unreg>" + num_subs_unreg + "</num_subs_unreg>\n";
	    xml += "		<num_pension_reset>" + num_pension_reset+ "</num_pension_reset>\n";
	    xml += "		<add_signature>" + add_signature+ "</add_signature>\n";
	    xml += "		<commit_changes>" + commit_changes+ "</commit_changes>\n";	    	    
	    xml += "	</Password>\n";	    
		xml += "</AjaxLetterParameterRequest>\n";
		return xml;	
}

function loadPasswordNumberActives(){
	
	var xml = generatePasswordLetterXML(0, 0, 0, 0, false, false);
	
	var passwordLetterURL = ajaxurl + "/PasswordLetterHandler?xml="+xml;
	
	//alert(passwordLetterURL);

	letterHttp = createXmlHttpRequestObject();

	letterHttp.open("POST", passwordLetterURL );

	letterHttp.onreadystatechange = handleInitPasswordLetterResponse;		
	
	loadTotalWhizzy(true);
	
	letterHttp.send(null); 

}

function submitPasswordLetterGenRequest(){

	var addSignature = false;
	/*
	if (document.getElementById('add_signature').checked == true)
	{
		addSignature = true;
	}*/
	var commitChanges = false;
	if (document.getElementById('commit_changes').checked == true)
	{
		commitChanges = true;
	}	
	
	if (num_bp_unreg_submitted == 0 && num_bc_unreg_submitted == 0 && num_subs_unreg_submitted == 0
		&& num_pension_reset_submitted == 0) 
	{
		alert('There is no members added!');
		return false;
	}

	
	var xml = generatePasswordLetterXML(num_bp_unreg_submitted, num_bc_unreg_submitted, 
		num_subs_unreg_submitted, num_pension_reset_submitted, addSignature, commitChanges);			
	//alert(xml);
	var passwordLetterURL = ajaxurl + "/PasswordLetterHandler?xml="+xml;
	
	letterHttp = createXmlHttpRequestObject();

	letterHttp.open("POST", passwordLetterURL );

	letterHttp.onreadystatechange = handlePasswordLetterResponse;
	
	genDocWhizzy(true);
	
	letterHttp.send(null); 

}


function handleInitPasswordLetterResponse(){

   	if (letterHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (letterHttp.status == 200) {
			var response = createResponseXML(letterHttp.responseText);					
			//alert(letterHttp.responseText);
			//alert(http.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
				}
				else
				{
					total_bp_unreg = parseInt(response.getElementsByTagName("total_bp_unreg")[0].firstChild.nodeValue);
					total_bc_unreg = parseInt(response.getElementsByTagName("total_bc_unreg")[0].firstChild.nodeValue);
					total_subs_unreg = parseInt(response.getElementsByTagName("total_subs_unreg")[0].firstChild.nodeValue);
					total_pension_reset = parseInt(response.getElementsByTagName("total_pension_reset")[0].firstChild.nodeValue);
					
					loadTotalWhizzy(false);
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}
	}

}

function handlePasswordLetterResponse(){
   	if (letterHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (letterHttp.status == 200) {
			var response = createResponseXML(letterHttp.responseText);
			//alert('response: ' + letterHttp.responseText);
			var error = response.getElementsByTagName("Error").length;				
			if(error > 0){
				window.location = "_letter_password_error.html";
			}
			else
			{
				var commitChanges = response.getElementsByTagName("commit_changes")[0].firstChild.nodeValue;
				if (commitChanges.toLowerCase() == "true") 
				{
					//document.getElementById('add_signature').checked = false;
					document.getElementById('commit_changes').checked = false;
					document.getElementById('total_commit').innerHTML = '(0 members awaiting)';				
				}
				//alert('go go form');
				genDocWhizzy(false);
				//downloadme('_letter_password.doc');
				//document.getElementById('form_password_letter').submit();
				window.location = "_letter_complete.html";
			}				
		}
	}
}

 function downloadme(x){
    var myTempWindow = window.open(x,'','left=10000,screenX=10000');
    myTempWindow.document.execCommand('SaveAs','null',x);
    myTempWindow.close();
}

var errorMessage = 'The number must be a positive integer and not be greater than the number of members available.';
function addMoreWelcomePackMembers ()
{
	var bp_unreg_input = document.getElementById('bp_unreg');
	var bc_unreg_input = document.getElementById('bc_unreg');
	var subs_unreg_input = document.getElementById('subs_unreg');	

	if (!isValidPositiveInteger(bp_unreg_input.value)) {
		alert(errorMessage);
		bp_unreg_input.focus();
		return false;
	}
	var bpNumberToAdd = parseInt(bp_unreg_input.value);
	if (bpNumberToAdd < 0 || bpNumberToAdd > total_bp_unreg) {
		alert(errorMessage);
		bp_unreg_input.focus();
		return false;
	}	

	if (!isValidPositiveInteger(bc_unreg_input.value)) {
		alert(errorMessage);
		bc_unreg_input.focus();
		return false;
	}	
	var bcNumberToAdd = parseInt(bc_unreg_input.value);
	if (bcNumberToAdd < 0 || bcNumberToAdd > total_bc_unreg) {
		alert(errorMessage);
		bc_unreg_input.focus();
		return false;
	}
	
	if (!isValidPositiveInteger(subs_unreg_input.value)) {
		alert(errorMessage);
		subs_unreg_input.focus();
		return false;
	}	
	var subsNumberToAdd = parseInt(subs_unreg_input.value);	
	if (subsNumberToAdd < 0 || subsNumberToAdd > total_subs_unreg) {
		alert(errorMessage);
		subs_unreg_input.focus();
		return false;
	}		

	// update number submitted and available
	num_bp_unreg_submitted += bpNumberToAdd;
	num_bc_unreg_submitted += bcNumberToAdd;
	num_subs_unreg_submitted += subsNumberToAdd;
	
	total_bp_unreg -= bpNumberToAdd;
	total_bc_unreg -= bcNumberToAdd;
	total_subs_unreg -= subsNumberToAdd;
//	alert('bp submitted: ' + num_bp_unreg_submitted);
//	alert('bc submitted: ' + num_bc_unreg_submitted);
//	alert('subs submitted: ' + num_subs_unreg_submitted);
	updateUnregNumbers();
	return true;	
}

function addMorePasswordResetMembers ()
{
	var pension_reset_input = document.getElementById('pension_reset');	

	if (!isValidPositiveInteger(pension_reset_input.value)) {
		alert(errorMessage);
		pension_reset_input.focus();
		return false;
	}	
	var prNumberToAdd = parseInt(pension_reset_input.value);
	if (prNumberToAdd < 0 || prNumberToAdd > total_pension_reset) {
		alert(errorMessage);
		pension_reset_input.focus();
		return false;
	}	

	// update number submitted and available
	num_pension_reset_submitted += prNumberToAdd;
	
	total_pension_reset -= prNumberToAdd;

	//alert('pension reset submitted: ' + num_pension_reset_submitted);
	updatePensionResetNumbers();
	return true;	
}

function updateUnregNumbers ()
{
	document.getElementById('bp_unreg_total').innerHTML = '(' + formatNumber(total_bp_unreg) + ' available)';
	document.getElementById('bc_unreg_total').innerHTML = '(' + formatNumber(total_bc_unreg) + ' available)';
	document.getElementById('subs_unreg_total').innerHTML = '(' + formatNumber(total_subs_unreg) + ' available)';
	
	document.getElementById('bp_unreg').value = total_bp_unreg;
	document.getElementById('bc_unreg').value = total_bc_unreg;
	document.getElementById('subs_unreg').value = total_subs_unreg;
	
	if (total_bp_unreg == 0) {
		document.getElementById('bp_unreg').setAttribute('disabled', 'disabled');		
	} else {
		document.getElementById('bp_unreg').removeAttribute('disabled');
	}	
	if (total_bc_unreg == 0) {
		document.getElementById('bc_unreg').setAttribute('disabled', 'disabled');		
	} else {
		document.getElementById('bc_unreg').removeAttribute('disabled');
	}
	if (total_subs_unreg == 0) {
		document.getElementById('subs_unreg').setAttribute('disabled', 'disabled');		
	} else {
		document.getElementById('subs_unreg').removeAttribute('disabled');
	}
	
	var totalCommit = num_bp_unreg_submitted + num_bc_unreg_submitted + num_subs_unreg_submitted + num_pension_reset_submitted;
	document.getElementById('total_commit').innerHTML = '(' + formatNumber(totalCommit) + ' members awaiting)';
}

function updatePensionResetNumbers ()
{
	document.getElementById('pension_reset_total').innerHTML = '(' + formatNumber(total_pension_reset) + ' available)';

	document.getElementById('pension_reset').value = total_pension_reset;
	
	if (total_pension_reset == 0) {
		document.getElementById('pension_reset').setAttribute('disabled', 'disabled');		
	} else {
		document.getElementById('pension_reset').removeAttribute('disabled');
	}			

	var totalCommit = num_bp_unreg_submitted + num_bc_unreg_submitted + num_subs_unreg_submitted + num_pension_reset_submitted;
	document.getElementById('total_commit').innerHTML = '(' + formatNumber(totalCommit) + ' members awaiting)';
}

function genDocWhizzy(isWaiting)
{
	if (isWaiting)
	{
		document.getElementById('password_letter_form').style.display = "none";
		document.getElementById('password_letter_whizzy').style.visibility = "visible";
		document.getElementById('password_letter_whizzy').innerHTML = waitingMessage + waitingImage;
	}
	else
	{
		document.getElementById('password_letter_form').style.display = "";
		document.getElementById('password_letter_whizzy').style.visibility = "hidden";		
	}
}

function loadTotalWhizzy(isWaiting)
{
	if (isWaiting) 
	{
		document.getElementById('bp_unreg_total').innerHTML = spinningImage;
		document.getElementById('bc_unreg_total').innerHTML = spinningImage;
		document.getElementById('subs_unreg_total').innerHTML = spinningImage;
		document.getElementById('pension_reset_total').innerHTML = spinningImage;	
		document.getElementById('total_commit').innerHTML = '(0 members awaiting)';
	} 
	else 
	{
		updateUnregNumbers();
		updatePensionResetNumbers();
	}

}

function isValidPositiveInteger(value){
	var numRegex = new RegExp('[0-9]{1}');
	if (value != null) {		
		for(var index=0; index<value.length; index++) {
			var c = value.charAt(index);
			if (!c.match(numRegex)) {
				return false;
			}
		}
		
		return true;
	}
	
	return false;
}

function formatNumber(number)
{
	var result = number;
	if (number > 1000)
	{
		var carrier = '' + (number/1000); 
		var remainder = '' + (number%1000);
		result = carrier.substring(0, carrier.indexOf('.')) + ',' + remainder;
	}
	
	return result;
}


/******************************* Password Flagging ********************************/
var passwordFlaggingHttp = null;

var lastWPListContent ="";
var lastPRListContent ="";
var wpListContent = "";
var prListContent = "";
var isUpdated = false;

function listPasswordFlagged(){
	var passwordFlaggingURL = ajaxurl + "/PasswordFlaggingHandler?command=list";
	
	//alert(passwordLetterURL);

	passwordFlaggingHttp = createXmlHttpRequestObject();

	passwordFlaggingHttp.open("POST", passwordFlaggingURL );

	passwordFlaggingHttp.onreadystatechange = handlePasswordFlaggedListResponse;
	
	passwordFlagWhizzy(true);
	
	passwordFlaggingHttp.send(null); 

}

function handlePasswordFlaggedListResponse(){

   	if (passwordFlaggingHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (passwordFlaggingHttp.status == 200) {
			var response = createResponseXML(passwordFlaggingHttp.responseText);					
			//alert(passwordFlaggingHttp.responseText);
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
				}
				else
				{
					if (response.getElementsByTagName("welcome_pack_list")[0].firstChild)
					{
						wpListContent = response.getElementsByTagName("welcome_pack_list")[0].firstChild.nodeValue;
					}
					else
					{
						wpListContent = "";
					}
					if (prListContent = response.getElementsByTagName("password_reset_list")[0].firstChild)
					{
						prListContent = response.getElementsByTagName("password_reset_list")[0].firstChild.nodeValue;
					} 
					else 
					{
						prListContent = "";
					}
					
					if (isUpdated) 
					{
						alert("Password flagged members have been updated!");
						isUpdated = false;
					}

					passwordFlagWhizzy(false);
				}	
			}
			catch(err){
				alert("List initialization error." );	
				document.getElementById('welcome_pack_list').innerHTML = '<textarea name="wp_list"></textarea>';
				document.getElementById('password_reset_list').innerHTML = '<textarea name="pr_list"></textarea>';	
				document.getElementById('wp_update').style.visibility = "visible";	
				document.getElementById('pr_update').style.visibility = "visible";					
			}						
		}
	}

}

function updateWPPasswordFlagged()
{
	lastWPListContent = wpListContent;
	wpListContent = document.getElementsByName('wp_list')[0].innerHTML;
	var normalizedContent = wpListContent.replace(/\n/g, ',');
	var lastNormalizedContent = lastWPListContent.replace(/\n/g, ',');	
	var ninoArray = normalizedContent.split(",");
	var havePPTN = false;
	normalizedContent = "";
	for (var i=0; i<ninoArray.length; i++) {
		var nino = ninoArray[i].toLowerCase();
		if (nino.indexOf("pp")==0 || nino.indexOf("tn")==0) {
			havePPTN = true;
		} else {
			normalizedContent += ","+nino;
		}
	}
	normalizedContent = normalizedContent.substring(1);
	if (havePPTN) {
		alert("The members which start with PP or TN will not be updated!");
	}
	var passwordFlaggingURL = ajaxurl + "/PasswordFlaggingHandler?command=update&wpFlaggedList=" 
									  + normalizedContent + "&lastWPFlaggedList=" + lastNormalizedContent;
	passwordFlaggingHttp = createXmlHttpRequestObject();
	passwordFlaggingHttp.open("POST", passwordFlaggingURL );
	passwordFlaggingHttp.onreadystatechange = handlePasswordFlaggedUpdateResponse;
	passwordFlagWhizzy(true);	
	passwordFlaggingHttp.send(null); 	
}

function updatePRPasswordFlagged()
{
	lastPRListContent = prListContent;	
	prListContent = document.getElementsByName('pr_list')[0].innerHTML;
	// remove break characters
	var normalizedContent = prListContent.replace(/\n/g, ',');
	var lastNormalizedContent = lastPRListContent.replace(/\n/g, ',');
	var ninoArray = normalizedContent.split(",");
	var havePPTN = false;
	normalizedContent = "";
	for (var i=0; i<ninoArray.length; i++) {
		var nino = ninoArray[i].toLowerCase();
		if (nino.indexOf("pp")==0 || nino.indexOf("tn")==0) {
			havePPTN = true;
		} else {
			normalizedContent += ","+nino;
		}
	}
	normalizedContent = normalizedContent.substring(1);
	if (havePPTN) {
		alert("The members which start with PP or TN will not be updated!");
	}
	var passwordFlaggingURL = ajaxurl + "/PasswordFlaggingHandler?command=update&prFlaggedList=" 
		+ normalizedContent + "&lastPRFlaggedList=" + lastNormalizedContent;
	//alert(passwordFlaggingURL);
	passwordFlaggingHttp = createXmlHttpRequestObject();

	passwordFlaggingHttp.open("POST", passwordFlaggingURL );

	passwordFlaggingHttp.onreadystatechange = handlePasswordFlaggedUpdateResponse;
	
	passwordFlagWhizzy(true);
		
	passwordFlaggingHttp.send(null); 	
}

function handlePasswordFlaggedUpdateResponse(){

   	if (passwordFlaggingHttp.readyState == 4) {									
		// Check that a successful server response was received
		if (passwordFlaggingHttp.status == 200) {
			var response = createResponseXML(passwordFlaggingHttp.responseText);
			//alert(passwordFlaggingHttp.responseText);
			var updatelistValid = response.getElementsByTagName("update_list_validate")[0].firstChild.nodeValue;
			if (updatelistValid == "false") 
			{
				alert('One or more NI numbers does not exist! These numbers will not be updated.')
			}

			listPasswordFlagged();
			isUpdated = true;			
		}
	}

}

function passwordFlagWhizzy(waiting)
{
	if (waiting) 
	{
//		alert("waiting: " + wpListContent);
//		alert("waiting: " + prListContent);		
		document.getElementById('welcome_pack_list').innerHTML = spinningImage;
		document.getElementById('password_reset_list').innerHTML = spinningImage;
		document.getElementById('wp_update').style.visibility = "hidden";	
		document.getElementById('pr_update').style.visibility = "hidden";	
	} 
	else 
	{
//		alert("response: " + wpListContent);
//		alert("response: " + prListContent);
		document.getElementById('welcome_pack_list').innerHTML='<textarea name="wp_list">'+wpListContent+'</textarea>';
		document.getElementById('password_reset_list').innerHTML='<textarea name="pr_list">'+prListContent+'</textarea>';					
		document.getElementById('wp_update').style.visibility = "visible";	
		document.getElementById('pr_update').style.visibility = "visible";			
	}

	
}