var newsLetterHandlerURL = ajaxurl + "/NewsLetterHandler?xml=";

function subscribe(SubscribeOption)
{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";		
		xml += "		<SubscribeOption><![CDATA[" + SubscribeOption+ "]]></SubscribeOption>\n";
		xml += "</AjaxParameterRequest>\n";	

		var url = newsLetterHandlerURL + escape(xml);  

		newsLetterHttp.open("POST", url);
		newsLetterHttp.onreadystatechange = newsLetterHandle;		
		newsLetterHttp.send(null);
}