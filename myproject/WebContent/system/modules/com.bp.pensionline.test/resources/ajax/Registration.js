// JavaScript Document
var httpReg = createXmlHttpRequestObject();
function submitRegister()
{
		var notMatchPattern = /[\[\]<>\{\}]/;
		
     	var nino = document.getElementById("nino");
        var name = document.getElementById("name");
        var dob = document.getElementById("dob");
        var telno = document.getElementById("telno");
        var captcha = document.getElementById("captcha");
        var email = document.getElementById("email");
		var ref = document.getElementById("refno");
		var pay = document.getElementById("payroll_number");
        
		if(name.value == ""){
                alert("Name is missing");
				name.focus();
                return false;
        }
		
		if(nino.value == ""){
                alert("National insurance number is missing");
				nino.focus();
                return false;
        }
        
        if(dob.value == ""){
                alert("Date of Birth is missing");
				dob.focus();
                return false;
        }
        
        if(telno.value == "" && email.value == ""){             
                alert("Either a telephone number or and email address must be specified");
				telno.focus();
                return false;                           
        }
        
        if(telno.value == ""){
                
                if(isValidEmail(email)){                
                }
                else{
					email.focus();
                    return false;
				}
        }       
        
        if(captcha.value == ""){
                alert("You must enter the security code");
				captcha.focus();
                return false;
        } 
        
        if (name.value.match(notMatchPattern) || nino.value.match(notMatchPattern) ||
        	dob.value.match(notMatchPattern) || telno.value.match(notMatchPattern) ||
        	email.value.match(notMatchPattern) || ref.value.match(notMatchPattern) ||
        	pay.value.match(notMatchPattern))
    	{
    		alert('The information you provided contain too special characters: < > [ ] { }.\nPlease remove those characters first.');
    		return false;
    	}

	var xml = "";
	xml += "<AjaxParameterRequest>";
	xml += "	<Register>";
	xml += " 		<Name>"+name.value+"</Name>";     
	xml += " 		<Ni>"+nino.value+"</Ni>";   
	xml += " 		<Dob>"+dob.value+"</Dob>";         
	xml += " 		<Telephone>"+telno.value+"</Telephone>";
	xml += " 		<Email>"+email.value+"</Email>";
	xml += " 		<SecurityCode>"+captcha.value+"</SecurityCode>";	
	xml += " 		<Refno>"+ref.value+"</Refno>";	
	xml += " 		<Payroll>"+pay.value+"</Payroll>";		   
	xml += "	</Register>";		
	xml += "</AjaxParameterRequest>";
		
	//var url = ajaxurl + "/pl/_registration_handler.jsp?xml=" + xml ;
	var url = ajaxurl + "/RegistrationHandler?xml=" + xml ;
	httpReg.open("POST", url);
	httpReg.onreadystatechange = RegistryHandlerResponse;	
	httpReg.send(null);
}

function RegistryHandlerResponse()
{
	if (httpReg.readyState == 4) {								
		// Check that a successful server response was received
		if (httpReg.status == 200) {
			var response = createResponseXML(httpReg.responseText);
			var error = response.getElementsByTagName("Error").length;	
				if(error > 0){	
					alert(response.getElementsByTagName("Error")[0].firstChild.nodeValue);
					return false;
				}
				else
				{
					window.location = "_register_confirm.html";
				}
		}				
	}	
}
