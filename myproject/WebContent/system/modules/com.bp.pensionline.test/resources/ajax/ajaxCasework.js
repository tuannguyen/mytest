var csHttp = null;
var isDebugCs = false;

var headTable = "<table class='datatable'><tbody><tr><th class='label'>Subject</th><th class='label'>Submitted</th><th class='label'>Casework</th><th class='label'>Member</th></tr>";
var footTable = "</tbody></table>";


var bodyTable = "";

function replaceAll(strReplaceAll, replacement, newstr){
	var intIndexOfMatch = strReplaceAll.indexOf( replacement );
  
	// Loop over the string value replacing out each matching
	// substring.
	while (intIndexOfMatch != -1){
		// Relace out the current instance.
		strReplaceAll = strReplaceAll.replace( replacement, newstr )
  
		// Get the index of any next matching substring.
		intIndexOfMatch = strReplaceAll.indexOf( replacement );
	}

	return strReplaceAll;
}

function encodeHTML(str){

 	str = replaceAll(str, ">", "&gt;");
	
	str = replaceAll(str, "<", "&lt;");	
	
	return str;
}


function getXmlForSearchCaseWork(bgroup, csid){
	var xml = "";
	
	xml += "<AjaxRequestXml>";
	xml += "	<Bgroup>" + bgroup + "</Bgroup>";
	xml += "	<CaseWorkID>" + csid +"</CaseWorkID>";
	xml += "</AjaxRequestXml>";
	
	return xml;
}

function getXmlForViewCaseWork(){
	var xml = "";
	
	xml += "<AjaxRequestXml>";
	xml += "	<RequestResult>true</RequestResult>";
	xml += "</AjaxRequestXml>";
	
	return xml;
}

function submitSearchCasework(){
	
	var bgroup = document.getElementById("bgroup").value;

	var csid = document.getElementById("caseno").value;
	
	var xml = getXmlForSearchCaseWork(bgroup, csid);
	
	var csUrl = ajaxurl + "/CaseworkHandler?xml="+xml;
	
	//DebugCs(csUrl);
	
	csHttp = createXmlHttpRequestObject();
	
	csHttp.open("POST", csUrl );

	csHttp.onreadystatechange = handleSearchResponse;
	
	csHttp.send(null);/**/
	
}

function submitViewCasework(){

	var xml = getXmlForViewCaseWork();
	
	var csUrl = ajaxurl + "/CaseworkHandler?xml="+xml;
	
	DebugCs(csUrl);
	csHttp = createXmlHttpRequestObject();
	
	csHttp.open("POST", csUrl );

	csHttp.onreadystatechange = handleViewResponse;
	
	csHttp.send(null);

}

function DebugCs(msg){
	if(isDebugCs){
		alert(msg);
	}
}

function handleSearchResponse(){
	if (csHttp.readyState == 4) {
								
		// Check that a successful server response was received
	   	if (csHttp.status == 200) 
		{
			//DebugCs(csHttp.responseText);
			var response = csHttp.responseXML;
			if(response){
				var errtags = response.getElementsByTagName("Error");
				if(errtags.length > 0){
					var errmsg = errtags[0].firstChild.nodeValue;
					alert(errmsg);
				}
else
{
window.location="_casework_forms.html";
}
			}else{
				DebugCs("Bad response");
			}
				
		}
	}
}

function handleViewResponse(){
	if (csHttp.readyState == 4) {
								
		// Check that a successful server response was received
	   	if (csHttp.status == 200) 
		{
			DebugCs(csHttp.responseText);

			var response = csHttp.responseXML;
			
			var member = "";
			var submitted = "";
			var subject = "";
			var casework = "";
			var formdata = "";
			try{
				var memtag = response.getElementsByTagName("Member");
			
				if(memtag.length > 0){
					member = memtag[0].firstChild.nodeValue;				
				}
			
				var subtag = response.getElementsByTagName("Submitted");
			
				if(subtag.length > 0){
					submitted = subtag[0].firstChild.nodeValue;				
				}
			
				var subjtag = response.getElementsByTagName("Subject");
			
				if(subjtag.length > 0){
					subject = subjtag[0].firstChild.nodeValue;				
				}
			
				var cstag = response.getElementsByTagName("CaseworkId");
			
				if(cstag.length > 0){
					caseworkid = cstag[0].firstChild.nodeValue;				
				}			

				var frmtag = response.getElementsByTagName("FormData");
			
				if(frmtag.length > 0){
					formdata = frmtag[0].xml;
					DebugCs(formdata);
				}
				
			
				if(formdata.length > 0){
					formdata = encodeHTML(formdata);
					DebugCs(formdata);
					formdata = 	"<span style='overflow: visible; position:absolute;background-color: #FFFFCC;BORDER-BOTTOM: black 2px solid;BORDER-RIGHT: black 2px solid;BORDER-TOP: black 2px solid;BORDER-LEFT: black 2px solid; layer-background-color: #FFFFCC;visibility:hidden' id='CaseWorkID' >"+formdata+"</span>";
				}
				/*
				var foundpoint=-1;
				var endpoint = -1;

				foundpoint=csHttp.responseText.indexOf("<FormData>");

				if(foundpoint>0)
				{
					endpoint = csHttp.responseText.indexOf("</FormData>");

					formdata = csHttp.responseText.substring(foundpoint+"<FormData>".length,endpoint );
					formdata = 	"<span style='position:absolute;width:200;background-color: #FFFFCC;BORDER-BOTTOM: black 2px solid;BORDER-RIGHT: black 2px solid;BORDER-TOP: black 2px solid;BORDER-LEFT: black 2px solid;layer-background-color: #FFFFCC; visibility:hidden'  id='CaseWorkID' ><span class=bookmark_component_holder><span class=bookmark_value>"+formdata+"</span></span>";
				}
				*/
				bodyTable +="<tr><td>" 
					+ subject + "</td><td>" + submitted 
					+ "</td><td><span style='cursor:pointer;cursor:hand'  id='' onclick=ViewTag('CaseWorkID',this); onmouseout=HideTag('CaseWorkID');>"+ caseworkid 
					+ "</span>"+formdata+"</td><td>" + member + "</td></tr>";
		
				
				
				var table = headTable + bodyTable + footTable ;
				document.getElementById("casework_form").outerHTML = table;
			}catch(err){
				DebugCs(err.description);
			}
		
		}
	}
}
