// Common variables
var queryDesignerHttp = null;
var datasource = '';
var PANEL_LEFT = 220;
var PANEL_TOP = 30;
var PANEL_WIDTH = 700;
var TABLE_DISTANCE_X = 20;
var TABLE_DISTANCE_Y = 40;
var TABLE_WIDTH = 150;
var TABLE_HEIGHT = 250;
var current_cursor_x = PANEL_LEFT + TABLE_DISTANCE_X;
var current_cursor_y = PANEL_TOP + 20;

var selectedTables = new Array();
var reloadTables = new Array();
var selectedFields = new Array();	// contains mapping objects selected by checkbox

var whereConditions = new Array(); // in this version all conditions in this array is combined AND
var havingConditions = new Array();
//var expressionFields = new Array(); no use
var groupByFields = new Array();
var orderByFields = new Array();

var lastBuilderInnerHTML = '';

var EXPRESSION_NAMES = Array('count', 'max', 'min', 'sum');
var BULLET_CHAR = '•';
// tab control
var tabs = new Array
(
	"Builder|BUILDER|*",
	"SQL|SQL"
);

function initDesignerPanel()
{
	var openner = window.opener;
	if (openner)
	{
		if (openner.document.getElementById('report_datasource'))
		{
			datasource = openner.document.getElementById('report_datasource').value;
		}
	}
	//alert(datasource);
	if (datasource.length > 0)
	{
		// init the table list
		initTableListRequest (datasource);
	}
	
	tabLoad();
}

/*********************************************/
// Align Tab: LEFT, CENTER, RIGHT
var tabAlign = "LEFT";

function tabLoad()
{
	var HTML = "";

	HTML += "<P ALIGN="+tabAlign+">";
	for (var i = 0; i < tabs.length; i++)
	{
		var tab = tabs[i].split("|");
		if (tab.length > 1)
		{
			HTML += "<INPUT TYPE='BUTTON' ID="+i+" CLASS='tabOff' VALUE="+tab[0]+" onClick='tabOnClick("+i+");'>&nbsp;";	
		}		
	}

	divTabButtons.innerHTML = HTML;

	for (var i = 0; i < tabs.length; i++)
	{
		var tab = tabs[i].split("|");

		if (tab.length > 2 && tab[2] == "*")
		{
			tabOnClick(i);
			break;
		}
	}
}

function tabOnClick(ID)
{
	var oElement = null;
	for (var i = 0; i < tabs.length; i++)
	{
		oElement = document.getElementById(i);
		oElement.className = "tabOff";
	}

	oElement = document.getElementById(ID);
	oElement.className = "tabOn";	

	if (ID == 1)	// SQL
	{		
		renderSQLBoard();
		for(var i=0; i<selectedTables.length; i++) 
		{
			reloadTables.push(selectedTables[i]);
		}
	}
	else if (ID == 0) // BUILDER
	{
		// restore the content of query holder
		var queryHolderFrame = document.getElementById('query_holder_frame');
		if (queryHolderFrame)
		{
			// reload the tables selected
			queryHolderFrame.innerHTML = '';
			current_cursor_x = PANEL_LEFT + TABLE_DISTANCE_X;
			current_cursor_y = PANEL_TOP + 20;
			reloadTable();
			
			// update outline
			updateOutlineSelect();
			updateOutlineFrom();
			updateOutlineWhere();
			updateOutlineHaving();
			updateOutlineGroupBy();
			updateOutlineOrderBy();
		}		
	}
}

function reloadTable()
{
	if (reloadTables.length == 0)
	{
		return true;
	}
	
	var tableId = reloadTables.shift();
	// request sevlet for data mappings of this table
	var queryDesginerURL = "/content/QueryDesignerFetchAttributeHandler";
	var params = "xml=" + generateFetchAttributesRequestXML(datasource, tableId);
	queryDesignerHttp = createXmlHttpRequestObject();

	queryDesignerHttp.open("POST", queryDesginerURL, true );

	queryDesignerHttp.onreadystatechange = handleReloadTableResponse;		
	
    queryDesignerHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    queryDesignerHttp.setRequestHeader("Content-length", params.length);
    queryDesignerHttp.setRequestHeader("Connection", "close");		

	queryDesignerHttp.send(params);	
}

function handleReloadTableResponse ()
{
   	if (queryDesignerHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (queryDesignerHttp.status == 200) {
			var response = createResponseXML(queryDesignerHttp.responseText);					
			//alert(queryDesignerHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					return false;
				}
				else
				{
					var table = response.getElementsByTagName("DD_Table")[0].firstChild.nodeValue;										
					var mappingNodeList = response.getElementsByTagName("DD_AttAliasMapping");
					
					if (mappingNodeList != null)
					{
						var numMappings = mappingNodeList.length;
						// update table list box
						var mappingListDiv = document.createElement('DIV');
						mappingListDiv.id = table;
						mappingListDiv.style.display = 'block';
						mappingListDiv.style.position = 'absolute';
						mappingListDiv.style.pixelLeft = current_cursor_x;						
						mappingListDiv.style.pixelTop = current_cursor_y;
						mappingListDiv.style.width = TABLE_WIDTH + 'px';								
						mappingListDiv.style.backgroundColor = "#FFF";
						mappingListDiv.style.border = 'solid #090 1px';

						// create table label with a close button
						var tblLabelDiv = document.createElement('DIV');
						tblLabelDiv.style.backgroundColor = "#9c0";
						tblLabelDiv.innerHTML = "<a href=\"\" onclick=\"closeTable('" +
								table + "'); return false;\">X</a>";
						mappingListDiv.appendChild(tblLabelDiv);
						tblLabelDiv.innerHTML += "<label>&nbsp;&nbsp;" + table + "</label>";
						
						// create table content
						var tblContentDiv = document.createElement('DIV');
						tblContentDiv.style.width = TABLE_WIDTH + 'px';
						tblContentDiv.style.height = TABLE_HEIGHT + 'px';						
						tblContentDiv.style.overflow = "auto";
						mappingListDiv.appendChild(tblContentDiv);	
																						
						// get the mapping details
						for(var i=0; i<numMappings; i++) 
						{
							// get the mapping details and create check box for each attribute
							var att = mappingNodeList[i].childNodes[0].firstChild.nodeValue;
							var alias = att;
							if (mappingNodeList[i].childNodes[1].firstChild)
							{
								alias = mappingNodeList[i].childNodes[1].firstChild.nodeValue;
							}						
							var attDiv = document.createElement('DIV');
							attDiv.onclick = function(){elementClick(this);};
							attDiv.id =  table + '_' + att;
							
							var attCb = document.createElement('INPUT');
							attCb.type = "checkbox";							
 							attCb.id = table + '_' + att + "_cb";
 							attCb.onclick = function() {updateSelectedFields(this);};
 							
 							var attLabel = document.createElement('LABEL');
							attLabel.id = table + '_' + att + "_lb";
							attLabel.innerHTML = alias;							
							attDiv.appendChild(attCb);
							attDiv.appendChild(attLabel);														
							tblContentDiv.appendChild(attDiv);
							initContextMenuForField(attDiv, false);
						}						
						
						var tableHolderDiv = document.getElementById('query_holder_frame');
						tableHolderDiv.appendChild(mappingListDiv);
						//alert(mappingListDiv.innerHTML);
						current_cursor_x += (TABLE_WIDTH + TABLE_DISTANCE_X);
						// move to next line if outside of box
						if ((current_cursor_x + TABLE_WIDTH)> PANEL_LEFT + PANEL_WIDTH)
						{
							current_cursor_x = PANEL_LEFT + TABLE_DISTANCE_X;
							current_cursor_y += TABLE_HEIGHT + TABLE_DISTANCE_Y;
						}
						
						// update selected field for this table
						for(var i=0; i<selectedFields.length; i++) 
						{
							var mapping = selectedFields[i];
							if (mapping.expression == -1)
							{
								var attCb = document.getElementById(mapping.table + '_' + mapping.attribute + "_cb");
								if (attCb)
								{
									attCb.checked = true;
								}
							}
						}						
					}
				}
				
				if (reloadTables.length > 0)	
				{
					reloadTable();
				}
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}		
	}	
}

function initTableListRequest(datasource)
{		
	var queryDesginerURL = "/content/QueryDesignerInitTableHandler";
	var params = "xml=" + datasource;
	
	queryDesignerHttp = createXmlHttpRequestObject();

	queryDesignerHttp.open("POST", queryDesginerURL, true );

	queryDesignerHttp.onreadystatechange = handleInitTableListResponse;		
	
    queryDesignerHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    queryDesignerHttp.setRequestHeader("Content-length", params.length);
    queryDesignerHttp.setRequestHeader("Connection", "close");		

	queryDesignerHttp.send(params);
}

function handleInitTableListResponse(){

   	if (queryDesignerHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (queryDesignerHttp.status == 200) {
			var response = createResponseXML(queryDesignerHttp.responseText);					
			//alert(queryDesignerHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					return false;
				}
				else
				{
					var tblNodeList = response.getElementsByTagName("DD_Table");
					if (tblNodeList != null)
					{
						var numTables = tblNodeList.length;
						// update table list box
						var tbList = document.getElementById('table_list_frame');
						if (tbList)
						{										
							for(var i=0; i<numTables; i++) 
							{
								var tbName = tblNodeList[i].firstChild.nodeValue;
								var tableDiv = document.createElement('DIV');
								tableDiv.onclick = function(){elementClick(this);};
								tableDiv.ondblclick = function () {tableDivDblClick(this);};
								tableDiv.innerHTML = tbName;	
								tbList.appendChild(tableDiv);								
							} 
						}
					}
				}	
			}
			catch(err){
				alert("XML Parser error." );		
			}						
		}		
	}
}


function elementClick(this1)
{
	var pNode = this1.parentNode;
	if (pNode)
	{			
		var numAtts = pNode.childNodes.length;
		for(var i=0; i<numAtts; i++) 
		{
			var attDiv = pNode.childNodes[i];
			if (attDiv)
			{
				attDiv.style.backgroundColor='#FFF';
			}
		} 
	}
		
	// refresh all the div of the list
	this1.style.backgroundColor='#CCC';
}

function generateFetchAttributesRequestXML (datasource, tableId)
{
	var xml = "";
	xml += "<AjaxDDParameterRequest>";
	xml += "<dd_dbname>" + datasource + "</dd_dbname>";
	xml += "<dd_table>" + tableId + "</dd_table>";	
	xml += "</AjaxDDParameterRequest>";
	return xml;	
}

function tableDivDblClick(_this)
{	
	//alert(_this.firstChild.nodeValue);
	if (!_this)
	{
		alert('Please select a table!');
	}
	var tableId = _this.innerHTML;
	
	// check if table has been added
	for(var i=0; i<selectedTables.length; i++) 
	{
		if (tableId == selectedTables[i])
		{
			return false;
		}
	}
	
	// request sevlet for data mappings of this table
	var queryDesginerURL = "/content/QueryDesignerFetchAttributeHandler";
	var params = "xml=" + generateFetchAttributesRequestXML(datasource, tableId);
	queryDesignerHttp = createXmlHttpRequestObject();

	queryDesignerHttp.open("POST", queryDesginerURL, true );

	queryDesignerHttp.onreadystatechange = handleLoadTableResponse;		
	
    queryDesignerHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    queryDesignerHttp.setRequestHeader("Content-length", params.length);
    queryDesignerHttp.setRequestHeader("Connection", "close");		

	queryDesignerHttp.send(params);	
}

function handleLoadTableResponse(){

   	if (queryDesignerHttp.readyState == 4) {								
	
		// Check that a successful server response was received
		if (queryDesignerHttp.status == 200) {
			var response = createResponseXML(queryDesignerHttp.responseText);					
			//alert(queryDesignerHttp.responseText);
			try{                                    
				var errors = response.getElementsByTagName("Error");
				if(errors.length > 0){
					return false;
				}
				else
				{
					var table = response.getElementsByTagName("DD_Table")[0].firstChild.nodeValue;										
					var mappingNodeList = response.getElementsByTagName("DD_AttAliasMapping");
					selectedTables.push(table);
					
					if (mappingNodeList != null)
					{
						var numMappings = mappingNodeList.length;
						// update table list box
						var mappingListDiv = document.createElement('DIV');
						mappingListDiv.id = table;
						mappingListDiv.style.display = 'block';
						mappingListDiv.style.position = 'absolute';
						mappingListDiv.style.pixelLeft = current_cursor_x;						
						mappingListDiv.style.pixelTop = current_cursor_y;
						mappingListDiv.style.width = TABLE_WIDTH + 'px';								
						mappingListDiv.style.backgroundColor = "#FFF";
						mappingListDiv.style.border = 'solid #090 1px';

						// create table label with a close button
						var tblLabelDiv = document.createElement('DIV');
						tblLabelDiv.style.backgroundColor = "#9c0";
						tblLabelDiv.innerHTML = "<a href=\"\" onclick=\"closeTable('" +
								table + "'); return false;\">X</a>";
						mappingListDiv.appendChild(tblLabelDiv);
						tblLabelDiv.innerHTML += "<label>&nbsp;&nbsp;" + table + "</label>";
						
						// create table content
						var tblContentDiv = document.createElement('DIV');
						tblContentDiv.style.width = TABLE_WIDTH + 'px';
						tblContentDiv.style.height = TABLE_HEIGHT + 'px';						
						tblContentDiv.style.overflow = "auto";
						mappingListDiv.appendChild(tblContentDiv);	
																						
						// get the mapping details
						for(var i=0; i<numMappings; i++) 
						{
							// get the mapping details and create check box for each attribute
							var att = mappingNodeList[i].childNodes[0].firstChild.nodeValue;
							var alias = att;
							if (mappingNodeList[i].childNodes[1].firstChild)
							{
								alias = mappingNodeList[i].childNodes[1].firstChild.nodeValue;
							}						
							var attDiv = document.createElement('DIV');
							attDiv.onclick = function(){elementClick(this);};
							attDiv.id =  table + '_' + att;
							
							var attCb = document.createElement('INPUT');
							attCb.type = "checkbox";							
 							attCb.id = table + '_' + att + "_cb";
 							attCb.onclick = function() {updateSelectedFields(this);};
 							
 							var attLabel = document.createElement('LABEL');
							attLabel.id = table + '_' + att + "_lb";
							attLabel.innerHTML = alias;							
							attDiv.appendChild(attCb);
							attDiv.appendChild(attLabel);														
							tblContentDiv.appendChild(attDiv);
							initContextMenuForField(attDiv, false);
						}						
						
						var tableHolderDiv = document.getElementById('query_holder_frame');
						tableHolderDiv.appendChild(mappingListDiv);
						//alert(mappingListDiv.innerHTML);
						current_cursor_x += (TABLE_WIDTH + TABLE_DISTANCE_X);
						// move to next line if outside of box
						if ((current_cursor_x + TABLE_WIDTH)> PANEL_LEFT + PANEL_WIDTH)
						{
							current_cursor_x = PANEL_LEFT + TABLE_DISTANCE_X;
							current_cursor_y += TABLE_HEIGHT + TABLE_DISTANCE_Y;
						}
																
						// update outline from
						updateOutlineFrom ();
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}		
	}
}

function closeTable(tableId)
{
	var closedTable = document.getElementById(tableId);
	
	if (closedTable)
	{
		var closedTable_X = closedTable.style.pixelLeft;
		var closedTable_Y = closedTable.style.pixelTop;
		
		var pTable = closedTable.parentNode;
		if (pTable)
		{
			pTable.removeChild(closedTable);
		}
		
		// if table is the last one, adjust cursor
		if (selectedTables[selectedTables.length - 1] == tableId)
		{
			current_cursor_x -= TABLE_WIDTH + TABLE_DISTANCE_X;
			if (current_cursor_x < PANEL_LEFT + TABLE_DISTANCE_X)
			{
				// set the cursor to the removed table
				current_cursor_x = closedTable_X;
				current_cursor_y = closedTable_Y;				
			}
		}	
		
	
		// remove from selectedTables array
		var updateselectedTables = new Array();
		for(var i=0; i<selectedTables.length; i++) 
		{
			if (selectedTables[i] != tableId)
			{
				updateselectedTables.push(selectedTables[i]);
			}
		}
		selectedTables = updateselectedTables;		
		
		// if there's no more tables, reset the cursor
		if (selectedTables.length == 0)
		{
			current_cursor_x = PANEL_LEFT + TABLE_DISTANCE_X;
			current_cursor_y = PANEL_TOP + 20;
		}
		
		// update selected fields
		var updateSelectedFields = new Array();
		for(var i=0; i<selectedFields.length; i++) 
		{
			var mapping = selectedFields[i];
			if (mapping.table == tableId)
			{
				// do nothing
			}
			else
			{
				updateSelectedFields.push(mapping);
			}			
		}		
		selectedFields = updateSelectedFields;
		
		// update where and having clauses
		var updateWhereConditions = new Array();
		for(var i=0; i<whereConditions.length; i++) 
		{
			var whereClause = whereConditions[i];
			if (whereClause.indexOf(tableId) == 0)
			{
				// do nothing
			}
			else
			{
				updateWhereConditions.push(whereClause);
			}			
		}		
		whereConditions = updateWhereConditions;
		
		var updateHavingConditions = new Array();
		for(var i=0; i<havingConditions.length; i++) 
		{
			var havingClause = havingConditions[i];
			if (havingClause.indexOf(tableId) == 0)
			{
				// do nothing
			}
			else
			{
				updateHavingConditions.push(havingClause);
			}			
		}		
		havingConditions = updateHavingConditions;				
		
		updateOutlineFrom ();
		updateOutlineSelect();
		updateOutlineWhere();
		updateOutlineHaving();
	}
	//debugselectedFields();
}

function selectField(table, attribute, alias)
{
	var newMapping = {table: table, attribute: attribute, alias: alias};
	
	var fieldExisted = false;
	// check if field has been selected
	var newSelectedFields = new Array();
	for(var i=0; i<selectedFields.length; i++) 
	{
		var mapping = selectedFields[i];
		if (mapping.table == table && mapping.attribute == attribute)
		{
			fieldExisted = true;
		}
		else
		{
			newSelectedFields.push(mapping);
		}
	}
	if (!fieldExisted)
	{
		selectedFields.push(newMapping);
	
		// update the check box
		var attCb = document.getElementById(table + '_' + attribute + "_cb");
		if (attCb)
		{
			attCb.checked = true;
		}		
	}
	else
	{
		selectedFields = newSelectedFields;
	
		// update the check box
		var attCb = document.getElementById(table + '_' + attribute + "_cb");
		if (attCb)
		{
			attCb.checked = false;
		}		
	}
	
	updateOutlineSelect();
}

function popupAddWhereDialog(table, attribute)
{
	// popup form to input where clause
	var addConditionDialog = document.getElementById('addConditionWhere');
	if (addConditionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay == null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	overlay 			= document.createElement('div');
	    	overlay.id			= 'overlay';
	    	bod.appendChild(overlay);   	
	    }
	    overlay.style.display = 'block';
    		
		addConditionDialog.style.pixelLeft = 250;
		addConditionDialog.style.pixelTop = 200;
		addConditionDialog.style.display = 'block';
		
		var leftConditionText = document.getElementById('condition_left_part_where');
		if (leftConditionText)
		{
			leftConditionText.value = table + '.' + attribute;
		}
		var rightConditionText = document.getElementById('condition_right_part_where');
		if (rightConditionText)
		{
			rightConditionText.value = '';
		}		
	}	
}

function AddConditionWhere()
{
	// popup form to input where clause
	var addConditionDialog = document.getElementById('addConditionWhere');
	if (addConditionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay != null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	bod.removeChild(overlay);   	
	    }
    		
		// get the condition
		var leftCondtionValue = '';
		var leftConditionText = document.getElementById('condition_left_part_where');
		if (leftConditionText)
		{
			leftCondtionValue = leftConditionText.value;
		}
		
		var rightCondtionValue = '';
		var rightConditionText = document.getElementById('condition_right_part_where');
		if (rightConditionText)
		{
			rightCondtionValue = rightConditionText.value;
		}	
		
		var operant = '';
		var operantSelect = document.getElementById('condition_comparator_where');
		if (operantSelect)
		{
			operant = operantSelect.value;
		}
		
		if (operant == 'EQUAL')							
		{
			whereConditions.push(leftCondtionValue + ' = ' + rightCondtionValue);
		}
		else if (operant == 'LESS_THAN')							
		{
			whereConditions.push(leftCondtionValue + ' < ' + rightCondtionValue);
		}
		else if (operant == 'GREATER_THAN')							
		{
			whereConditions.push(leftCondtionValue + ' > ' + rightCondtionValue);
		}
		else if (operant == 'EQUAL_LESS_THAN')							
		{
			whereConditions.push(leftCondtionValue + ' <= ' + rightCondtionValue);
		}
		else if (operant == 'EQUAL_GREATER_THAN')							
		{
			whereConditions.push(leftCondtionValue + ' >= ' + rightCondtionValue);
		}
		else if (operant == 'NOT_EQUAL')							
		{
			whereConditions.push(leftCondtionValue + ' <> ' + rightCondtionValue);
		}
		else if (operant == 'LIKE')							
		{
			whereConditions.push(leftCondtionValue + ' LIKE ' + rightCondtionValue);
		}										
		else if (operant == 'NOT_LIKE')							
		{
			whereConditions.push(leftCondtionValue + ' NOT LIKE ' + rightCondtionValue);
		}	
		
		addConditionDialog.style.display = 'none';
		updateOutlineWhere ();
	}	
}

function popupAddHavingDialog(table, attribute)
{
	// popup form to input having clause
	var addConditionDialog = document.getElementById('addConditionHaving');
	if (addConditionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay == null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	overlay 			= document.createElement('div');
	    	overlay.id			= 'overlay';
	    	bod.appendChild(overlay);   	
	    }
	    overlay.style.display = 'block';
    		
		addConditionDialog.style.pixelLeft = 250;
		addConditionDialog.style.pixelTop = 200;
		addConditionDialog.style.display = 'block';
		
		var leftConditionText = document.getElementById('condition_left_part_having');
		if (leftConditionText)
		{
			leftConditionText.value = table + '.' + attribute;
		}
		var rightConditionText = document.getElementById('condition_right_part_having');
		if (rightConditionText)
		{
			rightConditionText.value = '';
		}		
	}	
}

function AddConditionHaving()
{
	// popup form to input Having clause
	var addConditionDialog = document.getElementById('addConditionHaving');
	if (addConditionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay != null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	bod.removeChild(overlay);   	
	    }
    		
		// get the condition
		var leftCondtionValue = '';
		var leftConditionText = document.getElementById('condition_left_part_having');
		if (leftConditionText)
		{
			leftCondtionValue = leftConditionText.value;
		}
		
		var rightCondtionValue = '';
		var rightConditionText = document.getElementById('condition_right_part_having');
		if (rightConditionText)
		{
			rightCondtionValue = rightConditionText.value;
		}	
		
		var operant = '';
		var operantSelect = document.getElementById('condition_comparator_having');
		if (operantSelect)
		{
			operant = operantSelect.value;
		}
		
		if (operant == 'EQUAL')							
		{
			havingConditions.push(leftCondtionValue + ' = ' + rightCondtionValue);
		}
		else if (operant == 'LESS_THAN')							
		{
			havingConditions.push(leftCondtionValue + ' < ' + rightCondtionValue);
		}
		else if (operant == 'GREATER_THAN')							
		{
			havingConditions.push(leftCondtionValue + ' > ' + rightCondtionValue);
		}
		else if (operant == 'EQUAL_LESS_THAN')							
		{
			havingConditions.push(leftCondtionValue + ' <= ' + rightCondtionValue);
		}
		else if (operant == 'EQUAL_GREATER_THAN')							
		{
			havingConditions.push(leftCondtionValue + ' >= ' + rightCondtionValue);
		}
		else if (operant == 'NOT_EQUAL')							
		{
			havingConditions.push(leftCondtionValue + ' <> ' + rightCondtionValue);
		}
		else if (operant == 'LIKE')							
		{
			havingConditions.push(leftCondtionValue + ' LIKE ' + rightCondtionValue);
		}										
		else if (operant == 'NOT_LIKE')							
		{
			havingConditions.push(leftCondtionValue + ' NOT LIKE ' + rightCondtionValue);
		}	
		
		addConditionDialog.style.display = 'none';
		updateOutlineHaving ();
	}	
}

function popupAddExpressionDialog(table, attribute)
{
	// popup form to input expression
	var addExpressionDialog = document.getElementById('addExpression');
	if (addExpressionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay == null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	overlay 			= document.createElement('div');
	    	overlay.id			= 'overlay';
	    	bod.appendChild(overlay);   	
	    }
	    overlay.style.display = 'block';
    		
		addExpressionDialog.style.pixelLeft = 250;
		addExpressionDialog.style.pixelTop = 200;
		addExpressionDialog.style.display = 'block';
		
		var expressionTableInput = document.getElementById('expression_table');
		if (expressionTableInput)
		{
			expressionTableInput.value = table;
		}
		var expressionAttriubteInput = document.getElementById('expression_attribute');
		if (expressionAttriubteInput)
		{
			expressionAttriubteInput.value = attribute;
		}		
	}	
}

function AddExpression()
{
	var addExpressionDialog = document.getElementById('addExpression');
	if (addExpressionDialog)
	{
		var overlay = document.getElementById('overlay');
	    if (overlay != null)
	    {
	    	var bod 			= document.getElementsByTagName('body')[0];
	    	bod.removeChild(overlay);   	
	    }
    		
		// get the condition
		var table = '';
		var attribute = '';
		var expresion = '';
		var expresionSelect = document.getElementById('expression_name');
		if (expresionSelect)
		{
			expresion = expresionSelect.value;
		}
		
		var expressionTableInput = document.getElementById('expression_table');
		if (expressionTableInput)
		{
			table = expressionTableInput.value;
		}
		var expressionAttriubteInput = document.getElementById('expression_attribute');
		if (expressionAttriubteInput)
		{
			attribute = expressionAttriubteInput.value;
		}		
		
		var mapping = {table: table, attribute: attribute, alias: '', expression: expresion};
		selectedFields.push(mapping);
		addExpressionDialog.style.display = 'none';
		updateOutlineSelect();
	}
}

function addToGroupBy(selectOulineDivId)
{
	var selectOutlineDiv = document.getElementById(selectOulineDivId);

	if (selectOutlineDiv)
	{
		var table =	selectOutlineDiv.getAttribute('table');
		var attribute =	selectOutlineDiv.getAttribute('attribute');		
		// find the mapping correspond with the att
		for(var i=0; i<selectedFields.length; i++) 
		{
			var mapping = selectedFields[i];
			if (table == mapping.table && attribute == mapping.attribute)
			{
				groupByFields.push(mapping);
				break;
			}
		}
	}
	
	updateOutlineGroupBy();
}

function addToOrderBy(selectOulineDivId)
{
	var selectOutlineDiv = document.getElementById(selectOulineDivId);

	if (selectOutlineDiv)
	{
		var table =	selectOutlineDiv.getAttribute('table');
		var attribute =	selectOutlineDiv.getAttribute('attribute');		
		
		// find the mapping correspond with the att
		for(var i=0; i<selectedFields.length; i++) 
		{
			var mapping = selectedFields[i];
			if (table == mapping.table && attribute == mapping.attribute)
			{
				orderByFields.push(mapping);
				break;
			}
		}
	}	
	updateOutlineOrderBy();
}

function removeSelectedField(selectOulineDivId)
{
	var selectOutlineDiv = document.getElementById(selectOulineDivId);

	if (selectOutlineDiv)
	{
		var table =	selectOutlineDiv.getAttribute('table');
		var attribute =	selectOutlineDiv.getAttribute('attribute');		
		
		// find the mapping correspond with the att
		var updateSelectedFields = new Array();
		for(var i=0; i<selectedFields.length; i++) 
		{
			var mapping = selectedFields[i];
			if (table == mapping.table && attribute == mapping.attribute)
			{

			}
			else
			{
				updateSelectedFields.push(mapping);
			}
		}
 		selectedFields = updateSelectedFields;
	}	
	updateOutlineSelect();	
}

function removeGroupByField(groupByOulineDivId)
{
	var groupByOutlineDiv = document.getElementById(groupByOulineDivId);

	if (groupByOutlineDiv)
	{
		var table =	groupByOutlineDiv.getAttribute('table');
		var attribute =	groupByOutlineDiv.getAttribute('attribute');		
		
		// find the mapping correspond with the att
		var updateGroupByFields = new Array();
		for(var i=0; i<groupByFields.length; i++) 
		{
			var mapping = groupByFields[i];
			if (table == mapping.table && attribute == mapping.attribute)
			{

			}
			else
			{
				updateGroupByFields.push(mapping);
			}
		}
 		groupByFields = updateGroupByFields;
	}	
	updateOutlineGroupBy();	
}

function removeOrderByField(orderByOulineDivId)
{
	var orderByOutlineDiv = document.getElementById(orderByOulineDivId);

	if (orderByOutlineDiv)
	{
		var table =	orderByOutlineDiv.getAttribute('table');
		var attribute =	orderByOutlineDiv.getAttribute('attribute');		
		
		// find the mapping correspond with the att
		var updateOrderByFields = new Array();
		for(var i=0; i<orderByFields.length; i++) 
		{
			var mapping = orderByFields[i];
			if (table == mapping.table && attribute == mapping.attribute)
			{

			}
			else
			{
				updateOrderByFields.push(mapping);
			}
		}
 		orderByFields = updateOrderByFields;
	}	
	updateOutlineOrderBy();	
}

function AddConditionCancel()
{
	var overlay = document.getElementById('overlay');
    if (overlay != null)
    {
    	var bod 			= document.getElementsByTagName('body')[0];
    	bod.removeChild(overlay);   	
    }
    
	var addConditionWhereDialog = document.getElementById('addConditionWhere');
	if (addConditionWhereDialog)
	{
		addConditionWhereDialog.style.display = 'none';
	}  
	var addConditionHavingDialog = document.getElementById('addConditionHaving');
	if (addConditionHavingDialog)
	{
		addConditionHavingDialog.style.display = 'none';
	}    
	var addExpressionDialog = document.getElementById('addExpression');
	if (addExpressionDialog)
	{
		addExpressionDialog.style.display = 'none';
	}    
    
    
    return false;
}


function updateSelectedFields(_this)
{
	var pNode = _this.parentNode;
	if (pNode)
	{			
		var table = '';
		// get the table id contains this check box
		var	ppNode = pNode.parentNode;
		if (ppNode)
		{
			var pppNode = ppNode.parentNode;
			if (pppNode)
			{
				table = pppNode.id;
			}
		}

		if (table.length > 0)
		{
			var attId = pNode.id;
			
			var attCb = document.getElementById(attId + '_cb');
			var attribute = attId.substring(table.length + 1);
			if (attCb)
			{
				if (attCb.checked)
				{
					// add this mapping to the list
					var aliasLb = document.getElementById(attId + '_lb');
					if (aliasLb)
					{
						var alias = aliasLb.innerHTML;						
						var mapping = {table: table, attribute: attribute, alias: alias, expression: -1};
						selectedFields.push(mapping);
					}
				}
				else
				{
					// remove this mapping from list
					var updateSelectedFields = new Array();
					for(var i=0; i<selectedFields.length; i++) 
					{
						var mapping = selectedFields[i];
						if (mapping.table == table && mapping.attribute == attribute && mapping.expression == -1)
						{
							// do nothing						
						}
						else
						{
							updateSelectedFields.push(mapping);
						}						
					}
					selectedFields = updateSelectedFields;
				}
			}
		}
	}	
	
	updateOutlineSelect ();
	
	//debugselectedFields();
}

function debugselectedFields ()
{
	var debugStr = '';
	for(var i=0; i<selectedFields.length; i++) 
	{
		var mapping = selectedFields[i];
		debugStr += '{' + mapping.table + ', ' + mapping.attribute + ', ' + mapping.alias + '}\n';
	}
	
	alert(debugStr);
}

function updateOutlineSelect ()
{
	var outlineSelectDiv = document.getElementById('query_outline_select');
	if (outlineSelectDiv)
	{
		outlineSelectDiv.innerHTML = 'Select';
		for(var i=0; i<selectedFields.length; i++) 
		{
			var mapping = selectedFields[i];			
			var selectDiv = document.createElement('DIV');
			selectDiv.id = 'outline_select_' + mapping.table + '_' + mapping.attribute;
			selectDiv.setAttribute('table', mapping.table);
			selectDiv.setAttribute('attribute', mapping.attribute);			
			selectDiv.style.paddingLeft = '10px';
			selectDiv.style.width = '500px';
			if (mapping.expression && mapping.expression > -1)
			{
				// get the expression name
				var expressionName = EXPRESSION_NAMES[mapping.expression];
				selectDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')';
				initContextMenuForOutlineSelect_2(selectDiv, false);
			}
			else
			{				
				selectDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + mapping.table + '.' + mapping.attribute +
					' as ' + mapping.alias;	
				initContextMenuForOutlineSelect_1(selectDiv, false);			
				
			}
			outlineSelectDiv.appendChild(selectDiv);
		}
	}
}

function updateOutlineFrom ()
{
	var outlineFromDiv = document.getElementById('query_outline_from');
	if (outlineFromDiv)
	{
		outlineFromDiv.innerHTML = 'From';
		for(var i=0; i<selectedTables.length; i++) 
		{
			var tableId = selectedTables[i];
			var fromDiv = document.createElement('DIV');
			fromDiv.style.paddingLeft = '10px';
			fromDiv.style.width = '500px';
			fromDiv.innerHTML = '<font color="#009900">' +
					BULLET_CHAR +
					' </font>' + tableId + ' ' + tableId;
			outlineFromDiv.appendChild(fromDiv);
		}
	}
}

function updateOutlineWhere ()
{
	var outlineWhereDiv = document.getElementById('query_outline_where');
	if (outlineWhereDiv)
	{
		outlineWhereDiv.innerHTML = 'Where';
		for(var i=0; i<whereConditions.length; i++) 
		{
			var whereClause = whereConditions[i];
			var whereDiv = document.createElement('DIV');
			whereDiv.style.paddingLeft = '10px';
			whereDiv.style.width = '500px';
			whereDiv.innerHTML = '<font color="#009900">' +
					BULLET_CHAR + ' </font>' + whereClause;
			outlineWhereDiv.appendChild(whereDiv);
		}
	}
}

function updateOutlineHaving ()
{
	var outlineHavingDiv = document.getElementById('query_outline_having');
	if (outlineHavingDiv)
	{
		outlineHavingDiv.innerHTML = 'Having';
		for(var i=0; i<havingConditions.length; i++) 
		{
			var havingClause = havingConditions[i];
			var havingDiv = document.createElement('DIV');
			havingDiv.style.paddingLeft = '10px';
			havingDiv.style.width = '500px';
			havingDiv.innerHTML = '<font color="#009900">' +
					BULLET_CHAR + ' </font>' + havingClause;
			outlineHavingDiv.appendChild(havingDiv);
		}
	}
}

function updateOutlineGroupBy()
{
	var outlineGroupByDiv = document.getElementById('query_outline_groupby');
	if (outlineGroupByDiv)
	{
		outlineGroupByDiv.innerHTML = 'Group by';
		for(var i=0; i<groupByFields.length; i++) 
		{
			var mapping = groupByFields[i];			
			var groupByDiv = document.createElement('DIV');
			groupByDiv.id = 'outline_groupby_' + mapping.table + '_' + mapping.attribute;
			groupByDiv.setAttribute('table', mapping.table);
			groupByDiv.setAttribute('attribute', mapping.attribute);
			groupByDiv.style.paddingLeft = '10px';
			groupByDiv.style.width = '500px';
			if (mapping.expression && mapping.expression > -1)
			{
				// get the expression name
				var expressionName = EXPRESSION_NAMES[mapping.expression];
				groupByDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')';					
			}
			else
			{				
				groupByDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + mapping.table + '.' + mapping.attribute;				
				
			}
			initContextMenuForOutlineGroupBy(groupByDiv, false);
			outlineGroupByDiv.appendChild(groupByDiv);
		}
	}	
}

function updateOutlineOrderBy()
{
	var outlineOrderByDiv = document.getElementById('query_outline_orderby');
	if (outlineOrderByDiv)
	{
		outlineOrderByDiv.innerHTML = 'Order by';
		for(var i=0; i<orderByFields.length; i++) 
		{
			var mapping = orderByFields[i];			
			var orderByDiv = document.createElement('DIV');
			orderByDiv.id = 'outline_orderby_' + mapping.table + '_' + mapping.attribute;
			orderByDiv.setAttribute('table', mapping.table);
			orderByDiv.setAttribute('attribute', mapping.attribute);
			orderByDiv.style.paddingLeft = '10px';
			orderByDiv.style.width = '500px';
			if (mapping.expression && mapping.expression > -1)
			{
				// get the expression name
				var expressionName = EXPRESSION_NAMES[mapping.expression];
				orderByDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')';					
			}
			else
			{				
				orderByDiv.innerHTML = '<font color="#009900">' +
						BULLET_CHAR +
						' </font>' + mapping.table + '.' + mapping.attribute;				
				
			}
			initContextMenuForOutlineOrderBy(orderByDiv, false);
			outlineOrderByDiv.appendChild(orderByDiv);
		}
	}	
}

function renderSQLBoard()
{
	// restore the content of query holder
	var queryHolderFrame = document.getElementById('query_holder_frame');
	if (queryHolderFrame)
	{
		queryHolderFrame.innerHTML = '';
		var sqlArea = document.createElement('TEXTAREA');
		sqlArea.rows = 30;
		sqlArea.cols = 84;
		sqlArea.value = generateSQL();
		queryHolderFrame.appendChild(sqlArea);
		
		// add copy to clipboard button
		var clipboardButtonDiv = document.createElement('DIV');
		clipboardButtonDiv.style.textAlign = 'right';
		clipboardButtonDiv.style.paddingTop = '10px';
		clipboardButtonDiv.style.paddingRight = '10px';
		var clipboardButton = document.createElement('INPUT');
		clipboardButton.type = "button";
		clipboardButton.value = "Copy To Clipboard";
		clipboardButton.onclick = function() {copyToClipboard(sqlArea.value);};		
		clipboardButtonDiv.appendChild(clipboardButton);
		queryHolderFrame.appendChild(clipboardButtonDiv);
	}	
}

function generateSQL()
{
	var sql = '';
	
	// build Select clause
	sql += 'SELECT \n';
	var comma = ',';
	for(var i=0; i<selectedFields.length; i++) 
	{
		if (i == selectedFields.length -1)
		{
			comma = '';
		}
		var mapping = selectedFields[i];
		if (mapping.expression && mapping.expression > -1)
		{
			// get the expression name
			var expressionName = EXPRESSION_NAMES[mapping.expression];
			sql += '\t' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')' + comma + ' \n';					
		}
		else
		{				
			sql += '\t' + mapping.table + '.' + mapping.attribute + ' as ' + mapping.alias + comma + ' \n';					
		}
		
	}
	comma = ',';
	if (selectedTables.length > 0)
	{
		// build From clause
		sql += 'FROM \n';
		for(var i=0; i<selectedTables.length; i++) 
		{
			if (i == selectedTables.length -1)
			{
				comma = '';
			}			
			var	tableId = selectedTables[i];
			sql += '\t' + tableId + ' ' + tableId + comma + ' \n';
		}
	}
	
	// build Where clause		
	if (whereConditions.length > 0)
	{
		sql += 'WHERE \n';
		for(var i=0; i<whereConditions.length; i++) 
		{
			var	whereClause = whereConditions[i];
			if (i == 0)
			{
				sql += '\t' + whereClause + ' \n';
			}
			else
			{
				sql += '\tAND ' + whereClause + ' \n';
			}
		}
	}
	comma = ',';
	// build Group by clause
	if (groupByFields.length > 0)
	{
		sql += 'GROUP BY \n';
		for(var i=0; i<groupByFields.length; i++) 
		{
			if (i == groupByFields.length -1)
			{
				comma = '';
			}
			var mapping = groupByFields[i];
			if (mapping.expression && mapping.expression > -1)
			{
				// get the expression name
				var expressionName = EXPRESSION_NAMES[mapping.expression];
				sql += '\t' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')' + comma + ' \n';					
			}
			else
			{				
				sql += '\t' + mapping.table + '.' + mapping.attribute + comma + ' \n';					
			}			
		}
	}
	
	// build Having clause
	if (havingConditions.length > 0)
	{
		sql += 'HAVING \n';
		for(var i=0; i<havingConditions.length; i++) 
		{
			var	havingClause = havingConditions[i];
			if (i == 0)
			{
				sql += '\t' + havingClause + ' \n';
			} 
			else
			{
				sql += '\tAND ' + havingClause + ' \n';
			}
		}
	}	
	comma = ',';
	// build Order by clause
	if (orderByFields.length > 0)
	{
		sql += 'ORDER BY \n';
		for(var i=0; i<orderByFields.length; i++) 
		{
			if (i == orderByFields.length -1)
			{
				comma = '';
			}
			var mapping = orderByFields[i];
			if (mapping.expression && mapping.expression > -1)
			{
				// get the expression name
				var expressionName = EXPRESSION_NAMES[mapping.expression];
				sql += '\t' + expressionName + '(' + mapping.table + '.' + mapping.attribute + ')' + ' ASC' + comma + ' \n';					
			}
			else
			{				
				sql += '\t' + mapping.table + '.' + mapping.attribute  + ' ASC' + comma + ' \n';					
			}			
		}
	}	
	return sql;
}

function copyToClipboard(s)
{
	if( window.clipboardData && clipboardData.setData )
	{
		clipboardData.setData("Text", s);
	}
	else
	{
		// You have to sign the code to enable this or allow the action in about:config by changing
		user_pref("signed.applets.codebase_principal_support", true);
		netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

		var clip = Components.classes['@mozilla.org/widget/clipboard;[[[[1]]]]'].createInstance(Components.interfaces.nsIClipboard);
		if (!clip) return;

		// create a transferable
		var trans = Components.classes['@mozilla.org/widget/transferable;[[[[1]]]]'].createInstance(Components.interfaces.nsITransferable);
		if (!trans) return;

		// specify the data we wish to handle. Plaintext in this case.
		trans.addDataFlavor('text/unicode');

		// To get the data from the transferable we need two new objects
		var str = new Object();
		var len = new Object();

		var str = Components.classes["@mozilla.org/supports-string;[[[[1]]]]"].createInstance(Components.interfaces.nsISupportsString);

		var copytext=meintext;

		str.data=copytext;

		trans.setTransferData("text/unicode",str,copytext.length*[[[[2]]]]);

		var clipid=Components.interfaces.nsIClipboard;

		if (!clip) return false;

		clip.setData(trans, null, clipid.kGlobalClipboard);	   
	}
}

// context menus
var contextMenuObj;
var MSIE = navigator.userAgent.indexOf('MSIE')?true:false;
var navigatorVersion = navigator.appVersion.replace(/.*?MSIE (\d\.\d).*/g,'$1')/1;	
var activeContextMenuItem = false;
function highlightContextMenuItem()
{
	this.className='contextMenuHighlighted';
}

function deHighlightContextMenuItem()
{
	this.className='';
}

function showContextMenuField(e)
{
  	var contextMenuSource = this;
	contextMenuObj = document.getElementById('contextMenu');
	
	// build the menu
	if (contextMenuSource)
	{
		// get the table of the element from grand pa
		var pElement = contextMenuSource.parentNode;
		var table = '';
		var attribute = '';
		if (pElement)
		{
			var ppElement = pElement.parentNode;
			if (ppElement)
			{
				table = ppElement.id;			
				if (contextMenuSource.id && contextMenuSource.id.indexOf(table) >= 0) 
				{
					attribute = contextMenuSource.id.substring(table.length + 1);
				}				
			}
		}	
		var alias = '';
		var attLb = document.getElementById(table + '_' + attribute + "_lb")
		if (attLb)
		{
			alias = attLb.innerHTML;
	
		}
				
		if (contextMenuObj)
		{
			contextMenuObj.innerHTML = '';
			contextMenuObj.style.display = 'block';
			// create menu items
			var menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			var aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){selectField(table, attribute, alias); return false;};
			
			var fieldExisted = false;
			// check if attribute exit to change to unselect link
			for(var i=0; i<selectedFields.length; i++) 
			{
				var mapping = selectedFields[i];
				if (mapping.table == table && mapping.attribute == attribute)
				{
					fieldExisted = true;
					break;
				}
			}
			if (fieldExisted)
			{
				aTag.innerHTML = "Unselect";
			}
			else
			{
				aTag.innerHTML = "Select";
			}
			
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);	
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){popupAddWhereDialog(table, attribute); return false;};
			aTag.innerHTML = "Add Where condition";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);	
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){popupAddHavingDialog(table, attribute); return false;};
			aTag.innerHTML = "Add Having condition";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){popupAddExpressionDialog(table, attribute); return false;};
			aTag.innerHTML = "Add Expression";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);				
	
			contextMenuObj.style.display = 'none';
			
		  	if(activeContextMenuItem)activeContextMenuItem.className='';
		  	if(document.all)e = event;
		  	var xPos = e.clientX;
		  	if(xPos + contextMenuObj.offsetWidth > (document.documentElement.offsetWidth-20)){
		   		xPos = xPos + (document.documentElement.offsetWidth - (xPos + contextMenuObj.offsetWidth)) - 20; 
		  	}
		  
		  	var yPos = e.clientY;
		  	if(window.document.body.scrollTop > 0)
		    {
		      	yPos = (window.screen.Height) ? e.clientY + window.document.body.scrollTop -20 : e.clientY -20;
		    }
		    else if (window.pageYOffset) 
		    {
		      	yPos = (window.pageYOffset > 0) ? e.clientY + window.pageYOffset -20 : e.clientY -20;
		    }
		    else
		    { 	yPos = e.clientY -20; }
		  	/* * */
		  	contextMenuObj.style.left = xPos + 'px';
		  	contextMenuObj.style.top = yPos + 'px';
		  	contextMenuObj.style.display='block';			
		}				
	}	
 
  	return false; 
}

function showContextMenuOutlineSelect_1(e)
{
  	var contextMenuSource = this;
	contextMenuObj = document.getElementById('contextMenu');
	
	// build the menu
	if (contextMenuSource)
	{
		// get the selected field in outline
		var outlineSelectedFieldId = contextMenuSource.id;
				
		if (contextMenuObj)
		{
			contextMenuObj.innerHTML = '';
			contextMenuObj.style.display = 'block';
			// create menu items
			var menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			var aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){addToGroupBy(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Add to group-by";
			
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);	
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){addToOrderBy(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Add to order-by";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);										
	
			contextMenuObj.style.display = 'none';
			
		  	if(activeContextMenuItem)activeContextMenuItem.className='';
		  	if(document.all)e = event;
		  	var xPos = e.clientX;
		  	if(xPos + contextMenuObj.offsetWidth > (document.documentElement.offsetWidth-20)){
		   		xPos = xPos + (document.documentElement.offsetWidth - (xPos + contextMenuObj.offsetWidth)) - 20; 
		  	}
		  
		  	var yPos = e.clientY;
		  	if(window.document.body.scrollTop > 0)
		    {
		      	yPos = (window.screen.Height) ? e.clientY + window.document.body.scrollTop -20 : e.clientY -20;
		    }
		    else if (window.pageYOffset) 
		    {
		      	yPos = (window.pageYOffset > 0) ? e.clientY + window.pageYOffset -20 : e.clientY -20;
		    }
		    else
		    { 	yPos = e.clientY -20; }
		  	/* * */
		  	contextMenuObj.style.left = xPos + 'px';
		  	contextMenuObj.style.top = yPos + 'px';
		  	contextMenuObj.style.display='block';			
		}				
	}	
 
  	return false; 
}

function showContextMenuOutlineSelect_2(e)
{
  	var contextMenuSource = this;
	contextMenuObj = document.getElementById('contextMenu');
	
	// build the menu
	if (contextMenuSource)
	{
		// get the selected field id in outline
		var outlineSelectedFieldId = contextMenuSource.id;
				
		if (contextMenuObj)
		{
			contextMenuObj.innerHTML = '';
			contextMenuObj.style.display = 'block';
			// create menu items
			var menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			var aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){addToGroupBy(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Add to group-by";
			
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);	
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){addToOrderBy(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Add to order-by";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);				
			
			menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){removeSelectedField(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Remove";
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);														
	
			contextMenuObj.style.display = 'none';
			
		  	if(activeContextMenuItem)activeContextMenuItem.className='';
		  	if(document.all)e = event;
		  	var xPos = e.clientX;
		  	if(xPos + contextMenuObj.offsetWidth > (document.documentElement.offsetWidth-20)){
		   		xPos = xPos + (document.documentElement.offsetWidth - (xPos + contextMenuObj.offsetWidth)) - 20; 
		  	}
		  
		  	var yPos = e.clientY;
		  	if(window.document.body.scrollTop > 0)
		    {
		      	yPos = (window.screen.Height) ? e.clientY + window.document.body.scrollTop -20 : e.clientY -20;
		    }
		    else if (window.pageYOffset) 
		    {
		      	yPos = (window.pageYOffset > 0) ? e.clientY + window.pageYOffset -20 : e.clientY -20;
		    }
		    else
		    { 	yPos = e.clientY -20; }
		  	/* * */
		  	contextMenuObj.style.left = xPos + 'px';
		  	contextMenuObj.style.top = yPos + 'px';
		  	contextMenuObj.style.display='block';			
		}				
	}	
 
  	return false; 
}

function showContextMenuOutlineGroupBy(e)
{
  	var contextMenuSource = this;
	contextMenuObj = document.getElementById('contextMenu');
	
	// build the menu
	if (contextMenuSource)
	{
		// get the selected field id in outline
		var outlineSelectedFieldId = contextMenuSource.id;
				
		if (contextMenuObj)
		{
			contextMenuObj.innerHTML = '';
			contextMenuObj.style.display = 'block';
			// create menu items
			var menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			var aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){removeGroupByField(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Remove";
			
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);																					
	
			contextMenuObj.style.display = 'none';
			
		  	if(activeContextMenuItem)activeContextMenuItem.className='';
		  	if(document.all)e = event;
		  	var xPos = e.clientX;
		  	if(xPos + contextMenuObj.offsetWidth > (document.documentElement.offsetWidth-20)){
		   		xPos = xPos + (document.documentElement.offsetWidth - (xPos + contextMenuObj.offsetWidth)) - 20; 
		  	}
		  
		  	var yPos = e.clientY;
		  	if(window.document.body.scrollTop > 0)
		    {
		      	yPos = (window.screen.Height) ? e.clientY + window.document.body.scrollTop -20 : e.clientY -20;
		    }
		    else if (window.pageYOffset) 
		    {
		      	yPos = (window.pageYOffset > 0) ? e.clientY + window.pageYOffset -20 : e.clientY -20;
		    }
		    else
		    { 	yPos = e.clientY -20; }
		  	/* * */
		  	contextMenuObj.style.left = xPos + 'px';
		  	contextMenuObj.style.top = yPos + 'px';
		  	contextMenuObj.style.display='block';			
		}				
	}	
 
  	return false; 
}

function showContextMenuOutlineOrderBy(e)
{
  	var contextMenuSource = this;
	contextMenuObj = document.getElementById('contextMenu');
	
	// build the menu
	if (contextMenuSource)
	{
		// get the selected field id in outline
		var outlineSelectedFieldId = contextMenuSource.id;
				
		if (contextMenuObj)
		{
			contextMenuObj.innerHTML = '';
			contextMenuObj.style.display = 'block';
			// create menu items
			var menuItemSelect = document.createElement('LI');
			menuItemSelect.onmouseover = highlightContextMenuItem;
			menuItemSelect.onmouseout = deHighlightContextMenuItem;			
			var aTag = document.createElement('A');
			aTag.href = "#";
			aTag.onclick = function(){removeOrderByField(outlineSelectedFieldId); return false;};
			aTag.innerHTML = "Remove";
			
			if(MSIE && navigatorVersion<6)
			{
				aTag.style.paddingLeft = '15px';
				//aTag.style.width = (aTag.offsetWidth - 30) + 'px';
			}else
			{
				aTag.style.paddingLeft = '30px';
				//aTag.style.width = (aTag.offsetWidth - 60) + 'px';
			}	
			menuItemSelect.appendChild(aTag);	
			contextMenuObj.appendChild(menuItemSelect);																					
	
			contextMenuObj.style.display = 'none';
			
		  	if(activeContextMenuItem)activeContextMenuItem.className='';
		  	if(document.all)e = event;
		  	var xPos = e.clientX;
		  	if(xPos + contextMenuObj.offsetWidth > (document.documentElement.offsetWidth-20)){
		   		xPos = xPos + (document.documentElement.offsetWidth - (xPos + contextMenuObj.offsetWidth)) - 20; 
		  	}
		  
		  	var yPos = e.clientY;
		  	if(window.document.body.scrollTop > 0)
		    {
		      	yPos = (window.screen.Height) ? e.clientY + window.document.body.scrollTop -20 : e.clientY -20;
		    }
		    else if (window.pageYOffset) 
		    {
		      	yPos = (window.pageYOffset > 0) ? e.clientY + window.pageYOffset -20 : e.clientY -20;
		    }
		    else
		    { 	yPos = e.clientY -20; }
		  	/* * */
		  	contextMenuObj.style.left = xPos + 'px';
		  	contextMenuObj.style.top = yPos + 'px';
		  	contextMenuObj.style.display='block';			
		}				
	}	
 
  	return false; 
}

function hideContextMenu(e)
{
	if(document.all) e = event;
	if(e.button==0 && !MSIE){
		
	}else{
		if (contextMenuObj)
		{
			contextMenuObj.style.display='none';
		}
	}
}

function initContextMenuForField(element, selected)
{
	if (element != null)
	{
		element.oncontextmenu = showContextMenuField;
		document.documentElement.onclick = hideContextMenu;
	}
}

function initContextMenuForOutlineSelect_1(element, selected)
{
	if (element != null)
	{
		element.oncontextmenu = showContextMenuOutlineSelect_1;
		document.documentElement.onclick = hideContextMenu;
	}
}

function initContextMenuForOutlineSelect_2(element, selected)
{
	if (element != null)
	{
		element.oncontextmenu = showContextMenuOutlineSelect_2;
		document.documentElement.onclick = hideContextMenu;
	}
}

function initContextMenuForOutlineGroupBy(element, selected)
{
	if (element != null)
	{
		element.oncontextmenu = showContextMenuOutlineGroupBy;
		document.documentElement.onclick = hideContextMenu;
	}
}

function initContextMenuForOutlineOrderBy(element, selected)
{
	if (element != null)
	{
		element.oncontextmenu = showContextMenuOutlineOrderBy;
		document.documentElement.onclick = hideContextMenu;
	}
}


/Old function*/
function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}
// trim both end
function trim(str) {
    var trimmed = str.replace(/^\s+|\s+$/g, '') ;
    return trimmed;
}

function hasWhiteSpace (str)
{
	if (str != null)
	{
		if (str.match(/[\s]/g))
		{
			return true;
		}
	}
	
	return false;
}