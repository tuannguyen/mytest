/**
 *  JavaScript page parser for AjAX 
 *  
 *
 *  @author dcarlyle
 *  @author $Author: cvsuser $
 *  @version $Revision: 1.3 $
 * 
 *  $Id: Exp $
 */
// Handle Data
//check if arrays have data, default is false
	var working = false; //normaltag
	var condition = false; //condition tag
	var CurrentWorking  = false;
	var TwinWorking= false;
	var DictionWorking =false;
	var DictionShortWorking=false;
	var CaveatWorking =false;
	var CaveatShortWorking = false;/**/
	var FAQWorking =false;
 	var ShortFAQWorking = false;/**/
        var BonusWaiverWork = false;
/*End check*/
/*Get Ajax object*/
	var http = createXmlHttpRequestObject(); // for nomaltag
	var conditionHttp = createXmlHttpRequestObject(); //for condition tag
	var tableHttp = createXmlHttpRequestObject(); //for table tag
	//var CurrentHttp = createXmlHttpRequestObject(); //No longer used
	var Twin  = createXmlHttpRequestObject();
	var dictionaryHttp= createXmlHttpRequestObject();
	var caveatHttp = createXmlHttpRequestObject();
	var FAQHttp = createXmlHttpRequestObject();
	var httpContributary= createXmlHttpRequestObject();
	var AddressHttp=createXmlHttpRequestObject();
    var WaiverHttp=createXmlHttpRequestObject();
	var httpBookMark = createXmlHttpRequestObject(); // for nomaltag
/*End*/
var ns6 = document.getElementById && ! document.all;
/*Start get normal tag*/
function startGetData(name , update_field){
		
		working = true;
		
 		//generate the xml content
        //alert('startGetData: ' + name);

		var xml = generateXML(name, update_field);
		//alert(xml);
		// Build the URL to connect to
		var url = staticURL + escape(xml);   //goes to Java Servlet
		
		http.open("POST", url);
		
		http.onreadystatechange = handleResponse;
		
		http.send(null);
		
	}
/*End*/
/*Start get normal tag*/
function startGetConditionTagData(name , update_field){
		condition = true;
		
		//generate the xml content
		var xml = generateCondXML(name, update_field);

		//alert('xml: ' + xml);
		// Build the URL to connect to
		var url = staticCondURL + escape(xml);   //goes to Java Servlet
		document.getElementById(update_field).style.display = "none";
		
		conditionHttp.open("POST", url);
		
		conditionHttp.onreadystatechange = handleConditionTagResponse;
		
		conditionHttp.send(null);
		
	}	

function generateCondXML(expression, update_field)
{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";
	    xml += "	<Member>\n";
		xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
	    xml += "		<Refno>" + crefno+ "</Refno>\n";
	    xml += "	</Member>\n";
		xml += "	<Type>CONDITIONAL</Type>\n";	    
		xml += "	<RequestedTag>" + expression + "</RequestedTag>\n";
	    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
		xml += "</AjaxParameterRequest>\n";
		return xml;	
}
/*Start get conditional tag*/
function startGetTableTagData(table, update_field){
		//generate the xml content
		var xml = generateTableXML(table, update_field);

		//alert(xml);
		// Build the URL to connect to
		var url = staticTableURL + escape(xml);   //goes to Java Servlet

		tableHttp.open("POST", url);
		
		tableHttp.onreadystatechange = handleTableTagResponse;
		
		tableHttp.send(null);
		
	}	

function generateTableXML(table, update_field)
{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";
	    xml += "	<Member>\n";
		xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
	    xml += "		<Refno>" + crefno+ "</Refno>\n";
	    xml += "	</Member>\n";
		xml += "	<Type>TABLE</Type>\n";	    
		xml += "	<RequestedTag>" + table + "</RequestedTag>\n";	
	    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
		xml += "</AjaxParameterRequest>\n";
		return xml;	
}

	/*End*/
/*Handle Request Dictionary*/
function DictionaryGetData(scheme,phrase, updateField)
{
	DictionWorking = true;
	
	//generate the xml content
	var xml = generateDictionaryXML(scheme, phrase, updateField);
	
	// Build the URL to connect to
	var url = staticDicURL + escape(xml);   //goes to Java Servlet

	//alert(xml);
	//alert(url);
	dictionaryHttp.open("POST", url);
	
	dictionaryHttp.onreadystatechange = DictionaryHandleResponse;
	
	dictionaryHttp.send(null);
}
/*End*/
/*handle Caveat tag*/
function CaveAtGetData(scheme,phrase)
{
	CaveatWorking = true;
	
	//generate the xml content
	var xml = generateCaveAtXML(scheme, phrase);
	
	// Build the URL to connect to
	var url = staticCaveURL + escape(xml);   //goes to Java Servlet
	
	caveatHttp.open("POST", url);
	
	caveatHttp.onreadystatechange = CaveatHandleResponse;
	
	caveatHttp.send(null);
}
/*End*/
/*handle FAQ tag*/
function FAQGetData(scheme,phrase)
{
	FAQWorking = true;
	
	//generate the xml content
	var xml = generateFAQXML(scheme, phrase);
	//alert(xml);
	// Build the URL to connect to
	var url = faqUrl + escape(xml);   //goes to Java Servlet
	
	FAQHttp.open("POST", url);
	
	FAQHttp.onreadystatechange = FAQHandleResponse;
	
	FAQHttp.send(null);
}
/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}
/*End*/

function handleResponse(){
	if (http.readyState == 4) {
						
		// Check that a successful server response was received
      	if (http.status == 200) {
			var response = createResponseXML(http.responseText);					
			//alert(http.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var fieldType;
				var value ="";
                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
                    			ErrorTag.push(Array(fieldName, elementToUpdate));
                    			if (errorCount <= limitErrorCount) {
						value = "<span><img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"waiting\" /></span>";
        				} else {
            					value = "<span style='color: red'>" + response.getElementsByTagName("Message")[0].firstChild.nodeValue + "</span>";
        				}
					fieldType = "";
				} else{
        				if(response.getElementsByTagName("Value")[0].childNodes.length>0)
        				{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
						value = value.replace(/&lt;/g, '<');
						value = value.replace(/&gt;/g, '>');
						//alert(value);
						if (value == "null") value = "";
                    			}
					var tlength = response.getElementsByTagName("Type")[0].childNodes.length;
					var i = 0;
					for (i=0; i<tlength; i++){
						if(response.getElementsByTagName("Type")[0].childNodes[i].nodeType == 1)
							fieldType = response.getElementsByTagName("Type")[0].childNodes[i].nodeName;
					}
				}

				populateFieldValue(elementToUpdate, value, fieldType);  
  
				if(request_array.length) {
					// get next element off the array.
					var next = request_array[0];
					request_array.shift();
			
					// set this as late as possible to avoid conflict
					working = false;
					startGetData(next[0], next[1]);
				} else {
					// nothing in the queue so signal complete.
					working = false;
				}                                        
			}
			catch(err){
				alert("XML Parser error." );
				//hide spinning image
				//visiWhizzy(elementToUpdate, false);				
			}	
			visiWhizzy(elementToUpdate, false);			
		}else {		
			//hide spinning image
			visiWhizzy(elementToUpdate, false);
	    	}                            
	}
			
}
/*End*/
/*Handle condition tag response from server*/
function handleConditionTagResponse()
{
	if (conditionHttp.readyState == 4) {												
		// Check that a successful server response was received
      	if (conditionHttp.status == 200) 
		{
			var response = createResponseXML(conditionHttp.responseText);
			//alert(conditionHttp.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var fieldType;
				var value = "FALSE";

				var error = response.getElementsByTagName("Error").length;
				
				if(error > 0)
				{			
                    CondErrorTag.push(Array("<![CDATA[" + fieldName + "]]>", elementToUpdate));
				}
				else 
				{
					if(response.getElementsByTagName("Value")[0].childNodes.length>0)
					{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
					}					
				}
				
				var divElement = document.getElementById(elementToUpdate);					
				if(value.toLowerCase() == "true")
				{		
					divElement.style.display = ""
				}
				else
				{
					divElement.style.display = "none"
				}

				if(conditionTagArray.length) {
				// Pop the top element off the array.
					var next = conditionTagArray.pop();
			    
					// set this as late as possible to avoid conflict
					condition = true;
					startGetConditionTagData(next[0], next[1]);
				} else {
					// nothing in the queue so signal complete.
					condition = false;
				}
			}
			catch(err)
			{
				alert(err);
			}	
		}			
	}
}
/*End*/
function handleTableTagResponse()
{
	if (tableHttp.readyState == 4) {												
		// Check that a successful server response was received
      	if (tableHttp.status == 200) 
		{
			var response = createResponseXML(tableHttp.responseText);
			//alert(tableHttp.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var value = "";

				var error = response.getElementsByTagName("Error").length;
				
				if(error > 0){
					value = "<span style='color: red'>" + fieldName + "</span>";
				}
				else 
				{
					if(response.getElementsByTagName("Value")[0].childNodes.length>0)
					{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
						// apply style to the table
//						value = value.replace(/<table>/g, '<table class="datatable">');
//						value = value.replace(/<th/g, '<th class="label"');
//						value = value.replace(/<td>/g, '<td align=center>');						
					}					
				}
				
				var appliedElement = document.getElementById(elementToUpdate);				
				if (appliedElement.getAttribute('style'))
				{		
					appliedElement.removeAttribute('style');
				}
				
				// Replace content to display table
				var content = appliedElement.innerHTML;
				var startReplace = content.indexOf(startMetaDataChars);
				var endReplace = content.indexOf(stopMetaDataChars);
				if (startReplace >= 0 && endReplace > startReplace)
				{
					content = content.substring(0, startReplace) + value + content.substring(endReplace + stopMetaDataChars.length);						
				}				
				appliedElement.innerHTML = content;

				// get next tag
				if(tableTagArray.length) {
					var next = tableTagArray.shift();
					startGetTableTagData(next[0], next[1]);
				}
			}
			catch(err)
			{
				alert(err);
			}	
		}			
	}
}
/*End*/
/*Handle Dictionary data response from server*/
function DictionaryHandleResponse()
{
	if (dictionaryHttp.readyState == 4) {
							
		// Check that a successful server response was received

      	if (dictionaryHttp.status == 200) 
		{
			var tmpResponse = dictionaryHttp.responseText;
			var response = createResponseXML(tmpResponse );
			//alert(tmpResponse);
			try{
				if(tmpResponse.indexOf("<Message>")<0)
				{
					// parse the XML to get the values from XML abov
					var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var TitleElement = response.getElementsByTagName("Title")[0].firstChild.nodeValue;
					var UpdateElement = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;						
					var value = response.getElementsByTagName("Definition")[0].firstChild.nodeValue;
					var ContentID = UpdateElement + "_C";

					var dictElement = document.getElementById(UpdateElement);
					if (dictElement .getAttribute('style'))
					{		
						dictElement.removeAttribute('style');
					}											
					dictElement.innerHTML = "<span class='fakelink' style='cursor:pointer; cursor:hand; font-size:1em;' onclick=ViewTag('"+ContentID+"',this); onmouseout=HideTag('"+ContentID+"');>"+ TitleElement+"</span>";					

					var x=document.getElementById("DIVTEMPLATE");
					x.innerHTML += "<span class='dictionary_definition' style='display:none;' id="+ContentID+"><span class=bookmark_component_holder><span class=bookmark_value>"+value+"</span></span></span>";		
				}
				else
				{
					// parse the XML to get the values from XML abov
					var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var TitleElement = response.getElementsByTagName("Title")[0].firstChild.nodeValue;
					var UpdateElement = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;					
					var value = response.getElementsByTagName("Message")[0].firstChild.nodeValue;

					var dictElement = document.getElementById(UpdateElement);											
					dictElement.innerHTML = "<font color=#FF0000>"+ErrorElement+"</font>";
				}
			}
			catch(err){
				//alert("XML Parser error." );
				alert(err);
				//hide spinning image
				//visiWhizzy("", false);
			}
			
			//hide spinning image
			visiWhizzy("", false);
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);
			
	    }
		
		if(dictionaryArr.length) {
			// Pop the top element off the array.
			var next = dictionaryArr[0];
			dictionaryArr.shift();
    
			// set this as late as possible to avoid conflict
			DictionWorking = true;
			DictionaryGetData(next[0], next[1], next[2]);
		} 
		else {
			// nothing in the queue so signal complete.
			DictionWorking = false;
		}
	}
}
/*End*/
/*FAQ handle*/
function FAQHandleResponse()
{
	if (FAQHttp.readyState == 4) {
							
		// Check that a successful server response was received
      	if (FAQHttp.status == 200) 
		{
            var response = createResponseXML(FAQHttp.responseText);
			var tmpResponse = FAQHttp.responseText;

			try{					
				var foundPoint= -1;
				var endPoint  = -1;
                var error = response.getElementsByTagName("Error").length;
				
				if(error > 0){							
					value = "<span style='color: red'>" + response.getElementsByTagName("Message")[0].firstChild.nodeValue + "</span>";
					fieldType = "";						
				}
				else
				{
					
					foundPoint = tmpResponse.indexOf("<Anchor>");
					endPoint = tmpResponse.indexOf("</Anchor>");
					var AnchorElement = tmpResponse.substring(foundPoint + "<Anchor>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Question>");
					endPoint = tmpResponse.indexOf("</Question>");
					var QuestionElement = tmpResponse.substring(foundPoint + "<Question>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Phrase>");
					endPoint = tmpResponse.indexOf("</Phrase>");
					var PhraseElement = tmpResponse.substring(foundPoint + "<Phrase>".length, endPoint);
                    foundPoint = tmpResponse.indexOf("<Title>");
					endPoint = tmpResponse.indexOf("</Title>");
					var TitleElement = tmpResponse.substring(foundPoint + "<Title>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Scheme>");
					endPoint = tmpResponse.indexOf("</Scheme>");
					var SchemeElement = tmpResponse.substring(foundPoint + "<Scheme>".length, endPoint);

					var value ;
					
					var valueID=SchemeElement+""+PhraseElement;											
					var x=document.getElementById(valueID);
					if(SchemeElement.length>0)
					{
						SchemeElement=SchemeElement+"_";
					}
					SchemeElement=SchemeElement.toLowerCase();
					x.outerHTML="<a href="+ajaxurl+"/pl/faq/"+SchemeElement+"faq.jsp#"+AnchorElement+">"+TitleElement+"</a> ";		

				}
			}
			catch(err)
			{
				//alert("XML Parser error." );
				alert(err);
			}
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);			
	    }
		
		if(faqArray.length) {
			// Pop the top element off the array.
			var next = longFaqArray[0];
			faqArray.shift();
    
			// set this as late as possible to avoid conflict
			FAQWorking = true;
			FAQGetData(next[0], next[1]);
		}
		else {
			// nothing in the queue so signal complete.
			FAQWorking = false;
		}
		
	}
}
/*end*/
/*Caveat handle*/
function CaveatHandleResponse()
{
	if (caveatHttp.readyState == 4) {
							
		// Check that a successful server response was received
      	if (caveatHttp.status == 200) 
		{
			var tmpResponse = caveatHttp.responseText;

			try{
				if(tmpResponse.indexOf("<Message>")<=0)
				{
					// parse the XML to get the values from XML abov
					//var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					//var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var foundPoint= -1;
					var endPoint  = -1;

					foundPoint = tmpResponse.indexOf("<Phrase>");
					endPoint = tmpResponse.indexOf("</Phrase>");
					var PhraseElement = tmpResponse.substring(foundPoint + "<Phrase>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Title>");
					endPoint = tmpResponse.indexOf("</Title>");
					var TitleElement = tmpResponse.substring(foundPoint + "<Title>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Scheme>");
					endPoint = tmpResponse.indexOf("</Scheme>");
					var SchemeElement = tmpResponse.substring(foundPoint + "<Scheme>".length, endPoint);

					var value ;
					
					var valueID=SchemeElement+""+PhraseElement;
					valueID= noWhiteSpace(valueID);	
					foundPoint=tmpResponse.indexOf("<Description>");
					endPoint = tmpResponse.indexOf("</Description>");

					value =tmpResponse.substring(foundPoint+"<Description>".length,endPoint);
																
										
					var x=document.getElementById(valueID);

					x.outerHTML="<b>"+TitleElement +"</b><br><p style='width:500'>"+value+"</p>";		

				}
			}
			catch(err){
				//alert("XML Parser error." );
				alert(err);
				//hide spinning image
				visiWhizzy("", false);
			}
			
			//hide spinning image
			visiWhizzy("", false);
							
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);
			
	    }
		
		if(caveatArr.length) {
			// Pop the top element off the array.
			var next = caveatArr[0];
			caveatArr.shift();
    
			// set this as late as possible to avoid conflict
			CaveatWorking = false;
			CaveAtGetData(next[0], next[1]);
		}
		else {
			// nothing in the queue so signal complete.
			CaveatWorking = false;
		}
		
	}
}
/*end*/
/*Old function*/
function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

function AddressHandle()
{

			if (AddressHttp.readyState == 4) {
								
				// Check that a successful server response was received

		      	     if (AddressHttp.status == 200) 
				{

					   var kq=AddressHttp.responseText;
					   var foundPoint= -1;
						var endPoint  = -1;
					   //document.getElementById("title").style.visibility="hidden";
				       foundPoint = kq.indexOf("<Success>");
						
                                   if(foundPoint >0)
                                   {
                                        
										endPoint = kq.indexOf("</Success>");
                                        var SuccessElement = kq.substring(foundPoint + "<Success>".length, endPoint);
                                        var fa = document.getElementById("full_address_holder");
		                        var at = document.getElementById("address_thanks_holder");
		                        fa.style.display = "none";
                         		at.style.display = "block";
                                   }
                                   else {
                                        foundPoint = kq.indexOf("<Error>");
                                          if(foundPoint >0)
                                          {
                                            endPoint = kq.indexOf("</Error>");
                                            var ErrorElement = kq.substring(foundPoint + "<Error>".length, endPoint);
                                		var fa = document.getElementById("full_address_holder");
                                 		var er = document.getElementById("address_error_holder");
                                  		fa.style.display = "none";
                                                er.style.display = "block";
                                		er.innerHTML = ErrorElement ;
	                                   }
				    }
			}
           }
}
/*End*/
/*Bookmark handle*/
function HandleBookmarkAjax(desc, refp){

		var description = desc;
		var refpage = refp;
		try
               {
		// Build the URL to connect to
		var url = ajaxurl + "/BookmarkMember?description=" + description + "&refpage=" + refpage;
		httpBookMark.open("POST", url);
		
		httpBookMark.onreadystatechange = HandleBookMarkAjaxFunc;
		
		httpBookMark.send(null);
}catch(err)
{
alert(err.description);
}
}
/*End*/
/*Handle Ajax Book mark*/
function HandleBookMarkAjaxFunc()
{
if (httpBookMark.readyState == 4) {
								
		// Check that a successful server response was received
	    if (httpBookMark.status == 200) {
			if(httpBookMark.responseText=="true")
			{
				alert("Test Member Updated !");
			}
			document.getElementById('bookmark_display').style.display='none';
		}
	}
}
/*Use for testmember*/
function TestLoaderAjax(desc){

	visiWhizzy("Test_Member_", true);

	var myHttp = createXmlHttpRequestObject();
	
	var url= ajaxurl + "/TestMemberLoader?description=" + desc;

	myHttp.open("POST", url);
	
	myHttp.onreadystatechange = function(){

		var textHTML = "";

		var selectHTML = "<select id='Test_Member_PoS' name='Test_Member_PoS' onChange='TestLoaderAjax(document.getElementById(\"Test_Member_PoS\").value);window.location.reload();' >";


		if (myHttp.readyState == 4) {
							
			// Check that a successful server response was received
	      	if (myHttp.status == 200) {

			var response = createResponseXML(myHttp.responseText);
	    
		    	var rootNode = response.documentElement;
	    
        		var length = rootNode.childNodes.length;

			var i=0;
		    		    
			for(i=0; i<length; i++){
			
		    	var member = rootNode.childNodes[i];

				
				if(member.nodeType ==1){
					
			    	var atts = member.childNodes;
		    	

		    		var j=0;

		    		var description = "";
	    			var selected = "";		    	
		    		
	    			for( j=0; j<atts.length; j++){
	    				if(atts[j].nodeName == "Selected"){
							if(atts[j].firstChild.nodeValue == "True"){
								selected = " selected ";
							}else{
								selected = "";
							}
		    			}else if(atts[j].nodeName == "Description" ){
	    			
		    				description = atts[j].firstChild.nodeValue;

			    		}
			    		
		    		}
		    		
			    	textHTML += "<option value=\"" + description + "\"" + selected + " >" + description + "</option>";
			    	
		    	}		    	
        	}			
	        
		        document.getElementById("Test_Member_PoS").outerHTML = selectHTML + textHTML + "</select>";

			}
			
		}	
	
	}
	
	myHttp.send(null);
}

/*end*///

/*DateTime*/
var MONTH_NAMES=new Array('Jan','Feb','Mar','Apr','May','June','July','August','Sep','Oct','Nov','Dec','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
function LZ(x) {return(x<0||x>9?"":"0")+x}

// ------------------------------------------------------------------
// isDate ( date_string, format_string )
// Returns true if date string matches format of format string and
// is a valid date. Else returns false.
// It is recommended that you trim whitespace around the value before
// passing it to this function, as whitespace is NOT ignored!
// ------------------------------------------------------------------
function isDate(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
	}
function LZ(x) {return(x<0||x>9?"":"0")+x}
	// ------------------------------------------------------------------
// formatDate (date_object, format)
// Returns a date in the output format specified.
// The format string uses the same abbreviations as in getDateFromFormat()
// ------------------------------------------------------------------
function formatDate(date,format) {
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["NNN"]=MONTH_NAMES[M+11];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["E"]=DAY_NAMES[E+7];
	value["EE"]=DAY_NAMES[E];
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
		}
		
	return result;
	}
	
// ------------------------------------------------------------------
// Utility functions for parsing in getDateFromFormat()
// ------------------------------------------------------------------
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
		}
	return null;
	}
	
// ------------------------------------------------------------------
// getDateFromFormat( date_string , format_string )
//
// This function takes a date string and a format string. It matches
// If the date string matches the format string, it returns the 
// getTime() of the date. If it does not match, it returns 0.
// ------------------------------------------------------------------
function getDateFromFormat(val,format) {

	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=1;
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	var ampm="";
	
	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
				}
			}
		else if (token=="MMM"||token=="NNN"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					if (token=="MMM"||(token=="NNN"&&i>11)) {
						month=i+1;
						if (month>12) { month -= 12; }
						i_val += month_name.length;
						break;
						}
					}
				}
			if ((month < 1)||(month>12)){return 0;}
			}
		else if (token=="EE"||token=="E"){
			for (var i=0; i<DAY_NAMES.length; i++) {
				var day_name=DAY_NAMES[i];
				if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
					i_val += day_name.length;
					break;
					}
				}
			}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return 0; }
			}
		else { if (date > 28) { return 0; } }
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return 0; }
		}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh=hh-0+12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	
	return newdate.getTime();
	/**/
	}

// ------------------------------------------------------------------
// parseDate( date_string [, prefer_euro_format] )
//
// This function takes a date string and tries to match it to a
// number of possible date formats to get the value. It will try to
// match against the following international formats, in this order:
// y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
// M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
// d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
// A second argument may be passed to instruct the method to search
// for formats like d/M/y (european format) before M/d/y (American).
// Returns a Date object or null if no patterns match.
// ------------------------------------------------------------------
function parseDate(val) {
	var preferEuro=(arguments.length==2)?arguments[1]:false;
	generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d');
	monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
	dateFirst =new Array('d/M/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
	var checkList=new Array('generalFormats',preferEuro?'dateFirst':'monthFirst',preferEuro?'monthFirst':'dateFirst');
	var d=null;
	for (var i=0; i<checkList.length; i++) {
		var l=window[checkList[i]];
		for (var j=0; j<l.length; j++) {
			d=getDateFromFormat(val,l[j]);
			if (d!=0) { return new Date(d); }
			}
		}
	return null;
	}
