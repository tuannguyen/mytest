<%@ page import = "java.util.*,org.opencms.jsp.*,org.opencms.file.*,org.opencms.main.*" %>
<%
CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);

//check if users logged in
CmsJspBean bean=new CmsJspBean();
bean.init(pageContext, request, response);
CmsObject cmso = bean.getCmsObject();
CmsUser user = cmso.getRequestContext().currentUser();

// Get navigation info
String pageTitle = cms . property("Title");
CmsJspNavBuilder navigation = cms . getNavigation();
List navItems = navigation . getNavigationTreeForFolder(0, 3);
Iterator i = navItems . iterator();
// Images for the navigation
String linkunselected = cms . link("/system/modules/com.bp.pensionline.template/resources/gfx/link_arrow.gif");
String linkselected = cms . link("/system/modules/com.bp.pensionline.template/resources/gfx/link_arrow_selected.gif");

out . println("<ul>");

String afirst = "first_menu ";
String first = afirst;
while (i . hasNext()) {
	CmsJspNavElement navElement = (CmsJspNavElement) i . next();
	int level = navElement . getNavTreeLevel();
	String link = cms . link(navElement . getResourceName());
	String title = navElement . getTitle();
	String navigationText = navElement . getNavText();
	
	if (user.isGuestUser() && ("Self Services".equalsIgnoreCase(navigationText) || "UserId self service".equalsIgnoreCase(navigationText) || "Password self service".equalsIgnoreCase(navigationText))){
	    continue; //trick, hide "Self Services" link for Guest, but allow them to see inside
	}
	// navigation position
	float position = navElement . getNavPosition();

	String menu_class = "submenu" + level;
	String selected = "";

	if (pageTitle . equals(title)) {
		selected = " class='selected_menu_item'";
	}

	if (level == 0) {
		menu_class = first + "rootmenu";
		first = "";
	} else {
		first = afirst;
	}

	out . println("<li  class='" + menu_class + "'><img src=\"" + linkunselected + "\" alt='menu indicator' /><a" + selected + " href=\"" + link + "\">" + navigationText + "</a></li>");
}

out . println("</ul>");
%>