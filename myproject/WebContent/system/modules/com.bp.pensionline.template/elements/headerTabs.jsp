<%@ page import="org.opencms.main.*"%>
<%@ page import="org.opencms.search.*"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.security.*"%>
<%@ page import="java.util.*"%>
<%@page import="org.opencms.jsp.CmsJspActionElement"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<% 
	CmsJspActionElement cmsJspAction = new CmsJspActionElement(pageContext, request, response);

	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request, response);
	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cms = bean.getCmsObject();
	// get the values from configuration file opencms.properties
	//Map map=cms.getConfigurations();
	CmsUser user = cms.getRequestContext().currentUser();
%>


<jsp:useBean id="search" scope="request" class="org.opencms.search.CmsSearch">
<jsp:setProperty name = "search" property="*"/>
<% 
	
	search.init(cmsJspAction.getCmsObject());     

%>
</jsp:useBean>
<%
	// Get the search manager
	CmsSearchManager searchManager = OpenCms.getSearchManager(); 

	Iterator k = searchManager.getIndexNames().iterator(); 
	String indexName = (String)k.next();
	
	String baseDir = "";	
	String requestUrl = request.getPathTranslated();
	if (requestUrl != null && requestUrl.indexOf(".war") > 0)
	{		
		requestUrl = requestUrl.substring(requestUrl.indexOf(".war") + 4);
		requestUrl = requestUrl.replace("\\", "/") + "/";
	}
	String homeTabClassName = "";
	String bpdocumentTabClassName = "";
	String mydetailTabClassName = "";
	
 	if(requestUrl == null || requestUrl.trim().equals("/") || requestUrl.trim().equals("//") || requestUrl.trim().equals("/index.html/")) 
	{
		homeTabClassName = "selected";
	}

 	if(requestUrl != null && requestUrl.indexOf(baseDir + "/schemes_bp/") >= 0) 
 	{
 		bpdocumentTabClassName = "selected";
	}
 	else if(requestUrl != null && requestUrl.indexOf(baseDir + "/mydetails/") >= 0) 
 	{
		mydetailTabClassName = "selected";
	}	

%>

 <ul>
  <li  class="<%=homeTabClassName%>" onmouseover="this.className='over'" onmouseout="this.className='<%=homeTabClassName%>'"><a href="<cms:link>/index.html</cms:link>">Home</a></li>
  <li  class="<%= bpdocumentTabClassName%>" onmouseover="this.className='over'" onmouseout="this.className='<%=bpdocumentTabClassName%>'"><a href="<cms:link>/schemes_bp/index.html</cms:link>">Scheme Documentation</a></li>
<%	
	if (!cms.userInGroup(user.getName(),"Guests") && !cms.userInGroup(user.getName(),"FORCE_CHANGE_PASSWORD")){
%>
  <li class="<%=mydetailTabClassName %>" onmouseover="this.className='over'" onmouseout="this.className='<%=mydetailTabClassName %>'"><a href="<cms:link>/mydetails/index.html</cms:link>">My details</a></li>
<%	
	}
%>  
  
</ul>
<div class='search' id="search_box">
	<form method="get" action="<cms:link>/result.jsp</cms:link>" name='frm'>
	  <span class='label'><label for='sbox'>Search:</label> </span>
	  <input type=text name='query' id='sbox' value='<%=(cmsJspAction.property("query") != null) ? cmsJspAction.property("query") : ""%>' /> 
	  <a class='goButton' onclick='frm.submit();'>Go</a>
	  <input type="hidden" name="index" value='<%=indexName%>'>
	</form>
</div>