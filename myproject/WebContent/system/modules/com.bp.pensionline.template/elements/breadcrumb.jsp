<%@ page import="java.util.*,org.opencms.jsp.*,org.opencms.workplace.*" %><%   


CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
// Get navigation info
String pageTitle = cms.property("Title") ;
CmsJspNavBuilder navigation = cms.getNavigation();
// Get the current file 
String fileName = cms.getCmsObject().getRequestContext().getUri();


List navItems = navigation.getNavigationBreadCrumb(1, 4);
//List navItems1 = navigation.getNavigationTreeForFolder(0,4);
Iterator i = navItems.iterator();
// Images for the navigation
 String arrow = cms.link("/system/modules/com.bp.pensionline.template/resources/gfx/link_arrow.gif");

// Link to home page
String homepage = cms.link("/index.html");


	out.println("<div><a href=\"" + homepage + "\">Home</a></div>");
	 
	while( i.hasNext() ) 
	{
	CmsJspNavElement navElement = ( CmsJspNavElement )i.next();
	int level = navElement.getNavTreeLevel();
	String link = cms.link( navElement.getResourceName() );
	//String title = navElement.getTitle();
	String title = navElement.getNavText();

        // navigation position
	float position = navElement.getNavPosition();
	String file = navElement.getNavText();

	out.println("<div><img src=\"" + arrow + "\" alt=\"Navigation arrow\" /><a href=\"" + link + "\">" + title + "</a></div>");
	
	}

	if (!fileName.endsWith("index.html")){
	out.println("<div><img src=\"" + arrow + "\">" + pageTitle + "</div>");
	}
%>

<%
// START - Login workplace messages
        /** The locale to use for display, this will not be the workplace locale, but the browser locale. */
        java.util.Locale m_locale = org.opencms.main.OpenCms.getWorkplaceManager().getDefaultLocale();
        // display login message if required
        org.opencms.db.CmsLoginMessage loginMessage = org.opencms.main.OpenCms.getLoginManager().getLoginMessage();

        String msgLogin = "";
	boolean msgLoginAvailable = false;
       
        if ((loginMessage != null) && (loginMessage.isActive())) {
            msgLoginAvailable = true;
            
            /*
            if (loginMessage.isLoginForbidden()) {
                // login forbidden for normal users, current user must be Administrator
                msgLogin= org.opencms.workplace.Messages.get().container(
                        org.opencms.workplace.Messages.GUI_LOGIN_SUCCESS_WITH_MESSAGE_2,
                    loginMessage.getMessage(),
                    new java.util.Date(loginMessage.getTimeEnd())).key(m_locale);
            } else {
                // just display the message
                msgLogin= loginMessage.getMessage();
            }*/
            
            msgLogin= loginMessage.getMessage();
            msgLogin=(org.opencms.util.CmsStringUtil.escapeJavaScript(msgLogin));
	}
%>
<script type="text/javascript">
   <%-- is there a login message? --%>
   function isLoginMessage()
   {
     return('<%=msgLoginAvailable %>');
   }

   <%-- get login message - so you can put it where you need it.--%>
   function getLoginMessage()
   {
     return('<%=msgLogin%>');
   }

   function alertLoginMessage()
   {
<%
if ((loginMessage != null) && (loginMessage.isActive())) {
%>
     alert('<%=msgLogin%>');
<%
}
%>
//
   }

</script>
<%-- END - Login workplace messages --%>
