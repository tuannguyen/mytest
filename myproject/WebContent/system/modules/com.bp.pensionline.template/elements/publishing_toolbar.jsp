<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.*"%>
<%@ page import="com.bp.pensionline.publishing.handler.*"%>
<%@ page import="com.bp.pensionline.constants.Environment"%>
<%@ page import="com.bp.pensionline.publishing.dto.ReleasePageDTO"%>
<%@ page import="com.bp.pensionline.publishing.constants.Constant"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<script type='text/javascript'>
var toolbar_displayed = false;
function tb_findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function tb_findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }
</script>
<script>	
	/*** Cookies control **/
	if( BrowserDetect.browser == "Explorer" && (BrowserDetect.version == "6" || BrowserDetect.version == "7"))
	{
	
	}
	else
	{
		//delete_cookie("browser_supported");
		//if ( !get_cookie ( "browser_supported" ) )
		//{
		  //var browser_confirmed = alert( "You are using a browser that is not currently supported by PensionLine. This means that the site may not display correctly and some of the functional components will not work. Please bear with us while we improve the compatibility." );		
		  //set_cookie ( "browser_supported", "true");
		//}		
	}
</script>
<%
	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request, response);
	CmsJspBean bean = new CmsJspBean();
	bean.init(pageContext, request, response);
	
	CmsObject cmsObject = bean.getCmsObject();
	
	CmsUser cmsUser = cmsObject.getRequestContext().currentUser();
	
	if (cmsUser != null && !cmsObject.getRequestContext().currentProject().isOnlineProject())
	{
		String userName = cmsLogin.getUserName();
		
		// Process for deployer
		if (userName != null && cmsObject.userInGroup(userName, Environment.PUBLISHING_DEPLOYER_GROUP))
		{
			boolean hasPagesToDeploy = EditorLandingHandler.hasDeployablePages();
			String deploy_icon = "";
			String deploy_onclick = "";
			if (hasPagesToDeploy)
			{
				deploy_icon = "publish.gif";
				deploy_onclick = "buttonDeployPressAction('DeployAlChanges', this);";
			}
			else
			{
				deploy_icon = "desat_publish.gif";
			}
%>
	<div class='toolbar' id='publishing_toolbar_deploy'>
	<span>
	<img src="<cms:link>../resources/gfx/desat_revoke.gif</cms:link>" alt="Revoke this page from publishing list" />
	</span>
	<span>
	<img src="<cms:link>../resources/gfx/desat_undo_changes.gif</cms:link>" alt="Undo all changes of this page" />
	</span>
	<span>
	<img src="<cms:link>../resources/gfx/desat_send4pub.gif</cms:link>" alt="Send this page for checking" />
	</span>
	<span>
	<img src="<cms:link>../resources/gfx/desat_approve.gif</cms:link>" alt="Pass content change checking for this page" />
	</span>
	<span>
	<img src="<cms:link>../resources/gfx/desat_approve.gif</cms:link>" alt="Approve publishing request for this page" />
	</span>
	<span>
	<img src="<cms:link>../resources/gfx/desat_reject.gif</cms:link>" alt="Reject changes made to this page" />
	</span>	
<% 
			if (hasPagesToDeploy)
			{
%>	
	<a href="" onclick="<%=deploy_onclick%> return false;">
	<img src="<cms:link>../resources/gfx/<%=deploy_icon%></cms:link>" alt="Deploy all changes" />
	</a>
<% 
			}
			else
			{
%>
	<span>
	<img src="<cms:link>../resources/gfx/<%=deploy_icon%></cms:link>" alt="Deploy all changes" />
	</span>
<% 
			}
%>
	</div>
	
	<div class="dialog" id="publish_reason_deploy">
	      <div class="dialog_title">Enter a reason</div>
	      <div class="component_holder">
			<textarea id='reason_text_deploy'></textarea>
	      </div>
	      <div class="dialog_title">Insert release id</div>
	      <div class="component_holder">
			<input type="text" id='deploy_name' class="dialog_text"></input>
	      </div>
	      <div class="component_label"> </div>
	      <div class="component_value">
	         <input class="goButton" id="dopublish" type="button" name="dobookmark" value="Go" onclick="ptb_confirmDeployAction(document.getElementById('reason_text_deploy').value, document.getElementById('deploy_name').value);" />
	         &nbsp;
	         <input class="goButton" id="cancelpublish" type="button" value="Cancel" onclick="ptb_cancelDeployAction();" /> 
	      </div>
	</div>
	<script type='text/javascript'>
	document.getElementById('publishing_toolbar_deploy').style.display = 'block';
	var ptb_deployAction = "";
	var ptb_deployMandatory = true;
	
	function ptb_confirmDeployAction(reason, package_name) {
		if(ptb_deployMandatory && (reason == null || reason.length == 0 || package_name == null || package_name.length == 0)) {
			
			
			("Please input reason and release id");
			return false;
		}
		ptb_submitRequest(-1, 'All approved pages', ptb_deployAction, reason, package_name);
		document.getElementById('publish_reason_deploy').style.display='none';
	}
	
	function ptb_cancelDeployAction() {
		ptb_deployAction = "";
		document.getElementById('publish_reason_deploy').style.display='none';
	}
	
	function buttonDeployPressAction (action, this1)
	{
		ptb_deployAction = action;
		document.getElementById('publish_reason_deploy').style.display='block';
		document.getElementById('publish_reason_deploy').style.pixelTop=parseInt(tb_findPosY(this1) + 40);
		document.getElementById('publish_reason_deploy').style.pixelLeft=parseInt(tb_findPosX(this1) - 60);			
	}		
	</script>		
<%
		}						
		if (userName != null && 
				(cmsObject.userInGroup(userName, Environment.PUBLISHING_EDITOR_GROUP) ||
					cmsObject.userInGroup(userName, Environment.PUBLISHING_REVIEWER_GROUP)||
					cmsObject.userInGroup(userName, Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)||						
					cmsObject.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP))
			) 
		{				
			String pageURI = cmsObject.getRequestContext().getUri();
			int pageId = -1;
			//System.out.println("pageURI = " + pageURI);
			ReleasePageDTO releasePageDTO = EditorLandingHandler.processCurrentPublishingPage(request, pageURI);
			String pageStatus = null;
			String bugId = null;
			String actionHis = "";
			String mode = "";
			String selfEdited = "N";
			String bugIdReadOnly = "readonly='readonly' ";
			String currentUserId = cmsUser.getId().toString();
			
			if (releasePageDTO != null)	// release page now in the workflow, go on process the toolbar
			{
				pageStatus = releasePageDTO.getStatus();
				bugId = releasePageDTO.getBugId();
				pageId = releasePageDTO.getPageId();				
				mode = releasePageDTO.getMode();
				
				if (bugId == null)
				{
					bugId = "";
				}
				
				actionHis = EditorLandingHandler.getReleasePageActionHistories (pageId);
				String modeLabel = "(" + releasePageDTO.getMode() + ")";
				selfEdited = releasePageDTO.getSelfEdited();
			
				//System.out.println("Publishing toolbar userName: " + userName);
				//System.out.println("pageURI: " + pageURI);
				//System.out.println("PublishingPageStatus: " + pageStatus);
				//System.out.println("Page selfEdited: " + selfEdited);

		
				// Button state: 0: Disabled; 1: Enabled; 2: Disabled not passed; 3: Disabled passed
				int revokeChange = 0;
				int undoChange = 0;
				int send4Review = 0;
				int	passChecking = 0;
				int approveChange = 0;
				int rejectChange = 0;
				int pauseDeploy = 0;
				int deployAllChanges = 0;
				
				// specify button state for Deployers. Deployers have no involes in content processing
	
					
				// specify button state for Author
				if (userName != null && 
						(cmsObject.userInGroup(userName,Environment.PUBLISHING_EDITOR_GROUP) || 
						cmsObject.userInGroup(userName,Environment.PUBLISHING_EDITOR_REVIEWER_GROUP))
						&& pageStatus != null)
				{
					// Allow author to undo all changes at any state of publishing
					undoChange = 1;
					revokeChange = 1;

					if (pageStatus.equals("Editing"))
					{
						//revokeChange = 0;
						//undoChange = 1;
						send4Review = 1;
						//rejectChange = 0;
						bugIdReadOnly = "";
					}										
					
					if (mode != null && mode.equals("Impacted"))
					{
						revokeChange = 0;
						send4Review = 0;
						undoChange = 0;
					}
				}
				
				String rejectAlt = "Reject content change";
				// specify button state for Reviewer. Not overide state of Author in case user has > 1 roles
				if (userName != null && 
						(cmsObject.userInGroup(userName,Environment.PUBLISHING_REVIEWER_GROUP) ||
						 cmsObject.userInGroup(userName,Environment.PUBLISHING_EDITOR_REVIEWER_GROUP))
						&& pageStatus != null)
				{
					if (pageStatus.equals("Edited"))
					{
						passChecking = 1;
						rejectChange = 1;
						rejectAlt = "Reject this content";
						
						// check if current user is the same as the one who submit the review request
						if (!EditorLandingHandler.checkPublishingActionValid (pageId, 
								Constant.PUBLISHING_ACTION_SEND4REVIEW, currentUserId))	
						{
							passChecking = 0;
							rejectChange = 0;
						}
					}
					
					if (mode != null && mode.equals("Impacted"))
					{
						rejectChange = 0;
					}					
				}	
				
				// specify button state for Authoriser
				if (userName != null && cmsObject.userInGroup(userName, Environment.PUBLISHING_AUTHORISER_GROUP) 
						&& pageStatus != null)
				{
					if (pageStatus.equals("Checked"))
					{
						approveChange = 1;
						rejectChange = 1;	
						rejectAlt = "Reject this content";
						// check if current user is the same as the one who submit the review request
						if (!EditorLandingHandler.checkPublishingActionValid (pageId, 
								Constant.PUBLISHING_ACTION_PASSCHECKING, currentUserId))	
						{
							approveChange = 0;
							rejectChange = 0;
						}					
					}
					else if (pageStatus.equals("Approved"))
					{
						pauseDeploy = 1;						
					}
					else if (pageStatus.equals("Paused"))
					{
						pauseDeploy = 2;						
					}
					if (mode != null && mode.equals("Impacted"))
					{
						pauseDeploy = 0;
					}					
				}
				
				String revokeChange_icon = "";
				String undoChange_icon = "";			
				String send4Review_icon = "";
				String passChecking_icon = "";
				String approveChange_icon = "";
				String rejectChange_icon = "";
				String pauseDeploy_icon = "";				
				
				String revokeChange_onclick = "";
				String undoChange_onclick = "";			
				String send4Review_onclick = "";
				String passChecking_onclick = "";
				String approveChange_onclick = "";
				String rejectChange_onclick = "";		
				String pauseDeploy_onclick = "";				
				
	//			System.out.println("passChecking: " + passChecking);
				if (revokeChange == 1)
				{
					revokeChange_icon = "revoke.gif";
					revokeChange_onclick = "buttonPressAction('RevokeChange', this)";
				}
				else
				{
					revokeChange_icon = "desat_revoke.gif";
				}
				
				if (undoChange == 1)
				{
					undoChange_icon = "undo_changes.gif";
					undoChange_onclick = "buttonPressAction('UndoChange', this)";
				}
				else
				{
					undoChange_icon = "desat_undo_changes.gif";
				}			
				
				if (send4Review == 1)
				{
					send4Review_icon = "send4pub.gif";
					send4Review_onclick = "buttonPressAction('Send4Review', this)";					
				}
				else
				{
					send4Review_icon = "desat_send4pub.gif";
				}
				
				if (passChecking == 1)
				{
					if (mode != null && mode.equals("Impacted"))
					{
						passChecking_icon = "approve_impact.gif";
					}
					else
					{
						passChecking_icon = "approve.gif";
					}
					passChecking_onclick = "buttonPressAction('PassChecking', this)";
				}
				else
				{
					if (mode != null && mode.equals("Impacted"))
					{
						passChecking_icon = "desat_approve_impact.gif";
					}
					else
					{
						passChecking_icon = "desat_approve.gif";
					}
				}			
				
				if (approveChange == 1)
				{
					if (mode != null && mode.equals("Impacted"))
					{
						approveChange_icon = "approve_impact.gif";
					}
					else
					{
						approveChange_icon = "approve.gif";
					}					
					
					approveChange_onclick = "buttonPressAction('ApproveChange', this)";
				}
				else
				{
					if (mode != null && mode.equals("Impacted"))
					{
						approveChange_icon = "desat_approve_impact.gif";
					}
					else
					{
						approveChange_icon = "desat_approve.gif";
					}						
				}					
				
				if (rejectChange == 1)
				{
					rejectChange_icon = "reject.gif";
					if (pageStatus != null && pageStatus.equals("Edited"))	// Reviewer reject
					{
						rejectChange_onclick = "buttonPressAction('RejectChange', this)";
					}
					else if (pageStatus != null && pageStatus.equals("Checked"))	// Authorisers reject
					{
						rejectChange_onclick = "buttonPressAction('UnapproveChange', this)";
					}
				}
				else
				{
					rejectChange_icon = "desat_reject.gif";
				}
				
				if (pauseDeploy == 1)
				{
					pauseDeploy_icon = "pause.gif";
					pauseDeploy_onclick = "buttonPressAction('PauseDeploy', this)";
				}
				else if (pauseDeploy == 2)
				{
					pauseDeploy_icon = "send4pub.gif";
					pauseDeploy_onclick = "buttonPressAction('UnpauseDeploy', this)";					
				}
				else
				{
					pauseDeploy_icon = "desat_pause.gif";
				}

%>

<div class='toolbar' id='publishing_toolbar'>
<% 
				if (revokeChange == 1)
				{
%>	
<a href="" onclick="<%= revokeChange_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= revokeChange_icon %></cms:link>" alt="Revoke this page from publishing list" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= revokeChange_icon %></cms:link>" alt="Revoke this page from publishing list" />
</span>
<% 
				}
				if (undoChange == 1)
				{
%>
<a href="" onclick="<%= undoChange_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= undoChange_icon %></cms:link>" alt="Undo all changes of this page" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= undoChange_icon %></cms:link>" alt="Undo all changes of this page" />
</span>
<% 
				}
				if (send4Review == 1)
				{
%>
<a href="" onclick="<%= send4Review_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= send4Review_icon %></cms:link>" alt="Send this page for checking" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= send4Review_icon %></cms:link>" alt="Send this page for checking" />
</span>
<% 
				}
				if (passChecking == 1)
				{
%>
<a href="" onclick="<%= passChecking_onclick %>; return false;">
<img src="<cms:link>../resources/gfx/<%= passChecking_icon %></cms:link>" alt="Pass content change checking for this page" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= passChecking_icon %></cms:link>" alt="Pass content change checking for this page" />
</span>
<% 
				}
				if (approveChange == 1)
				{
%>
<a href="" onclick="<%= approveChange_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= approveChange_icon %></cms:link>" alt="Approve publishing request for this page" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= approveChange_icon %></cms:link>" alt="Approve publishing request for this page" />
</span>
<% 
				}
				if (rejectChange == 1)
				{
%>
<a href="" onclick="<%= rejectChange_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= rejectChange_icon %></cms:link>" alt="<%=rejectAlt%>" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= rejectChange_icon %></cms:link>" alt="<%=rejectAlt%>" />
</span>
<% 
				}
				if (pauseDeploy == 1)
				{
%>
<a href="" onclick="<%= pauseDeploy_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= pauseDeploy_icon %></cms:link>" alt="Pause deployment of this page" />
</a>
<% 
				}
				else if (pauseDeploy == 2)
				{
%>
<a href="" onclick="<%= pauseDeploy_onclick %>;return false;">
<img src="<cms:link>../resources/gfx/<%= pauseDeploy_icon %></cms:link>" alt="Unpause deployment of this page" />
</a>
<% 
				} else
				{
%>
<span>
<img src="<cms:link>../resources/gfx/<%= pauseDeploy_icon %></cms:link>" alt="Pause deployment of this page" />
</span>
<% 
				}
%>
<a href="" onclick="buttonShowHisPressAction(this); return false;">
<img src="<cms:link>../resources/gfx/action_his.gif</cms:link>" alt="Show action history" />
</a>
</div>

<div class="dialog" id="publish_reason">
      <div class="dialog_title">Enter a reason</div>
      <div class="component_holder">
		<textarea id='reason_text'></textarea>
      </div>
 <% 
				if (mode != null && (mode.equals("Edited") || mode.equals("Deleted")))
				{
%>	
      <div class="dialog_title">Bug ID</div>
      <div class="component_holder">
		<input <%=bugIdReadOnly%> type="text" id='bug_id' class="dialog_text" value="<%=bugId%>"></input>
      </div>      
      <div class="component_label"> </div>
      <div class="component_value">
         <input class="goButton" id="dopublish" type="button" name="dobookmark" value="Go" onclick="ptb_confirmAction(document.getElementById('reason_text').value, document.getElementById('bug_id').value);" />
         &nbsp;
         <input class="goButton" id="cancelpublish" type="button" value="Cancel" onclick="ptb_cancelAction();" /> 
      </div>
 <% 
				}
				else if (mode != null && mode.equals("Impacted"))
				{					
%>	      
      <div class="component_label"> </div>
      <div class="component_value">
         <input class="goButton" id="dopublish" type="button" name="dobookmark" value="Go" onclick="ptb_confirmAction(document.getElementById('reason_text').value, '');" />
         &nbsp;
         <input class="goButton" id="cancelpublish" type="button" value="Cancel" onclick="ptb_cancelAction();" /> 
      </div>
<% 
				}				
%>	
</div>
<div class="dialog" id="publish_his">
      <div class="dialog_title">Action history <%=modeLabel%></div>
      <div class="component_holder">
		<textarea id='action_his' readonly="readonly"><%=actionHis%></textarea>
      </div>
      <div class="component_label"> </div>
      <div class="component_value">
         <input class="goButton" type="button" value="Ok" onclick="ptb_showHisCancel();" /> 
      </div>
</div>
<script type='text/javascript'>
var ptb_pageId = '<%=pageId%>';
var ptb_pageURI = '<%=pageURI%>';
/*
var url = document.URL;
var pageURIStart = url.indexOf('content/pl');
if (pageURIStart >= 0)
{
	 ptb_pageURI = url.substring(pageURIStart + 'content/pl'.length);
}
*/
<%
				if (pageId != -1)	// display the toolbar if page is in the workflow
				{
%>
document.getElementById('publishing_toolbar').style.display = 'block';
var depoyTB = document.getElementById('publishing_toolbar_deploy');
if (depoyTB != null)
{
	depoyTB.style.top = '85px';
}
<%
				}
%>
var ptb_action = "";
var ptb_mandatory = true;

function ptb_confirmAction(reason, bug_id) {
	if (ptb_action != 'Send4Review')
	{
		if(ptb_mandatory && (reason == null || reason.length == 0)) {
			alert("Please input a reason");
			return false;
		}
	}
	else
	{
		if(ptb_mandatory && (reason == null || reason.length == 0 || bug_id == null || bug_id.length == 0)) {
			alert("Please input a reason and bug ID");
			return false;
		}	
	}
	//alert(ptb_action+" in progress");
	ptb_submitRequest(ptb_pageId, ptb_pageURI, ptb_action, reason, bug_id);
	document.getElementById('publish_reason').style.display='none';
}

function ptb_cancelAction() {
	ptb_action = "";
	document.getElementById('publish_reason').style.display='none';
}

function buttonPressAction (action, this1)
{
	ptb_showHisCancel ();
	ptb_action = action;
	document.getElementById('publish_reason').style.display='block';
	document.getElementById('publish_reason').style.pixelTop=parseInt(tb_findPosY(this1) + 40);
	document.getElementById('publish_reason').style.pixelLeft=parseInt(tb_findPosX(this1) - 60);		
}

function buttonShowHisPressAction (this1)
{
	ptb_cancelAction();
	document.getElementById('publish_his').style.display='block';	
}

function ptb_showHisCancel ()
{
	document.getElementById('publish_his').style.display='none';	
}

</script>
<%	
			}
		}
	}
%>