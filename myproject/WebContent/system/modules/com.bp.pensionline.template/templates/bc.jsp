<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.security.*"%>
<%@ page import="java.util.*"%>
<%@ page import="org.opencms.main.*"%>
<%@ page import="org.opencms.search.*"%>
<%@ page import="com.bp.pensionline.constants.*"%>
<%@ page import="com.bp.pensionline.util.*"%>
<%@ page import="com.bp.pensionline.test.*"%>
<%@ page import="com.bp.pensionline.dao.MemberDao"%>
<%@page import="com.bp.pensionline.webstats.producer.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%
	response.setHeader("Cache-Control","no-cache, no-store"); //HTTP 1.1
	//response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0);
%>
<%
	org.opencms.jsp.CmsJspActionElement cmsJspAction = new CmsJspActionElement(pageContext, request, response);
    
    // Get the search manager
    CmsSearchManager searchManager = OpenCms.getSearchManager(); 
    
	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request, response);
	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cms = bean.getCmsObject();
	
   	CmsUser user = cms.getRequestContext().currentUser();
	//Call Webstat producer	
	WebstatsPageProducer.runStats(request);

	String requestedResource = request.getParameter("requestedResource");
	request.getSession().setAttribute("requestedResource",requestedResource);
    
    String uri=cmsLogin.getRequestContext().getUri();
	boolean isView=false;
	try{
 		CmsObject obj=SystemAccount.getAdminCmsObject();
 		obj.readProject("Online");
 		CmsPermissionSet permissionSet=cms.getPermissions(uri,user.getName());
 		String permissions=permissionSet.toString();
 		String read="+r";
 		String view="+v";


 		if (permissions.indexOf(view)==-1){
 			isView=false;
 			
 		}else{
 			isView=true;
 		}
 		
 	}catch(Exception ex){
 		System.out.println(ex);
 	//	ex.printStackTrace();
 		isView=false;
 	}

	//logged in?
	
	if (!isView && user.isGuestUser()){

			response.sendRedirect(cmsJspAction.link("/_login_ask.html"));
	}

	//cms get the current page 

	//Authenticated but no records

        //else logged in and also in a record	
%>


<cms:template element="head">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><cms:property name="Title" escapeHtml="true" /></title>
	<meta http-equiv="Cache-Control" content="no-cache, no-store">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Content-Type" content="text/xml; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
	<script type="text/javascript" src="<cms:link>../resources/scripts/browser.js</cms:link>"></script>
<script type="text/javascript" language="javascript">
	// IE7 is the development platform of choice
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version >= "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>../resources/style.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>../resources/style.print.ie.css</cms:link>' type='text/css' media='print' />");

	}
	// check if IE6 is detected, and degrade some features
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>../resources/style.ie6.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>../resources/style.print.ie6.css</cms:link>' type='text/css' media='print' />");
	
	}
	// check if Firefox is detected
	if( BrowserDetect.browser == "Firefox" ) {
		document.write("<link rel='stylesheet' href='<cms:link>../resources/style.firefox.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>../resources/style.print.css</cms:link>' type='text/css' media='print' />");                
	        
	}	   
	
	if( BrowserDetect.browser == "Chrome"||BrowserDetect.browser == "Mozilla" ) {
	        document.write("<link rel='stylesheet' href='<cms:link>../resources/style.chrome.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>../resources/style.print.css</cms:link>' type='text/css' media='print' />");    
	            
         } 

    // safari
	if( BrowserDetect.browser == "Safari") {
        document.write("<link rel='stylesheet' href='<cms:link>../resources/style.safari.css</cms:link>' type='text/css' media='screen, projection, print' />");
            document.write("<link rel='stylesheet' href='<cms:link>../resources/style.print.css</cms:link>' type='text/css' media='print' />");    
            
     }       
var ajaxurl="/content";
//var ajaxurl="/opencms705";
var bgroup = "XXX";
var crefno = "0000000";
</script>
<%if (!cms.getRequestContext().currentProject().isOnlineProject()) {%>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.js" ></script>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.dimensions.js" ></script>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.flydom.js" ></script>
<%}%>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/AjaxDataTagHandler.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/AjaxDataTagParser.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/BookmarkMember.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/xmldom.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/WallAjax.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/AjaxPublishingHandler.js</cms:link>" ></script>
<script language="JavaScript1.2" type="text/javascript" src="<cms:link>../resources/scripts/dialogs.js</cms:link>"></script>
<script language="javascript" type="text/javascript" src="<cms:link>../resources/scripts/onload.js</cms:link>" ></script>
<script language="JavaScript1.2" type="text/javascript" src="<cms:link>../resources/scripts/accessibility.js</cms:link>"></script>
<cms:editable />
</head>
<%
	if (!cms.getRequestContext().currentProject().isOnlineProject())
	{	
%>
<body>
<%
	}
	else
	{
%>
<body onLoad="onLoadHandler();" onUnload="onUnloadHandler();">
<%
	}
%>
<div id='container'>
<%
	org.opencms.db.CmsLoginMessage loginMessage = org.opencms.main.OpenCms.getLoginManager().getLoginMessage();
	if ((loginMessage != null) && (loginMessage.isActive())) 
	{	
%>
<div style="float: left; width: 100%;">
	<cms:include file="/_lockout_warning.html" element="bpblank"></cms:include>
</div>
<%		
	}
%>
<div id='headernav' class='non-print'>
<%
if (!cms.getRequestContext().currentProject().isOnlineProject())
{	
	if (cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) || 
		cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||
		cms.userInGroup(user.getName(), Environment.PUBLISHING_REVIEWER_GROUP) ||
		cms.userInGroup(user.getName(), Environment.PUBLISHING_AUTHORISER_GROUP) ||
		cms.userInGroup(user.getName(), Environment.PUBLISHING_DEPLOYER_GROUP)
	){
%>

<a href="<cms:link>/publishing/index.html</cms:link>">Recent changes</a> &nbsp; | &nbsp;
<%	
	}
	if (cms.userInGroup(user.getName(),"Superusers")){
%>
<a href="<cms:link>/_logout_publishing.jsp</cms:link>">Exit publishing</a> &nbsp; | &nbsp; 	
<%
	}	
	if (cms.userInGroup(user.getName(),"Authors") ||
		cms.userInGroup(user.getName(),"Publishers") ||
		cms.userInGroup(user.getName(),"Testers") ||		
		cms.userInGroup(user.getName(),"Administrators") ||
		cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
		cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
		) {
%>
<a onclick="popupHelp('https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page'); return false;" href="https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page">online help</a> &nbsp; | &nbsp;
<%
	}
	if (cms.userInGroup(user.getName(),"Authors") ||
			cms.userInGroup(user.getName(),"Publishers") ||
			cms.userInGroup(user.getName(),"Administrators") ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
			)
	{
%>
<a href="<cms:link>/system/workplace/views/workplace.jsp</cms:link>">Workplace</a> &nbsp; | &nbsp;
<%
	}		
}
else
{
	if (cms.userInGroup(user.getName(),"Superusers")){
%>
<a onclick="popupHelp('https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page'); return false;" href="https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page">online help</a> &nbsp; | &nbsp;
<a href="<cms:link>/superuser/index.html</cms:link>">Change member</a> &nbsp; | &nbsp;
<a class="fakelink" onclick="document.getElementById('bookmark_display').style.display='block';">Bookmark member</a> &nbsp; | &nbsp; 	
<%
	}
	if (cms.userInGroup(user.getName(),"Authors") ||
				cms.userInGroup(user.getName(),"Publishers") ||
				cms.userInGroup(user.getName(),"Administrators") ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
				)
		{
	%>
	<a href="<cms:link>/system/workplace/views/workplace.jsp</cms:link>">Workplace</a> &nbsp; | &nbsp;
	<%
		}
	if (cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) || 
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_REVIEWER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_AUTHORISER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_DEPLOYER_GROUP)
		){
%>	
<a href="<cms:link>/_login_publishing.jsp</cms:link>">Enter publishing</a> &nbsp; | &nbsp;
<%
	}
}
%>
  <a href="<cms:link>/contactus.html</cms:link>">contact us </a>
</div> <!-- headernav -->

<div id='header'>
 	<div class='logoholder'>
          <img src='<cms:link>../resources/gfx/logo.gif</cms:link>' alt='BP logo'/>
        </div>
  	<div class='bannerholder'>
  	  <img src='<cms:link>../resources/gfx/pensionline_banner.gif</cms:link>' alt='pensionline logo'/>
  	</div>
</div> <!-- header -->
 
<div id='bannernav' class='non-print'>
  <cms:include file="/system/modules/com.bp.pensionline.template/elements/headerTabs.jsp"/>
</div> <!-- banernav -->

<div id='outer'>
  <div id='crumbtrail'>
    <cms:include file="../elements/breadcrumb.jsp"/>
  </div> <!-- crumbtrail -->

  <!-- left hand sidebar -->
  <div id='sidebar' class='lefty non-print'>
	<div id='menu' class='lefty'>
		<cms:include file="../elements/sidebar.jsp"/>
	</div>
	<div id='rss'>
		<ul>     
			<li><img src='<cms:link>../resources/gfx/rss.gif</cms:link>' alt='RSS icon' /> <a href="<cms:link>/RSS/newsRSS.xml</cms:link>">PensionLine news</a></li>
<!--
			<li><img src='<cms:link>../resources/gfx/rss.gif</cms:link>' alt='RSS icon' /> <a href="<cms:link>/RSS/eventsRSS.xml</cms:link>">PensionLine events</a></li>
-->
		</ul>
	</div>
	<div id='rss_explanation'>
		<ul>
			<li><img src='<cms:link>../resources/gfx/link_arrow.gif</cms:link>' alt='link arrow' /> <a href="<cms:link>/rss.html</cms:link>">what is RSS?</a></li>
		</ul>
	</div>
<!--
	<div id='rss_explanation'>
		<ul>
			<li><img src='<cms:link>../resources/gfx/link_arrow.gif</cms:link>' alt='link arrow' /> <a href="<cms:link>/PensionLine_Survey.html</cms:link>">PensionLine Survey</a></li>
		</ul>
	</div>
-->
  </div>

  <div id='content'>
 
</cms:template>

<cms:template element="body">

	<cms:template ifexists="bpblank">

	<div id='blank_template'>
  		<div id='pagetitle'>
			<cms:include element="bptitle" editable="true" />
		</div>
		<div id='pagebody'>
			<cms:include element="bpblank" editable="true" />
		</div>
	</div>
        </cms:template>


       	<cms:template ifexists="body">

	<div id='homepage_template'>
		<div id='pagetitle'>
			<cms:include element="body" editable="true" />
		</div>

		<div id='main_story'>
			<div class='image'>
				<cms:include element="body2" editable="true" />
			</div>

			<div class='story mains'>
				<cms:include element="body3" editable="true" />
			</div>
		</div>

		<div id='section_note'>
			<cms:include element="body4" editable="true" />
		</div>

		<div id='secondary_stories'>
			<div id='lhs'>
				<div id='second_story'>
					<div class='image'>
						<cms:include element="body5" editable="true" />
					</div>

					<div class='story'>
						<cms:include element="body6" editable="true" />
					</div>
				</div>
			</div> <!-- lhs -->

			<div id='rhs'>
				<div id='third_story'>
					<div class='image'>
						<cms:include element="body7" editable="true" />
					</div>

					<div class='story'>
						<cms:include element="body8" editable="true" />
					</div>
				</div>

				<div id='fourth_story'>
					<div class='image'>
						<cms:include element="body9" editable="true" />
					</div>

					<div class='story'>
						<cms:include element="body10" editable="true" />
					</div>
				</div>
			</div> <!-- secondary_stories -->
		</div> <!-- rhs -->

	</div>
	</cms:template>

<cms:template ifexists="bodyright">

             
	<div id='body_template'>
		<div id='pagetitle'>
			<cms:include element="bodyright" editable="true" />
		</div>

		<div id='main'>
			<div id='writing'>
				<div id='story'>
					<cms:include element="bodyright2" editable="true" />
				</div>
			</div>

			<div id='imagery' class='lefty'>
				<div id='image'>
					<cms:include element="bodyright3" editable="true" />
				</div>
				<div id='banner'>
					<cms:include element="bodyright4" editable="true" />
				</div>
			</div>
		</div>
	</div>
	</cms:template>



       <cms:template ifexists="bodyleft">

             
	<div id='body_template'>
		<div id='pagetitle'>
			<cms:include element="bodyleft" editable="true" />
		</div>

		<div id='main'>
			<div id='imagery'>
				<div id='image'>
					<cms:include element="bodyleft2" editable="true" />
				</div>
				<div id='banner'>
					<cms:include element="bodyleft3" editable="true" />
				</div>
			</div>
			<div id='writing'>
				<div id='story'>
					<cms:include element="bodyleft4" editable="true" />
				</div>
			</div>
		</div>
	</div>
	</cms:template>


	<cms:template ifexists="headerright">

	<div id='header_template'>
		<div id='pagetitle'>
			<cms:include element="headerright" editable="true" />
		</div>

		<div id='top_row'>
			<div id='summary'>
				<cms:include element="headerright2" editable="true" />
			</div>
			<div id='image'>
				<cms:include element="headerright3" editable="true" />
			</div>
		</div>

		<div id='bottom_row'>
			<div id='picture'>
				<cms:include element="headerright4" editable="true" />
			</div>
			<div id='story'>
				<cms:include element="headerright5" editable="true" />
			</div>
			<div id='caption'>
				<cms:include element="headerright6" editable="true" />
			</div>
		</div>
	</div>
	</cms:template>

      <cms:template ifexists="headerleft">

      
	<div id='header_template' class='lefty'>
		<div id='pagetitle'>
			<cms:include element="headerleft" editable="true" />
		</div>

		<div id='top_row'>
			<div id='image'>
				<cms:include element="headerleft2" editable="true" />
			</div>
			<div id='summary'>
				<cms:include element="headerleft3" editable="true" />
			</div>
		</div>

		<div id='bottom_row'>
			<div id='caption'>
				<cms:include element="headerleft4" editable="true" />
			</div>
			<div id='story'>
				<cms:include element="headerleft5" editable="true" />
			</div>
			<div id='picture'>
				<cms:include element="headerleft6" editable="true" />
			</div>
		</div>
	</div>
       </cms:template>
  

</cms:template>

<cms:template element="foot">


 </div> <!-- content -->

</div> <!-- outer -->

<div id='printfooter'>
  <span class='fakelink'>&copy; 2006 BP p.l.c.</span>
  </div> <!-- printfooter -->

  <div id='footer' class='non-print'>

    <div id='tomorrowtoday_tag'>
	<img src='<cms:link>../resources/gfx/tomorrowtoday_small.gif</cms:link>' alt='tomorrow today logo' />
    </div> <!-- owner -->

    <div id='footernav'>
<%
SimpleDateFormat year_formatter = new SimpleDateFormat("yyyy");
String this_year = year_formatter.format(new Date());
%>
   <span class='fakelink'>&copy; 2006-<%= this_year %> BP p.l.c.</span> &nbsp; | &nbsp;
<a href="<cms:link>/legalnotice.html</cms:link>">legal notice </a>&nbsp; | &nbsp;
<a href="<cms:link>/privacy.html</cms:link>">privacy statement </a>
   </div> <!-- footernav -->

  </div> <!-- footer -->

</div> <!-- container -->

<div class="dialog" id="bookmark_display">
      <div class="dialog_title">Bookmark member</div>
      <div class="component_holder">
      <div class="component_label"><label for="refpage">Referring page</label> </div>
      <div class="component_value"><input class="dialog_text" id="refpage" name="refpage" type="text" readonly value='Details here'/> </div>
      </div>
      <div class="component_holder">
      <div class="component_label"><label for="description">Description</label> </div>
      <div class="component_value"><input class="dialog_text" id="description" name="description" type="text" /> </div>
      </div>
      <div class="component_holder">
      <div class="component_label"> </div>
      <div class="component_value">
         <input class="goButton" id="dobookmark" type="button" name="dobookmark" value="Go" onclick="HandleBookmarkAjax(document.getElementById('description').value, document.getElementById('refpage').value);" />
         &nbsp;
         <input class="goButton" id="cancelbookmark" type="button" value="Cancel" onclick="document.getElementById('bookmark_display').style.display='none';" /> </div>
      </div>
  </div>
<div id="DIVTEMPLATE"></div>
<cms:include file="../elements/publishing_toolbar.jsp"/>
</body>
</html>
</cms:template>