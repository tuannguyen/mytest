var EOWWorking = false;
var httpEOW = createXmlHttpRequestObject();

var escaped_one_to_xml_special_map = {
		'&amp;': '&',
		'&quot;': '"',
		'&lt;': '<',
		'&gt;': '>'
	};
							
function Beneficiary() {
  this.title = "";
  this.forenames = "";
  this.initials = "";
  this.surname = "";
  this.address_line_1 = "";
  this.address_line_2 = "";
  this.address_line_3 = "";
  this.address_line_4 = "";
  this.postcode = "";
  this.country = "";
  this.relationship = "";
  this.percentage = "";
}
function setBeneficiary(strTitle, strForenames, strInitials, strSurname, strAdd1, strAdd2, strAdd3, strAdd4, strPostcode, strCountry, strRelatonship, intPercentage){
  this.title = strTitle;
  this.forenames = strForenames;
  this.initials = strInitials;
  this.surname = strSurname;
  this.address_line_1 = strAdd1;
  this.address_line_2 = strAdd2;
  this.address_line_3 = strAdd3;
  this.address_line_4 = strAdd4;
  this.postcode = strPostcode;
  this.country = strCountry;
  this.relationship = strRelatonship;
  this.percentage = intPercentage;
}
// these need sending bvack to the server.
var eow_lumpSumBeneficiaries = new Array();
var eow_pensionBeneficiaries = new Array();
var eow_additionalInformation = "";

// These are state variables.
var eow_lumpSumActiveId = -1;
var total_ls_percentage = 0;
var eow_pensionActiveId = -1;
var changes_made = false;

/***********************************************************************
 * LumpSum section
 */

function eow_setLumpSumBeneficiary(ben) {
	setWhat("ls_ben_title", ben.title);
	setWhat("ls_forenames", ben.forenames);
	setWhat("ls_initials", ben.initials);
	setWhat("ls_surname", ben.surname);
	setWhat("ls_address_line_1", ben.address_line_1);
	setWhat("ls_address_line_2", ben.address_line_2);
	setWhat("ls_address_line_3", ben.address_line_3);
	setWhat("ls_address_line_4", ben.address_line_4);
	setWhat("ls_address_postcode", ben.postcode);
	setWhat("ls_address_country", ben.country);
	setWhat("ls_relationship", ben.relationship);
	setWhat("ls_percentage", ben.percentage);
}

function eow_getLumpSumBeneficiary() {
	ben = new Beneficiary();
	ben.title = getWhat("ls_ben_title");
	ben.forenames = getWhat("ls_forenames");
	ben.initials = getWhat("ls_initials");
	ben.surname = getWhat("ls_surname");
	ben.address_line_1 = getWhat("ls_address_line_1");
	ben.address_line_2 = getWhat("ls_address_line_2");
	ben.address_line_3 = getWhat("ls_address_line_3");
	ben.address_line_4 = getWhat("ls_address_line_4");
	ben.postcode = getWhat("ls_address_postcode");
	ben.country = getWhat("ls_address_country");
	ben.relationship = getWhat("ls_relationship");
	ben.percentage = parseInt(getWhat("ls_percentage"));
	if(isNaN(ben.percentage)) {
		ben.percentage = 0;
	}
	return ben;
}

function eow_popupLumpSumBeneficiary() {
	popupDialog('ls_beneficiary_display', -60, -100);
}

function eow_popdownLumpSumBeneficiary() {
	e = document.getElementById("ls_beneficiary_display");
	if(e) {
		e.style.display = "none";
	}
	eow_setLumpSumBeneficiary(new Beneficiary());
}
function eow_loadLumpSumBeneficiary() {
	setBeneficiary()
}
function eow_addLumpSumBeneficiary() {
	ben = eow_getLumpSumBeneficiary();
	if(ben.title.length == 0 ) {
		alert("You must specify the beneficiaries title");
		return false;
	}
	if((ben.forenames).length == 0 )  {
		alert("You must specify the beneficiaries forenames");
		return false;
	}
	if((ben.surname).length == 0 ) {
		alert("You must specify the beneficiaries surname");
		return false;
	}
	if((ben.initials).length > 4 ) {
		alert("You can't input more than 4 characters");
		return false;
	}
	if((ben.relationship).length == 0 ) {
		alert("You must specify the beneficiaries relationship to you");
		return false;
	}

	if(ben.percentage <= 0 || ben.percentage > 100 || isNaN(ben.percentage)) {
		alert("You must enter a percentage (between one and one hundred) to provide to this beneficiary");
		return false;
	}
	eow_popdownLumpSumBeneficiary();
	if(eow_lumpSumActiveId == -1) {
		eow_lumpSumActiveId = eow_lumpSumBeneficiaries.length;
	}
	eow_lumpSumBeneficiaries[eow_lumpSumActiveId] = ben;
	eow_lumpSumActiveId = -1;
	changes_made = true;
	document.getElementById("eow_submit_form").style.display = "block";
	eow_drawLumpSumTable();
}

function eow_updateLumpSumBeneficiary(id) {
	eow_lumpSumActiveId = id;
	eow_setLumpSumBeneficiary(eow_lumpSumBeneficiaries[id]);
	eow_popupLumpSumBeneficiary();
}

function eow_removeLumpSumBeneficiary(id) {
	eow_lumpSumBeneficiaries[id] = 0; // not null cuz it will wipe it.
	changes_made = true;
	document.getElementById("eow_submit_form").style.display = "block";
	eow_drawLumpSumTable();
}

function eow_drawLumpSumTable() {
	e = document.getElementById("part_a_table");
	if(e) {
		var err_tag = "";
		str = "";
		str += "<table cellspacing=\"0\" cellpadding=\"0\" class=\"historytable\">\n";
		str += "    <tbody>\n";
		str += "        <tr>\n";
		str += "            <th class=\"label\">Beneficiary</th>\n";
		str += "            <th class=\"label\">Relationship</th>\n";
		str += "            <th class=\"label\">Percentage</th>\n";
		str += "        </tr>\n";
		total_ls_percentage = 0;


		if(eow_countArray(eow_lumpSumBeneficiaries) > 0) {

			for(i = 0; i < eow_lumpSumBeneficiaries.length; i++) {
				if(eow_lumpSumBeneficiaries[i] != 0) {
					var this_percent = parseInt(eow_lumpSumBeneficiaries[i].percentage);
					total_ls_percentage += this_percent;
					var name = "";
					name += eow_lumpSumBeneficiaries[i].title + " ";
					name += eow_lumpSumBeneficiaries[i].forenames + " ";
					name += eow_lumpSumBeneficiaries[i].surname;
					var relationship = eow_lumpSumBeneficiaries[i].relationship;
					str += "        <tr>\n";
					str += "            <td class=\"\">";
					str += "<a class=\"fakelink\" title=\"Remove " + name + " from lump sum beneficiaries\" onclick=\"eow_removeLumpSumBeneficiary(" + i + "); return false;\">[R]</a>";
					str += "<a class=\"fakelink\" title=\"Edit details for " + name + " in lump sum beneficiaries\" onclick=\"eow_updateLumpSumBeneficiary(" + i + "); return false;\">[E]</a> " + htmlEncode(name) + "</td>\n";
					str += "            <td class=\"\">" + relationship + "</td>\n";
					str += "            <td class=\"value number\">" + this_percent + "%</td>\n";
					str += "        </tr>\n";
				}
			}
			var pre_tag = "";
			var post_tag = "";
			if(total_ls_percentage != 100) {
				pre_tag = "<font color='red'><strong>";
				post_tag = "</strong></font>";
				err_tag = "<div style='float:right;'>" + pre_tag + "The total percentage must equal 100%" + post_tag + "</div>";
			}
			str += "        <tr>\n";
			str += "            <td class=\"blank\" colspan=\"2\">&nbsp;</td>\n";
			str += "            <td class=\"value number total\">";
			str += pre_tag;
			str += total_ls_percentage + "%";
			str += post_tag;
			str += "</td>\n";
			str += "        </tr>\n";
		} else {
			str += "        <tr>\n";
			str += "            <td class=\"value\" colspan=\"3\">you do not have any beneficiary information on the system.</td>\n";
			str += "        </tr>\n";
		}
		str += "    </tbody>\n";
		str += "</table>\n";
		str += err_tag;
		e.innerHTML = str;
	}
}

/***********************************************************************
 * Pension section
 */

function eow_setPensionBeneficiary(ben) {
	setWhat("pen_ben_title", ben.title);
	setWhat("pen_forenames", ben.forenames);
	setWhat("pen_initials", ben.initials);
	setWhat("pen_surname", ben.surname);
	setWhat("pen_address_line_1", ben.address_line_1);
	setWhat("pen_address_line_2", ben.address_line_2);
	setWhat("pen_address_line_3", ben.address_line_3);
	setWhat("pen_address_line_4", ben.address_line_4);
	setWhat("pen_address_postcode", ben.postcode);
	setWhat("pen_address_country", ben.country);
	setWhat("pen_relationship", ben.relationship);
}

function eow_getPensionBeneficiary() {
	ben = new Beneficiary();
	ben.title = getWhat("pen_ben_title");
	ben.forenames = getWhat("pen_forenames");
	ben.initials = getWhat("pen_initials");
	ben.surname = getWhat("pen_surname");
	ben.address_line_1 = getWhat("pen_address_line_1");
	ben.address_line_2 = getWhat("pen_address_line_2");
	ben.address_line_3 = getWhat("pen_address_line_3");
	ben.address_line_4 = getWhat("pen_address_line_4");
	ben.postcode = getWhat("pen_address_postcode");
	ben.country = getWhat("pen_address_country");
	ben.relationship = getWhat("pen_relationship");
	ben.percentage = 0;
	return ben;
}

function eow_popupPensionBeneficiary() {
	popupDialog('pen_beneficiary_display', -60, -100);
}

function eow_popdownPensionBeneficiary() {
	e = document.getElementById("pen_beneficiary_display");
	if(e) {
		e.style.display = "none";
	}
	eow_setPensionBeneficiary(new Beneficiary());
}

function eow_addPensionBeneficiary() {
	ben = eow_getPensionBeneficiary();
	if(ben.title.length == 0) {
		alert("You must specify the beneficiaries title");
		return false;
	}
	if((ben.forenames).length == 0) {
		alert("You must specify the beneficiaries forenames");
		return false;
	}
	if((ben.surname).length == 0) {
		alert("You must specify the beneficiaries surname");
		return false;
	}
	if((ben.relationship).length == 0) {
		alert("You must specify the beneficiaries relationship to you");
		return false;
	}
	eow_popdownPensionBeneficiary();
	if(eow_pensionActiveId == -1) {
		eow_pensionActiveId = eow_pensionBeneficiaries.length;
	}
	eow_pensionBeneficiaries[eow_pensionActiveId] = ben;
	eow_pensionActiveId = -1;
	changes_made = true;
	document.getElementById("eow_submit_form").style.display = "block";
	eow_drawPensionTable();
}

function eow_updatePensionBeneficiary(id) {
	eow_pensionActiveId = id;
	eow_setPensionBeneficiary(eow_pensionBeneficiaries[id]);
	eow_popupPensionBeneficiary();
}

function eow_removePensionBeneficiary(id) {
	eow_pensionBeneficiaries[id] = 0; // not null cuz it will wipe it.
	changes_made = true;
	document.getElementById("eow_submit_form").style.display = "block";
	eow_drawPensionTable();
}

function eow_drawPensionTable() {
	e = document.getElementById("part_b_table");
	if(e) {
		str = "";
		str += "<table cellspacing=\"0\" cellpadding=\"0\" class=\"datatable\">\n";
		str += "    <tbody>\n";
		str += "        <tr>\n";
		str += "            <th class=\"label\">Beneficiary</th>\n";
		str += "            <th class=\"label\">Relationship</th>\n";
		str += "        </tr>\n";
		if(eow_countArray(eow_pensionBeneficiaries) > 0) {
			for(i = 0; i < eow_pensionBeneficiaries.length; i++) {
				if(eow_pensionBeneficiaries[i] != 0) {
					var name = "";
					name += eow_pensionBeneficiaries[i].title + " ";
					name += eow_pensionBeneficiaries[i].forenames + " ";
					name += eow_pensionBeneficiaries[i].surname;
					var relationship = eow_pensionBeneficiaries[i].relationship;
					str += "        <tr>\n";
					str += "            <td class=\"\">";
					str += "<a class=\"fakelink\" title=\"Remove " + name + " from pension beneficiaries\" onclick=\"eow_removePensionBeneficiary(" + i + "); return false;\">[R]</a>";
					str += "<a class=\"fakelink\" title=\"Edit details for " + name + " in pension beneficiaries\" onclick=\"eow_updatePensionBeneficiary(" + i + "); return false;\">[E]</a> " + htmlEncode(name) + "</td>\n";
					str += "            <td class=\"\">" + relationship + "</td>\n";
					str += "        </tr>\n";
				}
			}
		} else {
			str += "        <tr>\n";
			str += "            <td class=\"value\" colspan=\"2\">you do not have any beneficiary information on the system.</td>\n";
			str += "        </tr>\n";
		}
		str += "    </tbody>\n";
		str += "</table>\n";
		e.innerHTML = str;
	}
}

/***********************************************************************
 * additional info section
 */

function eow_updateAdditionalInformation() {
	e = document.getElementById("part_c_table");
	if(e) {
		eow_additionalInformation = document.getElementById("eow_info").value;
	}
	changes_made = true;
	document.getElementById("eow_submit_form").style.display = "block";
	eow_popdownAdditionalInformation();
	
	//alert("before encode:"+eow_additionalInformation)
	var encodedInfor = "";
	if (eow_additionalInformation != "")
		encodedInfor = htmlEncode(eow_additionalInformation);
	eow_drawAdditionalInformationTable(encodedInfor);
}

function eow_popupAdditionalInformation() {
	e = document.getElementById("part_c_table");	
	if(e) {
		
		// Hold the temporary value of eow_additionalInformation
		//var decodedStr = eow_additionalInformation;
		
		// Process decode above local variable for display 
		if (eow_additionalInformation=="") document.getElementById("eow_info").value = '';
	    else document.getElementById("eow_info").value = decodeXml(eow_additionalInformation);		
		popupDialog("additional_info_display", -60, -100);		
	}
}

function eow_popdownAdditionalInformation() {
	e = document.getElementById("additional_info_display");
	if(e) {
		e.style.display = "none";
	}
}

function eow_drawAdditionalInformationTable(encodedInfor) {
	e = document.getElementById("part_c_table");
	
	if (encodedInfor) 
		eow_additionalInformation = encodedInfor;
	if(e) {
		str = "";
		str += "<table cellspacing=\"0\" cellpadding=\"0\" class=\"datatable\">\n";
		str += "    <tbody>\n";
		str += "        <tr>\n";
		str += "            <th class=\"label\">Additional information</th>\n";
		str += "        </tr>\n";
		if(eow_additionalInformation.length > 0) {	
			str += "        <tr>\n";
			str += "            <td class=\"\">"+eow_additionalInformation+"</td>\n";			
			str += "        </tr>\n";
		} else {
			str += "        <tr>\n";
			str += "            <td class=\"value\">you do not have any additional information on the system.</td>\n";
			str += "        </tr>\n";
		}
		str += "    </tbody>\n";
		str += "</table>\n";
		e.innerHTML = str;
	}
}

/***********************************************************************
 * general handling section
 */

function setWhat(id, val) {
	e = document.getElementById(id);
	if (e == null)
	{
		var es = document.getElementsByName(id);
		if (es.length > 0)
		{
			e = es[0];
		}
	}	
	if(e) {
		e.value = val;
	} else {
		alert("Unable to find element with ID: '" + id + "'");
	}
}

function getWhat(id) {
	var e = document.getElementById(id);
	if (e == null)
	{
		var es = document.getElementsByName(id);
		if (es.length > 0)
		{
			e = es[0];
		}
	}
	if(e) {
		return e.value;
	} else {
		alert("Unable to find element with ID: '" + id + "'");
	}
}

function eow_countArray(arr) {
	var count = 0;
	for(i = 0; i < arr.length; i++) {
		if(arr[i] != 0) {
			count++;
		}
	}
	return count;
}

function eow_onLoad() {
	var partATable = document.getElementById("part_a_table");
	var partBTable = document.getElementById("part_b_table");
	var partCTable = document.getElementById("part_c_table");
	if(partATable != null && partBTable != null && partCTable != null) 
	{
		eow_getData();
			
		// Display spinning images in table holders			
		partATable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
		partBTable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
		partCTable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
	}
}

// TODO: make this stop the unload, or submit the changes.
function eow_onUnload() {
	// check for changes and prompt user.
	if(changes_made) {
		var ans;
		ans = window.confirm("You have made changes, do you want to save them?");
		if(ans == true) 
		{
			// TODO: if this fails (i.e. the total percentage is not 100%) we need to stop the unload.
			var xml = EOWStoreAjaxHandle("Store");
			handleSubmit(xml);
			/*var eowUrl = ajaxurl + "/EOWHandler?xml="+xml;
				httpEOW.open("POST", eowUrl);
				alert(xml);
				httpEOW.onreadystatechange = function();
				
				httpEOW.send(null);	/**/
			//eow_submitForm();
		}
	}
}
/***********************************************************************
 * general handling section
 */

function eow_getData() {
	// Set the whizzies going
	// kick off the AJAX call to the server

	eow_getDataResponse();
}

function eow_getDataResponse() {
	// this is the AJAX response handler.
	// It should process the response into the arrays and data in the script,
	// and then call the table draw routines here.
	//EOWHandleResponse();
	EOWAjaxHandle();	
}
function EOWAjaxHandle()
{
	
	var xml = "";
	xml += "<AjaxParameterRequest>";
	xml += "<Select>";			
	xml += "</Select>";		
	xml += "</AjaxParameterRequest>";
	var eowUrl = ajaxurl + "/EOWHandler?xml="+xml;
	httpEOW.open("POST", eowUrl);
	httpEOW.onreadystatechange = EOWHandleResponse;
	httpEOW.send(null);

}
function  EOWHandleResponse()
{
	if (httpEOW.readyState == 4) 
	{
								
		// Check that a successful server response was received
      	if (httpEOW.status == 200) {
			 /*Ttest
			  var stringXMl = "<AjaxParameterResponse><LumpSum><Title>TitleLum</Title><Forenames>ForenamesLum</Forenames><Initials>InitialsSum</Initials><Surname></Surname><Street>StreetLump</Street><PostalTown>PostalTownLump</PostalTown><District>DistrictLump</District><County>CountyLump</County><Postcode>PostcodeLump</Postcode><Country>CountryLump</Country><Relationship>RelationshipLmup</Relationship><Percentage>50</Percentage></LumpSum><LumpSum><Title>YYY</Title><Forenames>YYY</Forenames><Initials>YYY</Initials><Surname>YYY</Surname><Street>YYY</Street><PostalTown>YYY</PostalTown><District>YYY</District><County>YYY</County><Postcode>YYY</Postcode><Country>YYY</Country><Relationship>YYY</Relationship><Percentage>50</Percentage></LumpSum><SurvivorsPension><Title>YYY</Title><Forenames>YYY</Forenames><Initials>YYY</Initials><Surname>YYY</Surname><Street>YYY</Street><PostalTown>YYY</PostalTown><District>YYY</District><County>YYY</County><Postcode>YYY</Postcode><Country>YYY</Country><Relationship>YYY</Relationship></SurvivorsPension><AdditionalInformation><Message>XXX</Message></AdditionalInformation></AjaxParameterResponse>";
			 var response = createResponseXML(stringXMl);
			 /*End test*/									 
			try
			{
				//alert(httpEOW.responseText);
			  	var response = createResponseXML(httpEOW.responseText);	
			  	//				

			  	var title = "";
			  	var forenames = "";
			  	var initials = "";
			  	var surname = "";
			  	var address_line_1 = "";
			  	var address_line_2 = "";
			  	var address_line_3 = "";
			  	var address_line_4 = "";
			  	var postcode = "";
			  	var country = "";
			  	var relationship = "";
			  	var percentage = "";

				var eowLumpSum= response.getElementsByTagName("LumpSum");	
				var eowSurvival= response.getElementsByTagName("SurvivorsPension");	
                                               

				var foundPoint = httpEOW.responseText.indexOf("<Message>");
				if(foundPoint >0)
				{
					var endPoint = httpEOW.responseText.indexOf("</Message>");
					var eowAdditional= httpEOW.responseText.substring(foundPoint + "<Message>".length, endPoint);
				}
				else
				{
				 	var eowAdditional = "";
				}
				//var eowAdditional= response.getElementsByTagName("AdditionalInformation");	
				if(eowLumpSum.length>0)
				{

					for (i = 0 ; i < eowLumpSum.length ; i++) 
					{
						var eowLumpSumTmp= response.getElementsByTagName("LumpSum")[i];
						//alert("infor"+response.getElementsByTagName("LumpSum")[i].firstChild.nodeValue);
						title = getResponseTag(eowLumpSumTmp,"Title",i);
						forenames = getResponseTag(eowLumpSumTmp,"Forenames",i);
						initials = getResponseTag(eowLumpSumTmp,"Initials",i);
						surname = getResponseTag(eowLumpSumTmp,"Surname",i);	
						address_line_1 = getResponseTag(eowLumpSumTmp,"Street",i);
						address_line_2 = getResponseTag(eowLumpSumTmp,"PostalTown",i);
						address_line_3 = getResponseTag(eowLumpSumTmp,"District",i);
						address_line_4 = getResponseTag(eowLumpSumTmp,"County",i);
						postcode = getResponseTag(eowLumpSumTmp,"Postcode",i);								
						country = getResponseTag(eowLumpSumTmp,"Country",i);
						relationship = getResponseTag(eowLumpSumTmp,"Relationship",i);
						percentage = getResponseTag(eowLumpSumTmp,"Percentage",i);								//alert("----"+title+"----"+forenames+"----"+initials+"----"+surname+"----"+address_line_1+"----"+address_line_2+"----"+address_line_3+"----"+address_line_4+"----"+postcode+"----"+country+"----"+relationship+"----"+percentage);
						//alert(i+"---"+ eowLumpSum.length);
						ben = new setBeneficiary(title,forenames,initials,surname,address_line_1,address_line_2,address_line_3,address_line_4,postcode,country,relationship,percentage);
						if(!eow_lumpSumBeneficiaries)
						{								
							eow_lumpSumBeneficiaries= new Array();
						}
						//alert(ben.forenames+"-1--"+eow_lumpSumBeneficiaries.length);							
						eow_lumpSumBeneficiaries[i] = ben;							
						eow_lumpSumActiveId = -1;			
							
					}
				}
				if(eowSurvival.length>0)
				{
					for (i = 0 ; i < eowSurvival.length ; i++) 
					{
						var eowLumpSumTmp= response.getElementsByTagName("SurvivorsPension")[i];								
						title = getResponseTag(eowLumpSumTmp,"Title",i);
						forenames = getResponseTag(eowLumpSumTmp,"Forenames",i);
						initials = getResponseTag(eowLumpSumTmp,"Initials",i);
						surname = getResponseTag(eowLumpSumTmp,"Surname",i);	
						address_line_1 = getResponseTag(eowLumpSumTmp,"Street",i);
						address_line_2 = getResponseTag(eowLumpSumTmp,"PostalTown",i);
						address_line_3 = getResponseTag(eowLumpSumTmp,"District",i);
						address_line_4 = getResponseTag(eowLumpSumTmp,"County",i);
						postcode = getResponseTag(eowLumpSumTmp,"Postcode",i);								
						country = getResponseTag(eowLumpSumTmp,"Country",i);
						relationship = getResponseTag(eowLumpSumTmp,"Relationship",i);
						ben = new setBeneficiary(title,forenames,initials,surname,address_line_1,address_line_2,address_line_3,address_line_4,postcode,country,relationship,0);
						if(!eow_pensionBeneficiaries)
						{								
							eow_pensionBeneficiaries= new Array();
						}
						eow_pensionBeneficiaries[i] = ben;
						eow_pensionActiveId = -1;
					}
				}
				if(eowAdditional.length>0)
				{
					//alert(eowAdditional);
                    var foundNumb= eowAdditional.indexOf("[CDATA[");
                    var endNumb= eowAdditional.indexOf("]]");
                    eowAdditional = eowAdditional.substring(foundNumb+ "[CDATA[".length, endNumb);
					eow_additionalInformation = eowAdditional;						
				}
				if(response.getElementsByTagName("Store").length > 0)
				{
				
					document.getElementById("eow_submit_form").style.display = "block";
				}
				else
				{				
					document.getElementById("eow_submit_form").style.display = "none";
				}
				eow_drawLumpSumTable();
				eow_drawPensionTable();
				eow_drawAdditionalInformationTable();
			
					
			}
			catch (err)
			{
				alert(err.description);	
			}
		}
	}

}
function getResponseTag(objRoot,tagName,position)
{
	var tagValue="";
	if(objRoot.getElementsByTagName(tagName)[0].childNodes.length > 0) 
	{
		tagValue = objRoot.getElementsByTagName(tagName)[0].firstChild.nodeValue
	}
	else
	{
		tagValue =" ";
	}

	if (tagValue == 'null')
	{
		tagValue = "";
	}

	return tagValue
}
function EOWStoreAjaxHandle(strAction)
{
	var xml = "" ;
	xml += "<AjaxParameterRequest>\n";
	xml += "	<"+strAction+">\n";			
	if(eow_countArray(eow_lumpSumBeneficiaries) > 0) 
	{
		for(i = 0; i < eow_lumpSumBeneficiaries.length; i++) 
		{			
			if(eow_lumpSumBeneficiaries[i] != 0) 
			{	
				xml+="<LumpSum>\n";				
				xml += "<Title>"+eow_lumpSumBeneficiaries[i].title+"</Title>\n";
				xml += "<Forenames><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].forenames)+"]]></Forenames>\n";
				xml += "<Initials><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].initials)+"]]></Initials>\n";
				xml += "<Surname><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].surname)+"]]></Surname>\n";
				xml +="<Street><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].address_line_1)+"]]></Street>\n";
				xml +="<PostalTown><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].address_line_2)+"]]></PostalTown>\n";
				xml +="<District><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].address_line_3)+"]]></District>\n";
				xml +="<County><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].address_line_4)+"]]></County>\n";
				xml +="<Postcode><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].postcode)+"]]></Postcode>\n";
				xml +="<Country><![CDATA["+formatToSendableString(eow_lumpSumBeneficiaries[i].country)+"]]></Country>\n";
				xml +="<Relationship>"+eow_lumpSumBeneficiaries[i].relationship+"</Relationship>\n";
				xml +="<Percentage>"+eow_lumpSumBeneficiaries[i].percentage+"</Percentage>\n";	
				xml+="</LumpSum>\n";
			}	
		}
	}
	if(eow_countArray(eow_pensionBeneficiaries) > 0) 
	{
		for(i = 0; i < eow_pensionBeneficiaries.length; i++) 
		{			
			if(eow_pensionBeneficiaries[i] != 0) 
			{
				xml+="<SurvivorsPension>\n";
				xml += "<Title>"+eow_pensionBeneficiaries[i].title+"</Title>\n";
				xml += "<Forenames><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].forenames)+"]]></Forenames>\n";
				xml += "<Initials><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].initials)+"]]></Initials>\n";
				xml += "<Surname><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].surname)+"]]></Surname>\n";							
				xml += "<Street><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].address_line_1)+"]]></Street>\n";
				xml += "<PostalTown><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].address_line_2)+"]]></PostalTown>\n";
				xml += "<District><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].address_line_3)+"]]></District>\n";
				xml += "<County><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].address_line_4)+"]]></County>\n";
				xml += "<Postcode><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].postcode)+"]]></Postcode>\n";
				xml += "<Country><![CDATA["+formatToSendableString(eow_pensionBeneficiaries[i].country)+"]]></Country>\n";
				xml += "<Relationship>"+eow_pensionBeneficiaries[i].relationship+"</Relationship>\n";	
				xml+="</SurvivorsPension>\n";				
			}				
		}
	}
	xml+="<AdditionalInformation><Message><![CDATA["+formatToSendableString(eow_additionalInformation)+"]]></Message>\n";
	xml+="</AdditionalInformation>\n";
	 
	xml += "	</"+strAction+">\n";	
	xml += "</AjaxParameterRequest>\n";
	//alert("-->xml :"+xml);
	return xml;
	
}
function eow_submitForm() 
{
	if(eow_countArray(eow_lumpSumBeneficiaries) > 0 && total_ls_percentage != 100) 
	{
		alert("If you are providing beneficiary information for your lump sum, the total must equal 100%");
		return false;
	}
	changes_made = false;
	document.getElementById("eow_submit_form").style.display = "none";
	var submitXml=EOWStoreAjaxHandle("Update");
	handleSubmit(submitXml);
	
	// Ajax the details to the server. Display spinning images at table holders
	var partATable = document.getElementById("part_a_table");
	var partBTable = document.getElementById("part_b_table");
	var partCTable = document.getElementById("part_c_table");
	if(partATable != null && partBTable != null && partCTable != null) 
	{
		// Display spinning images in table holders			
		partATable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
		partBTable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
		partCTable.innerHTML = "<img src=\"/content/pl/system/modules/com.bp.pensionline.test/resources/spinning_image.gif\" alt=\"Getting EOW data...\" />";
	}	
}

function handleSubmit(xml)
{
	var eowUrl = ajaxurl + "/EOWHandler";
	httpEOW.open("POST", eowUrl);
	httpEOW.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	httpEOW.onreadystatechange = EOWSubmitHandleResponse;
	//alert(xml);	
	httpEOW.send("xml="+xml);	

}
function EOWSubmitHandleResponse()
{
	if (httpEOW.readyState == 4) 
	{
								
		// Check that a successful server response was received
      	if (httpEOW.status == 200) 
      	{
            var response = createResponseXML(httpEOW.responseText);	
			var error = response.getElementsByTagName("Error").length;
			eow_getData();
			if(error > 0)
			{
				alert("Wishes update failed!");
			}
			else
			{
				alert("Wishes update successful!");
			}
		}	
	}
}

function formatToSendableString(str)
{
	if (str && str.length > 0)
	{
		var newStr = str.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');		
		//var newStr = str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g,'&gt;').replace(/"/g, "&quot;");
		return newStr;
	}
	return str;
}

function htmlEncode(str)
{
	if (str && str.length > 0) {
		var newStr = str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
		return newStr;
	}
	return str;
}

function decodeXml(strParams){
	if (strParams && strParams.length > 0) {
		return strParams.replace(/(&quot;|&lt;|&gt;|&amp;)/g, 
			function(str, item){
				return escaped_one_to_xml_special_map[item];
		});
	}
}


