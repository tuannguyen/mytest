//////////////////////////////////////////////////////////////////////////////////////////////////
// Generic modeller functions
//

var popped_up_panel = null;

function modeller_popupControlPanel(id, dis) {
	if(popped_up_panel != null) {
		popped_up_panel.style.display = "none";
	        popped_up_panel = null;
	}
	var dp = "";

	if(dis == true){
		dp = "block";
	}else{
		dp = "none";
	}

	popped_up_panel = document.getElementById(id);
    	if(popped_up_panel) {
        	popped_up_panel.style.display = dp;
	}

}

function modeller_submitRequest(package_func) {
    if(popped_up_panel != null) {
        popped_up_panel.style.display = "none";
        popped_up_panel = null;
    }
    package_func();
}

