
// If NS -- that is, !IE -- then set up for mouse capture
if (BrowserDetect.browser != "Explorer") document.captureEvents(Event.MOUSEMOVE)

// Set-up to use getMouseXY function onMouseMove
document.onmousemove = getMouseXY;

// Temporary variables to hold mouse x-y pos.s
var mouseX = 0
var mouseY = 0

// Main function to retrieve mouse x-y pos.s

function getMouseXY(e) {
  if (BrowserDetect.browser == "Explorer") { // grab the x-y pos.s if browser is IE
    // need to use documentElement because of the DTD we are using in the doctype of the page.
    mouseX = event.clientX + document.documentElement.scrollLeft;
    mouseY = event.clientY + document.documentElement.scrollTop;
  } else {  // grab the x-y pos.s if browser is NS
    mouseX = e.pageX;
    mouseY = e.pageY;
  }
  // catch possible negative values in NS4
  if (mouseX < 0){mouseX = 0}
  if (mouseY < 0){mouseY = 0}
  return true
}

// This function pops up a dialog with the given ID with an x/y offset from the mouse co-ordinates.
function popupDialog(id, x_offset, y_offset) {
  e = document.getElementById(id);
  if(e) {
    e.style.top = (mouseY + y_offset) + "px";
    e.style.left = (mouseX + x_offset) + "px";
    e.style.display='block';
    str = "";
    str = str + "mouseX: " + mouseX + ", mouseY: " + mouseY + "\n";
    str = str + "offsetX: " + x_offset + ", offsetY: " + y_offset + "\n";
    str = str + "top: " + e.style.top + ", left: " + e.style.left + "\n";
//    alert(str);
  }
}