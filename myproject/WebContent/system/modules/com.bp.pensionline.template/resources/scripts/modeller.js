var service= 10;
var age = 48;
var psal = 113400;
var nra = 65;
var opt = "45ths";

var cash = 0;
var acc = (service)/(45.0);
var pen = psal * (acc + ((nra - age)/45.0));

var max_cash = 120000;
var mc_def = 10; // total deflection of pension for full max cash is 10%??

var use_levelling = false;
var mpsal = psal;
var mnra = nra;
var mopt = opt;
var mpen = pen;
var mlpen = pen;
var mcash = cash;

function doCalc() {
  if(mopt == "60ths") cont = 60.0;
  if(mopt == "53rds") cont = 53.0;
  if(mopt == "45ths") cont = 45.0;

  mpen = psal * (acc + ((mnra - age)/cont));
  
  // adjust for cash
  p = 1 - ((mcash/max_cash) * (mc_def/100)); 
  mpen = p * mpen;
  
  // adjust for levelling
  if(use_levelling) {
    p = mpen;
    mpen = p * 1.2;
    mlpen = p * 0.9;
  }
}

var working_img = "<div class='working'><img src='gfx/ajax_working.gif' alt='working' /></div>";

function randNum(min, max) {
  var spread = max - min + 1; // cuz random does 0 to max - 1
  var rand = Math.floor(Math.random() * spread);
  return rand + min;
}

function formatNumber(num) {
	var n = new NumberFormat(num);
	return n.toFormatted();
}

function populateFieldValue(which, what) {
  e = document.getElementById(which);
  if(e) {
  	e.innerHTML = what;
  }
}

var timers = Array();
function populateValues() {

  doCalc();
  
  var cumulative = randNum(500, 1000);
  var min_t = 50;
  var max_t = 300;

  id = setTimeout('populateFieldValue("current_psal", "&pound; " + formatNumber(psal))', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_psal", "&pound; " + formatNumber(mpsal))', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("current_nra", nra)', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_nra", mnra)', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("current_cont_opt", opt)', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_cont_opt", mopt)', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("current_cash", "&pound; " + formatNumber(cash))', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_cash", "&pound; " + formatNumber(mcash))', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("current_pension", "&pound; " + formatNumber(pen))', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_pension", "&pound; " + formatNumber(mpen))', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("current_spouses_pension", "&pound; " + formatNumber((pen*2.0)/(3.0)))', cumulative);
  timers.push(id);
  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_spouses_pension", "&pound; " + formatNumber((mpen*2.0)/(3.0)))', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_levelled_pension", "&pound; " + formatNumber(mlpen))', cumulative);
  timers.push(id);

  cumulative = cumulative + randNum(min_t, max_t);
  id = setTimeout('populateFieldValue("modelled_spouses_levelled_pension", "&pound; " + formatNumber((mlpen*2.0)/(3.0)))', cumulative);
  timers.push(id);

  id = setTimeout('clearTimers()', cumulative + 100);
  timers.push(id);

}

function clearTimers() {
  for(i = 0; i < timers.length; i++) {
    clearTimeout(timers[i]);
  }
}

function setWorking() {
  populateFieldValue("modelled_psal", working_img);
  populateFieldValue("modelled_nra", working_img);
  populateFieldValue("modelled_cont_opt", working_img);
  populateFieldValue("modelled_cash", working_img);
  populateFieldValue("modelled_pension", working_img);
  populateFieldValue("modelled_spouses_pension", working_img);
  populateFieldValue("modelled_levelled_pension", working_img);
  populateFieldValue("modelled_spouses_levelled_pension", working_img);
}

function updateRetirementAge() {
  mnra = sage.n_value;
  setWorking();
  populateValues();
}

function updateCash() {
  mcash = scash.n_value;
  setWorking();
  populateValues();
}

function updateContributoyOptions() {
  e = document.getElementById("copts");
  if(e) {
    mopt = e.value;
    e = document.getElementById("req_cont_opts");
    ed = document.getElementById("req_cont_opts_date");
    if(e) {
      if(mopt != opt) {
      	e.style.visibility = "visible";
      	ed.style.display = "block";
      } else {
      	e.style.visibility = "hidden";
      	ed.style.display = "none";
      }
    }
    setWorking();
    populateValues();
  } else {
    alert("core field is missing");
  }
}

function hideOption(id, tf) {
    e = document.getElementById(id);
    if(e) {
      if(!tf) {
      	e.style.visibility = "visible";
      	e.style.display = "block";
      } else {
      	e.style.visibility = "hidden";
      	e.style.display = "none";
      }
    }
}

function hideOptions() {
	hideOption("firstin_control_panel", true);
	hideOption("age_control_panel", true);
	hideOption("contopt_control_panel", true);
	hideOption("cash_control_panel", true);
}

function viewAge() {
	hideOptions();
	hideOption("age_control_panel", false);
}

function viewContoption() {
	hideOptions();
	hideOption("contopt_control_panel", false);
}

function viewCash() {
	hideOptions();
	hideOption("cash_control_panel", false);
}

