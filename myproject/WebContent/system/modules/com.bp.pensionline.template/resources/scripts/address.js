// JavaScript Document

function checkAddress() {
	//This is where the submit to the database event will occur

	// The following detail .value's will need to make it to the server for processing.
	var add_line_1  = document.getElementById("address_line_1");
	var add_line_2  = document.getElementById("address_line_2");
	var postal_town = document.getElementById("address_line_3");
	var city        = document.getElementById("address_line_4");
	var county      = document.getElementById("address_line_5");
	var postcode    = document.getElementById("address_postcode");
	var country     = document.getElementById("address_country");
	
	//Handle the post response, redirect to the oops page on error?

	// Assuming its o.k...
	if (add_line_1.value.length==0){
		alert("You must enter a house name/number and street");
		house_name_number.focus();
		return false;
	}
	if (postal_town.value.length==0){
		alert("You must enter a postal town");
		postal_town.focus();
		return false;
	}
	if (city.value.length == 0 && county.value.length==0){
		alert("You must enter a county or a city");
		county.focus();
		return false;
	}
	if (postcode.value.length==0){
		alert("You must enter a postcode");
		postcode.focus();
		return false;
	}
	
	//var validRegExp = new RegExp("^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$");
  	//strEmail = postcode.value.toUpperCase();
	//if (!strEmail.match(validRegExp)) 
	//{
	//	alert('A valid postcode is required.\nPlease amend and retry');
	//	postcode.focus();
	//	return false;
	//} 
	
	if (country.value.length==0){
		alert("You must enter postal country");
		country.focus();
		return false;
	}
	
        var address = add_line_1.value + " " + add_line_2.value + " " + postal_town.value + " " + city.value + " " + county.value + " " + postcode.value + " " + country.value;
        UpdateAddress(add_line_1.value,add_line_2.value,postal_town.value,county.value,city.value,postcode.value.toUpperCase(),country.value);

}
