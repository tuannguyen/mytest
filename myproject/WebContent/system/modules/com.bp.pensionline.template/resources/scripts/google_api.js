// This script file is set up to contain the google interface calls.
//
// Here is the AJAX search developer guide:
// http://code.google.com/apis/ajaxsearch/documentation/
//
// Here is the maps API documentation:
// http://www.google.com/apis/maps/documentation/
//
// Here is a turorial for google maps:
// http://www.econym.demon.co.uk/googlemaps/
//
// Here is a good example of how to bypass the google API for geocoding uk postcodes:
// http://www.tomanthony.co.uk/blog/geocoding-uk-postcodes-with-google-map-api/
//
// The following key will work for all http://localhost:8080/ addresses, and for both the maps and AJAX search API's.
var localhost_google_api_key = "ABQIAAAACprf9FSU-9XampYB7yOTMRTwM0brOpm-All5BF6PoaKBxRWWERTn5gDDMmRlKpsns9G64_LJHuh2-w";

