function onLoadHandler() {
	//alert("load");
	var searchQuery = gup('query');
	ParseSearchHtml(searchQuery);
	ParseHtml(); 
	callTagHandle();
	
	/*** Cookies control **/
//	if( BrowserDetect.browser == "Explorer" && (BrowserDetect.version == "6" || BrowserDetect.version == "7"))
//	{
//	
//	}
//	else
//	{
//		//delete_cookie("browser_supported");
//		if ( !get_cookie ( "browser_supported" ) )
//		{
//		  var browser_confirmed = alert( "You are using a browser that is not currently supported by PensionLine. This means that the site may not display correctly and some of the functional components will not work. Please bear with us while we improve the compatibility." );		
//		  set_cookie ( "browser_supported", "true");
//		}		
//	}	
}

function onUnloadHandler() {
}

function popupHelp(url) {
	window.open(url,'PL4WIKI','width=800,height=600,status=no,resizable=0,top=110,left=200,toolbar=no,menubar=no,scrollbars=yes');
}

function falsify(func) {
	func();
	return false;
}

function eow_onLoadHandler() {
	onLoadHandler();
	eow_onLoad();
}

function eow_onUnloadHandler() {
	onUnloadHandler();
	eow_onUnload();
}

//check session timeout and set to global variable
//setTimeout(sessionTimeout()', sTimeout);
var isSessionTimeout = false;
function sessionTimeout() {
	//alert('Hey bonehead, your session is going to time out unless you do something!');
	isSessionTimeout = true;
	return;
}

var browserUSMessage = 'You are using a browser that is not currently supported by PensionLine. This means that the site may not display correctly and some of the functional components will not work. Please bear with us while we improve the compatibility.';
function set_cookie ( name, value, path, domain, secure )
{
//alert("set cookie");
  	var cookie_string = name + "=" + escape ( value );

	var expires = new Date ();
	expires.setTime(expires.getTime() + (12*3600000));
	cookie_string += "; expires=" + expires.toGMTString();

  	if ( path )
        cookie_string += "; path=" + escape ( path );

  	if ( domain )
        cookie_string += "; domain=" + escape ( domain );
  
  	if ( secure )
        cookie_string += "; secure";
  
  	document.cookie = cookie_string;
}

function get_cookie ( cookie_name )
{
//alert("get cookie");
  	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  	if ( results )
    	return ( unescape ( results[2] ) );
  	else
    	return null;
}

function delete_cookie ( cookie_name )
{
//alert("delete cookie");
  	var cookie_date = new Date ( );  // current date & time
  	cookie_date.setTime ( cookie_date.getTime() - 1 );
  	document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

function ParseSearchHtml(query)
{		
	var rootNode = document.body;
	var queryWords = getQueryWords(query);			
	for(var i=0; i<queryWords.length; i++) 
	{
		var queryWord = queryWords[i];
		if (queryWord.trim().length > 0)
		{	
			TreeWalkSearchIE(rootNode, queryWord.trim());
		}		
	}	
}

function TreeWalkSearchIE(rootNode, queryWord){
        
    if(rootNode == null){
        return;
    }
    
    var length = rootNode.childNodes.length;
	var i=0;
	    		    
	for(i=0; i<length; i++)
	{
	    var child = rootNode.childNodes[i];
	    
	    if(child.nodeType == 1)//is element node
	    {		        
	    	if (child.id && child.id == 'imagery')
	    	{
	    		// no highlights
	    	}
	    	else
	    	{
	    		TreeWalkSearchIE(child, queryWord);
	    	}
	        
	        
	    }else if(child.nodeType == 3)//is text node
	    {
	        HighlightSearchResult(child, queryWord);
	    }
    }    
}

function HighlightSearchResult(child, queryWord)
{
	if (child && queryWord && queryWord.length > 0)
	{		
		
		var childText = child.nodeValue;
				
		if (childText.indexOf('((') < 0 && childText.indexOf('[[') < 0 && child.parentNode && 			
			(child.parentNode.tagName.toLowerCase() == 'p' || child.parentNode.tagName.toLowerCase() == 'font'
			|| child.parentNode.tagName.toLowerCase() == 'b' || child.parentNode.tagName.toLowerCase() == 'strong'
			|| child.parentNode.tagName.toLowerCase() == 'li' || child.parentNode.tagName.toLowerCase() == 'span'))
		{		
			
			var childTextTmp = childText;
			var intIndexOfMatch = childTextTmp.toLowerCase().indexOf(queryWord.toLowerCase());
//			if (child.parentNode.tagName.toLowerCase() == 'b')	
//			{
//				alert(childText + ':' + intIndexOfMatch);
//			}
			var childTmp = child;
			while (intIndexOfMatch != -1)
			{
				// Relace out the current instance.
				var replaceWord = childTextTmp.substring(intIndexOfMatch, intIndexOfMatch + queryWord.length);
				var leftText = childTextTmp.substring(0, intIndexOfMatch);
				var rightText = childTextTmp.substring(intIndexOfMatch + queryWord.length);
				childTmp = insertSearchElement(childTmp, leftText, rightText, "span", replaceWord);
				
//				childText = childText.replace( replaceWord, '<span style="color: #090; background-color: #ff0; font-weight: bold">' + replaceWord + '</span>' )
//				 
//				// Get the index of any next matching substring.
				childTextTmp = childTextTmp.substring(intIndexOfMatch + queryWord.length);
				intIndexOfMatch = childTextTmp.toLowerCase().indexOf( queryWord.toLowerCase() );
			}	
		}
	}
}

function getQueryWords(query)
{
	var isQuoteValid = true;
	var queryTmp = decodeURIComponent(query.replace(/\+/g, ' '));
	var QueryWords = new Array();
	
	// get words in double quote first
	var startQuoteIndex = queryTmp.indexOf('"');
	var endQuoteIndex = -1;
	while (startQuoteIndex != -1)
	{
		endQuoteIndex = queryTmp.indexOf('"', startQuoteIndex + 1);
		
		if (endQuoteIndex < 0)
		{
			isQuoteValid = false;
			break;
		}
		else
		{
			var queryWordExtracted = queryTmp.substring(startQuoteIndex + 1, endQuoteIndex);
			QueryWords.push(queryWordExtracted.trim());
			
			// rebuld the query tmp to remove the word just extracted
			queryTmp = queryTmp.replace(new RegExp('"' + queryWordExtracted + '"', 'g'), "");
		}				
		
		startQuoteIndex = queryTmp.indexOf('"');
	}
	
	if (isQuoteValid)
	{
		var remainWords = queryTmp.split(' ');		
		for(var i=0; i<remainWords.length; i++) {	
			if (remainWords[i].trim().length > 0)
			{				 				  
				QueryWords.push(remainWords[i].trim());
			}			
		}		
	}
	else
	{
		queryTmp = queryTmp.replace(/"/g, '');
		var remainWords = queryTmp.split(' ');
		for(var i=0; i<remainWords.length; i++) {
			if (remainWords[i].trim().length > 0)
			{				
				QueryWords.push(remainWords[i].trim());
			}
		}
	}
	
	return removeDuplicationWords(QueryWords);	
}

function removeDuplicationWords (queryWords)
{
	var filteredWords = new Array();
	if (queryWords && queryWords.length > 0)
	{
		for(var i=0; i<queryWords.length; i++) {
			var word = queryWords[i];
			var wordExisted = false;
			for(var j=0; j<filteredWords.length; j++) {
				if (filteredWords[j].toLowerCase() == word.toLowerCase())
				{
					wordExisted = true;
					break;
				}
			}
			
			if (!wordExisted) filteredWords.push(word);
		}
	}
	
	return filteredWords;
}

function insertSearchElement (child, leftText, rightText, elementName, elementText)
{
	var parent = child.parentNode;
	var rightTextNode;
	
	if (leftText.length)
	{
		leftText = convertBlankToHTMLAtBothEnd(leftText);
		
		var leftTextNode = document.createElement(elementName);
		leftTextNode.innerHTML = leftText;
		parent.insertBefore(leftTextNode, child);
	}
	
	var element = document.createElement(elementName);
	element.style.color = "#090";
	element.style.backgroundColor= "#ff0";
	element.style.fontWeight= "bold";
	element.innerHTML = elementText;
	parent.insertBefore(element, child);
	
	if (rightText.length)
	{
		rightText = convertBlankToHTMLAtBothEnd(rightText);
		rightTextNode = document.createElement(elementName);
		rightTextNode.innerHTML = rightText;
		parent.insertBefore(rightTextNode, child);
	}
	
	parent.removeChild(child);	
	
	return rightTextNode;
}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

function convertBlankToHTMLAtBothEnd (text)
{
	if (text)
	{
		text = text.replace(/^\s+|\s+$/g, ' ');
		var resultText = text.trim();
		
		// replace blank chars at both end to &nbsp;
		for(var i=0; i<text.length; i++) 
		{
			if(text.charAt(i) == ' ' || text.charAt(i) == '&nbsp;')
			{				
				resultText = '&nbsp;' + resultText;				
			}
			else
			{
				break;
			}
		}
		
		for(var i=text.length - 1; i>-1; i--) 
		{
			if(text.charAt(i) == ' '|| text.charAt(i) == '&nbsp;')
			{
				resultText = resultText + '&nbsp;';
			}
			else
			{
				break;
			}
		}
		
		return resultText;
	}
	
	return '';
}

String.prototype.trim = function() 
{
	var trimmed = this.replace(/^\s+|\s+$/g, '') ;
    return trimmed;
}