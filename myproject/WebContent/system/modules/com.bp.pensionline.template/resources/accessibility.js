var tags = new Array( 'div','td','tr','p','b','table','strong','emphasis','a','h1','h2','h3','pre','sub','sup','i','th','cp','ul','ol','li','dt','dd');
var pixelArray =  new Array('10','12','16','20','24','30','40');
var emArray =  new Array('0.7','0.9','1.0','1.5','2.0','2.5','3');
var initSize = 3;

function fontSizer(inc,unit) {
	if (!document.getElementById) 
		return;
	var size = initSize;
		size += inc;
	if (size < 0 ) {
		size = 0;
}
	if (size > 6 ) {
		size = 6;
}
		initSize = size;
		getBody = document.getElementsByTagName('body')[0];
	for (i = 0 ; i < tags.length ; i++ ) {
		getallTags = getBody.getElementsByTagName(tags[i]);
	for (k = 0 ; k < getallTags.length ; k++) 
		getallTags[k].style.fontSize = (unit=='px') ? pixelArray[size]+unit: emArray[size]+unit;
	}
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);



function init() {
    
    document.onkeypress = showKeyPress
}

function showKeyPress(evt) {
    evt = (evt) ? evt : window.event;
	//for FFs
	if(evt.charCode == 45){
		fontSizer(-1,'px');
		return false
	}
	if(evt.charCode == 43){
		fontSizer(1,'px');
		return false
	}
	
	//for IE
	if(evt.keyCode == 45){
		fontSizer(-1,'px');
		return false
	}
	if(evt.keyCode == 43){
		fontSizer(1,'px');
		return false
	}
    
}
