var ninoSelfServiceURL = ajaxurl + "/ChangeNINO?xml=";
var userIdSelfServiceURL = ajaxurl + "/ShowMyID?xml=";
var passwordSelfServiceURL = ajaxurl + "/ForgotPasswordServlet?xml=";
var defaultMsg = "Unable to validate the details provided";

var requestObjectHttp;

var redirectUrl = ajaxurl + "/pl/_login_ask.html";
var errorPageUrl = ajaxurl + "/pl/errorPage.jsp";
var passwordCompleteUrl = ajaxurl + "/pl/Self_services/password_self_service_complete.html";

// PL ID pattern
var plIdPattern = "^([a-zA-Z0-9-_.]){8,30}$";
//var userIdPattern = "^[\\s]*([a-zA-Z0-9-_.]){8,30}[\\s]*$";
var ninoPattern = "(^[\\s]*([a-zA-Z]){2}[0-9]{6}[a-zA-Z]{1})[\\s]*$";
var ninoNumberPattern = "(^[\\s]*([a-zA-Z]){2}[\\s]?[0-9]{6}[\\s]?[a-zA-Z]{1})[\\s]*$";
var yearPattern = "^[\\s]*([0-9]{4})[\\s]*$";
var onlyNumberPattern = "^[\\s]*([0-9]+)[\\s]*$";

// Change NINO
function changeNINO(plName){
	var reportingURL = ajaxurl + "/ChangeNINO";
	var params = "plid=" + plName;
	
	requestObjectHttp = createXmlHttpRequestObject();

	requestObjectHttp.open("POST", reportingURL, true);

	requestObjectHttp.onreadystatechange = ChangeNINOHandle;		
	    
	requestObjectHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	requestObjectHttp.setRequestHeader("Content-length", params.length);
	requestObjectHttp.setRequestHeader("Connection", "close");	
	
	requestObjectHttp.send(params);	
}

function ChangeNINOHandle() {
	if (requestObjectHttp.readyState == 4) {
		// Check that a successful server response was received
	  	if (requestObjectHttp.status == 200){
			var result = requestObjectHttp.responseText;
			showloading(false);
			
			if (result == "NINO has been changed successfully."){
				if (openwindow()) {
					window.location = redirectUrl;
				}
			}else {
	             document.getElementById("error_text").innerText = result;				
			}
		}
	}
}

function openwindow(){
	mywindow = dhtmlmodal.open('NINO', 'div', 'ninobox', 'Username self-service', 'width=350px,height=200px,center=1,resize=0,scrolling=1');
	mywindow.onclose = function(){
		return true;
	};
}

function closewindow() {
	mywindow.hide();
	window.location = redirectUrl;
}

function showloading(bval) {
	// Loading variables
	var ninoLoadID = document.getElementById('loading');
	var ninoButtonID = document.getElementById('dochange');
	
	if (bval == true) {
		ninoLoadID.style.display = "";
		ninoButtonID.style.display = "none";
	} else {
		ninoLoadID.style.display = "none";
		ninoButtonID.style.display = "";
	}
}

function validateNINOInput(plName) {
	var result = "";
	showloading(true);
	
	if (plName.length < 8) {
		result = "Your username must be at least eight characters long.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_new_username").focus();
		showloading(false);
		return;
	}
	
	if (plName.length > 30) {
		result = "Your username must be less than thirty characters long.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_new_username").focus();
		showloading(false);
		return;
	}
	
	var reg = new RegExp(plIdPattern);
	if (!document.getElementById("_new_username").value.match(reg)) {
		result = "Your username must contains only alpha numeric characters,"
			+ " hypens, underscores and full stops.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_new_username").focus();
		showloading(false);
		return;
	}
	
	// Everything is OK so that go to change the NINO
	document.getElementById("error_text").innerText = "";
	plName = plName.toLowerCase();
	changeNINO(plName);
}

// UserID self service
function validateUserIdSelfservice(nino, dob, postcode) {
	var result = "";
	showloading(true);
	
	if (nino.length <= 0) {
		result = "Please enter your NI Number.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_nino").focus();
		showloading(false);
		return;
	}
	
	var reg = new RegExp(ninoNumberPattern);
	var ninoValue = correctString(document.getElementById("_request_nino").value);
	if (!ninoValue.match(reg)) {
		result = "The NI Number is not in format of a NI Number.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_nino").focus();
		showloading(false);
		return;
	}
	
	var yearValue = document.getElementById("_request_year").value;
	if (yearValue.length <= 0) {
		result = "Please enter your year of birth.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_year").focus();
		showloading(false);
		return;
	}
	
	var yearReg = new RegExp(yearPattern);
	if (!document.getElementById("_request_year").value.match(yearReg)) {
		result = "The year of birth is not correct.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_year").focus();
		showloading(false);
		return;
	}
	
	var numericReg = new RegExp(onlyNumberPattern);
	if (postcode.length > 0 && (postcode.match(numericReg))) {
		result = "The postcode is not correct.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_postcode").focus();
		showloading(false);
		return;
	}
	
	// Everything is OK so that go to change the NINO
	document.getElementById("error_text").innerText = "";
	nino = correctString(nino);
	dob = correctString(dob);
	
	getUserId(nino, dob, postcode);
}

function getUserId(nino, dob, postcode) {
	var xml = "";
	
	var day = document.getElementById("_request_day").value;
	var month = document.getElementById("_request_month").value;
	var date = dob + "-" + month + "-" + day;
	
	xml += "<AjaxParameterRequest>\n";		
	xml += "		<NINO><![CDATA[" + nino + "]]></NINO>\n";
	xml += "		<DOB><![CDATA[" + date + "]]></DOB>\n";
	xml += "		<POSTCODE><![CDATA[" + postcode + "]]></POSTCODE>\n";
	xml += "</AjaxParameterRequest>\n";
	
	var url = userIdSelfServiceURL + escape(xml);
	
	requestObjectHttp = createXmlHttpRequestObject();
	
	requestObjectHttp.open("POST", url);

	requestObjectHttp.onreadystatechange = GetUserIDHandle;
	
	requestObjectHttp.send(null);
}

function GetUserIDHandle() {
	if (requestObjectHttp.readyState == 4) {
	  	if (requestObjectHttp.status == 200){
			showloading(false);
			
			var message = requestObjectHttp.responseXML.
					getElementsByTagName("Message")[0].childNodes[0].nodeValue;
			var userid = requestObjectHttp.responseXML.
					getElementsByTagName("UserId")[0].childNodes[0].nodeValue;
			
			if (message == "Success"){
				document.getElementById("inputBox").style.display = "none";
				document.getElementById("mandatory_text").style.display = "none";
				var reg = new RegExp(ninoPattern);
				if (userid.match(reg)) {
					userid = "\"YOUR NATIONAL INSURANCE NUMBER\"";
				} else {
					userid = "Your PensionLine ID: " + userid;					
				}
				
				document.getElementById("result").innerText = userid;
			} else if (message == "EE101") {
				document.getElementById("error_text").innerText = 
					document.getElementById("ee101").innerText;
			} else {
	            document.getElementById("error_text").innerText = defaultMsg;				
			}
		}
	}
}

function validatePasswdSelfservice() {
	var result = "";
	var reg = null;
	showloading(true);
	
	var year = document.getElementById("_request_year").value;
	var ninoName = document.getElementById("_request_nino").value;
	var postCode = document.getElementById("_request_postcode").value;
	
	var day = document.getElementById("_request_day").value;
	var month = document.getElementById("_request_month").value;
	var dateOfBirth = year + "-" + month + "-" + day;
	
	if (ninoName.length <= 0) {
		result = "Please enter your NI Number.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_nino").focus();
		showloading(false);
		return;
	}
	
	reg = new RegExp(ninoNumberPattern);
	var ninoValue = correctString(ninoName);
	if (!ninoValue.match(reg)) {
		result = "The NI Number is not in format of a NI Number.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_nino").focus();
		showloading(false);
		return;
	}
	
	if (year.length <= 0) {
		result = "Please enter your year of birth.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_year").focus();
		showloading(false);
		return;
	}
	
	reg = new RegExp(yearPattern);
	if (!year.match(reg)) {
		result = "The year of birth is not correct.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_year").focus();
		showloading(false);
		return;
	}
	
	var numericReg = new RegExp(onlyNumberPattern);
	if (postCode.length > 0 && (postCode.match(numericReg))) {
		result = "The postcode is not correct.";
		document.getElementById("error_text").innerText = result;
		document.getElementById("_request_postcode").focus();
		showloading(false);
		return;
	}
	
	// Everything is OK so that go to change the NINO
	document.getElementById("error_text").innerText = "";
	ninoName = correctString(ninoName);
	dateOfBirth = correctString(dateOfBirth);
	
	getUserPassword(ninoName, dateOfBirth, postCode);
}

function getUserPassword(ninoName, dateOfBirth, postCode) {
	var xml = "";
	
	xml += "<AjaxParameterRequest>\n";	
	xml += "		<NINO><![CDATA[" + ninoName + "]]></NINO>\n";
	xml += "		<DOB><![CDATA[" + dateOfBirth + "]]></DOB>\n";
	xml += "		<POSTCODE><![CDATA[" + postCode + "]]></POSTCODE>\n";
	xml += "</AjaxParameterRequest>\n";
	
	var url = passwordSelfServiceURL + escape(xml);
	
	requestObjectHttp = createXmlHttpRequestObject();
	requestObjectHttp.open("POST", url);

	requestObjectHttp.onreadystatechange = ForgotPasswordHandle;
	
	requestObjectHttp.send(null);
}

function ForgotPasswordHandle() {
	if (requestObjectHttp.readyState == 4) {
	  	if (requestObjectHttp.status == 200){
			showloading(false);
			
			var result = requestObjectHttp.responseText;
			showloading(false);
			
			if (result == "Success") {
				window.location = passwordCompleteUrl;
			} else if (result == "EE101") {
				result = document.getElementById("ee101").innerText;
				if ((result) && (result.length <= 0)) {
					result = defaultMsg;
				}
				document.getElementById("error_text").innerText = result;
			} else {
				document.getElementById("error_text").innerText = defaultMsg;
			}  // end-if-else
		}
	}
}

function correctString(name) {
	var result = "";
	
	if (name.length > 0) {
		result = name.replace(/^\s*|\s*|\s*$/g,'');
	}
	
	return result;
}

