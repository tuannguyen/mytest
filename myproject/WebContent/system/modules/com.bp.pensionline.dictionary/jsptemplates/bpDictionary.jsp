<%@ page session="false" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<div class="element">

<cms:include file="list_content.html" element="header" editable="true"/> 

<cms:contentload collector="${property.collector}" param="/dictionary/|bp_pensionline_dictionary" editable="true">

<div class="element">

<h3><cms:contentshow element="Title" /></h3>

<p>
<cms:contentshow element="Definition" /></br>
</p>
</div>

</cms:contentload>


</div>


