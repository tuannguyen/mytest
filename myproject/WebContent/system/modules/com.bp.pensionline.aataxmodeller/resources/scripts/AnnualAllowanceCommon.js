function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}

//This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked)
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
  //cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {  
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.element.style.zIndex = '999';
  _dynarch_popupCalendar.element.style.position = 'absolute';
  var pos = Calendar.getAbsolutePos(el);
  _dynarch_popupCalendar.showAt(pos.x, pos.y + el.offsetHeight);   
}


function _findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
        
    return curleft;
  }  
function _findPosY(obj)
{
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
        
    return curtop;
}


function formatEncode(str)
{
	if (str && str.length > 0)
	{
		var newStr = str.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');		
		return newStr;
	}
	return str;
}

function getDouble(str)
{
	var doubleValue = 0;
	if (str)
	{
		try{
			var newStr = str.replace(/,/g, '').replace(/\%/g,'');
			doubleValue = parseFloat(newStr);
			if (isNaN(doubleValue))
			{
				doubleValue = 0;
			}
		}
		catch (err){}
		
	}
	
	return doubleValue;
}

function getInt(str)
{
	var doubleValue = 0;
	if (str)
	{
		try{
			var newStr = str.replace(/,/g, '').replace(/\%/g,'');
			doubleValue = parseInt(newStr);
			if (isNaN(doubleValue))
			{
				doubleValue = 0;
			}
		}
		catch (err){}
		
	}
	
	return doubleValue;
}


function formatEncode(str)
{
	if (str && str.length > 0)
	{
		var newStr = str.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');		
		return newStr;
	}
	return str;
}

String.prototype.trim = function() 
{
	var trimmed = this.replace(/^\s+|\s+$/g, '') ;
    return trimmed;
}


function getOrderNumber(numberStr)
{
	if (typeof numberStr  == 'string')
	{
		if (numberStr.length > 0)
		{
			var lastDigit = numberStr.substring(numberStr.length - 1)
			if (lastDigit == '1') return numberStr + 'st';
			if (lastDigit == '2') return numberStr + 'nd';
			if (lastDigit == '3') return numberStr + 'rd';
		}
	}
	else if (typeof numberStr  == 'number')
	{
		var lastDigit = numberStr % 10;
		
		if (lastDigit == 1) return numberStr + 'st';
		if (lastDigit == 2) return numberStr + 'nd';
		if (lastDigit == 3) return numberStr + 'rd';
	}
	
	return numberStr + 'th';
}

function formatToDecimal(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function roundUpTo100(number)
{
	if (number % 100 == 0)
	{
		return number;
	}
	return 100 * Math.round(number/100 + 0.5);
}

function roundDownTo100(number)
{
	if (number % 100 == 0)
	{
		return number;
	}
	return 100 * Math.round(number/100 - 0.5);
}

function roundUpTo1000(number)
{
	if (number % 1000 == 0)
	{
		return number;
	}
	return 1000 * Math.round(number/1000 + 0.5);
}

function roundDownTo1000(number)
{
	if (number % 1000 == 0)
	{
		return number;
	}
	return 1000 * Math.round(number/1000 - 0.5);
}