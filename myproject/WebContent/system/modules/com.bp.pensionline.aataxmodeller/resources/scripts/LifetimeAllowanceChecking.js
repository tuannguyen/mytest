var aaLTARequest = null;

var SLIDER_CONFIG = {
        'b_vertical' : false,
        'b_watch': true,
        'n_controlWidth': 320,
        'n_controlHeight': 16,
        'n_sliderWidth': 16,
        'n_sliderHeight': 15,
        'n_pathLeft' : 1,
        'n_pathTop' : 1,
        'n_pathLength' : 303,
        's_imgControl': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_bg.gif',
        's_imgSlider': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_sl.gif',
        'n_zIndex': 1
}
var SLIDER_SALARY = {
        
        's_name': 'input_salary',
        'n_minValue' : 0,
        'n_maxValue' : 400000,
        'n_value' : 22000,
        'n_step' : 1000
}       

var member_details_fields   = new Array(
	"reference",
	"nino",
	"name",
	"gender",
	"date_of_birth",
	"joined_company",
	"joined_scheme",
	"birthday_55th",
	"birthday_60th",
	"birthday_65th",
	"salary",
	"avc_fund",
	"aver_fund",
	"tvin_a",
	"tvin_b",
	"tvin_c",
	"augmentation",
	"lta",
	"banked_egp"
	);
	
var scheme_history_node_name = 'SchemeHistory';
var absence_history_node_name = 'AbsenceHistory';
var headroom_history_node_name = 'ServiceHistory';
var salary_history_node_name = 'SalaryHistory';

var salarySlider = null;

var salaryBallon = null;
var popupInitId;

var bgroup ='';
var refno ='';
var sdate = '';

function loadMember()
{
	
	bgroup = document.getElementById("bgroup").value.trim();
	refno = document.getElementById("refno").value.trim();
	sdate = document.getElementById("sdate").value;
	
	if (refno == null || refno.length == 0)
	{
		alert('Please input member\'s refno');
		return false;
	}
	else
	{
		for(var i=0; i<refno.length; i++) {
			var c = refno.charAt(i);
			
			if (c  < '0' || c > '9')
			{
				alert('Please input a valid member\'s refno');
				return false;
			}
		}
		
	}
	
	systemDate = sdate;	
	
		
	var aaTaxModellerMemberLoadURL = ajaxurl + "/MemberLoadingService";
	var params = "bgroup=" + bgroup + "&refno=" + refno + "&sdate=" + sdate;

	aaLTARequest = createXmlHttpRequestObject();
	aaLTARequest.open("POST", aaTaxModellerMemberLoadURL, true);

	aaLTARequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaLTARequest.setRequestHeader("Content-length", params.length);
    aaLTARequest.setRequestHeader("Connection", "close");	
    
	aaLTARequest.onreadystatechange = handleMemberLoadResult;		       
	
	whizzy(true, 'member_running_div', 'member_result_div');
	whizzy(true, 'headroom_running_div', 'headroom_result_div');
		
	aaLTARequest.send(params);	
}

function handleMemberLoadResult(){

   	if (aaLTARequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaLTARequest.status == 200) {
			//alert(aaLTARequest.responseText.length);
			var response = createResponseXML(aaLTARequest.responseText);					
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					
					if (document.getElementById('member_running_div'))
					{
						document.getElementById('member_running_div').style.display = 'none';
					}
					if (document.getElementById('headroom_running_div'))
					{
						document.getElementById('headroom_running_div').style.display = 'none';
					}
					return false;
				}
				else
				{
					// member details
					handleMemberData(response);	
					
					whizzy(false, 'member_running_div', 'member_result_div');
					
					// load headroom data
					getMemberLTA();
				} 					
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}

function handleMemberData (response)
{
	if (response)
	{
		// update member details
		for(var i=0; i<member_details_fields.length; i++) 
		{
			var tag_name = member_details_fields[i];
			var update_nodes = response.getElementsByTagName(tag_name);
			
			if (update_nodes.length > 0)
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
				}
			}
			else
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = "&nbsp;";
				}							
			}
		}
		
		// update scheme history
		var schemeHistoryNodes = response.getElementsByTagName(scheme_history_node_name);
		var schemeHistoriesInnerHTML = '<table class="scheme_absence_table" cellspacing="0" cellpadding="0" id="scheme_absence_table">' +
				'<tr><th class="longlabel" colspan="5">Scheme and absence history</th></tr>';
		schemeHistoriesInnerHTML += '<tr><th>From</th><th>To</th><th>Category</th><th colspan="2">Accrual</th></tr>';
		if (schemeHistoryNodes.length > 0)
		{
			for(var i=0; i<schemeHistoryNodes.length; i++) 
			{
				var schemeHistoryInnerHTML = '<tr>';
				var schemeHistoryNode = schemeHistoryNodes[i];
				if (schemeHistoryNode.getElementsByTagName('from').length > 0 && schemeHistoryNode.getElementsByTagName('from')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				if (schemeHistoryNode.getElementsByTagName('to').length > 0 && schemeHistoryNode.getElementsByTagName('to')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (schemeHistoryNode.getElementsByTagName('category').length > 0 && schemeHistoryNode.getElementsByTagName('category')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('category')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (schemeHistoryNode.getElementsByTagName('accrual').length > 0 && schemeHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue)
				{
					schemeHistoryInnerHTML += '<td colspan="2">' + schemeHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td colspan="2">&nbsp;</td>';
				}
				
				schemeHistoryInnerHTML += '</tr>';
				schemeHistoriesInnerHTML += schemeHistoryInnerHTML;
			}
		}
		else
		{
			schemeHistoriesInnerHTML += '<tr><td colspan="5">&nbsp;</td></tr>';
		}
							
		// update absence history		
		var absenceHistoriesInnerHTML = '<tr><th>From</th><th>To</th><th>Type</th><th>Worked</th><th>Employed</th></tr>';
		
		var absenceHistoryNodes = response.getElementsByTagName(absence_history_node_name);
		if (absenceHistoryNodes.length > 0)
		{
			for(var i=0; i<absenceHistoryNodes.length; i++) 
			{
				var absenceHistoryNode = absenceHistoryNodes[i];
				var absenceHistoryInnerHTML = '<tr>';
				if (absenceHistoryNode.getElementsByTagName('from').length > 0 && absenceHistoryNode.getElementsByTagName('from')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('to').length > 0 && absenceHistoryNode.getElementsByTagName('to')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('type').length > 0 && absenceHistoryNode.getElementsByTagName('type')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('type')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('worked').length > 0 && absenceHistoryNode.getElementsByTagName('worked')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('worked')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('worked').length > 0 && absenceHistoryNode.getElementsByTagName('employed')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('employed')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				absenceHistoryInnerHTML += '</tr>';
				absenceHistoriesInnerHTML += absenceHistoryInnerHTML;
			}
		}
		else
		{
			absenceHistoriesInnerHTML += '<tr><td colspan="5">&nbsp;</td></tr>';
		}
		
		
		if (document.getElementById('scheme_absence_table'))
		{
			document.getElementById('scheme_absence_table').innerHTML = schemeHistoriesInnerHTML + absenceHistoriesInnerHTML + '</table>';
		}
		
		// salary history
		var salaryHistoriesInnerHTML = '<table class="scheme_absence_table" cellspacing="0" cellpadding="0">' +
				'<tr><th class="longlabel" colspan="2">Salary history (Show last 3 pay rises)</th></tr>';				
		salaryHistoriesInnerHTML += '<tr><th>Effective</th><th>Salary</th></tr>';
		var salaryHistoryNodes = response.getElementsByTagName(salary_history_node_name);

		if (salaryHistoryNodes.length > 0)
		{
			for(var i=0; i<salaryHistoryNodes.length; i++) 
			{
				var salaryHistoryNode = salaryHistoryNodes[i];
				var salaryHistoryInnerHTML = '<tr>';
				if (salaryHistoryNode.getElementsByTagName('Effective').length > 0)
				{
					salaryHistoryInnerHTML += '<td>' + salaryHistoryNode.getElementsByTagName('Effective')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					salaryHistoryNode += '<td>&nbsp;</td>';
				}
				
				if (salaryHistoryNode.getElementsByTagName('Salary').length > 0)
				{
					salaryHistoryInnerHTML += '<td>&pound;' + salaryHistoryNode.getElementsByTagName('Salary')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					salaryHistoryNode += '<td>&nbsp;</td>';
				}				
				
				salaryHistoryInnerHTML += '</tr>';
				salaryHistoriesInnerHTML += salaryHistoryInnerHTML;
			}
		}
		else
		{
			salaryHistoriesInnerHTML += '<tr><td colspan="2">&nbsp;</td></tr>';
		}
		
		
		if (document.getElementById('salary_table'))
		{
			document.getElementById('salary_table').innerHTML = salaryHistoriesInnerHTML + '</table>';
		}		 	
	}
}

function getMemberLTA()
{	
	var updateMemberDataURL = ajaxurl + "/LifetimeAllowanceCheckingService";
	aaLTARequest = createXmlHttpRequestObject();
	aaLTARequest.open("POST", updateMemberDataURL, true);

	aaLTARequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaLTARequest.setRequestHeader("Content-length", 0);
    aaLTARequest.setRequestHeader("Connection", "close");	
    
	aaLTARequest.onreadystatechange = handleGetMemberLTAResponse;		     

	aaLTARequest.send(null);			
}

function handleGetMemberLTAResponse ()
{
	if (aaLTARequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaLTARequest.status == 200) {
			var response = createResponseXML(aaLTARequest.responseText);
			//alert(aaLTARequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					handleHeadroomData(response);
					
					// update AaMarginalRateChoice
					var minSalary = response.getElementsByTagName('MinSalary')[0].firstChild.nodeValue;
					var maxSalary = response.getElementsByTagName('MaxSalary')[0].firstChild.nodeValue;
					var salary =  response.getElementsByTagName('Salary')[0].firstChild.nodeValue;
					var pension =  response.getElementsByTagName('Pension')[0].firstChild.nodeValue;
					var lta =  response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var overLTA =  response.getElementsByTagName('OverLTA')[0].firstChild.nodeValue;
					
					if (document.getElementById('current_salary'))
					{
						document.getElementById('current_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('modelled_salary'))
					{
						document.getElementById('modelled_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('current_pension'))
					{
						document.getElementById('current_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('modelled_pension'))
					{
						document.getElementById('modelled_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('current_lta'))
					{
						document.getElementById('current_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('modelled_lta'))
					{
						document.getElementById('modelled_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('current_overlta'))
					{
						document.getElementById('current_overlta').innerHTML = overLTA;
					}
					if (document.getElementById('modelled_overlta'))
					{
						document.getElementById('modelled_overlta').innerHTML = overLTA;
					}
						
					whizzy(false, 'headroom_running_div', 'headroom_result_div');
					
					// create pop ups before create the slider		
					if (salaryBallon == null)
					{
						initPopups();						
					}			
					initSalarySlider(getInt(minSalary), getInt(maxSalary), getInt(salary));			 			
		
				}	
			}
			catch(err){
				alert("XML Parser error." + err);	
			}					
		}				
	}	
}


function handleHeadroomData (response)
{
	if (response)
	{
		var headroomNode = response.getElementsByTagName('Headroom')[0];
		// update scheme history
		var headroomHistoriesInnerHTML = '<table class="headroom_table" cellspacing="0" cellpadding="0" id="scheme_absence_table">' +
				'<tr><th class="longlabel" colspan="12">Service history</th></tr>';
		headroomHistoriesInnerHTML += '<tr><th width="100">From</th><th width="100">To</th><th width="45">Cat</th>' +
				'<th width="50">years</th><th width="50">days</th><th width="50">total years</th><th width="40">FTE</th>' +
				'<th width="60">service</th><th width="45">accru\'</th><th width="40">ERF</th>' + 
				'<th width="60">accrued</th><th width="40">60th</th></tr> ';
				
		var headroomHistoryNodes = headroomNode.getElementsByTagName(headroom_history_node_name);

		for(var i=0; i<headroomHistoryNodes.length; i++) 
		{
			var headroomHistoryNode = headroomHistoryNodes[i];			

			headroomHistoryInnerHTML = '<tr>';
			if (headroomHistoryNode.getElementsByTagName('from').length > 0 && headroomHistoryNode.getElementsByTagName('from')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
	
			if (headroomHistoryNode.getElementsByTagName('to').length > 0 && headroomHistoryNode.getElementsByTagName('to')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('category').length > 0 && headroomHistoryNode.getElementsByTagName('category')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('category')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('years').length > 0 && headroomHistoryNode.getElementsByTagName('years')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('years')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('days').length > 0 && headroomHistoryNode.getElementsByTagName('days')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('days')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('tyears').length > 0 && headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('fte').length > 0 && headroomHistoryNode.getElementsByTagName('fte')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('fte')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('service').length > 0 && headroomHistoryNode.getElementsByTagName('service')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('service')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('accrual').length > 0 && headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('erf').length > 0 && headroomHistoryNode.getElementsByTagName('erf')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('erf')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}			
			
			if (headroomHistoryNode.getElementsByTagName('accrued').length > 0 && headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + '&pound;' + formatToDecimal(getInt(headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue)) + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('service60th').length > 0 && headroomHistoryNode.getElementsByTagName('service60th')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('service60th')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			headroomHistoryInnerHTML += '</tr>';
			headroomHistoriesInnerHTML += headroomHistoryInnerHTML;
		}

		// total row
		var totalYears = '&nbsp;';
		var totalServiceYears = '&nbsp;';
		var totalAccrued = '&nbsp;';
		var totalServiceYears60th = '&nbsp;';
		if (headroomNode.getElementsByTagName('TotalYears') && headroomNode.getElementsByTagName('TotalYears')[0].firstChild)
		{
			totalYears = headroomNode.getElementsByTagName('TotalYears')[0].firstChild.nodeValue;
		}
		if (headroomNode.getElementsByTagName('TotalServiceYears') && headroomNode.getElementsByTagName('TotalServiceYears')[0].firstChild)
		{
			totalServiceYears = headroomNode.getElementsByTagName('TotalServiceYears')[0].firstChild.nodeValue;
		}
		if (headroomNode.getElementsByTagName('TotalAccrued') && headroomNode.getElementsByTagName('TotalAccrued')[0].firstChild)
		{
			totalAccrued = formatToDecimal(getInt(headroomNode.getElementsByTagName('TotalAccrued')[0].firstChild.nodeValue));
		}
		if (headroomNode.getElementsByTagName('TotalServiceYearsAt60th') && headroomNode.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild)
		{
			totalServiceYears60th = headroomNode.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild.nodeValue;
			totaleServiceYearAt60 = getDouble(totalServiceYears60th);
		}		

		headroomHistoriesInnerHTML += '<tr><td class="empty" colspan=5>&nbsp;</td>' +
				'<td class="total">' + totalYears + 
				'</td><td class="empty"">&nbsp;</td><td class="total">' + totalServiceYears +
				'</td><td class="empty" >&nbsp;</td>' + '</td><td class="empty" >&nbsp;</td>' + 
				'<td class="total">' + '&pound;' + totalAccrued +
				'</td><td class="total">' + totalServiceYears60th + '</td></tr> ';

		if (document.getElementById('headroom_table'))
		{
			document.getElementById('headroom_table').innerHTML = headroomHistoriesInnerHTML + '</table>';
		}			
	}
}

function modelMemberLTA()
{	
	var salary = document.getElementById('input_salary').value;
	var updateMemberDataURL = ajaxurl + "/LifetimeAllowanceCheckingService?salary=" + salary;
	aaLTARequest = createXmlHttpRequestObject();
	aaLTARequest.open("POST", updateMemberDataURL, true);

	aaLTARequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaLTARequest.setRequestHeader("Content-length", 0);
    aaLTARequest.setRequestHeader("Connection", "close");	
    
	aaLTARequest.onreadystatechange = handleModelMemberLTAResponse;		     
	
    salaryBallon.hide();
	whizzy(true, 'headroom_running_div', 'headroom_result_div');
	
	aaLTARequest.send(null);			
}

function handleModelMemberLTAResponse ()
{
	if (aaLTARequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaLTARequest.status == 200) {
			var response = createResponseXML(aaLTARequest.responseText);
			//alert(aaLTARequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					handleHeadroomData(response);
					
					// update AaMarginalRateChoice
					var salary =  response.getElementsByTagName('Salary')[0].firstChild.nodeValue;
					var pension =  response.getElementsByTagName('Pension')[0].firstChild.nodeValue;
					var lta =  response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var overLTA =  response.getElementsByTagName('OverLTA')[0].firstChild.nodeValue;
					
					if (document.getElementById('modelled_salary'))
					{
						document.getElementById('modelled_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('modelled_pension'))
					{
						document.getElementById('modelled_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('modelled_lta'))
					{
						document.getElementById('modelled_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('modelled_overlta'))
					{
						document.getElementById('modelled_overlta').innerHTML = overLTA;
					}
					
					whizzy(false, 'headroom_running_div', 'headroom_result_div');		
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
			}					
		}				
	}	
}

function initPopups()
{
	salaryBallon= new HelpBalloon({
		id: 'salary_balloon',
		returnElement: true,
		title: '',
		contentId: 'popup_CurrentSalary',
		icon:$('info_CurrentSalary'),
		iconStyle: {						
			'verticalAlign': 'middle'
		},
		balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
		pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
		button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
		balloonFixedX: 205
	});		
	
	$('popup_CurrentSalary').remove();		
}

function initSalarySlider(minSalary, maxSalary, currentSalary)
{
	SLIDER_SALARY.n_minValue = roundDownTo1000(minSalary);
	SLIDER_SALARY.n_maxValue = roundUpTo1000(maxSalary);
	SLIDER_SALARY.n_value = currentSalary;
	if (document.getElementById('salary_slider'))
	{
		salarySlider = new slider('salary_slider', SLIDER_SALARY, SLIDER_CONFIG);
		salarySlider.f_setValue(SLIDER_SALARY.n_value);
	}	
}

function whizzy (isWhizzy, runningId, resultId)
{
	if (isWhizzy)
	{
		if (document.getElementById(runningId))
		{
			document.getElementById(runningId).style.display = 'block';
		}
		if (document.getElementById(resultId))
		{
			document.getElementById(resultId).style.display = 'none';
		}
	}
	else
	{
		if (document.getElementById(runningId))
		{
			document.getElementById(runningId).style.display = 'none';
		}
		if (document.getElementById(resultId))
		{
			document.getElementById(resultId).style.display = 'block';
		}
	}
}