var aaLTARequest = null;

var SLIDER_CONFIG = {
        'b_vertical' : false,
        'b_watch': true,
        'n_controlWidth': 320,
        'n_controlHeight': 16,
        'n_sliderWidth': 16,
        'n_sliderHeight': 15,
        'n_pathLeft' : 1,
        'n_pathTop' : 1,
        'n_pathLength' : 303,
        's_imgControl': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_bg.gif',
        's_imgSlider': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_sl.gif',
        'n_zIndex': 1
}
var SLIDER_SALARY = {
        
        's_name': 'input_salary',
        'n_minValue' : 0,
        'n_maxValue' : 400000,
        'n_value' : 22000,
        'n_step' : 1000
}       



var salarySlider = null;

var balloonArr = new Array();
var popupInitId;

function initLTATables()
{	
	getMemberLTA();
}

function getMemberLTA()
{	
	var updateMemberDataURL = ajaxurl + "/LifetimeAllowanceService";
	aaLTARequest = createXmlHttpRequestObject();
	aaLTARequest.open("POST", updateMemberDataURL, true);

	aaLTARequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaLTARequest.setRequestHeader("Content-length", 0);
    aaLTARequest.setRequestHeader("Connection", "close");	
    
	aaLTARequest.onreadystatechange = handleGetMemberLTAResponse;		     
	
	whizzyModel(true, 'current_salary');
	whizzyModel(true, 'modelled_salary');
	whizzyModel(true, 'current_pension');
	whizzyModel(true, 'modelled_pension');
	whizzyModel(true, 'current_lta');
	whizzyModel(true, 'modelled_lta');
	whizzyModel(true, 'current_overlta');
	whizzyModel(true, 'modelled_overlta');
	aaLTARequest.send(null);			
}

function handleGetMemberLTAResponse ()
{
	if (aaLTARequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaLTARequest.status == 200) {
			var response = createResponseXML(aaLTARequest.responseText);
			//alert(aaLTARequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					// update AaMarginalRateChoice
					var minSalary = response.getElementsByTagName('MinSalary')[0].firstChild.nodeValue;
					var maxSalary = response.getElementsByTagName('MaxSalary')[0].firstChild.nodeValue;
					var salary =  response.getElementsByTagName('Salary')[0].firstChild.nodeValue;
					var pension =  response.getElementsByTagName('Pension')[0].firstChild.nodeValue;
					var lta =  response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var overLTA =  response.getElementsByTagName('OverLTA')[0].firstChild.nodeValue;
					
					if (document.getElementById('current_salary'))
					{
						document.getElementById('current_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('modelled_salary'))
					{
						document.getElementById('modelled_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('current_pension'))
					{
						document.getElementById('current_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('modelled_pension'))
					{
						document.getElementById('modelled_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('current_lta'))
					{
						document.getElementById('current_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('modelled_lta'))
					{
						document.getElementById('modelled_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('current_overlta'))
					{
						document.getElementById('current_overlta').innerHTML = overLTA;
					}
					if (document.getElementById('modelled_overlta'))
					{
						document.getElementById('modelled_overlta').innerHTML = overLTA;
					}
					
					// create popups before creating the slider
					initPopups();
					
					initSalarySlider(getInt(minSalary), getInt(maxSalary), getInt(salary));

					// parse data tags
					ParseHtml(); 
					callTagHandle();		
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false, 'current_salary');
				whizzyModel(false, 'modelled_salary');
				whizzyModel(false, 'current_pension');
				whizzyModel(false, 'modelled_pension');
				whizzyModel(false, 'current_lta');
				whizzyModel(false, 'modelled_lta');
				whizzyModel(false, 'current_overlta');
				whizzyModel(false, 'modelled_overlta');		
			}					
		}				
	}	
}

function modelMemberLTA()
{	
	var salary = document.getElementById('input_salary').value;
	var updateMemberDataURL = ajaxurl + "/LifetimeAllowanceService?salary=" + salary;
	aaLTARequest = createXmlHttpRequestObject();
	aaLTARequest.open("POST", updateMemberDataURL, true);

	aaLTARequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaLTARequest.setRequestHeader("Content-length", 0);
    aaLTARequest.setRequestHeader("Connection", "close");	
    
	aaLTARequest.onreadystatechange = handleModelMemberLTAResponse;		     
	
	whizzyModel(true, 'modelled_salary');
	whizzyModel(true, 'modelled_pension');
	whizzyModel(true, 'modelled_lta');
	whizzyModel(true, 'modelled_overlta');
	
    for(var i=0; i<balloonArr.length; i++) {
		balloonArr[i].hide();
	}
		
	aaLTARequest.send(null);			
}

function handleModelMemberLTAResponse ()
{
	if (aaLTARequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaLTARequest.status == 200) {
			var response = createResponseXML(aaLTARequest.responseText);
			//alert(aaLTARequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					// update AaMarginalRateChoice
					var salary =  response.getElementsByTagName('Salary')[0].firstChild.nodeValue;
					var pension =  response.getElementsByTagName('Pension')[0].firstChild.nodeValue;
					var lta =  response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var overLTA =  response.getElementsByTagName('OverLTA')[0].firstChild.nodeValue;
					
					if (document.getElementById('modelled_salary'))
					{
						document.getElementById('modelled_salary').innerHTML = '&pound;' + salary;
					}
					if (document.getElementById('modelled_pension'))
					{
						document.getElementById('modelled_pension').innerHTML = '&pound;' + pension;
					}
					if (document.getElementById('modelled_lta'))
					{
						document.getElementById('modelled_lta').innerHTML = '&pound;' + lta;
					}
					if (document.getElementById('modelled_overlta'))
					{
						document.getElementById('modelled_overlta').innerHTML = overLTA;
					}		
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false, 'modelled_salary');
				whizzyModel(false, 'modelled_pension');
				whizzyModel(false, 'modelled_lta');
				whizzyModel(false, 'modelled_overlta');
			}					
		}				
	}	
}

function initPopups()
{
	var contentElement = document.getElementById('content');
	if (contentElement)
	{
		var infoImgs = contentElement.getElementsByTagName('img');
		var infoHrefs = contentElement.getElementsByTagName('a');
		for(var i=0; i<infoImgs.length; i++) 
		{
			var infoId = infoImgs[i].id
			if (infoId && infoId.indexOf('info_') == 0)
			{
				var tagName = infoId.substring(5);
				var popupBalloon = new HelpBalloon({
					returnElement: true,
					title: '',
					contentId: 'popup_' + tagName,
					icon:infoImgs[i],
					iconStyle: {						
						'verticalAlign': 'middle'
					},
					balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
					pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
					button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
					balloonFixedX: 205
				});	
				
				balloonArr.push(popupBalloon);	
				
				// remove the content after clone to balloon				
			}					
		}
		
		for(var i=0; i<infoHrefs.length; i++) 
		{
			var infoId = infoHrefs[i].id;
			if (infoId && infoId.indexOf('info_') == 0)
			{
				var tagName = infoId.substring(5);
				var popupBalloon = new HelpBalloon({
					returnElement: true,
					title: '',
					contentId: 'popup_' + tagName,
					icon:$(infoId),
					iconStyle: {						
						'verticalAlign': 'middle'
					},
					balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
					pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
					button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
					balloonFixedX: 205
				});	
				
				balloonArr.push(popupBalloon);	
			}				
		}
		
		$('popup_CurrentSalary').remove();			
	}				
}

function initSalarySlider(minSalary, maxSalary, currentSalary)
{
	SLIDER_SALARY.n_minValue = roundDownTo1000(minSalary);
	SLIDER_SALARY.n_maxValue = roundUpTo1000(maxSalary);
	SLIDER_SALARY.n_value = currentSalary;
	if (salarySlider == null && document.getElementById('salary_slider'))
	{
		salarySlider = new slider('salary_slider', SLIDER_SALARY, SLIDER_CONFIG);
	}	
}

function whizzyModel (isWhizzy, running)
{
	if (isWhizzy)
	{
		var update_element = document.getElementById(running);
		if (update_element)		
		{	
			update_element.innerHTML = 
				"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />";
		}	
	}
	else
	{
		var update_element = document.getElementById(running);
		if (update_element)		
		{	
			update_element.innerHTML = "&nbsp;";
		} 	
	}
}