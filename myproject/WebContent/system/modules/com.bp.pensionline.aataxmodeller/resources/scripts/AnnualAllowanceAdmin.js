var taxAdjustHttp = null;
var status_fields   = new Array(
	"status"
	);
var longestYear = 2017; //this is defined in default.xml when the year increase we have to increase this value
var updatedElement = null;
var updatedValue = null;
var currentValue = null;
function generateTaxModellerXML(averageSalary, allowanceTaxRate, annualInflation, capitalisation, annualAllowance, lta)
{
	var xml = "";
	xml += "<DefaultValues>";
	xml += "<AverageSalaryIncrease><![CDATA[" + formatEncode(averageSalary) + "]]></AverageSalaryIncrease>";
	xml += "<AnnualAllowanceTaxRate><![CDATA[" + formatEncode(allowanceTaxRate) + "]]></AnnualAllowanceTaxRate>";
	xml += "<AnnualInflation><![CDATA[" + formatEncode(annualInflation) + "]]></AnnualInflation>";
	xml += "<Capitalisation><![CDATA[" + formatEncode(capitalisation) + "]]></Capitalisation>";
	xml += "<AnnualAllowance><![CDATA[" + formatEncode(annualAllowance) + "]]></AnnualAllowance>";
	xml += "<LTA><![CDATA[" + formatEncode(lta) + "]]></LTA>";
	xml += "</DefaultValues>";
	return xml;
}

function initPopups()
{
	lta_update_popup();
	cap_update_popup();
}

function lta_update_popup(){
	var LTAUpdate = document.getElementById('lta_update_image');
	if (LTAUpdate){
		var LTAUpdatePopup = new HelpBalloon({
			id: 'lta_update_image',
			returnElement: true,
			title: '',
			contentId: 'lta_popup',
			icon:$('lta_update_image'),
			iconStyle: {						
				'verticalAlign': 'middle'
			},
		balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
		pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
		button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
		titleStyle: {
			'color': '#090',
			'fontSize': '16px',
			'fontWeight': 'bold',
			'fontFamily': 'Arial',
			'vertical-align': 'top'
		},
		balloonFixedX: 200
		});
	}
	
	document.getElementById('lta_popup').innerHTML = '&nbsp;';
}
function cap_update_popup(){
	var LTAUpdate = document.getElementById('cap_update_image');
	if (LTAUpdate){
		var LTAUpdatePopup = new HelpBalloon({
			id: 'lta_update_image',
			returnElement: true,
			title: '',
			contentId: 'cap_popup',
			icon:$('cap_update_image'),
			iconStyle: {						
				'verticalAlign': 'middle'
			},
		balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
		pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
		button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
		titleStyle: {
			'color': '#090',
			'fontSize': '16px',
			'fontWeight': 'bold',
			'fontFamily': 'Arial',
			'vertical-align': 'top'
		},
		balloonFixedX: 200
		});
	}
	
	document.getElementById('cap_popup').innerHTML = '&nbsp;';
}
function setUpValue(element, value){
	document.getElementById(element).value = value;
}

var ltaPipYear;
var capPipYear;

function updateLTA(editedElement, editedPipYear){
	var editedElementValue = document.getElementById('lta_' + editedElement).value;
	if (editedElementValue == null || editedElementValue.length == 0){
		alert('Please input a number!');
		return false;
	} else {
		if (isNaN(editedElementValue)) {
			alert('Please only input a number ');
			return false;
		} 
	}
	updatedValue = editedElementValue;
	ltaPipYear = editedPipYear;
	
	var aeAdminURL = ajaxurl + "/LtaCaptialisationUpdateService?type=LTA&&editedElement="+editedElement+"&editedElementValue="+editedElementValue;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", aeAdminURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
	aeRequest.onreadystatechange = handleResultLTAEdit;		     
	
	aeRequest.send(null);	
}

function handleResultLTAEdit(){
	if (aeRequest.readyState == 4) {							
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){;
					alert(errorMsg);					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<20; i++) {						
 						var nextUpdateId = 'oldlta_Year_' + (parseInt(ltaPipYear) + i) + '_' + (parseInt(ltaPipYear) + i + 1);
 						if (document.getElementById(nextUpdateId))
 						{
 							document.getElementById(nextUpdateId).value = updatedValue;
 						}
 						
						// update the text box in the the root form if the update is for current pip year
						if 	(document.getElementById('LTA_' + (parseInt(ltaPipYear) + i) + '_' + (parseInt(ltaPipYear) + i + 1)))
						{
							document.getElementById('LTA_' + (parseInt(ltaPipYear) + i) + '_' + (parseInt(ltaPipYear) + i + 1)).value = updatedValue;
						} 						
					}						
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}

function updateCap(editedElement, editedPipYear){
	var editedElementValue = document.getElementById('cap_' + editedElement).value;

	if (editedElementValue == null || editedElementValue.length == 0){
		alert('Please input a number');
		return false;
	} else {
		if (isNaN(editedElementValue)) {
			alert('Please only input a number ');
			return false;
		} 
	}
	updatedValue = editedElementValue;
	capPipYear = editedPipYear;
	
	var aeAdminURL = ajaxurl + "/LtaCaptialisationUpdateService?type=Capitalisation&&editedElement="+editedElement+"&editedElementValue="+editedElementValue;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", aeAdminURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
	aeRequest.onreadystatechange = handleResultCapEdit;		     
	
	aeRequest.send(null);	
}

function handleResultCapEdit(){
	
	if (aeRequest.readyState == 4) {							
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					alert(errorMsg);
					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<20; i++) 
					{						
 						var nextUpdateId = 'oldcap_Year_' + (parseInt(capPipYear) + i) + '_' + (parseInt(capPipYear) + i + 1);
						
 						if (document.getElementById(nextUpdateId))
 						{
 							document.getElementById(nextUpdateId).value = updatedValue;
 						} 						
						
						// update the text box in the the root form if the update is for current pip year
						if 	(document.getElementById('Capitalisation_' + (parseInt(capPipYear) + i) + '_' + (parseInt(capPipYear) + i + 1)))
						{
							document.getElementById('Capitalisation_' + (parseInt(capPipYear) + i) + '_' + (parseInt(capPipYear) + i + 1)).value = updatedValue;
						} 						
					}
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}
function submitDefaultTaxModellerRequest(){
	// Get input value by ID
	var averageSalary = document.getElementById("AverageSalaryIncrease").value;
	var allowanceTaxRate = document.getElementById("AnnualAllowanceTaxRate").value;
	var annualInflation = document.getElementById("AnnualInflation").value;
	var annualAllowance = document.getElementById("AnnualAllowance").value;
	
	if (averageSalary == null || averageSalary.length == 0){
		alert('Please input a number for average salary increase percentage');
		return false;
	} else {
		if (isNaN(averageSalary)) {
			alert('Please only input a number ');
			return false;
		} 
	}
	
	if (allowanceTaxRate == null || allowanceTaxRate.length == 0){		
		alert('Please input a number for allowance tax rate');
		return false;
	} else {
		if (isNaN(allowanceTaxRate)) {
			alert('Please only input a number ');
			return false;
		}
  	} 
	
	if (annualInflation == null || annualInflation.length == 0){
		alert('Please input a number for annual inflation');
		return false;
	} else {
		if (isNaN(annualInflation)) {
			alert('Please only input a number ');
			return false;
		}
  	} 
	
	if (annualAllowance == null || annualAllowance.length == 0){
		alert('Please input a number for annual allowance');
		return false;
	} else {
		if (isNaN(annualAllowance)) {
			alert('Please only input a number ');
			return false;
		}
  	} 
	 	
	var unitTestCalcURL = ajaxurl + "/DefaultTaxModellerService?AverageSalaryIncrease="+averageSalary+"&&AnnualAllowanceTaxRate="+allowanceTaxRate+"&&AnnualInflation="+
									annualInflation+"&&AnnualAllowance="+annualAllowance;   
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", unitTestCalcURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleResult;	
	aeRequest.send(null);
}
function handleResult(){
	if (aeRequest.readyState == 4) {							
		
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					$j("#pagebody_button").empty();
					$j("#pagebody_road").empty();
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<status_fields.length; i++) {
						var tag_name = status_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						
						if (update_nodes.length > 0) {
							var update_element = document.getElementById(tag_name);
							if (update_element != null){
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
						} else {
							var update_element = document.getElementById(tag_name);
							if (update_element != null) {
								update_element.innerHTML = "&nbsp;";
							}							
						}
					}	
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}
/**
 * This function does configuration of "AA Carry forward configuration" in the administration side
 */
function submitAaCarryConfig(){
	var initTag = document.getElementById("initTag").value;
	var operationTag = document.getElementById("operationTag").value;
	var endTag = document.getElementById("endTag").value;
	if (initTag == null || initTag.length == 0){
		alert('Please input data for init tag');
		return false;
	}
	if (endTag == null || endTag.length == 0){
		alert('Please input data for end tag');
		return false;
	}
	//var xml = generateTaxModellerXML(averageSalary, allowanceTaxRate, annualInflation, capitalisation, annualAllowance, lta);	
	var unitTestCalcURL = ajaxurl + "/AACarryCarSalaryConfigService?type=AaCarryConfig&initTag="+initTag+"&operationTag="+operationTag+"&endTag="+endTag;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", unitTestCalcURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleResultAaCarryConfig;		     
	
	aeRequest.send(null);
}
/**
 * This function does configuration of "My service car configuration " in the administration side
 */
function submitCarConfig(){
	var serviceCar = document.getElementById("serviceCar").value;
	var unitTestCalcURL = ajaxurl + "/AACarryCarSalaryConfigService?type=carConfig&serviceCar="+serviceCar;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", unitTestCalcURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleResultCarConfig;		     
	
	aeRequest.send(null);
}
/**
 * This function does configuration of "My service salary modelling limits " in the administration side
 */
function submitServiceSalaryConfig(){
	var lowerBound = document.getElementById("lowerBound").value;
	var uperBound = document.getElementById("uperBound").value;
	var typeOfData = typeof lowerBound;
	
	if (lowerBound == null || lowerBound.length == 0 ){
		alert('Please input data for init tag');
		return false;
	}else if(isNaN(lowerBound)){
		alert("You must enter a number for both Lower bound and Uper bound!");
		return false;
	}
	if (uperBound == null || uperBound.length == 0 ){
		alert('Please input data for init tag');
		return false;
	}else if(isNaN(uperBound)){
		alert("You must enter a number for both Lower bound and Uper bound!");
		return false;
	}
	var unitTestCalcURL = ajaxurl + "/AACarryCarSalaryConfigService?type=serviceSalary&lowerBound="+lowerBound+"&uperBound="+uperBound;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", unitTestCalcURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        aeRequest.setRequestHeader("Content-length", 0);
        aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleResultServiceSalary;		     
	
	aeRequest.send(null);
}

function handleResultAaCarryConfig(){

	if (aeRequest.readyState == 4) {							
		
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					$j("#pagebody_button").empty();
					$j("#pagebody_road").empty();
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<status_fields.length; i++) {
						var tag_name = status_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						
						if (update_nodes.length > 0) {
							var update_element = document.getElementById("statusAaCarryConfig");
							if (update_element != null){
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
						} else {
							var update_element = document.getElementById("statusAaCarryConfig");
							if (update_element != null) {
								update_element.innerHTML = "&nbsp;";
							}							
						}
 
					}	
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}
function handleResultCarConfig(){

	if (aeRequest.readyState == 4) {							
		
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					$j("#pagebody_button").empty();
					$j("#pagebody_road").empty();
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<status_fields.length; i++) {
						var tag_name = status_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						
						if (update_nodes.length > 0) {
							var update_element = document.getElementById("statusCarConfig");
							if (update_element != null){
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
						} else {
							var update_element = document.getElementById("statusCarConfig");
							if (update_element != null) {
								update_element.innerHTML = "&nbsp;";
							}							
						}
 
					}	
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}
function handleResultServiceSalary(){

	if (aeRequest.readyState == 4) {							
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					$j("#pagebody_button").empty();
					$j("#pagebody_road").empty();
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				} else {
					// Get array of status fields for alert status of updating.
					for(var i=0; i<status_fields.length; i++) {
						var tag_name = status_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						
						if (update_nodes.length > 0) {
							var update_element = document.getElementById("statusServiceSalary");
							if (update_element != null){
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
						} else {
							var update_element = document.getElementById("statusServiceSalary");
							if (update_element != null) {
								update_element.innerHTML = "&nbsp;";
							}							
						}
 
					}	
				}
			} catch(err){
				alert("XML Parser error." + err);	
			}		
		}
	}
}


function getResponseTag(objRoot,tagName,position)
{
	var tagValue="";
	if(objRoot.getElementsByTagName(tagName)[0].childNodes.length > 0) 
	{
		tagValue = objRoot.getElementsByTagName(tagName)[0].firstChild.nodeValue
	}
	else
	{
		tagValue =" ";
	}

	if (tagValue == 'null')
	{
		tagValue = "";
	}

	return tagValue
}

//window.onload = doLoad;