var aeRequest = null;
var member_details_fields   = new Array(
	"reference",
	"nino",
	"name",
	"gender",
	"date_of_birth",
	"joined_company",
	"joined_scheme",
	"birthday_55th",
	"birthday_60th",
	"birthday_65th",
	"salary",
	"avc_fund",
	"aver_fund",
	"tvin_a",
	"tvin_b",
	"tvin_c",
	"augmentation",
	"lta",
	"banked_egp"
	);
	
var headroom_fields = new Array(
	"Accrual",
	"Salary",
	"RetireAge",
	"CashLumpSum",
	"TotalYears",
	"TotalServiceYears",
	"TotalAccrued",
	"TotalServiceYearsAt60th"
);
	
var scheme_absence_headroom_fields   = new Array(
	"scheme_from",
	"scheme_to",
	"scheme_category",
	"scheme_accrual",
	"absence_from",
	"absence_to",
	"absence_type",
	"absence_worked",
	"absence_employed",
	"headroom_from",
	"headroom_to",
	"headroom_category",
	"headroom_years",
	"headroom_fte",
	"headroom_service",
	"headroom_accrual",
	"headroom_accrued",
	"headroom_60th"
	);	
	
var MODELLED_FIELD_IDS   = new Array(
	"retire_age",
	"accrued_to_date",
	"modelled_salary",
	"modelled_accrual",
	"modelled_retire_age",
	"modelled_retire_age",
	"cash_lump_sum",
	"pension",
	"pension_pot",
	"next_taxyear_table"	
	);

var SLIDER_CONFIG = {
                'b_vertical' : false,
                'b_watch': true,
                'n_controlWidth': 320,
                'n_controlHeight': 16,
                'n_sliderWidth': 16,
                'n_sliderHeight': 15,
                'n_pathLeft' : 1,
                'n_pathTop' : 1,
                'n_pathLength' : 303,
                's_imgControl': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_bg.gif',
                's_imgSlider': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_sl.gif',
                'n_zIndex': 1
        }
var SLIDER_SALARY = {        
        's_name': 'input_salary',
        'n_minValue' : 0,
        'n_maxValue' : 450000,
        'n_value' : 22000,
        'n_step' : 1000
}   

var SLIDER_RETIRE_AGE = {
        's_name': 'input_retire_age',
        'n_minValue' : 55,
        'n_maxValue' : 65,      
        'n_value' : 65,
        'n_step' : 1
}

var balloonArr = new Array();
var salarySlider = null;
var retireAgeSlider = null; 
	
var scheme_history_node_name = 'SchemeHistory';
var absence_history_node_name = 'AbsenceHistory';
var headroom_history_node_name = 'ServiceHistory';
var salary_history_node_name = 'SalaryHistory';

var bgroup ='';
var refno ='';
var sdate = '';

// parameters stored to used for tax modeller

var systemDate = '';

var inputSalary = '';
var inputAccrual = '';
var inputRetireAge = 65;
var inputCash = 0;

var salary = 0;

var totaleServiceYearAt60 = 0.00;
var lta = 1000000;
var minSalary = 0;
var maxSalary = lta/4;

function initPopups()
{
	var contentElement = document.getElementById('content');
	if (contentElement)
	{
		var infoImgs = contentElement.getElementsByTagName('img');
		for(var i=0; i<infoImgs.length; i++) 
		{
			var infoId = infoImgs[i].id
			if (infoId && infoId.indexOf('info_') == 0)
			{
				var tagName = infoId.substring(5);
				var popupBalloon = new HelpBalloon({
					returnElement: true,
					title: '',
					contentId: 'popup_' + tagName,
					icon:infoImgs[i],
					iconStyle: {						
						'verticalAlign': 'middle'
					},
					balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
					pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
					button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
					balloonFixedX: 205
				});	
				
				balloonArr.push(popupBalloon);	
				
				// remove the content after clone to balloon
				$('popup_' + tagName).remove();	
			}		
		}			
	}				
}
			
function loadMember()
{
	
	bgroup = document.getElementById("bgroup").value.trim();
	refno = document.getElementById("refno").value.trim();
	sdate = document.getElementById("sdate").value;
	
	if (refno == null || refno.length == 0)
	{
		alert('Please input member\'s refno');
		return false;
	}
	else
	{
		for(var i=0; i<refno.length; i++) {
			var c = refno.charAt(i);
			
			if (c  < '0' || c > '9')
			{
				alert('Please input a valid member\'s refno');
				return false;
			}
		}		
	}
	
	systemDate = sdate;
	var dayMonthYear = systemDate.split('-');
	if (dayMonthYear.length == 3)
	{
		var month = dayMonthYear[1];
		var year = dayMonthYear[2];
		var soyDay = '1-January-' + year;
		var eoyDay = '31-December-' + year;
		
		if (month == 'Oct' || month == 'Nov' || month == 'Dec')	// after 1-Oct
		{
			soyDay = '1-January-' + (parseInt(year) + 1);
			eoyDay = '31-December-' + (parseInt(year) + 1);
		}
			
		if (document.getElementById('accrued_date'))
		{
			document.getElementById('accrued_date').innerHTML = soyDay;
		}
		if (document.getElementById('salaried_date'))
		{
			document.getElementById('salaried_date').innerHTML = eoyDay;
		}		
	}
		
	var aaTaxModellerMemberLoadURL = ajaxurl + "/MemberLoadingService";
	var params = "bgroup=" + bgroup + "&refno=" + refno + "&sdate=" + sdate;

	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", aaTaxModellerMemberLoadURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", params.length);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleMemberLoadResult;		       
	
	whizzy(true, 'member_running_div', 'member_result_div');
	whizzy(true, 'headroom_running_div', 'headroom_result_div');
		
	aeRequest.send(params);	
}

function handleMemberLoadResult(){

   	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			//alert(aeRequest.responseText.length);
			var response = createResponseXML(aeRequest.responseText);					
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					
					if (document.getElementById('member_running_div'))
					{
						document.getElementById('member_running_div').style.display = 'none';
					}
					if (document.getElementById('headroom_running_div'))
					{
						document.getElementById('headroom_running_div').style.display = 'none';
					}
					return false;
				}
				else
				{
					// member details
					handleMemberData(response);	
					
					whizzy(false, 'member_running_div', 'member_result_div');
					
					// load headroom data
					loadMemberAE();
				} 					
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}

function handleMemberData (response)
{
	if (response)
	{
		// update member details
		for(var i=0; i<member_details_fields.length; i++) 
		{
			var tag_name = member_details_fields[i];
			var update_nodes = response.getElementsByTagName(tag_name);
			
			if (update_nodes.length > 0)
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
				}
			}
			else
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = "&nbsp;";
				}							
			}
		}
		
		// update scheme history
		var schemeHistoryNodes = response.getElementsByTagName(scheme_history_node_name);
		var schemeHistoriesInnerHTML = '<table class="scheme_absence_table" cellspacing="0" cellpadding="0" id="scheme_absence_table">' +
				'<tr><th class="longlabel" colspan="5">Scheme and absence history</th></tr>';
		schemeHistoriesInnerHTML += '<tr><th>From</th><th>To</th><th>Category</th><th colspan="2">Accrual</th></tr>';
		if (schemeHistoryNodes.length > 0)
		{
			for(var i=0; i<schemeHistoryNodes.length; i++) 
			{
				var schemeHistoryInnerHTML = '<tr>';
				var schemeHistoryNode = schemeHistoryNodes[i];
				if (schemeHistoryNode.getElementsByTagName('from').length > 0 && schemeHistoryNode.getElementsByTagName('from')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				if (schemeHistoryNode.getElementsByTagName('to').length > 0 && schemeHistoryNode.getElementsByTagName('to')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (schemeHistoryNode.getElementsByTagName('category').length > 0 && schemeHistoryNode.getElementsByTagName('category')[0].firstChild)
				{
					schemeHistoryInnerHTML += '<td>' + schemeHistoryNode.getElementsByTagName('category')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (schemeHistoryNode.getElementsByTagName('accrual').length > 0 && schemeHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue)
				{
					schemeHistoryInnerHTML += '<td colspan="2">' + schemeHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					schemeHistoryInnerHTML += '<td colspan="2">&nbsp;</td>';
				}
				
				schemeHistoryInnerHTML += '</tr>';
				schemeHistoriesInnerHTML += schemeHistoryInnerHTML;
			}
		}
		else
		{
			schemeHistoriesInnerHTML += '<tr><td colspan="5">&nbsp;</td></tr>';
		}
							
		// update absence history		
		var absenceHistoriesInnerHTML = '<tr><th>From</th><th>To</th><th>Type</th><th>Worked</th><th>Employed</th></tr>';
		
		var absenceHistoryNodes = response.getElementsByTagName(absence_history_node_name);
		if (absenceHistoryNodes.length > 0)
		{
			for(var i=0; i<absenceHistoryNodes.length; i++) 
			{
				var absenceHistoryNode = absenceHistoryNodes[i];
				var absenceHistoryInnerHTML = '<tr>';
				if (absenceHistoryNode.getElementsByTagName('from').length > 0 && absenceHistoryNode.getElementsByTagName('from')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('to').length > 0 && absenceHistoryNode.getElementsByTagName('to')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('type').length > 0 && absenceHistoryNode.getElementsByTagName('type')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('type')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('worked').length > 0 && absenceHistoryNode.getElementsByTagName('worked')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('worked')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				if (absenceHistoryNode.getElementsByTagName('worked').length > 0 && absenceHistoryNode.getElementsByTagName('employed')[0].firstChild)
				{
					absenceHistoryInnerHTML += '<td>' + absenceHistoryNode.getElementsByTagName('employed')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					absenceHistoryInnerHTML += '<td>&nbsp;</td>';
				}
				
				absenceHistoryInnerHTML += '</tr>';
				absenceHistoriesInnerHTML += absenceHistoryInnerHTML;
			}
		}
		else
		{
			absenceHistoriesInnerHTML += '<tr><td colspan="5">&nbsp;</td></tr>';
		}
		
		
		if (document.getElementById('scheme_absence_table'))
		{
			document.getElementById('scheme_absence_table').innerHTML = schemeHistoriesInnerHTML + absenceHistoriesInnerHTML + '</table>';
		}
		
		// salary history
		var salaryHistoriesInnerHTML = '<table class="scheme_absence_table" cellspacing="0" cellpadding="0">' +
				'<tr><th class="longlabel" colspan="2">Salary history (Show last 3 pay rises)</th></tr>';				
		salaryHistoriesInnerHTML += '<tr><th>Effective</th><th>Salary</th></tr>';
		var salaryHistoryNodes = response.getElementsByTagName(salary_history_node_name);

		if (salaryHistoryNodes.length > 0)
		{
			for(var i=0; i<salaryHistoryNodes.length; i++) 
			{
				var salaryHistoryNode = salaryHistoryNodes[i];
				var salaryHistoryInnerHTML = '<tr>';
				if (salaryHistoryNode.getElementsByTagName('Effective').length > 0)
				{
					salaryHistoryInnerHTML += '<td>' + salaryHistoryNode.getElementsByTagName('Effective')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					salaryHistoryNode += '<td>&nbsp;</td>';
				}
				
				if (salaryHistoryNode.getElementsByTagName('Salary').length > 0)
				{
					salaryHistoryInnerHTML += '<td>&pound;' + salaryHistoryNode.getElementsByTagName('Salary')[0].firstChild.nodeValue + '</td>';
				}
				else
				{
					salaryHistoryNode += '<td>&nbsp;</td>';
				}				
				
				salaryHistoryInnerHTML += '</tr>';
				salaryHistoriesInnerHTML += salaryHistoryInnerHTML;
			}
		}
		else
		{
			salaryHistoriesInnerHTML += '<tr><td colspan="2">&nbsp;</td></tr>';
		}
		
		
		if (document.getElementById('salary_table'))
		{
			document.getElementById('salary_table').innerHTML = salaryHistoriesInnerHTML + '</table>';
		}		 	
	}
}

function loadMemberAE()
{
	
	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentCheckingService";
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleLoadMemberAEResponse;		     
	
	aeRequest.send(null);			
}

function handleLoadMemberAEResponse(){

   	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			//alert(aeRequest.responseText);
			var response = createResponseXML(aeRequest.responseText);					
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					//alert(errorMsg);
					whizzy(false, 'headroom_running_div', 'headroom_result_div');
					return false;
				}
				else
				{
					// Headroom
					handleHeadroomData(response);					
					
					// Tax year
					handleCurrentTaxYearData(response);
					
					handleNextTaxYearData(response, false);
					
					if (balloonArr.length == 0)
					{
						initPopups();
						SLIDER_SALARY.n_minValue = roundDownTo1000(minSalary);
						SLIDER_SALARY.n_maxValue = roundUpTo1000(maxSalary);
						SLIDER_SALARY.n_value = salary;
						if (salarySlider == null)
						{
							salarySlider = new slider('salary_slider', SLIDER_SALARY, SLIDER_CONFIG);
						}
						if (retireAgeSlider == null)
						{
							retireAgeSlider = new slider('retire_age_slider', SLIDER_RETIRE_AGE, SLIDER_CONFIG);
						}	
					}
					
					whizzy(false, 'headroom_running_div', 'headroom_result_div');
				} 					
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}

function handleHeadroomData (response)
{
	if (response)
	{
		// update scheme history
		var headroomHistoriesInnerHTML = '<table class="headroom_table" cellspacing="0" cellpadding="0" id="scheme_absence_table">' +
				'<tr><th class="longlabel" colspan="12">Service history</th></tr>';
		headroomHistoriesInnerHTML += '<tr><th width="100">From</th><th width="100">To</th><th width="45">Cat</th>' +
				'<th width="50">years</th><th width="50">days</th><th width="50">total years</th><th width="40">FTE</th>' +
				'<th width="60">service</th><th width="45">accru\'</th><th width="40">ERF</th>' + 
				'<th width="60">accrued</th><th width="40">60th</th></tr> ';
				
		var headroomHistoryNodes = response.getElementsByTagName(headroom_history_node_name);

		for(var i=0; i<headroomHistoryNodes.length; i++) 
		{
			var headroomHistoryNode = headroomHistoryNodes[i];			

			headroomHistoryInnerHTML = '<tr>';
			if (headroomHistoryNode.getElementsByTagName('from').length > 0 && headroomHistoryNode.getElementsByTagName('from')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
	
			if (headroomHistoryNode.getElementsByTagName('to').length > 0 && headroomHistoryNode.getElementsByTagName('to')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('category').length > 0 && headroomHistoryNode.getElementsByTagName('category')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('category')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('years').length > 0 && headroomHistoryNode.getElementsByTagName('years')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('years')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('days').length > 0 && headroomHistoryNode.getElementsByTagName('days')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('days')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('tyears').length > 0 && headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('fte').length > 0 && headroomHistoryNode.getElementsByTagName('fte')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('fte')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('service').length > 0 && headroomHistoryNode.getElementsByTagName('service')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('service')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('accrual').length > 0 && headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('erf').length > 0 && headroomHistoryNode.getElementsByTagName('erf')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('erf')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}			
			
			if (headroomHistoryNode.getElementsByTagName('accrued').length > 0 && headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + '&pound;' + formatToDecimal(getInt(headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue)) + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('service60th').length > 0 && headroomHistoryNode.getElementsByTagName('service60th')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('service60th')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			headroomHistoryInnerHTML += '</tr>';
			headroomHistoriesInnerHTML += headroomHistoryInnerHTML;
		}

		// total row
		var totalYears = '&nbsp;';
		var totalServiceYears = '&nbsp;';
		var totalAccrued = '&nbsp;';
		var totalServiceYears60th = '&nbsp;';
		if (response.getElementsByTagName('TotalYears') && response.getElementsByTagName('TotalYears')[0].firstChild)
		{
			totalYears = response.getElementsByTagName('TotalYears')[0].firstChild.nodeValue;
		}
		if (response.getElementsByTagName('TotalServiceYears') && response.getElementsByTagName('TotalServiceYears')[0].firstChild)
		{
			totalServiceYears = response.getElementsByTagName('TotalServiceYears')[0].firstChild.nodeValue;
		}
		if (response.getElementsByTagName('TotalAccrued') && response.getElementsByTagName('TotalAccrued')[0].firstChild)
		{
			totalAccrued = formatToDecimal(getInt(response.getElementsByTagName('TotalAccrued')[0].firstChild.nodeValue));
		}
		if (response.getElementsByTagName('TotalServiceYearsAt60th') && response.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild)
		{
			totalServiceYears60th = response.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild.nodeValue;
			totaleServiceYearAt60 = getDouble(totalServiceYears60th);
		}		

		headroomHistoriesInnerHTML += '<tr><td class="empty" colspan=5>&nbsp;</td>' +
				'<td class="total">' + totalYears + 
				'</td><td class="empty"">&nbsp;</td><td class="total">' + totalServiceYears +
				'</td><td class="empty" >&nbsp;</td>' + '</td><td class="empty" >&nbsp;</td>' + 
				'<td class="total">' + '&pound;' + totalAccrued +
				'</td><td class="total">' + totalServiceYears60th + '</td></tr> ';

		if (document.getElementById('headroom_table'))
		{
			document.getElementById('headroom_table').innerHTML = headroomHistoriesInnerHTML + '</table>';
		}		
		if (document.getElementById('headroom_div'))
		{
			document.getElementById('headroom_div').style.display = 'block';
		}	
		
		// update total accrued to tax modeller table
		if (document.getElementById('accrued_to_date'))
		{
			document.getElementById('accrued_to_date').innerHTML = '&pound;' + totalAccrued;
		}		
		
		if (response.getElementsByTagName('Salary') && response.getElementsByTagName('Salary')[0].firstChild)
		{
			if (document.getElementById('modelled_salary'))
			{
				document.getElementById('modelled_salary').innerHTML = '&pound;' + 
					formatToDecimal(response.getElementsByTagName('Salary')[0].firstChild.nodeValue);
				salary = response.getElementsByTagName('Salary')[0].firstChild.nodeValue;
			}
		}
		if (response.getElementsByTagName('MinSalary') && response.getElementsByTagName('MinSalary')[0].firstChild)
		{
			minSalary = parseInt(response.getElementsByTagName('MinSalary')[0].firstChild.nodeValue);
		}
		if (response.getElementsByTagName('MaxSalary') && response.getElementsByTagName('MaxSalary')[0].firstChild)
		{
			maxSalary = parseInt(response.getElementsByTagName('MaxSalary')[0].firstChild.nodeValue);
		}		
		if (response.getElementsByTagName('Accrual') && response.getElementsByTagName('Accrual')[0].firstChild)
		{
			if (document.getElementById('modelled_accrual'))
			{
				document.getElementById('modelled_accrual').innerHTML = response.getElementsByTagName('Accrual')[0].firstChild.nodeValue;
			}
			document.getElementById('input_accrual').value = response.getElementsByTagName('Accrual')[0].firstChild.nodeValue;
		}
		
		if (document.getElementById('modelled_retire_age'))
		{
			document.getElementById('modelled_retire_age').innerHTML = inputRetireAge;
		}
		if (document.getElementById('retire_age'))
		{
			document.getElementById('retire_age').innerHTML = inputRetireAge;
		}
		
		if (response.getElementsByTagName('CashLumpSum') && response.getElementsByTagName('CashLumpSum')[0].firstChild)
		{
			if (document.getElementById('cash_lump_sum'))
			{
				document.getElementById('cash_lump_sum').innerHTML =  '&pound;' + 
					formatToDecimal(response.getElementsByTagName('CashLumpSum')[0].firstChild.nodeValue);
			}
		}	
		if (response.getElementsByTagName('Pension') && response.getElementsByTagName('Pension')[0].firstChild)
		{
			if (document.getElementById('pension'))
			{
				document.getElementById('pension').innerHTML =  '&pound;' + 
					formatToDecimal(response.getElementsByTagName('Pension')[0].firstChild.nodeValue);
			}
		}
		if (response.getElementsByTagName('LTA') && response.getElementsByTagName('LTA')[0].firstChild)
		{
			lta = getDouble(response.getElementsByTagName('LTA')[0].firstChild.nodeValue);
		}
		var pensionPot = 0;
		if (response.getElementsByTagName('PensionPot') && response.getElementsByTagName('PensionPot')[0].firstChild)
		{
			pensionPot = getDouble(response.getElementsByTagName('PensionPot')[0].firstChild.nodeValue);
			if (document.getElementById('pension_pot'))
			{
				document.getElementById('pension_pot').innerHTML = '&pound;' + formatToDecimal(response.getElementsByTagName('PensionPot')[0].firstChild.nodeValue);
				if (pensionPot > lta)
				{
					document.getElementById('pension_pot').style.color = "#ff0000";
				}
			}
		}
		
	}
}

function handleCurrentTaxYearData (response)
{
	if (response)
	{
		if (response.getElementsByTagName("CurrentTaxYear").length > 0)
		{
			var currentTaxYearNode = response.getElementsByTagName("CurrentTaxYear")[0];
			// update scheme history
			var currentTaxTableInnerHTML = '<table class="headroom_table" cellspacing="0" cellpadding="0">' +
					'<tr><th class="longlabel" colspan="13">Current Tax Year</th></tr>';
			currentTaxTableInnerHTML += 
					'<tr><th width="60">Tax year</th>' +
					'<th width="60">SoY Sal</th><th width="60">SoY Ser\'</th><th width="60">SoY Ben\'</th>' +
					'<th width="40">CPI</th><th width="60">CPI Rev</th><th width="40">Cont Opt</th>' +				
					'<th width="60">EoY Sal</th><th width="60">EoY Ser\'</th><th width="60">EoY Ben\'</th>' +				
					'<th width="60">Increase</th><th width="60">AA Check</th><th width="60">Taxable</th>';
	
			var currentTaxRowInnerHTML = '<tr>';
			if (currentTaxYearNode.getElementsByTagName('TaxYear').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + currentTaxYearNode.getElementsByTagName('TaxYear')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('SOYSalary').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('SOYSalary')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('SOYService').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + currentTaxYearNode.getElementsByTagName('SOYService')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('SOYBenefit').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('SOYBenefit')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('CPI').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + currentTaxYearNode.getElementsByTagName('CPI')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('CpiReval').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('CpiReval')[0].firstChild.nodeValue + '</td>';
			}	
			if (currentTaxYearNode.getElementsByTagName('AccruedRate').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + currentTaxYearNode.getElementsByTagName('AccruedRate')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('EOYSalary').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('EOYSalary')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('EOYService').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + currentTaxYearNode.getElementsByTagName('EOYService')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('EOYBenefit').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('EOYBenefit')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('Increase').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('Increase')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('AACheck').length > 0)
			{
				currentTaxRowInnerHTML += '<td>' + '&pound;' + currentTaxYearNode.getElementsByTagName('AACheck')[0].firstChild.nodeValue + '</td>';
			}
			if (currentTaxYearNode.getElementsByTagName('AAExcess').length > 0)
			{
				var taxableAmount = currentTaxYearNode.getElementsByTagName('AAExcess')[0].firstChild.nodeValue;
				if (taxableAmount.indexOf('-') > -1)
				{
					taxableAmount = '-';
				}
				currentTaxRowInnerHTML += '<td>' + '&pound;' + taxableAmount + '</td>';
			}
			
			currentTaxRowInnerHTML += '</tr>';
			currentTaxTableInnerHTML += currentTaxRowInnerHTML + '</table>';	
			
			if (document.getElementById('current_taxyear_table'))
			{
				document.getElementById('current_taxyear_table').innerHTML = currentTaxTableInnerHTML;
			}
		}
	}
}

function handleNextTaxYearData (response, isModelled)
{
	if (response)
	{
		if (response.getElementsByTagName("NextTaxYear").length > 0)
		{
			var nextTaxYearNode = response.getElementsByTagName("NextTaxYear")[0];			
			
			// update scheme history
			var nextTaxTableInnerHTML = '<table class="headroom_table" cellspacing="0" cellpadding="0">' +
					'<tr><th class="longlabel" colspan="13">Next Tax Year</th></tr>';
			nextTaxTableInnerHTML += 
					'<tr><th width="60">Tax year</th>' +
					'<th width="60">SoY Sal</th><th width="60">SoY Ser\'</th><th width="60">SoY Ben\'</th>' +
					'<th width="40">CPI</th><th width="60">CPI Rev</th><th width="40">Cont Opt</th>' +				
					'<th width="60">EoY Sal</th><th width="60">EoY Ser\'</th><th width="60">EoY Ben\'</th>' +				
					'<th width="60">Increase</th><th width="60">AA Check</th><th width="60">Taxable</th>';
	
			var nextTaxRowInnerHTML = '<tr>';
			if (nextTaxYearNode.getElementsByTagName('TaxYear').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('TaxYear')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('SOYSalary').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('SOYSalary')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('SOYService').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('SOYService')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('SOYBenefit').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('SOYBenefit')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('CPI').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('CPI')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('CpiReval').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('CpiReval')[0].firstChild.nodeValue + '</td>';
			}	
			if (nextTaxYearNode.getElementsByTagName('AccruedRate').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('AccruedRate')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('EOYSalary').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('EOYSalary')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('EOYService').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('EOYService')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('EOYBenefit').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('EOYBenefit')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('Increase').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + nextTaxYearNode.getElementsByTagName('Increase')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('AACheck').length > 0)
			{
				nextTaxRowInnerHTML += '<td>' + '&pound;' + nextTaxYearNode.getElementsByTagName('AACheck')[0].firstChild.nodeValue + '</td>';
			}
			if (nextTaxYearNode.getElementsByTagName('AAExcess').length > 0)
			{
				var taxableAmount = nextTaxYearNode.getElementsByTagName('AAExcess')[0].firstChild.nodeValue;
				if (taxableAmount.indexOf('-') > -1)
				{
					taxableAmount = '-';
				}
				nextTaxRowInnerHTML += '<td>' + '&pound;' + taxableAmount + '</td>';
			}
			
			nextTaxRowInnerHTML += '</tr>';
			nextTaxTableInnerHTML += nextTaxRowInnerHTML + '</table>';	
			
			if (document.getElementById('next_taxyear_table'))
			{
				document.getElementById('next_taxyear_table').innerHTML = nextTaxTableInnerHTML;
			}	
		}
	}
}

function modelSalaryChange()
{
	if (document.getElementById('input_salary'))
    {
    	inputSalary = document.getElementById('input_salary').value;
    } 
    
    submitModelling();
    
    for(var i=0; i<balloonArr.length; i++) {
		balloonArr[i].hide();
	}
}

function modelAccrualChange()
{
	if (document.getElementById('input_accrual'))
    {
    	inputAccrual = document.getElementById('input_accrual').value;
    } 
    
    submitModelling();
    
    for(var i=0; i<balloonArr.length; i++) {
		balloonArr[i].hide();
	}
}

function modelRetireAgeChange()
{
	if (document.getElementById('input_retire_age'))
    {
    	inputRetireAge = document.getElementById('input_retire_age').value;
    } 
    
    submitModelling();
    
    for(var i=0; i<balloonArr.length; i++) {
		balloonArr[i].hide();
	}
}

function modelCashChange()
{
	if (document.getElementById('input_cash'))
    {
    	inputCash = document.getElementById('input_cash').value;
    } 
    
    submitModelling();
    
    for(var i=0; i<balloonArr.length; i++) {
		balloonArr[i].hide();
	}
}

function submitModelling()
{
	var modelledSalary = inputSalary;
	var modelledAccrual = inputAccrual;
	var modelledRetireAge = inputRetireAge;
	var modellingCash = inputCash;
	
	var aeCheckingURL = ajaxurl + "/AnnualEnrolmentCheckingService";
	var params = "salary=" + modelledSalary + 
		"&accrual=" + modelledAccrual + 
		"&retire=" + modelledRetireAge +
		"&cash=" + modellingCash;

	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", aeCheckingURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", params.length);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModellingResponse;		     
	
	for(var i=0; i<MODELLED_FIELD_IDS.length; i++) 
	{
		var tag_name = MODELLED_FIELD_IDS[i];	
		var update_element = document.getElementById(tag_name);
		if (update_element)		
		{	
			update_element.innerHTML = 
				"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />";
		}
	}
		
	aeRequest.send(params);	
}

function handleModellingResponse ()
{
	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			//(aeRequest.responseText.length);
			var response = createResponseXML(aeRequest.responseText);					
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					whizzy(false, 'headroom_running_div', 'headroom_result_div');
					return false;
				}
				else
				{
					// Headroom
					handleHeadroomData(response);					
					
					// Tax year
					handleNextTaxYearData(response, true);
					
					whizzy(false, 'headroom_running_div', 'headroom_result_div');
				} 					
			}
			catch(err){
				alert("XML Parser error." + err);		
			}						
		}
	}
}

function whizzy (isWhizzy, runningId, resultId)
{
	if (isWhizzy)
	{
		if (document.getElementById(runningId))
		{
			document.getElementById(runningId).style.display = 'block';
		}
		if (document.getElementById(resultId))
		{
			document.getElementById(resultId).style.display = 'none';
		}
	}
	else
	{
		if (document.getElementById(runningId))
		{
			document.getElementById(runningId).style.display = 'none';
		}
		if (document.getElementById(resultId))
		{
			document.getElementById(resultId).style.display = 'block';
		}
	}
}
