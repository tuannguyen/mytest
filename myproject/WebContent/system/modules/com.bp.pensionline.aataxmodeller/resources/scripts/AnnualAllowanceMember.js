var aaTaxModellerRequest = null;
	
var updated_tax_fields   = new Array(
	"accrued_date",
	"salary_date",
	"accrued_to_date",
	"modelled_salary",
	"modelled_taxrate",
	"tax_amount",
	"tax_status"
	);
	
var modelled_tax_fields   = new Array(
	"modelled_salary",
	"modelled_taxrate",
	"tax_amount",
	"tax_status"
	);	
	
var taxyear_fields   = new Array(
	"Year",
	"TaxYear",
	"SOYSalary",
	"Accrued",
	"SOYBenefit",
	"CPI",
	"CpiReval",
	"SalaryIncrease",
	"EOYSalary",
	"AccruedRate",
	"EOYBenefit",
	"Increase",
	"AAFac",
	"AACheck",
	"AnnualAllowance",
	"AAExcess",
	"TaxRate",
	"Tax"
	);

var SLIDER_CONFIG = {
                'b_vertical' : false,
                'b_watch': true,
                'n_controlWidth': 120,
                'n_controlHeight': 16,
                'n_sliderWidth': 16,
                'n_sliderHeight': 15,
                'n_pathLeft' : 1,
                'n_pathTop' : 1,
                'n_pathLength' : 103,
                's_imgControl': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_bg.gif',
                's_imgSlider': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_sl.gif',
                'n_zIndex': 1
        }
var SLIDER_SALARY = {
        
        's_name': 'input_salary',
        'n_minValue' : 0,
        'n_maxValue' : 400000,
        'n_value' : 22000,
        'n_step' : 1000
}       

var SLIDER_TAXRATE = {
        's_name': 'input_taxrate',
        'n_minValue' : 0,
        'n_maxValue' : 100,      
        'n_value' : 50,
        'n_step' : 1
}


var salarySlider = null;
var taxrateSlider = null; 
var inputSalary = 10000;
var inputTaxrate = 50;
var lta = 400000;
	
var headroom_history_node_name = 'ServiceHistory';

var bgroup ='';
var refno ='';
var sdate = '';

// parameters stored to used for tax modeller
var servicesTo1st = 0.00;
var accrual = 0;
var systemDate = '';;

var balloonArr = new Array();
var popupInitId;

function initAATables()
{
	initPopups();
	updateMemberData();							
}

function updateMemberData()
{	
	var updateMemberDataURL = ajaxurl + "/UpdateMemberData";
	aaTaxModellerRequest = createXmlHttpRequestObject();
	aaTaxModellerRequest.open("POST", updateMemberDataURL, true);

	aaTaxModellerRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaTaxModellerRequest.setRequestHeader("Content-length", 0);
    aaTaxModellerRequest.setRequestHeader("Connection", "close");	
    
	aaTaxModellerRequest.onreadystatechange = handleUpdateMemberDataResponse;		     
	
	whizzyModel(true);
	aaTaxModellerRequest.send(null);			
}

function handleUpdateMemberDataResponse ()
{
	if (aaTaxModellerRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaTaxModellerRequest.status == 200) {
			var response = createResponseXML(aaTaxModellerRequest.responseText);
			//alert(aaTaxModellerRequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					// update AaMarginalRateChoice
					var defaultAaMarginalRateNodes = response.getElementsByTagName('AaMarginalTaxRate');
					if (defaultAaMarginalRateNodes.length > 0)
					{
						var defaultAaMarginalRate =  defaultAaMarginalRateNodes[0].firstChild.nodeValue;
						var marginalRateSelect = document.getElementById('AaMarginalRateChoice');
						if (marginalRateSelect)
						{
							for(i=0;i<marginalRateSelect.length;i++)
							{
								if(marginalRateSelect.options[i].value == defaultAaMarginalRate)
								{
									marginalRateSelect.options[i].selected = 'selected';
									marginalRateSelect.selectedIndex = i;		
									break;				
								}														
							}							
						}						
					}
					
					ParseHtml(); 
					callTagHandle();		
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false);		
			}					
		}				
	}	
}

function initPopups()
{
	var contentElement = document.getElementById('content');
	if (contentElement)
	{
		var infoImgs = contentElement.getElementsByTagName('img');
		var infoHrefs = contentElement.getElementsByTagName('a');
		for(var i=0; i<infoImgs.length; i++) 
		{
			var infoId = infoImgs[i].id
			if (infoId && infoId.indexOf('info_') == 0)
			{
				var tagName = infoId.substring(5);
				var popupBalloon = new HelpBalloon({
					returnElement: true,
					title: '',
					contentId: 'popup_' + tagName,
					icon:infoImgs[i],
					iconStyle: {						
						'verticalAlign': 'middle'
					},
					balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
					pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
					button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
					balloonFixedX: 205
				});	
				
				balloonArr.push(popupBalloon);		
			}				
		}
		
		for(var i=0; i<infoHrefs.length; i++) 
		{
			var infoId = infoHrefs[i].id;
			if (infoId && infoId.indexOf('info_') == 0)
			{
				var tagName = infoId.substring(5);
				var popupBalloon = new HelpBalloon({
					returnElement: true,
					title: '',
					contentId: 'popup_' + tagName,
					icon:$(infoId),
					iconStyle: {						
						'verticalAlign': 'middle'
					},
					balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
					pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
					button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
					balloonFixedX: 205
				});	
				
				balloonArr.push(popupBalloon);	
				
				// clear content of marginal tax rate
				document.getElementById('popup_' + tagName).innerHTML = '&nbsp;';
			}				
		}		
	}				
}

function understandCaveat()
{		
	//loadHeadroom();	
	if (document.getElementById('aa_taxmodeller_caveat'))
	{
		document.getElementById('aa_taxmodeller_caveat').style.display = 'none';
		if (document.getElementById('aa_taxmodeller_caveat_print'))
		{
			document.getElementById('aa_taxmodeller_caveat_print').innerHTML = document.getElementById('aa_taxmodeller_caveat').innerHTML;
		}
	}
	if (document.getElementById('aa_taxmodeller_data'))
	{
		document.getElementById('aa_taxmodeller_data').style.display = 'block';
	}		
}

function updateMarginalTaxRate ()
{	
	var updateMarginalTaxRateURL = ajaxurl + "/UpdateMarginalTaxRate";
	if (document.getElementById('AaMarginalRateChoice'))
	{
		var rate = document.getElementById('AaMarginalRateChoice').value;
		var params = "rate=" + rate;
		aaTaxModellerRequest = createXmlHttpRequestObject();
		aaTaxModellerRequest.open("POST", updateMarginalTaxRateURL, true);
	
		aaTaxModellerRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    aaTaxModellerRequest.setRequestHeader("Content-length", params.length);
	    aaTaxModellerRequest.setRequestHeader("Connection", "close");	
	    
		aaTaxModellerRequest.onreadystatechange = handleUpdateMarginalTaxRateResponse;		     
		
		whizzyModel(true);
		aaTaxModellerRequest.send(params);	
		
		
		for (i =0; i<document.getElementsByTagName('td').length; i++)
		{
			var tdElement = document.getElementsByTagName('td')[i];
			if (tdElement.getAttribute("name") == 'AaMarginalRate')
			{
				tdElement.innerHTML = 
					'<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />'
			}
			if (tdElement.getAttribute("name") == 'AltAaMarginalRate')
			{
				tdElement.innerHTML = 
					'<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />'
			}
			if (tdElement.getAttribute("name") == 'AaTaxDue')
			{
				tdElement.innerHTML = 
					'<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />'
			}
			if (tdElement.getAttribute("name") == 'AltAaTaxDue')
			{
				tdElement.innerHTML = 
					'<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />'
			}
		}
		
		for(var i=0; i<balloonArr.length; i++) {
			balloonArr[i].hide();
		}
	}
}

function handleUpdateMarginalTaxRateResponse ()
{
	if (aaTaxModellerRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaTaxModellerRequest.status == 200) {
			var response = createResponseXML(aaTaxModellerRequest.responseText);					
			//alert(aaTaxModellerRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{					
					var aaMarginalRateNodes = response.getElementsByTagName('AaMarginalTaxRate');
					var aaMarginalRate =  '&nbsp';
					var aaTaxDue =  '&nbsp';
					var altAaTaxDue =  '&nbsp';
					if (aaMarginalRateNodes.length > 0)
					{
						aaMarginalRate =  aaMarginalRateNodes[0].firstChild.nodeValue;										
					}
					
					var aaTaxDueNodes = response.getElementsByTagName('AaTaxDue');
					if (aaTaxDueNodes.length > 0)
					{
						aaTaxDue =  aaTaxDueNodes[0].firstChild.nodeValue;											
					}
					
					var altAaTaxDueNodes = response.getElementsByTagName('AltAaTaxDue');
					if (altAaTaxDueNodes.length > 0)
					{
						altAaTaxDue =  altAaTaxDueNodes[0].firstChild.nodeValue;					
					}
					
					for (i =0; i<document.getElementsByTagName('td').length; i++)
					{
						var tdElement = document.getElementsByTagName('td')[i];
						if (tdElement.getAttribute("name") == 'AaMarginalRate')
						{
							tdElement.innerHTML = aaMarginalRate + '%&nbsp;';
						}
						if (tdElement.getAttribute("name") == 'AltAaMarginalRate')
						{
							tdElement.innerHTML = aaMarginalRate + '%&nbsp;';
						}
						if (tdElement.getAttribute("name") == 'AaTaxDue')
						{
							tdElement.innerHTML = '&pound;' + aaTaxDue + '&nbsp;'
						}
						if (tdElement.getAttribute("name") == 'AltAaTaxDue')
						{
							tdElement.innerHTML = '&pound;' + altAaTaxDue + '&nbsp;'
						}
					}					
					
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false);		
			}					
		}
	}	
}
			
function loadHeadroom()
{
	var aaTaxModelleHeadroomURL = ajaxurl + "/HeadroomCheckingService";
	var params = "";
	aaTaxModellerRequest = createXmlHttpRequestObject();
	aaTaxModellerRequest.open("POST", aaTaxModelleHeadroomURL, true);

	aaTaxModellerRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaTaxModellerRequest.setRequestHeader("Content-length", params.length);
    aaTaxModellerRequest.setRequestHeader("Connection", "close");	
    
	aaTaxModellerRequest.onreadystatechange = handleHeadroomResult;		       

	//whizzy(true);		
	
	salarySlider = null;
	taxrateSlider = null;	
	aaTaxModellerRequest.send(params);	
	
}

function handleHeadroomResult(){

   	if (aaTaxModellerRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaTaxModellerRequest.status == 200) {
			//alert(aaTaxModellerRequest.responseText.length);
			var response = createResponseXML(aaTaxModellerRequest.responseText);					
			
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					whizzy(false);
					return false;
				}
				else
				{
					// Headroom
					handleHeadroomData(response);						
					
					// Tax year
					//handleTaxYearData(response);										
				} 					
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzy(false);		
			}						
		}
	}
}

function handleHeadroomData (response)
{
	if (response)
	{
		// update scheme history
		var headroomHistoriesInnerHTML = '<table class="aa_headroom_table" cellspacing="0" cellpadding="0">' +
				'<tr><th class="longlabel" colspan="9">Service history</th></tr>';
		headroomHistoriesInnerHTML += '<tr><th width="80">From</th><th width="80">To</th><th width="35">Cat</th>' +
				'<th width="35">years</th><th width="35">days</th><th width="40">total years</th><th width="35">FTE</th><th width="40">accrual</th>' + 
				'<th width="60">accrued benefit</th></tr> ';
				
		var headroomHistoryNodes = response.getElementsByTagName(headroom_history_node_name);

		for(var i=0; i<headroomHistoryNodes.length; i++) 
		{
			var headroomHistoryNode = headroomHistoryNodes[i];
			
			headroomHistoryInnerHTML = '<tr>';
			if (headroomHistoryNode.getElementsByTagName('from').length > 0 && headroomHistoryNode.getElementsByTagName('from')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('from')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
	
			if (headroomHistoryNode.getElementsByTagName('to').length > 0 && headroomHistoryNode.getElementsByTagName('to')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('to')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('category').length > 0 && headroomHistoryNode.getElementsByTagName('category')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('category')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('years').length > 0 && headroomHistoryNode.getElementsByTagName('years')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('years')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('days').length > 0 && headroomHistoryNode.getElementsByTagName('days')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('days')[0].firstChild.nodeValue + '</td>';
			}
									
			if (headroomHistoryNode.getElementsByTagName('tyears').length > 0 && headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('tyears')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('fte').length > 0 && headroomHistoryNode.getElementsByTagName('fte')[0].firstChild)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('fte')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			
			if (headroomHistoryNode.getElementsByTagName('accrual').length > 0 && headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + headroomHistoryNode.getElementsByTagName('accrual')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			if (headroomHistoryNode.getElementsByTagName('accrued').length > 0 && headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue)
			{
				headroomHistoryInnerHTML += '<td>' + '&pound;' + headroomHistoryNode.getElementsByTagName('accrued')[0].firstChild.nodeValue + '</td>';
			}
			else
			{
				headroomHistoryInnerHTML += '<td>&nbsp;</td>';
			}
			
			headroomHistoryInnerHTML += '</tr>';
			headroomHistoriesInnerHTML += headroomHistoryInnerHTML;
		}
		
		// total row
		var totalYears = '&nbsp;';
		var totalServiceYears = '&nbsp;';
		var totalAccrued = '&nbsp;';
		var totalServiceYears60th = '&nbsp;';
		if (response.getElementsByTagName('TotalYears') && response.getElementsByTagName('TotalYears')[0].firstChild)
		{
			totalYears = response.getElementsByTagName('TotalYears')[0].firstChild.nodeValue;
		}
		if (response.getElementsByTagName('TotalServiceYears') && response.getElementsByTagName('TotalServiceYears')[0].firstChild)
		{
			totalServiceYears = response.getElementsByTagName('TotalServiceYears')[0].firstChild.nodeValue;
		}
		if (response.getElementsByTagName('TotalAccrued') && response.getElementsByTagName('TotalAccrued')[0].firstChild)
		{
			totalAccrued = response.getElementsByTagName('TotalAccrued')[0].firstChild.nodeValue;
		}
		if (response.getElementsByTagName('TotalServiceYearsAt60th') && response.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild)
		{
			totalServiceYears60th = response.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild.nodeValue;
			servicesTo1st = getDouble(response.getElementsByTagName('TotalServiceYearsAt60th')[0].firstChild.nodeValue);
		}

		headroomHistoriesInnerHTML += '<tr><td class="empty" colspan=5>&nbsp;</td>' +
				'<td class="total">' + totalYears + 
				'</td><td class="empty" colspan=2>&nbsp;</td>' + 
				'<td class="total">' + totalAccrued + '</td></tr></table> ';
		
		// update total accrued to tax modeller table
//		if (document.getElementById('accrued_to_date'))
//		{
//			document.getElementById('accrued_to_date').innerHTML = '&pound;' + totalAccrued;
//		}		
		// update to print
		if (document.getElementById('table_BenefitAtPipStart') && document.getElementById('popup_BenefitAtPipStart'))
		{
			document.getElementById('table_BenefitAtPipStart').innerHTML = headroomHistoriesInnerHTML;
			
			benefitAtPipStartHB = new HelpBalloon({
				returnElement: true,
				title: '',
				content: document.getElementById('popup_BenefitAtPipStart').innerHTML,
				icon:$('info_BenefitAtPipStart'),
				iconStyle: {
					'verticalAlign': 'middle'
				},		
				balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
				pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
				button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
				titleStyle: {
					'color': '#090',
					'fontSize': '16px',
					'fontWeight': 'bold',
					'fontFamily': 'Arial',
					'vertical-align': 'top'
				},
				balloonFixedX: 205
			});			
		}		
	}
}

function handleTaxYearData (response)
{
	if (response)
	{
		for(var i=0; i<taxyear_fields.length; i++) 
		{
			var tag_name = taxyear_fields[i];
			var update_nodes = response.getElementsByTagName(tag_name);
			
			if (update_nodes.length > 0)
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
				}

				if (tag_name == 'SOYSalary')
				{
					// update slider
					inputSalary = getDouble(update_nodes[0].firstChild.nodeValue);
					if (document.getElementById('modelled_salary'))
					{
						document.getElementById('modelled_salary').innerHTML = '&pound;' + update_nodes[0].firstChild.nodeValue;
					}
				}
				if (tag_name == 'AccruedRate')
				{
					// update slider
					accrual = update_nodes[0].firstChild.nodeValue;
				}
								
				if (tag_name == 'TaxRate')
				{
					// update slider
					inputTaxrate = getDouble(update_nodes[0].firstChild.nodeValue);
					if (document.getElementById('modelled_taxrate'))
					{
						document.getElementById('modelled_taxrate').innerHTML = update_nodes[0].firstChild.nodeValue;
					}
				}
				if (tag_name == 'Tax')
				{
					var taxAmount = getDouble(update_nodes[0].firstChild.nodeValue);
					if (document.getElementById('tax_amount'))
					{
						document.getElementById('tax_amount').innerHTML = '&pound;' + update_nodes[0].firstChild.nodeValue;
					}
					if (document.getElementById('tax_status'))
					{
						if (taxAmount > 0)
						{
							document.getElementById('tax_status').innerHTML = 
								"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/red-indicator.png\" alt=\"Red\" />";
						}
						else
						{
							document.getElementById('tax_status').innerHTML = 
								"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/green-indicator.png\" alt=\"Green\" />";
						}
					}
				}	
				
				if (tag_name == 'Year')
				{
					var soyDay = '1-January-' + update_nodes[0].firstChild.nodeValue;
					var eoyDay = '31-December-' + update_nodes[0].firstChild.nodeValue;
					if (document.getElementById('accrued_date'))
					{
						document.getElementById('accrued_date').innerHTML = soyDay;
					}
					if (document.getElementById('salary_date'))
					{
						document.getElementById('salary_date').innerHTML = eoyDay;
					}
				}
				if (tag_name == 'TaxYear')
				{
					if (document.getElementById('tax_year'))
					{
						document.getElementById('tax_year').innerHTML = update_nodes[0].firstChild.nodeValue;
					}
				}
			}
			else
			{
				var update_element = document.getElementById(tag_name);
				if (update_element != null)
				{
					update_element.innerHTML = "&nbsp;";
				}							
			}
		}								
	}
}


function popupSalarySlider(_this, _event)
{
//	if (salarySlider == null)
//	{
//		salarySlider = new slider('salary_slider', SLIDER_SALARY, SLIDER_CONFIG);
//	}
	// close other slider first
	hideTaxrateSlider();
	
	var salaryInputDialog = document.getElementById('salary_slider_panel');
    if (salaryInputDialog )
    {
        salaryInputDialog.style.display='block';
        salaryInputDialog.style.position = 'absolute';
        salaryInputDialog.style.pixelTop = _findPosY(_this) - 240;
        salaryInputDialog.style.pixelLeft = _findPosX(_this) + 40;
        salaryInputDialog.style.top = (_findPosY(_this)  - 240) + "px";	//firefox
        salaryInputDialog.style.left = _findPosX(_this) + 40 + "px"; // firefox
        if (_event)
        {
	        salaryInputDialog.style.pixelLeft = _event.x;  
	        salaryInputDialog.style.left = _event.x + "px"; // firefox   	
        }
    }
    
    if (document.getElementById('input_salary'))
    {
    	document.getElementById('input_salary').value = inputSalary;
    	//salarySlider.f_setValue(inputSalary);
    }
}

function hideSalarySlider()
{
	var salaryInputDialog = document.getElementById('salary_slider_panel');
    if (salaryInputDialog )
    {
        salaryInputDialog.style.display='none';        
    }	
    if (document.getElementById('input_salary'))
    {
    	document.getElementById('input_salary').value = inputSalary;
    }
}

function modelSalary()
{
	if (document.getElementById('input_salary'))
    {
    	inputSalary = document.getElementById('input_salary').value;
    } 
    
    submitTaxModelling();
    
    var salaryInputDialog = document.getElementById('salary_slider_panel');
    if (salaryInputDialog )
    {
        salaryInputDialog.style.display='none';        
    }
}

function popupTaxrateSlider(_this, _event)
{
//	if (taxrateSlider == null)
//	{
//		taxrateSlider = new slider('taxrate_slider', SLIDER_TAXRATE, SLIDER_CONFIG);
//	}	
	
	// close salary slider first
	hideSalarySlider();
	
	var taxrateInputDialog = document.getElementById('taxrate_slider_panel');
    if (taxrateInputDialog )
    {
        taxrateInputDialog.style.display='block';
        taxrateInputDialog.style.position = 'absolute';
        taxrateInputDialog.style.pixelTop = _findPosY(_this) - 240;
        taxrateInputDialog.style.pixelLeft = _findPosX(_this) + 40;
        taxrateInputDialog.style.top = (_findPosY(_this) - 240) + "px";	//firefox
        taxrateInputDialog.style.left = _findPosX(_this) + 40 + "px"; // firefox
        if (_event)
        {
	        taxrateInputDialog.style.pixelLeft = _event.x;  
	        taxrateInputDialog.style.left = _event.x + "px"; // firefox   	
        }        
    }
    
    if (document.getElementById('input_taxrate'))
    {
    	document.getElementById('input_taxrate').value = inputTaxrate;
    	//taxrateSlider.f_setValue(inputTaxrate);
    }
}

function hideTaxrateSlider()
{
	var taxrateInputDialog = document.getElementById('taxrate_slider_panel');
    if (taxrateInputDialog )
    {
        taxrateInputDialog.style.display='none';        
    }	
    if (document.getElementById('input_taxrate'))
    {
    	document.getElementById('input_taxrate').value = inputTaxrate;
    }
}

function modelTaxrate()
{
	if (document.getElementById('input_taxrate'))
    {
    	inputTaxrate = document.getElementById('input_taxrate').value;
    } 
    
    submitTaxModelling();
    
    var taxrateInputDialog = document.getElementById('taxrate_slider_panel');
    if (taxrateInputDialog )
    {
        taxrateInputDialog.style.display='none';        
    }
}


function submitTaxModelling()
{
	var modelledSalary = inputSalary;
	var modelledTaxrate = inputTaxrate;

	if (modelledSalary == null || modelledSalary.length == 0)
	{
		alert('Please input an current salary ');
		return false;
	} else {
		if (isNaN(modelledSalary)) {
			alert('Please only input a number ');
			return false;
		}
  	} 
	
	if (modelledTaxrate == null || modelledTaxrate.length == 0)
	{
		alert('Please input an allowance tax rate');
		return false;
	} else {
		if (isNaN(modelledTaxrate)) {
			alert('Please only input a number ');
			return false;
		}
  	} 
	
	var taxChoiceSelectURL = ajaxurl + "/TaxModellerService";
	var params = "salary=" + modelledSalary + 
		"&taxrate=" + modelledTaxrate + 
		"&services=" + servicesTo1st + 
		"&accrual=" + accrual + "&sdate=" + systemDate;

	aaTaxModellerRequest = createXmlHttpRequestObject();
	aaTaxModellerRequest.open("POST", taxChoiceSelectURL, true);

	aaTaxModellerRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aaTaxModellerRequest.setRequestHeader("Content-length", params.length);
    aaTaxModellerRequest.setRequestHeader("Connection", "close");	
    
	aaTaxModellerRequest.onreadystatechange = handleTaxYearModelResult;		     
	
	whizzyModel(true);
	aaTaxModellerRequest.send(params);	
	
}

function handleTaxYearModelResult ()
{
	if (aaTaxModellerRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaTaxModellerRequest.status == 200) {
			var response = createResponseXML(aaTaxModellerRequest.responseText);					
			//alert(aaTaxModellerRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{					
					for(var i=0; i<taxyear_fields.length; i++) 
					{
						var tag_name = taxyear_fields[i];
						var update_nodes = response.getElementsByTagName(tag_name);
						
						if (update_nodes.length > 0)
						{
							var update_element = document.getElementById(tag_name);
							if (update_element != null)
							{
								update_element.innerHTML = update_nodes[0].firstChild.nodeValue;
							}
							if (tag_name == 'SOYSalary')
							{
								// update slider
								inputSalary = getDouble(update_nodes[0].firstChild.nodeValue);
								if (document.getElementById('modelled_salary'))
								{
									document.getElementById('modelled_salary').innerHTML = '&pound;' + update_nodes[0].firstChild.nodeValue;
								}
							}
							if (tag_name == 'TaxRate')
							{
								// update slider
								inputTaxrate = getDouble(update_nodes[0].firstChild.nodeValue);
								if (document.getElementById('modelled_taxrate'))
								{
									document.getElementById('modelled_taxrate').innerHTML = update_nodes[0].firstChild.nodeValue;
								}
							}
							if (tag_name == 'Tax')
							{
								var taxAmount = getDouble(update_nodes[0].firstChild.nodeValue);
								if (document.getElementById('tax_amount'))
								{
									document.getElementById('tax_amount').innerHTML = '&pound;' + update_nodes[0].firstChild.nodeValue;
								}
								if (document.getElementById('tax_status'))
								{
									if (taxAmount > 0)
									{
										document.getElementById('tax_status').innerHTML = 
											"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/red-indicator.png\" alt=\"Red\" />";
									}
									else
									{
										document.getElementById('tax_status').innerHTML = 
											"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/green-indicator.png\" alt=\"Green\" />";
									}
								}
							}	
							
//							if (tag_name == 'Year')
//							{
//								var soyDay = '1-January-' + update_nodes[0].firstChild.nodeValue;
//								var eoyDay = '31-December-' + update_nodes[0].firstChild.nodeValue;
//								if (document.getElementById('accrued_date'))
//								{
//									document.getElementById('accrued_date').innerHTML = soyDay;
//								}
//								if (document.getElementById('salary_date'))
//								{
//									document.getElementById('salary_date').innerHTML = eoyDay;
//								}
//							}
//							if (tag_name == 'TaxYear')
//							{
//								if (document.getElementById('tax_year'))
//								{
//									document.getElementById('tax_year').innerHTML = update_nodes[0].firstChild.nodeValue;
//								}
//							}
						}
						else
						{
							var update_element = document.getElementById(tag_name);
							if (update_element != null)
							{
								update_element.innerHTML = "&nbsp;";
							}							
						}
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false);		
			}					
		}
	}
}
var thankyouMessage;

function selectTaxChoice()
{
	if (document.getElementById('tax_choice'))
	{
		var taxChoice = document.getElementById('tax_choice').value;

		var taxChoiceSelectURL = ajaxurl + "/SelectTaxChoice";
		var params = "choice=" + taxChoice;
	
		aaTaxModellerRequest = createXmlHttpRequestObject();
		aaTaxModellerRequest.open("POST", taxChoiceSelectURL, true);
	
		aaTaxModellerRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    aaTaxModellerRequest.setRequestHeader("Content-length", params.length);
	    aaTaxModellerRequest.setRequestHeader("Connection", "close");	
	    
		aaTaxModellerRequest.onreadystatechange = handleSelectTaxChoiceResponse;		     
		
		whizzyModel(true);
		aaTaxModellerRequest.send(params);	
		
		if (document.getElementById('thankyou_message') && document.getElementById('select_choice_div'))
		{
			thankyouMessage = document.getElementById('thankyou_message').innerHTML;
			
			document.getElementById('select_choice_div').innerHTML = '<p>' + 
				'<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />' + '</p>';
		}		
	}	
}

function handleSelectTaxChoiceResponse ()
{
	if (aaTaxModellerRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aaTaxModellerRequest.status == 200) {
			var response = createResponseXML(aaTaxModellerRequest.responseText);					
			//alert(aaTaxModellerRequest.responseText);
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{					
					if (document.getElementById('select_choice_div'))
					{						
						document.getElementById('select_choice_div').innerHTML = '<p>' + thankyouMessage + '</p>';
					}
					var currentChoiceNodes = response.getElementsByTagName('CurrentChoice');
					var currentChoiceDateNodes = response.getElementsByTagName('CurrentChoiceDate');
					var currentChoice =  '&nbsp';
					var currentChoiceDate =  '&nbsp';
					if (currentChoiceNodes.length > 0)
					{
						currentChoice =  currentChoiceNodes[0].firstChild.nodeValue;										
					}
					if (currentChoiceDateNodes.length > 0)
					{
						currentChoiceDate =  currentChoiceDateNodes[0].firstChild.nodeValue;										
					}
					
					if (document.getElementById('AaCurrentChoice'))
					{						
						document.getElementById('AaCurrentChoice').innerHTML = currentChoice;
					}
					if (document.getElementById('AaCurrentChoiceDate'))
					{						
						document.getElementById('AaCurrentChoiceDate').innerHTML = currentChoiceDate;
					}
					
					if (document.getElementById('current_choice_div'))
					{						
						document.getElementById('current_choice_div').style.display = 'block';
					}
				}	
			}
			catch(err){
				alert("XML Parser error." + err);
				whizzyModel(false);		
			}					
		}
	}	
}

function whizzy (isWhizzy)
{
	if (isWhizzy)
	{
		for(var i=0; i<updated_tax_fields.length; i++) 
		{
			var tag_name = updated_tax_fields[i];	
			var update_element = document.getElementById(tag_name);
			if (update_element)		
			{	
				update_element.innerHTML = 
					"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />";
			}
		} 	

	}
	else
	{
		for(var i=0; i<updated_tax_fields.length; i++) 
		{
			var tag_name = updated_tax_fields[i];	
			var update_element = document.getElementById(tag_name);
			if (update_element)		
			{	
				update_element.innerHTML = "&nbsp;";
			}
		} 	
	}
}

function whizzyModel (isWhizzy)
{
	if (isWhizzy)
	{
		for(var i=0; i<modelled_tax_fields.length; i++) 
		{
			var tag_name = modelled_tax_fields[i];	
			var update_element = document.getElementById(tag_name);
			if (update_element)		
			{	
				update_element.innerHTML = 
					"<img src=\"/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/spinning_image.gif\" alt=\"running...\" />";
			}
		} 	

	}
	else
	{
		for(var i=0; i<modelled_tax_fields.length; i++) 
		{
			var tag_name = modelled_tax_fields[i];	
			var update_element = document.getElementById(tag_name);
			if (update_element)		
			{	
				update_element.innerHTML = "&nbsp;";
			}
		} 	
	}
}