/**
 *  JavaScript page parser for AjAX 
 *  
 *
 *  @author dcarlyle
 *  @author $Author: cvsuser $
 *  @version $Revision: 1.3 $
 * 
 *  $Id: Exp $
 */
// Handle Data
//check if arrays have data, default is false
	var working = false; //normaltag
	var condition = false; //condition tag
	var CurrentWorking  = false;
	var TwinWorking= false;
	var DictionWorking =false;
	var DictionShortWorking=false;
	var CaveatWorking =false;
	var CaveatShortWorking = false;/**/
	var FAQWorking =false;
 	var ShortFAQWorking = false;/**/
        var BonusWaiverWork = false;
/*End check*/
/*Get Ajax object*/
	var http = createXmlHttpRequestObject(); // for nomaltag
	var conditionHttp = createXmlHttpRequestObject(); //for condition tag
	var tableHttp = createXmlHttpRequestObject(); //for table tag
	//var CurrentHttp = createXmlHttpRequestObject(); //No longer used
	var Twin  = createXmlHttpRequestObject();
	var dictionaryHttp= createXmlHttpRequestObject();
	var caveatHttp = createXmlHttpRequestObject();
	var FAQHttp = createXmlHttpRequestObject();
	var httpContributary= createXmlHttpRequestObject();
	var AddressHttp=createXmlHttpRequestObject();
    var WaiverHttp=createXmlHttpRequestObject();
	var httpBookMark = createXmlHttpRequestObject(); // for nomaltag
/*End*/
var ns6 = document.getElementById && ! document.all;

var dataResponsed = false;

/*Start get normal tag*/
function startGetData(name , update_field){
		
		working = true;
		
 		//generate the xml content
        //alert('startGetData: ' + name);

		var xml = generateXML(name, update_field);
		
		//alert(xml);
		// Build the URL to connect to
		var url = staticURL + escape(xml);   //goes to Java Servlet
		
		http.open("POST", url);
		
		http.onreadystatechange = handleResponse;
		
		http.send(null);
		
		dataResponsed = false;
		
	}
/*End*/
/*Start get normal tag*/
function startGetConditionTagData(name , update_field){
		condition = true;
		
		//generate the xml content
		var xml = generateCondXML(name, update_field);

		//alert('xml: ' + xml);
		// Build the URL to connect to
		var url = staticCondURL + escape(xml);   //goes to Java Servlet
		document.getElementById(update_field).style.display = "none";
		
		conditionHttp.open("POST", url);
		
		conditionHttp.onreadystatechange = handleConditionTagResponse;
		
		conditionHttp.send(null);
		
	}	

function generateCondXML(expression, update_field)
{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";
	    xml += "	<Member>\n";
		xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
	    xml += "		<Refno>" + crefno+ "</Refno>\n";
	    xml += "	</Member>\n";
		xml += "	<Type>CONDITIONAL</Type>\n";	    
		xml += "	<RequestedTag>" + expression + "</RequestedTag>\n";
	    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
		xml += "</AjaxParameterRequest>\n";
		return xml;	
}
/*Start get conditional tag*/
function startGetTableTagData(table, update_field){
		//generate the xml content
		var xml = generateTableXML(table, update_field);

		//alert(xml);
		// Build the URL to connect to
		var url = staticTableURL + escape(xml);   //goes to Java Servlet

		tableHttp.open("POST", url);
		
		tableHttp.onreadystatechange = handleTableTagResponse;
		
		tableHttp.send(null);
		
	}	

function generateTableXML(table, update_field)
{
		var xml = "";
		xml += "<AjaxParameterRequest>\n";
	    xml += "	<Member>\n";
		xml += "		<Bgroup>" + bgroup + "</Bgroup>\n";
	    xml += "		<Refno>" + crefno+ "</Refno>\n";
	    xml += "	</Member>\n";
		xml += "	<Type>TABLE</Type>\n";	    
		xml += "	<RequestedTag>" + table + "</RequestedTag>\n";	
	    xml += "	<UpdateField>" + update_field + "</UpdateField>\n";
		xml += "</AjaxParameterRequest>\n";
		return xml;	
}

	/*End*/
/*Handle Request Dictionary*/
function DictionaryGetData(scheme,phrase, updateField)
{
	DictionWorking = true;
	
	//generate the xml content
	var xml = generateDictionaryXML(scheme, phrase, updateField);
	
	// Build the URL to connect to
	var url = staticDicURL + escape(xml);   //goes to Java Servlet

	//alert(xml);
	//alert(url);
	dictionaryHttp.open("POST", url);
	
	dictionaryHttp.onreadystatechange = DictionaryHandleResponse;
	
	dictionaryHttp.send(null);
}
/*End*/
/*handle Caveat tag*/
function CaveAtGetData(scheme,phrase)
{
	CaveatWorking = true;
	
	//generate the xml content
	var xml = generateCaveAtXML(scheme, phrase);
	
	// Build the URL to connect to
	var url = staticCaveURL + escape(xml);   //goes to Java Servlet
	
	caveatHttp.open("POST", url);
	
	caveatHttp.onreadystatechange = CaveatHandleResponse;
	
	caveatHttp.send(null);
}
/*End*/
/*handle FAQ tag*/
function FAQGetData(scheme,phrase)
{
	FAQWorking = true;
	
	//generate the xml content
	var xml = generateFAQXML(scheme, phrase);
	//alert(xml);
	// Build the URL to connect to
	var url = faqUrl + escape(xml);   //goes to Java Servlet
	
	FAQHttp.open("POST", url);
	
	FAQHttp.onreadystatechange = FAQHandleResponse;
	
	FAQHttp.send(null);
}
/*Create response XML from data received from server*/
function createResponseXML(textXML){

	// code for IE
	if (window.ActiveXObject)
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");


		xmlDoc.async="false";
		xmlDoc.loadXML(textXML);
		return xmlDoc;
	}
	// code for Mozilla, Firefox, Opera, etc.
	else
	{
		var parser=new DOMParser();
		var xmlDoc = parser.parseFromString(textXML,"text/xml");
		return xmlDoc;
	}
}
/*End*/

function handleResponse(){
	if (http.readyState == 4) {
						
		// Check that a successful server response was received
      	if (http.status == 200) {
			var response = createResponseXML(http.responseText);					
			//alert(http.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var fieldType;
				var value ="";
                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
                    value = "<span style='color: red'>" + response.getElementsByTagName("Message")[0].firstChild.nodeValue + "</span>";
					fieldType = "";
				}
				else{
        			if(response.getElementsByTagName("Value")[0].childNodes.length>0)
        			{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
						value = value.replace(/&lt;/g, '<');
						value = value.replace(/&gt;/g, '>');
						if (value == "null") value = "";
                    }
				}
				

				populateFieldValue(elementToUpdate, value, "Text");
				populateFieldValue('popup_'+ elementToUpdate, value, "Text");
  
				if(request_array.length) {
					// get next element off the array.
					var next = request_array[0];
					request_array.shift();
			
					// set this as late as possible to avoid conflict
					working = false;
					startGetData(next[0], next[1]);
				} else {
					// nothing in the queue so signal complete.
					working = false;
				}
				
				dataResponsed = true;
			}
			catch(err){
				alert("XML Parser error." );
				//hide spinning image
				visiWhizzy(elementToUpdate, false);				
			}			
		}else {		
			//hide spinning image
			visiWhizzy(elementToUpdate, false);
	    }                            
	}
			
}
/*End*/
/*Handle condition tag response from server*/
function handleConditionTagResponse()
{
	if (conditionHttp.readyState == 4) {												
		// Check that a successful server response was received
      	if (conditionHttp.status == 200) 
		{
			var response = createResponseXML(conditionHttp.responseText);
			//alert(conditionHttp.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var fieldType;
				var value = "FALSE";

				var error = response.getElementsByTagName("Error").length;
				
				if(error > 0)
				{			
                    CondErrorTag.push(Array("<![CDATA[" + fieldName + "]]>", elementToUpdate));
				}
				else 
				{
					if(response.getElementsByTagName("Value")[0].childNodes.length>0)
					{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
					}					
				}
				
				var divElement = document.getElementById(elementToUpdate);					
				if(value.toLowerCase() == "true")
				{		
					divElement.style.display = ""
				}
				else
				{
					divElement.style.display = "none"
				}

				if(conditionTagArray.length) {
				// Pop the top element off the array.
					var next = conditionTagArray.pop();
			    
					// set this as late as possible to avoid conflict
					condition = true;
					startGetConditionTagData(next[0], next[1]);
				} else {
					// nothing in the queue so signal complete.
					condition = false;
				}
			}
			catch(err)
			{
				alert(err);
			}	
		}			
	}
}
/*End*/
function handleTableTagResponse()
{
	if (tableHttp.readyState == 4) {												
		// Check that a successful server response was received
      	if (tableHttp.status == 200) 
		{
			var response = createResponseXML(tableHttp.responseText);
			//alert(tableHttp.responseText);
			try{
				// parse the XML to get the values from XML above
				var elementToUpdate = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;
				var fieldName = response.getElementsByTagName("RequestedTag")[0].firstChild.nodeValue;
				var value = "";

				var error = response.getElementsByTagName("Error").length;
				
				if(error > 0){
					value = "<span style='color: red'>" + fieldName + "</span>";
				}
				else 
				{
					if(response.getElementsByTagName("Value")[0].childNodes.length>0)
					{
						value = response.getElementsByTagName("Value")[0].firstChild.nodeValue;
						// apply style to the table
//						value = value.replace(/<table>/g, '<table class="datatable">');
//						value = value.replace(/<th/g, '<th class="label"');
//						value = value.replace(/<td>/g, '<td align=center>');						
					}					
				}
				
				var appliedElement = document.getElementById(elementToUpdate);				
				if (appliedElement.getAttribute('style'))
				{		
					appliedElement.removeAttribute('style');
				}
				
				// Replace content to display table
				var content = appliedElement.innerHTML;
				var startReplace = content.indexOf(startMetaDataChars);
				var endReplace = content.indexOf(stopMetaDataChars);
				if (startReplace >= 0 && endReplace > startReplace)
				{
					content = content.substring(0, startReplace) + value + content.substring(endReplace + stopMetaDataChars.length);						
				}				
				appliedElement.innerHTML = content;

				// get next tag
				if(tableTagArray.length) {
					var next = tableTagArray.shift();
					startGetTableTagData(next[0], next[1]);
				}
			}
			catch(err)
			{
				alert(err);
			}	
		}			
	}
}
/*End*/
/*Handle Dictionary data response from server*/
function DictionaryHandleResponse()
{
	if (dictionaryHttp.readyState == 4) {
							
		// Check that a successful server response was received

      	if (dictionaryHttp.status == 200) 
		{
			var tmpResponse = dictionaryHttp.responseText;
			var response = createResponseXML(tmpResponse );
			//alert(tmpResponse);
			try{
				if(tmpResponse.indexOf("<Message>")<0)
				{
					// parse the XML to get the values from XML abov
					var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var TitleElement = response.getElementsByTagName("Title")[0].firstChild.nodeValue;
					var UpdateElement = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;						
					var value = response.getElementsByTagName("Definition")[0].firstChild.nodeValue;
					var ContentID = UpdateElement + "_C";

					var dictElement = document.getElementById(UpdateElement);
					if (dictElement .getAttribute('style'))
					{		
						dictElement.removeAttribute('style');
					}											
					dictElement.innerHTML = "<span class='fakelink' style='cursor:pointer; cursor:hand; font-size:1em;' onclick=ViewTag('"+ContentID+"',this); onmouseout=HideTag('"+ContentID+"');>"+ TitleElement+"</span>";					

					var x=document.getElementById("DIVTEMPLATE");
					x.innerHTML += "<span class='dictionary_definition' style='display:none;' id="+ContentID+"><span class=bookmark_component_holder><span class=bookmark_value>"+value+"</span></span></span>";		
				}
				else
				{
					// parse the XML to get the values from XML abov
					var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var TitleElement = response.getElementsByTagName("Title")[0].firstChild.nodeValue;
					var UpdateElement = response.getElementsByTagName("UpdateField")[0].firstChild.nodeValue;					
					var value = response.getElementsByTagName("Message")[0].firstChild.nodeValue;

					var dictElement = document.getElementById(UpdateElement);											
					dictElement.innerHTML = "<font color=#FF0000>"+ErrorElement+"</font>";
				}
			}
			catch(err){
				//alert("XML Parser error." );
				alert(err);
				//hide spinning image
				//visiWhizzy("", false);
			}
			
			//hide spinning image
			visiWhizzy("", false);
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);
			
	    }
		
		if(dictionaryArr.length) {
			// Pop the top element off the array.
			var next = dictionaryArr[0];
			dictionaryArr.shift();
    
			// set this as late as possible to avoid conflict
			DictionWorking = true;
			DictionaryGetData(next[0], next[1], next[2]);
		} 
		else {
			// nothing in the queue so signal complete.
			DictionWorking = false;
		}
	}
}
/*End*/
/*FAQ handle*/
function FAQHandleResponse()
{
	if (FAQHttp.readyState == 4) {
							
		// Check that a successful server response was received
      	if (FAQHttp.status == 200) 
		{
            var response = createResponseXML(FAQHttp.responseText);
			var tmpResponse = FAQHttp.responseText;

			try{					
				var foundPoint= -1;
				var endPoint  = -1;
                var error = response.getElementsByTagName("Error").length;
				
				if(error > 0){							
					value = "<span style='color: red'>" + response.getElementsByTagName("Message")[0].firstChild.nodeValue + "</span>";
					fieldType = "";						
				}
				else
				{
					
					foundPoint = tmpResponse.indexOf("<Anchor>");
					endPoint = tmpResponse.indexOf("</Anchor>");
					var AnchorElement = tmpResponse.substring(foundPoint + "<Anchor>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Question>");
					endPoint = tmpResponse.indexOf("</Question>");
					var QuestionElement = tmpResponse.substring(foundPoint + "<Question>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Phrase>");
					endPoint = tmpResponse.indexOf("</Phrase>");
					var PhraseElement = tmpResponse.substring(foundPoint + "<Phrase>".length, endPoint);
                    foundPoint = tmpResponse.indexOf("<Title>");
					endPoint = tmpResponse.indexOf("</Title>");
					var TitleElement = tmpResponse.substring(foundPoint + "<Title>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Scheme>");
					endPoint = tmpResponse.indexOf("</Scheme>");
					var SchemeElement = tmpResponse.substring(foundPoint + "<Scheme>".length, endPoint);

					var value ;
					
					var valueID=SchemeElement+""+PhraseElement;											
					var x=document.getElementById(valueID);
					if(SchemeElement.length>0)
					{
						SchemeElement=SchemeElement+"_";
					}
					SchemeElement=SchemeElement.toLowerCase();
					x.outerHTML="<a href="+ajaxurl+"/pl/faq/"+SchemeElement+"faq.jsp#"+AnchorElement+">"+TitleElement+"</a> ";		

				}
			}
			catch(err)
			{
				//alert("XML Parser error." );
				alert(err);
			}
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);			
	    }
		
		if(faqArray.length) {
			// Pop the top element off the array.
			var next = longFaqArray[0];
			faqArray.shift();
    
			// set this as late as possible to avoid conflict
			FAQWorking = true;
			FAQGetData(next[0], next[1]);
		}
		else {
			// nothing in the queue so signal complete.
			FAQWorking = false;
		}
		
	}
}
/*end*/
/*Caveat handle*/
function CaveatHandleResponse()
{
	if (caveatHttp.readyState == 4) {
							
		// Check that a successful server response was received
      	if (caveatHttp.status == 200) 
		{
			var tmpResponse = caveatHttp.responseText;

			try{
				if(tmpResponse.indexOf("<Message>")<=0)
				{
					// parse the XML to get the values from XML abov
					//var PhraseElement = response.getElementsByTagName("Phrase")[0].firstChild.nodeValue;
					//var SchemeElement = response.getElementsByTagName("Scheme")[0].firstChild.nodeValue;
					var foundPoint= -1;
					var endPoint  = -1;

					foundPoint = tmpResponse.indexOf("<Phrase>");
					endPoint = tmpResponse.indexOf("</Phrase>");
					var PhraseElement = tmpResponse.substring(foundPoint + "<Phrase>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Title>");
					endPoint = tmpResponse.indexOf("</Title>");
					var TitleElement = tmpResponse.substring(foundPoint + "<Title>".length, endPoint);
					foundPoint = tmpResponse.indexOf("<Scheme>");
					endPoint = tmpResponse.indexOf("</Scheme>");
					var SchemeElement = tmpResponse.substring(foundPoint + "<Scheme>".length, endPoint);

					var value ;
					
					var valueID=SchemeElement+""+PhraseElement;
					valueID= noWhiteSpace(valueID);	
					foundPoint=tmpResponse.indexOf("<Description>");
					endPoint = tmpResponse.indexOf("</Description>");

					value =tmpResponse.substring(foundPoint+"<Description>".length,endPoint);
																
										
					var x=document.getElementById(valueID);

					x.outerHTML="<b>"+TitleElement +"</b><br><p style='width:500'>"+value+"</p>";		

				}
			}
			catch(err){
				//alert("XML Parser error." );
				alert(err);
				//hide spinning image
				visiWhizzy("", false);
			}
			
			//hide spinning image
			visiWhizzy("", false);
							
		}
		else {
		
	        // An HTTP problem has occurred
	        //alert("HTTP error: " + http.status);
			
			//hide spinning image
			visiWhizzy("", false);
			
	    }
		
		if(caveatArr.length) {
			// Pop the top element off the array.
			var next = caveatArr[0];
			caveatArr.shift();
    
			// set this as late as possible to avoid conflict
			CaveatWorking = false;
			CaveAtGetData(next[0], next[1]);
		}
		else {
			// nothing in the queue so signal complete.
			CaveatWorking = false;
		}
		
	}
}
/*end*/
/*Old function*/
function createXmlHttpRequestObject() {
    var ro;
    var browser = navigator.appName;
    // Need to determine IE7 and not do this.
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}