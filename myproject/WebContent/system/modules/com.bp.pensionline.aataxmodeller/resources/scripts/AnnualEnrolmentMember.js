var aeRequest = null;
var SLIDER_CONFIG = {
                'b_vertical' : false,
                'b_watch': true,
                'n_controlWidth': 320,
                'n_controlHeight': 16,
                'n_sliderWidth': 16,
                'n_sliderHeight': 15,
                'n_pathLeft' : 1,
                'n_pathTop' : 1,
                'n_pathLength' : 303,
                's_imgControl': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_bg.gif',
                's_imgSlider': '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sldr2h_sl.gif',
                'n_zIndex': 1
        }
var SLIDER_SALARY = {        
        's_name': 'salary_select',
        'n_minValue' : 0,
        'n_maxValue' : 450000,
        'n_value' : 22000,
        'n_step' : 1000
}

var salarySlider = null;
var accrualModelBallon = null;
var salaryModelBallon = null;
var taxInfoBalloon = null;
var pensionInfoBalloon = null;
var cashInfoBalloon = null;

var isAccrualModelled = false;
var isSalaryModelled = false;
var cashPercentModlled = 0;

var ACCRUAL_OPTION_VALUES = [60, 54, 51, 45, 40, 35];
var ACCRUAL_OPTION_LABELS = ['60ths', '54ths', '51sts', '45ths', '40ths', '35ths'];

var isTagDataLoaded = false;

var MemberAE = Object.extend(Class.create(), {});
MemberAE.prototype = {
	birthYear: null,
	schemeYear: null,
	yearAt65: null,	
	schemeHistories: null,
	currentOptions: {accrual: null, salary: null},
	modelOptions: {accrual: null, salary: null},	
	taxYear: {year: null, fiscalYear: null, amount: null},
	currentYear: null,
	pipYear: null,
	retirements: null,
	lta: null,
	
	initialize: function(options)
	{
		this.birthYear = parseInt(options.birthYear);
		this.schemeYear = parseInt(options.schemeYear);
		this.schemeHistories = new Array();
		for (var i=0; i<options.schemeHistories.length; i++)
		{
			var schemeHistory = {year:parseInt(options.schemeHistories[i][0]), accrual:options.schemeHistories[i][1]};
			this.schemeHistories.push(schemeHistory);
		}
		
		this.retirements = new Array();
		for (var i=0; i<options.retirements.length; i++)
		{
			var retirement = {year:parseInt(options.retirements[i][0]), pension: parseInt(options.retirements[i][1]), 				
				cash: parseInt(options.retirements[i][2]), pot: parseInt(options.retirements[i][3]),
				pension100: parseInt(options.retirements[i][4]), pension50: parseInt(options.retirements[i][5])};
			this.retirements.push(retirement);			
		}

		this.yearAt65 = this.retirements[this.retirements.length - 1].year;
		
		this.currentOptions.accrual = parseInt(options.currentOptions.accrual);
		this.currentOptions.salary = parseInt(options.currentOptions.salary);	
		
		this.modelOptions.accrual = parseInt(options.modelOptions.accrual);
		this.modelOptions.salary = parseInt(options.modelOptions.salary);			
		

		this.taxYear.year = parseInt(options.taxYear.year);
		this.taxYear.fiscalYear = options.taxYear.fiscalYear;
		this.taxYear.amount = parseInt(options.taxYear.amount);

		this.currentYear = parseInt(options.currentYear);
		this.pipYear = parseInt(options.pipYear);
		
		this.lta = parseInt(options.lta);
		
	}
};

// global member object
var member = null;
var retireYear = null;
var carType = null, carChoiceEnabled = false;

var YEAR_INTERVAL = 5;

var CANVAS_W = 750;
var CANVAS_H = 454;

var PADDING_LEFT = 20;

var CLOUD_W = 450;
var CLOUD_H = 214;
var ROAD_TOP_W = 534;
var ROAD_MID_W = 560;
var ROAD_BOT_W = 650;
var ROAD_H = 240;
var LANER_W = 600;
var LANER_H = 6;
var LANER_X = 40;
var LANER_Y = 362;

var RESET_BUTTON_W = 150;
var RESET_BUTTON_H = 60;
var RESET_BUTTON_Y = 0;

var FLOWER_W = 26;
var FLOWER_H = 34;

var COIN_W = 80;
var COIN_H = 75;
var COIN_OFFSET = 60;
var COIN_INFO_FLAG_W = 120;
var COIN_INFO_FLAG_H = 80;

var FENCE_W = 2;
var FENCE_H = 45;
var FENCE_OFFSET = 105;
var FLAG1_W = 72;
var FLAG1_H = 80;
var INFO_FLAG1_W = 220;
var INFO_FLAG1_H = 70;
var AGE_LABEL_W = 40;
var AGE_LABEL_H = 20;

var CAR_W = 140;
var CAR_H = 71;
var CAR_Y = 355;	// the tires
var CAR_FLAG_OFFSET = 70;
var FLAG2_W = 2;
var FLAG2_H = 30;
var INFO_FLAG2_W = 180;
var INFO_FLAG2_H = 80;

var PIPYEAR_MARKER_W = 40;
var PIPYEAR_MARKER_H = 37;

var ACCRUAL_FLAG_H = 25;
var ACCRUAL_FLAG_W = 45;

var SALARY_FLAG_W = 80;
var SALARY_FLAG_H = 25;


var POPUP1_W = 170;
var POPUP1_H = 50;

var WARDEN_OFFSET = 30;
var WARDEN_W = 43;
var WARDEN_H = 100;


var POPUP2_W = 350;
var POPUP2_H = 68;
var POPUP2_POINTER = 100;

var POPUP3_W = 256;
var POPUP3_H = 200;
var POPUP3_POINTER = 76;

var POPUP4_W = 250;
var POPUP4_H = 40;

var POPUP6_W = 110;
var POPUP6_H = 30;

var PLUS_BUTTON_DIV_W = 280;

var loaded = 0, fImgLoaded, drawJourney, drawPrintJourney, fLoadImg, Timer, cdePre;
var timer = null;

var CAR_SPEED = 300;

var CASH_FLAG_TEXT_ELEMENT_ID = 'cash_flag_text';
var PENSION_FLAG_TEXT_ELEMENT_ID = 'pension_flag_text';
var TAX_FLAG_TEXT_ELEMENT_ID = 'tax_flag_text';
var WARDEN_TEXT_ELEMENT_ID = 'warden_text';

var CASH_LUMP_SUM_TAG = 'CASH_LUMP_SUM';
var RETIRE_AGE_TAG = 'RETIRE_AGE';
var PENSION_TAG = 'PENSION';
var TAXYEAR_TAG = 'TAXYEAR';
var TAXABLE_AMOUNT_TAG = 'TAXABLE_AMOUNT';

var infoPopupCounter = 0;

// create a changable interval interface *grumble*
Timer = function(f, t){
	this.f = f;
	
	timer = window.setInterval(f, t);
};
Timer.prototype.changeTime = function (t){
	window.clearInterval(this.timer);
	timer = window.setInterval(this.f, t);
};
Timer.prototype.stop = function(){
	window.clearInterval(timer);
}


var cdeBody, cdeContent, journeyContainer, journeyCanvas, journeyContainerPrint, journeyCanvasPrint; // container
var cloud, road, laner, reset_button, car, flower, fences;
var flag1, infoFlag1, flag2, infoFlag2, coins, coinInfoFlag, warden, flag1Print, infoFlag1Print, coinsPrint, coinInfoFlagPrint;
var pipYearMarker, accrualFlag,  salaryFlag, modelledAccrualFlag, modelledSalaryFlag;
var popup1, popup1Close, popup2, popup2Close, popup3, popup3Close, popup4, popup5, popup6, popup6Close;
var accrualPlus, salaryPlus;

var objPos = {cloud: 0, laner: 0, road:0, car:0, lastCar:0}, carTimer, second, secondNow, frames;

var carStopX, carStopXPrint;

var carIE6Imgs = null;

// start animation
drawJourney = function(member){	
	if (member)
	{
		cdeBody = CDE.wrap('pagebody_road');		
		
		
		journeyContainer = CDE.div().id('journey_container').rel().wh(CANVAS_W + PADDING_LEFT, CANVAS_H).fl().m(0, 0, 180, 0);
		journeyCanvas = CDE.div().abs(0,0, CANVAS_W + PADDING_LEFT, CANVAS_H).bgc('#ffffff').oh().p(0, 0, 0, 0);
	
		// the road
		cloud = CDE.div().id('cloud').abs(PADDING_LEFT + (ROAD_BOT_W - CLOUD_W) / 2, 0 ,CLOUD_W, CLOUD_H)
			.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/cloud.png', 'repeat', 0, 0);
		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) {
			road = CDE.div().id('road').abs(PADDING_LEFT, CLOUD_H, ROAD_BOT_W, ROAD_H);
			road.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
						"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/road.png', sizingMethod='scale')");
		}
		else
		{
			road = CDE.div().id('road').abs(PADDING_LEFT, CLOUD_H, ROAD_BOT_W, ROAD_H)
				.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/road.png', 'repeat', 0, 0);					
		}
		laner = CDE.div().id('laner').abs(LANER_X + PADDING_LEFT, LANER_Y, LANER_W - 30, LANER_H)
			.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/laner.png', 'repeat-x', 0, 0);
				
		journeyCanvas.add(cloud);
		journeyCanvas.add(road);
		journeyCanvas.add(laner);
		
		// draw reset button
		reset_button = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/reset-button.png', 'Reset').abs(CANVAS_W - RESET_BUTTON_W, RESET_BUTTON_Y, RESET_BUTTON_W, RESET_BUTTON_H);
        reset_button.cp().on('click', function() {resetMemberAE();});	
        journeyCanvas.add(reset_button);	
        
		// the flowers
		var numFlowers = Math.floor((member.yearAt65 - member.schemeYear) / YEAR_INTERVAL) + 1;	
		for (var i=0; i<numFlowers; i++)
		{
			var flowerAge = member.yearAt65 - (i*YEAR_INTERVAL);
			var flowerX = ROAD_BOT_W *(flowerAge - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT - (FLOWER_W / 2) + 1;
			flower = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flower.gif', flowerAge +'').id('flower_' + i)
					.abs(flowerX, CLOUD_H + ROAD_H - FLOWER_H, FLOWER_W, FLOWER_H).z(6);
			journeyCanvas.add(flower);
		}				
	
		// the fence and the flag1			
		var numFence = member.retirements.length;		
		fences = new Array();
		for (var i=0; i<=10; i++)
		{
			var yearAtRetire = member.yearAt65  - i;
						
			var fenceH = (i%5 == 0) ? 15: 10;	
			var fenceX = ROAD_TOP_W *(yearAtRetire - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT + (ROAD_BOT_W - ROAD_TOP_W)/2 - FENCE_W;
			
			// only draw fence if it is on the road
			if (fenceX >= PADDING_LEFT + ROAD_BOT_W - ROAD_TOP_W)
			{		
				var fenceY = CLOUD_H + FENCE_OFFSET - fenceH;					
				var fence = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png', '').id('fence_' + i).abs(fenceX, fenceY, FENCE_W, fenceH);
				journeyCanvas.add(fence);
				
				// store only fences that member can retire (future years)
				if (i < numFence)
				{
					fences.push(fence);
				}
		
				if (yearAtRetire == retireYear)
				{
					flag1 = CDE.div().id('flag1').abs(fenceX - FLAG1_W + FENCE_W, CLOUD_H + FENCE_OFFSET - 4*FENCE_H - FLAG1_H, FLAG1_W, FLAG1_H).cp().z(1).tar();
					
					if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
					{
						var flag1Img = CDE.div().abs(0, 0, FLAG1_W, FLAG1_H).id('flag1Img');
						flag1Img.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
							"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flag.png', sizingMethod='scale')");
						flag1.add(flag1Img);
					}
					else
					{
						var flag1Img = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flag.png')
											.abs(0, 0, FLAG1_W, FLAG1_H).id('flag1Img');
						flag1.add(flag1Img);
					}				
					var flag1PoleImg = CDE.div().bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
						.abs(FLAG1_W - 2, FLAG1_H, 2, 4*FENCE_H);				
					flag1.add(flag1PoleImg);
					journeyCanvas.add(flag1);		
				}
				
				// draw retire age numbers
				if (i == 0)
				{
					// draw 65
					var age65Label = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65Label.fs(14).bold().c('#009900').txt('65');
					journeyCanvas.add(age65Label);
				}
				if (i == 5)
				{
					// draw 60
					var age65Label = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65Label.fs(14).bold().c('#009900').txt('60');
					journeyCanvas.add(age65Label);
				}
				if (i == 10)
				{
					// draw 55
					var age65Label = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65Label.fs(14).bold().c('#009900').txt('55');
					journeyCanvas.add(age65Label);
				}
			}
		}
		// draw coins and coin flag
		var coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins0.png';
		if (cashPercentModlled == 50)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins50.png';
		}
		else if (cashPercentModlled == 100)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins100.png';
		}
		var coinsX = CANVAS_W - COIN_INFO_FLAG_W /2 - COIN_W / 2 - 2 ;
		var coinsY = CLOUD_H + ROAD_H - COIN_OFFSET - COIN_H;
		coins = CDE.div().id('coins').abs(coinsX, coinsY, COIN_W, COIN_H).bg(coinImgSrc, 'repeat', 0, 0).cp();
		journeyCanvas.add(coins);
		
		// draw the pip year marker
		var accrualFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 2;
		var salaryFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear + 1) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 4;		
		var pipYearMarkerX = accrualFlagX + ( salaryFlagX - accrualFlagX) / 2 - PIPYEAR_MARKER_W / 2;
					
		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
		{
			pipYearMarker =	CDE.div().id('pipyear_marker')
				.abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H - 8).z(6);	
			pipYearMarker.p(8, 0, 0, 0).tac().fs(11).c('#ffffff').bold().txt('' + member.pipYear);
			pipYearMarker.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
				"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pipyear.png', sizingMethod='scale')");
			journeyCanvas.add(pipYearMarker);
		}
		else
		{
			pipYearMarker =	CDE.div().bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pipyear.png').id('pipyear_marker')
				.abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H - 8).z(6);	
			pipYearMarker.p(8, 0, 0, 0).tac().fs(11).c('#ffffff').bold().txt('' + member.pipYear);
			journeyCanvas.add(pipYearMarker);
		}
				
		
		// carStopX to current currentYear
		carStopX = ROAD_BOT_W * ((member.currentYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT;												
						
		// the car
		var carImgSrc = '';
		var carImgSrcIe6 = '';
		if (carType != null && carType.toLowerCase() == 'urban')
		{
			CAR_W= 120;
			CAR_H = 76;
			CAR_FLAG_OFFSET = 65;
			carImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/urban-cars.png';	
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/urban-cars_0';
		}
		else if (carType != null && carType.toLowerCase() == 'sport')
		{
			CAR_W= 140;
			CAR_H = 61;
			CAR_FLAG_OFFSET = 75;
			carImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sport-cars.png';
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sport-cars_0';
		}
		else
		{
			CAR_W= 140;
			CAR_H = 71;
			CAR_FLAG_OFFSET = 75;
			carImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/saloon-cars.png';	
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/saloon-cars_0';		
		}	

		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
		{	
			car = CDE.div().id('car').abs((ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT, CAR_Y - CAR_H, CAR_W, CAR_H).cp().z(4);
			carIE6Imgs = new Array();
			for(var i=0; i<5; i++) {
				var carIe6Img = CDE.div().rel().abs(0, 0, CAR_W, CAR_H).cp();
				carIe6Img.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + carImgSrcIe6 + i + ".png', sizingMethod='scale')").z(5-i);
				carIE6Imgs.push(carIe6Img);
				
				car.add(carIe6Img);
			}			
		}
		else
		{
			car = CDE.sprite(carImgSrc, 0, 0, CAR_W, CAR_H, CAR_W, CAR_H, 'car').id('car').abs((ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT, CAR_Y - CAR_H).cp().z(4);
		}
		
		journeyCanvas.add(car);
	
		// create stage with all actors
		cdeBody.add(journeyContainer.add(journeyCanvas));
		drawRoadBottom();
		
			
		var carTravel = carStopX - (ROAD_BOT_W - ROAD_MID_W) / 2 - PADDING_LEFT - CAR_FLAG_OFFSET;
		// only animinate if
		if (carTravel > ROAD_MID_W - CAR_W)
		{
			carTravel = ROAD_MID_W - CAR_W;
			carStopX = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + carTravel + CAR_FLAG_OFFSET;
		}
		// only animate if left edge of car image on the road
		if (carTravel > 0)
		{
			$j("#car").animate(
				{left: '+=' + carTravel}, 
				{
					duration: (carTravel * 1000 / CAR_SPEED), complete: function () {carStop();}
				});
		}
		else
		{
			carTravel = 0;
			carStopX = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + CAR_FLAG_OFFSET;
			carStop();
		}
	
		// start animation
		// at 60 fps, set the display according to the current pos
		carTimer = new Timer(function(){
			var n, m, t = CDE.now()*1000;
	
			// current second
			secondNow= ~~(t/1000)%10;
			if (secondNow != second) {
				// new second!
				second = secondNow;
				// display last score
				//cdeFps.txt(frames+" fps");
				// clear counter
				frames = 0;
			} else {
				// another frame this second!
				++frames;
			}
			
			// move cloude
			n = ~~((t % (30*CLOUD_W))/30);
			if (objPos.cloud != n) {
				objPos.cloud = n;
				cloud.bgpos(n, 0);
			}
			
			n = ~~((t % (6*LANER_W))/6);
			if (objPos.laner != n) {
				objPos.laner = n;
				laner.bgpos(n, 0);
			}			
	
			// change cars and move
			n = ~~((t % (5*80))/80);
			if (objPos.car != n) {
				objPos.car = n;
				if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
				{					
					carIE6Imgs[objPos.lastCar].z(1);
					carIE6Imgs[objPos.car].z(5);							
				}
				else
				{
					car.spriteTo(n*CAR_W, 0, CAR_W, CAR_H, CAR_W, CAR_H);
				}
				objPos.lastCar = n;
			}
			//alert(m);
		}, ~~(1000/60)); // 60 fps
	}		
}

drawPrintJourney = function(member)
{
	if (member)
	{
		var cdeBodyPrint = CDE.wrap('pagebody_road');	
		
		
		journeyContainerPrint = CDE.div().id('journey_container_print').rel().wh(CANVAS_W + PADDING_LEFT, CANVAS_H + 130).fl();
		journeyCanvasPrint = CDE.div().abs(0,0, CANVAS_W + PADDING_LEFT, CANVAS_H + 130).bgc('#ffffff').oh().p(0, 0, 0, 0);
	
		
		var cloudPrint = CDE.div().id('cloud_print').abs(PADDING_LEFT + (ROAD_BOT_W - CLOUD_W) / 2, 0 ,CLOUD_W, CLOUD_H);
		var cloudPrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/cloud.png', 'cloud').rel().abs(0, 0 ,CLOUD_W, CLOUD_H);
		cloudPrint.add(cloudPrintImg);
		
		// the road
		var roadPrint = CDE.div().id('road_print').abs(PADDING_LEFT, CLOUD_H, ROAD_BOT_W, ROAD_H);
		var roadPrintImg =	CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/road.png', 'road').rel().abs(0, 0 ,ROAD_BOT_W, ROAD_H);
		roadPrint.add(roadPrintImg);
		
		var lanerPrint = CDE.div().id('laner_print').abs(LANER_X + PADDING_LEFT, LANER_Y, LANER_W - 30, LANER_H);
		var lanerPrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/laner.png', 'laner')
			.rel().abs(0, 0, LANER_W - 30, LANER_H);
		lanerPrint.add(lanerPrintImg);
				
		journeyCanvasPrint.add(cloudPrint);
		journeyCanvasPrint.add(roadPrint);
		journeyCanvasPrint.add(lanerPrint);			
        
		// the flowers
		var numFlowers = Math.floor((member.yearAt65 - member.schemeYear) / YEAR_INTERVAL) + 1;	
		for (var i=0; i<numFlowers; i++)
		{
			var flowerAge = member.yearAt65 - (i*YEAR_INTERVAL);
			var flowerX = ROAD_BOT_W *(flowerAge - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT - (FLOWER_W / 2) + 1;
			var flowerPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flower.png', flowerAge +'')
				.abs(flowerX, CLOUD_H + ROAD_H - FLOWER_H, FLOWER_W, FLOWER_H).z(6);

			journeyCanvasPrint.add(flowerPrint);
		}				
	
		// the fence and the flag1			
		for (var i=0; i<=10; i++)
		{
			var yearAtRetire = member.yearAt65  - i;
						
			var fenceH = (i%5 == 0) ? 15: 10;	
			var fenceX = ROAD_TOP_W *(yearAtRetire - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT + 
				(ROAD_BOT_W - ROAD_TOP_W)/2 - FENCE_W;
			
			// only draw fence if it is on the road
			if (fenceX >= PADDING_LEFT + ROAD_BOT_W - ROAD_TOP_W)
			{		
				var fenceY = CLOUD_H + FENCE_OFFSET - fenceH;					
				var fence = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png', '').abs(fenceX, fenceY, FENCE_W, fenceH);
				journeyCanvasPrint.add(fence);				
		
				if (yearAtRetire == retireYear)
				{
					flag1Print = CDE.div().id('flag1_print')
						.abs(fenceX - FLAG1_W + FENCE_W, CLOUD_H + FENCE_OFFSET - 4*FENCE_H - FLAG1_H, FLAG1_W, FLAG1_H).cp().z(1).tar();
					
					var flag1PrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flag.png')
											.abs(0, 0, FLAG1_W, FLAG1_H).id('flag1Img_print');
					flag1Print.add(flag1PrintImg);				
					var flag1PrintPoleImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png', '')
						.abs(FLAG1_W - 2, FLAG1_H, 2, 4*FENCE_H);				
					flag1Print.add(flag1PrintPoleImg);
					journeyCanvasPrint.add(flag1Print);		
				}
				
				// draw retire age numbers
				if (i == 0)
				{
					// draw 65
					var age65LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65LabelPrint.fs(14).bold().c('#009900').txt('65');
					journeyCanvasPrint.add(age65LabelPrint);
				}
				if (i == 5)
				{
					// draw 60
					var age65LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65LabelPrint.fs(14).bold().c('#009900').txt('60');
					journeyCanvasPrint.add(age65LabelPrint);
				}
				if (i == 10)
				{
					// draw 55
					var age55LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age55LabelPrint.fs(14).bold().c('#009900').txt('55');
					journeyCanvasPrint.add(age55LabelPrint);
				}
			}
		}
		// draw coins and coin flag
		var coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins0.png';
		if (cashPercentModlled == 50)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins50.png';
		}
		else if (cashPercentModlled == 100)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins100.png';
		}
		var coinsX = CANVAS_W - COIN_INFO_FLAG_W /2 - COIN_W / 2 - 2 ;
		var coinsY = CLOUD_H + ROAD_H - COIN_OFFSET - COIN_H;
		coinsPrint = CDE.img(coinImgSrc, '').id('coins_print').abs(coinsX, coinsY, COIN_W, COIN_H);
		journeyCanvasPrint.add(coinsPrint);
					
		// draw accrual flag at current year
		var accrualFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 2;
		if (accrualFlagX - ACCRUAL_FLAG_W < 1)
		{
			accrualFlagX = ACCRUAL_FLAG_W + 1;
		}		
		var accrualPolePrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
			.abs(accrualFlagX, CLOUD_H + ROAD_H, FENCE_W,  FENCE_H);
		journeyCanvasPrint.add(accrualPolePrint);
		
		var accrualFlagPrint = CDE.div().id('accrual_flag_print')
			.abs(accrualFlagX - ACCRUAL_FLAG_W - 1, CLOUD_H + ROAD_H + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
			.bgc("#009900").sb('#006633', 1).bold().p(3, 3, 3, 3).o(0.3).c('#000').z(1);
		accrualFlagPrint.txt(getOrderNumber(member.currentOptions.accrual));
		journeyCanvasPrint.add(accrualFlagPrint);	

		// draw the modelled accrual
		if (isAccrualModelled)
		{	
			var modelledAccrualFlagPrint = CDE.div().id('modelled_accrual_flag_print')
				.abs(accrualFlagX + 1, CLOUD_H + ROAD_H + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
				.bgc('#009900').sb('#006633', 1).bold().p(3, 3, 3, 3).c('#ffffff').z(1);
			modelledAccrualFlagPrint.txt(getOrderNumber(member.modelOptions.accrual));
			journeyCanvasPrint.add(modelledAccrualFlagPrint);			
		}				
		// salary flag
		var salaryFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear + 1) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 4;
		if (salaryFlagX - SALARY_FLAG_W < 1)
		{
			salaryFlagX = SALARY_FLAG_W + 1;
		}
		var salaryPolePrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
			.abs(salaryFlagX, CLOUD_H + ROAD_H, FENCE_W, 2 * FENCE_H);
		journeyCanvasPrint.add(salaryPolePrint);	
		
		var salaryFlagPrint = CDE.div().id('salary_flag_print')
				.abs(salaryFlagX - SALARY_FLAG_W - 1, CLOUD_H + ROAD_H + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
				.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#000').o(0.3).cp().z(1);
		salaryFlagPrint.txt('&pound;' + formatToDecimal(Math.round(member.currentOptions.salary/100) * 100));
		journeyCanvasPrint.add(salaryFlagPrint);

		// modelled salary flag	
		if (isSalaryModelled)
		{			
			var modelledSalaryFlagPrint = CDE.div().id('modelled_salary_flag_print')
				.abs(salaryFlagX + 1, CLOUD_H + ROAD_H + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
				.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#ffffff').cp().z(1);
			modelledSalaryFlagPrint.txt('&pound;' + formatToDecimal(Math.round(member.modelOptions.salary/100) * 100));
			journeyCanvasPrint.add(modelledSalaryFlagPrint);										
		}		
		
		// draw the pip year marker
		var pipYearMarkerX = accrualFlagX + ( salaryFlagX - accrualFlagX) / 2 - PIPYEAR_MARKER_W / 2;
					
		var pipYearMarkerPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pipyear.png','')
				.abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H).z(6);	
		var pipYearMarkerPrintTxt = CDE.div().abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H - 8).z(6)
			.p(8, 0, 0, 0).tac().fs(11).c('#000').bold().txt('' + member.pipYear);
		journeyCanvasPrint.add(pipYearMarkerPrint);
		journeyCanvasPrint.add(pipYearMarkerPrintTxt);								
		
		// carStopX to current currentYear
		carStopXPrint = ROAD_BOT_W * ((member.currentYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT;												
		if (carStopXPrint < (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + CAR_FLAG_OFFSET)	
		{
			carStopXPrint = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + CAR_FLAG_OFFSET;
		}	
		else if (carStopXPrint > (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + ROAD_MID_W - CAR_W + CAR_FLAG_OFFSET)
		{
			carStopXPrint = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + ROAD_MID_W - CAR_W + CAR_FLAG_OFFSET;
		}
		// the car
		var carImgSrcIe6 = '';
		if (carType != null && carType.toLowerCase() == 'urban')
		{
			CAR_W= 120;
			CAR_H = 76;
			CAR_FLAG_OFFSET = 65;	
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/urban-cars_01.png';
		}
		else if (carType != null && carType.toLowerCase() == 'sport')
		{
			CAR_W= 140;
			CAR_H = 61;
			CAR_FLAG_OFFSET = 75;
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sport-cars_01.png';
		}
		else
		{
			CAR_W= 140;
			CAR_H = 71;
			CAR_FLAG_OFFSET = 75;
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/saloon-cars_01.png';		
		}	

		var carPrint = CDE.div().id('car_print').abs(carStopXPrint - CAR_FLAG_OFFSET, CAR_Y - CAR_H, CAR_W, CAR_H).z(4);
		var carIe6ImgPrint = CDE.img(carImgSrcIe6,'').rel().abs(0, 0, CAR_W, CAR_H);				
		carPrint.add(carIe6ImgPrint);
		journeyCanvasPrint.add(carPrint);
	
		// create stage with all actors
		cdeBodyPrint.add(journeyContainerPrint.add(journeyCanvasPrint));
					
	}
}

drawPrintJourneyIE6 = function(member)
{
	if (member)
	{
		
		var cdeBodyPrint = CDE.wrap('pagebody_road');	
		
		
		journeyContainerPrint = CDE.div().id('journey_container_print').rel().wh(CANVAS_W + PADDING_LEFT, CANVAS_H + 130).fl();
		journeyCanvasPrint = CDE.div().abs(0,0, CANVAS_W + PADDING_LEFT, CANVAS_H + 130).bgc('#ffffff').oh().p(0, 0, 0, 0);
	
		
		var cloudPrint = CDE.div().id('cloud_print').abs(PADDING_LEFT + (ROAD_BOT_W - CLOUD_W) / 2, 0 ,CLOUD_W, CLOUD_H);
		var cloudPrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/cloud.png', 'cloud').rel().abs(0, 0 ,CLOUD_W, CLOUD_H);
		cloudPrint.add(cloudPrintImg);
		
		// the road
		var roadPrint = CDE.div().id('road_print').abs(PADDING_LEFT, CLOUD_H, ROAD_BOT_W, ROAD_H);
		var roadPrintImg =	CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/road.png', 'road').rel().abs(0, 0 ,ROAD_BOT_W, ROAD_H);
		roadPrint.add(roadPrintImg);
		
		var lanerPrint = CDE.div().id('laner_print').abs(LANER_X + PADDING_LEFT, LANER_Y, LANER_W - 30, LANER_H);
		var lanerPrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/laner.png', 'laner')
			.rel().abs(0, 0, LANER_W - 30, LANER_H);
		lanerPrint.add(lanerPrintImg);
				
		journeyCanvasPrint.add(cloudPrint);
		journeyCanvasPrint.add(roadPrint);
		journeyCanvasPrint.add(lanerPrint);			
        
		// the flowers
		var numFlowers = Math.floor((member.yearAt65 - member.schemeYear) / YEAR_INTERVAL) + 1;	
		for (var i=0; i<numFlowers; i++)
		{
			var flowerAge = member.yearAt65 - (i*YEAR_INTERVAL);
			var flowerX = ROAD_BOT_W *(flowerAge - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT - (FLOWER_W / 2) + 1;
			var flowerPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flower.gif', flowerAge +'')
				.abs(flowerX, CLOUD_H + ROAD_H - FLOWER_H, FLOWER_W, FLOWER_H).z(6);

			journeyCanvasPrint.add(flowerPrint);
		}				
	
		// the fence and the flag1			
		for (var i=0; i<=10; i++)
		{
			var yearAtRetire = member.yearAt65  - i;
						
			var fenceH = (i%5 == 0) ? 15: 10;	
			var fenceX = ROAD_TOP_W *(yearAtRetire - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT + 
				(ROAD_BOT_W - ROAD_TOP_W)/2 - FENCE_W;
			
			// only draw fence if it is on the road
			if (fenceX >= PADDING_LEFT + ROAD_BOT_W - ROAD_TOP_W)
			{		
				var fenceY = CLOUD_H + FENCE_OFFSET - fenceH;					
				var fence = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png', '').abs(fenceX, fenceY, FENCE_W, fenceH);
				journeyCanvasPrint.add(fence);				
		
				if (yearAtRetire == retireYear)
				{
					flag1Print = CDE.div().id('flag1_print')
						.abs(fenceX - FLAG1_W + FENCE_W, CLOUD_H + FENCE_OFFSET - 4*FENCE_H - FLAG1_H, FLAG1_W, FLAG1_H).cp().z(1).tar();
					
					var flag1PrintImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/flag.gif')
											.abs(0, 0, FLAG1_W, FLAG1_H).id('flag1Img_print');
					flag1Print.add(flag1PrintImg);				
					var flag1PrintPoleImg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png', '')
						.abs(FLAG1_W - 2, FLAG1_H, 2, 4*FENCE_H);				
					flag1Print.add(flag1PrintPoleImg);
					journeyCanvasPrint.add(flag1Print);		
				}
				
				// draw retire age numbers
				if (i == 0)
				{
					// draw 65
					var age65LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65LabelPrint.fs(14).bold().c('#009900').txt('65');
					journeyCanvasPrint.add(age65LabelPrint);
				}
				if (i == 5)
				{
					// draw 60
					var age65LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age65LabelPrint.fs(14).bold().c('#009900').txt('60');
					journeyCanvasPrint.add(age65LabelPrint);
				}
				if (i == 10)
				{
					// draw 55
					var age55LabelPrint = CDE.div().abs(fenceX - AGE_LABEL_W / 2, fenceY - AGE_LABEL_H, AGE_LABEL_W, AGE_LABEL_H).z(1).tac();
					age55LabelPrint.fs(14).bold().c('#009900').txt('55');
					journeyCanvasPrint.add(age55LabelPrint);
				}
			}
		}
		// draw coins and coin flag
		var coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins0.png';
		if (cashPercentModlled == 50)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins50.png';
		}
		else if (cashPercentModlled == 100)
		{
			coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins100.png';
		}
		var coinsX = CANVAS_W - COIN_INFO_FLAG_W /2 - COIN_W / 2 - 2 ;
		var coinsY = CLOUD_H + ROAD_H - COIN_OFFSET - COIN_H;
		coinsPrint = CDE.img(coinImgSrc, '').id('coins_print').abs(coinsX, coinsY, COIN_W, COIN_H);
		journeyCanvasPrint.add(coinsPrint);
					
		// draw accrual flag at current year
		var accrualFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 2;
		if (accrualFlagX - ACCRUAL_FLAG_W < 1)
		{
			accrualFlagX = ACCRUAL_FLAG_W + 1;
		}		
		var accrualPolePrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
			.abs(accrualFlagX, CLOUD_H + ROAD_H, FENCE_W,  FENCE_H);
		journeyCanvasPrint.add(accrualPolePrint);
		
		var accrualFlagPrint = CDE.div().id('accrual_flag_print')
			.abs(accrualFlagX - ACCRUAL_FLAG_W - 1, CLOUD_H + ROAD_H + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
			.bgc("#009900").sb('#006633', 1).bold().p(3, 3, 3, 3).o(0.3).c('#000').z(1);
		accrualFlagPrint.txt(getOrderNumber(member.currentOptions.accrual));
		journeyCanvasPrint.add(accrualFlagPrint);	

		// draw the modelled accrual
		if (isAccrualModelled)
		{	
			var modelledAccrualFlagPrint = CDE.div().id('modelled_accrual_flag_print')
				.abs(accrualFlagX + 1, CLOUD_H + ROAD_H + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
				.bgc('#009900').sb('#006633', 1).bold().p(3, 3, 3, 3).c('#ffffff').z(1);
			modelledAccrualFlagPrint.txt(getOrderNumber(member.modelOptions.accrual));
			journeyCanvasPrint.add(modelledAccrualFlagPrint);			
		}				
		// salary flag
		var salaryFlagX = ROAD_BOT_W * ((member.pipYear - member.schemeYear + 1) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 4;
		if (salaryFlagX - SALARY_FLAG_W < 1)
		{
			salaryFlagX = SALARY_FLAG_W + 1;
		}
		var salaryPolePrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
			.abs(salaryFlagX, CLOUD_H + ROAD_H, FENCE_W, 2 * FENCE_H);
		journeyCanvasPrint.add(salaryPolePrint);	
		
		var salaryFlagPrint = CDE.div().id('salary_flag_print')
				.abs(salaryFlagX - SALARY_FLAG_W - 1, CLOUD_H + ROAD_H + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
				.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#000').o(0.3).cp().z(1);
		salaryFlagPrint.txt('&pound;' + formatToDecimal(Math.round(member.currentOptions.salary/100) * 100));
		journeyCanvasPrint.add(salaryFlagPrint);

		// modelled salary flag	
		if (isSalaryModelled)
		{			
			var modelledSalaryFlagPrint = CDE.div().id('modelled_salary_flag_print')
				.abs(salaryFlagX + 1, CLOUD_H + ROAD_H + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
				.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#ffffff').cp().z(1);
			modelledSalaryFlagPrint.txt('&pound;' + formatToDecimal(Math.round(member.modelOptions.salary/100) * 100));
			journeyCanvasPrint.add(modelledSalaryFlagPrint);										
		}		
		
		// draw the pip year marker
		var pipYearMarkerX = accrualFlagX + ( salaryFlagX - accrualFlagX) / 2 - PIPYEAR_MARKER_W / 2;
					
		var pipYearMarkerPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pipyear.gif','')
				.abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H).z(6);	
		var pipYearMarkerPrintTxt = CDE.div().abs(pipYearMarkerX, CLOUD_H + ROAD_H - PIPYEAR_MARKER_H, PIPYEAR_MARKER_W, PIPYEAR_MARKER_H - 8).z(6)
			.p(8, 0, 0, 0).tac().fs(11).c('#000').bold().txt('' + member.pipYear);
		journeyCanvasPrint.add(pipYearMarkerPrint);
		journeyCanvasPrint.add(pipYearMarkerPrintTxt);								
		
		// carStopX to current currentYear
		carStopXPrint = ROAD_BOT_W * ((member.currentYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT;												
		if (carStopXPrint < (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + CAR_FLAG_OFFSET)	
		{
			carStopXPrint = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + CAR_FLAG_OFFSET;
		}	
		else if (carStopXPrint > (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + ROAD_MID_W - CAR_W + CAR_FLAG_OFFSET)
		{
			carStopXPrint = (ROAD_BOT_W - ROAD_MID_W) / 2 + PADDING_LEFT + ROAD_MID_W - CAR_W + CAR_FLAG_OFFSET;
		}
		// the car
		var carImgSrcIe6 = '';
		if (carType != null && carType.toLowerCase() == 'urban')
		{
			CAR_W= 120;
			CAR_H = 76;
			CAR_FLAG_OFFSET = 65;	
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/urban-cars_01.gif';
		}
		else if (carType != null && carType.toLowerCase() == 'sport')
		{
			CAR_W= 140;
			CAR_H = 61;
			CAR_FLAG_OFFSET = 75;
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/sport-cars_01.gif';
		}
		else
		{
			CAR_W= 140;
			CAR_H = 71;
			CAR_FLAG_OFFSET = 75;
			carImgSrcIe6 = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/saloon-cars_01.gif';		
		}	

		var carPrint = CDE.div().id('car_print').abs(carStopXPrint - CAR_FLAG_OFFSET, CAR_Y - CAR_H, CAR_W, CAR_H).z(4);
		var carIe6ImgPrint = CDE.img(carImgSrcIe6,'').rel().abs(0, 0, CAR_W, CAR_H);				
		carPrint.add(carIe6ImgPrint);
		journeyCanvasPrint.add(carPrint);
	
		// create stage with all actors
		cdeBodyPrint.add(journeyContainerPrint.add(journeyCanvasPrint));
					
	}				
}

function carStop()
{		
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
	{					
		drawPrintJourneyIE6(member);							
	}	
	else
	{
		drawPrintJourney(member);
	}
	
	// draw coins and coin flag
	var coinsX = CANVAS_W - COIN_INFO_FLAG_W /2 - COIN_W / 2 - 2 ;
	var coinsY = CLOUD_H + ROAD_H - COIN_OFFSET - COIN_H;
	
	var coinPole = CDE.div().bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
		.abs(coinsX + COIN_W/2, coinsY - 2*FENCE_H, FENCE_W, 2*FENCE_H);
	journeyCanvas.add(coinPole);	
	
	// add pole to print
	var coinPolePrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
		.abs(coinsX + COIN_W/2, coinsY - 2*FENCE_H, FENCE_W, 2*FENCE_H);
	journeyCanvasPrint.add(coinPolePrint);
	
	// the cash lump sum flag
	var cashAtRetireAge = roundDownTo100(member.retirements[member.retirements.length - 1 - (member.yearAt65 - retireYear)].cash * cashPercentModlled / 100);
	coinInfoFlag = CDE.div().id('coin_info').abs(coinsX + COIN_W/2 - COIN_INFO_FLAG_W/2, coinsY - 2*FENCE_H - COIN_INFO_FLAG_H, COIN_INFO_FLAG_W - 10, COIN_INFO_FLAG_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666');
	var cashLumSumTxt = getTextFromElement(CASH_FLAG_TEXT_ELEMENT_ID);
	cashLumSumTxt = replaceTagWithValue(cashLumSumTxt, CASH_LUMP_SUM_TAG, formatToDecimal(cashAtRetireAge));
	coinInfoFlag.txt(cashLumSumTxt + '</b> (<a id="info_CashInfo" onclick="return false;" href="#">more...</a>)');
	journeyCanvas.add(coinInfoFlag);
	
	// add to print
	coinInfoFlagPrint = CDE.div().id('coin_info_print').abs(coinsX + COIN_W/2 - COIN_INFO_FLAG_W/2, coinsY - 2*FENCE_H - COIN_INFO_FLAG_H, COIN_INFO_FLAG_W - 10, COIN_INFO_FLAG_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666');
	coinInfoFlagPrint.txt(cashLumSumTxt);
	journeyCanvasPrint.add(coinInfoFlagPrint);	
	
	// draw retirement flag
	for (var i=0; i<member.retirements.length; i++)
	{
		var yearAtRetire = member.yearAt65  - i;
		var fenceX = ROAD_TOP_W *(yearAtRetire - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT + (ROAD_BOT_W - ROAD_TOP_W)/2 - FENCE_W;		
		var fenceY = CLOUD_H + FENCE_OFFSET - FENCE_H;				

		if (yearAtRetire == retireYear)
		{
			// the info flag1
			infoFlag1 = CDE.div().id('info1').abs(fenceX - FLAG1_W - INFO_FLAG1_W + 1, fenceY - 3*FENCE_H - FLAG1_H + 8, INFO_FLAG1_W - 10, INFO_FLAG1_H - 10).
				bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(3);
			var pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - i].pension);
			if (cashPercentModlled == 50)
			{
				pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - i].pension50);
			}
			else if (cashPercentModlled == 100)
			{
				pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - i].pension100);
			}			
			
			var pensionFlagTxt = getTextFromElement(PENSION_FLAG_TEXT_ELEMENT_ID);
			pensionFlagTxt = replaceTagWithValue(pensionFlagTxt, RETIRE_AGE_TAG, getOrderNumber(retireYear - member.birthYear));
			pensionFlagTxt = replaceTagWithValue(pensionFlagTxt, PENSION_TAG, formatToDecimal(pensionAtRetire));
			infoFlag1.txt(pensionFlagTxt +  '(<a id="info_PensionInfo" onclick="return false;" href="#">more...</a>)');
			journeyCanvas.add(infoFlag1);
			
			// add to print
			infoFlag1Print = CDE.div().id('info1_print').abs(fenceX - FLAG1_W - INFO_FLAG1_W + 1, fenceY - 3*FENCE_H - FLAG1_H + 8, INFO_FLAG1_W - 10, INFO_FLAG1_H - 10).
				bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(3);
			
			infoFlag1Print.txt(pensionFlagTxt);
			journeyCanvasPrint.add(infoFlag1Print);		
		}
	}	
	
	// draw tax flag
	var infoFlag2X = journeyCanvas.pos().x + carStopX  - INFO_FLAG2_W - 10;
	var infoFlag2Y = journeyCanvas.pos().y + CAR_Y - CAR_H - FLAG2_H - INFO_FLAG2_H - 2;
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
	{					
		infoFlag2Y += 10;							
	}
	infoFlag2 = CDE.div().id('info2')
		.abs(infoFlag2X, infoFlag2Y, INFO_FLAG2_W - 10, INFO_FLAG2_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(4);
		
	var taxFlagTxt = getTextFromElement(TAX_FLAG_TEXT_ELEMENT_ID);
	taxFlagTxt = replaceTagWithValue(taxFlagTxt, TAXYEAR_TAG, member.taxYear.fiscalYear);
	taxFlagTxt = replaceTagWithValue(taxFlagTxt, TAXABLE_AMOUNT_TAG, formatToDecimal(roundUpTo100(member.taxYear.amount)));
	
	infoFlag2.txt(taxFlagTxt + '(<a id="info_TaxInfo" onclick="return false;" href="#">more...</a>)');
	infoFlag2.dba();
	
	// add to print			
	var flag2Print = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png')
		.abs(carStopX - 10, CAR_Y - CAR_H - FLAG2_H, FLAG2_W, FLAG2_H).z(4);
	journeyCanvasPrint.add(flag2Print);
	
	var infoFlag2XPrint = carStopX  - INFO_FLAG2_W - 10;
	if (infoFlag2XPrint < 0) infoFlag2XPrint = 0;
	var infoFlag2YPrint = CAR_Y - CAR_H - FLAG2_H - INFO_FLAG2_H - 2;
	var infoFlag2Print = CDE.div().id('info2_print')
		.abs(infoFlag2XPrint , infoFlag2YPrint, INFO_FLAG2_W - 10, INFO_FLAG2_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(4);
	infoFlag2Print.txt(taxFlagTxt);
	journeyCanvasPrint.add(infoFlag2Print);
				
	flag2 = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png').id('flag2')
		.abs(carStopX - 10, CAR_Y - CAR_H - FLAG2_H, FLAG2_W, FLAG2_H).z(4);
	journeyCanvas.add(flag2);
	
	
						
	//carTimer.stop();
	//objPos.road = 0;	
	// 
	
	if(Prototype.Browser.MobileSafari)
	{
		initIPadFlagDragDrop();
		//initIPadSlider();
	}
	else
	{
		updateFlagDragable(member);
	}
		
	var retirementArrIndex = member.retirements.length - 1 - (member.yearAt65 - retireYear);	
	if (member.retirements[retirementArrIndex].pot > member.lta)
	{			
		// reset this variable
		warden = null;			
		popUpOverLTA();
	}	
	if (!Prototype.Browser.MobileSafari)
	{
		$j("#flag1").mouseover(function(){
			popUpFlagDradDrop();
		});
		
		$j("#flag1").mouseout(function(){
			removePopUpFlagDradDrop();
		});
	
		$j("#flag1").click(function(){	
			removePopUpFlagDradDrop();
		});	
	}		
	
	if (!Prototype.Browser.MobileSafari)
	{	
		$j("#coins").mouseover(function(){
			popUpCashChange();		
		});	
		$j("#coins").mouseout(function(){	
			removePopUpCashChange();			
		});
		$j("#coins").mouseout(function(){	
			removePopUpCashChange();
			modelCashChange();			
		});
	}
	else
	{
		$j("#coins").click(function(){	
			modelCashChange();			
		});	
	}	
	
	if (!Prototype.Browser.MobileSafari)
	{	
		initAccrualSalaryPopups();
	}
	
	// add pop up for car choices if car selected
	$j("#car").click(function () {
		if (carChoiceEnabled)
		{
			popUpCarChoices();
		}
	});

	// re-create tax info balloon
	if (taxInfoBalloon)
	{
		$j("#tax_info_balloon").remove();
		taxInfoBalloon = null;
	}
	initTaxInfoPopups();

	
	// re-create pension info balloon
	if (pensionInfoBalloon)
	{
		$j("#pension_info_balloon").remove();
		pensionInfoBalloon = null;
	}
	initPensionInfoPopups();	
	
	// re-create cash info balloon
	if (cashInfoBalloon)
	{
		$j("#cash_info_balloon").remove();
		cashInfoBalloon = null;
	}
	initCashInfoPopups();
	
	// Only load once but after the balloons are created
	if (!isTagDataLoaded)
	{				
		// get tag data
		ParseHtml(); 
		callTagHandle();	
		isTagDataLoaded = true;	
	}
}

function popUpFlagDradDrop()
{
	if (document.getElementById('popup1') != null) return;
	
	var popupX = flag1.pos().x + FLAG1_W;
	var popupY = flag1.pos().y - POPUP1_H;
	// draw the pop up for dragable feature
	popup1 = CDE.div().id('popup1').abs(popupX, popupY, POPUP1_W - 10, POPUP1_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(7);
	popup1.txt("Drag &amp; drop the flag to change retirement age");	
		
	popup1.dba();
}

function removePopUpFlagDradDrop()
{
	if (document.getElementById('popup1') == null) return;
	
	$j('#popup1').remove();
}

function popUpCashChange()
{
	if (document.getElementById('popup6') != null) return;
	
	if (document.getElementById('cash_modelling_balloon') && 
		document.getElementById('cash_modelling_balloon').className == 'aa_popup_show') return;
	
	var popupX = coins.pos().x + COIN_W/2 - POPUP6_W/2;
	var popupY = coins.pos().y + COIN_H + 2;
	// draw the pop up for dragable feature
	popup6 = CDE.div().id('popup6').abs(popupX, popupY, POPUP6_W - 10, POPUP1_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(7);
	popup6.txt("Click here to change option");	
		
	popup6.dba();			
}

function removePopUpCashChange()
{
	if (document.getElementById('popup6') == null) return;
	
	$j('#popup6').remove();
}

function updateFlagDragable(member){
	var dragX1 = fences[fences.length-1].pos().x - FLAG1_W;
	var dragY1 = fences[fences.length-1].pos().y - FLAG1_H;
	var dragX2 = fences[0].pos().x + FENCE_W - FLAG1_W;
	var dragY2 = fences[0].pos().y + FENCE_H - FLAG1_H;
	var pensionFlagTxt = getTextFromElement(PENSION_FLAG_TEXT_ELEMENT_ID);
	var cashLumSumTxt = getTextFromElement(CASH_FLAG_TEXT_ELEMENT_ID);
	
	$j( "#flag1" ).draggable({
		cursor:'pointer',
		containment: [dragX1, dragY1, dragX2, dragY2],
		axis: 'x',
		start: function ()
		{
			removePopUpFlagDradDrop();
			// recreate info flags again
		 	recreateInfoFlags();
		},
		drag: function ()
		{				
			// check colisions with the fences
	        var flagOffset = $j(this).offset();
	       		
			// get the pole coordinates
			var poleX = flagOffset.left + FLAG1_W - 2;
			var poleY = flagOffset.top + FLAG1_H;
	
			// find the nearest fence
			var minFenceDis = 1000;
			var nearestFenceIndex = 0;
			for (var i=0; i<fences.length; i++)
			{
				var fenceOffset = $j('#fence_' + i).offset();
				var fenceX = fenceOffset.left;
				var fenceY = fenceOffset.top;
				var yearAtRetire = member.yearAt65; 
				var fenceDis = Math.abs(fenceX - poleX);
				if (fenceDis < minFenceDis)
				{
					minFenceDis = fenceDis;
					nearestFenceIndex = i;
				}
			}
	
			var yearAtRetire = member.yearAt65 - nearestFenceIndex;
			var pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension);
			if (cashPercentModlled == 50)
			{
				pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension50);
			}
			else if (cashPercentModlled == 100)
			{
				pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension100);
			}
			
			var pensionFlagTxtUpdate = replaceTagWithValue(pensionFlagTxt, RETIRE_AGE_TAG, getOrderNumber(yearAtRetire - member.birthYear));
			pensionFlagTxtUpdate = replaceTagWithValue(pensionFlagTxtUpdate, PENSION_TAG, formatToDecimal(pensionAtRetire));
			$j("#info1").html(pensionFlagTxtUpdate +  '(<a id="info_PensionInfo" onclick="return false;" href="#">more...</a>)');			

			var infoFlagY = flag1.qpos().y  + 8;
			var infoFlagX = flag1.qpos().x - INFO_FLAG1_W;
			
			$j("#info1").css("top", infoFlagY + "px");
			$j("#info1").css("left",  infoFlagX + "px");
			
			
			var cashAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].cash * cashPercentModlled / 100);			
			var cashLumSumTxtUpdate = replaceTagWithValue(cashLumSumTxt, CASH_LUMP_SUM_TAG, formatToDecimal(cashAtRetire));		
			$j("#coin_info").html(cashLumSumTxtUpdate +'(<a href="#">more...</a>)');			
		},
		stop: function ()
		{
			// check colisions with the fences
            var flagOffset = $j(this).offset();
           		
			// get the pole coordinates
			var poleX = flagOffset.left + FLAG1_W - 2;
			var poleY = flagOffset.top + FLAG1_H;

			// find the nearest fence
			var minFenceDis = 1000;
			var nearestFenceIndex = 0;
			for (var i=0; i<fences.length; i++)
			{
				var fenceOffset = $j('#fence_' + i).offset();
				var fenceX = fenceOffset.left;
				var fenceY = fenceOffset.top; 
				var fenceDis = Math.abs(fenceX - poleX);
				if (fenceDis < minFenceDis)
				{
					minFenceDis = fenceDis;
					nearestFenceIndex = i;
				}
			}

			retireYear = member.yearAt65 - nearestFenceIndex;
					
			var infoFlagY = CLOUD_H + FENCE_OFFSET - 4*FENCE_H - FLAG1_H + 8;
			var infoFlagX = fences[nearestFenceIndex].qpos().x - FLAG1_W - INFO_FLAG1_W + (FENCE_W / 2);
			var flagX = (fences[nearestFenceIndex].qpos().x - FLAG1_W + (FENCE_W / 2) + 1);			
			
			$j("#flag1").css("top", infoFlagY - 8 + "px");
			$j("#flag1").css("left", flagX + "px");					
		 		
		 	if (member.retirements[member.retirements.length - 1 - nearestFenceIndex].pot > member.lta)
		 	{
		 		popUpOverLTA();
		 	}	
		 	else
		 	{		 		
		 		removePopUpOverLTA();
		 	}
		 	
		 	//update flag on print panel
		 	$j("#flag1_print").css("top", infoFlagY - 8 + "px");
			$j("#flag1_print").css("left", flagX + "px");
		 	
		 	// recreate info flags again
		 	recreateInfoFlags();
		 	
		 	// re-create pension info balloon
			if (pensionInfoBalloon)
			{
				$j("#pension_info_balloon").remove();
				pensionInfoBalloon = null;
			}
			initPensionInfoPopups();
			
			// re-create cash info balloon
			if (cashInfoBalloon)
			{
				$j("#cash_info_balloon").remove();
				cashInfoBalloon = null;
			}
			initCashInfoPopups();	
															
		}
	});
}

function popUpOverLTA()
{	
	var wardenX = PADDING_LEFT + ROAD_BOT_W  - (ROAD_BOT_W - ROAD_TOP_W)/2;
	var wardenY = CLOUD_H + ROAD_H - WARDEN_OFFSET - WARDEN_H;
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
	{					
		warden = CDE.div().id('warden').abs(wardenX, wardenY, WARDEN_W, WARDEN_H).z(5);
		warden.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
					"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/warden.png', sizingMethod='scale')");						
	}
	else
	{
		warden = CDE.div().id('warden').abs(wardenX, wardenY, WARDEN_W, WARDEN_H)
			.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/warden.png', 'repeat', 0, 0).z(5);
	}
	journeyCanvas.add(warden);
	
	var popupX = wardenX - POPUP2_W + 10;
	var popupY = wardenY + 28;	
	// draw the pop up for dragable feature
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
	{					
		popup2 = CDE.div().id('popup2').abs(popupX, popupY, POPUP2_W - 80, POPUP2_H - 25).p(25, 60, 0, 20);
		popup2.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
					"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup2.png', sizingMethod='scale')");						
	}
	else
	{
		popup2 = CDE.div().id('popup2').abs(popupX, popupY, POPUP2_W - 80, POPUP2_H - 25).p(25, 60, 0, 20)
			.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup2.png', 'repeat', 0, 0);
	}				
	popup2.txt(getTextFromElement(WARDEN_TEXT_ELEMENT_ID)).z(5);		
	journeyCanvas.add(popup2);
	
	// add to print
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
	{					
		var wardenPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/warden.gif', '')
			.id('warden_print').abs(wardenX, wardenY, WARDEN_W, WARDEN_H).z(5);
		var popup2PrintBg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup2.gif', '').id('popup2_print_bg')
			.abs(popupX, popupY, POPUP2_W, POPUP2_H).z(5);
		var popup2PrintTxt = CDE.div().id('popup2_print_txt').abs(popupX, popupY, POPUP2_W - 80, POPUP2_H - 25).p(25, 60, 0, 20);
		popup2PrintTxt.txt(getTextFromElement(WARDEN_TEXT_ELEMENT_ID)).z(5);
		
		journeyCanvasPrint.add(wardenPrint);
		journeyCanvasPrint.add(popup2PrintBg);
		journeyCanvasPrint.add(popup2PrintTxt);						
	}
	else
	{	
		var wardenPrint = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/warden.png', '')
			.id('warden_print').abs(wardenX, wardenY, WARDEN_W, WARDEN_H).z(5);
		var popup2PrintBg = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup2.png', '').id('popup2_print_bg')
			.abs(popupX, popupY, POPUP2_W, POPUP2_H).z(5);
		var popup2PrintTxt = CDE.div().id('popup2_print_txt').abs(popupX, popupY, POPUP2_W - 80, POPUP2_H - 25).p(25, 60, 0, 20);
		popup2PrintTxt.txt(getTextFromElement(WARDEN_TEXT_ELEMENT_ID)).z(5);
		
		journeyCanvasPrint.add(wardenPrint);
		journeyCanvasPrint.add(popup2PrintBg);
		journeyCanvasPrint.add(popup2PrintTxt);
	}
}

function removePopUpOverLTA()
{
	$j("#warden").remove();
	$j('#popup2').remove();
	
	$j("#warden_print").remove();
	$j('#popup2_print_bg').remove();
	$j('#popup2_print_txt').remove();
}

function popUpCarChoices(popX, popY)
{	
	if (car != null && popup3 == null)
	{			
		var popupX = car.pos().x + CAR_W/2 - POPUP3_POINTER;
		var popupY = car.pos().y - POPUP3_H;	
		
		// draw the pop up for dragable feature
		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
		{		
			popup3 = CDE.div().id('popup3').abs(popupX, popupY, POPUP3_W, POPUP3_H).p(0, 0, 0, 0).z(6);
			var popup3Bg = CDE.div().abs(0, 0, POPUP3_W, POPUP3_H);
			popup3Bg.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
						"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup3.png', sizingMethod='scale')");
										
			popup3Close = CDE.div().id('popup3Close').abs(popupX + POPUP3_W - 35, popupY + 15, 19, 19);
			popup3Close.css('filter', "progid:DXImageTransform.Microsoft.AlphaImageLoader(" +
						"src='/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png', sizingMethod='scale')");
			var popup3Inner = CDE.div().abs(0, 0, POPUP3_W - 20, POPUP3_H - 30).p(30, 0, 0, 20);
			popup3Inner.txt(document.getElementById('pagebody_carchoices').innerHTML);
			popup3.add(popup3Bg);
			popup3.add(popup3Inner);
		}
		else
		{
			popup3 = CDE.div().id('popup3').abs(popupX, popupY, POPUP3_W - 20, POPUP3_H - 30).p(30, 0, 0, 20)
				.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/popup3.png', 'repeat', 0, 0);				
			popup3Close = CDE.div().id('popup3Close').abs(popupX + POPUP3_W - 35, popupY + 15, 19, 19)
				.bg('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png', 'repeat', 0, 0);
			popup3.txt(document.getElementById('pagebody_carchoices').innerHTML).z(6);
		}	
			
		
		popup3Close.cp().on('click', function() {removePopUpCarChoices();}).z(6);
		
		$j('#pagebody_carchoices').remove();
		popup3.dba();
		popup3Close.dba();			
	}	
	
	if (popup3)
	{
		$j('#popup3').show();
		$j('#popup3Close').show();
	}				
}

function initAccrualSalaryPopups()
{
	
	$j("#accrual_flag").mouseover(function(){	
			// check colisions with the fences
        var flagOffset = $j(this).offset();
       		
		// get the pole coordinates
		var popupX = flagOffset.left - POPUP4_W/2 +  ACCRUAL_FLAG_W / 2;
		var popupY = flagOffset.top + ACCRUAL_FLAG_H + 5;				
		var popup4 = CDE.div().id('popup4').abs(popupX, popupY, POPUP4_W - 10, POPUP4_H - 10)
			.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(7);
							
		var mes = 'In ' + member.currentYear + ' your accrual rate is ' + formatToDecimal(member.currentOptions.accrual);
		popup4.txt(mes);
		
		popup4.dba();			
	});
		
	$j("#accrual_flag").mouseout(function()
	{
		$j("#popup4").remove();
	});		
	
	$j("#salary_flag").mouseover(function(){	
        var flagOffset = $j(this).offset();
       		
		// get the pole coordinates
		var popupX = flagOffset.left - POPUP4_W/2 +  ACCRUAL_FLAG_W / 2;
		var popupY = flagOffset.top + ACCRUAL_FLAG_H + 5;				
		var popup5 = CDE.div().id('popup5').abs(popupX, popupY, POPUP4_W - 10, POPUP4_H - 10)
			.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(7);
							
		var mes = 'In ' + member.currentYear + ' your salary is &pound;' + formatToDecimal(Math.round(member.currentOptions.salary/100) * 100);
		popup5.txt(mes);
		
		popup5.dba();			
	});
		
	$j("#salary_flag").mouseout(function()
	{
		$j("#popup5").remove();
	});	
}

function removePopUpCarChoices()
{
	$j('#popup3').hide();
	$j('#popup3Close').hide(); 
}
function initCarChoice(selectedCar)
{
	var carChoiceEls = document.getElementsByName('carchoice');
	for(var i=0; i<carChoiceEls.length; i++) {
		var carChoiceEl = carChoiceEls[i];
		if (selectedCar != null && carChoiceEl.value.toLowerCase() == selectedCar.toLowerCase())
		{
			carChoiceEl.checked = true;
			
			return;
		}
	}
	
	// select saloon car
	for(var i=0; i<carChoiceEls.length; i++) {
		var carChoiceEl = carChoiceEls[i];
		if (carChoiceEl.value.toLowerCase() == 'saloon')
		{
			carChoiceEl.checked = true;
			
			return;
		}
	}
}

function updateCarChoice()
{
	var carChoiceEls = document.getElementsByName('carchoice');
	for(var i=0; i<carChoiceEls.length; i++) {
		var carChoiceEl = carChoiceEls[i];
		if (carChoiceEl.checked)
		{
			carType = carChoiceEl.value;
		}
	}	
		
	clearRoad();
	drawJourney(member);
}

function recreateInfoFlags()
{
	var retireIndex = member.yearAt65 - retireYear;
	var pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - retireIndex].pension);
	var cashAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - retireIndex].cash * cashPercentModlled / 100);	
	
	// update coins	
	var coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins0.png';
		
	if (cashPercentModlled == 50)
	{
		coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins50.png';
		pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - retireIndex].pension50);
	}
	else if (cashPercentModlled == 100)
	{
		coinImgSrc = '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/coins100.png';
		pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - retireIndex].pension100);
	}
	coins.bg(coinImgSrc);
	
	// printing
	coinsPrint.atr('src', coinImgSrc);
	
	// recreate cash lump sum flag
	coinInfoFlag.del();
	var coinsX = CANVAS_W - COIN_INFO_FLAG_W /2 - COIN_W / 2 - 2 ;
	var coinsY = CLOUD_H + ROAD_H - COIN_OFFSET - COIN_H;						
						
	var cashLumSumTxt = getTextFromElement(CASH_FLAG_TEXT_ELEMENT_ID);		
	cashLumSumTxt = replaceTagWithValue(cashLumSumTxt, CASH_LUMP_SUM_TAG, formatToDecimal(cashAtRetire));	
	coinInfoFlag = CDE.div().id('coin_info').abs(coinsX + COIN_W/2 - COIN_INFO_FLAG_W/2, coinsY - 2*FENCE_H - COIN_INFO_FLAG_H, COIN_INFO_FLAG_W - 10, COIN_INFO_FLAG_H - 10)
		.bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666');
	coinInfoFlag.txt(cashLumSumTxt + '(<a id="info_CashInfo" onclick="return false;" href="#">more...</a>)');
	journeyCanvas.add(coinInfoFlag);	
	
	// printing
	coinInfoFlagPrint.txt(cashLumSumTxt);
	
	// recreate retirement flag
	infoFlag1.del();
	
	for (var i=0; i<member.retirements.length; i++)
	{
		var yearAtRetire = member.yearAt65  - i;
		var fenceX = ROAD_TOP_W *(yearAtRetire - member.schemeYear) / (member.yearAt65 - member.schemeYear) + PADDING_LEFT + (ROAD_BOT_W - ROAD_TOP_W)/2 - FENCE_W;		
		var fenceY = CLOUD_H + FENCE_OFFSET - FENCE_H;				

		if (yearAtRetire == retireYear)
		{
			// the info flag1
			infoFlag1 = CDE.div().id('info1').abs(fenceX - FLAG1_W - INFO_FLAG1_W + 1, fenceY - 3*FENCE_H - FLAG1_H + 8, INFO_FLAG1_W - 10, INFO_FLAG1_H - 10).
				bgc('#ffffcc').sb('#ffcccc', 1).p(5, 5, 5, 5).c('#666666').z(3);
			pensionFlagTxt = getTextFromElement(PENSION_FLAG_TEXT_ELEMENT_ID);
			pensionFlagTxt = replaceTagWithValue(pensionFlagTxt, RETIRE_AGE_TAG, getOrderNumber(retireYear - member.birthYear));
			pensionFlagTxt = replaceTagWithValue(pensionFlagTxt, PENSION_TAG, formatToDecimal(pensionAtRetire));			
			infoFlag1.txt(pensionFlagTxt + '(<a id="info_PensionInfo" onclick="return false;" href="#">more...</a>)');
			journeyCanvas.add(infoFlag1);	
			
			// printing	
			infoFlag1Print.x(fenceX - FLAG1_W - INFO_FLAG1_W + 1);
			infoFlag1Print.txt(pensionFlagTxt);
				
			break;
		}
	}				
}

function resizeInfoPopUp(popup, expandedId)
{
	if (document.getElementById(expandedId))
	{
		// get the height of expanded area
		var expandedHeight =$j("#"	 + expandedId).height() + 10; // Add a height of an empty paragaraph

		// re-create tax info balloon
		if (popup == 'popup_TaxInfo')
		{
			if (taxInfoBalloon)
			{
				taxInfoBalloon.updateContentHeight(expandedHeight);		
			}
		}
		else if (popup == 'popup_CashInfo')
		{
			// resize cash info balloon
			if (cashInfoBalloon)
			{
				cashInfoBalloon.updateContentHeight(expandedHeight);
			}
		}
		else if (popup == 'popup_PensionInfo')
		{
			// resize pension info balloon
			if (pensionInfoBalloon)
			{
				pensionInfoBalloon.updateContentHeight(expandedHeight);
			}	
		}	
	}	
}

function drawRoadBottom()
{
	// draw accrual flag at current year
	var accrualFlagX = journeyCanvas.pos().x + ROAD_BOT_W * ((member.pipYear - member.schemeYear) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 2;
	var accrualFlagY = journeyCanvas.pos().y + CLOUD_H + ROAD_H;
	if (!accrualFlag)
	{				
		var accrualPole = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png').id('accrual_flag_pole')
			.abs(accrualFlagX, accrualFlagY, FENCE_W,  FENCE_H).z(-1);
		accrualPole.dba();
		accrualFlag = CDE.div().id('accrual_flag')
			.abs(accrualFlagX - ACCRUAL_FLAG_W - 1, accrualFlagY + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
			.bgc("#009900").sb('#006633', 1).bold().p(3, 3, 3, 3).o(0.3).c('#000').cp().z(1);
		accrualFlag.txt(getOrderNumber(member.currentOptions.accrual));
		accrualFlag.dba();	
	}

	// draw the modelled accrual
	if (isAccrualModelled && !modelledAccrualFlag)
	{	
		var modelledAccrualFlagX = accrualFlagX + 1;
		if (member.pipYear == member.currentYear)
		{
			 modelledAccrualFlagX = accrualFlagX + ROAD_BOT_W * 1 / (member.yearAt65 - member.schemeYear) - 1;
			var modelledAccrualPole = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png').id('modelled_accrual_flag_pole')
				.abs(modelledAccrualFlagX, accrualFlagY, FENCE_W,  FENCE_H).z(-1);
			modelledAccrualPole.dba();			 
		}
		modelledAccrualFlag = CDE.div().id('modelled_accrual_flag')
			.abs(modelledAccrualFlagX + 1, accrualFlagY + FENCE_H - ACCRUAL_FLAG_H - 2, ACCRUAL_FLAG_W - 6, ACCRUAL_FLAG_H - 6)
			.bgc('#009900').sb('#006633', 1).bold().p(3, 3, 3, 3).c('#ffffff').z(1);
		modelledAccrualFlag.txt(getOrderNumber(member.modelOptions.accrual));
		modelledAccrualFlag.dba();			
	}				
	// salary flag
	var salaryFlagX = journeyCanvas.pos().x + ROAD_BOT_W * ((member.pipYear - member.schemeYear + 1) /(member.yearAt65 - member.schemeYear)) + PADDING_LEFT - 4;
	var salaryFlagY = journeyCanvas.pos().y + CLOUD_H + ROAD_H;
	if (!salaryFlag)
	{
		if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) 
		{					
			salaryFlagY -= 10;							
		}
		var salaryPole = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/fence.png').id('salary_flag_pole')
			.abs(salaryFlagX, salaryFlagY, FENCE_W, 2 * FENCE_H).z(-1);
		salaryPole.dba();	
		
		salaryFlag = CDE.div().id('salary_flag')
				.abs(salaryFlagX - SALARY_FLAG_W - 1, salaryFlagY + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
				.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#000').o(0.3).cp().z(1);
		salaryFlag.txt('&pound;' + formatToDecimal(Math.round(member.currentOptions.salary/100) * 100));
		salaryFlag.dba();
	}

	// modelled salary flag	
	if (isSalaryModelled && !modelledSalaryFlag)
	{			
		modelledSalaryFlag = CDE.div().id('modelled_salary_flag')
			.abs(salaryFlagX + 1, salaryFlagY + 2 * FENCE_H - SALARY_FLAG_H - 2, SALARY_FLAG_W - 6, SALARY_FLAG_H - 6)
			.bgc('#FF6600').sb('#000000', 1).p(3, 3, 3, 3).bold().c('#ffffff').cp().z(1);
		modelledSalaryFlag.txt('&pound;' + formatToDecimal(Math.round(member.modelOptions.salary/100) * 100));
		modelledSalaryFlag.dba();										
	}
		
	if (accrualPlus != null)
	{
		// add plus button next to the accrual flag
		if (isAccrualModelled && modelledAccrualFlag)
		{
			// add next to modelled flag
			var accrualPlusX = modelledAccrualFlag.pos().x  + ACCRUAL_FLAG_W + 10;
			accrualPlus.x(accrualPlusX);
		}
		else if (!isAccrualModelled && salaryFlag)
		{
			// add next to salary flag to avoid colission with salary flag
			var accrualPlusX = salaryFlag.pos().x  + SALARY_FLAG_W + 10;
			accrualPlus.x(accrualPlusX);	
		}
		
		//accrualPlus.db();
	}
	else
	{
		// add plus button next to the accrual flag
		var accrualPlusTxtContent = getTextFromElement('plus_accrual_text');
		if (isAccrualModelled && modelledAccrualFlag)
		{
			// add next to modelled flag
			var accrualPlusX = modelledAccrualFlag.pos().x  + ACCRUAL_FLAG_W + 10;
			var accrualPlusY = modelledAccrualFlag.pos().y;	
			
			accrualPlus = CDE.div().id('accrual_plus').abs(accrualPlusX, accrualPlusY, PLUS_BUTTON_DIV_W, ACCRUAL_FLAG_H).vam();
			var accrualPlusBtn = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png', 
				'Click to add an accrual rate change').wh(25, 25).id('accrual_plus_img').fl();
			var accrualPlusTxt = CDE.div().fl().p(5,0,0, 5).txt(accrualPlusTxtContent);
			accrualPlus.add(accrualPlusBtn);
			accrualPlus.add(accrualPlusTxt);
			accrualPlus.dba();
		}
		else if (!isAccrualModelled && salaryFlag && accrualFlag)
		{
			// add next to the accrual flag
			// add next to modelled flag
			var accrualPlusX = salaryFlag.pos().x  + SALARY_FLAG_W + 10;
			var accrualPlusY = accrualFlag.pos().y;	
			
			accrualPlus = CDE.div().id('accrual_plus').abs(accrualPlusX, accrualPlusY, PLUS_BUTTON_DIV_W, ACCRUAL_FLAG_H).vam();
			var accrualPlusBtn = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png', 
				'Click to add an accrual rate change').wh(25, 25).id('accrual_plus_img').fl();
			var accrualPlusTxt = CDE.div().fl().p(5,0,0, 5).txt(accrualPlusTxtContent);		
			accrualPlus.add(accrualPlusBtn);
			accrualPlus.add(accrualPlusTxt);
			accrualPlus.dba();	
		}
	}
	
	if (salaryPlus != null)
	{
		// add plus button next to the accrual flag
		if (isSalaryModelled && modelledSalaryFlag)
		{
			// add next to modelled flag
			var salaryPlusX = modelledSalaryFlag.pos().x  + SALARY_FLAG_W + 10;
			salaryPlus.x(salaryPlusX);
		}
		else if (!isSalaryModelled && salaryFlag)
		{
			// add next to modelled flag
			var salaryPlusX = salaryFlag.pos().x  + SALARY_FLAG_W + 10;
			salaryPlus.x(salaryPlusX);	
		}
		//salaryPlus.db();
	}
	else
	{
		var salaryPlusTxtContent = getTextFromElement('plus_salary_text');
		
		// add plus button next to the accrual flag
		if (isSalaryModelled && modelledSalaryFlag)
		{
			// add next to modelled flag
			var salaryPlusX = modelledSalaryFlag.pos().x  + SALARY_FLAG_W + 10;
			var salaryPlusY = modelledSalaryFlag.pos().y;	
			
			salaryPlus = CDE.div().id('salary_plus').abs(salaryPlusX, salaryPlusY, PLUS_BUTTON_DIV_W, SALARY_FLAG_H).vam();
			var salaryPlusBtn = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png', 
				'Click to add a salary change').wh(25, 25).id('salary_plus_img').fl();
			var salaryPlusTxt = CDE.div().fl().p(5,0,0, 5).txt(salaryPlusTxtContent);
			salaryPlus.add(salaryPlusBtn);
			salaryPlus.add(salaryPlusTxt);
			salaryPlus.dba();
		}
		else if (!isSalaryModelled && salaryFlag)
		{
			// add next to modelled flag
			var salaryPlusX = salaryFlag.pos().x  + SALARY_FLAG_W + 10;
			var salaryPlusY = salaryFlag.pos().y;	
			
			salaryPlus = CDE.div().id('salary_plus').abs(salaryPlusX, salaryPlusY, PLUS_BUTTON_DIV_W, SALARY_FLAG_H).vam();
			var salaryPlusBtn = CDE.img('/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/add-icon.png', 
				'Click to add a salary change').wh(25, 25).id('salary_plus_img').fl();
			var salaryPlusTxt = CDE.div().fl().p(5,0,0, 5).txt(salaryPlusTxtContent);
			salaryPlus.add(salaryPlusBtn);
			salaryPlus.add(salaryPlusTxt);
			salaryPlus.dba();	
		}
	}	
}

function showPleaseWait()
{		
	var overlay = document.getElementById('canvas_overlay');
    if (overlay == null)
    {
    	//alert('add overlay');
    	var overlayX = cdeBody.pos().x;
    	var overlayY = cdeBody.pos().y;
    	overlay = CDE.div().id('canvas_overlay').abs(overlayX,overlayY, CANVAS_W + 2*PADDING_LEFT, CANVAS_H).bgc('#fff').o(0.5).p(0, 0, 0, 0).tac().z(99).c('#000');
    	overlay.txt("<p><b>Please wait...</b></p>").db().dba();
    }
}

function clearRoad()
{	
	removePopUpCarChoices();
	removePopUpOverLTA();
	
	journeyContainer.del(true, true);
	journeyContainerPrint.del(true, true);
	$j("#info2").remove();
}

function clearCanvas()
{
	//$j("#canvas_overlay").remove();
	removePopUpCarChoices();
	removePopUpOverLTA();
	
	journeyContainer.del(true, true);
	journeyContainerPrint.del(true, true);
	
	$j("#info2").remove();	
	$j("#accrual_flag").remove();
	$j("#salary_flag").remove();
	$j("#accrual_flag_pole").remove();	
	$j("#salary_flag_pole").remove();
	$j("#modelled_accrual_flag_pole").remove();
	$j("#modelled_accrual_flag").remove();
	$j("#modelled_salary_flag").remove();
	accrualFlag = null;
	salaryFlag = null;
	modelledAccrualFlag = null;
	modelledSalaryFlag = null;
	
//	accrualPlus.dn();
//	salaryPlus.dn();
}

function loadMemberAE()
{		
	//loadPeusedoMember();
	updateMemberData();			
}

function updateMemberData()
{	
//	ParseHtml();
	var updateMemberDataURL = ajaxurl + "/UpdateMemberData";
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", updateMemberDataURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleUpdateMemberDataResponse;		     
	
	aeRequest.send(null);			
}

function handleUpdateMemberDataResponse ()
{
	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			var response = createResponseXML(aeRequest.responseText);
			//alert(aeRequest.responseText);					
			try{                                    
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					alert(errorMsg);
					return false;
				}
				else
				{
					// load member
					var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService";
					aeRequest = createXmlHttpRequestObject();
					aeRequest.open("POST", loadMemeberAEURL, true);
				
					aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				    aeRequest.setRequestHeader("Content-length", 0);
				    aeRequest.setRequestHeader("Connection", "close");	
				    
					aeRequest.onreadystatechange = handleLoadMemberAEResponse;		     
					
					aeRequest.send(null);							
				}	
			}
			catch(err){
				alert("Update member tax data error." + err);	
			}					
		}				
	}	
}

function handleLoadMemberAEResponse ()
{
	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			//alert(aeRequest.responseText);
			var response = createResponseXML(aeRequest.responseText);
			
			try{                               
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				}
				else
				{										
					var birthYearResponse = response.getElementsByTagName('BirthYear')[0].firstChild.nodeValue;					
					var schemeYearResponse = response.getElementsByTagName('SchemeYear')[0].firstChild.nodeValue;
					var ltaResponse = response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var currentYearResponse = response.getElementsByTagName('CurrentYear')[0].firstChild.nodeValue;
					var pipYearResponse = response.getElementsByTagName('PipYear')[0].firstChild.nodeValue;					
					
					var schemeHistoriesResponse = new Array();
					var schemeHistoriesResponseNode = response.getElementsByTagName('SchemeHistories')[0];
					var schemeHistoryResponseNodeList = schemeHistoriesResponseNode.getElementsByTagName('SchemeHistory');
					for(var i=0; i<schemeHistoryResponseNodeList.length; i++) {
						var schemeHistoryResponseNode = schemeHistoryResponseNodeList[i];
						var year = schemeHistoryResponseNode.getElementsByTagName('Year')[0].firstChild.nodeValue;
						var accrual = schemeHistoryResponseNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue;
						
						schemeHistoriesResponse.push([year, accrual]);
					}
					
					var retirementsResponse = new Array();
					var retirementsResponseNode = response.getElementsByTagName('Retirements')[0];
					var retirementResponseNodeList = retirementsResponseNode.getElementsByTagName('Retirement');
					for(var i=0; i<retirementResponseNodeList.length; i++) {
						var retirementResponseNode = retirementResponseNodeList[i];
						var year = retirementResponseNode.getElementsByTagName('Year')[0].firstChild.nodeValue;
						var pension = retirementResponseNode.getElementsByTagName('Pension')[0].firstChild.nodeValue;
						var cash = retirementResponseNode.getElementsByTagName('SchemeCash')[0].firstChild.nodeValue;
						var pot = retirementResponseNode.getElementsByTagName('PensionPot')[0].firstChild.nodeValue;
						var pension100 = retirementResponseNode.getElementsByTagName('PensionWithMaxCash')[0].firstChild.nodeValue;
						var pension50 = retirementResponseNode.getElementsByTagName('PensionWithHaftCash')[0].firstChild.nodeValue;
						
						retirementsResponse.push([year, pension, cash, pot, pension100, pension50]);
					}		
					
					// current option					
					var currentOptionNode = response.getElementsByTagName('CurrentOptions')[0];
					var minSalaryResponse = 1000 * Math.round(parseInt(currentOptionNode.getElementsByTagName('MinSalary')[0].firstChild.nodeValue)/1000 - 0.5);
					var maxSalaryResponse = 1000 * Math.round(parseInt(currentOptionNode.getElementsByTagName('MaxSalary')[0].firstChild.nodeValue)/1000 - 0.5);
					var carChoiceResponse = currentOptionNode.getElementsByTagName('CarChoice')[0].firstChild.nodeValue;
					var currentOptionResponse = 
					{
						accrual: currentOptionNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue,
						salary: currentOptionNode.getElementsByTagName('Salary')[0].firstChild.nodeValue
					};
					
					// model option
					var modelOptionNode = response.getElementsByTagName('ModelOptions')[0];
					var modelOptionResponse = 
					{
						accrual: modelOptionNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue,
						salary: modelOptionNode.getElementsByTagName('Salary')[0].firstChild.nodeValue
					};					
					
					// current tax year
					var taxYearNode = response.getElementsByTagName('TaxYear')[0];
					var taxYearResponse = 
					{
						year: taxYearNode.getElementsByTagName('Year')[0].firstChild.nodeValue,
						fiscalYear: taxYearNode.getElementsByTagName('FiscalYear')[0].firstChild.nodeValue,
						amount: taxYearNode.getElementsByTagName('Amount')[0].firstChild.nodeValue
					};		
					
					carType = carChoiceResponse;
					if (carType != null && carType.toLowerCase() == 'variable')
					{
						carChoiceEnabled = true;	
					}
					
					member = new MemberAE({
						birthYear: birthYearResponse,
						schemeYear: schemeYearResponse,
						schemeHistories: schemeHistoriesResponse,
						retirements: retirementsResponse,
						currentOptions: currentOptionResponse,
						modelOptions: modelOptionResponse,						
						taxYear: taxYearResponse,
						currentYear: currentYearResponse,
						pipYear: pipYearResponse,
						lta: ltaResponse
					}); 
					
					retireYear = member.yearAt65;
													
					drawJourney(member);
					
					if (document.getElementById('accrual_next_tax_year'))
					{
						document.getElementById('accrual_next_tax_year').innerHTML = member.taxYear.fiscalYear;
					}
					if (document.getElementById('salary_next_tax_year'))
					{
						document.getElementById('salary_next_tax_year').innerHTML = member.taxYear.fiscalYear;
					}										

					initModellingPopups();												
					initAccrualList(member.currentOptions.accrual);
					initSalarySlider(minSalaryResponse, maxSalaryResponse, member.currentOptions.salary);
																	
				}					
			}
			catch(err){
				alert("Load/Modelling member data error." + err);	
			}					
		}				
	}	
}

function resetMemberAE()
{
	hideModellingPopups();
			
	isAccrualModelled = false;
	isSalaryModelled = false;
	cashPercentModlled = 0;
	carType = null;
	document.getElementById('accrual_select').value = member.currentOptions.accrual;
	if (salarySlider != null)
	{
		salarySlider.f_setValue(member.currentOptions.salary);
	}	
		
	// reset the retire year of member before member is reset
	retireYear = member.yearAt65;
	member = null;	
	
	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService";	
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModelMemberAEResponse;		     
	aeRequest.send(null);		
}


function modelAccrualChange()
{		
	var accrual = document.getElementById('accrual_select').value;
	var salary = '';
	if (isSalaryModelled)
	{
		salary = document.getElementById('salary_select').value;
	}
	
	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService?accrual=" + accrual + "&salary=" + salary;	
	
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModelMemberAEResponse;		     
	
	aeRequest.send(null);

	hideModellingPopups();
	isAccrualModelled = true;
	//showPleaseWait();
	
	// testing
//	clearCanvas();
//	member.modelOptions.accrual = accrual;
//	drawJourney(member);

	
}

function modelSalaryChange()
{		
	var salary = document.getElementById('salary_select').value;	
	var accrual = '';
	if (isAccrualModelled)
	{
		accrual = document.getElementById('accrual_select').value;
	}
	
	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService?accrual=" + accrual + "&salary=" + salary;		
	
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModelMemberAEResponse;		     
	
	aeRequest.send(null);
	
	hideModellingPopups();
	isSalaryModelled = true;		
	//showPleaseWait();
	
	// testing
//	clearCanvas();
//	member.modelOptions.salary = salary;
//	drawJourney(member);	
	
}

function modelCashChange()
{		
	cashPercentModlled += 50;
	if (cashPercentModlled > 100) cashPercentModlled = 0;
	
	recreateInfoFlags();
	
	// recreate popups
	// re-create cash info balloon
	if (cashInfoBalloon)
	{
		$j("#cash_info_balloon").remove();
		cashInfoBalloon = null;
	}
	initCashInfoPopups();
		
	// re-create pension info balloon
	if (pensionInfoBalloon)
	{
		$j("#pension_info_balloon").remove();
		pensionInfoBalloon = null;
	}
	initPensionInfoPopups();	
}


function handleModelMemberAEResponse ()
{
	if (aeRequest.readyState == 4) {							
	
		// Check that a successful server response was received
		if (aeRequest.status == 200) {
			//alert(aeRequest.responseText);
			var response = createResponseXML(aeRequest.responseText);
			
			try{        
				clearCanvas();
			                            
				var error = response.getElementsByTagName("Error").length;
				if(error > 0){
					var errorMsg = response.getElementsByTagName("Error")[0].firstChild.nodeValue;
//					$j("#pagebody_button").empty();
//					$j("#pagebody_road").empty();
					$j("#pagebody_text").html(errorMsg);
					alert(errorMsg);
					
					return false;
				}
				else
				{										
					var birthYearResponse = response.getElementsByTagName('BirthYear')[0].firstChild.nodeValue;					
					var schemeYearResponse = response.getElementsByTagName('SchemeYear')[0].firstChild.nodeValue;
					var ltaResponse = response.getElementsByTagName('LTA')[0].firstChild.nodeValue;
					var currentYearResponse = response.getElementsByTagName('CurrentYear')[0].firstChild.nodeValue;
					var pipYearResponse = response.getElementsByTagName('PipYear')[0].firstChild.nodeValue;					
					
					var schemeHistoriesResponse = new Array();
					var schemeHistoriesResponseNode = response.getElementsByTagName('SchemeHistories')[0];
					var schemeHistoryResponseNodeList = schemeHistoriesResponseNode.getElementsByTagName('SchemeHistory');
					for(var i=0; i<schemeHistoryResponseNodeList.length; i++) {
						var schemeHistoryResponseNode = schemeHistoryResponseNodeList[i];
						var year = schemeHistoryResponseNode.getElementsByTagName('Year')[0].firstChild.nodeValue;
						var accrual = schemeHistoryResponseNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue;
						
						schemeHistoriesResponse.push([year, accrual]);
					}
					
					var retirementsResponse = new Array();
					var retirementsResponseNode = response.getElementsByTagName('Retirements')[0];
					var retirementResponseNodeList = retirementsResponseNode.getElementsByTagName('Retirement');
					for(var i=0; i<retirementResponseNodeList.length; i++) {
						var retirementResponseNode = retirementResponseNodeList[i];
						var year = retirementResponseNode.getElementsByTagName('Year')[0].firstChild.nodeValue;
						var pension = retirementResponseNode.getElementsByTagName('Pension')[0].firstChild.nodeValue;
						var cash = retirementResponseNode.getElementsByTagName('SchemeCash')[0].firstChild.nodeValue;
						var pot = retirementResponseNode.getElementsByTagName('PensionPot')[0].firstChild.nodeValue;
						var pension100 = retirementResponseNode.getElementsByTagName('PensionWithMaxCash')[0].firstChild.nodeValue;
						var pension50 = retirementResponseNode.getElementsByTagName('PensionWithHaftCash')[0].firstChild.nodeValue;
						
						retirementsResponse.push([year, pension, cash, pot, pension100, pension50]);
					}		
					
					// current option					
					var currentOptionNode = response.getElementsByTagName('CurrentOptions')[0];
					var minSalaryResponse = 1000 * Math.round(parseInt(currentOptionNode.getElementsByTagName('MinSalary')[0].firstChild.nodeValue)/1000 - 0.5);
					var maxSalaryResponse = 1000 * Math.round(parseInt(currentOptionNode.getElementsByTagName('MaxSalary')[0].firstChild.nodeValue)/1000 - 0.5);
					var carChoiceResponse = currentOptionNode.getElementsByTagName('CarChoice')[0].firstChild.nodeValue;
					var currentOptionResponse = 
					{
						accrual: currentOptionNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue,
						salary: currentOptionNode.getElementsByTagName('Salary')[0].firstChild.nodeValue
					};
					
					// model option
					var modelOptionNode = response.getElementsByTagName('ModelOptions')[0];
					var modelOptionResponse = 
					{
						accrual: modelOptionNode.getElementsByTagName('Accrual')[0].firstChild.nodeValue,
						salary: modelOptionNode.getElementsByTagName('Salary')[0].firstChild.nodeValue
					};					
					
					// current tax year
					var taxYearNode = response.getElementsByTagName('TaxYear')[0];
					var taxYearResponse = 
					{
						year: taxYearNode.getElementsByTagName('Year')[0].firstChild.nodeValue,
						fiscalYear: taxYearNode.getElementsByTagName('FiscalYear')[0].firstChild.nodeValue,
						amount: taxYearNode.getElementsByTagName('Amount')[0].firstChild.nodeValue
					};												
					
					if (carType == null)
					{
						carType = carChoiceResponse;						
					}
					
					member = new MemberAE({
						birthYear: birthYearResponse,
						schemeYear: schemeYearResponse,
						schemeHistories: schemeHistoriesResponse,
						retirements: retirementsResponse,
						currentOptions: currentOptionResponse,
						modelOptions: modelOptionResponse,						
						taxYear: taxYearResponse,
						currentYear: currentYearResponse,
						pipYear: pipYearResponse,
						lta: ltaResponse
					}); 
					
													
					drawJourney(member);
					
					if (document.getElementById('accrual_next_tax_year'))
					{
						document.getElementById('accrual_next_tax_year').innerHTML = member.taxYear.fiscalYear;
					}
					if (document.getElementById('salary_next_tax_year'))
					{
						document.getElementById('salary_next_tax_year').innerHTML = member.taxYear.fiscalYear;
					}
				}	
			}
			catch(err){
				alert("Load/Modelling member data error." + err);	
			}					
		}				
	}	
}


function removeModelledAccrual()
{
	var salary = '';
	if (isSalaryModelled)
	{
		salary = document.getElementById('salary_select').value;
	}

	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService?salary=" + salary + '&cash=' + cashPercentModlled;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModelMemberAEResponse;		     
	
	aeRequest.send(null);
	
	isAccrualModelled = false;
	
	showPleaseWait();
	
	document.getElementById('accrual_select').value = member.currentOptions.accrual;
}

function removeModelledSalary()
{
	var accrual = '';
	if (isAccrualModelled)
	{
		accrual = document.getElementById('accrual_select').value;
	}
	
	var loadMemeberAEURL = ajaxurl + "/AnnualEnrolmentService?accrual=" + accrual+ '&cash=' + cashPercentModlled;
	aeRequest = createXmlHttpRequestObject();
	aeRequest.open("POST", loadMemeberAEURL, true);

	aeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    aeRequest.setRequestHeader("Content-length", 0);
    aeRequest.setRequestHeader("Connection", "close");	
    
	aeRequest.onreadystatechange = handleModelMemberAEResponse;		     
	
	aeRequest.send(null);
	
	isSalaryModelled = false;
	
	showPleaseWait();
	
	if (salarySlider != null)
	{
		salarySlider.f_setValue(member.currentOptions.salary);
	}
}

function initModellingPopups()
{
	var accrualPlusImg = document.getElementById('accrual_plus_img');
	if(accrualPlusImg) 
	{
		accrualModelBallon = new HelpBalloon({
			id: 'accrual_modelling_balloon',
			returnElement: true,
			title: '',
			contentId: 'popup_AccrualChange',
			icon:accrualPlusImg,
			iconStyle: {						
				'verticalAlign': 'middle'
			},
			balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
			pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
			button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png'
		});	
		
		// remove the content after clone to balloon
		$j('#popup_AccrualChange').remove()		
	}		
	
	var salaryPlusImg = document.getElementById('salary_plus_img');
	if(salaryPlusImg) 
	{
		salaryModelBallon = new HelpBalloon({
			id: 'salary_modelling_balloon',
			returnElement: true,
			title: '',
			contentId: 'popup_SalaryChange',
			icon:salaryPlusImg,
			iconStyle: {						
				'verticalAlign': 'middle'
			},
			balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
			pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
			button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png'		
		});	
		
		// remove the content after clone to balloon
		$j('#popup_SalaryChange').remove()		
	}	
}

function hideModellingPopups()
{
	if (accrualModelBallon) accrualModelBallon.hide();
	if (salaryModelBallon) salaryModelBallon.hide();		
}

function initTaxInfoPopups()
{
	var taxInfoHref = document.getElementById('info_TaxInfo')
	if (taxInfoHref) 
	{			
		taxInfoBalloon = new HelpBalloon({
			id: 'tax_info_balloon',
			returnElement: true,
			title: '',
			contentId: 'popup_TaxInfo',
			icon:$('info_TaxInfo'),
			iconStyle: {						
				'verticalAlign': 'middle'
			},
			balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
			pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
			button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
			autoUpdateIds: false
		});	
		// trick
		taxInfoBalloon.show();
		taxInfoBalloon.hide();										
	}							
}

function initPensionInfoPopups()
{
	var pensionInfoHref = document.getElementById('info_PensionInfo')
	if (pensionInfoHref) 
	{			
		pensionInfoBalloon = new HelpBalloon({
			id: 'pension_info_balloon',
			returnElement: true,
			title: '',
			contentId: 'popup_PensionInfo',
			icon:$('info_PensionInfo'),
			iconStyle: {						
				'verticalAlign': 'middle'
			},
			balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
			pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
			button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
			autoUpdateIds: false
		});		
		// trick
		pensionInfoBalloon.show();
		pensionInfoBalloon.hide();			
	}							
}

function initCashInfoPopups()
{	
	var cashInfoHref = document.getElementById('info_CashInfo')
	if (cashInfoHref) 
	{					
		cashInfoBalloon = new HelpBalloon({
			id: 'cash_info_balloon',
			returnElement: true,
			title: '',
			contentId: 'popup_CashInfo',
			icon:$('info_CashInfo'),
			iconStyle: {						
				'verticalAlign': 'middle'
			},
			balloonPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/balloon-',
			pointerPrefix: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/pointer-',
			button: '/content/pl/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/button.png',
			balloonFixedX: 415,
			autoUpdateIds: false
		});	
		// trick
		cashInfoBalloon.show();
		cashInfoBalloon.hide();								
	}							
}

function initAccrualList(memberAccrual)
{
//	var accrualIndexFound = 0;
//	for(var i=0; i<ACCRUAL_OPTION_VALUES.length; i++) {
//		if (ACCRUAL_OPTION_VALUES[i] == memberAccrual)
//		{
//			accrualIndexFound = i;
//			break;
//		}
//	}
//	var limitAccrualValues = new Array();
//	var limitAccrualLabels = new Array();
//	if (accrualIndexFound == 0)
//	{
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound]);
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound + 1]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound + 1]);
//	}
//	else if (accrualIndexFound == ACCRUAL_OPTION_VALUES.length - 1)
//	{
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound - 1]);
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound - 1]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound]);		
//	}
//	else
//	{
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound - 1]);
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound]);
//		limitAccrualValues.push(ACCRUAL_OPTION_VALUES[accrualIndexFound + 1]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound - 1]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound]);
//		limitAccrualLabels.push(ACCRUAL_OPTION_LABELS[accrualIndexFound + 1]);											
//	}
//	
//	var accrualSelectEl = document.getElementById('accrual_select');
//	while (accrualSelectEl.firstChild)
//	{
//		accrualSelectEl.removeChild(accrualSelectEl.firstChild);
//	}
//	
//	limit the accrual list to 1 increment
//	for(var i=0; i<limitAccrualValues.length; i++) {
//		var optionEl = document.createElement('option');
//		optionEl.setAttribute('value', limitAccrualValues[i]);
//		optionEl.innerHTML = limitAccrualLabels[i];	
//		if (limitAccrualValues[i]== memberAccrual)
//		{			
//			optionEl.setAttribute('selected', 'selected');
//		}
//		accrualSelectEl.appendChild(optionEl);
//	}

	var accrualSelectEl = document.getElementById('accrual_select');

	if (!accrualSelectEl) return;
	
	var accrualExist = false;
	for(var i=0; i<ACCRUAL_OPTION_VALUES.length; i++) {
		if (ACCRUAL_OPTION_VALUES[i] == memberAccrual)
		{
			accrualExist = true;
			break;
		}
	}
	
	if (!accrualExist)
	{
		var optionEl = document.createElement('option');
		optionEl.setAttribute('value', memberAccrual);
		
		if (memberAccrual == 0)
		{
			optionEl.innerHTML = 'N/A';
			accrualSelectEl.appendChild(optionEl);
		}
		else
		{
			optionEl.innerHTML = getOrderNumber(memberAccrual);
			accrualSelectEl.insertBefore(optionEl, accrualSelectEl.firstChild);
		}
				
		
	}
	accrualSelectEl.value = memberAccrual;
}

function initSalarySlider(minSalary, maxSalary, currentSalary)
{
	SLIDER_SALARY.n_minValue = roundDownTo1000(minSalary);
	SLIDER_SALARY.n_maxValue = roundUpTo1000(maxSalary);
	SLIDER_SALARY.n_value = currentSalary;
	if (salarySlider == null)
	{
		salarySlider = new slider('salary_slider', SLIDER_SALARY, SLIDER_CONFIG);
	}	
}

function getTextFromElement(elementId)
{
	var ele = document.getElementById(elementId);
	if (ele)
	{
		for(var i=0; i<ele.childNodes.length; i++) {
			var child = ele.childNodes[i];
			if (child.nodeType == 1)
			{
				return child.innerHTML;
			}
		}
		
		return ele.innerHTML;
	}
	
	return '&nbsp;';
}

function replaceTagWithValue(text, tag, value)
{
	if (text != null)
	{
		while (text.indexOf(tag) > -1)
		{
		 	text = text.replace('{' + tag + '}', value);
		}
	}
	
	return text;
}


// IPAD touch handlers	

var pensionFlagTxtIPad = '';
var cashLumSumTxtIPad = '';
 	
function initIPadFlagDragDrop() {
	pensionFlagTxtIPad = getTextFromElement(PENSION_FLAG_TEXT_ELEMENT_ID);
	cashLumSumTxtIPad = getTextFromElement(CASH_FLAG_TEXT_ELEMENT_ID);	
		
	var dragX1 = fences[fences.length-1].pos().x - FLAG1_W - journeyCanvas.pos().x;
	var dragX2 = fences[0].pos().x + FENCE_W - FLAG1_W - journeyCanvas.pos().x;
	var flagIPadDragable = new webkit_draggable('flag1', {onStart: ipadFlagDragStart, onMove: ipadFlagDragMove, onEnd: ipadFlagDragEnd, x1: dragX1, x2: dragX2});
}


function ipadFlagDragStart()
{	
	if (accrualModelBalon) accrualModelBallon.hide();
	if (salaryModelBallon) salaryModelBallon.hide();
	if (taxInfoBalloon) taxInfoBalloon.hide();
	if (pensionInfoBalloon) pensionInfoBalloon.hide();
	if (cashInfoBalloon) cashInfoBalloon.hide();	
}

function ipadFlagDragMove()
{
	// check colisions with the fences
    var flagOffset = $j('#flag1').offset();
   		
	// get the pole coordinates
	var poleX = flagOffset.left + FLAG1_W - 2;
	var poleY = flagOffset.top + FLAG1_H;

	// find the nearest fence
	var minFenceDis = 1000;
	var nearestFenceIndex = 0;
	for (var i=0; i<fences.length; i++)
	{
		var fenceOffset = $j('#fence_' + i).offset();
		var fenceX = fenceOffset.left;
		var fenceY = fenceOffset.top;
		var yearAtRetire = member.yearAt65; 
		var fenceDis = Math.abs(fenceX - poleX);
		if (fenceDis < minFenceDis)
		{
			minFenceDis = fenceDis;
			nearestFenceIndex = i;
		}
	}

	var yearAtRetire = member.yearAt65 - nearestFenceIndex;
	var pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension);
	if (cashPercentModlled == 50)
	{
		pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension50);
	}
	else if (cashPercentModlled == 100)
	{
		pensionAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].pension100);
	}
	
	var pensionFlagTxtUpdate = replaceTagWithValue(pensionFlagTxtIPad, RETIRE_AGE_TAG, getOrderNumber(yearAtRetire - member.birthYear));
	pensionFlagTxtUpdate = replaceTagWithValue(pensionFlagTxtUpdate, PENSION_TAG, formatToDecimal(pensionAtRetire));
	$j("#info1").html(pensionFlagTxtUpdate +  '(<a id="info_PensionInfo" onclick="return false;" href="#">more...</a>)');			

	var infoFlagY = flag1.qpos().y  + 8;
	var infoFlagX = flag1.qpos().x - INFO_FLAG1_W;
	
	$j("#info1").css("top", infoFlagY + "px");
	$j("#info1").css("left",  infoFlagX + "px");
	
	
	var cashAtRetire = roundDownTo100(member.retirements[member.retirements.length - 1 - nearestFenceIndex].cash * cashPercentModlled / 100);			
	var cashLumSumTxtUpdate = replaceTagWithValue(cashLumSumTxtIPad, CASH_LUMP_SUM_TAG, formatToDecimal(cashAtRetire));		
	$j("#coin_info").html(cashLumSumTxtUpdate +'(<a href="#">more...</a>)');
}

function ipadFlagDragEnd()
{
	// check colisions with the fences
    var flagOffset = $j('#flag1').offset();
   		
	// get the pole coordinates
	var poleX = flagOffset.left + FLAG1_W - 2;
	var poleY = flagOffset.top + FLAG1_H;

	// find the nearest fence
	var minFenceDis = 1000;
	var nearestFenceIndex = 0;
	for (var i=0; i<fences.length; i++)
	{
		var fenceOffset = $j('#fence_' + i).offset();
		var fenceX = fenceOffset.left;
		var fenceY = fenceOffset.top; 
		var fenceDis = Math.abs(fenceX - poleX);
		if (fenceDis < minFenceDis)
		{
			minFenceDis = fenceDis;
			nearestFenceIndex = i;
		}
	}

	retireYear = member.yearAt65 - nearestFenceIndex;
			
	var infoFlagY = CLOUD_H + FENCE_OFFSET - 4*FENCE_H - FLAG1_H + 8;
	var infoFlagX = fences[nearestFenceIndex].qpos().x - FLAG1_W - INFO_FLAG1_W + (FENCE_W / 2);
	var flagX = (fences[nearestFenceIndex].qpos().x - FLAG1_W + (FENCE_W / 2) + 1);			
	
	$j("#flag1").css("top", infoFlagY - 8 + "px");
	$j("#flag1").css("left", flagX + "px");		
	$j("#info1").css("top", infoFlagY + "px");
	$j("#info1").css("left", infoFlagX + "px");				
 	$j("#info1").css("display", "block");
 	
 	if (member.retirements[member.retirements.length - 1 - nearestFenceIndex].pot > member.lta)
 	{
 		popUpOverLTA();
 	}	
 	else
 	{		 		
 		removePopUpOverLTA();
 	}
 	
 	// recreate info flags again
 	recreateInfoFlags();
 	
 	// re-create pension info balloon
	if (pensionInfoBalloon)
	{
		$j("#pension_info_balloon").remove();
		pensionInfoBalloon = null;
	}
	initPensionInfoPopups();
	
	// re-create cash info balloon
	if (cashInfoBalloon)
	{
		$j("#cash_info_balloon").remove();
		cashInfoBalloon = null;
	}
	initCashInfoPopups();			
}
//
//function initIPadSlider()
//{
//	alert('init');
//	var sliderIPadDragable = new webkit_draggable('sl' + salarySlider.n_id + 'slider', {onStart: sliderIPadStart, onMove: sliderIPadMove, onEnd: sliderIPadEnd});
//}
//
//function sliderIPadStart()
//{
//	salarySlider.f_sliderMouseDown(salarySlider.n_id);
//}
//function sliderIPadMove()
//{
//	salarySlider.f_sliderMouseMove();
//}
//function sliderIPadEnd()
//{
//	salarySlider.f_sliderMouseUp();
//}


function loadPeusedoMember()
{
	var sampleSchemeHis = new Array();
	sampleSchemeHis.push(Array('2009', '45'),
							new Array('2007', '60'),
							new Array('2004', '45'),
							new Array('2000', '60'));
							
	var sampleRetirements = new Array();
	sampleRetirements.push(	new Array('2011', '64123', '95123', '1000000', '33123', '34123'),
							new Array('2012', '65123', '105123', '1100000', '43123', '44123'),
							new Array('2013', '66123', '115123', '1200000', '53123', '54123'),
							new Array('2014', '67123', '125123', '1300000', '63123', '64123'),
							new Array('2015', '68123', '135123', '1400000', '65123', '66123'),
							new Array('2016', '69123', '145123', '1500000', '67123', '68123'),
							new Array('2017', '71230', '155123', '1612300', '69123', '70123'),
							new Array('2018', '71678', '165678', '1767800', '71123', '72123')
							);							
	member = new MemberAE({
						birthYear: '1953',
						schemeYear: '2000',
						currentOptions: {accrual: '45', salary: '109000'},
						modelOptions: {accrual: '60', salary: '114000'},						
						taxYear: {year: '2011', fiscalYear: '2012/2013', amount: '2567'},
						currentYear: '2011',
						pipYear: '2012',
						schemeHistories: sampleSchemeHis,
						retirements: sampleRetirements,
						lta: '1500000'					
	});
	
	carType = 'variable';
	carChoiceEnabled = true;
	isAccrualModelled = false;
	isSalaryModelled = false;
		
	retireYear = member.yearAt65;
	drawJourney(member);
	initModellingPopups();
	initAccrualList(member.currentOptions.accrual);
	initSalarySlider(member.currentOptions.salary * 0.8, member.currentOptions.salary * 1.2, member.currentOptions.salary);
}

