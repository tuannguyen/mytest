// 
// Copyright (c) 2008 Beau D. Scott | http://www.beauscott.com
// 
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 

/**
 * HelpBalloon.js
 * Prototype/Scriptaculous based help balloons / dialog balloons
 * @version 2.0.1
 * @requires prototype.js <http://www.prototypejs.org/>
 * @author Beau D. Scott <beau_scott@hotmail.com>
 * 4/10/2008
 */
var HelpBalloon = Object.extend(Class.create(), {
	/**
	 * Enumerated value for dynamic rendering position.
	 * @static
	 */
	POS_DYNAMIC: -1,
	/**
	 * Enumerated value for the top-left rendering position.
	 * @static
	 */
	POS_TOP_LEFT: 0,
	/**
	 * Enumerated value for the top-right rendering position.
	 * @static
	 */
	POS_TOP_RIGHT: 1,
	/**
	 * Enumerated value for the bottom-left rendering position.
	 * @static
	 */
	POS_BOTTOM_LEFT: 2,
	/**
	 * Enumerated value for the bottom-right rendering position.
	 * @static
	 */
	POS_BOTTOM_RIGHT: 3,
	/**
	 * CSS Classname to look for when doing auto link associations.
	 * @static
	 */
	ELEMENT_CLASS_NAME: 'HelpBalloon',
	/**
	 * Global array of all HelpBalloon instances.
	 * (Cheaper than document.getElementByClassName with a property check)
	 * @static
	 * @private
	 */
	_balloons: [],
	/**
	 * Event listener that auto-associates anchors with a dynamic HelpBalloon.
	 * Also begins mouse movement registration
	 * @static
	 */
	registerClassLinks: function(e) {
		$A(document.getElementsByClassName(HelpBalloon.ELEMENT_CLASS_NAME))
			.each(function(obj){
			// Only apply any element with an href tag
			if(obj && obj.tagName && obj.href && obj.href != '')
			{
				new HelpBalloon({
					icon:obj,
					method: 'get'
				});
			}
		});
		
		Event.observe(document, 'mousemove', HelpBalloon._trackMousePosition);
		
	},
	
	/**
	 * Private cache of the client's mouseX position
	 */
	_mouseX: 0,
	
	/**
	 * Private cache of the client's mouseY position
	 */
	_mouseY: 0,
			
	
	/**
	 * @param {Event} e
	 */
	_trackMousePosition: function(e) {
		if(!e) e = window.event;
		HelpBalloon._mouseX = e.clientX;
		HelpBalloon._mouseY = e.clientY;
	}
});

//
// Event for activating HelpBalloon classed links
//
Event.observe(window, 'load', HelpBalloon.registerClassLinks);

HelpBalloon.prototype = {
	
//
// Properties
// 	
	/**
	 * Configuration options
	 * @var {HelpBalloon.Options}
	 */
	options: null,

	/**
	 * Containing element of the balloon
	 * @var {Element}
	 */
	container: null,
	
	containerTop: null,
	
	containerMid: null,
	
	containerBtm: null,	
	/**
	 * Inner content container
	 * @var {Element}
	 */
	inner: null,
	/**
	 * A reference to the anchoring element/icon
	 * @var {Element}
	 */
	icon: null,
	/**
	 * Content container
	 * @var {Element}
	 */
	content: null,
	/**
	 * Closing button element
	 * @var {Element}
	 */
	button: null,
	/**
	 * The closer object. This can be the same as button, but could 
	 * also be a div with a png loaded as the back ground, browser dependent.
	 * @var {Element}
	 */
	closer: null,
	
	/**
	 * The closer object. This can be the same as button, but could 
	 * also be a div with a png loaded as the back ground, browser dependent.
	 * @var {Element}
	 */
	pointerContainer: null,
	pointerHolder: null,	
		
	/**
	 * Title container
	 * @var {Element}
	 */
	btnContainer: null,
	/**
	 * Background container for top and bottom banner (houses the balloon images
	 * @var {Element}
	 */
	bgContainerTop: null,
	bgContainerBtm: null,
	/**
	 * Array of balloon image references
	 * @var {Array}
	 */
	balloons: null,
	
	/**
	 * Array of pointer image references
	 * @var {Array}
	 */
	pointers: null,
	
	pointer: null,
	
	/**
	 * The local store of 'title'. Will change if the balloon is making a remote call
	 * unless options.title is specified
	 * @var {String}
	 */
	_titleString: null,
	
	/**
	 * The balloons visibility state.
	 * @var {Boolean}
	 */
	visible: false,
	
	/**
	 * Rendering status
	 * @var {Boolean}
	 */
	drawn: false,

	/**
	 * Stores the balloon coordinates
	 * @var {Object}
	 */
	balloonCoords: null,
		
	/**
	 * Width,height of the balloons
	 * @var {Array}
	 */
	balloonDimensions: null,
	
	/**
	 * x offsets the balloons
	 * @var {Array}
	 */
	balloonFixedX: 0,	
	
	/**
	 * ID for HelpBalloon
	 * @var {String}
	 */
	id: null,
	
	/**
	 * Used at render time to measure the dimensions of the loaded balloon
	 * @private
	 */
	_lastBalloon: null,	
	
	_contentHeight:0,	
	
	_hiddenSelects: null,
		

//
// Methods
//

	/**
	 * @param {Object} options
	 * @see HelpBalloon.Options
	 * @constructor
	 */
	initialize: function(options)
	{
		
		this.options = new HelpBalloon.Options();
		Object.extend(this.options, options || {});

		this._titleString = this.options.title;
		this.balloonDimensions = this.options.balloonDimensions;
		this.balloonFixedX = this.options.balloonFixedX;
		//
		// Preload the balloon and button images so they're ready
		
		// at render time
		//
		// 0 1
		//  X
		// 2 3
		//
		this.balloons = [];
		for(var i = 0; i < 3; i++)
		{
			var balloon = new Element('img', {
				src: this.options.balloonPrefix + i + this.options.balloonSuffix
			});
			this.balloons.push(balloon.src);
		}
		this.pointers = [];
		for(var i = 0; i < 2; i++)
		{
			var pointer = new Element('img', {
				src: this.options.pointerPrefix + i + this.options.pointerSuffix,
				width: 23,
				height: 24
			});
			this.pointers.push(pointer);
		}
		
		this._lastBalloon = balloon;
		
		this.button = new Element('img', {
			src: this.options.button
		});
		
		//
		// Create the anchoring icon, or attach the balloon to the given icon element
		// If a string is passed in, assume it's a URL, if it's an object, assume it's
		// a DOM member.
		//
		if(typeof this.options.icon == 'string')
		{
			this.icon = new Element('img', {
				src: this.options.icon,
				id: this.id + "_icon"
			});
			Element.setStyle(this.icon, this.options.iconStyle);
		}
		else
		{
			//
			// Not a string given (most likely an object. Do not append the element
			// Kind of a hack for now, but I'll fix it in the next version.
			//
			this.icon = this.options.icon;
			this.options.returnElement = true;
		}
		
		this.icon._HelpBalloon = this;		
			
		//
		// Attach rendering events
		//

		for(i = 0; i < this.options.useEvent.length; i++)
			Event.observe(this.icon, this.options.useEvent[i], this.toggle.bindAsEventListener(this));
		
		this.container = new Element('div');
		this.container._HelpBalloon = this;
		this.container.id = this.options.id;
		
		this.id = 'HelpBalloon_' + Element.identify(this.container);
		
		HelpBalloon._balloons.push(this);
		
		this.pointerContainer = new Element('div');
		//this.pointerHolder = new Element('div');
		this.containerTop = new Element('div');
		this.containerMid = new Element('div');
		this.containerBtm = new Element('div');

		//
		// If we are not relying on other javascript to attach the anchoring icon
		// to the DOM, we'll just do where the script is called from. Default behavior.
		//
		// If you want to use external JavaScript to attach it to the DOM, attach this.icon
		//
		// calculate the potential height of the content
		this.content = document.createElement("div");
		this.content.style.width = (this.balloonDimensions[0] - 2*this.options.contentMargin) + 'px';
		this.content.style.cssFloat = 'left';
		if (document.getElementById(this.options.contentId))
		{									
			var contentHtml = $(this.options.contentId).innerHTML;
			
			if (this.options.autoUpdateIds)	
			{			
				var contentHtmlTmp = contentHtml;
				// replace all elements' ids with new ids
				var innerIds = new Array();
				// find the id
				var partern = '\\bid[ ]*[=][ ]*[\\\'\\\"]*[\\w]+[\\\'\\\"]*';
				var re = new RegExp(partern);
				var m = re.exec(contentHtmlTmp);	
				while (m != null && m.length > 0)
				{
					var matchIndex = m.index;
					var matchStr = m[0];
				 	matchStr = matchStr.replace(/'[\\\'\\\"]'/g, '').replace(/^\s+|\s+$/g, '');	// trim()
				    matchId = matchStr.substring(matchStr.indexOf('=') + 1).replace(/^\s+|\s+$/g, ''); // trim()
				    
				    innerIds.push(matchId);
				    
				    contentHtmlTmp = contentHtmlTmp.substring(matchIndex + matchStr.length);
				    m = re.exec(contentHtmlTmp);
				}
				
				for(var i=0; i<innerIds.length; i++) {
					var innerId = innerIds[i];
					var re = new RegExp('\\b' + innerId + '\\b', "g");
					// replace old id with new id
					contentHtml = contentHtml.replace(re, 'popup_' + innerId);
				}
			}
			this.content.innerHTML = contentHtml;															
		}
		else
		{
			this.content.innerHTML = '<p>&nbsp</p>';
		}
		document.getElementById('content').appendChild(this.content);
		this._contentHeight = this.getContentHeight(this.content);
		this.content.style.display = 'none';

		if(!this.options.returnElement)
		{
			document.write('<span id="' + this.id + '"></span>');
			var te = $(this.id);
			var p = te.parentNode;
			p.insertBefore(this.icon, te);
			p.removeChild(te);
		}	
		
		this._hiddenSelects = new Array();	
	},
	
	getContentHeight: function(contentEle)
	{
		var contentHeightExtract = 0;
		if (contentEle)
		{
			for(var i=0; i<contentEle.childNodes.length; i++) {
				var contentChild = contentEle.childNodes[i];
				if (contentChild.nodeType == 1 && $(contentChild).getStyle('display').toLowerCase() != 'none')
				{					
					var contentChildHeight = $(contentChild).getHeight();
					var contentChildMarginTop = $(contentChild).getStyle('marginTop');
					var contentChildMarginBottom = $(contentChild).getStyle('marginBottom');
					
					var contentChildHeightOuter = contentChildHeight;
					 
					if (contentChildMarginTop.indexOf('px') > 0)
					{
						contentChildHeightOuter += parseInt(contentChildMarginTop);
					}
					else if (contentChildMarginTop.indexOf('em') > 0)
					{
						contentChildHeightOuter += (parseInt(contentChildMarginTop) * 12);
					}
					
					if (contentChildMarginBottom.indexOf('px') > 0)
					{
						contentChildHeightOuter += parseInt(contentChildMarginBottom);
					}
					else if (contentChildMarginBottom.indexOf('em') > 0)
					{
						contentChildHeightOuter += (parseInt(contentChildMarginBottom) * 12);
					}
					
					if (contentChild.nodeName.toLowerCase() == 'ul')
					{
						contentHeightExtract += 10;
					}
					contentHeightExtract += contentChildHeightOuter;
					contentHeightExtract -= 5; // trick
				}
			}
		}

		return contentHeightExtract;
	},
	
	updateContentHeight: function(expand)
	{
		this._contentHeight += expand;
		//alert(this._contentHeight);
		var containerTopHeight = 25;
		var containerBtmHeight = 37;
		
		// update balloon height
		this.balloonDimensions[1] = containerTopHeight + this._contentHeight + containerBtmHeight;
		if (Prototype.Browser.IE)
		{
			if (parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
			{
				Element.setStyle(this.content, {
					'height':		this._contentHeight + 'px'
				});
			}
			else
			{
				Element.setStyle(this.content, {
					'height':		this._contentHeight + 'px'
				});
			}
		}
		else
		{		
			Element.setStyle(this.content, {
				'height':		this._contentHeight + 'px'
			});
		}
		this._reposition(window.event);
	},
	
	
	/**
	 * Toggles the help balloon
	 * @param {Object} e Event
	 */
	toggle: function(event)
	{		
		if(!event) event = window.event || {type: this.options.useEvent, target: this.icon};
		var icon = Event.element(event);
		Event.stop(event);
		if(event.type == this.options.useEvent && !this.visible && icon == this.icon)
		{			
			this.show(event);
			this._updatePointerX(event);
		}
		else
			this.hide();
	},

	/**
	 * Triggers the balloon to appear
	 */
	show: function(event)
	{
		if(!this.visible){
			if(!event) event = window.event;
			if(!this.drawn || !this.options.cacheRemoteContent) this._draw();
			this._reposition(event);
			this._hideOtherHelps();
			if(this.options.showEffect)
			{
				this.options.showEffect(this.container, Object.extend(this.options.showEffectOptions, {
					afterFinish: this._afterShow.bindAsEventListener(this)
				}));
			}
			else
			{
				this._afterShow();
			}
			Event.observe(window, 'resize', this._reposition.bindAsEventListener(this));
		}
	},
	
	/**
	 * Sets the container to block styling and hides the elements below the
	 * container (if in IE)
	 * @private
	 */
	_afterShow: function()
	{
		this.container.className = 'aa_popup_show';
		
		this._hideLowerElements();
		this.visible = true;
		if(this.options.autoHideTimeout)
		{
			setTimeout(this._hideQueue.bind(this), this.options.autoHideTimeout);
		}
		
	},
	
	/**
	 * Checks the mouse position and triggers a hide after the time specified in autoHideTimeout
	 * if the mouse is not currently over the balloon, otherwise it requeue's a hide for later.
	 */
	_hideQueue: function()
	{
		if(Position.within(this.container, HelpBalloon._mouseX, HelpBalloon._mouseY))
			setTimeout(this._hideQueue.bind(this), this.options.autoHideTimeout);
		else
			this.hide();
	},
	
	/**
	 * Hides the balloon
	 */
	hide: function()
	{
		if(this.visible)
		{
			this._showLowerElements();
			if(this.options.hideEffect)
			{
				this.options.hideEffect(this.container, Object.extend(this.options.hideEffectOptions, {
					afterFinish: this._afterHide.bindAsEventListener(this)
				}));
			}
			else
			{
				this._afterHide();
			}
			Event.stopObserving(window, 'resize', this._reposition.bindAsEventListener(this));
			
			// display hidden select box
			for(var i=0; i< this._hiddenSelects.length; i++) {
				this._hiddenSelects[i].style.visibility = '';
			}			
		}
	},
	
	/**
	 * Sets the container's display to block
	 * @private
	 */
	_afterHide: function()
	{
//		Element.setStyle(this.container, {
//			'display': 'none'
//		});
		this.container.className = 'aa_popup_hide';
		this.visible = false;
	},
	
	/**
	 * Redraws the balloon based on the current coordinates of the icon.
	 * @private
	 */
	_reposition: function(event)
	{				
		// Fixed
		var pointerDimensions = [this.pointers[0].width, this.pointers[0].height];		
		var containerTopHeight = 25;
		var containerBtmHeight = 37;
		var pointerContainerHeight = 12;
		
				
		var ho = 0;
		var vo = 0;
		if(this.icon.tagName.toLowerCase() == 'area' || !!this.icon.isMap)
		{
			this.balloonCoords = Event.pointer(event);
		}
		else
		{
			this.balloonCoords = this._getXY(this.icon);
			//Horizontal and vertical offsets in relation to the icon's 0,0 position.
			// Default is the middle of the object
			ho = this.icon.offsetWidth;
			vo = this.icon.offsetHeight;						
		}		
		
		// update balloon height
		this.balloonDimensions[1] = containerTopHeight + this._contentHeight + containerBtmHeight;
		
		// identify if balloon speach out up or down
		var speakUp = true;
		var offsetHeight = this.balloonCoords.y - this.balloonDimensions[1];
		
		if(offsetHeight < 0) speakUp = false;
		
		var zx = this.balloonCoords.x - 300;
		var zx = this.balloonCoords.x - 300;
		var bodyX = 0;
		var bodyMarginLeft = $(document.body).getStyle('marginLeft'); 
		if (bodyMarginLeft.indexOf('px') > 0)
		{
			bodyX = parseInt(bodyMarginLeft);
		}
		else if (bodyMarginLeft.indexOf('em') > 0)
		{
			bodyX = (parseInt(bodyMarginLeft) * 12);
		}
		if (zx < bodyX) zx = bodyX;
		
		if (this.balloonFixedX > -1)
		{
			zx = this.balloonFixedX;
		}
		var zy = 0;
		
		if (speakUp)
		{
			zy = this.balloonCoords.y - this.balloonDimensions[1];
		}
		else
		{
			this.balloonCoords.y += vo;	
			zy += this.balloonCoords.y;
		}				
		
		// hiden the select boxes under the balloon for IE6
		this._hiddenSelects.splice(0,this._hiddenSelects.length);
		var mainDocumentContentEle = document.getElementById('content');
		var selectEles = mainDocumentContentEle.getElementsByTagName('select');
		for(var i=0; i<selectEles.length; i++) {
			var selectEle = selectEles[i];
			var selectY = this._findPosY(selectEle);
			if (selectY >= zy && selectY <= (zy + this.balloonDimensions[1]))
			{
				this._hiddenSelects.push(selectEle);
				selectEle.style.visibility = 'hidden';
			}
		} 
		
		var pointerX = this.balloonCoords.x - zx + (ho - pointerDimensions[0])/2;	
		var pointerY = 0;
		if (speakUp)
		{
			pointerY = this.balloonDimensions[1] - pointerDimensions[1];
			if (this.pointer == null) this.pointer = this.pointers[0];
		}
		else
		{
			pointerY = 2;
			if (this.pointer == null) this.pointer = this.pointers[1];		
		}		
		
		var containerStyle = {
			/*'backgroundRepeat': 'no-repeat',
			'backgroundColor': 'transparent',
			'backgroundPosition': 'top left',*/
			'left' 	: zx + "px",
			'top'	: zy + "px",
			'width' : this.balloonDimensions[0] + 'px'
		}
		
		Element.setStyle(this.container, containerStyle);						
		Element.setStyle(this.pointerContainer, {
			'float' 	: 'left',
			'padding' 	: '0px',
			'width' 	: '100%',
			'height'	: pointerContainerHeight + "px"
		});
				
		var containerTopStyle = {
			'float' 	: 'left',		
			'padding' 	: '0px',
			'width' 	: '100%',
			'height'	: containerTopHeight + "px"				
		}		
		if(Prototype.Browser.IE)
		{
			//
			// Fix for IE alpha transparencies
			//
			if(parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
			{
				Element.setStyle(this.bgContainerTop, {
					'float' 	: 'left',
					'padding' 	: '0px',
					'width' 	: '100%',
					'height'	: containerTopHeight + "px",					
					'filter'	: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.balloons[0] + "', sizingMethod='scale')"
				});
			}
			else
				containerTopStyle['background'] = 'transparent url(' + this.balloons[0] + ') top left no-repeat';
		}
		else
		{
			containerTopStyle['background'] = 'transparent url(' + this.balloons[0] + ') top left repeat-y';
		}		
		Element.setStyle(this.containerTop, containerTopStyle);

		
		var containerMidStyle = {
			'float' 	: 'left',		
			'padding' 	: '0px',
			'width' 	: '100%',
			'height'	: this._contentHeight + "px"			
		}		
		if(Prototype.Browser.MobileSafari)
		{
			//containerMidStyle['backgroundColor'] = '#ff0000';
			containerMidStyle['background'] = '#ffffff url(' + this.balloons[1] + ') top left repeat-y';
			containerMidStyle['height'] = (this._contentHeight + 2) + "px"
		}
		else
		{
			containerMidStyle['background'] = 'transparent url(' + this.balloons[1] + ') top left repeat-y';
		}
		
		Element.setStyle(this.containerMid, containerMidStyle);
		
		var containerBtmStyle = {
			'float' : 'left',				
			'left' 	: "0px",
			'width' : '100%',
			'height': containerBtmHeight + "px"
				
		}
		if(Prototype.Browser.IE)
		{
			//
			// Fix for IE alpha transparencies
			//
			if(parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
			{
				Element.setStyle(this.bgContainerBtm, {
					'float' 	: 'left',
					'padding' 	: '0px',
					'width' 	: '100%',
					'height'	: containerBtmHeight + "px",					
					'filter'	: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.balloons[2] + "', sizingMethod='scale')"
				});
			}
			else
				containerBtmStyle['background'] = 'transparent url(' + this.balloons[2] + ') top left no-repeat';
		}
		else
		{
			containerBtmStyle['background'] = 'transparent url(' + this.balloons[2] + ') top left repeat-y';
		}		
		Element.setStyle(this.containerBtm, containerBtmStyle);		
			
		if (speakUp)
		{			
			this.container.appendChild(this.containerTop);
			this.container.appendChild(this.containerMid);
			this.container.appendChild(this.containerBtm);
			
		}
		else
		{
			this.container.appendChild(this.pointerContainer);			
			this.container.appendChild(this.containerTop);
			this.container.appendChild(this.containerMid);
			this.container.appendChild(this.containerBtm);						
		}
		
		// closer		
		var buttonDimensions = [
			this.button.width,
			this.button.height
		];		
		if (speakUp)
		{
			Element.setStyle(this.closer, {
				'width': buttonDimensions[0] + 'px',
				'height': buttonDimensions[1] + 'px',
				'cursor': 	'pointer',
				'position':	'absolute',
				'top': 		'15px',
				'right': 	'15px'
			});
		}
		else
		{
			Element.setStyle(this.closer, {
				'width': buttonDimensions[0] + 'px',
				'height': buttonDimensions[1] + 'px',
				'cursor': 	'pointer',
				'position':	'absolute',
				'top': 		(pointerContainerHeight + 15) + 'px',
				'right': 	'15px'
			});			
		}		
		
		if (this.pointerHolder == null)
		{
			this.pointerHolder = new Element('div');
		
			var pointerHolderStyle = {
				'position': 	'absolute',
				'top':			pointerY + 'px',
				'left':			pointerX + 'px',
				'width': 		pointerDimensions[0] + 'px',
				'height': 		pointerDimensions[1] + 'px'
			}			
			if(Prototype.Browser.IE)
			{
				//
				// Fix for IE 6 alpha transparencies
				//
				if(parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
				{
					Element.setStyle(this.pointerHolder, {
						'float' 	: 'left',
						'padding' 	: '0px',
						'position'	: 'absolute',
						'top'		: pointerY + 'px',
						'left'		: pointerX + 'px',						
						'width' 	: pointerDimensions[0] + 'px',
						'height'	: pointerDimensions[1] + 'px',				
						'filter'	: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.pointer.src + "', sizingMethod='scale')"
					});
				}
				else
					pointerHolderStyle['background'] = 'transparent url(' + this.pointer.src + ') top left no-repeat';
			}
			else
			{
				pointerHolderStyle['background'] = 'transparent url(' + this.pointer.src + ') top left no-repeat';
			}		
			Element.setStyle(this.pointerHolder, pointerHolderStyle);	
			
			if (speakUp)
			{
				if(Prototype.Browser.IE)
				{
					this.bgContainerBtm.appendChild(this.pointerHolder);
				}
				else
				{
					this.containerBtm.appendChild(this.pointerHolder);
				}
			}
			else
			{
				this.pointerContainer.appendChild(this.pointerHolder);
			}
		}	
		else
		{
			var pointerHolderStyle = {
				'position': 	'absolute',
				'top':			pointerY + 'px',
				'left':			pointerX + 'px',
				'width': 		pointerDimensions[0] + 'px',
				'height': 		pointerDimensions[1] + 'px'
			}			
			if(Prototype.Browser.IE)
			{
				//
				// Fix for IE 6 alpha transparencies
				//
				if(parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
				{
					Element.setStyle(this.pointerHolder, {
						'float' 	: 'left',
						'padding' 	: '0px',
						'position'	: 'absolute',
						'top'		: pointerY + 'px',
						'left'		: pointerX + 'px',						
						'width' 	: pointerDimensions[0] + 'px',
						'height'	: pointerDimensions[1] + 'px',				
						'filter'	: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.pointer.src + "', sizingMethod='scale')"
					});
				}
				else
					pointerHolderStyle['background'] = 'transparent url(' + this.pointer.src + ') top left no-repeat';
			}
			else
			{
				pointerHolderStyle['background'] = 'transparent url(' + this.pointer.src + ') top left no-repeat';
			}		
			Element.setStyle(this.pointerHolder, pointerHolderStyle);
		}												
	},
	
	_updatePointerX:function (event)
	{
		var pointerDimensions = [this.pointers[0].width, this.pointers[0].height];		

		var ho = 0;
		var vo = 0;		
		if(this.icon.tagName.toLowerCase() == 'area' || !!this.icon.isMap)
		{
			this.balloonCoords = Event.pointer(event);
		}
		else
		{
			this.balloonCoords = this._getXY(this.icon);
			//Horizontal and vertical offsets in relation to the icon's 0,0 position.
			// Default is the middle of the object
			ho = this.icon.offsetWidth;
			vo = this.icon.offsetHeight;						
		}		
		
		var zx = this.balloonCoords.x - 300;
		var bodyX = 0;
		var bodyMarginLeft = $(document.body).getStyle('marginLeft'); 
		if (bodyMarginLeft.indexOf('px') > 0)
		{
			bodyX = parseInt(bodyMarginLeft);
		}
		else if (bodyMarginLeft.indexOf('em') > 0)
		{
			bodyX = (parseInt(bodyMarginLeft) * 12);
		} 
		if (zx < bodyX) zx = bodyX;
		
		if (this.balloonFixedX > -1)
		{
			zx = this.balloonFixedX;
		}			
		
		var pointerX = this.balloonCoords.x - zx + (ho - pointerDimensions[0])/2;	
		var pointerHolderStyle = {
				'left':			pointerX + 'px'
			}
		Element.setStyle(this.pointerHolder, pointerHolderStyle);		
	},

	/**
	 * Renders the Balloon
	 * @private
	 */
	_draw: function()
	{
		Element.setStyle(
			this.container, 
			Object.extend(this.options.balloonStyle, {
				'position': 	'absolute'
			})
		);
		
		this.container.className = 'aa_popup_hide';
		
		var url = this.options.dataURL;
		
		//
		// Play nicely with anchor tags being used as the icon. Use it's specified href as our
		// data URL unless one has already been used specified in this.options.dataURL. 
		// We'll also force a new request with this as it may be an image map.
		//
		if(this.icon.className == 'a')
		{
			if(!this.options.dataURL && this.icon.href != ''){
				url = this.icon.href;
				this.options.cacheRemoteContent = false;
			}
		}
				
		
		var contentDimensions = [
			this.balloonDimensions[0] - (2 * this.options.contentMargin),
			this.balloonDimensions[1] - (2 * this.options.contentMargin)
		];
				
		//
		// Create all the elements on demand if they haven't been created yet
		//
		if(!this.drawn)
		{
			this.inner = new Element('div');		
			// PNG fix for IE
			if(Prototype.Browser.IE)
			{
				this.bgContainerTop = new Element('div');
				
				// Have to create yet-another-child of container to house the background for IE... when it was set in
				// the main container, it for some odd reason prevents child components from being clickable.
				this.containerTop.appendChild(this.bgContainerTop);
				
				this.bgContainerBtm = new Element('div');
				
				// Have to create yet-another-child of container to house the background for IE... when it was set in
				// the main container, it for some odd reason prevents child components from being clickable.
				this.containerBtm.appendChild(this.bgContainerBtm);			
																						
			}	
			
			// PNG fix for IE
			if(Prototype.Browser.IE  && this.options.button.toLowerCase().indexOf('.png') > -1)
			{				
				this.closer =  new Element('div');
				Element.setStyle(this.closer, {
					'filter':
						"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.options.button + "', sizingMethod='scale')"
				});
				this.bgContainerTop.appendChild(this.closer);
			}
			else
			{
				this.closer = this.button;
				this.containerTop.appendChild(this.closer);
			}
			
			Event.observe(this.closer, 'click', this.toggle.bindAsEventListener(this));
			
			
			//this.content =  new Element('div');			
			this.inner.appendChild(this.content);			
			this.containerMid.appendChild(this.inner);					
			
			document.getElementsByTagName('body')[0].appendChild(this.container);
			
			this.drawn = true;
			
			Event.observe(document.getElementById('container'), 'click', this.hide.bindAsEventListener(this));
		}
		
		// Reset content value:
		// set the margin of the last child to 0 as it will expand in IE 6
		var contentChilds = this.content.childNodes;
		var contentLastChild = null;
		for(var i=0; i<contentChilds.length; i++) {
			if (contentChilds[i].nodeType == 1)// element node
			{
				contentLastChild = contentChilds[i];
			}
		}
		if (contentLastChild)
		{
			Element.setStyle(contentLastChild, {
			'marginBottom' 	: '0px'
		});	
		}
		//
		//Reapply styling to components as values might have changed
		//
		Element.setStyle(this.inner, {
			'float' 	: 'left',
			'padding' 	: '0px',
			'width' 	: contentDimensions[0] + 'px',
			'marginLeft': this.options.contentMargin
		});				
		
		if (Prototype.Browser.IE)
		{
			if (parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 6)
			{
				Element.setStyle(this.content, {
					'width':		contentDimensions[0] + 'px',
					'float': 		'left',
					'fontFamily': 	'Arial',
					'fontSize': 	'9pt',
					'fontWeight': 	'normal',
					'color': 		'#666',
					'paddingLeft': 	'0px',
					'display': 		'block'
				});
			}
			else
			{
				Element.setStyle(this.content, {
					'width':		(contentDimensions[0] - 5) + 'px',
					'float': 		'left',
					'fontFamily': 	'Arial',
					'fontSize': 	'9pt',
					'fontWeight': 	'normal',
					'color': 		'#666',
					'paddingLeft': 	'5px',
					'display': 		'block'
				});
			}
		}
		else
		{		
			Element.setStyle(this.content, {
				'width':		(contentDimensions[0] - 10) + 'px',
				'height':		this._contentHeight + 'px',
				'float': 		'left',
				'fontFamily': 	'Arial',
				'fontSize': 	'9pt',
				'fontWeight': 	'normal',
				'color': 		'#666',
				'paddingLeft': 	'10px',
				'display': 		'block'
			});
		}			
		
	},

	/**
	 * Gets the current position of the obj
	 * @param {Element} element to get position of
	 * @return Object of (x, y, x2, y2)
	 */
	_getXY: function(obj)
	{
		var pos = Position.cumulativeOffset(obj)
		var y = pos[1];
		var x = pos[0];
		var x2 = x + parseInt(obj.offsetWidth);
		var y2 = y + parseInt(obj.offsetHeight);
		return {'x':x, 'y':y, 'x2':x2, 'y2':y2};

	},

	/**
	 * Determins if the object is a child of the balloon element
	 * @param {Element} Element to check parentage
	 * @return {Boolean}
	 * @private
	 */
	_isChild: function(obj)
	{
		var i = 15;
		do{
			if(obj == this.container)
				return true;
			obj = obj.parentNode;
		}while(obj && i--);
		return false
	},

	/**
	 * Determines if the balloon is over this_obj object
	 * @param {Element} Object to look under
	 * @return {Boolean}
	 * @private
	 */
	_isOver: function(this_obj)
	{
		if(!this.visible) return false;
		if(this_obj == this.container || this._isChild(this_obj)) return false;
		var this_coords = this._getXY(this_obj);
		var that_coords = this._getXY(this.container);
		if(
			(
			 (
			  (this_coords.x >= that_coords.x && this_coords.x <= that_coords.x2)
			   ||
			  (this_coords.x2 >= that_coords.x &&  this_coords.x2 <= that_coords.x2)
			 )
			 &&
			 (
			  (this_coords.y >= that_coords.y && this_coords.y <= that_coords.y2)
			   ||
			  (this_coords.y2 >= that_coords.y && this_coords.y2 <= that_coords.y2)
			 )
			)

		  ){
			return true;
		}
		else
			return false;
	},

	/**
	 * Restores visibility of elements under the balloon
	 * (For IE)
	 * TODO: suck yourself
	 * @private
	 */
	_showLowerElements: function()
	{
		if(this.options.hideUnderElementsInIE)
		{
			var elements = this._getWeirdAPIElements();
			for(var i = 0; i < elements.length; i++)
			{
				if(this._isOver(elements[i]))
				{
					if(elements[i].style.visibility != 'visible' && elements[i].hiddenBy == this)
					{
						elements[i].style.visibility = 'visible';
						elements[i].hiddenBy = null;
					}
				}
			}
		}
	},

	/**
	 * Hides elements below the balloon
	 * (For IE)
	 * @private
	 */
	_hideLowerElements: function()
	{
		if(this.options.hideUnderElementsInIE)
		{
			var elements = this._getWeirdAPIElements();
			for(var i = 0; i < elements.length; i++)
			{
				if(this._isOver(elements[i]))
				{
					if(elements[i].style.visibility != 'hidden')
					{
						elements[i].style.visibility = 'hidden';
						elements[i].hiddenBy = this;
					}
				}
			}
		}
	},

	/**
	 * Determines which elements need to be hidden
	 * (For IE)
	 * @return {Array} array of elements
	 */
	_getWeirdAPIElements: function()
	{
		if(!Prototype.Browser.IE) return [];
		var objs = ['select', 'input', 'object'];
		var elements = [];
		for(var i = 0; i < objs.length; i++)
		{
			var e = document.getElementsByTagName(objs[i]);
			for(var j = 0; j < e.length; j++)
			{
				elements.push(e[j]);
			}
		}
		return elements;
	},

	/**
	 * Hides the other visible help balloons
	 * @param {Event} e
	 */
	_hideOtherHelps: function(e)
	{
		if(this.options.hideOtherBalloonsOnDisplay)
		{
			$A(HelpBalloon._balloons).each(function(obj){
				if(obj != this)
				{
					obj.hide();
				}
			}.bind(this));
		}
	},
	
	_findPosX: function (obj)
	  {
	    var curleft = 0;
	    if(obj.offsetParent)
	        while(1) 
	        {
	          curleft += obj.offsetLeft;
	          if(!obj.offsetParent)
	            break;
	          obj = obj.offsetParent;
	        }
	    else if(obj.x)
	        curleft += obj.x;
	        
	    return curleft;
	  } ,
	  
	_findPosY: function (obj)
	{
	    var curtop = 0;
	    if(obj.offsetParent)
	        while(1)
	        {
	          curtop += obj.offsetTop;
	          if(!obj.offsetParent)
	            break;
	          obj = obj.offsetParent;
	        }
	    else if(obj.y)
	        curtop += obj.y;
	        
	    return curtop;
	}
	
};

/**
 * HelpBalloon.Options
 * Helper class for defining options for the HelpBalloon object
 * @author Beau D. Scott <beau_scott@hotmail.com>
 */
HelpBalloon.Options = Class.create();
HelpBalloon.Options.prototype = {
	
	/**
	 * @constructor
	 * @param {Object} overriding options
	 */
	initialize: function(values){
		// Apply the overriding values to this
		Object.extend(this, values || {});
	},
	
	id: null,
	
	/**
	 * Show Effect
	 * The Scriptaculous (or compatible) showing effect function
	 * @var Function
	 */
	showEffect: window.Scriptaculous ? Effect.Appear : null,
	
	/**
	 * Show Effect options
	 */
	showEffectOptions: {duration: 0.2},
	
	/**
	 * Hide Effect
	 * The Scriptaculous (or compatible) hiding effect function
	 * @var Function
	 */
	hideEffect: window.Scriptaculous ? Effect.Fade : null,
	
	/**
	 * Show Effect options
	 */
	hideEffectOptions: {duration: 0.2},
	
	/**
	 * For use with embedding this object into another. If true, the icon is not created
	 * and not appeneded to the DOM at construction.
	 * Default is false
	 * @var {Boolean}
	 */
	returnElement: false,
	
	/**
	 * URL to the anchoring icon image file to use. This can also be a direct reference 
	 * to an existing element if you're using that as your anchoring icon.
	 * @var {Object}
	 */
	icon: 'images/icon.gif',
	
	/**
	 * Alt text of the help icon
	 * @var {String}
	 */
	altText: 'Click here for help with this topic.',
	
	/**
	 * URL to pull the title/content XML
	 * @var {String}
	 */
	dataURL: null,
	
	/**
	 * Static title of the balloon
	 * @var {String}
	 */
	title: null,
	
	/**
	 * Static content of the balloon
	 * @var {String}
	 */
	content: null,
	
	contentId: null,
	
	/**
	 * The event type to listen for on the icon to show the balloon.
	 * Default 'click'
	 * @var {String}
	 */
	useEvent: ['click'],
	
	/**
	 * Request method for dynamic content. (get, post)
	 * Default 'get'
	 * @var {String}
	 */
	method:	'get',
	
	/**
	 * Flag indicating cache the request result. If this is false, every
	 * time the balloon is shown, it will retrieve the remote url and parse it
	 * before the balloon appears, updating the content. Otherwise, it will make
	 * the call once and use the same content with each subsequent showing.
	 * Default true
	 * @var {Boolean}
	 */
	cacheRemoteContent: true,
	
	/**
	 * Vertical and horizontal margin of the content pane
	 * @var {Number}
	 */
	contentMargin: 10,
	
	/**
	 * X coordinate of the closing button
	 * @var {Number}
	 */
	buttonX: 246,
	
	/**
	 * Y coordinate of the closing button
	 * @var {Number}
	 */
	buttonY: 35,
	
	/**
	 * Closing button image path
	 * @var {String}
	 */
	button: 'images/button.png',
	
	/**
	 * Balloon image path prefix. There are 4 button images, numerically named, starting with 0.
	 * 0 1
	 *  X
	 * 2 3
	 * X indicates the anchor corner
	 * @var {String}
	 */
	balloonPrefix: 'images/balloon-',
	
	/**
	 * The image filename suffix, including the file extension
	 * @var {String}
	 */
	balloonSuffix: '.png',
	
	/**
	 * Balloon pointer image path prefix. There are 4 button images, numerically named, starting with 0.
	 * 0 1
	 *  X
	 * 2 3
	 * X indicates the anchor corner
	 * @var {String}
	 */
	pointerPrefix: 'images/balloon-',
	
	/**
	 * The pointer image filename suffix, including the file extension
	 * @var {String}
	 */
	pointerSuffix: '.png',	
	
	/**
	 * Position of the balloon's anchor relative to the icon element.
	 * Combine one horizontal indicator (left, center, right) and one vertical indicator (top, middle, bottom).
	 * Numeric values can also be used in an X Y order. So a value of 9 13 would place the anchor 9 pixels from
	 * the left and 13 pixels below the top. (0,0 is top left). If values are greater than the width or height
	 * the width or height of the anchor are used instead. If less than 0, 0 is used.
	 * Default is 'center middle'
	 * @var {String}
	 */
	anchorPosition: 'center top',
	
	balloonDimensions: [570,365],
	
	balloonFixedX: -1,
	
	/**
	 * Flag indicating whether to hide the elements under the balloon in IE.
	 * Setting this to false can cause rendering issues in Internet Explorer
	 * as some elements appear on top of the balloon if they're not hidden.
	 * Default is true.
	 * @var {Boolean}
	 */
	hideUnderElementsInIE: true,
	
	/**
	 * Default Balloon styling
	 * @var {Object}
	 */	
	balloonStyle: {},
	
	/**
	 * Default Title Bar style
	 * @var {Object}
	 */
	titleStyle: {
		'color': 'black',
		'fontSize': '16px',
		'fontWeight': 'bold',
		'fontFamily': 'Arial'
	},
	
	/**
	 * Icon custom styling
	 * @var {Object}
	 */
	iconStyle: {
		'cursor': 'pointer'
	},
	
	/**
	 * Flag indication whether to automatically hide any other visible HelpBalloon on the page before showing the current one.
	 * @var {Boolean}
	 */
	hideOtherBalloonsOnDisplay: true,
	
	/**
	 * If you want the balloon to always display in a particular location, set this 
	 */
	fixedPosition: HelpBalloon.POS_DYNAMIC,
	
	/**
	 * Number of milliseconds to hide the balloon after showing and after the mouse is not over the balloon.
	 * A value of 0 means it will not auto-hide
	 * @var {Number}
	 */
	autoHideTimeout: 0,
	
	autoUpdateIds: false
	
};