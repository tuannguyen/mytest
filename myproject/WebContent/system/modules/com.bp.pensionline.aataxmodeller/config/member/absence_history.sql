SELECT ta.TA03A as "Absence Type"
    ,ta.TA04D as "From"
    ,ta.TA05D as "To"
    ,nvl(ta.TA10P ,0) as "Worked"
    ,nvl(ta.TA11P ,0) as "Employed"
    ,ta.TA06A as "Service Indicator"
FROM temporary_absence ta
WHERE bgroup = :bgroup
AND refno = :refno
order by ta.TA04D desc