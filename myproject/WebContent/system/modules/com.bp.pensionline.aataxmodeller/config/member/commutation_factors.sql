select case  
  when FI01X = '01JW' then 'M'
  when FI01X = '01JX' then 'F'
  end as "Gender",
  FI02I as "NRA",
  FI03E as "Value"
from 
  FACTOR_VALUES_INTEGER 
where 
  bgroup = 'BPF'
  and FI01X in ('01JX','01JW')