SELECT nvl(ab.ab06i, 0) as "Days", ab.SUB as "Sub", CA26X as "Type", 
case 
  when CA26X = 'EAST' then 45 
  when CA26X = 'OEST' then 45
  else 60
end as "Accrual"
 FROM augmentation_benefit ab
 WHERE bgroup = :bgroup AND refno = :refno
      AND ab.SUB in ('OLDN','NEWE','NEWF','NEWK','NEWL','OLDA','OLDB'
                    ,'OLDG','OLDM','OLDH','OLDN','NEWE','NEWF','NEWK'
                    ,'NEWL','OLDA','OLDB','OLDG','OLDM','OLDH')