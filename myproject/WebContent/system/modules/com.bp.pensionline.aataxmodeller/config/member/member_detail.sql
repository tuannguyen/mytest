select (b.bgroup || '-' || b.refno) as "Membership number"
  , b.bd08x as "NINO"
  , (b.BD06A|| ' ' || b.BD38A) as name
  , b.BD11D as "Date of birth"
  , b.BD14D as "Joined Company"
  , b.BD15D as "Joined Scheme"
  , b.BD22D as "Service Qualified"
  , b.BD09A as "Gender"
  , s.psalary as "Pensionable Salary"
  , avc.total as "AVC fund"
  , aver.total as "AVER fund"
  , tvin1.days as "TVIN A days"
  , tvin2.days as "TVIN B days"
  , tvin3.days as "TVIN C days"
  , aug.days as "Augmentation days"
  , ltaamount.lta as "Lta"
  , cpiamount.cpi as "Cpi"
  , bankedegp.egp as "Banked EGP"
from basic b
  , (select mh.mh02d as effective, mh.mh03c as psalary
      from MISCELLANEOUS_HISTORY mh
        , (select max (mh02d) as ef
            from MISCELLANEOUS_HISTORY mh
            where mh.bgroup = :bgroup
              and mh.refno = :refno
              and mh.mh03c is not null) mhs
      where mh.bgroup = :bgroup
        and mh.refno = :refno
        and mh.mh02d = mhs.ef
      ) s
    , (SELECT nvl(sum(ah.AH10P), 0) as total
        FROM avc_history ah
        WHERE ah.bgroup = :bgroup
          AND ah.refno = :refno
          AND ah.AH01X in ('AVC', 'TVIN')
      ) avc
    , (SELECT nvl(sum(ah.AH10P), 0) as total
        FROM avc_history ah
        WHERE ah.bgroup = :bgroup
          AND ah.refno = :refno
          AND ah.AH01X in ('AVER')
      ) aver
    , (select nvl(sum(TI22I), 0) as days
        from transfer_in ti
        where bgroup= :bgroup
            and refno= :refno
            and ti.SUB in ('0010','0011','0012','0014','0015','0022','0023')
            and ti.TI22I > 0
            and ti.TI04D < '01-Oct-1986'
      ) tvin1
    , (select nvl(sum(TI22I), 0) as days
        from transfer_in ti
        where bgroup= :bgroup
            and refno= :refno
            and ti.SUB in ('0010','0011','0012','0014','0015','0022','0023')
            and ti.TI22I > 0
            and ti.TI04D >= '01-Oct-1986'
            and ti.TI04D < '01-Dec-2006'
      ) tvin2
    , (select nvl(sum(TI22I), 0) as days
        from transfer_in ti
        where bgroup= :bgroup
            and refno= :refno
            and ti.SUB in ('0010','0011','0012','0014','0015','0022','0023')
            and ti.TI22I > 0
            and ti.TI04D >= '01-Dec-2006'
      ) tvin3
    , (SELECT nvl(sum(ab.ab06i), 0) as days
        FROM augmentation_benefit ab
        WHERE bgroup = :bgroup
          AND refno = :refno
          AND ab.SUB in ('OLDN','NEWE','NEWF','NEWK','NEWL','OLDA','OLDB'
                        ,'OLDG','OLDM','OLDH','OLDN','NEWE','NEWF','NEWK'
                        ,'NEWL','OLDA','OLDB','OLDG','OLDM','OLDH')
      ) aug
    , (select trim(to_char(rf.REVFAC, '9999999999.99')) as lta
        from 
          (select to_number(to_char(sysdate, 'YYYY'),'9999') year
            from dual 
            where to_char(sysdate, 'MMDD') >= to_number('0406', '9999')
          union select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year
            from dual 
            where to_char(sysdate, 'MMDD') < to_number('0406', '9999')
          ) taxyear
          ,revfac rf
        where rf.FACTYPE = 'LTAA'
          and rf.YOE = taxyear.year
    ) ltaamount
    , (select trim(to_char(sf.COL12, '9999999999.99')) as cpi
        from 
          (select to_number(to_char(sysdate, 'YYYY'),'9999') year
            from dual 
            where to_char(sysdate, 'MMDD') >= to_number('0406', '9999')
          union select nvl(to_number(to_char(sysdate, 'YYYY'), '9999') - 1, 0) year
            from dual 
            where to_char(sysdate, 'MMDD') < to_number('0406', '9999')
          ) taxyear
          ,statfac sf
        where sf.FACTYPE = 'RPI'
          and to_number(to_char(sf.RELDATE, 'YYYY'),'9999') = taxyear.year
    ) cpiamount    
    , (SELECT max(egp) as egp FROM (SELECT nvl(md10c, 0) as egp
        FROM miscellaneous_detail
        WHERE bgroup = :bgroup
        AND refno = :refno
        union select 0 from dual
        )
    ) bankedegp
where b.bgroup = :bgroup
  and b.refno = :refno