select 
	sd.OL03D as "From" ,
	sd.OL04D as "To" , 
	sd.CA26X as "Category" , 
	cd.CA10T  as "Accrual", 
	cd.CA01U as "Contribution" 
from OLD_SCHEME_DETAIL sd, CATEGORY_DETAIL cd 
where sd.bgroup =:bgroup and sd.refno = :refno and sd.bgroup = cd.bgroup and sd.CA26X = cd.CA26X 
order by 1 desc