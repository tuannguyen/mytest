<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.opencms.file.*"%>
<%@ page import="org.opencms.jsp.*"%>
<%@ page import="org.opencms.security.*"%>
<%@ page import="java.util.*"%>
<%@ page import="org.opencms.main.*"%>
<%@ page import="org.opencms.search.*"%>
<%@ page import="com.bp.pensionline.constants.*"%>
<%@page import="com.bp.pensionline.webstats.producer.WebstatsPageProducer"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%
    response.setHeader("Cache-Control","no-cache, no-store"); //HTTP 1.1
    //response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0);
%>
<%		
	org.opencms.jsp.CmsJspActionElement cmsJspAction = new CmsJspActionElement(pageContext, request, response);
    
    // Get the search manager
    CmsSearchManager searchManager = OpenCms.getSearchManager(); 
    
	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request, response);
	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cms = bean.getCmsObject();
	// get the values from configuration file opencms.properties
	// Map map=cms.getConfigurations();
   	CmsUser user = cms.getRequestContext().currentUser();
   
	//Call Webstat producer	
	WebstatsPageProducer.runStats(request);
	
	String requestedResource = request.getParameter("requestedResource");
	request.getSession().setAttribute("requestedResource",requestedResource);
  
  	String uri=cmsLogin.getRequestContext().getUri();
	boolean isView = false;
	try {
		CmsObject obj = SystemAccount.getAdminCmsObject();
		obj.readProject("Online");
		CmsPermissionSet permissionSet = cms.getPermissions(uri, user.getName());
		String permissions = permissionSet.toString();
		String read = "+r";
		String view = "+v";

		if (permissions.indexOf(view) == -1) {
			isView = false;
		} else {
			isView = true;
		}

	} catch (Exception ex) {
		System.out.println(ex);
	
		isView = false;
	}

	//logged in?

	if (!isView && user.isGuestUser()){

		response.sendRedirect(cmsJspAction.link("/_login_ask.html"));
	}	
%>

<cms:template element="head">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><cms:property name="Title" escapeHtml="true" /></title>
	<meta http-equiv="Cache-Control" content="no-cache, no-store">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Content-Type" content="text/xml; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
	<script type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.template/resources/scripts/browser.js</cms:link>"></script>
<script type="text/javascript" language="javascript">
	// IE7 is the development platform of choice
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version >= "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.css</cms:link>' type='text/css' media='screen, projection' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.ie.css</cms:link>' type='text/css' media='print' />");

	}
	// check if IE6 is detected, and degrade some features
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.ie6.css</cms:link>' type='text/css' media='screen, projection' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.ie6.css</cms:link>' type='text/css' media='print' />");
	
	}
	// check if Firefox is detected
	if( BrowserDetect.browser == "Firefox" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.firefox.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.css</cms:link>' type='text/css' media='print' />");                
	        
	}	
	
	if( BrowserDetect.browser == "Chrome"||BrowserDetect.browser == "Mozilla" ) {
	        document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.chrome.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.css</cms:link>' type='text/css' media='print' />");    
	            
         }

    // safari
	if( BrowserDetect.browser == "Safari") {
        document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.safari.css</cms:link>' type='text/css' media='screen, projection, print' />");
            document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.css</cms:link>' type='text/css' media='print' />");    
            
     }    
var ajaxurl="/content";
var bgroup = "XXX";
var crefno = "0000000";
</script>
<link rel="stylesheet" type="text/css" media="all" href="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/aataxmodeller.css</cms:link>" title="aa tax modeller" />
<link rel="stylesheet" type="text/css" media="print" href="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/aamodeller.print.css</cms:link>"  type='text/css'/>
<%
	if (cms.getRequestContext().currentProject().isOnlineProject())
	{	
%>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/prototype.js</cms:link>"></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/modeller-slider.js</cms:link>"></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/HelpBalloon.js</cms:link>"></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/AnnualAllowanceCommon.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/AnnualAllowanceMember.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/AjaxDataTagHandler.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/AjaxDataTagParser.js</cms:link>" ></script>
<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/scripts/jquery-1.4.4.js</cms:link>"></script>
<script>
	var $j = jQuery.noConflict();     
</script>
</head>
<body  onload="initAATables();">
<%
	}
	else
	{
%>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.js" ></script>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.dimensions.js" ></script>
<script language="javascript" type="text/javascript" src="/content/resources/jquery/unpacked/jquery.flydom.js" ></script>
<cms:editable />
</head>
<body>
<%
	}
%>

<div id='container'>
<%
	org.opencms.db.CmsLoginMessage loginMessage = org.opencms.main.OpenCms.getLoginManager().getLoginMessage();
	if ((loginMessage != null) && (loginMessage.isActive())) 
	{	
%>
<div style="float: left; width: 100%;">
	<cms:include file="/_lockout_warning.html" element="bpblank"></cms:include>
</div>
<%		
	}
%>
<div id='headernav' class='non-print'>
<%
	if (!cms.getRequestContext().currentProject().isOnlineProject())
	{	
		if (cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) || 
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_REVIEWER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_AUTHORISER_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_DEPLOYER_GROUP)
		){
%>

	<a href="<cms:link>/publishing/index.html</cms:link>">Recent changes</a> &nbsp; | &nbsp;
<%	
		}
		if (cms.userInGroup(user.getName(),"Superusers")){
%>
	<a href="<cms:link>/_logout_publishing.jsp</cms:link>">Exit publishing</a> &nbsp; | &nbsp; 	
<%
		}	
		if (cms.userInGroup(user.getName(),"Authors") ||
			cms.userInGroup(user.getName(),"Publishers") ||
			cms.userInGroup(user.getName(),"Testers") ||		
			cms.userInGroup(user.getName(),"Administrators") ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
			cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
			) {
%>
	<a href="" onclick="popupHelp('https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page'); return false;">online help</a> &nbsp; | &nbsp;
<%
		}
		if (cms.userInGroup(user.getName(),"Authors") ||
				cms.userInGroup(user.getName(),"Publishers") ||
				cms.userInGroup(user.getName(),"Administrators") ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
				)
		{
%>
		<a href="<cms:link>/system/workplace/views/workplace.jsp</cms:link>">Workplace</a> &nbsp; | &nbsp;
<%
		}		
	}
	else
	{	
		if (cms.userInGroup(user.getName(),"Superusers")){
%>
	<a href="" onclick="popupHelp('https://ukpensionsasp.bp.com:4430/support/wiki/index.php?title=Main_Page'); return false;">online help</a> &nbsp; | &nbsp;
	<a href="<cms:link>/superuser/index.html</cms:link>">Change member</a> &nbsp; | &nbsp;
	<a class="fakelink" onclick="document.getElementById('bookmark_display').style.display='block';">Bookmark member</a> &nbsp; | &nbsp; 	
<%
		}

		if (cms.userInGroup(user.getName(),"Authors") ||
				cms.userInGroup(user.getName(),"Publishers") ||
				cms.userInGroup(user.getName(),"Administrators") ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP)
				)
		{
	%>
	<a href="<cms:link>/system/workplace/views/workplace.jsp</cms:link>">Workplace</a> &nbsp; | &nbsp;
	<%
		}				
		if (cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_GROUP) || 
				cms.userInGroup(user.getName(), Environment.PUBLISHING_EDITOR_REVIEWER_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_REVIEWER_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_AUTHORISER_GROUP) ||
				cms.userInGroup(user.getName(), Environment.PUBLISHING_DEPLOYER_GROUP)
			)
		{
%>	
	<a href="<cms:link>/_login_publishing.jsp</cms:link>">Enter publishing</a> &nbsp; | &nbsp;
<%
		}
	}
%>
  <a href="<cms:link>/contactus.html</cms:link>">contact us </a>
</div> <!-- headernav -->

<div id='header'>
 	<div class='logoholder'>
          <img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/logo.gif</cms:link>' alt='BP logo'/>
        </div>
  	<div class='bannerholder'>
  	  <img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/pensionline_banner.gif</cms:link>' alt='pensionline logo'/>
  	</div>
</div> <!-- header -->
 
<div id='bannernav' class='non-print'>
  	<cms:include file="/system/modules/com.bp.pensionline.template/elements/headerTabs.jsp"/>
</div> <!-- banernav -->

<div id='outer'>
  	<div id='crumbtrail'>
    	<cms:include file="/system/modules/com.bp.pensionline.template/elements/breadcrumb.jsp"/>
  	</div> <!-- crumbtrail -->

  	<!-- left hand sidebar -->
  	<div id='sidebar' class='lefty non-print'>
		<div id='menu' class='lefty'>
			<cms:include file="/system/modules/com.bp.pensionline.template/elements/sidebar.jsp"/>
		</div>
	<div id='rss'>
		<ul>     
			<li><img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/rss.gif</cms:link>' alt='RSS icon' /> <a href="<cms:link>/RSS/newsRSS.xml</cms:link>">PensionLine news</a></li>
<!--
			<li><img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/rss.gif</cms:link>' alt='RSS icon' /> <a href="<cms:link>/RSS/eventsRSS.xml</cms:link>">PensionLine events</a></li>
-->
		</ul>
	</div>
	<div id='rss_explanation'>
		<ul>
			<li><img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/link_arrow.gif</cms:link>' alt='link arrow' /> <a href="<cms:link>/rss.html</cms:link>">what is RSS?</a></li>
		</ul>
	</div>
  	</div>
  
	<div id='content'>
 
	</cms:template>

	<cms:template element="body">
		<cms:template ifexists="bpblank">
		<div id='blank_template'>
  			<div id='pagetitle'>
				<cms:include element="bptitle" editable="true" />
			</div>
			<div id='pagebody'>
				<cms:include element="bpblank" editable="true" />
			</div>		
		</div>
	   	</cms:template>
	</cms:template>	

	<cms:template element="foot">

 	</div> <!-- content -->
</div> <!-- outer -->

<div id='printfooter'>
  <span class='fakelink'>&copy; 2006 BP p.l.c.</span>
  </div> <!-- printfooter -->

  <div id='footer' class='non-print'>

    <div id='tomorrowtoday_tag'>
	<img src='<cms:link>/system/modules/com.bp.pensionline.template/resources/gfx/tomorrowtoday_small.gif</cms:link>' alt='tomorrow today logo' />
    </div> <!-- owner -->

    <div id='footernav'>
<%
SimpleDateFormat year_formatter = new SimpleDateFormat("yyyy");
String this_year = year_formatter.format(new Date());
%>
   <span class='fakelink'>&copy; 2006-<%= this_year %> BP p.l.c.</span> &nbsp; | &nbsp;
<a href="<cms:link>/legalnotice.html</cms:link>">legal notice </a>&nbsp; | &nbsp;
<a href="<cms:link>/privacy.html</cms:link>">privacy statement </a>
   </div> <!-- footernav -->

  </div> <!-- footer -->

</div> <!-- container -->
<!--  
<div class="model_dialog" id="salary_slider_panel">
	<div class="model_dialog_inner">
	<div class="model_dialog_title">Pensionable salary</div>
	<div class="model_dialog_content">
		<div style="float: left; width: 190px; padding-top: 10px;">
			<input id="input_salary" class="slider_value" name="input_salary" type="text" style="width: 114px;"/>&nbsp;&nbsp;
			<div id="salary_slider">&nbsp;</div>
			<a href="" onclick="hideSalarySlider(); return false;">Cancel</a>
		</div>
		<div style="float: left; width: 120px; vertical-align: middle;">
			<a href="" onclick="modelSalary(); return false;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/model_button.png</cms:link>"></img></a>
		</div>		
	</div>
	</div>
</div>
-->
<!--
<div class="model_dialog" id="taxrate_slider_panel">
	<div class="model_dialog_inner">
	<div class="model_dialog_title">Marginal tax rate</div>
	<div class="model_dialog_content">
		<div style="float: left; width: 190px; padding-top: 10px">
		<input id="input_taxrate" class="slider_value" name="input_taxrate" type="text" style="width: 114px;"/>&nbsp;&nbsp;
		 <div id="taxrate_slider">&nbsp;</div> 
			<a href="" onclick="hideTaxrateSlider(); return false;">Cancel</a>&nbsp;
		</div>
		<div style="float: left; width: 120px; vertical-align: middle;">
			<a href="" onclick="modelTaxrate(); return false;"><img src="<cms:link>/system/modules/com.bp.pensionline.aataxmodeller/resources/gfx/model_button.png</cms:link>"></img></a>
		</div>		
	</div>
	</div>
</div>
-->
<div class="dialog" id="bookmark_display">
      <div class="dialog_title">Bookmark member</div>
      <div class="component_holder">
      <div class="component_label"><label for="refpage">Referring page</label> </div>
      <div class="component_value"><input class="dialog_text" id="refpage" name="refpage" type="text" readonly value='Details here'/> </div>
      </div>
      <div class="component_holder">
      <div class="component_label"><label for="description">Description</label> </div>
      <div class="component_value"><input class="dialog_text" id="description" name="description" type="text" /> </div>
      </div>
      <div class="component_holder">
      <div class="component_label"> </div>
      <div class="component_value">
         <input class="goButton" id="dobookmark" type="button" name="dobookmark" value="Go" onclick="HandleBookmarkAjax(document.getElementById('description').value, document.getElementById('refpage').value);" />
         &nbsp;
         <input class="goButton" id="cancelbookmark" type="button" value="Cancel" onclick="document.getElementById('bookmark_display').style.display='none';" /> </div>
      </div>
  </div>
<div id="DIVTEMPLATE"></div>
<cms:include file="/system/modules/com.bp.pensionline.template/elements/publishing_toolbar.jsp"/>
</body>
</html>
</cms:template>