/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "MemberValue Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.
FCKCommands.RegisterCommand( 'InsertMemberValue', new FCKDialogCommand( 'InsertMemberValue', FCKLang.InsertMemberValueDlgTitle, FCKPlugins.Items['InsertMemberValue'].Path + 'fck_InsertMemberValue.jsp', 350, 200 ) ) ;

// Create the "Plaholder" toolbar button.
var oInsertMemberValueItem = new FCKToolbarButton( 'InsertMemberValue', FCKLang.InsertMemberValueBtn ) ;
oInsertMemberValueItem.IconPath = FCKPlugins.Items['InsertMemberValue'].Path + 'InsertMemberValue.png' ;

FCKToolbarItems.RegisterItem( 'InsertMemberValue', oInsertMemberValueItem ) ;


// The object used for all InsertMemberValue operations.
var FCKInsertMemberValue = new Object() ;

// Add a new InsertMemberValue at the actual selection.
FCKInsertMemberValue.Add = function( valueName)
{
	var oSpan = FCK.CreateElement( 'SPAN' ) ;
	this.SetupMemberValue( oSpan, valueName) ;
}

FCKInsertMemberValue.SetupMemberValue = function( span, valueName)
{
	if (!valueName || valueName=="") return false;
	
	span.innerHTML = '[[' + valueName + ']]';

	if ( FCKBrowserInfo.IsGecko )
		span.style.cursor = 'default' ;

	span._fckInsertMemberValueId = generateNextID('_fckMemberValue');
	span.contentEditable = 'false';			

	// To avoid it to be resized.
	span.onresizestart = function()
	{
		FCK.EditorWindow.event.returnValue = false ;
		return false ;
	}
}

