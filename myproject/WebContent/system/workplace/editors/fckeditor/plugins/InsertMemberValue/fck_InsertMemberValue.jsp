<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@ page import="
	org.opencms.jsp.*,
	org.opencms.main.*,
	org.opencms.file.*"
%><%

	CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
	//get Admin permission to read file from VFS folder
	CmsObject cmsObj = cms.getCmsObject();
	cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
	cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
	
	byte[] arr = null;
	try{
		String filename = "/system/workplace/editors/fckeditor/plugins/InsertMemberValue/memberProperties.xml";
		boolean exist = cmsObj.existsResource(filename);
		
		//check if the file is exist or not
		if(exist){//if true
			
			//read file and return a byte array
			CmsFile xmlFile = (CmsFile)cmsObj.readFile(filename);		
			arr = xmlFile.getContents();			
		}
	}
	catch(Exception ex){

	}

%>
<%
	TreeMap memberValues = null;
	
	
	
	if (arr != null)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(arr);
			document = builder.parse(is);

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		Element root = document.getDocumentElement();

		NodeList nodeList = root.getChildNodes();
		
		int nlength = nodeList.getLength();
		Map tmpMap = new HashMap();
		for (int i = 0; i < nlength; i++)
		{
			String property = "";
			String value = "";			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				property = node.getNodeName();
				value = node.getFirstChild().getNodeValue();
				tmpMap.put(property, value);
			}

		}	
		
		memberValues = new TreeMap(tmpMap); 
		
	}
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 *
 *
 * File Name: fck_InsertMemberValue.html
 * 	MemberValue Tag Plugin.
 *
 * File Authors:
 * 		Huy Tran (huytran@c-mg.net)
-->
<html>
	<head>
		<title>Member Value Selection</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="noindex, nofollow" name="robots">
		<script language="javascript">

var oEditor = window.parent.InnerDialogLoaded() ;
var FCKLang = oEditor.FCKLang ;
var FCKInsertMemberValue = oEditor.FCKInsertMemberValue;

window.onload = function ()
{
	// First of all, translate the dialog box texts
	oEditor.FCKLanguageManager.TranslatePage( document ) ;

	LoadSelected() ;

	// Show the "Ok" button.
	window.parent.SetOkButton(true) ;
}

var eSelected = oEditor.FCKSelection.GetSelectedElement() ;

function LoadSelected()
{
	if ( !eSelected )
		return ;

	if ( eSelected.tagName.toLowerCase() == 'span' && eSelected._fckInsertMemberValueId )
	{
		var spanTextValue = eSelected.innerHTML;
		document.getElementById('memberValue').value = getMemberValue(spanTextValue);
	}
	else
	{
		eSelected == null ;
	}
}

function Ok()
{
	var memberValue 	= document.getElementById('memberValue').value ;
	
	if ( memberValue.length == 0)
	{
		alert( FCKLang.InsertMemberValueErrNoName ) ;
		return false ;
	}
	
	FCKInsertMemberValue.Add( memberValue) ;
	return true ;
}

function getMemberValue(innerHTML)
{
	var startMemberValue= "[[";
	var stopMemberValue="]]";
	
	// To keep the changes at minimum, table tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startMemberValuePos = innerHTML.indexOf(startMemberValue);	
	var endMemberValuePos = innerHTML.indexOf(stopMemberValue);
	if(startMemberValuePos >= 0 && endMemberValuePos > startMemberValuePos)
	{
		var memberValue = innerHTML.substring(startMemberValuePos + 2, endMemberValuePos);
		return memberValue;
	}	
	
	return "";
}

function noWhiteSpace(s)
{
	if((s==null)||(typeof(s)!='string')||!s.length)return'';return s.replace(/\s+/g,'');
}
		</script>
	</head>
	<body scroll="no" style="OVERFLOW: hidden">
		<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td>
		 			<table cellSpacing="0" cellPadding="0" align="center" border="0" width="100%" height="100%">
						<tr>
							<td align="left" width="40%">Member Value:</td>
							<td align="left">
								<select id="memberValue">
								<% 
									Set keySet = memberValues.keySet();
									Iterator keyIterator = keySet.iterator();	
									while (keyIterator.hasNext()){
										String key = (String)keyIterator.next();
										String displayValue = (String)memberValues.get(key);
								%>
									<option value="<%= key %>"><%= displayValue %></option>
								<%
									}
								%>
								</select>
							</td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>