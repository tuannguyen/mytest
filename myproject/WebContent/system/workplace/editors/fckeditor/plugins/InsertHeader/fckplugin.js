/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Headeral Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.

FCKCommands.RegisterCommand( 'InsertHeader', new FCKDialogCommand( 'InsertHeader', FCKLang.InsertHeaderDlgTitle, FCKPlugins.Items['InsertHeader'].Path + 'fck_InsertHeader.jsp', 620, 360) ) ;

// Create the "Plaholder" toolbar button.
var oInsertHeaderItem = new FCKToolbarButton( 'InsertHeader', FCKLang.InsertHeaderBtn ) ;
oInsertHeaderItem.IconPath = FCKPlugins.Items['InsertHeader'].Path + 'InsertHeader.png' ;

FCKToolbarItems.RegisterItem( 'InsertHeader', oInsertHeaderItem ) ;


// The object used for all InsertHeader operations.
var FCKInsertHeader = new Object() ;

// the element that <span> Headeral tag </span> applies
var appliedElement = null;

// Add a new InsertHeader at the actual selection.
FCKInsertHeader.Add = function(){
	//alert('Here');
	var oP = FCK.CreateElement( 'P' ) ;
	var oSpan = FCK.CreateElement( 'SPAN' ) ;
	oSpan.innerHTML = '((--LetterHeader--))';
	oSpan._fckInsertHeaderId = 'header';
	//alert("configuring");
	if ( FCKBrowserInfo.IsGecko )
		oSpan.style.cursor = 'default' ;
	oSpan.contentEditable = 'false';
	//alert("done");	
}

// On Gecko we must do this trick so the user select all the SPAN when clicking on it.
FCKInsertHeader._SetupClickListener = function(){
	alert('listener');
	FCKInsertHeader._ClickListener = function( e ){
		if ( e.target.tagName.toLowerCase() == 'span' && e.target._fckInsertHeaderId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}						
	}

	FCK.EditorDocument.addEventListener( 'click', FCKInsertHeader._ClickListener, true ) ;
}

// Open the dialog on double click. This function is shared with other plugins which use element for its place holder
FCKInsertHeader.OnDoubleClick = function( element )
{	//alert("dbl click");
	if ( element.tagName.toLowerCase() == 'span' && element._fckInsertHeaderId)
	{
		appliedElement = findNextElement(element);	// for Headeral tag only			
		FCKCommands.GetCommand( 'InsertHeader' ).Execute() ;	
		return;
	}		
}

FCK.RegisterDoubleClickHandler( FCKInsertHeader.OnDoubleClick, 'DIV' );
FCK.RegisterDoubleClickHandler( FCKInsertHeader.OnDoubleClick, 'SPAN' );