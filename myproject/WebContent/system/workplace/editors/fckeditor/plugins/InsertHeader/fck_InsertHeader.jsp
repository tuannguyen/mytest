<%@ page session="true" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.opencms.jsp.*, org.opencms.main.*, org.opencms.file.*"%>
<%@page import="com.bp.pensionline.letter.constants.Constants"%>
<%@page import="com.bp.pensionline.letter.constants.IconFile"%>
<%@page import="com.bp.pensionline.letter.constants.HeaderLine"%>
<%@page import="com.bp.pensionline.letter.util.HeaderXmlDaoUtil"%>
<%@page import="com.bp.pensionline.util.SystemAccount"%>
<%@page import="org.opencms.file.CmsObject"%>
<%@page import="org.opencms.file.CmsFile"%>
<%@page import="org.opencms.main.OpenCms"%>
<%@page import="java.util.*"%>
<%@page import="java.io.ByteArrayInputStream"%>

<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
HeaderXmlDaoUtil headerUtil = new HeaderXmlDaoUtil("/system/modules/com.bp.pensionline.letter/header_setting.xml");
List headerLines = headerUtil.getHeaderLines();
String headerPros = "/system/modules/com.bp.pensionline.letter/header.properties";
Properties pros = new Properties();
CmsObject cmsObj = SystemAccount.getAdminCmsObject();
cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
boolean exist = cmsObj.existsResource(headerPros);
if(exist){//if true
	CmsFile xmlFile = (CmsFile)cmsObj.readFile(headerPros);		
	byte[] arr = xmlFile.getContents();
	ByteArrayInputStream bIn = new ByteArrayInputStream(arr);
	pros.load(bIn);
}
String headerStr = pros.getProperty("header");	
String[] headers = headerStr.split(",");		

int numLine = headerLines.size();
int numHead = headers.length;
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 *
 *
 * File Name: fck_InsertCondition.html
 * 	Conditional Tag Plugin.
 *
 * File Authors:
 * 		Huy Tran (huytran@c-mg.net)
-->
<html>
	<head>
		<title>Setting up signature</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="noindex, nofollow" name="robots">
		<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/xmldom.js</cms:link>"></script>
        
		<script language="javascript">
var oEditor = window.parent.InnerDialogLoaded() ;
var FCKLang = oEditor.FCKLang ;
var FCKInsertHeader = oEditor.FCKInsertHeader;

window.onload = function () {
	// Show the "Ok" button.
	window.parent.SetOkButton(true) ;
}

var request = "<%=request.getRequestURI()%>";
function Ok() {
	var form = document.forms[0];
	var params = "";
	for (var i=0; i<headerSet.length; i++) {
		var param = "";
		var line = headerSet[i];
		for (var j=0; j<line.length; j++) {
			if (line[j]==1) {
				param += ","+headers[j];
			}
		}
		params += "&line"+i+"="+param.substring(1);
	}
	var action = form.action;
	action += "?"+params.substring(1);
	//alert(action);
	form.action = action;
	form.submit();
	var eSelected = oEditor.FCKSelection.GetSelectedElement() ;
	if (!eSelected) {
		FCKInsertHeader.Add() ;
	}
	return true;
}

var headers = new Array();
<%for (int i=0; i<numHead; i++) {%>
	headers.push('<%=headers[i]%>');
<%}%>
var headerSet = new Array();
<%for (int i=0; i<numLine; i++) {%>
	var headerLineVal = "<%=((HeaderLine)headerLines.get(i)).getValue()%>";
	var headerLine = new Array();
	for (var i=0; i<headers.length; i++) {
		if (headerLineVal.indexOf(headers[i]) != -1) {
			headerLine.push(1);
		} else {
			headerLine.push(0);
		}
	}
	headerSet.push(headerLine);
<%}%>

var selectedLine = null;
function drawHeaderSetting() {
	//alert(headerSet.length);
	var i,j;
	for (i=0; i<headerSet.length; i++) {
		var id="line"+i;
		var obj = document.getElementById(id);
		var lineiArr = headerSet[i];
		var str="";
		for (j=0; j<headers.length; j++) {
			if (lineiArr[j]==1) {
				str += "[["+headers[j]+"]] ";
			}
		}
		if (obj) {
			obj.innerHTML = str;
		}
	}	
}

function chooseHeaderLine(obj) {
	if (obj) {
		selectedLine = obj.value;
		var lineValues = headerSet[selectedLine];
		var headerCheckbox = document.getElementsByName("headerCheck");
		for (var i=0; i<lineValues.length; i++) {
			if (lineValues[i]==1) {
				headerCheckbox[i].checked = true;
			} else {
				headerCheckbox[i].checked = false;
			}
		}
	} 
}

function checkHeaderBox(obj) {
	if(obj && selectedLine) {
		var lineValues = headerSet[selectedLine];
		var checkedValue = obj.value;
		//alert(checkedValue);
		for (var i=0; i<headers.length; i++) {
			if (checkedValue == headers[i]) {
				if (obj.checked) {
					lineValues[i]=1;
				} else {
					lineValues[i]=0;
				}
			}
		}
		drawHeaderSetting();
	}
}
		</script>
    <style>
/*signature upload css*/
.signature_file {
	height: 20px;
}
	</style>
	</head>
<body scroll="yes" style="OVERFLOW: scroll" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	 			<!--<table cellSpacing="0" cellPadding="0" align="center" border="1" width="100%" height="100%" bgcolor="#FFFFCC">-->
                <table cellSpacing="0" cellPadding="0" align="center" border="0" width="100%" height="100%">
                   	<colgroup>
						<col width="30%">
                        <col width="70%">
					</colgroup>
                    <tr><td><b>Available header items:</b></td>
                    	<td><b>Current header setting:</b></td>
                    </tr>
                    <tr><td><table id="headerDetails">
                    		<%for (int i=0; i<numHead; i++) {%>
                            <tr><td>
                            	<input type="checkbox" name="headerCheck" value="<%=headers[i]%>" onClick="checkHeaderBox(this)"><%=headers[i]%>
							</td></tr>
                            <%}%>
                    	</table></td>
                        <td style="padding-top: 25"><div style="width: 100%; height:90%; overflow-x:auto;overflow-y:auto; border:thin solid #999999;"><table id="headerForm" style=" width:100%; height:100%; background-color:white; text-align:left;overflow-x:auto;overflow-y:auto;" cellpadding="2" cellspacing="2">
                        	<colgroup>
                            	<col width="10%">
                                <col width="90%">
                            </colgroup>
                        	<%for (int i=0; i<numLine; i++) {%>
                            	<tr><td style="width:auto"><input type="radio" name="lineRdo" value="<%=i%>" onClick="chooseHeaderLine(this)"/></td>
                                	<td id="line<%=i%>"></td>
                                </tr>
                            <%}%>
                        </table></div></td>
                    </tr>
			    </table>
</body>
<form id="form" action="/content/pl/system/workplace/editors/fckeditor/plugins/InsertHeader/saveHeaderSetting.jsp" method="post">
</form>
<script language="javascript">
drawHeaderSetting();
</script>
</html>