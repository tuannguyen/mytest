/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Table Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.
var FCKInsertPagebreak_command = new Object();
FCKInsertPagebreak_command.name = 'InsertPagebreak';
FCKInsertPagebreak_command.Execute = function() {
	var oDiv = FCK.CreateElement('P') ;
	oDiv.innerHTML = '((--PageBreak--))';
	oDiv.contentEditable = 'false';
}
FCKInsertPagebreak_command.GetState = function() {
	// Let's make it always enabled.
	return FCK_TRISTATE_OFF ;
}

FCKCommands.RegisterCommand( 'InsertPagebreak', FCKInsertPagebreak_command) ;

// Create the "Plaholder" toolbar button.
var oInsertPagebreakItem = new FCKToolbarButton( 'InsertPagebreak', FCKLang.InsertPagebreak) ;
oInsertPagebreakItem.IconPath = FCKPlugins.Items['InsertPagebreak'].Path + 'page_break.png' ;

FCKToolbarItems.RegisterItem( 'InsertPagebreak', oInsertPagebreakItem ) ;


// The object used for all InsertPagebreak operations.