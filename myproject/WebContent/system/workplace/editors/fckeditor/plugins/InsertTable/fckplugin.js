/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Table Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.
FCKCommands.RegisterCommand( 'InsertTable', new FCKDialogCommand( 'InsertTable', FCKLang.InsertTableDlgTitle, FCKPlugins.Items['InsertTable'].Path + 'fck_InsertTable.jsp', 350, 200 ) ) ;

// Create the "Plaholder" toolbar button.
var oInsertTableItem = new FCKToolbarButton( 'InsertTable', FCKLang.InsertTableBtn ) ;
oInsertTableItem.IconPath = FCKPlugins.Items['InsertTable'].Path + 'InsertTable.png' ;

FCKToolbarItems.RegisterItem( 'InsertTable', oInsertTableItem ) ;


// The object used for all InsertTable operations.
var FCKInsertTable = new Object() ;

// Add a new InsertTable at the actual selection.
FCKInsertTable.Add = function( tableName, message)
{
	var oDiv = FCK.CreateElement( 'DIV' ) ;
	this.SetupTable( oDiv, tableName, message) ;
	
	finalizeTableTag(oDiv);
	appliedElement = null;	
}

FCKInsertTable.SetupTable = function( div, tableName, message)
{
	div.innerHTML = '((Table(' + tableName + '):' + message + '))';

	if ( FCKBrowserInfo.IsGecko )
		div.style.cursor = 'default' ;

	div._fckInsertTableId = generateNextID('_fckTable');
	div.contentEditable = 'false';			
	div.style.backgroundColor	= "#eeeeee";

	// To avoid it to be resized.
	div.onresizestart = function()
	{
		FCK.EditorWindow.event.returnValue = false ;
		return false ;
	}
}

function finalizeTableTag (div)
{
	if(div != null)
	{				
		if (appliedElement)
		{
			var parent = appliedElement.parentNode;
			var oldAppliedElement = appliedElement;
			parent.insertBefore(div, appliedElement);
			parent.insertBefore(appliedElement, div);
			//parent.removeChild(oldAppliedElement);				
		}
	}
}
