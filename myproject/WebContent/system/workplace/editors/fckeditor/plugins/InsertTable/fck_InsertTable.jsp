<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@ page import="
	org.opencms.jsp.*,
	org.opencms.main.*,
	org.opencms.file.*"
%><%

	CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
	//get Admin permission to read file from VFS folder
	CmsObject cmsObj = cms.getCmsObject();
	cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
	cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
	
	byte[] arr = null;
	try{
		String filename = "/system/workplace/editors/fckeditor/plugins/InsertTable/memberProperties.xml";
		boolean exist = cmsObj.existsResource(filename);
		
		//check if the file is exist or not
		if(exist){//if true
			
			//read file and return a byte array
			CmsFile xmlFile = (CmsFile)cmsObj.readFile(filename);		
			arr = xmlFile.getContents();			
		}
	}
	catch(Exception ex){

	}

%>
<%
	TreeMap memberValues = null;
	
	
	
	if (arr != null)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(arr);
			document = builder.parse(is);

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		Element root = document.getDocumentElement();

		NodeList nodeList = root.getChildNodes();
		
		int nlength = nodeList.getLength();
		Map tmpMap = new HashMap();
		for (int i = 0; i < nlength; i++)
		{
			String property = "";
			String value = "";			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				property = node.getNodeName();
				value = node.getFirstChild().getNodeValue();
				tmpMap.put(property, value);
			}

		}	
		
		memberValues = new TreeMap(tmpMap); 
		
	}	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 *
 *
 * File Name: fck_InsertTable.html
 * 	Table Tag Plugin.
 *
 * File Authors:
 * 		Huy Tran (huytran@c-mg.net)
-->
<html>
	<head>
		<title>Table Tag Properties</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="noindex, nofollow" name="robots">
		<script language="javascript">

var oEditor = window.parent.InnerDialogLoaded() ;
var FCKLang = oEditor.FCKLang ;
var FCKInsertTable = oEditor.FCKInsertTable;

window.onload = function ()
{
	// First of all, translate the dialog box texts
	oEditor.FCKLanguageManager.TranslatePage( document ) ;

	LoadSelected() ;

	// Show the "Ok" button.
	window.parent.SetOkButton(true) ;
}

var eSelected = oEditor.FCKSelection.GetSelectedElement() ;

function LoadSelected()
{
	if ( !eSelected )
		return ;

	if ( eSelected.tagName.toLowerCase() == 'div' && eSelected._fckInsertTableId )
	{
		var divTextValue = eSelected.innerHTML;
		var table = getTableTag(divTextValue);
		document.getElementById('tableName').value = getTableName(table);		
		document.getElementById('tableMessage').value = getMessage(table);
	}
	else
	{
		eSelected == null ;
	}
}

function Ok()
{
	var tableName 		= document.getElementById('tableName').value ;
	var tableMessage 	= document.getElementById('tableMessage').value ;
	if (tableMessage == null || tableMessage == "") tableMessage = "Waiting...";
	
	if ( tableName.length == 0)
	{
		alert( FCKLang.InsertTableErrNoName ) ;
		return false ;
	}
	
	FCKInsertTable.Add( tableName, tableMessage ) ;
	return true ;
}

function getTableTag(innerHTML)
{
	var startTable= "((";
	var stopTable="))";
	
	// To keep the changes at minimum, table tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startTablePos = innerHTML.indexOf(startTable);	
	var endTablePos = innerHTML.indexOf(stopTable);
	if(startTablePos >= 0 && endTablePos > startTablePos)
	{
		var metaString = innerHTML.substring(startTablePos + 2, endTablePos);
		var tablePos = metaString.toLowerCase().indexOf("table(");	
		if(tablePos >= 0)
		{
			return metaString;
		}
	}	
	
	return "";
}

function getTableName(table)
{
	var startValue = "(";
	var stopValue = ")";
	
	// To keep the changes at minimum, table tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startValuePos = table.indexOf(startValue);
	var endValuePos = table.indexOf(stopValue);
	if(startValuePos >= 0 && endValuePos > startValuePos)
	{
		var value = table.substring(startValuePos + startValue.length, endValuePos);
		//alert('table name: ' + value);
		return value;
	}
	
	return "";
}

function getMessage(table)
{
	var startPos = table.lastIndexOf(':')
	if (startPos > 0) {
		return table.substring(startPos + 1);
	}
	
	return "";
}

function noWhiteSpace(s)
{
	if((s==null)||(typeof(s)!='string')||!s.length)return'';return s.replace(/\s+/g,'');
}
		</script>
	</head>
	<body scroll="no" style="OVERFLOW: hidden">
		<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td>
		 			<table cellSpacing="0" cellPadding="0" align="center" border="0" width="100%" height="100%">
						<tr>
							<td align="left" width="40%">Create Table For:</td>
							<td align="left">
								<select id="tableName">
								<% 
									Set keySet = memberValues.keySet();
									Iterator keyIterator = keySet.iterator();	
									while (keyIterator.hasNext()){
										String key = (String)keyIterator.next();
										String displayValue = (String)memberValues.get(key);
								%>
									<option value="<%= key %>"><%= displayValue %></option>
								<%
									}
								%>
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" width="40%">Message:</td>
							<td align="left">
								<input id="tableMessage" type="text" maxlength="40" size="42">
							</td>						
						</tr>	
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>