<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@ page import="
	org.opencms.jsp.*,
	org.opencms.main.*,
	org.opencms.file.*"
%><%

	CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
	//get Admin permission to read file from VFS folder
	CmsObject cmsObj = cms.getCmsObject();
	cmsObj.getRequestContext().setSiteRoot(OpenCms.getSiteManager().getDefaultSite().getSiteRoot());
	cmsObj.getRequestContext().setCurrentProject(cmsObj.readProject("Offline"));
	
	byte[] arr = null;
	try{
		String filename = "/system/workplace/editors/fckeditor/plugins/InsertCondition/memberProperties.xml";
		boolean exist = cmsObj.existsResource(filename);
		
		//check if the file is exist or not
		if(exist){//if true
			
			//read file and return a byte array
			CmsFile xmlFile = (CmsFile)cmsObj.readFile(filename);		
			arr = xmlFile.getContents();			
		}
	}
	catch(Exception ex){

	}

%>
<%
	TreeMap memberValues = null;
	
	
	
	if (arr != null)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		try
		{
			builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(arr);
			document = builder.parse(is);

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		Element root = document.getDocumentElement();

		NodeList nodeList = root.getChildNodes();
		
		int nlength = nodeList.getLength();
		Map tmpMap = new HashMap();
		for (int i = 0; i < nlength; i++)
		{
			String property = "";
			String value = "";			
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				property = node.getNodeName();
				value = node.getFirstChild().getNodeValue();
				tmpMap.put(property, value);
			}

		}	
		
		memberValues = new TreeMap(tmpMap); 
		
	}
	
	byte[] noteArr = null;
	String note = "&nbsp;";
	try{
		String noteFile = "/system/workplace/editors/fckeditor/plugins/InsertCondition/notes.txt";
		boolean noteExist = cmsObj.existsResource(noteFile);
		
		//check if the file is exist or not
		if(noteExist){//if true
			
			//read file and return a byte array
			CmsFile xmlFile = (CmsFile)cmsObj.readFile(noteFile);		
			noteArr = xmlFile.getContents();
			note = new String(noteArr);
		}
	}
	catch(Exception ex){
	}	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 *
 *
 * File Name: fck_InsertCondition.html
 * 	Conditional Tag Plugin.
 *
 * File Authors:
 * 		Huy Tran (huytran@c-mg.net)
-->
<html>
	<head>
		<title>Conditional Tag Properties</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="noindex, nofollow" name="robots">
		<script language="javascript">

var oEditor = window.parent.InnerDialogLoaded() ;
var FCKLang = oEditor.FCKLang ;
var FCKInsertCondition = oEditor.FCKInsertCondition;

window.onload = function ()
{
	// First of all, translate the dialog box texts
	oEditor.FCKLanguageManager.TranslatePage( document ) ;

	LoadSelected() ;

	// Show the "Ok" button.
	window.parent.SetOkButton(true) ;
}

var eSelected = oEditor.FCKSelection.GetSelectedElement() ;

function LoadSelected()
{
	if ( !eSelected )
		return ;

	if ( eSelected.tagName.toLowerCase() == 'span' && eSelected._fckInsertConditionId )
	{
		var spanTextValue = eSelected.innerHTML;
		var condition = getConditionalTag(spanTextValue);

		document.getElementById('condProperty').value = getMemberValue(condition);
		document.getElementById('condOperator').value = getOperator(condition);
		var tmpValue = getValue(condition);
		document.getElementById('condValueText').value = tmpValue;
		
		// Check if value is a member property
		var startPos = tmpValue.indexOf('[[');
		var endPos = tmpValue.indexOf(']]');		
		if(startPos >= 0 && endPos >= 0 && endPos > startPos)
		{
			document.getElementById('condValueSelect').value = tmpValue.substring(startPos + 2, endPos);
		}
		
		document.getElementById('condMessage').value = getMessage(condition);
	}
	else
	{
		eSelected == null ;
	}
}

function Ok()
{
	var condProperty 	= document.getElementById('condProperty').value ;
	var condOperator 	= document.getElementById('condOperator').value ;	
	var condValue 		= document.getElementById('condValueText').value ;	
	var tmpCondValue 	= trim(condValue);
	if (tmpCondValue == '') condValue = '[[' + document.getElementById('condValueSelect').value + ']]';
		
	var condMessage 	= trim(document.getElementById('condMessage').value);
	if (condMessage==null || condMessage == "") condMessage = "Waiting...";
	
	if ( condProperty.length == 0 || condOperator.length == 0 | condValue.length == 0)
	{
		alert( FCKLang.InsertConditionErrNoName ) ;
		return false ;
	}
	
	FCKInsertCondition.Add( condProperty, condOperator, condValue, condMessage ) ;
	return true ;
}

function getConditionalTag(innerHTML)
{
	var startCondition= "((";
	var stopCondition="))";
	
	// To keep the changes at minimum, conditional tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startConPos = innerHTML.indexOf(startCondition);	
	var endConPos = innerHTML.indexOf(stopCondition);
	if(startConPos >= 0 && endConPos >= 0 && endConPos > startConPos)
	{
		var metaString = innerHTML.substring(startConPos + 2, endConPos);
		var conditionPos = metaString.toLowerCase().indexOf("conditional:");	
		if(conditionPos >= 0)
		{
			return metaString;
		}
	}	
	
	return "";
}

function getMemberValue(condition)
{
	var startValue = "[[";
	var stopValue = "]]";
	
	// To keep the changes at minimum, conditional tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startValuePos = condition.indexOf(startValue);
	var endValuePos = condition.indexOf(stopValue);
	if(startValuePos >= 0 && endValuePos >= 0 && endValuePos > startValuePos)
	{
		var value = condition.substring(startValuePos + startValue.length, endValuePos);
		return value;
	}
	
	return "";
}
function getOperator(condition)
{
	if(condition.indexOf('&lt;=') >= 0 || condition.indexOf('<=') >= 0) return '<=';
	if(condition.indexOf('&gt;=') >= 0 || condition.indexOf('>=') >= 0) return '<=';
	if(condition.indexOf('!=') >= 0) return '!=';
	if(condition.indexOf('=') >= 0) return '=';
	if(condition.indexOf('&lt;') >= 0 || condition.indexOf('<') >= 0) return '<';
	if(condition.indexOf('&gt;') >= 0 || condition.indexOf('>') >= 0) return '>';	
	
	return '';
}

function getLiteralOperator(condition)
{
	if(condition.indexOf('&lt;=') >= 0) return '&lt;=';
	if(condition.indexOf('&gt;=') >= 0) return '&gt;=';
	if(condition.indexOf('<=') >= 0) return '<=';
	if(condition.indexOf('>=') >= 0) return '>=';	
	if(condition.indexOf('!=') >= 0) return '!=';
	if(condition.indexOf('=') >= 0) return '=';
	if(condition.indexOf('&lt;') >= 0) return '&lt;';
	if(condition.indexOf('&gt;') >= 0) return '&gt;';	
	if(condition.indexOf('<') >= 0) return '<';
	if(condition.indexOf('>') >= 0) return '>';	
	
	return '';
}

function getValue(condition)
{
	var operator = getLiteralOperator(condition);
	var dividerPos = condition.indexOf('|');
	if (operator.length > 0) {
		var value = condition.substring(condition.indexOf(operator) + operator.length, dividerPos);
		return trim(value);
	}
	
	return "";
}


function getMessage(condition)
{
	var dividerPos = condition.lastIndexOf('|')
	if (dividerPos > 0) {
		return trim(condition.substring(dividerPos + 1));
	}
	
	return "";
}


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function noWhiteSpace(s)
{
	if((s==null)||(typeof(s)!='string')||!s.length)return'';return s.replace(/\s+/g,'');
}
		</script>
	</head>
	<body scroll="no" style="OVERFLOW: hidden">
		<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td>
		 			<table cellSpacing="0" cellPadding="0" align="center" border="0" width="100%" height="100%">
						<tr>
							<td align="left" width="30%">Member Property:</td>
							<td align="left">
								<select id="condProperty">
								<% 
									Set keySet = memberValues.keySet();
									Iterator keyIterator = keySet.iterator();	
									while (keyIterator.hasNext()){
										String key = (String)keyIterator.next();
										String displayValue = (String)memberValues.get(key);
								%>
									<option value="<%= key %>"><%= displayValue %></option>
								<%
									}
								%>
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" width="30%">Operator:</td>
							<td align="left">
								<select id="condOperator">
									<option value="=">=</option>
									<option value="&lt;">&lt;</option>
									<option value="&gt;">&gt;</option>
									<option value="&lt=">&lt;=</option>
									<option value="&gt;=">&gt;=</option>																																				
									<option value="!=">!=</option>																
								</select>
							</td>							
						</tr>
						<tr>
							<td align="left" width="30%">Value:</td>
							<td>
							<table width="100%">
								<tr>
									<td align="left">
										<select id="condValueSelect">
										<% 
											Iterator newKeyIterator = keySet.iterator();	
											while (newKeyIterator.hasNext()){
												String key = (String)newKeyIterator.next();
												String displayValue = (String)memberValues.get(key);
										%>
											<option value="<%= key %>"><%= displayValue %></option>
										<%
											}
										%>
										</select>
									</td>
									<td>
										<input id="condValueText" type="text">
									</td>							
								</tr>								
							</table>
							</td>
						</tr>
						<tr>
							<td align="left" width="30%">Message:</td>
							<td align="left">
								<input id="condMessage" type="text" maxlength="60" size="62">
							</td>						
						</tr>	
						<tr>
							<td colspan="2"><%= note %></td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>