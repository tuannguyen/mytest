/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Conditional Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.
FCKCommands.RegisterCommand( 'InsertCondition', new FCKDialogCommand( 'InsertCondition', FCKLang.InsertConditionDlgTitle, FCKPlugins.Items['InsertCondition'].Path + 'fck_InsertCondition.jsp', 520, 300 ) ) ;

// Create the "Plaholder" toolbar button.
var oInsertConditionItem = new FCKToolbarButton( 'InsertCondition', FCKLang.InsertConditionBtn ) ;
oInsertConditionItem.IconPath = FCKPlugins.Items['InsertCondition'].Path + 'InsertCondition.png' ;

FCKToolbarItems.RegisterItem( 'InsertCondition', oInsertConditionItem ) ;


// The object used for all InsertCondition operations.
var FCKInsertCondition = new Object() ;

// the element that <span> conditional tag </span> applies
var appliedElement = null;

// Add a new InsertCondition at the actual selection.
FCKInsertCondition.Add = function( property, operator, value, message)
{
	var oSpan = FCK.CreateElement( 'SPAN' ) ;
	this.SetupCondition( oSpan, property, operator, value, message) ;
	
	finalizeConditionTag(oSpan);
	appliedElement = null;
}

FCKInsertCondition.SetupCondition = function( span, property, operator, value, message)
{
	span.innerHTML = '((Conditional:' + '[[' + property + ']] ' + operator + ' ' + value + '|' + message + '))';
	if ( FCKBrowserInfo.IsGecko )
		span.style.cursor = 'default' ;

	span._fckInsertConditionId = generateNextID('_fckConditional');
	span.contentEditable = 'false';
	//span.style.color	= "#000000";			
	span.style.backgroundColor	= "#eeeeee";

	// To avoid it to be resized.
	span.onresizestart = function()
	{
		FCK.EditorWindow.event.returnValue = false ;
		return false ;
	}
}

// On Gecko we must do this trick so the user select all the SPAN when clicking on it.
FCKInsertCondition._SetupClickListener = function()
{
	FCKInsertCondition._ClickListener = function( e )
	{
		if ( e.target.tagName.toLowerCase() == 'span' && e.target._fckInsertConditionId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}
		if ( e.target.tagName.toLowerCase() == 'span' && e.target._fckInsertMemberValueId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}
		if ( e.target.tagName.toLowerCase() == 'div' && e.target._fckInsertTableId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}						
	}

	FCK.EditorDocument.addEventListener( 'click', FCKInsertCondition._ClickListener, true ) ;
}

// Open the dialog on double click. This function is shared with other plugins which use element for its place holder
FCKInsertCondition.OnDoubleClick = function( element )
{	
	if ( element.tagName.toLowerCase() == 'span' && element._fckInsertConditionId)
	{
		appliedElement = findNextElement(element);	// for conditional tag only			
		FCKCommands.GetCommand( 'InsertCondition' ).Execute() ;	
		return;
	}
	if ( element.tagName.toLowerCase() == 'span' && element._fckInsertMemberValueId)
	{
		//alert('_fckInsertMemberValueId: ' + span._fckInsertMemberValueId);
		FCKCommands.GetCommand( 'InsertMemberValue' ).Execute();
		return;
	}
	if ( element.tagName.toLowerCase() == 'div' && element._fckInsertTableId)
	{
		//alert('_fckInsertTableId: ' + div._fckInsertTableId);
		appliedElement = findPreviousElement(element);	// for table tag only		
		FCKCommands.GetCommand( 'InsertTable' ).Execute() ;
		return;
	}		
}

FCK.RegisterDoubleClickHandler( FCKInsertCondition.OnDoubleClick, 'DIV' );
FCK.RegisterDoubleClickHandler( FCKInsertCondition.OnDoubleClick, 'SPAN' );


FCKInsertCondition.Redraw = function()
{
	TreeWalkerHighlight(FCK.EditorDocument);
}

FCK.Events.AttachEvent( 'OnAfterSetHTML', FCKInsertCondition.Redraw );

/*End Get All tag from html*/
/*Walk with normal tag*/
function TreeWalkerHighlight(rootNode){
        
    if(rootNode == null){
        return;
    }
    
    var length = rootNode.childNodes.length;
	var i=0;
	    		    
	for(i=0; i<length; i++){
	    var child = rootNode.childNodes[i];
	    
	    if(child.nodeType == 1)//is element node
	    {		        
	        TreeWalkerHighlight(child);
	        
	    }else if(child.nodeType == 3)//is text node
	    {
	        highlightNode(child);

	    }
    }    
}
/*End Walk*/
/*
*Start modify tag
* Conditional or Table tag is consisdered the only 1 child of its parent node.
* Member Value tag can comes with others in a text node
*/
function highlightNode(child)
{
    var parameter = child.nodeValue;
	var startCondition= "((";
	var stopCondition="))";
	var startMemberValue= "[[";
	var stopMemberValue="]]";	
	
	// To keep the changes at minimum, conditional tag are handled in a diffirrent part. //HUY: MODIFY HERE
	var startConPos = parameter.indexOf(startCondition);
	var endConPos = parameter.indexOf(stopCondition);
	if(startConPos >= 0 && endConPos > startConPos)
	{	
		var metaString = parameter.substring(startConPos + 2, endConPos);
		// Normarlize node content
		child.parentNode.insertBefore(
			FCK.EditorDocument.createTextNode(startCondition + metaString + stopCondition), child);
		child.parentNode.style.backgroundColor	= "#eeeeee";
		child.parentNode.contentEditable = false;
		child.parentNode.onresizestart = function()
		{
			FCK.EditorWindow.event.returnValue = false ;
			return false ;
		}
				
		var conditionPos = metaString.toLowerCase().indexOf("conditional:");
		var tablePos = metaString.toLowerCase().indexOf("table(");	
		if(conditionPos >= 0)
		{		
			if (!child.parentNode._fckInsertConditionId) {
				child.parentNode._fckInsertConditionId = generateNextID('_fckConditional');
			}
		}
		else if(tablePos >= 0)
		{
			if (!child.parentNode._fckInsertTableId) {
				child.parentNode._fckInsertTableId = generateNextID('_fckTable');
			}
		}
		// remove this child
		child.parentNode.removeChild(child);
	}
	else
	{
		var startMemberPos = parameter.indexOf(startMemberValue);
		var endMemberPos = parameter.indexOf(stopMemberValue);		
		if(startMemberPos >= 0 && endMemberPos > startMemberPos)
		{		
			var memberValue = parameter.substring(startMemberPos + 2, endMemberPos);
			if(	memberValue.toLowerCase().indexOf("dictionary:")>=0 || memberValue.toLowerCase().indexOf("dictionary(") >=0 ||
				memberValue.toLowerCase().indexOf("caveat:")>=0 || memberValue.toLowerCase().indexOf("caveat(") >=0 ||
				memberValue.toLowerCase().indexOf("faq:")>=0 || memberValue.toLowerCase().indexOf("faq(")>=0 ||
				memberValue.toLowerCase().indexOf("payslip:")>=0 || memberValue.toLowerCase().indexOf("payslip(")>=0)
			{	
				//isMemberValue = false;	// dictionary, caveat and faq tag will be handled later
				return;
			}			
			var pa = child.parentNode;			
			replaceMemberValue(pa, child.nodeValue, child);
			child.parentNode.removeChild(child);
			removeRedundantTag(pa);
		}
	}
}

// recursive function to replace member value with <span> element
function replaceMemberValue(parentNode, textValue, child)
{					
	var startMemberValue = "[[";
	var endMemberValue = "]]";	
	var startMemberPos = textValue.indexOf(startMemberValue);
	var endMemberPos = textValue.indexOf(endMemberValue);
	//alert('textValue: ' + textValue);
	if(startMemberPos >= 0 && endMemberPos > startMemberPos)
	{	
		var memberValue = textValue.substring(startMemberPos + 2, endMemberPos);
		//alert('memberValue: ' + memberValue);
		var isMemberValue = true;
		
		var beforeText = textValue.substring(0, startMemberPos);
		if (beforeText.length) 
		{
			//alert('beforeText: ' + beforeText);
			parentNode.insertBefore(FCK.EditorDocument.createTextNode(beforeText) , child);
		}
		var oSpan = FCK.EditorDocument.createElement( 'span' ) ;
		if (isMemberValue)
		{
			oSpan._fckInsertMemberValueId = generateNextID('_fckMemberValue');
			oSpan.contentEditable = 'false';
		}			
		else 
		{
			oSpan.style.backgroundColor	= "#eeeeee";
		}
		oSpan.innerHTML = startMemberValue + memberValue + endMemberValue;
		// To avoid it to be resized.
		oSpan.onresizestart = function()
		{
			FCK.EditorWindow.event.returnValue = false ;
			return false ;
		}
		parentNode.insertBefore( oSpan, child ) ;	
		
		var afterText = textValue.substring(endMemberPos + 2);
		if (afterText.length) 
		{
			//alert('afterText: ' + afterText);
			replaceMemberValue(parentNode, afterText, child)
		}
	}
	else 
	{
		parentNode.insertBefore(FCK.EditorDocument.createTextNode(textValue) , child);
	}
}
/*
 * Remove redundant tag when parent and the only child has the same tag name
 */
function removeRedundantTag(pa)
{
	var grandPa = pa.parentNode;
	var children = pa.childNodes;
	var numElementChild = 0;
	var hasTextNode = false;
	var onlyChild = null;
	for(var i=0; i<children.length; i++) {
		var child = children[i];
		if (child.nodeType == 1)
		{ 
			numElementChild += 1;
			onlyChild = child;
		}
		else if (child.nodeType == 3)
		{
			if(child.nodeValue.length)
			{ 
				hasTextNode = true;
				break;
			}
		}
	}
	
	if (!hasTextNode && numElementChild == 1 && pa.tagName == onlyChild.tagName)
	{
		grandPa.insertBefore(onlyChild, pa);
		grandPa.removeChild(pa);
	}
}

function generateNextID (coreName)
{
	var i = 1;
	coreName = coreName + "_";
	while(true){
	
		if(document.getElementById(coreName + i) == null){
			coreName += i;					
			break;
		}else{
			++i;
		}
	}
	
	return coreName;
}

function finalizeConditionTag (span)
{
	if(span != null)
	{				
		if (appliedElement)
		{
			appliedElement.parentNode.insertBefore(span, appliedElement);
			//span.parentNode.removeChild(span);
			return;			
		}
		
		// else, new conditional tag created, adjust it to the right place
		var pa = span.parentNode;
		var grandPa = pa.parentNode;		
		var elderBrother = span.previousSibling;
		var youngerBrother = span.nextSibling;		
		var hasElderText = false;		
		var hasYoungerText = false;	
		var hasElderElement = false;		
		var hasYoungerElement = false;
			
		if (youngerBrother)	
		{
			if (youngerBrother.nodeType == 3) 
			{
				if(!isWhiteSpace(youngerBrother.nodeValue.replace(/\xA0/g,' ')))
				{				
					//alert('younger text: ' + elderBrother.nodeValue);						
					hasYoungerText = true;
				}
				else 
				{
					//alert('youngerBrother text empty');
					// search for younger element
					var tempYoungerBrother = youngerBrother;
					while (tempYoungerBrother) {
						if (tempYoungerBrother.nodeType == 1)
						{
							hasYoungerElement = true;
							//alert(tempYoungerBrother.tagName);							
							break;
						}
						tempYoungerBrother = tempYoungerBrother.nextSibling;
					}
				}				
			} 
			else if (youngerBrother.nodeType == 1 )
			{				
				hasYoungerElement = true;
				//alert("younger tagname: " + youngerBrother.tagName);				
			}
		}				
			
		if (elderBrother)
		{
			if (elderBrother.nodeType == 3) 
			{
				if (!isWhiteSpace(elderBrother.nodeValue.replace(/\xA0/g,' ')))
				{
					//alert('elder text: ' + elderBrother.nodeValue);
					hasElderText = true;
				} 
				else 
				{
					// search for elder element
					var tempElderBrother = elderBrother;
					while (tempElderBrother) {
						if (tempElderBrother.nodeType == 1)
						{
							hasElderElement = true;
							break;
						}
						tempElderBrother = tempElderBrother.previousSibling;
					}					
				}
			}
			else if (elderBrother.nodeType == 1 )
			{
				hasElderElement = true;
				//alert("elder tagname: " + elderBrother.tagName);				
			}				
		}		
		
		//alert('hasElderText: ' + hasElderText);
		//alert('hasYoungerText: ' + hasYoungerText);
		//alert('hasElderElement: ' + hasElderElement);
		//alert('hasYoungerElement: ' + hasYoungerElement);			
		// if no element and text siblings found and there is no element siblings, parent is redundant tag, remove it		
		if (!hasElderText && !hasYoungerText && !hasElderElement && !hasYoungerElement) 
		{				
			if (grandPa)
			{
				grandPa.insertBefore(span, pa);
				grandPa.removeChild(pa);
				//alert('insert 1');				
			}
			return;
		}
	
		if (hasElderText || hasYoungerText)	// make conditional as an uncle
		{
			if (grandPa)
			{
				//alert('insert 2');
				grandPa.insertBefore(span, pa);
			}
			return;
		}
	}	
}

function findNextElement (element)
{
	var nextElement = element.nextSibling;
	while (nextElement)										// FireFox has a text node with empty value
	{
		if (nextElement.nodeType == 1) {					// Element node in both IE and FF
			return nextElement;	
		}
		else {
			nextElement = nextElement.nextSibling;
		}				
	}
	
	return null;
}


function findPreviousElement (element)
{
	var preElement = element.previousSibling;
	while (preElement)										// FireFox has a text node with empty value
	{
		if (preElement.nodeType == 1) {					// Element node in both IE and FF
			return preElement;	
		}
		else {
			preElement = preElement.previousSibling;
		}				
	}
	
	return null;
}

function isWhiteSpace(s)
{
	if (!s)	return true;
	var whitespace = " \t\n\r";
   	for (i = 0; i < s.length; i++)
   	{
        // Check that current character is whitespace.
        var c = s.charAt(i);
        if (whitespace.indexOf(c) == -1) return false;
   	}

   	// All characters are not whitespace.
   	return true;	
}