/*
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * "Support Open Source software. What about a donation today?"
 * 
 * File Name: fckplugin.js
 * 	Plugin to insert "Signatureal Tag" in the editor.
 * 
 * File Authors:
 * 		Huy Tran (HuyTran@c-mg.net)
 */

// Register the related command.

FCKCommands.RegisterCommand( 'InsertSignature', new FCKDialogCommand( 'InsertSignature', FCKLang.InsertSignatureDlgTitle, FCKPlugins.Items['InsertSignature'].Path + 'fck_InsertSignature.jsp', 620, 360) ) ;

// Create the "Plaholder" toolbar button.
var oInsertSignatureItem = new FCKToolbarButton( 'InsertSignature', FCKLang.InsertSignatureBtn ) ;
oInsertSignatureItem.IconPath = FCKPlugins.Items['InsertSignature'].Path + 'InsertSignature.png' ;

FCKToolbarItems.RegisterItem( 'InsertSignature', oInsertSignatureItem ) ;


// The object used for all InsertSignature operations.
var FCKInsertSignature = new Object() ;

// the element that <span> Signatureal tag </span> applies
var appliedElement = null;

// Add a new InsertSignature at the actual selection.
FCKInsertSignature.Add = function(id){
	var oP = FCK.CreateElement( 'P' ) ;
	var oSpan = FCK.CreateElement( 'SPAN' ) ;
	oSpan.innerHTML = '((--Signature:' + id + '--))';
	oSpan.contentEditable = 'false';
}

// On Gecko we must do this trick so the user select all the SPAN when clicking on it.
FCKInsertSignature._SetupClickListener = function(){
	alert('listener');
	FCKInsertSignature._ClickListener = function( e ){
		if ( e.target.tagName.toLowerCase() == 'span' && e.target._fckInsertSignatureId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}
		if ( e.target.tagName.toLowerCase() == 'span' && e.target._fckInsertMemberValueId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}
		if ( e.target.tagName.toLowerCase() == 'div' && e.target._fckInsertTableId)
		{
			FCKSelection.SelectNode( e.target ) ;
			return;
		}						
	}

	FCK.EditorDocument.addEventListener( 'click', FCKInsertSignature._ClickListener, true ) ;
}

// Open the dialog on double click. This function is shared with other plugins which use element for its place holder
FCKInsertSignature.OnDoubleClick = function( element )
{	
	if ( element.tagName.toLowerCase() == 'span' && element._fckInsertSignatureId)
	{
		appliedElement = findNextElement(element);	// for Signatureal tag only			
		FCKCommands.GetCommand( 'InsertSignature' ).Execute() ;	
		return;
	}
	if ( element.tagName.toLowerCase() == 'div' && element._fckInsertTableId)
	{
		//alert('_fckInsertTableId: ' + div._fckInsertTableId);
		appliedElement = findPreviousElement(element);	// for table tag only		
		FCKCommands.GetCommand( 'InsertSignature' ).Execute() ;
		return;
	}		
}

FCK.RegisterDoubleClickHandler( FCKInsertSignature.OnDoubleClick, 'DIV' );
FCK.RegisterDoubleClickHandler( FCKInsertSignature.OnDoubleClick, 'SPAN' );