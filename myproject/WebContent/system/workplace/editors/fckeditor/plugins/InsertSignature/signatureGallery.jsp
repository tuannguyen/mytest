<%@ page session="true" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.opencms.jsp.*, org.opencms.main.*, org.opencms.file.*"%>
<%@page import="com.bp.pensionline.letter.util.SignatureUtil"%>
<%@page import="com.bp.pensionline.letter.util.SignatureXmlDaoUtil"%>
<%@page import="com.bp.pensionline.letter.constants.Constants"%>
<%@page import="com.bp.pensionline.letter.constants.IconFile"%>
<%@page import="com.bp.pensionline.letter.dao.SignatureXmlDao"%>
<%@page import="com.bp.pensionline.util.CheckConfigurationKey"%>
<%@page import="org.opencms.file.*"%>
<%@page import="org.opencms.jsp.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
	CmsJspActionElement cmsJspAction = new CmsJspActionElement(pageContext, request, response);
	CmsJspLoginBean cmsLogin = new CmsJspLoginBean(pageContext, request, response);
	if (!cmsLogin.isLoggedIn()) {
		String username = CheckConfigurationKey.getStringValue("adminName");
		String password = CheckConfigurationKey.getStringValue("adminPassword");
		cmsLogin.login(username,password);
	}
	
	CmsJspBean bean=new CmsJspBean();
	bean.init(pageContext, request, response);
	CmsObject cms = bean.getCmsObject();
	
	SignatureUtil sU = new SignatureUtil(Constants.SIGNATURE.IMAGE_GALLERY_LOCATION);
	List icons = sU.getAllIcons();
	System.out.println("Size: "+icons.size());
%>
<html>
<head>
	<title>Image galleries</title>
    <script type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.template/resources/scripts/browser.js</cms:link>"></script>
	<script type="text/javascript" language="javascript">
	// IE7 is the development platform of choice
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version >= "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.ie.css</cms:link>' type='text/css' media='print' />");

	}
	// check if IE6 is detected, and degrade some features
	if( BrowserDetect.browser == "Explorer" && BrowserDetect.version == "6" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.ie6.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.ie6.css</cms:link>' type='text/css' media='print' />");
	
	}
	// check if Firefox is detected
	if( BrowserDetect.browser == "Firefox" ) {
		document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.firefox.css</cms:link>' type='text/css' media='screen, projection, print' />");
                document.write("<link rel='stylesheet' href='<cms:link>/system/modules/com.bp.pensionline.template/resources/style.print.css</cms:link>' type='text/css' media='print' />");                
	        
	}

function addImage() {
	var imageURL="";
	var radioList = document.getElementsByName('<%=Constants.SIGNATURE.TAG_PATH%>');
	for(var i=radioList.length-1; i>-1; i--) {		
		if (radioList[i].checked) {			
			imageURL = radioList[i].value;		
		}	
	}
	if (imageURL=="") {
		alert("Please select image!");
		return;
	} else {
		window.opener.document.getElementById('onlineSource').value=imageURL;
		window.opener.document.getElementById('<%=Constants.SIGNATURE.TAG_PATH%>').value=imageURL;
		window.close();
	}
}
function popupHelp(url) {
	window.open(url,'PL4WIKI','width=800,height=600,status=no,resizable=0,top=110,left=200,toolbar=no,menubar=no,scrollbars=yes');
}
	</script>
<style>
.sButton {width: 80px;}
</style>
</head>
<body leftmargin="0" rightmargin="0">
<table width="100%" cellspacing="0" border="0">
<colgroup>
<col width="80%">
<col width="20%">
</colgroup>
<tr><td colspan="2"><H2>Please select one of below images:</H2></td></tr>
<%for (int i=0; i<icons.size(); i++) {
	IconFile icon = (IconFile)icons.get(i);
%>
<tr>
<td><input type="radio" name="<%=Constants.SIGNATURE.TAG_PATH%>" value="<%=icon.getUrl()%>"><%=icon.getName()%></td>
<td><img height="35" width="150" border="0" alt="Click to enlarge" style="cursor:hand" src="/content/pl<%=icon.getUrl()%>" onClick="popupHelp('/content/pl<%=icon.getUrl()%>');"/></td>
</tr>
<%}%>
<tr><td colspan="2" align="center"><input type="button" class="sButton" value="OK" onClick="addImage()">&nbsp;<input type="button" class="sButton" value="Cancel" onClick="window.close()"></td></tr>
</table>
</body>
</html>