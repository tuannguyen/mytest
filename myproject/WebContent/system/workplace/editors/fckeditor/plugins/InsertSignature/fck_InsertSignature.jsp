<%@ page session="true" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="org.w3c.dom.*" %>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.opencms.jsp.*, org.opencms.main.*, org.opencms.file.*"%>
<%@page import="com.bp.pensionline.letter.util.SignatureUtil"%>
<%@page import="com.bp.pensionline.letter.util.SignatureXmlDaoUtil"%>
<%@page import="com.bp.pensionline.letter.constants.Constants"%>
<%@page import="com.bp.pensionline.letter.constants.IconFile"%>
<%@page import="com.bp.pensionline.letter.dao.SignatureXmlDao"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
	SignatureUtil sU = new SignatureUtil(Constants.SIGNATURE.IMAGE_GALLERY_LOCATION);
	List icons = sU.getAllIcons();
	System.out.println("Size: "+icons.size());

	SignatureXmlDaoUtil daoUtil = new SignatureXmlDaoUtil(Constants.SIGNATURE.XML_VFS_PATH);
	List signList = (List)daoUtil.getSignList();
	
	Boolean isOnline = (Boolean)request.getSession().getAttribute(Constants.SIGNATURE.TAG_ONLINE);
	String path = (String)request.getSession().getAttribute(Constants.SIGNATURE.TAG_PATH);
	String id = (String)request.getSession().getAttribute(Constants.SIGNATURE.TAG_ID);
	System.out.println("ID IN SESSION: "+id);
	request.getSession().removeAttribute(Constants.SIGNATURE.TAG_ONLINE);
	request.getSession().removeAttribute(Constants.SIGNATURE.TAG_PATH);
	request.getSession().removeAttribute(Constants.SIGNATURE.TAG_ID);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for internet
 * Copyright (C) 2003-2006 Frederico Caldeira Knabben
 *
 *
 * File Name: fck_InsertCondition.html
 * 	Conditional Tag Plugin.
 *
 * File Authors:
 * 		Huy Tran (huytran@c-mg.net)
-->
<html>
	<head>
		<title>Setting up signature</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="noindex, nofollow" name="robots">
		<script language="javascript" type="text/javascript" src="<cms:link>/system/modules/com.bp.pensionline.test/resources/ajax/xmldom.js</cms:link>"></script>
        
		<script language="javascript">
var oEditor = window.parent.InnerDialogLoaded() ;
var FCKLang = oEditor.FCKLang ;
var FCKInsertSignature = oEditor.FCKInsertSignature;

window.onload = function () {
	// Show the "Ok" button.
	window.parent.SetOkButton(true) ;
}

function popupHelp(url) {
	window.open(url,'PL4WIKI','width=800,height=600,status=no,resizable=0,top=110,left=200,toolbar=no,menubar=no,scrollbars=yes');
}

var signArray = new Array();
<%for(int i=0; i<signList.size(); i++) {
	SignatureXmlDao signature = (SignatureXmlDao)signList.get(i);
%>
	signArray.push(new Array("<%=signature.getId()%>",<%=signature.isUseOnlineImage()%>,"<%=signature.getImageFileName()%>","<%=signature.getName()%>","<%=signature.getTelephone()%>","<%=signature.getEmail()%>","<%=signature.getUrl()%>","<%=signature.getTitle()%>"));
<%}%>

function Ok() {
	//title = title.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');
	//name = name.replace(/&/g, '(amp)').replace(/</g, '(lt)').replace(/>/g,'(gt)').replace(/\+/g,'(plus)').replace(/\%/g,'(percent)');
	var ids = document.getElementsByName('<%=Constants.SIGNATURE.TAG_ID%>');
	var id = null;
	for (var i=0; i<ids.length; i++) {
		if (ids[i].checked) {
			id = ids[i].value;
			break;
		}
	}
	if (id!=null) {
		//alert(id);
		FCKInsertSignature.Add(id) ;
		return true;
	} else {
		alert("Please select signature!");
		return false;
	}
}

var currentURL = "<%=request.getRequestURI()%>";
function changeImageSource() {
	var isOnline = document.getElementById('<%=Constants.SIGNATURE.TAG_ONLINE%>').value;
	var onlineDiv = document.getElementById('onlineSourceDiv');
	var localDiv = document.getElementById('localSourceDiv');
	if(isOnline=="online") {
		onlineDiv.style.display="";
		localDiv.style.display="none";
	} else {
		onlineDiv.style.display="none";
		localDiv.style.display="";
	}
}
function showImageGallery(url) {
	//document.getElementById("popupGallery").innerHTML = "<img src='waiting.gif' alt='Please wait...'/>";
	window.open(url,'Gallery','width=600,height=400,status=no,resizable=0,dependent=yes,toolbar=no,menubar=no,scrollbars=yes');
}
var signature = null;
var isChangeImage = false;
function createCustomSignature(form, action) {
	var isUpdate = "Save"==form.Create.value?true:false;
	var selectObj = document.getElementById('<%=Constants.SIGNATURE.TAG_ONLINE%>');
	var path = "";
	var isOnline = false;
	if ("online"==selectObj.options[selectObj.selectedIndex].value) {
		path = document.getElementById('onlineSource').value;
		isOnline = true;
	} else {
		if (document.getElementById('localSource')) {
			path = document.getElementById('localSource').value;
		}
	}
	if (isUpdate) {
		if (action != "Upload")	action = "Update";
		if (isOnline!=signature[1] || isChangeImage)  {
			if(trim(path)=="") {
				//alert("Please select image for signature!");
				//return;
			} else {
				form.<%=Constants.SIGNATURE.TAG_PATH%>.value = path;
			}
			isChangeImage = false;
		}
	} else {
		if(trim(path)=="") {
			//alert("Please select image for signature!");
			//return;
		} else {
			form.<%=Constants.SIGNATURE.TAG_PATH%>.value = path;
		}
	}
	var name = document.getElementById('<%=Constants.SIGNATURE.TAG_NAME%>').value;
	if (trim(name)=="") {
		alert("Name is required!");
		return;
	} else {
		if (isUpdate) {
			var id = form.<%=Constants.SIGNATURE.TAG_ID%>.value;
			for(var i=0; i<signArray.length; i++) {
				if (id!=signArray[i][0] && name==signArray[i][0]) {
					alert(name+" is already existing. Please use another name, for example: "+name+"1,...");
					return;
				}
			}
		} else {
			for(var i=0; i<signArray.length; i++) {
				if (name==signArray[i][0]) {
					alert(name+" is already existing. Please use another name, for example: "+name+"1,...");
					return;
				}
			}
		}
	}
	signature = null;
	document.getElementById('<%=Constants.SIGNATURE.ACTION%>').value = action;
	//alert(currentURL);
	form.submit();
}
function showCreateSignatureForm(isShow, formHeader) {
	var formDiv = document.getElementById("signatureFormDiv");
	if (formDiv) {
		if (isShow) {
			formDiv.style.display = "";
		} else {
			formDiv.style.display = "none";
		}
		document.getElementById("signatureFormSpan").innerHTML="<b>"+formHeader+"</b>";
		var localSourceDiv = document.getElementById("localSourceDiv");
		localSourceDiv.innerHTML = "<input type='file' accept='image/jpeg' id='localSource' name='localSource' class='signature_file'/>";
		localSourceDiv.style.display = "";
		document.getElementById("onlineSourceDiv").style.display="none";
		document.forms[0].Create.value = "Create";
		document.forms[0].reset();
		document.forms[0].<%=Constants.SIGNATURE.TAG_ONLINE%>.selectedIndex = 0;
	}
	return;
}
function showEditSignatureForm(isNew) {
	if (isNew) {
		showCreateSignatureForm(true,'Create your custom signature:');
	} else {
		showCreateSignatureForm(true,'Edit signature');
		document.forms['signatureForm'].Create.value = "Save";
	}
	document.getElementById("localSourceDiv").style.display="none";
	document.getElementById("onlineSourceDiv").style.display="";
	if (document.getElementById("signatureFormSpan")) {
		//document.getElementById("signatureFormSpan").focus();
	}
}
function changeLocalImage(bool) {
	var localSourceDiv = document.getElementById("localSourceDiv");
	if(bool) {
		isChangeImage = true;
		localSourceDiv.innerHTML = "<input type='file' accept='image/jpeg' id='localSource' name='localSource' class='signature_file'/>&nbsp;<a href='#signatureFormSpan' onclick='changeLocalImage(false)'>use current image</a>";
	} else {
		isChangeImage = false;
		localSourceDiv.innerHTML = "<span alt='Current local image path'>"+signature[2]+"</span>&nbsp;<a href='#signatureFormSpan' onclick='changeLocalImage(true)'>change image</a>";
	}
}
function resetOnlineImage() {
	isChangeImage = true;
	//var form = document.forms[0];
}
function deleteSignature(id) {
	var form = document.forms['delForm'];
	form.<%=Constants.SIGNATURE.TAG_ID%>.value = id;
	form.<%=Constants.SIGNATURE.ACTION%>.value = "Delete";
	if(confirm("Are you sure you want to delete this signature?")) {
		form.submit();
	} else {
		return;
	}
}
function editSignature(id) {
	showCreateSignatureForm(true,'Edit signature');
	var form = document.forms[0];
	form.Create.value = "Save";
	for (var i=0; i<signArray.length; i++) {
		if (id == signArray[i][0]) {
			signature = signArray[i];
			//alert(signature);
			break;
		}
	}
	//alert(id+"\n"+signature);
	if (signature != null) {
		form.<%=Constants.SIGNATURE.TAG_ID%>.value = id;
		form.<%=Constants.SIGNATURE.ACTION%>.value = "Update";
		var isOnline = null;
		var path = null;
		<%if(isOnline!=null){%>
			if (id=="<%=id%>") isOnline = <%=isOnline.booleanValue()%>;
			else isOnline = signature[1];
		<%}else{%>
			isOnline = signature[1];
		<%}%>
		if (isOnline) {
			if(id=="<%=id%>") {
				path = "<%=path%>";
			} else {
				path = signature[2];
			}
			document.getElementById("localSourceDiv").style.display="none";
			document.getElementById("onlineSourceDiv").style.display="";
			form.onlineSource.value = path;
			form.<%=Constants.SIGNATURE.TAG_ONLINE%>.options[1].selected = true;
		} else {
			path = signature[2];
			var localSourceDiv = document.getElementById("localSourceDiv");
			localSourceDiv.innerHTML = "<span alt='Current local image path'>"+path+"</span>&nbsp;<a href='#signatureFormSpan' onclick='changeLocalImage(true)'>change image</a>";
			localSourceDiv.style.display="";
			document.getElementById("onlineSourceDiv").style.display="none";
			form.<%=Constants.SIGNATURE.TAG_ONLINE%>.options[0].selected = true;
		}
		form.<%=Constants.SIGNATURE.TAG_NAME%>.value = signature[3];
		form.<%=Constants.SIGNATURE.TAG_TELEPHONE%>.value = signature[4];
		form.<%=Constants.SIGNATURE.TAG_EMAIL%>.value = signature[5];
		form.<%=Constants.SIGNATURE.TAG_URL%>.value = signature[6];
		form.<%=Constants.SIGNATURE.TAG_TITLE%>.value = signature[7];
		form.<%=Constants.SIGNATURE.TAG_PATH%>.value = path;
	}
}
		</script>
    <style>
/*signature upload css*/
.signature_file {
	height: 20px;
}
	</style>
	</head>
<body scroll="yes" style="OVERFLOW: scroll" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	 			<!--<table cellSpacing="0" cellPadding="0" align="center" border="1" width="100%" height="100%" bgcolor="#FFFFCC">-->
                <table cellSpacing="0" cellPadding="0" align="center" border="0" width="100%" height="100%">
                   	<colgroup>
						<col width="50%">
                        <col width="10%">
						<col width="40%">
					</colgroup>
                    <tr><td colspan="3">Here are current signatures in the system, select one and click "OK" at the bottom to add. You are also able to create your custom signature in the section <a href="#signatureFormSpan" onClick="showCreateSignatureForm(true,'Create your custom signature:')">"Create your custom signature"</a>. If you upload your signature, it will be stored in the system and viewable by everyone.</td></tr>
                    <tr><td colspan="3">&nbsp;</td></tr>
                    <tr><td><b>Available signatures:</b></td><td align="left"><b>Edit</b></td><td align="center"><b>Thumbnails:</b></td></tr>
					<%for (int i=0; i<signList.size(); i++) {
							SignatureXmlDao signature = (SignatureXmlDao)signList.get(i);
						%>
					<tr>
					  <td><input type="radio" name="<%=Constants.SIGNATURE.TAG_ID%>" value="<%=signature.getId()%>" <%if(signature.isDefault()){%>checked<%}%>><%=signature.getName()%></td>
                      <td align="left"><%if(!signature.isDefault()){%><a href="#signatureFormSpan" onClick="editSignature('<%=signature.getId()%>')">Edit</a>/<a href="#signatureFormSpan" onClick="deleteSignature('<%=signature.getId()%>')">Delete</a><%}%></td>
						<td align="right"><%if(signature.isUseOnlineImage()){%><img height="35" width="150" border="0" alt="Click to enlarge" style="cursor:hand" src="/content/pl<%=signature.getImageFileName()%>" onClick="popupHelp('/content/pl<%=signature.getImageFileName()%>');"/>
                        	<%}else{%>Not available (user's local image)
                            <%}%>
                        </td>
					</tr>
					<%}%>
                    <tr>
                      <td colspan="3"><div id="signatureFormDiv" style="display:none">
                      <table width="100%" cellpadding="2" cellspacing="2"><form id="signatureForm" action="/content/SignatureCreator" method="post" enctype="multipart/form-data">
                      	    <input type="hidden" name="<%=Constants.SIGNATURE.ACTION%>" value="Save"/>
                            <input type="hidden" name="<%=Constants.SIGNATURE.TAG_ID%>" value=""/> <!--used in case edit-->
                            <input type="hidden" name="<%=Constants.SIGNATURE.URI%>" value="<%=request.getRequestURI()%>"/>
                            <input type="hidden" id="<%=Constants.SIGNATURE.TAG_PATH%>" name="<%=Constants.SIGNATURE.TAG_PATH%>" value="<%if(path!=null){%><%=path%><%}%>"/>
                            <colgroup>
                            <col width="40%">
                            <col width="60%">
                            </colgroup>
                            <tr>
                              <td colspan="2"><hr width="70%"/></td>
                            </tr>
                            <tr>
                              <td colspan="2"><a name="signatureFormSpan" id="signatureFormSpan"/><span id="signatureFormSpan" style="font-weight:bold">Create your custom signature:</span></td>
                            </tr>
                            <tr>
                              <td align="right">Select image source: &nbsp;</td>
                              <td><select id="<%=Constants.SIGNATURE.TAG_ONLINE%>" name="<%=Constants.SIGNATURE.TAG_ONLINE%>" onChange="changeImageSource()">
									<option value="local">Local</option>
                              		<option value="online" <%if(isOnline!=null&&isOnline.booleanValue()){%>selected<%}%>>Online</option>
                              </select></td>
                            </tr>
                            <tr>
                              <td align="right">Image: &nbsp; </td>
                              <td><div id="localSourceDiv" <%if(isOnline!=null&&isOnline.booleanValue()){%>style="display:none"<%}%>><input type="file" accept="image/jpeg" id="localSource" name="localSource" class="signature_file"/></div>
                              	  <div id="onlineSourceDiv" <%if(isOnline!=null&&!isOnline.booleanValue()){%>style="display:none"<%}%>>
                                  		<input type="text" id="onlineSource" name="onlineSource" value="<%if(path!=null){%><%=path%><%}%>" onChange="resetOnlineImage()"/>
                                        <span id="popupGallery"><input type="button" value="Gallery" onClick="showImageGallery('/content/pl/_signatureGallery.jsp')"/></span>
                                  </div>
                              </td>
                            </tr>
                            <tr>
                              <td align="right">Name(*): &nbsp; </td>
                              <td><input type="text" id="<%=Constants.SIGNATURE.TAG_NAME%>" name="<%=Constants.SIGNATURE.TAG_NAME%>" value=""/></td>
                            </tr>
                            <tr>
                              <td align="right">Job Title: &nbsp; </td>
                              <td><input type="text" id="<%=Constants.SIGNATURE.TAG_TITLE%>" name="<%=Constants.SIGNATURE.TAG_TITLE%>" value=""/></td>
                            </tr>
                            <tr>
                              <td align="right">Telephone: &nbsp; </td>
                              <td><input type="text" id="<%=Constants.SIGNATURE.TAG_TELEPHONE%>" name="<%=Constants.SIGNATURE.TAG_TELEPHONE%>" value=""/></td>
                            </tr>
                            <tr>
                              <td align="right">Email: &nbsp; </td>
                              <td><input type="text" id="<%=Constants.SIGNATURE.TAG_EMAIL%>" name="<%=Constants.SIGNATURE.TAG_EMAIL%>" value=""/></td>
                            </tr>
                            <tr>
                              <td align="right">Url: &nbsp; </td>
                              <td><input type="text" id="<%=Constants.SIGNATURE.TAG_URL%>" name="<%=Constants.SIGNATURE.TAG_URL%>" value=""/></td>
                            </tr>
                            <tr>
                              <td align="right">&nbsp; </td>
                              <td><input type="button" name="Create" value="Create" onClick="createCustomSignature(document.getElementById('signatureForm'),'Save')"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onClick="showCreateSignatureForm(false,'Create your custom signature:')"/></td>
                            </tr></form>
                      </table></div></td>
                    </tr>
			    </table>
</body>
<form id="delForm" name="delForm" action="/content/SignatureCreator" method="post" enctype="multipart/form-data">
	<input type="hidden" name="<%=Constants.SIGNATURE.ACTION%>" value="Delete"/>
	<input type="hidden" name="<%=Constants.SIGNATURE.TAG_ID%>" value=""/>
    <input type="hidden" name="<%=Constants.SIGNATURE.URI%>" value="<%=request.getRequestURI()%>"/>
</form>
<script>
<%if(isOnline!=null) {
	if(id!=null && id.trim().length()>0) {%>
	editSignature('<%=id%>');
	<%}else {%>
	showEditSignatureForm(false);
	<%}
}%>
</script>
</html>